#pragma once

#pragma managed

#include <afxwin.h>
#include <afxwinforms.h>
#include <list>
#include "resource.h"

#ifdef MYDEBUG
  #using <../GisControl/bin/Debug/GisControl.dll>
#else
  #using <../GisControl/bin/Release/GisControl.dll>
#endif

using namespace GisControl;
using namespace std;

class CGISView : public CWinFormsView
{
	DECLARE_DYNCREATE(CGISView)

protected:
	CGISView();           // protected constructor used by dynamic creation
	virtual ~CGISView();

public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	GisControlView^ GetControl();

	BEGIN_DELEGATE_MAP(CGISView)
		EVENT_DELEGATE_ENTRY(OnCreateElvObject, System::Object^, GisControlView::ObjectEventArgs^)
		EVENT_DELEGATE_ENTRY(OnCreateStand, System::Object^, GisControlView::StandEventArgs^)
		EVENT_DELEGATE_ENTRY(OnCreateSection, System::Object^, GisControlView::SectionEventArgs^)
		EVENT_DELEGATE_ENTRY(OnRefreshStand, System::Object^, System::EventArgs^)
		EVENT_DELEGATE_ENTRY(OnShowRelationTooltip, System::Object^, GisControlView::RelationTooltipEventArgs^)
		EVENT_DELEGATE_ENTRY(OnViewForestData, System::Object^, GisControlView::ForestEventArgs^)
	END_DELEGATE_MAP()

	void OnCreateElvObject(System::Object ^sender, GisControlView::ObjectEventArgs^ e);
	void OnCreateStand(System::Object ^sender, GisControlView::StandEventArgs^ e);
	void OnCreateSection(System::Object^ sender, GisControlView::SectionEventArgs^ e);
	void OnRefreshStand(System::Object ^sender, System::EventArgs^ e);
	void OnShowRelationTooltip(System::Object ^sender, GisControlView::RelationTooltipEventArgs^ e);
	void OnViewForestData(System::Object^ sender, GisControlView::ForestEventArgs^ e);

	enum { IDD = IDD_GIS };

protected:
	afx_msg LRESULT OnMessageFromSuite(WPARAM wParam, LPARAM lParam);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);

public:
	DECLARE_MESSAGE_MAP()
};
