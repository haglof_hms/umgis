#include "stdafx.h"
#include "defines.h"
#include "GISGenerics.h"
#include "ResLangFileReader.h"
#include "XBalloonMsg.h"

#define ID_SHOWVIEW_MSG							0x80017
#define MSG_IN_SUITE				 			(WM_USER + 10)		// This identifer's used to send messages internally

CString getLangStr(DWORD resid)
{
	CString langFile, string;

	// Obtain specified string from language file
	langFile.Format( _T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT );
	if( fileExists( langFile ) )
	{
		RLFReader *xml = new RLFReader;
		if( xml->Load( langFile ) )
		{
			string = xml->str( resid );
			string.Replace(_T("\\n"), _T("\n")); // Convert \n in string to line-breaks
		}
		delete xml;
	}

	return string;
}

CView *getFormViewByID(int idd)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		if( pTemplate )
		{
			pTemplate->GetDocString(sDocName, CDocTemplate::docName);
			sDocName = '\n' + sDocName;
			if( sDocName.Compare(sResStr) == 0 )
			{
				
				POSITION posDOC = pTemplate->GetFirstDocPosition();
				// Open only one instance of this window; 061002 p�d
				while(posDOC != NULL)
				{
					CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
					POSITION posView = pDocument->GetFirstViewPosition();
					if(posView != NULL)
					{
						CView* pView = pDocument->GetNextView(posView);
						if (pView)
						{
							return pView;
						}
					}	// if(posView != NULL)
				}	// while(posDOC != NULL)
			}
		}
	}
	return NULL;
}

CView *showFormView(int idd, LPCTSTR lang_fn, LPARAM lp)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();
	CView *pActiveView;

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	RLFReader *xml = new RLFReader();
	if (xml->Load(lang_fn))
	{
		sCaption = xml->str(idd);
	}
	delete xml;

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		if( pTemplate )
		{
			pTemplate->GetDocString(sDocName, CDocTemplate::docName);
			sDocName = '\n' + sDocName;
			if( sDocName.Compare(sResStr) == 0 )
			{
				
				POSITION posDOC = pTemplate->GetFirstDocPosition();
				// Open only one instance of this window; 061002 p�d
				while(posDOC != NULL)
				{
					CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
					POSITION posView = pDocument->GetFirstViewPosition();
					if(posView != NULL)
					{
						CView* pView = pDocument->GetNextView(posView);
						pView->GetParent()->BringWindowToTop();
						pView->GetParent()->SetFocus();
						pView->SendMessage(MSG_IN_SUITE,ID_SHOWVIEW_MSG,lp);
						pActiveView = pView;
						posDOC = (POSITION)1;
						break;
					}	// if(posView != NULL)
				}	// while(posDOC != NULL)

				if (posDOC == NULL)
				{

					pTemplate->OpenDocumentFile(NULL);

					// Find the CDocument for this tamplate, and set title.
					// Title is set in Languagefile; OBS! The nTableIndex
					// matches the string id in the languagefile; 051129 p�d
					POSITION posDOC = pTemplate->GetFirstDocPosition();
					while (posDOC != NULL)
					{
						CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
						// Set the caption of the document. Can be a resource string,
						// a string set in the language xml-file etc.
						CString sDocTitle;
						sDocTitle.Format(_T("%s"),sCaption);
						pDocument->SetTitle(sDocTitle);
						POSITION posView = pDocument->GetFirstViewPosition();
						if(posView != NULL)
						{
							CView* pView = pDocument->GetNextView(posView);
							pView->SendMessage(MSG_IN_SUITE,ID_SHOWVIEW_MSG,lp);
							break;
						}	// if(posView != NULL)
					}
					break;
				}
			}
		}
	}

	return pActiveView;
}

bool FileExist(LPCTSTR filename)
{
	bool ret = false;
	WIN32_FIND_DATA findData;

	memset(&findData, 0, sizeof(findData));

	// Search the file
	HANDLE hFind = FindFirstFile( filename, &findData );
	if( hFind != INVALID_HANDLE_VALUE )
	{
		// File found
		ret = true;
	}
	FindClose( hFind );

	return ret;
}

void ShowTooltip(LPCTSTR lpszTitle, LPCTSTR lpszMsg, int x, int y, CWnd* pParent, UINT nTimeout)
{
	RECT rect;
	rect.left = x;
	rect.right = x;
	rect.top = y;
	rect.bottom = y;

	// We need to create a "fake" control that can be used a parent to position the balloon
	CStatic *s = new CStatic;
	s->Create(_T(""), 0, rect, pParent);

	// Show tooltip balloon at specified position
	CXBalloonMsg::Show( lpszTitle,
						lpszMsg,
						s->GetSafeHwnd(),
						pParent->GetSafeHwnd(),
						AfxGetInstanceHandle(),
						TTI_INFO,
						TRUE,
						nTimeout );

	delete s; // Note! This is NOT safe in case CXBalloonMsg::PositionBalloon is called after this point
}
