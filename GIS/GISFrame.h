#pragma once

#include "GISDoc.h"

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
class CGISFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CGISFrame)

public:
	CGISFrame();
	virtual ~CGISFrame();

	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

protected:
	BOOL m_bFirstShow;

	afx_msg LRESULT OnMessageFromSuite(WPARAM wParam, LPARAM lParam);

	afx_msg void OnDestroy();
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	DECLARE_MESSAGE_MAP()
};
