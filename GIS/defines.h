#pragma once

const LPCTSTR PROGRAM_NAME				= _T("UMGIS");	// Name of suite/module, used on setting Language filename; 051214 p�d

const LPCTSTR REG_SUBKEY_PLACEMENT		= _T("UMGIS\\Placement");
const LPCTSTR REG_SUBKEY_VIEWPORT		= _T("UMGIS\\Viewport");

const int STRID_RELATIONTOOLTIPTITLE	= 11000;
const int STRID_RELATIONTOOLTIPDESC		= 11001;
