// GIS.cpp : Defines the initialization routines for the DLL.
//
#include "stdafx.h"
#include "defines.h"
#include "GISDoc.h"
#include "GISView.h"
#include "GISFrame.h"
#include "DBBaseClass_ADODirect.h"
#include "ResLangFileReader.h"

#using <../../bin/TatukGIS_DK10_NET.dll>

using namespace TatukGIS::NDK;


std::vector<HINSTANCE> m_vecHInstTable;
static AFX_EXTENSION_MODULE GISDLL = { NULL, NULL };

// extern "C" prevent name from becoming decorated
extern "C" void __declspec(dllexport) InitSuite(CStringArray *,vecINDEX_TABLE &,vecINFO_TABLE &);

extern "C" void __declspec(dllexport) OpenSuite(int idx,LPCTSTR func,CWnd *,vecINDEX_TABLE &,int *ret);

extern "C" void __declspec(dllexport) DoAlterTables(LPCTSTR dbname);

extern "C" void __declspec(dllexport) DoDatabaseTables(LPCTSTR dbname);

extern "C" void __declspec(dllexport) ConvertToLatLong(const int epsg, const double x, const double y, double *lat, double *lon);

void ConvertToLatLong(const int epsg, const double x, const double y, double *lat, double *lon)
{
	TGIS_CSCoordinateSystem ^cs, ^wgs;
	TGIS_Point pt = TGIS_Point(x, y);

	try
	{
		// Convert coordinates from specified cs to WGS84
		cs  = TGIS_CSFactory::ByEPSG(epsg);
		wgs = TGIS_CSFactory::ByEPSG(4326);
		pt = wgs->FromCS(cs, pt);

		*lat = pt.Y;
		*lon = pt.X;
	}
	catch(EGIS_Exception ^e)
	{
		System::Windows::Forms::MessageBox::Show(e->Message, _T("Error"), System::Windows::Forms::MessageBoxButtons::OK, System::Windows::Forms::MessageBoxIcon::Exclamation);
	}
}

BOOL CreateDbTables()
{
	TCHAR sDB_PATH[127];	TCHAR sUserName[127];	TCHAR sPSW[127];	TCHAR sDSN[127];	TCHAR sLocation[127];	TCHAR sDBName[127];
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;   
	CString S;
	CString csT=_T("");
	int nServerConn=0;
	bool check=false;
			
	try
	{
		if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		{
			// No need to check for username/pwd when using windows authentication
			GetAuthentication(&nServerConn);
			if(nServerConn)
			{
				check = (_tcscmp(sUserName,_T("")) != 0 && _tcscmp(sPSW,_T("")) != 0);
			}
			else
			{
				check = true;
			}

			if (_tcscmp(sLocation,_T("")) != 0 && _tcscmp(sDBName,_T("")) != 0 && check)
			{
				CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
				if (pDB)
				{
					if (pDB->connectToDatabase(nServerConn,sUserName,sPSW,sLocation,sDBName))
					{
						pDB->setDefaultDatabase(sDBName);
						pDB->commandExecute(_T("IF NOT EXISTS(SELECT 1 FROM sysobjects WHERE name LIKE 'gis_snapshots' AND xtype='U')")
											_T("CREATE TABLE [dbo].[gis_snapshots](")
											_T("[id] [int] IDENTITY,")
											_T("[name] [nvarchar](50) NOT NULL,")
											_T("[type] [smallint] NOT NULL,")
											_T("[widgetId] [int] NOT NULL,")
											_T("[img] [image] NOT NULL) ")
											_T("ELSE ")
											_T("IF NOT EXISTS(SELECT 1 FROM syscolumns WHERE name LIKE 'id' AND id = OBJECT_ID('gis_snapshots')) ")
											_T("ALTER TABLE [dbo].[gis_snapshots] ADD [id] [int] IDENTITY"));
					}
					delete pDB;
				}
			}
			
			bReturn = TRUE;
		}
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}

	return bReturn;
}

void __declspec(dllexport) DoAlterTables(LPCTSTR dbname)
{
	CreateDbTables();
}

void __declspec(dllexport) DoDatabaseTables(LPCTSTR dbname)
{
	CreateDbTables();
}

// Exported DLL initialization is run in context of running application
void __declspec(dllexport) InitSuite(CStringArray *user_modules,vecINDEX_TABLE &vecIndex,vecINFO_TABLE &vecInfo)
{
	CString sVersion;
	CString sCopyright;
	CString sCompany;
	// create a new CDynLinkLibrary for this app
	new CDynLinkLibrary(GISDLL);

	CString sModuleFN = getModuleFN(g_hInstance);
	// Setup the language filename
	CString sLangFN;
	sLangFN.Format(_T("%s%s"),getLanguageDir(),PROGRAM_NAME);

	m_vecHInstTable.clear();

	AfxGetApp()->AddDocTemplate(new CMultiDocTemplate(IDD_GIS,
			RUNTIME_CLASS(CGISDoc),
			RUNTIME_CLASS(CGISFrame),
			RUNTIME_CLASS(CGISView)));
	vecIndex.push_back(INDEX_TABLE(IDD_GIS,sModuleFN,sLangFN,TRUE));

	// Get version information; 060803 p�d
	const LPCTSTR VER_NUMBER	= _T("FileVersion");
	const LPCTSTR VER_COMPANY	= _T("CompanyName");
	const LPCTSTR VER_COPYRIGHT	= _T("LegalCopyright");

	sVersion	= getVersionInfo(g_hInstance,VER_NUMBER);
	sCopyright	= getVersionInfo(g_hInstance,VER_COPYRIGHT);
	sCompany	= getVersionInfo(g_hInstance,VER_COMPANY);
	vecInfo.push_back(INFO_TABLE(-999,1 /* Suite */,
									(TCHAR*)sLangFN.GetBuffer(),
									(TCHAR*)sVersion.GetBuffer(),
									(TCHAR*)sCopyright.GetBuffer(),
									(TCHAR*)sCompany.GetBuffer()));

	/* *****************************************************************************
		Load user module(s), specified in the ShellTree data file for this SUITE
	****************************************************************************** */

	typedef CRuntimeClass *(*Func)(CWinApp *,LPCTSTR suite,vecINDEX_TABLE &,vecINFO_TABLE &);
  Func proc;
	// Try to get modules connected to this Suite; 051129 p�d
	if (user_modules->GetCount() > 0)
	{
		for (int i = 0;i < user_modules->GetCount();i++)
		{
			CString sPath;
			sPath.Format(_T("%s%s"),getModulesDir(),user_modules->GetAt(i));
			// Check if the file exists, if not tell USER; 051213 p�d
			if (fileExists(sPath))
			{
				HINSTANCE hInst = AfxLoadLibrary(sPath);
				if (hInst != NULL)
				{
					m_vecHInstTable.push_back(hInst);
#ifdef UNICODE
				USES_CONVERSION;
				proc = (Func)GetProcAddress((HMODULE)m_vecHInstTable[m_vecHInstTable.size() - 1], W2A(INIT_MODULE_FUNC) );
#else
				proc = (Func)GetProcAddress((HMODULE)m_vecHInstTable[m_vecHInstTable.size() - 1], (INIT_MODULE_FUNC) );
#endif
					if (proc != NULL)
					{
						// call the function
						proc(AfxGetApp(),sModuleFN,vecIndex,vecInfo);
					}	// if (proc != NULL)
				}	// if (hInst != NULL)

			}	// if (fileExists(sPath))
			else
			{
				// Set Messages from language file; 051213 p�d
				::MessageBox(0,_T("File doesn't exist\n" + sPath),_T("Error"),MB_OK);
			}
		}	// for (int i = 0;i < m_sarrModules.GetCount();i++)
	}	// if (m_sarrModules.GetCount() > 0)

}

void __declspec(dllexport) OpenSuite(int idx,LPCTSTR func,CWnd *wnd,vecINDEX_TABLE &vecIndex,int *ret)
{
	CDocTemplate *pTemplate;
	CString sDocName;
	CString sResStr;
	CString sModuleFN;
	CString sVecIndexTableModuleFN;
	CString sLangFN;
	CString sCaption;
	CString sDocTitle;
	CString S;
	int nTableIndex;
	int nDocCounter;
	BOOL bFound = FALSE;
	BOOL bIsOneInst;

	ASSERT(AfxGetApp() != NULL);

	// Get path and filename of this SUITE; 051213 p�d
	sModuleFN = getModuleFN(g_hInstance);

	// Find template name for idx value; 051124 p�d
	if (vecIndex.size() == 0)
		return;


	for (UINT i = 0;i < vecIndex.size();i++)
	{
		// Get index of this Window, as set in Doc template
		nTableIndex = vecIndex[i].nTableIndex;
		// Get filename including searchpath to THIS SUITE, as set in
		// the table index vector, for suites and module(s); 051213 p�d
		sVecIndexTableModuleFN = vecIndex[i].szSuite;

		if (nTableIndex == idx && 
				sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
		{
			// Get language filename
			sLangFN = vecIndex[i].szLanguageFN;
			bFound = TRUE;
			bIsOneInst = vecIndex[i].bOneInstance;
			break;
		}	// if (nTableIndex == idx)
	}	// for (UINT i = 0;i < vecIndex.size();i++)
	
	// Make sure the languagefile is in directory; 081215 p�d
	if (!fileExists(sLangFN))
	{
		S.Format(_T("NB! Languagefile missing for suite/module:\n%s\n\nCan not open ..."),
			sVecIndexTableModuleFN);
		AfxMessageBox(S);
		return;
	}

	if (bFound)
	{
		// Get the stringtable resource, matching the TableIndex
		// This string is compared to the title of the document; 051212 p�d
		sResStr.LoadString(nTableIndex);

		RLFReader *xml = new RLFReader();
		if (xml->Load(sLangFN))
		{
			sCaption = xml->str(nTableIndex);
		}
		delete xml;

		// Check if the document or module is in this SUITE; 051213 p�d
		POSITION pos = AfxGetApp()->GetFirstDocTemplatePosition();
		while(pos != NULL)
		{
			pTemplate = AfxGetApp()->GetNextDocTemplate(pos);
			pTemplate->GetDocString(sDocName, CDocTemplate::docName);
			ASSERT(pTemplate != NULL);
			// Need to add a linefeed, infront of the docName.
			// This is because, for some reason, the document title,
			// set in resource, must have a linefeed.
			// OBS! Se documentation for CMultiDocTemplate; 051212 p�d
			sDocName = '\n' + sDocName;

			if (pTemplate && sDocName.Compare(sResStr) == 0)
			{
				
					if (bIsOneInst)
					{
						POSITION posDOC = pTemplate->GetFirstDocPosition();

						while(posDOC != NULL)
						{
							CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
							POSITION posView = pDocument->GetFirstViewPosition();
							if(posView != NULL)
							{
								CView* pView = pDocument->GetNextView(posView);
								pView->GetParent()->BringWindowToTop();
								pView->GetParent()->SetFocus();
								posDOC = (POSITION)1;
								break;
							}	// if(posView != NULL)
						}	// while(posDOC != NULL)

						if (posDOC == NULL)
						{

							pTemplate->OpenDocumentFile(NULL);

							// Find the CDocument for this tamplate, and set title.
							// Title is set in Languagefile; OBS! The nTableIndex
							// matches the string id in the languagefile; 051129 p�d
							POSITION posDOC = pTemplate->GetFirstDocPosition();
							while (posDOC != NULL)
							{
								CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
								// Set the caption of the document. Can be a resource string,
								// a string set in the language xml-file etc.
								sDocTitle.Format(_T("%s"),sCaption);
								pDocument->SetTitle(sDocTitle);
							}

							break;
						}
					}	// if (bIsOneInst)
					else
					{
							pTemplate->OpenDocumentFile(NULL);

							// Find the CDocument for this tamplate, and set title.
							// Title is set in Languagefile; OBS! The nTableIndex
							// matches the string id in the languagefile; 051129 p�d
							POSITION posDOC = pTemplate->GetFirstDocPosition();
							nDocCounter = 1;

							while (posDOC != NULL)
							{
								CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
								// Set the caption of the document. Can be a resource string,
								// a string set in the language xml-file etc.
								sDocTitle.Format(_T("%s (%d)"),sCaption,nDocCounter);
								pDocument->SetTitle(sDocTitle);
								nDocCounter++;
							}

							break;
					}
			}	// if (pTemplate && sDocName.Compare(sResStr) == 0)
		}	// while(pos != NULL)
		*ret = 1;
	}	// if (bFound)
	else
	{
		*ret = 0;
	}

}
