#pragma once

CString getLangStr(DWORD resid);
bool FileExist(LPCTSTR filename);
CView *getFormViewByID(int idd);
CView *showFormView(int idd, LPCTSTR lang_fn, LPARAM lp = -1);
void ShowTooltip(LPCTSTR lpszTitle, LPCTSTR lpszMsg, int x, int y, CWnd* pParent, UINT nTimeout = 10);
