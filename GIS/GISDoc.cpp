#include "stdafx.h"
#include "GISDoc.h"


/////////////////////////////////////////////////////////////////////////////
// CGISDoc

IMPLEMENT_DYNCREATE(CGISDoc, CDocument)

BEGIN_MESSAGE_MAP(CGISDoc, CDocument)
END_MESSAGE_MAP()


CGISDoc::CGISDoc()
{
}

CGISDoc::~CGISDoc()
{
}

BOOL CGISDoc::OnNewDocument()
{
	// Extract filename of this module
	TCHAR szBuf[MAX_PATH], szModule[MAX_PATH];
	GetModuleFileName(g_hInstance, szBuf, sizeof(szBuf) / sizeof(TCHAR));
	_tsplitpath(szBuf, NULL, NULL, szModule, NULL);

	// Check if we have a valid license
	_user_msg msg(820, _T("CheckLicense"), _T("License.dll"), _T("H2204"), _T(""), _T(""), szModule);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4, (LPARAM)&msg);
	if( _tcscmp(msg.getSuite(), _T("0")) == 0 ||
		_tcscmp(msg.getSuite(), _T("License.dll")) == 0 )
	{
		return FALSE;
	}

	return CDocument::OnNewDocument();
}
