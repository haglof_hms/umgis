#include "stdafx.h"
#include "defines.h"
#include "GISFrame.h"

using namespace std;

IMPLEMENT_DYNCREATE(CGISFrame, CChildFrameBase)


BEGIN_MESSAGE_MAP(CGISFrame, CChildFrameBase)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SHOWWINDOW()

	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromSuite)
END_MESSAGE_MAP()


CGISFrame::CGISFrame():
m_bFirstShow(TRUE)
{}

CGISFrame::~CGISFrame()
{}

LRESULT CGISFrame::OnMessageFromSuite(WPARAM wParam, LPARAM lParam)
{
	// Forward message to view
	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(WM_USER_MSG_SUITE, wParam, lParam);
		}
	}

	return 0L;
}

BOOL CGISFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

int CGISFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	// Try-catch in case GisControl.dll is missing
	try
	{
		if(CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		{
			return -1;
		}
	}
	catch( System::Exception ^e )
	{
		CString str(e->ToString());
		AfxMessageBox(str, MB_OK | MB_ICONSTOP);
		PostMessage(WM_CLOSE);
		return -1;
	}

	return 0;
}

void CGISFrame::OnDestroy()
{
	// Save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT, REG_SUBKEY_PLACEMENT);
	SavePlacement(this, csBuf);
	m_bFirstShow = true;

	CMDIChildWnd::OnDestroy();
}

void CGISFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CMDIChildWnd::OnShowWindow(bShow, nStatus);

	// Load window position
    if(bShow && !IsWindowVisible() && m_bFirstShow)
    {
		m_bFirstShow = false; // Important: set this flag before loading window placement!

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT, REG_SUBKEY_PLACEMENT);
		LoadPlacement(this, csBuf);
    }
}
