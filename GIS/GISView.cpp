// SettingsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "defines.h"
#include "GISFrame.h"
#include "GISGenerics.h"
#include "GISView.h"
#include "Resource.h"

using namespace std;

// Codes used for passing messages between GIS and Forrest suite
const int GISMSG_STAND = 2;
const int GISMSG_TCSTAND = 3;
const int GISMSG_SECTION = 4;
const int GISMSG_PROPERTY = 10;
const int GISMSG_OBJECT = 11;


// CGISView

IMPLEMENT_DYNCREATE(CGISView, CWinFormsView)
CGISView::CGISView():
CWinFormsView(GisControlView::typeid)
{
}

CGISView::~CGISView()
{
}


BEGIN_MESSAGE_MAP(CGISView, CWinFormsView)
	ON_WM_SETFOCUS()
	ON_WM_MOUSEWHEEL()

	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromSuite)
END_MESSAGE_MAP()


GisControlView^ CGISView::GetControl()
{
	System::Windows::Forms::Control^ control = CWinFormsView::GetControl();
	return safe_cast<GisControlView^>(control);
}

BOOL CGISView::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	// Check if running Estimate S�dra
	GisControlView ^gis = GetControl();
	typedef BOOL (*funcIsBuildSodra)();
	funcIsBuildSodra IsBuildSodra;
	HMODULE hEstimate = AfxLoadLibrary(_T("Modules\\UMEstimate.dll"));
	if( hEstimate )
	{
		if( IsBuildSodra = (funcIsBuildSodra)GetProcAddress(hEstimate, "export_IsBuildSodra") )
		{
			if( IsBuildSodra() )
			{
				gis->SetRunningEstimateSodra();
			}
		}
	}

	return TRUE;
}

void CGISView::OnInitialUpdate()
{
	CWinFormsView::OnInitialUpdate();

	GisControlView ^gis = GetControl();
	gis->CreateElvObject	+= MAKE_DELEGATE(GisControlView::CreateElvObjectEventHandler, OnCreateElvObject);
	gis->CreateStand		+= MAKE_DELEGATE(GisControlView::CreateStandEventHandler, OnCreateStand);
	gis->CreateSection		+= MAKE_DELEGATE(GisControlView::CreateSectionEventHandler, OnCreateSection);
	gis->RefreshStand		+= MAKE_DELEGATE(GisControlView::RefreshStandEventHandler, OnRefreshStand);
	gis->ShowRelationTooltip+= MAKE_DELEGATE(GisControlView::ShowRelationTooltipEventHandler, OnShowRelationTooltip);
	gis->ViewForestData		+= MAKE_DELEGATE(GisControlView::ViewForestDataEventHandler, OnViewForestData);
}

BOOL CGISView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	POINT ptc = pt;
	ScreenToClient(&ptc);
	GetControl()->ZoomBy(zDelta, ptc.x, ptc.y);
	return CWnd::OnMouseWheel(nFlags, zDelta, pt);
}

LRESULT CGISView::OnMessageFromSuite(WPARAM wParam, LPARAM lParam)
{
	try
	{
		if( wParam == ID_WPARAM_VALUE_FROM + 0x02 )
		{
			// Message from UMEstimate - bring up stand in GIS viewer
			_doc_identifer_msg *msg = (_doc_identifer_msg*)lParam;
			if( msg->getValue1() == GISMSG_STAND )
			{
				GetControl()->ZoomToWidget(msg->getValue2(), GisControl::RelationType::StandRelation, msg->getValue3() ? true : false);
			}
			else if( msg->getValue1() == GISMSG_SECTION )
			{
				GetControl()->ZoomToWidget(msg->getValue2(), GisControl::RelationType::SectionRelation, msg->getValue3() ? true : false);
			}
			else if( msg->getValue1() == GISMSG_TCSTAND )
			{
				GetControl()->ZoomToWidget(msg->getValue2(), GisControl::RelationType::TCPlotRelation, false);
			}
			else if( msg->getValue1() == GISMSG_PROPERTY )
			{
				GetControl()->ZoomToWidget(msg->getValue2(), GisControl::RelationType::PropertyRelation, false);
			}
			else if( msg->getValue1() == GISMSG_OBJECT )
			{
				GetControl()->ZoomToWidget(msg->getValue2(), GisControl::RelationType::ObjectRelation, true);
			}
		}
		else if( wParam == ID_WPARAM_VALUE_FROM + 0x03 )
		{
			// Message from UMLandValue - id of created object returned
			GetControl()->SetUpNewRelation( int(lParam), GisControl::RelationType::ObjectRelation );
		}
		else if( wParam == ID_WPARAM_VALUE_FROM + 0x04 )
		{
			// Message from UMEstimate - id of created stand returned
			GetControl()->SetUpNewRelation( int(lParam), GisControl::RelationType::StandRelation );
		}
		else if( wParam == ID_WPARAM_VALUE_FROM + 0x05 )
		{
#ifdef SODRA_BUILD
			// Message from UMEstimate - ids of created sections returned
			vecTransactionTraktSodraGis *pData = (vecTransactionTraktSodraGis*)lParam;
			for( UINT i = 0; i < pData->size(); i++ )
			{
				if( (*pData)[i].getEstiTraktId() > 0 )
				{
					GetControl()->SetUpNewSectionRelation( (*pData)[i].getEstiTraktId(), (*pData)[i].getTrakt_gis_uid() );
				}
			}
#endif
		}
		else if( wParam == ID_WPARAM_VALUE_FROM + 0x06 )
		{
			// Message from UMEstimate - request to update stand area
			GetControl()->UpdateStandArea( int(lParam) );

			// Set focus back to UMEstimate
			CString langFile = getLanguageFN(getLanguageDir(), _T("UMEstimate"), getLangSet(), LANGUAGE_FN_EXT, DEF_LANGUAGE_ABREV);
			showFormView(5000, langFile);
		}
	}
	catch( System::Exception ^e ) // Catch any unhandled exception
	{
		CString str(e->ToString());
		AfxMessageBox(str, MB_ICONSTOP);
		GetParentFrame()->PostMessage(WM_CLOSE);
	}

	return 0L;
}

void CGISView::OnCreateElvObject(System::Object^ sender, GisControlView::ObjectEventArgs^ e)
{
	CString langFile;
	int idx = 1;

	// Check property count
	if( e->PropertyList->Count == 0 )
	{
		return;
	}

	// Set up property list (first entry represent property count)
	int *pNewProperties = new int[e->PropertyList->Count + 1];
	pNewProperties[0] = e->PropertyList->Count;
	for each(int propId in e->PropertyList)
	{
		pNewProperties[idx++] = propId;
	}

	// Bring up LandValue
	langFile = getLanguageFN(getLanguageDir(), _T("UMLandValue"), getLangSet(), LANGUAGE_FN_EXT, DEF_LANGUAGE_ABREV);
	showFormView(5200, langFile);

	CView *pView = getFormViewByID(5200);
	if( pView )
	{
		pView->GetParent()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x03, (LPARAM)pNewProperties);
	}

	// Release this buffer when execution return to this module
	delete[] pNewProperties;
}

void CGISView::OnCreateStand(System::Object^ sender, GisControlView::StandEventArgs^ e)
{
	float area = float(e->Area);
	int data[3]; data[0] = e->Position.Y; data[1] = e->Position.X; data[2] = *((int*)&area);

	// Bring up Estimate
	CString langFile = getLanguageFN(getLanguageDir(), _T("UMEstimate"), getLangSet(), LANGUAGE_FN_EXT, DEF_LANGUAGE_ABREV);
	showFormView(5000, langFile);

	CView *pView = getFormViewByID(5000);
	if( pView )
	{
		pView->GetParent()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x04, (LPARAM)data);
	}
}

void CGISView::OnCreateSection(System::Object^ sender, GisControlView::SectionEventArgs^ e)
{
#ifdef SODRA_BUILD
	vecTransactionTraktSodraGis vec;

	// Build up a vector with section data and pass it to UMEstimate
	for each(GisControlView::SectionEventArgs::SectionData^ d in e->Data)
	{
		vec.push_back(CTransaction_trakt_sodra_gis( d->Gisuid, d->Id, CString(d->Avdnr), d->Agoslag, d->Malklass, d->Hkl, d->Alder, d->Atgard, d->Atgardof,
													d->Hasof, d->Skifte, d->Medelhojd, d->Volym, d->Fuktklass, d->Standortsi, d->Vegetation, d->Latitud, d->Longitud,
													d->Grundyta, CString(d->Framskrive), -1, d->Dgv, _T("") ));
	}

	// Bring up Estimate
	CString langFile = getLanguageFN(getLanguageDir(), _T("UMEstimate"), getLangSet(), LANGUAGE_FN_EXT, DEF_LANGUAGE_ABREV);
	showFormView(5000, langFile);

	CView *pView = getFormViewByID(5000);
	if( pView )
	{
		pView->GetParent()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x05, (LPARAM)&vec);
	}
#endif
}

void CGISView::OnRefreshStand(System::Object ^sender, System::EventArgs^ e)
{
	// Send message to Estimate only if form is open
	CView *pView = getFormViewByID(5000);
	if( pView )
	{
#ifdef SODRA_BUILD
#else
		pView->GetParent()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x06, NULL);
#endif
	}
}

void CGISView::OnShowRelationTooltip(System::Object^ sender, GisControlView::RelationTooltipEventArgs^ e)
{
	// Show tooltip for relation toolbar
	ShowTooltip( getLangStr(STRID_RELATIONTOOLTIPTITLE), getLangStr(STRID_RELATIONTOOLTIPDESC), e->Position.X, e->Position.Y, GetParent() );
}

void CGISView::OnViewForestData(System::Object^ sender, GisControlView::ForestEventArgs^ e)
{
	CString langFile;

	// Check layer type
	switch( e->TypeOfRelation )
	{
	case GisControl::RelationType::StandRelation:
	case GisControl::RelationType::SectionRelation:
		// Bring up Estimate
		langFile = getLanguageFN(getLanguageDir(), _T("UMEstimate"), getLangSet(), LANGUAGE_FN_EXT, DEF_LANGUAGE_ABREV);
		showFormView(5000, langFile);

		// Send id
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02,
									 (LPARAM)&_doc_identifer_msg(_T("Module888"), _T("View5000"), _T("<saknas>"), GISMSG_STAND, e->Id, 0));
		break;

	case GisControl::RelationType::PropertyRelation:
		// Bring up Forest suite / Property form
		langFile = getLanguageFN(getLanguageDir(), _T("Forrest"), getLangSet(), LANGUAGE_FN_EXT, DEF_LANGUAGE_ABREV);
		showFormView(118, langFile);

		// Send id
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02,
									 (LPARAM)&_doc_identifer_msg(_T("Module888"), _T("Module118"), _T("<saknas>"), GISMSG_PROPERTY, e->Id, 0));
		break;

	case GisControl::RelationType::ObjectRelation:
		// Bring up LandValue
		langFile = getLanguageFN(getLanguageDir(), _T("UMLandValue"), getLangSet(), LANGUAGE_FN_EXT, DEF_LANGUAGE_ABREV);
		showFormView(5200, langFile);

		// Send id
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02,
									 (LPARAM)&_doc_identifer_msg(_T("Module888"), _T("Module5200"), _T("<saknas>"), GISMSG_OBJECT, e->Id, 0));
		break;

	case GisControl::RelationType::TCTractRelation:
		// Bring up TCruise
		langFile = getLanguageFN(getLanguageDir(), _T("TimberCruise"), getLangSet(), LANGUAGE_FN_EXT, DEF_LANGUAGE_ABREV);
		showFormView(920, langFile);

		// Send id
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02,
									(LPARAM)&_doc_identifer_msg(_T("Module888"), _T("Module920"), _T("<saknas>"), GISMSG_TCSTAND, e->Id, 0));
		break;
	}
}

void CGISView::OnSetFocus(CWnd* pOldWnd)
{
	CWinFormsView::OnSetFocus(pOldWnd);

	// Turn off all imagebuttons in the main window
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_LIST, FALSE);
}
