#ifndef _GISDOC_H_
#define _GISDOC_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CGISDoc : public CDocument
{
protected: // create from serialization only
	CGISDoc();
	DECLARE_DYNCREATE(CGISDoc)

public:
	virtual ~CGISDoc();
	virtual BOOL OnNewDocument();

protected:
	DECLARE_MESSAGE_MAP()
};

#endif
