#include "stdafx.h"

HINSTANCE g_hInstance = NULL;
static AFX_EXTENSION_MODULE GISDLL = { NULL, NULL };


#ifdef _MANAGED
#pragma managed(push, off)
#endif

extern "C" int APIENTRY DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(GISDLL, hInstance))
			return 0;

		new CDynLinkLibrary(GISDLL);
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		// Terminate the library before destructors are called
		AfxTermExtensionModule(GISDLL);
	}

	g_hInstance = hInstance;

	return 1;   // ok
}

#ifdef _MANAGED
#pragma managed(pop)
#endif
