﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ImportPropertiesDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ImportPropertiesDialog))
        Me.uxCancel = New System.Windows.Forms.Button
        Me.uxOK = New System.Windows.Forms.Button
        Me.chkHighlight = New System.Windows.Forms.CheckBox
        Me.lblCount = New System.Windows.Forms.Label
        Me.cmbPropertyLayer = New System.Windows.Forms.ComboBox
        Me.lblPropertyLayer = New System.Windows.Forms.Label
        Me.grpAttributes = New System.Windows.Forms.GroupBox
        Me.lblCounty = New System.Windows.Forms.Label
        Me.cmbObjID = New System.Windows.Forms.ComboBox
        Me.cmbCounty = New System.Windows.Forms.ComboBox
        Me.lblObjID = New System.Windows.Forms.Label
        Me.lblMunicipal = New System.Windows.Forms.Label
        Me.cmbMeasuredArea = New System.Windows.Forms.ComboBox
        Me.cmbMunicipal = New System.Windows.Forms.ComboBox
        Me.lblMeasuredArea = New System.Windows.Forms.Label
        Me.lblParish = New System.Windows.Forms.Label
        Me.cmbArea = New System.Windows.Forms.ComboBox
        Me.cmbParish = New System.Windows.Forms.ComboBox
        Me.lblArea = New System.Windows.Forms.Label
        Me.lblPropNumber = New System.Windows.Forms.Label
        Me.cmbUnit = New System.Windows.Forms.ComboBox
        Me.cmbPropNumber = New System.Windows.Forms.ComboBox
        Me.lblUnit = New System.Windows.Forms.Label
        Me.lblPropName = New System.Windows.Forms.Label
        Me.cmbBlock = New System.Windows.Forms.ComboBox
        Me.cmbPropName = New System.Windows.Forms.ComboBox
        Me.lblBlock = New System.Windows.Forms.Label
        Me.lblProperties = New System.Windows.Forms.Label
        Me.uxShapeList = New AxXtremeReportControl.AxReportControl
        Me.grpAttributes.SuspendLayout()
        CType(Me.uxShapeList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'uxCancel
        '
        Me.uxCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.uxCancel.Location = New System.Drawing.Point(275, 588)
        Me.uxCancel.Name = "uxCancel"
        Me.uxCancel.Size = New System.Drawing.Size(67, 23)
        Me.uxCancel.TabIndex = 1
        Me.uxCancel.Text = "uxCancel"
        '
        'uxOK
        '
        Me.uxOK.Location = New System.Drawing.Point(202, 588)
        Me.uxOK.Name = "uxOK"
        Me.uxOK.Size = New System.Drawing.Size(67, 23)
        Me.uxOK.TabIndex = 0
        Me.uxOK.Text = "uxOK"
        '
        'chkHighlight
        '
        Me.chkHighlight.AutoSize = True
        Me.chkHighlight.Location = New System.Drawing.Point(12, 400)
        Me.chkHighlight.Name = "chkHighlight"
        Me.chkHighlight.Size = New System.Drawing.Size(85, 17)
        Me.chkHighlight.TabIndex = 3
        Me.chkHighlight.Text = "chkHighlight"
        Me.chkHighlight.UseVisualStyleBackColor = True
        '
        'lblCount
        '
        Me.lblCount.Location = New System.Drawing.Point(410, 397)
        Me.lblCount.Name = "lblCount"
        Me.lblCount.Size = New System.Drawing.Size(125, 13)
        Me.lblCount.TabIndex = 4
        Me.lblCount.Text = "lblCount"
        Me.lblCount.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cmbPropertyLayer
        '
        Me.cmbPropertyLayer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPropertyLayer.FormattingEnabled = True
        Me.cmbPropertyLayer.Location = New System.Drawing.Point(11, 25)
        Me.cmbPropertyLayer.Name = "cmbPropertyLayer"
        Me.cmbPropertyLayer.Size = New System.Drawing.Size(243, 21)
        Me.cmbPropertyLayer.TabIndex = 5
        '
        'lblPropertyLayer
        '
        Me.lblPropertyLayer.AutoSize = True
        Me.lblPropertyLayer.Location = New System.Drawing.Point(12, 9)
        Me.lblPropertyLayer.Name = "lblPropertyLayer"
        Me.lblPropertyLayer.Size = New System.Drawing.Size(82, 13)
        Me.lblPropertyLayer.TabIndex = 6
        Me.lblPropertyLayer.Text = "lblPropertyLayer"
        '
        'grpAttributes
        '
        Me.grpAttributes.Controls.Add(Me.lblCounty)
        Me.grpAttributes.Controls.Add(Me.cmbObjID)
        Me.grpAttributes.Controls.Add(Me.cmbCounty)
        Me.grpAttributes.Controls.Add(Me.lblObjID)
        Me.grpAttributes.Controls.Add(Me.lblMunicipal)
        Me.grpAttributes.Controls.Add(Me.cmbMeasuredArea)
        Me.grpAttributes.Controls.Add(Me.cmbMunicipal)
        Me.grpAttributes.Controls.Add(Me.lblMeasuredArea)
        Me.grpAttributes.Controls.Add(Me.lblParish)
        Me.grpAttributes.Controls.Add(Me.cmbArea)
        Me.grpAttributes.Controls.Add(Me.cmbParish)
        Me.grpAttributes.Controls.Add(Me.lblArea)
        Me.grpAttributes.Controls.Add(Me.lblPropNumber)
        Me.grpAttributes.Controls.Add(Me.cmbUnit)
        Me.grpAttributes.Controls.Add(Me.cmbPropNumber)
        Me.grpAttributes.Controls.Add(Me.lblUnit)
        Me.grpAttributes.Controls.Add(Me.lblPropName)
        Me.grpAttributes.Controls.Add(Me.cmbBlock)
        Me.grpAttributes.Controls.Add(Me.cmbPropName)
        Me.grpAttributes.Controls.Add(Me.lblBlock)
        Me.grpAttributes.Location = New System.Drawing.Point(11, 435)
        Me.grpAttributes.Name = "grpAttributes"
        Me.grpAttributes.Size = New System.Drawing.Size(523, 147)
        Me.grpAttributes.TabIndex = 36
        Me.grpAttributes.TabStop = False
        Me.grpAttributes.Text = "grpAttributes"
        '
        'lblCounty
        '
        Me.lblCounty.AutoSize = True
        Me.lblCounty.Enabled = False
        Me.lblCounty.Location = New System.Drawing.Point(5, 27)
        Me.lblCounty.Name = "lblCounty"
        Me.lblCounty.Size = New System.Drawing.Size(50, 13)
        Me.lblCounty.TabIndex = 15
        Me.lblCounty.Text = "lblCounty"
        '
        'cmbObjID
        '
        Me.cmbObjID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbObjID.FormattingEnabled = True
        Me.cmbObjID.Location = New System.Drawing.Point(359, 115)
        Me.cmbObjID.Name = "cmbObjID"
        Me.cmbObjID.Size = New System.Drawing.Size(150, 21)
        Me.cmbObjID.TabIndex = 19
        '
        'cmbCounty
        '
        Me.cmbCounty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCounty.FormattingEnabled = True
        Me.cmbCounty.Location = New System.Drawing.Point(93, 19)
        Me.cmbCounty.Name = "cmbCounty"
        Me.cmbCounty.Size = New System.Drawing.Size(150, 21)
        Me.cmbCounty.TabIndex = 10
        '
        'lblObjID
        '
        Me.lblObjID.AutoSize = True
        Me.lblObjID.Enabled = False
        Me.lblObjID.Location = New System.Drawing.Point(271, 123)
        Me.lblObjID.Name = "lblObjID"
        Me.lblObjID.Size = New System.Drawing.Size(44, 13)
        Me.lblObjID.TabIndex = 33
        Me.lblObjID.Text = "lblObjID"
        '
        'lblMunicipal
        '
        Me.lblMunicipal.AutoSize = True
        Me.lblMunicipal.Enabled = False
        Me.lblMunicipal.Location = New System.Drawing.Point(5, 51)
        Me.lblMunicipal.Name = "lblMunicipal"
        Me.lblMunicipal.Size = New System.Drawing.Size(62, 13)
        Me.lblMunicipal.TabIndex = 17
        Me.lblMunicipal.Text = "lblMunicipal"
        '
        'cmbMeasuredArea
        '
        Me.cmbMeasuredArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMeasuredArea.FormattingEnabled = True
        Me.cmbMeasuredArea.Location = New System.Drawing.Point(359, 91)
        Me.cmbMeasuredArea.Name = "cmbMeasuredArea"
        Me.cmbMeasuredArea.Size = New System.Drawing.Size(150, 21)
        Me.cmbMeasuredArea.TabIndex = 18
        '
        'cmbMunicipal
        '
        Me.cmbMunicipal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMunicipal.FormattingEnabled = True
        Me.cmbMunicipal.Location = New System.Drawing.Point(93, 43)
        Me.cmbMunicipal.Name = "cmbMunicipal"
        Me.cmbMunicipal.Size = New System.Drawing.Size(150, 21)
        Me.cmbMunicipal.TabIndex = 11
        '
        'lblMeasuredArea
        '
        Me.lblMeasuredArea.AutoSize = True
        Me.lblMeasuredArea.Enabled = False
        Me.lblMeasuredArea.Location = New System.Drawing.Point(271, 99)
        Me.lblMeasuredArea.Name = "lblMeasuredArea"
        Me.lblMeasuredArea.Size = New System.Drawing.Size(86, 13)
        Me.lblMeasuredArea.TabIndex = 31
        Me.lblMeasuredArea.Text = "lblMeasuredArea"
        '
        'lblParish
        '
        Me.lblParish.AutoSize = True
        Me.lblParish.Enabled = False
        Me.lblParish.Location = New System.Drawing.Point(5, 75)
        Me.lblParish.Name = "lblParish"
        Me.lblParish.Size = New System.Drawing.Size(46, 13)
        Me.lblParish.TabIndex = 19
        Me.lblParish.Text = "lblParish"
        '
        'cmbArea
        '
        Me.cmbArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbArea.FormattingEnabled = True
        Me.cmbArea.Location = New System.Drawing.Point(359, 67)
        Me.cmbArea.Name = "cmbArea"
        Me.cmbArea.Size = New System.Drawing.Size(150, 21)
        Me.cmbArea.TabIndex = 17
        '
        'cmbParish
        '
        Me.cmbParish.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbParish.FormattingEnabled = True
        Me.cmbParish.Location = New System.Drawing.Point(93, 67)
        Me.cmbParish.Name = "cmbParish"
        Me.cmbParish.Size = New System.Drawing.Size(150, 21)
        Me.cmbParish.TabIndex = 12
        '
        'lblArea
        '
        Me.lblArea.AutoSize = True
        Me.lblArea.Enabled = False
        Me.lblArea.Location = New System.Drawing.Point(271, 75)
        Me.lblArea.Name = "lblArea"
        Me.lblArea.Size = New System.Drawing.Size(39, 13)
        Me.lblArea.TabIndex = 29
        Me.lblArea.Text = "lblArea"
        '
        'lblPropNumber
        '
        Me.lblPropNumber.AutoSize = True
        Me.lblPropNumber.Enabled = False
        Me.lblPropNumber.Location = New System.Drawing.Point(5, 99)
        Me.lblPropNumber.Name = "lblPropNumber"
        Me.lblPropNumber.Size = New System.Drawing.Size(76, 13)
        Me.lblPropNumber.TabIndex = 21
        Me.lblPropNumber.Text = "lblPropNumber"
        '
        'cmbUnit
        '
        Me.cmbUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbUnit.FormattingEnabled = True
        Me.cmbUnit.Location = New System.Drawing.Point(359, 43)
        Me.cmbUnit.Name = "cmbUnit"
        Me.cmbUnit.Size = New System.Drawing.Size(150, 21)
        Me.cmbUnit.TabIndex = 16
        '
        'cmbPropNumber
        '
        Me.cmbPropNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPropNumber.FormattingEnabled = True
        Me.cmbPropNumber.Location = New System.Drawing.Point(93, 91)
        Me.cmbPropNumber.Name = "cmbPropNumber"
        Me.cmbPropNumber.Size = New System.Drawing.Size(150, 21)
        Me.cmbPropNumber.TabIndex = 13
        '
        'lblUnit
        '
        Me.lblUnit.AutoSize = True
        Me.lblUnit.Enabled = False
        Me.lblUnit.Location = New System.Drawing.Point(271, 51)
        Me.lblUnit.Name = "lblUnit"
        Me.lblUnit.Size = New System.Drawing.Size(36, 13)
        Me.lblUnit.TabIndex = 27
        Me.lblUnit.Text = "lblUnit"
        '
        'lblPropName
        '
        Me.lblPropName.AutoSize = True
        Me.lblPropName.Enabled = False
        Me.lblPropName.Location = New System.Drawing.Point(5, 123)
        Me.lblPropName.Name = "lblPropName"
        Me.lblPropName.Size = New System.Drawing.Size(67, 13)
        Me.lblPropName.TabIndex = 23
        Me.lblPropName.Text = "lblPropName"
        '
        'cmbBlock
        '
        Me.cmbBlock.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBlock.FormattingEnabled = True
        Me.cmbBlock.Location = New System.Drawing.Point(359, 19)
        Me.cmbBlock.Name = "cmbBlock"
        Me.cmbBlock.Size = New System.Drawing.Size(150, 21)
        Me.cmbBlock.TabIndex = 15
        '
        'cmbPropName
        '
        Me.cmbPropName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPropName.FormattingEnabled = True
        Me.cmbPropName.Location = New System.Drawing.Point(93, 115)
        Me.cmbPropName.Name = "cmbPropName"
        Me.cmbPropName.Size = New System.Drawing.Size(150, 21)
        Me.cmbPropName.TabIndex = 14
        '
        'lblBlock
        '
        Me.lblBlock.AutoSize = True
        Me.lblBlock.Enabled = False
        Me.lblBlock.Location = New System.Drawing.Point(271, 27)
        Me.lblBlock.Name = "lblBlock"
        Me.lblBlock.Size = New System.Drawing.Size(44, 13)
        Me.lblBlock.TabIndex = 25
        Me.lblBlock.Text = "lblBlock"
        '
        'lblProperties
        '
        Me.lblProperties.AutoSize = True
        Me.lblProperties.Location = New System.Drawing.Point(12, 60)
        Me.lblProperties.Name = "lblProperties"
        Me.lblProperties.Size = New System.Drawing.Size(64, 13)
        Me.lblProperties.TabIndex = 37
        Me.lblProperties.Text = "lblProperties"
        '
        'uxShapeList
        '
        Me.uxShapeList.Location = New System.Drawing.Point(12, 76)
        Me.uxShapeList.Name = "uxShapeList"
        Me.uxShapeList.OcxState = CType(resources.GetObject("uxShapeList.OcxState"), System.Windows.Forms.AxHost.State)
        Me.uxShapeList.Size = New System.Drawing.Size(523, 318)
        Me.uxShapeList.TabIndex = 38
        '
        'ImportPropertiesDialog
        '
        Me.AcceptButton = Me.uxOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.uxCancel
        Me.ClientSize = New System.Drawing.Size(545, 618)
        Me.Controls.Add(Me.uxShapeList)
        Me.Controls.Add(Me.cmbPropertyLayer)
        Me.Controls.Add(Me.lblProperties)
        Me.Controls.Add(Me.grpAttributes)
        Me.Controls.Add(Me.lblPropertyLayer)
        Me.Controls.Add(Me.lblCount)
        Me.Controls.Add(Me.chkHighlight)
        Me.Controls.Add(Me.uxCancel)
        Me.Controls.Add(Me.uxOK)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ImportPropertiesDialog"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "ImportPropertiesDialog"
        Me.grpAttributes.ResumeLayout(False)
        Me.grpAttributes.PerformLayout()
        CType(Me.uxShapeList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents uxCancel As System.Windows.Forms.Button
    Friend WithEvents uxOK As System.Windows.Forms.Button
    Friend WithEvents chkHighlight As System.Windows.Forms.CheckBox
    Friend WithEvents lblCount As System.Windows.Forms.Label
    Friend WithEvents cmbPropertyLayer As System.Windows.Forms.ComboBox
    Friend WithEvents lblPropertyLayer As System.Windows.Forms.Label
    Friend WithEvents grpAttributes As System.Windows.Forms.GroupBox
    Friend WithEvents lblCounty As System.Windows.Forms.Label
    Friend WithEvents cmbObjID As System.Windows.Forms.ComboBox
    Friend WithEvents cmbCounty As System.Windows.Forms.ComboBox
    Friend WithEvents lblObjID As System.Windows.Forms.Label
    Friend WithEvents lblMunicipal As System.Windows.Forms.Label
    Friend WithEvents cmbMeasuredArea As System.Windows.Forms.ComboBox
    Friend WithEvents cmbMunicipal As System.Windows.Forms.ComboBox
    Friend WithEvents lblMeasuredArea As System.Windows.Forms.Label
    Friend WithEvents lblParish As System.Windows.Forms.Label
    Friend WithEvents cmbArea As System.Windows.Forms.ComboBox
    Friend WithEvents cmbParish As System.Windows.Forms.ComboBox
    Friend WithEvents lblArea As System.Windows.Forms.Label
    Friend WithEvents lblPropNumber As System.Windows.Forms.Label
    Friend WithEvents cmbUnit As System.Windows.Forms.ComboBox
    Friend WithEvents cmbPropNumber As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnit As System.Windows.Forms.Label
    Friend WithEvents lblPropName As System.Windows.Forms.Label
    Friend WithEvents cmbBlock As System.Windows.Forms.ComboBox
    Friend WithEvents cmbPropName As System.Windows.Forms.ComboBox
    Friend WithEvents lblBlock As System.Windows.Forms.Label
    Friend WithEvents lblProperties As System.Windows.Forms.Label
    Friend WithEvents uxShapeList As AxXtremeReportControl.AxReportControl

End Class
