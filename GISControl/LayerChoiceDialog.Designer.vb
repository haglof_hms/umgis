﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LayerChoiceDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.uxCancel = New System.Windows.Forms.Button
        Me.uxOK = New System.Windows.Forms.Button
        Me.uxLayerOne = New System.Windows.Forms.ComboBox
        Me.uxLayerTwo = New System.Windows.Forms.ComboBox
        Me.uxLayerOneLabel = New System.Windows.Forms.Label
        Me.uxLayerTwoLabel = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'uxCancel
        '
        Me.uxCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.uxCancel.Location = New System.Drawing.Point(128, 101)
        Me.uxCancel.Name = "uxCancel"
        Me.uxCancel.Size = New System.Drawing.Size(75, 23)
        Me.uxCancel.TabIndex = 3
        Me.uxCancel.Text = "uxCancel"
        Me.uxCancel.UseVisualStyleBackColor = True
        '
        'uxOK
        '
        Me.uxOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.uxOK.Location = New System.Drawing.Point(47, 101)
        Me.uxOK.Name = "uxOK"
        Me.uxOK.Size = New System.Drawing.Size(75, 23)
        Me.uxOK.TabIndex = 2
        Me.uxOK.Text = "uxOK"
        Me.uxOK.UseVisualStyleBackColor = True
        '
        'uxLayerOne
        '
        Me.uxLayerOne.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.uxLayerOne.FormattingEnabled = True
        Me.uxLayerOne.Location = New System.Drawing.Point(12, 25)
        Me.uxLayerOne.Name = "uxLayerOne"
        Me.uxLayerOne.Size = New System.Drawing.Size(228, 21)
        Me.uxLayerOne.TabIndex = 0
        '
        'uxLayerTwo
        '
        Me.uxLayerTwo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.uxLayerTwo.FormattingEnabled = True
        Me.uxLayerTwo.Location = New System.Drawing.Point(12, 69)
        Me.uxLayerTwo.Name = "uxLayerTwo"
        Me.uxLayerTwo.Size = New System.Drawing.Size(228, 21)
        Me.uxLayerTwo.TabIndex = 1
        '
        'uxLayerOneLabel
        '
        Me.uxLayerOneLabel.AutoSize = True
        Me.uxLayerOneLabel.Location = New System.Drawing.Point(9, 9)
        Me.uxLayerOneLabel.Name = "uxLayerOneLabel"
        Me.uxLayerOneLabel.Size = New System.Drawing.Size(90, 13)
        Me.uxLayerOneLabel.TabIndex = 6
        Me.uxLayerOneLabel.Text = "uxLayerOneLabel"
        '
        'uxLayerTwoLabel
        '
        Me.uxLayerTwoLabel.AutoSize = True
        Me.uxLayerTwoLabel.Location = New System.Drawing.Point(9, 53)
        Me.uxLayerTwoLabel.Name = "uxLayerTwoLabel"
        Me.uxLayerTwoLabel.Size = New System.Drawing.Size(91, 13)
        Me.uxLayerTwoLabel.TabIndex = 7
        Me.uxLayerTwoLabel.Text = "uxLayerTwoLabel"
        '
        'LayerChoiceDialog
        '
        Me.AcceptButton = Me.uxOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.uxCancel
        Me.ClientSize = New System.Drawing.Size(251, 135)
        Me.Controls.Add(Me.uxLayerTwo)
        Me.Controls.Add(Me.uxLayerOne)
        Me.Controls.Add(Me.uxLayerTwoLabel)
        Me.Controls.Add(Me.uxLayerOneLabel)
        Me.Controls.Add(Me.uxCancel)
        Me.Controls.Add(Me.uxOK)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "LayerChoiceDialog"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "LayerChoiceDialog"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents uxCancel As System.Windows.Forms.Button
    Friend WithEvents uxOK As System.Windows.Forms.Button
    Friend WithEvents uxLayerOne As System.Windows.Forms.ComboBox
    Friend WithEvents uxLayerTwo As System.Windows.Forms.ComboBox
    Friend WithEvents uxLayerOneLabel As System.Windows.Forms.Label
    Friend WithEvents uxLayerTwoLabel As System.Windows.Forms.Label
End Class
