﻿Imports System.Data.OleDb
Imports System.Drawing
Imports System.Globalization
Imports TatukGIS.NDK

Public Class LayerPropertiesDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 400
    Private Const _strIdOK As Integer = 401
    Private Const _strIdCancel As Integer = 402
    Private Const _strIdMissingTitle As Integer = 403
    Private Const _strIdTabGeneral As Integer = 404
    Private Const _strIdTabRendering As Integer = 405
    Private Const _strIdTabArea As Integer = 406
    Private Const _strIdTabLine As Integer = 407
    Private Const _strIdTabMarker As Integer = 408
    Private Const _strIdTabLabel As Integer = 409
    Private Const _strIdTabChart As Integer = 410
    Private Const _strIdTitle As Integer = 411
    Private Const _strIdPath As Integer = 412
    Private Const _strIdPathDB As Integer = 413
    Private Const _strIdRelation As Integer = 414
    Private Const _strIdForestStand As Integer = 415
    Private Const _strIdForestProperty As Integer = 416
    Private Const _strIdForestObject As Integer = 417
    Private Const _strIdTcStand As Integer = 418
    Private Const _strIdRemoveRelation As Integer = 419
    Private Const _strIdConfirmRemoveRelation As Integer = 420
    Private Const _strIdRestructure As Integer = 421
    Private Const _strIdTransparency As Integer = 422
    Private Const _strIdRenderField As Integer = 423
    Private Const _strIdRenderZones As Integer = 424
    Private Const _strIdRenderMinValue As Integer = 425
    Private Const _strIdRenderMaxValue As Integer = 426
    Private Const _strIdRenderStartColor As Integer = 427
    Private Const _strIdRenderEndColor As Integer = 428
    Private Const _strIdRenderDefaultColor As Integer = 429
    Private Const _strIdRenderStartSize As Integer = 430
    Private Const _strIdRenderEndSize As Integer = 431
    Private Const _strIdRenderDefaultSize As Integer = 432
    Private Const _strIdPattern As Integer = 433
    Private Const _strIdSymbol As Integer = 434
    Private Const _strIdColor As Integer = 435
    Private Const _strIdWidth As Integer = 436
    Private Const _strIdSize As Integer = 437
    Private Const _strIdOutlineStyle As Integer = 438
    Private Const _strIdOutlineColor As Integer = 439
    Private Const _strIdOutlineWidth As Integer = 440
    Private Const _strIdUseRenderer As Integer = 441
    Private Const _strIdShowInLegend As Integer = 442
    Private Const _strIdLabelColor As Integer = 444
    Private Const _strIdLabelFont As Integer = 445
    Private Const _strIdLabelField As Integer = 446
    Private Const _strIdLabelValue As Integer = 447
    Private Const _strIdLabelVisible As Integer = 448
    Private Const _strIdChartStyle As Integer = 449
    Private Const _strIdChartSize As Integer = 450
    Private Const _strIdChartFields As Integer = 451
    Private Const _strIdMoreColors As Integer = 452
    Private Const _strIdPatternSolid As Integer = 453
    Private Const _strIdPatternTransparent As Integer = 454
    Private Const _strIdPatternHorizontal As Integer = 455
    Private Const _strIdPatternVertical As Integer = 456
    Private Const _strIdPatternFdiagonal As Integer = 457
    Private Const _strIdPatternBdiagonal As Integer = 458
    Private Const _strIdPatternCross As Integer = 459
    Private Const _strIdPatternDiagCross As Integer = 460
    Private Const _strIdStyleSolid As Integer = 461
    Private Const _strIdStyleDash As Integer = 462
    Private Const _strIdStyleDot As Integer = 463
    Private Const _strIdStyleDashDot As Integer = 464
    Private Const _strIdStyleDashDotDot As Integer = 465
    Private Const _strIdStyleClear As Integer = 466
    Private Const _strIdBox As Integer = 467
    Private Const _strIdCircle As Integer = 468
    Private Const _strIdCross As Integer = 469
    Private Const _strIdDiagCross As Integer = 470
    Private Const _strIdTriangleUp As Integer = 471
    Private Const _strIdTriangleDown As Integer = 472
    Private Const _strIdTriangleLeft As Integer = 473
    Private Const _strIdTriangleRight As Integer = 474
    Private Const _strIdChartPie As Integer = 475
    Private Const _strIdChartBar As Integer = 476
    Private Const _strIdProjection As Integer = 477
    Private Const _strIdProjectionSelect As Integer = 478
    Private Const _strIdView As Integer = 479
    Private Const _strIdTcPlot As Integer = 480
    Private Const _strIdTabRenderingFirst As Integer = 483
    Private Const _strIdTabRenderingSecond As Integer = 484
    Private Const _strIdSectionVisible As Integer = 485
    Private Const _strIdSectionHidden As Integer = 486
    Private Const _strIdSectionQuery As Integer = 487
    Private Const _strIdApply As Integer = 488
    Private Const _strIdWizard As Integer = 489
    Private Const _strIdTabSection As Integer = 490
    Private Const _strIdSectionMinScale As Integer = 491
    Private Const _strIdSectionMaxScale As Integer = 492
    Private Const _strIdSectionCurrentScale As Integer = 493
    Private Const _strIdSectionQueryParamsList As Integer = 494
    Private Const _strIdSectionLegend As Integer = 495
    Private Const _strIdSectionScale As Integer = 496
    Private Const _strIdSectionRenderer As Integer = 497
    Private Const _strIdLabelRotation As Integer = 498
    Private Const _strIdMarker As Integer = 499
    Private Const _strIdForestSampleTrees As Integer = 500
    Private Const _strIdLabelPosition As Integer = 501
    Private Const _strIdAlphaChannel As Integer = 502
    Private Const _strIdAlphaChannelNone As Integer = 503
    Private Const _strIdAlphaChannelDefault As Integer = 504
    Private Const _strIdAlphaChannelRed As Integer = 505
    Private Const _strIdAlphaChannelGreen As Integer = 506
    Private Const _strIdAlphaChannelBlue As Integer = 507
    Private Const _strIdForestSection As Integer = 508
    Private Const _strIdRemoveAllSections As Integer = 509
    Private Const _strIdRemoveAllButFirstSection As Integer = 510
    Private Const _strIdLabelInfo As Integer = 511
#End Region

#Region "Declarations"
    Private _ll As TGIS_LayerAbstract
    Private _lv As TGIS_LayerVector
    Private _grp As String
    Private _gisCtrl As TatukGIS.NDK.WinForms.TGIS_ViewerWnd
    Private _type As RelationType = RelationType.Undefined
    Private _joinView As String
    Private _noSave As Boolean
    Private _infoIcon As Icon

    Private Const _maxScale As Double = 1.7E+308

    Public Sub New(ByVal ll As TGIS_LayerAbstract, ByVal grp As String)
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _ll = ll
        _lv = TryCast(ll, TGIS_LayerVector)
        _grp = grp
        _gisCtrl = GisControlView.Instance.GisCtrl 'Shorthand
    End Sub
#End Region

#Region "Helper functions"
    Private Sub SetupLanguage()
        Dim c As Control, t As Control
        Dim tt As New ToolTip

        tt.SetToolTip(lblLabelInfo, GisControlView.LangStr(_strIdLabelInfo))

        'Set up language dependent strings
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        btnOK.Text = GisControlView.LangStr(_strIdOK)
        btnApply.Text = GisControlView.LangStr(_strIdApply)
        btnCancel.Text = GisControlView.LangStr(_strIdCancel)
        btnWizard.Text = GisControlView.LangStr(_strIdWizard)
        RemoveAllToolStripMenuItemToolStripMenuItem.Text = GisControlView.LangStr(_strIdRemoveAllSections)
        RemoveAllButFirstToolStripMenuItemToolStripMenuItem.Text = GisControlView.LangStr(_strIdRemoveAllButFirstSection)
        'TabGeneral
        TabGeneral.Text = GisControlView.LangStr(_strIdTabGeneral)
        lblCaption.Text = GisControlView.LangStr(_strIdTitle)
        lblPath.Text = GisControlView.LangStr(_strIdPath)
        lblRelation.Text = GisControlView.LangStr(_strIdRelation)
        lblView.Text = GisControlView.LangStr(_strIdView)
        lblProjection.Text = GisControlView.LangStr(_strIdProjection)
        btnProjection.Text = GisControlView.LangStr(_strIdProjectionSelect)
        btnRestructure.Text = GisControlView.LangStr(_strIdRestructure)
        lblTransparency.Text = GisControlView.LangStr(_strIdTransparency)
        lblAlphaChannel.Text = GisControlView.LangStr(_strIdAlphaChannel)
        'TabSection
        TabSection.Text = GisControlView.LangStr(_strIdTabSection)
        chkSectionVisible.Text = GisControlView.LangStr(_strIdSectionVisible)
        lblSectionMinScale.Text = GisControlView.LangStr(_strIdSectionMinScale)
        lblSectionMaxScale.Text = GisControlView.LangStr(_strIdSectionMaxScale)
        btnSectionCurrentScaleMin.Text = GisControlView.LangStr(_strIdSectionCurrentScale)
        btnSectionCurrentScaleMax.Text = GisControlView.LangStr(_strIdSectionCurrentScale)
        lblSectionQuery.Text = GisControlView.LangStr(_strIdSectionQuery)
        lblSectionLegend.Text = GisControlView.LangStr(_strIdSectionLegend)
        'TabRendering
        TabRendering.Text = GisControlView.LangStr(_strIdTabRendering)
        TabFirst.Text = GisControlView.LangStr(_strIdTabRenderingFirst)
        TabSecond.Text = GisControlView.LangStr(_strIdTabRenderingSecond)
        lblField.Text = GisControlView.LangStr(_strIdRenderField)
        lblZones.Text = GisControlView.LangStr(_strIdRenderZones)
        lblZonesEx.Text = GisControlView.LangStr(_strIdRenderZones)
        lblMinValue.Text = GisControlView.LangStr(_strIdRenderMinValue)
        lblMinValueEx.Text = GisControlView.LangStr(_strIdRenderMinValue)
        lblMaxValue.Text = GisControlView.LangStr(_strIdRenderMaxValue)
        lblMaxValueEx.Text = GisControlView.LangStr(_strIdRenderMaxValue)
        lblStartColor.Text = GisControlView.LangStr(_strIdRenderStartColor)
        lblStartColorEx.Text = GisControlView.LangStr(_strIdRenderStartColor)
        lblEndColor.Text = GisControlView.LangStr(_strIdRenderEndColor)
        lblEndColorEx.Text = GisControlView.LangStr(_strIdRenderEndColor)
        lblDefaultColor.Text = GisControlView.LangStr(_strIdRenderDefaultColor)
        lblDefaultColorEx.Text = GisControlView.LangStr(_strIdRenderDefaultColor)
        lblStartSize.Text = GisControlView.LangStr(_strIdRenderStartSize)
        lblStartSizeEx.Text = GisControlView.LangStr(_strIdRenderStartSize)
        lblEndSize.Text = GisControlView.LangStr(_strIdRenderEndSize)
        lblEndSizeEx.Text = GisControlView.LangStr(_strIdRenderEndSize)
        lblDefaultSize.Text = GisControlView.LangStr(_strIdRenderDefaultSize)
        lblDefaultSizeEx.Text = GisControlView.LangStr(_strIdRenderDefaultSize)
        'TabArea
        TabArea.Text = GisControlView.LangStr(_strIdTabArea)
        lblAreaPattern.Text = GisControlView.LangStr(_strIdPattern)
        lblAreaColor.Text = GisControlView.LangStr(_strIdColor)
        chkAreaColorUR.Text = GisControlView.LangStr(_strIdUseRenderer)
        lblAreaOutlineStyle.Text = GisControlView.LangStr(_strIdOutlineStyle)
        lblAreaOutlineColor.Text = GisControlView.LangStr(_strIdOutlineColor)
        lblAreaOutlineWidth.Text = GisControlView.LangStr(_strIdOutlineWidth)
        chkAreaOutlineColorUR.Text = GisControlView.LangStr(_strIdUseRenderer)
        chkAreaOutlineWidthUR.Text = GisControlView.LangStr(_strIdUseRenderer)
        chkAreaInLegend.Text = GisControlView.LangStr(_strIdShowInLegend)
        'TabLine
        TabLine.Text = GisControlView.LangStr(_strIdTabLine)
        lblLineStyle.Text = GisControlView.LangStr(_strIdPattern)
        lblLineColor.Text = GisControlView.LangStr(_strIdColor)
        lblLineWidth.Text = GisControlView.LangStr(_strIdWidth)
        chkLineColorUR.Text = GisControlView.LangStr(_strIdUseRenderer)
        chkLineWidthUR.Text = GisControlView.LangStr(_strIdUseRenderer)
        lblLineOutlineStyle.Text = GisControlView.LangStr(_strIdOutlineStyle)
        lblLineOutlineColor.Text = GisControlView.LangStr(_strIdOutlineColor)
        lblLineOutlineWidth.Text = GisControlView.LangStr(_strIdOutlineWidth)
        chkLineOutlineColorUR.Text = GisControlView.LangStr(_strIdUseRenderer)
        chkLineOutlineWidthUR.Text = GisControlView.LangStr(_strIdUseRenderer)
        chkLineInLegend.Text = GisControlView.LangStr(_strIdShowInLegend)
        'TabMarker
        TabMarker.Text = GisControlView.LangStr(_strIdTabMarker)
        lblMarkerStyle.Text = GisControlView.LangStr(_strIdSymbol)
        lblMarkerColor.Text = GisControlView.LangStr(_strIdColor)
        lblMarkerSize.Text = GisControlView.LangStr(_strIdSize)
        chkMarkerColorUR.Text = GisControlView.LangStr(_strIdUseRenderer)
        chkMarkerSizeUR.Text = GisControlView.LangStr(_strIdUseRenderer)
        lblMarkerOutlineStyle.Text = GisControlView.LangStr(_strIdOutlineStyle)
        lblMarkerOutlineColor.Text = GisControlView.LangStr(_strIdOutlineColor)
        lblMarkerOutlineWidth.Text = GisControlView.LangStr(_strIdOutlineWidth)
        chkMarkerOutlineColorUR.Text = GisControlView.LangStr(_strIdUseRenderer)
        chkMarkerOutlineWidthUR.Text = GisControlView.LangStr(_strIdUseRenderer)
        chkMarkerInLegend.Text = GisControlView.LangStr(_strIdShowInLegend)
        'TabLabel
        TabLabel.Text = GisControlView.LangStr(_strIdTabLabel)
        lblLabelColor.Text = GisControlView.LangStr(_strIdLabelColor)
        chkLabelColorUR.Text = GisControlView.LangStr(_strIdUseRenderer)
        lblLabelFont.Text = GisControlView.LangStr(_strIdLabelFont)
        btnLabelFont.Text = GisControlView.LangStr(_strIdLabelFont)
        lblLabelField.Text = GisControlView.LangStr(_strIdLabelField)
        lblLabelValue.Text = GisControlView.LangStr(_strIdLabelValue)
        chkLabelInLegend.Text = GisControlView.LangStr(_strIdShowInLegend)
        chkLabelVisible.Text = GisControlView.LangStr(_strIdLabelVisible)
        lblLabelRotation.Text = GisControlView.LangStr(_strIdLabelRotation)
        btnMarkerSymbol.Text = GisControlView.LangStr(_strIdMarker)
        uxLabelPosition.Text = GisControlView.LangStr(_strIdLabelPosition)
        'TabChart
        TabChart.Text = GisControlView.LangStr(_strIdTabChart)
        lblChartStyle.Text = GisControlView.LangStr(_strIdChartStyle)
        lblChartSize.Text = GisControlView.LangStr(_strIdChartSize)
        chkChartSizeUR.Text = GisControlView.LangStr(_strIdUseRenderer)
        lblChartFields.Text = GisControlView.LangStr(_strIdChartFields)
        chkChartInLegend.Text = GisControlView.LangStr(_strIdShowInLegend)

        'Find all color buttons, set 'More colors' text
        For Each t In Me.Tabs.Controls
            If TypeOf t Is TabPage Then
                For Each c In CType(t, TabPage).Controls
                    If TypeOf c Is ColorButton.ColorButton Then
                        CType(c, ColorButton.ColorButton).MoreColors = GisControlView.LangStr(_strIdMoreColors)
                    End If
                Next
            End If
        Next
    End Sub

    Private Shared Sub ListPatterns(ByVal cmb As ComboBox, ByVal curBS As TGIS_BrushStyle)
        Dim i As Integer
        Dim val As ValueDescriptionPair

        cmb.Items.Clear()

        'List values
        val = New ValueDescriptionPair(TGIS_BrushStyle.gisBsSolid, GisControlView.LangStr(_strIdPatternSolid))
        cmb.Items.Add(val)
        val = New ValueDescriptionPair(TGIS_BrushStyle.gisBsClear, GisControlView.LangStr(_strIdPatternTransparent))
        cmb.Items.Add(val)
        val = New ValueDescriptionPair(TGIS_BrushStyle.gisBsHorizontal, GisControlView.LangStr(_strIdPatternHorizontal))
        cmb.Items.Add(val)
        val = New ValueDescriptionPair(TGIS_BrushStyle.gisBsVertical, GisControlView.LangStr(_strIdPatternVertical))
        cmb.Items.Add(val)
        val = New ValueDescriptionPair(TGIS_BrushStyle.gisBsFDiagonal, GisControlView.LangStr(_strIdPatternFdiagonal))
        cmb.Items.Add(val)
        val = New ValueDescriptionPair(TGIS_BrushStyle.gisBsBDiagonal, GisControlView.LangStr(_strIdPatternBdiagonal))
        cmb.Items.Add(val)
        val = New ValueDescriptionPair(TGIS_BrushStyle.gisBsCross, GisControlView.LangStr(_strIdPatternCross))
        cmb.Items.Add(val)
        val = New ValueDescriptionPair(TGIS_BrushStyle.gisBsDiagCross, GisControlView.LangStr(_strIdPatternDiagCross))
        cmb.Items.Add(val)

        'Select current value
        For i = 0 To cmb.Items.Count - 1
            If CType(cmb.Items.Item(i), ValueDescriptionPair).Value = curBS Then
                cmb.SelectedIndex = i
                Exit For
            End If
        Next
    End Sub

    Private Shared Sub ListStyles(ByVal cmb As ComboBox, ByVal curPS As TGIS_PenStyle)
        Dim i As Integer
        Dim val As ValueDescriptionPair

        cmb.Items.Clear()

        'List values
        val = New ValueDescriptionPair(TGIS_PenStyle.gisPsSolid, GisControlView.LangStr(_strIdStyleSolid))
        cmb.Items.Add(val)
        val = New ValueDescriptionPair(TGIS_PenStyle.gisPsDash, GisControlView.LangStr(_strIdStyleDash))
        cmb.Items.Add(val)
        val = New ValueDescriptionPair(TGIS_PenStyle.gisPsDot, GisControlView.LangStr(_strIdStyleDot))
        cmb.Items.Add(val)
        val = New ValueDescriptionPair(TGIS_PenStyle.gisPsDashDot, GisControlView.LangStr(_strIdStyleDashDot))
        cmb.Items.Add(val)
        val = New ValueDescriptionPair(TGIS_PenStyle.gisPsDashDotDot, GisControlView.LangStr(_strIdStyleDashDotDot))
        cmb.Items.Add(val)
        val = New ValueDescriptionPair(TGIS_PenStyle.gisPsClear, GisControlView.LangStr(_strIdStyleClear))
        cmb.Items.Add(val)

        'Select current value
        For i = 0 To cmb.Items.Count - 1
            If CType(cmb.Items.Item(i), ValueDescriptionPair).Value = curPS Then
                cmb.SelectedIndex = i
                Exit For
            End If
        Next
    End Sub

    Private Sub ApplySettingsGeneral()
        Dim i As Integer

        If _ll IsNot Nothing Then
            _ll.Caption = Caption.Text
            _ll.Transparency = (100 - Transparency.Value)

            'Apply to any sublayers
            If _ll.SubLayers IsNot Nothing Then
                For i = 0 To _ll.SubLayers.Count - 1
                    _ll.SubLayers(i).Transparency = _ll.Transparency
                Next
            End If
        ElseIf _grp <> vbNullString Then
            'Group (we need to find the LayerGroup instance in the Items array and update the properties from there. Using _gisCtrl.Hierarchy.Groups array won't do)
            For i = 0 To _gisCtrl.Hierarchy.Layers.Count - 1
                If CType(_gisCtrl.Hierarchy.Layers.Items(i), TGIS_LayerAbstract).Name = _grp Then
                    CType(_gisCtrl.Hierarchy.Layers.Items(i), TGIS_LayerAbstract).Caption = Caption.Text
                End If
            Next
        End If
    End Sub

    Private Sub ApplySettingsSection()
        Dim minscale As Double = 0, maxscale As Double = _maxScale
        'Section properties
        'Min/max scale
        If ValidateScaleText(SectionMinScale.Text) Then
            Dim vals As String() = SectionMinScale.Text.Split(":"c)
            Try
                minscale = 1.0 / (Convert.ToDouble(vals(1), CultureInfo.InvariantCulture) / Convert.ToDouble(vals(0), CultureInfo.InvariantCulture))
            Catch ex As OverflowException
            End Try
        End If
        If ValidateScaleText(SectionMaxScale.Text) Then
            Dim vals As String() = SectionMaxScale.Text.Split(":"c)
            Try
                maxscale = 1.0 / (Convert.ToDouble(vals(1), CultureInfo.InvariantCulture) / Convert.ToDouble(vals(0), CultureInfo.InvariantCulture))
            Catch ex As OverflowException
            End Try
        End If

        _ll.Params.Visible = chkSectionVisible.Checked
        _ll.Params.MinScale = minscale
        _ll.Params.MaxScale = maxscale
        If _lv IsNot Nothing Then
            CType(_ll.Params, TGIS_ParamsSectionVector).Query = SectionQuery.Text
        End If
        _ll.Params.Legend = SectionLegend.Text
    End Sub

    Private Sub ApplySettingsRendering()
        If _lv IsNot Nothing Then
            If RenderField.SelectedIndex > 0 Then
                'Brackets are added to make sure field name is interpreted properly
                _lv.Params.Render.Expression = "[" & RenderField.Text & "]"
            Else
                _lv.Params.Render.Expression = vbNullString
            End If
            _lv.Params.Render.ColorDefault = RenderDefaultColor.Color
            _lv.Params.Render.SizeDefault = RenderDefaultSize.Value

            'First pass
            _lv.Params.Render.Zones = RenderZones.Value
            _lv.Params.Render.MinVal = Replace(RenderMinValue.Text, vbNullString, "0")
            _lv.Params.Render.MaxVal = Replace(RenderMaxValue.Text, vbNullString, "0")
            _lv.Params.Render.StartColor = RenderStartColor.Color
            _lv.Params.Render.EndColor = RenderEndColor.Color
            _lv.Params.Render.StartSize = RenderStartSize.Value
            _lv.Params.Render.EndSize = RenderEndSize.Value

            'Second pass
            _lv.Params.Render.ZonesEx = RenderZonesEx.Value
            _lv.Params.Render.MinValEx = Replace(RenderMinValueEx.Text, vbNullString, "0")
            _lv.Params.Render.MaxValEx = Replace(RenderMaxValueEx.Text, vbNullString, "0")
            _lv.Params.Render.StartColorEx = RenderStartColorEx.Color
            _lv.Params.Render.EndColorEx = RenderEndColorEx.Color
            _lv.Params.Render.StartSizeEx = RenderStartSizeEx.Value
            _lv.Params.Render.EndSizeEx = RenderEndSizeEx.Value
        End If
    End Sub

    Private Sub ApplySettingsArea(ByRef params As TGIS_ParamsSectionVector)
        'Area pattern
        If AreaPattern.SelectedItem IsNot Nothing Then
            params.Area.Pattern = CType(AreaPattern.SelectedItem, ValueDescriptionPair).Value
        End If

        'Area color
        If chkAreaColorUR.Checked Then
            params.Area.Color = TGIS_Utils.GIS_RENDER_COLOR()
        Else
            params.Area.Color = AreaColor.Color
        End If

        'Outline pattern
        If AreaOutlineStyle.SelectedItem IsNot Nothing Then
            params.Area.OutlineStyle = CType(AreaOutlineStyle.SelectedItem, ValueDescriptionPair).Value
        End If

        'Outline color
        If chkAreaOutlineColorUR.Checked Then
            params.Area.OutlineColor = TGIS_Utils.GIS_RENDER_COLOR()
        Else
            params.Area.OutlineColor = AreaOutlineColor.Color
        End If

        'Outline width
        If chkAreaOutlineWidthUR.Checked Then
            params.Area.OutlineWidth = TGIS_Utils.GIS_RENDER_SIZE()
        Else
            params.Area.OutlineWidth = AreaOutlineWidth.Value
        End If

        params.Area.ShowLegend = chkAreaInLegend.Checked
    End Sub

    Private Sub ApplySettingsLine(ByRef params As TGIS_ParamsSectionVector)
        'Line style
        If LineStyle.SelectedItem IsNot Nothing Then
            Params.Line.Style = CType(LineStyle.SelectedItem, ValueDescriptionPair).Value
        End If

        'Line color
        If chkLineColorUR.Checked Then
            params.Line.Color = TGIS_Utils.GIS_RENDER_COLOR()
        Else
            params.Line.Color = LineColor.Color
        End If

        'Line width
        If chkLineWidthUR.Checked Then
            params.Line.Width = TGIS_Utils.GIS_RENDER_SIZE()
        Else
            params.Line.Width = LineWidth.Value
        End If

        'Outline style
        If LineOutlineStyle.SelectedItem IsNot Nothing Then
            params.Line.OutlineStyle = CType(LineOutlineStyle.SelectedItem, ValueDescriptionPair).Value
        End If

        'Outline color
        If chkLineOutlineColorUR.Checked Then
            params.Line.OutlineColor = TGIS_Utils.GIS_RENDER_COLOR()
        Else
            params.Line.OutlineColor = LineOutlineColor.Color
        End If

        'Outline width
        If chkLineOutlineWidthUR.Checked Then
            params.Line.OutlineWidth = TGIS_Utils.GIS_RENDER_SIZE()
        Else
            params.Line.OutlineWidth = LineOutlineWidth.Value
        End If

        params.Line.ShowLegend = chkLineInLegend.Checked
    End Sub

    Private Sub ApplySettingsMarker(ByRef params As TGIS_ParamsSectionVector)
        'Marker style
        If MarkerStyle.SelectedItem IsNot Nothing Then
            params.Marker.Style = CType(MarkerStyle.SelectedItem, ValueDescriptionPair).Value
        End If

        'Marker color
        If chkMarkerColorUR.Checked Then
            params.Marker.Color = TGIS_Utils.GIS_RENDER_COLOR()
        Else
            params.Marker.Color = MarkerColor.Color
        End If

        'Marker size
        If chkMarkerSizeUR.Checked Then
            params.Marker.Size = TGIS_Utils.GIS_RENDER_SIZE()
        Else
            params.Marker.Size = MarkerSize.Value
        End If

        'Outline style
        If MarkerOutlineStyle.SelectedItem IsNot Nothing Then
            params.Marker.OutlineStyle = CType(MarkerOutlineStyle.SelectedItem, ValueDescriptionPair).Value
        End If

        'Outline color
        If chkMarkerOutlineColorUR.Checked Then
            params.Marker.OutlineColor = TGIS_Utils.GIS_RENDER_COLOR()
        Else
            params.Marker.OutlineColor = MarkerOutlineColor.Color
        End If

        'Outline width
        If chkMarkerOutlineWidthUR.Checked Then
            params.Marker.OutlineWidth = TGIS_Utils.GIS_RENDER_SIZE()
        Else
            params.Marker.OutlineWidth = MarkerOutlineWidth.Value
        End If

        params.Marker.ShowLegend = chkMarkerInLegend.Checked

        'Enable/disable marker style settings
        DisableMarkerSettings()
    End Sub

    Private Sub ApplySettingsLabel(ByRef params As TGIS_ParamsSectionVector)
        'Label color
        If chkLabelColorUR.Checked Then
            params.Labels.Color = TGIS_Utils.GIS_RENDER_COLOR()
        Else
            params.Labels.Color = LabelColor.Color
        End If

        params.Labels.Font = btnLabelFont.Font
        params.Labels.Field = LabelField.Text
        params.Labels.Value = LabelValue.Text
        params.Labels.ShowLegend = chkLabelInLegend.Checked
        params.Labels.Visible = chkLabelVisible.Checked

        params.Labels.Rotate = (LabelRotation.Value / 180.0 * Math.PI)

        params.Labels.Position = (TGIS_LabelPositions.gisLabelPositionUpLeft And uxLabelPos1.Checked) Or _
                                 (TGIS_LabelPositions.gisLabelPositionUpCenter And uxLabelPos2.Checked) Or _
                                 (TGIS_LabelPositions.gisLabelPositionUpRight And uxLabelPos3.Checked) Or _
                                 (TGIS_LabelPositions.gisLabelPositionMiddleLeft And uxLabelPos4.Checked) Or _
                                 (TGIS_LabelPositions.gisLabelPositionMiddleCenter And uxLabelPos5.Checked) Or _
                                 (TGIS_LabelPositions.gisLabelPositionMiddleRight And uxLabelPos6.Checked) Or _
                                 (TGIS_LabelPositions.gisLabelPositionDownLeft And uxLabelPos7.Checked) Or _
                                 (TGIS_LabelPositions.gisLabelPositionDownCenter And uxLabelPos8.Checked) Or _
                                 (TGIS_LabelPositions.gisLabelPositionDownRight And uxLabelPos9.Checked)
    End Sub

    Private Sub ApplySettingsRelation()
        Dim rt As RelationType = RelationType.Undefined
        Dim jv As String = vbNullString

        'Update relation settings if specified
        If _ll IsNot Nothing Then
            If Relation.SelectedIndex >= 1 Then rt = CType(Relation.SelectedItem, ValueDescriptionPair).Value
            If View.SelectedItem IsNot Nothing AndAlso TypeOf View.SelectedItem Is JoinView Then jv = CType(View.SelectedItem, JoinView).Name
            GisControlView.UpdateLayerRelation(_ll.Name, _ll.Caption, _ll.CS.EPSG, rt, jv)
        End If
    End Sub

    Private Sub ApplySettingsChart()
        Dim i As Integer

        If _lv IsNot Nothing Then
            'Chart style
            If ChartStyle.SelectedItem IsNot Nothing Then
                _lv.Params.Chart.Style = CType(ChartStyle.SelectedItem, ValueDescriptionPair).Value
            End If

            'Chart size
            If chkChartSizeUR.Checked Then
                _lv.Params.Chart.Size = TGIS_Utils.GIS_RENDER_SIZE()
            Else
                _lv.Params.Chart.Size = ChartSize.Value
            End If

            'Chart fields
            _lv.Params.Render.Chart = "0:0"
            For i = 0 To ChartFields.SelectedItems.Count - 1
                _lv.Params.Render.Chart += ":" & ChartFields.SelectedItems.Item(i)
            Next
            _lv.Params.Chart.Legend = _lv.Params.Render.Chart & ":" 'Legend explanation

            _lv.Params.Chart.ShowLegend = chkChartInLegend.Checked
        End If
    End Sub

    Private Sub SetupTabGeneral()
        Dim i As Integer
        Dim val As ValueDescriptionPair

        Relation.Items.Add("")
        If GisControlView.EstimateInstalled Then
            val = New ValueDescriptionPair(RelationType.StandRelation, GisControlView.LangStr(_strIdForestStand))
            Relation.Items.Add(val)
        End If

        If GisControlView.EstimateSodraInstalled Then
            val = New ValueDescriptionPair(RelationType.SectionRelation, GisControlView.LangStr(_strIdForestSection))
            Relation.Items.Add(val)
        End If

        If GisControlView.ForestInstalled Then
            val = New ValueDescriptionPair(RelationType.PropertyRelation, GisControlView.LangStr(_strIdForestProperty))
            Relation.Items.Add(val)
        End If

        If GisControlView.LandValueInstalled Then
            val = New ValueDescriptionPair(RelationType.ObjectRelation, GisControlView.LangStr(_strIdForestObject))
            Relation.Items.Add(val)

            val = New ValueDescriptionPair(RelationType.SampleTreeRelation, GisControlView.LangStr(_strIdForestSampleTrees))
            Relation.Items.Add(val)
        End If
        If GisControlView.TCruiseInstalled Then
            val = New ValueDescriptionPair(RelationType.TCTractRelation, GisControlView.LangStr(_strIdTcStand))
            Relation.Items.Add(val)

            val = New ValueDescriptionPair(RelationType.TCPlotRelation, GisControlView.LangStr(_strIdTcPlot))
            Relation.Items.Add(val)
        End If
        If Relation.Items.Count = 1 Then Relation.Enabled = False 'No options

        'Enable relations only on db layers
        View.Enabled = False 'Will be enabled when relation is choosen
        If _ll IsNot Nothing AndAlso TypeOf _ll Is TGIS_LayerSqlAbstract Then
            'Get layer relation type and joined view
            GisControlView.GetLayerRelationType(_ll.Name, _type, _joinView)

            'Find index of this relation type if set
            If _type <> RelationType.Undefined Then
                For i = 1 To Relation.Items.Count - 1
                    If CType(Relation.Items(i), ValueDescriptionPair).Value = _type Then
                        Relation.SelectedIndex = i
                        Exit For
                    End If
                Next

                'Check if this layer has any defined relations, if so user should not be able to change relation type
                Dim cmd As New OleDbCommand("", GisControlView.Conn)
                Dim reader As OleDbDataReader

                cmd.CommandText = "SELECT TOP 1 1 FROM " & GisControlView.GisTablePrefix & _ll.Name & "_FEA WHERE " & GisControlView.FieldNameRelation & " IS NOT NULL"
                reader = cmd.ExecuteReader(CommandBehavior.SingleRow)
                If reader.HasRows Then
                    Relation.Enabled = False
                End If
                reader.Close()
            End If
        Else
            Relation.Enabled = False
            btnRestructure.Enabled = False
        End If

        'Layer/Group properties
        If _ll IsNot Nothing Then
            'Layer
            If Not TypeOf _ll Is TGIS_LayerSqlAdo AndAlso Not TypeOf _ll Is TGIS_LayerPixelStoreAdo2 Then
                Path.Text = _ll.Path
            Else
                'No path for database layers
                Path.Text = GisControlView.LangStr(_strIdPathDB)
                Path.Enabled = False
            End If
            If _ll.CS IsNot Nothing Then
                Projection.Text = _ll.CS.WKT
            End If
            Transparency.Value = (100 - _ll.Transparency)
            Caption.Text = _ll.Caption

            If _lv IsNot Nothing Then
                'Vector layer, enable param section buttons - this will load button images
                btnParamsAdd.Enabled = True
                btnParamsRemoveAll.Enabled = True
            Else
                'Pixel layer, no params
                btnWizard.Enabled = False
                lstParams.Enabled = False
            End If

            If _ll.ParentLayer IsNot Nothing Then Transparency.Enabled = False 'Disable transparency (which have no effect) on sublayers
        ElseIf _grp <> vbNullString Then
            'Group
            Path.Enabled = False
            btnProjection.Enabled = False
            Transparency.Enabled = False
            lstParams.Enabled = False
            If _gisCtrl.Hierarchy.Groups(_grp) IsNot Nothing Then Caption.Text = _gisCtrl.Hierarchy.Groups(_grp).Caption
        End If

        'Alpha channel
        If TypeOf _ll Is TGIS_LayerPixel AndAlso Not TypeOf _ll Is TGIS_LayerPixelStoreAdo2 Then 'Only for pixel layers (except PixelStore2)
            lblAlphaChannel.Enabled = True
            AlphaChannel.Enabled = True

            AlphaChannel.Items.Add(GisControlView.LangStr(_strIdAlphaChannelNone))
            AlphaChannel.Items.Add(GisControlView.LangStr(_strIdAlphaChannelDefault))
            AlphaChannel.Items.Add(GisControlView.LangStr(_strIdAlphaChannelRed))
            AlphaChannel.Items.Add(GisControlView.LangStr(_strIdAlphaChannelGreen))
            AlphaChannel.Items.Add(GisControlView.LangStr(_strIdAlphaChannelBlue))

            Dim pp As TGIS_ParamsPixel
            pp = CType(_ll, TGIS_LayerPixel).Params.Pixel
            AlphaChannel.SelectedIndex = pp.AlphaBand + 1
        Else
            lblAlphaChannel.Enabled = False
            AlphaChannel.Enabled = False
        End If

        'Disable wizard on compound layers
        If TypeOf _ll Is TGIS_CompoundLayerAbstract Then btnWizard.Enabled = False
    End Sub

    Private Sub SetupTabSection(Optional ByVal fieldsOnly As Boolean = False)
        'Section properties
        If _lv IsNot Nothing Then
            'List fields for query
            Dim fields As New List(Of String)
            GetFields(fields)
            SectionQuery.Items.Clear()
            For Each fieldName As String In fields
                SectionQuery.Items.Add(fieldName)
            Next

            If Not fieldsOnly Then
                SectionQuery.Text = CType(_ll.Params, TGIS_ParamsSectionVector).Query
                SectionQuery.Enabled = True

                'These message handlers will prevent non-numeric strings from being pasted into these textboxes
                Dim OnPasteSectionMinScale As New TextBoxOnPaste(Me.SectionMinScale)
                Dim OnPasteSectionMaxScale As New TextBoxOnPaste(Me.SectionMaxScale)

                'Min/max scale
                If _ll.Params.MinScale <> 0 Then
                    SectionMinScale.Text = "1:" & Math.Round(1.0 / _ll.Params.MinScale)
                Else
                    SectionMinScale.Text = vbNullString
                End If
                If _ll.Params.MaxScale <> _maxScale Then
                    SectionMaxScale.Text = "1:" & Math.Round(1.0 / _ll.Params.MaxScale)
                Else
                    SectionMaxScale.Text = vbNullString
                End If
            End If
        ElseIf Not fieldsOnly Then
            SectionQuery.Enabled = False
        End If

        If Not fieldsOnly Then
            chkSectionVisible.Checked = _ll.Params.Visible
            SectionLegend.Text = _ll.Params.Legend
        End If
    End Sub

    Private Sub SetupTabRendering(Optional ByVal fieldsOnly As Boolean = False)
        Dim fields As New List(Of String)

        If _lv IsNot Nothing Then
            'Render field, select current value
            GetFields(fields)
            RenderField.Items.Clear()
            RenderField.Items.Add(" ") 'Empty choice
            For Each fieldName As String In fields
                RenderField.Items.Add(fieldName)
                If fieldName = _lv.Params.Render.Expression.TrimStart("[").TrimEnd("]") Then RenderField.SelectedItem = fieldName
            Next

            If Not fieldsOnly Then
                'These message handlers will prevent non-numeric strings from being pasted into these textboxes
                Dim OnPasteRenderMinValue As New TextBoxOnPaste(Me.RenderMinValue)
                Dim OnPasteRenderMaxValue As New TextBoxOnPaste(Me.RenderMaxValue)
                Dim OnPasteRenderMinValueEx As New TextBoxOnPaste(Me.RenderMinValueEx)
                Dim OnPasteRenderMaxValueEx As New TextBoxOnPaste(Me.RenderMaxValueEx)

                'First
                RenderZones.Value = _lv.Params.Render.Zones
                RenderMinValue.Text = _lv.Params.Render.MinVal
                RenderMaxValue.Text = _lv.Params.Render.MaxVal
                RenderStartColor.Color = _lv.Params.Render.StartColor
                RenderEndColor.Color = _lv.Params.Render.EndColor
                RenderDefaultColor.Color = _lv.Params.Render.ColorDefault
                RenderStartSize.Value = _lv.Params.Render.StartSize
                RenderEndSize.Value = _lv.Params.Render.EndSize
                RenderDefaultSize.Value = _lv.Params.Render.SizeDefault

                'Second
                RenderZonesEx.Value = _lv.Params.Render.ZonesEx
                RenderMinValueEx.Text = _lv.Params.Render.MinValEx
                RenderMaxValueEx.Text = _lv.Params.Render.MaxValEx
                RenderStartColorEx.Color = _lv.Params.Render.StartColorEx
                RenderEndColorEx.Color = _lv.Params.Render.EndColorEx
                RenderDefaultColorEx.Color = _lv.Params.Render.ColorDefault
                RenderStartSizeEx.Value = _lv.Params.Render.StartSizeEx
                RenderEndSizeEx.Value = _lv.Params.Render.EndSizeEx
                RenderDefaultSizeEx.Value = _lv.Params.Render.SizeDefault
            End If
        End If

        'Enable appropriate rendering controls
        EnableControls()

        RenderStartColor.Automatic = String.Empty
        RenderEndColor.Automatic = String.Empty
        RenderDefaultColor.Automatic = String.Empty
    End Sub

    Private Sub SetupTabArea()
        If _lv IsNot Nothing Then
            Dim pdef As New TGIS_ParamsSectionVector 'Default values

            'Area pattern
            ListPatterns(AreaPattern, _lv.Params.Area.Pattern)

            'Area outline style
            ListStyles(AreaOutlineStyle, _lv.Params.Area.OutlineStyle)

            'Area color
            If _lv.Params.Area.Color <> TGIS_Utils.GIS_RENDER_COLOR() Then
                AreaColor.Color = _lv.Params.Area.Color
                chkAreaColorUR.Checked = False
            Else
                AreaColor.Color = pdef.Area.Color
                chkAreaColorUR.Checked = True
            End If

            'Outline color
            If _lv.Params.Area.OutlineColor <> TGIS_Utils.GIS_RENDER_COLOR() Then
                AreaOutlineColor.Color = _lv.Params.Area.OutlineColor
                chkAreaOutlineColorUR.Checked = False
            Else
                AreaOutlineColor.Color = pdef.Area.OutlineColor
                chkAreaOutlineColorUR.Checked = True
            End If

            'Outline width
            If _lv.Params.Area.OutlineWidth <> TGIS_Utils.GIS_RENDER_SIZE() Then
                AreaOutlineWidth.Value = _lv.Params.Area.OutlineWidth
                chkAreaOutlineWidthUR.Checked = False
            Else
                AreaOutlineWidth.Value = pdef.Area.OutlineWidth
                chkAreaOutlineWidthUR.Checked = True
            End If

            chkAreaInLegend.Checked = _lv.Params.Area.ShowLegend
        End If

        AreaColor.Automatic = String.Empty
        AreaOutlineColor.Automatic = String.Empty
    End Sub

    Private Sub SetupTabLine()
        If _lv IsNot Nothing Then
            Dim pdef As New TGIS_ParamsSectionVector 'Default values

            'Line style
            ListStyles(LineStyle, _lv.Params.Line.Style)

            'Line color
            If _lv.Params.Line.Color <> TGIS_Utils.GIS_RENDER_COLOR() Then
                LineColor.Color = _lv.Params.Line.Color
                chkLineColorUR.Checked = False
            Else
                LineColor.Color = pdef.Line.Color
                chkLineColorUR.Checked = True
            End If

            'Line width
            If _lv.Params.Line.Width <> TGIS_Utils.GIS_RENDER_SIZE() Then
                LineWidth.Value = _lv.Params.Line.Width
                chkLineWidthUR.Checked = False
            Else
                LineWidth.Value = pdef.Line.Width
                chkLineWidthUR.Checked = True
            End If

            'Outline style
            ListStyles(LineOutlineStyle, _lv.Params.Line.OutlineStyle)

            'Outline color
            If _lv.Params.Line.OutlineColor <> TGIS_Utils.GIS_RENDER_COLOR() Then
                LineOutlineColor.Color = _lv.Params.Line.OutlineColor
                chkLineOutlineColorUR.Checked = False
            Else
                LineOutlineColor.Color = pdef.Line.OutlineColor
                chkLineOutlineColorUR.Checked = True
            End If

            'Outline width
            If _lv.Params.Line.OutlineWidth <> TGIS_Utils.GIS_RENDER_SIZE() Then
                LineOutlineWidth.Value = _lv.Params.Line.OutlineWidth
                chkLineOutlineWidthUR.Checked = False
            Else
                LineOutlineWidth.Value = pdef.Line.OutlineWidth
                chkLineOutlineWidthUR.Checked = True
            End If

            chkLineInLegend.Checked = _lv.Params.Line.ShowLegend
        End If

        LineColor.Automatic = String.Empty
    End Sub

    Private Sub SetupTabMarker()
        If _lv IsNot Nothing Then
            Dim i As Integer
            Dim val As ValueDescriptionPair
            Dim pdef As New TGIS_ParamsSectionVector 'Default values

            'Marker style
            MarkerStyle.Items.Clear()
            val = New ValueDescriptionPair(TGIS_MarkerStyle.gisMarkerStyleBox, GisControlView.LangStr(_strIdBox))
            MarkerStyle.Items.Add(val)
            val = New ValueDescriptionPair(TGIS_MarkerStyle.gisMarkerStyleCircle, GisControlView.LangStr(_strIdCircle))
            MarkerStyle.Items.Add(val)
            val = New ValueDescriptionPair(TGIS_MarkerStyle.gisMarkerStyleCross, GisControlView.LangStr(_strIdCross))
            MarkerStyle.Items.Add(val)
            val = New ValueDescriptionPair(TGIS_MarkerStyle.gisMarkerStyleDiagCross, GisControlView.LangStr(_strIdDiagCross))
            MarkerStyle.Items.Add(val)
            val = New ValueDescriptionPair(TGIS_MarkerStyle.gisMarkerStyleTriangleUp, GisControlView.LangStr(_strIdTriangleUp))
            MarkerStyle.Items.Add(val)
            val = New ValueDescriptionPair(TGIS_MarkerStyle.gisMarkerStyleTriangleDown, GisControlView.LangStr(_strIdTriangleDown))
            MarkerStyle.Items.Add(val)
            val = New ValueDescriptionPair(TGIS_MarkerStyle.gisMarkerStyleTriangleLeft, GisControlView.LangStr(_strIdTriangleLeft))
            MarkerStyle.Items.Add(val)
            val = New ValueDescriptionPair(TGIS_MarkerStyle.gisMarkerStyleTriangleRight, GisControlView.LangStr(_strIdTriangleRight))
            MarkerStyle.Items.Add(val)

            'Select current value
            For i = 0 To MarkerStyle.Items.Count - 1
                If CType(MarkerStyle.Items.Item(i), ValueDescriptionPair).Value = _lv.Params.Marker.Style Then
                    MarkerStyle.SelectedIndex = i
                    Exit For
                End If
            Next

            'Marker color
            If _lv.Params.Marker.Color <> TGIS_Utils.GIS_RENDER_COLOR() Then
                MarkerColor.Color = _lv.Params.Marker.Color
                chkMarkerColorUR.Checked = False
            Else
                MarkerColor.Color = pdef.Marker.Color
                chkMarkerColorUR.Checked = True
            End If

            'Marker size
            If _lv.Params.Marker.Size <> TGIS_Utils.GIS_RENDER_SIZE() Then
                MarkerSize.Value = _lv.Params.Marker.Size
                chkMarkerSizeUR.Checked = False
            Else
                MarkerSize.Value = pdef.Marker.Size
                chkMarkerSizeUR.Checked = True
            End If

            'Outline style
            ListStyles(MarkerOutlineStyle, _lv.Params.Marker.OutlineStyle)

            'Outline color
            If _lv.Params.Marker.OutlineColor <> TGIS_Utils.GIS_RENDER_COLOR() Then
                MarkerOutlineColor.Color = _lv.Params.Marker.OutlineColor
                chkMarkerOutlineColorUR.Checked = False
            Else
                MarkerOutlineColor.Color = pdef.Marker.OutlineColor
                chkMarkerOutlineColorUR.Checked = True
            End If

            'Outline width
            If _lv.Params.Marker.OutlineWidth <> TGIS_Utils.GIS_RENDER_SIZE() Then
                MarkerOutlineWidth.Value = _lv.Params.Marker.OutlineWidth
                chkMarkerOutlineWidthUR.Checked = False
            Else
                MarkerOutlineWidth.Value = pdef.Marker.OutlineWidth
                chkMarkerOutlineWidthUR.Checked = True
            End If

            chkMarkerInLegend.Checked = _lv.Params.Marker.ShowLegend

            'Enable/disable marker settings
            DisableMarkerSettings()
        End If

        MarkerColor.Automatic = String.Empty
    End Sub

    Private Sub SetupTabLabel(Optional ByVal fieldsOnly As Boolean = False)
        If _lv IsNot Nothing Then
            Dim fields As New List(Of String)
            Dim pdef As New TGIS_ParamsSectionVector 'Default values

            If Not fieldsOnly Then
                'Label color
                If _lv.Params.Labels.Color <> TGIS_Utils.GIS_RENDER_COLOR() Then
                    LabelColor.Color = _lv.Params.Labels.Color
                    chkLabelColorUR.Checked = False
                Else
                    LabelColor.Color = pdef.Labels.Color
                    chkLabelColorUR.Checked = True
                End If

                btnLabelFont.Font = New Font(_lv.Params.Labels.Font, _lv.Params.Labels.Font.Style) 'Construct a new font, assigning Params font reference will cause trigger exceptions on paint
                btnLabelFont.Text = btnLabelFont.Font.Name

                LabelValue.Text = _lv.Params.Labels.Value
                chkLabelInLegend.Checked = _lv.Params.Labels.ShowLegend
                chkLabelVisible.Checked = _lv.Params.Labels.Visible

                LabelRotation.Value = _lv.Params.Labels.Rotate / Math.PI * 180.0

                If _lv.Params.Labels.Position And TGIS_LabelPositions.gisLabelPositionUpLeft Then uxLabelPos1.Checked = True
                If _lv.Params.Labels.Position And TGIS_LabelPositions.gisLabelPositionUpCenter Then uxLabelPos2.Checked = True
                If _lv.Params.Labels.Position And TGIS_LabelPositions.gisLabelPositionUpRight Then uxLabelPos3.Checked = True
                If _lv.Params.Labels.Position And TGIS_LabelPositions.gisLabelPositionMiddleLeft Then uxLabelPos4.Checked = True
                If _lv.Params.Labels.Position And TGIS_LabelPositions.gisLabelPositionMiddleCenter Then uxLabelPos5.Checked = True
                If _lv.Params.Labels.Position And TGIS_LabelPositions.gisLabelPositionMiddleRight Then uxLabelPos6.Checked = True
                If _lv.Params.Labels.Position And TGIS_LabelPositions.gisLabelPositionDownLeft Then uxLabelPos7.Checked = True
                If _lv.Params.Labels.Position And TGIS_LabelPositions.gisLabelPositionDownCenter Then uxLabelPos8.Checked = True
                If _lv.Params.Labels.Position And TGIS_LabelPositions.gisLabelPositionDownRight Then uxLabelPos9.Checked = True
            End If

            'Label field, select current value
            GetFields(fields)
            LabelField.Items.Clear()
            If fields.Count > 0 Then LabelField.Items.Add("") 'Add empty option
            For Each fieldName As String In fields
                LabelField.Items.Add(fieldName)
                If fieldName = _lv.Params.Labels.Field Then LabelField.SelectedItem = fieldName
            Next
        End If

        LabelColor.Automatic = String.Empty
    End Sub

    Private Sub SetupTabChart(Optional ByVal fieldsOnly As Boolean = False)
        If _lv IsNot Nothing Then
            Dim i As Integer
            Dim fields As New List(Of String)
            Dim val As ValueDescriptionPair
            Dim pdef As New TGIS_ParamsSectionVector

            If Not fieldsOnly Then
                'Chart style
                ChartStyle.Items.Clear()
                val = New ValueDescriptionPair(TGIS_ChartStyle.gisChartStylePie, GisControlView.LangStr(_strIdChartPie))
                ChartStyle.Items.Add(val)
                val = New ValueDescriptionPair(TGIS_ChartStyle.gisChartStyleBar, GisControlView.LangStr(_strIdChartBar))
                ChartStyle.Items.Add(val)

                'Select current value
                For i = 0 To ChartStyle.Items.Count - 1
                    If CType(ChartStyle.Items.Item(i), ValueDescriptionPair).Value = _lv.Params.Chart.Style Then
                        ChartStyle.SelectedIndex = i
                        Exit For
                    End If
                Next

                'Chart size
                If _lv.Params.Chart.Size <> TGIS_Utils.GIS_RENDER_SIZE() Then
                    ChartSize.Value = _lv.Params.Chart.Size
                    chkChartSizeUR.Checked = False
                Else
                    ChartSize.Value = pdef.Chart.Size
                    chkChartSizeUR.Checked = True
                End If

                chkChartInLegend.Checked = _lv.Params.Chart.ShowLegend
            End If

            'Chart fields, select current values
            GetFields(fields)
            Dim chart As String() = Split(_lv.Params.Render.Chart, ":")
            ChartFields.Items.Clear()
            For Each fieldName As String In fields
                ChartFields.Items.Add(fieldName)

                'Check if this value is in use. Note! Array.BinaryCompare doesn't work reliably here for some reason.
                Dim s As String
                For Each s In chart
                    If s = fieldName Then
                        ChartFields.SelectedItems.Add(fieldName)
                        Exit For
                    End If
                Next
            Next
        End If
    End Sub

    Private Sub EnableControls()
        Dim value As Boolean

        'Pass one
        If RenderField.SelectedIndex > 0 AndAlso RenderZones.Value <> 0 Then
            value = True
        Else
            value = False
        End If

        RenderStartColor.Enabled = value
        RenderEndColor.Enabled = value
        RenderDefaultColor.Enabled = value
        RenderStartSize.Enabled = value
        RenderEndSize.Enabled = value
        RenderDefaultSize.Enabled = value

        'Pass two
        If RenderField.SelectedIndex > 0 AndAlso RenderZonesEx.Value <> 0 Then
            value = True
        Else
            value = False
        End If

        RenderStartColorEx.Enabled = value
        RenderEndColorEx.Enabled = value
        RenderDefaultColorEx.Enabled = value
        RenderStartSizeEx.Enabled = value
        RenderEndSizeEx.Enabled = value
        RenderDefaultSizeEx.Enabled = value
    End Sub

    Private Shared Function FilterChar(ByVal ch As Char) As Char
        'Let through only numeric values and minus sign (Ctrl is enabled for Ctrl+C)
        If Not Char.IsControl(ch) AndAlso (Asc(ch) < 48 OrElse Asc(ch) > 57) AndAlso ch <> "-"c Then
            Return vbNullChar
        Else
            Return ch
        End If
    End Function

    Private Sub LoadCurrentSection()
        'Load settings for current section
        _noSave = True 'This will prevent savings during initialization
        If _lv IsNot Nothing Then
            SetupTabSection()
            SetupTabRendering()
            If _lv.SupportedShapes And TGIS_ShapeType.gisShapeTypePolygon Then
                SetupTabArea()
            Else
                Tabs.TabPages.Remove(TabArea)
            End If
            If _lv.SupportedShapes And TGIS_ShapeType.gisShapeTypeArc Then
                SetupTabLine()
            Else
                Tabs.TabPages.Remove(TabLine)
            End If
            If (_lv.SupportedShapes And TGIS_ShapeType.gisShapeTypePoint) OrElse (_lv.SupportedShapes And TGIS_ShapeType.gisShapeTypeMultiPoint) Then
                SetupTabMarker()
            Else
                Tabs.TabPages.Remove(TabMarker)
            End If
            SetupTabLabel()
            SetupTabChart()

            'Update preview
            DrawPreview()
        End If
        _noSave = False
    End Sub

    Private Sub SaveCurrentSection()
        If _noSave Then Exit Sub

        'Apply settings for layer vector
        If _lv IsNot Nothing Then
            ApplySettingsSection()
            ApplySettingsRendering()
            If _lv.SupportedShapes And TGIS_ShapeType.gisShapeTypePolygon Then
                ApplySettingsArea(_lv.Params)
            End If
            If _lv.SupportedShapes And TGIS_ShapeType.gisShapeTypeArc Then
                ApplySettingsLine(_lv.Params)
            End If
            If (_lv.SupportedShapes And TGIS_ShapeType.gisShapeTypePoint) OrElse (_lv.SupportedShapes And TGIS_ShapeType.gisShapeTypeMultiPoint) Then
                ApplySettingsMarker(_lv.Params)
            End If
            ApplySettingsLabel(_lv.Params)
            ApplySettingsChart()
        End If
    End Sub

    Private Sub UpdateJoinView()
        'Update joined view
        If TypeOf _ll Is TGIS_LayerVector Then
            Dim _lv As TGIS_LayerVector = CType(_ll, TGIS_LayerVector)
            If TypeOf View.SelectedItem Is JoinView Then
                'Update join params immediately to get the columns listed
                Dim viewname As String = CType(View.SelectedItem, JoinView).Name
                _lv.JoinNET = GisControlView.GetJoinDT(viewname)
                _lv.JoinPrimary = GisControlView.GetJoinPrimary(viewname)
                _lv.JoinForeign = GisControlView.ForeignKeyField
            Else
                'No view selected
                _lv.JoinNET = Nothing
                _lv.JoinPrimary = vbNullString
                _lv.JoinForeign = vbNullString
            End If
        End If

        'Update affected tabs
        SetupTabSection(True)
        SetupTabRendering(True)
        SetupTabLabel(True)
        SetupTabChart(True)
    End Sub

    Private Sub GetFields(ByRef fields As List(Of String))
        Dim i As Integer

        'Always add uid
        fields.Add(GisControlView.FieldNameGisUid)

        'Get list of all fields in layer (also include any joined fields)
        For i = 0 To _lv.Fields.Count - 1
            If Not _lv.FieldInfo(i).IsUID AndAlso Not GisControlView.IsRelationField(_lv.FieldInfo(i).NewName) Then fields.Add(_lv.FieldInfo(i).NewName)
        Next
        If _lv.JoinNET IsNot Nothing Then
            Dim dt As DataTable = CType(_lv.JoinNET, DataTable)
            For i = 0 To dt.Columns.Count - 1
                'Add all fields but foreign key column
                If Not GisControlView.IsRelationField(dt.Columns(i).Caption) Then fields.Add(dt.Columns(i).Caption)
            Next
        End If
    End Sub

    Private Function GetSectionString(Optional ByVal ps As TGIS_ParamsSection = Nothing) As String
        Dim desc As String

        'Use current settings if no ParamsSection is given
        If ps Is Nothing Then
            'Visible
            If chkSectionVisible.Checked Then
                desc = GisControlView.LangStr(_strIdSectionVisible)
            Else
                desc = GisControlView.LangStr(_strIdSectionHidden)
            End If

            'Scale 1:x..1:x
            If ValidateScaleText(SectionMinScale.Text) OrElse ValidateScaleText(SectionMaxScale.Text) Then
                desc += " " & GisControlView.LangStr(_strIdSectionScale) & " [ "
                If ValidateScaleText(SectionMinScale.Text) Then desc += SectionMinScale.Text
                desc += ".."
                If ValidateScaleText(SectionMaxScale.Text) Then desc += SectionMaxScale.Text
                desc += " ]"
            End If

            'Query, Renderer
            If SectionQuery.Text <> vbNullString Then desc += " " & GisControlView.LangStr(_strIdSectionQueryParamsList) & " [ " & SectionQuery.Text & " ]"
            If RenderField.SelectedIndex > 0 Then desc += " " & GisControlView.LangStr(_strIdSectionRenderer) & " [" & RenderField.SelectedItem & " ]"
        Else
            'Visible
            If ps.Visible Then
                desc = GisControlView.LangStr(_strIdSectionVisible)
            Else
                desc = GisControlView.LangStr(_strIdSectionHidden)
            End If

            'Scale 1:x..1:x
            If ps.MinScale <> 0 OrElse ps.MaxScale <> _maxScale Then
                desc += " " & GisControlView.LangStr(_strIdSectionScale) & " [ "
                If ps.MinScale <> 0 Then desc += "1:" & Math.Round(1.0 / ps.MinScale)
                desc += ".."
                If ps.MaxScale <> _maxScale Then desc += "1:" & Math.Round(1.0 / ps.MaxScale)
                desc += " ]"
            End If

            'Query, Renderer
            Dim pv As TGIS_ParamsSectionVector
            pv = TryCast(ps, TGIS_ParamsSectionVector)
            If pv IsNot Nothing Then
                If pv.Query <> vbNullString Then desc += " " & GisControlView.LangStr(_strIdSectionQueryParamsList) & " [ " & pv.Query & " ]"
                If pv.Render.Expression <> vbNullString Then desc += " " & GisControlView.LangStr(_strIdSectionRenderer) & " [" & pv.Render.Expression.TrimStart("[").TrimEnd("]") & " ]"
            End If
        End If

        Return desc
    End Function

    Private Sub ListSections()
        Dim i As Integer

        lstParams.Items.Clear()

        'List sections
        For i = 0 To _ll.ParamsList.Count - 1
            lstParams.Items.Add(GetSectionString(_ll.ParamsList.Items(i)))
        Next

        _noSave = True 'Don't have any old settings
        lstParams.SelectedIndex = 0 'Default section
        _noSave = False
    End Sub

    Private Sub UpdateSelectedSectionText()
        'Update section list in real-time when editing values
        If lstParams.SelectedIndex >= 0 AndAlso _noSave = False Then
            lstParams.Items.Item(lstParams.SelectedIndex) = GetSectionString()
        End If
    End Sub

    Private Shared Function ValidateScaleText(ByVal val As String) As Boolean
        'Check if entered scale string is valid (format n:n)
        Dim parts As String() = val.Split(":"c)
        If parts.GetLength(0) <> 2 Then Return False
        If Not IsNumeric(parts(0)) Or Not IsNumeric(parts(1)) Then Return False
        Return True
    End Function

    Private Shared Sub SetButtonImageWhenEnabled(ByVal btn As Button, ByVal image As String)
        'Load/unload button image
        Dim bmp As Bitmap = Nothing
        If btn.Enabled Then
            bmp = My.Resources.ResourceManager.GetObject(image)
            bmp.MakeTransparent()
        End If
        btn.BackgroundImage = bmp
    End Sub

    Private Sub DrawPreview()
        'No need to draw preview if we are not on the polygon, line, point or label tab
        If Not Tabs.SelectedIndex = 3 And Not Tabs.SelectedIndex = 4 And Not Tabs.SelectedIndex = 5 And Not Tabs.SelectedIndex = 6 Then
            pbPreview.Image = Nothing
            Exit Sub
        End If

        Try
            Dim viewerBmp As New TGIS_ViewerBmp(128, 128)

            'Create a new hidden layer
            Dim lv As New TGIS_LayerVector
            lv.Name = GisControlView.PreviewLayerName
            lv.AddField("Label", TGIS_FieldType.gisFieldTypeString, 5, 0)
            viewerBmp.Add(lv)

            'Apply parameters
            ApplySettingsArea(lv.Params)
            ApplySettingsLine(lv.Params)
            ApplySettingsMarker(lv.Params)
            ApplySettingsLabel(lv.Params)
            lv.Params.Query = "Label = 'Label'"
            lv.Params.Labels.Field = "Label"

            If Tabs.SelectedIndex = 3 Then 'Polygon
                Dim shp As TGIS_Shape
                shp = lv.CreateShape(TGIS_ShapeType.gisShapeTypePolygon)
                shp.Lock(TGIS_Lock.gisLockExtent)
                shp.SetField("Label", "Label")
                shp.AddPart()
                shp.AddPoint(New TGIS_Point(40, 40))
                shp.AddPoint(New TGIS_Point(54, 54))
                shp.AddPoint(New TGIS_Point(80, 40))
                shp.AddPoint(New TGIS_Point(80, 80))
                shp.AddPoint(New TGIS_Point(40, 80))
                shp.Unlock()
            ElseIf Tabs.SelectedIndex = 4 Then 'Line
                Dim shp As TGIS_Shape
                shp = lv.CreateShape(TGIS_ShapeType.gisShapeTypeArc)
                shp.Lock(TGIS_Lock.gisLockExtent)
                shp.SetField("Label", "Label")
                shp.AddPart()
                shp.AddPoint(New TGIS_Point(40, 64))
                shp.AddPoint(New TGIS_Point(88, 64))
                shp.Unlock()
            ElseIf Tabs.SelectedIndex = 5 Or Tabs.SelectedIndex = 6 Then 'Point and Label
                Dim shp As TGIS_Shape
                shp = lv.CreateShape(TGIS_ShapeType.gisShapeTypePoint)
                shp.Lock(TGIS_Lock.gisLockExtent)
                shp.SetField("Label", "Label")
                shp.AddPart()
                shp.AddPoint(New TGIS_Point(64, 64))
                shp.Unlock()
            End If

            'Export layer to a bitmap
            viewerBmp.Center = New TGIS_Point(lv.Extent.XMax - lv.Extent.XMin / 2, lv.Extent.YMax - lv.Extent.YMin / 2)
            viewerBmp.Draw()
            Dim myMemoryStream As New System.IO.MemoryStream()
            viewerBmp.Bitmap.Save(myMemoryStream, System.Drawing.Imaging.ImageFormat.Bmp)

            'Populate PictureBox1 with exported bitmap
            pbPreview.Image = Image.FromStream(myMemoryStream)
            myMemoryStream.Close()
            myMemoryStream.Dispose()
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub

    Private Sub DisableMarkerSettings()
        'Disable controls not available when using custom font or picture styles
        Dim enableNormal As Boolean = True, enableColor As Boolean = True

        If Not _lv.Params.Marker.Symbol Is Nothing Then 'or asdfsadf
            enableNormal = False
            If Not TryCast(_lv.Params.Marker.Symbol, TGIS_SymbolPicture) Is Nothing Then
                enableColor = False
            End If
        End If

        MarkerStyle.Enabled = enableNormal
        MarkerColor.Enabled = enableColor
        chkMarkerColorUR.Enabled = enableColor
        MarkerOutlineStyle.Enabled = enableNormal
        If enableColor = True Then
            MarkerOutlineColor.Enabled = Not chkMarkerOutlineColorUR.Checked
        End If
        chkMarkerOutlineColorUR.Enabled = enableColor
        MarkerOutlineWidth.Enabled = enableNormal
        chkMarkerOutlineWidthUR.Enabled = enableNormal
    End Sub

    Private Sub ClearSections(Optional ByVal keepFirst As Boolean = False)
        Dim pp As TGIS_ParamsSection = _ll.ParamsList(0)

        'Clear all sections
        _ll.ParamsList.Clear()
        If keepFirst Then
            _ll.ParamsList(0) = pp
        Else
            If TypeOf _ll Is TGIS_LayerVector Then
                _ll.ParamsList(0) = New TGIS_ParamsSectionVector
            ElseIf TypeOf _ll Is TGIS_LayerPixel Then
                _ll.ParamsList(0) = New TGIS_ParamsSectionPixel
            End If
        End If

        'Relist sections
        _noSave = True 'Make sure old setting aren't saved when selection is changed
        lstParams.Items.Clear()
        ListSections()
        lstParams.SelectedIndex = 0 'Default selection
        _noSave = False

        'Reload settings
        LoadCurrentSection()
    End Sub
#End Region

#Region "Event handlers"
    Private Sub LayerPropertiesDialog_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim fi As New System.IO.FileInfo(System.Reflection.Assembly.GetEntryAssembly().Location)
        Dim iconFile As String = fi.DirectoryName + "\HMSIcons.icl"
        Dim hIconSmall(0) As IntPtr

        SetupLanguage()

        'Set up event handlers
        AddHandler uxLabelPos1.CheckedChanged, AddressOf uxLabelPos_CheckedChanged
        AddHandler uxLabelPos2.CheckedChanged, AddressOf uxLabelPos_CheckedChanged
        AddHandler uxLabelPos3.CheckedChanged, AddressOf uxLabelPos_CheckedChanged
        AddHandler uxLabelPos4.CheckedChanged, AddressOf uxLabelPos_CheckedChanged
        AddHandler uxLabelPos5.CheckedChanged, AddressOf uxLabelPos_CheckedChanged
        AddHandler uxLabelPos6.CheckedChanged, AddressOf uxLabelPos_CheckedChanged
        AddHandler uxLabelPos7.CheckedChanged, AddressOf uxLabelPos_CheckedChanged
        AddHandler uxLabelPos8.CheckedChanged, AddressOf uxLabelPos_CheckedChanged
        AddHandler uxLabelPos9.CheckedChanged, AddressOf uxLabelPos_CheckedChanged

        'Load info icon
        NativeMethods.ExtractIconEx(iconFile, 20, Nothing, hIconSmall, 1)
        If hIconSmall(0) <> IntPtr.Zero Then
            _infoIcon = Icon.FromHandle(hIconSmall(0)).Clone() 'Clone here so we can destroy the native handle
            NativeMethods.DestroyIcon(hIconSmall(0))
        End If

        'Initialize tabs
        _noSave = True 'Prevent saving settings until load is finished
        SetupTabGeneral()
        If _lv IsNot Nothing Then
            ListSections()
            LoadCurrentSection()
        End If
        _noSave = False

        'Hide tabs for any unsupported shape types
        If _lv Is Nothing Then
            'Non-layervector or group, hide tabs
            Tabs.TabPages.Remove(TabSection)
            Tabs.TabPages.Remove(TabRendering)
            Tabs.TabPages.Remove(TabArea)
            Tabs.TabPages.Remove(TabLine)
            Tabs.TabPages.Remove(TabMarker)
            Tabs.TabPages.Remove(TabLabel)
            Tabs.TabPages.Remove(TabChart)
        End If
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If Caption.Text.Trim() = vbNullString Then
            MsgBox(GisControlView.LangStr(_strIdMissingTitle), MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        Me.DialogResult = DialogResult.OK

        'Apply settings
        ApplySettingsGeneral()
        ApplySettingsRelation() 'Make sure to apply relation settings first so join view settings is up-to-date when rendering settings is applied
        SaveCurrentSection()

        'Save any changes
        If _ll IsNot Nothing Then _ll.SaveAll()
        _gisCtrl.SaveProject() 'Group settings

        Me.Close()
    End Sub

    Private Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        'Save settings
        ApplySettingsGeneral()
        ApplySettingsRelation()
        SaveCurrentSection()
        If _ll IsNot Nothing Then
            _ll.SaveAll()

            'Update viewer, make sure active parameter remains the same
            _gisCtrl.Update()
            _ll.ParamsList.Selected = lstParams.SelectedIndex
        End If

        'Update legend
        GisControlView.Instance.ControlLegend.Update()
        GisControlView.Instance.ControlHierarchy.Update()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If _ll IsNot Nothing Then _ll.RevertAll()
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnWizard_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnWizard.Click
        Dim jv As JoinView = Nothing
        If TypeOf View.SelectedItem Is JoinView Then jv = View.SelectedItem

        'Make sure any changes regarding views are saved first (#4497)
        ApplySettingsRelation()
        If _ll IsNot Nothing Then _ll.SaveAll()

        Dim dlg As New RenderingWizard(_ll, jv)
        If dlg.ShowDialog() = Windows.Forms.DialogResult.OK Then
            'Reload properties
            ListSections()
            LoadCurrentSection()
        End If
    End Sub

    Private Sub btnProjection_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProjection.Click
        Dim dlg As New CoordinateSystemDialog(_ll.CS)
        If dlg.ShowDialog() = Windows.Forms.DialogResult.OK Then
            'Apply selected coordinate system
            _ll.CS = dlg.CS
            Projection.Text = _ll.CS.WKT

            'Apply to any sublayers
            If _ll.SubLayers IsNot Nothing Then
                For i As Integer = 0 To _ll.SubLayers.Count - 1
                    _ll.SubLayers(i).CS = _ll.CS
                Next
            End If
        End If
    End Sub

    Private Sub Path_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Path.KeyPress
        'Disable editing in path field
        e.KeyChar = vbNullChar
    End Sub

    Private Sub btnRestructure_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRestructure.Click
        'Show restructure dialog for vector layers
        If TypeOf _ll Is TGIS_LayerVector Then
            Dim dlg As New RestructureDialog(_ll)
            dlg.ShowDialog()
        End If
    End Sub

    Private Sub Relation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Relation.SelectedIndexChanged
        EnableControls()

        Dim idx As Integer = -1 'Default view index

        View.Items.Clear()
        View.Items.Add("") 'Add an empty item to enable deselection

        If Relation.SelectedItem IsNot Nothing AndAlso TypeOf Relation.SelectedItem Is ValueDescriptionPair Then
            'List avaliable views for this relation
            Dim i As Integer
            For i = 0 To GisControlView.Views.Count - 1
                If GisControlView.Views(i).TypeOfRelation = CType(Relation.SelectedItem, ValueDescriptionPair).Value AndAlso GisControlView.Views(i).JoinDT IsNot Nothing Then
                    View.Items.Add(GisControlView.Views(i))
                    If _joinView = GisControlView.Views(i).Name Then idx = View.Items.Count - 1 'Select this item if matching current value
                End If
            Next
        End If

        'Select first item and enable control when values are avaliable
        If View.Items.Count > 1 Then
            View.SelectedIndex = idx
            View.Enabled = True
        Else
            View.Items.Clear() 'No options, remove empty record
            View.Enabled = False
        End If

        UpdateJoinView()
    End Sub

    Private Sub View_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles View.SelectedIndexChanged
        UpdateJoinView()
    End Sub

    Private Sub RenderField_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RenderField.SelectedIndexChanged
        EnableControls()
        UpdateSelectedSectionText()
    End Sub

    Private Sub RenderZones_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RenderZones.ValueChanged
        EnableControls()
    End Sub

    Private Sub RenderZonesEx_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RenderZonesEx.ValueChanged
        EnableControls()
    End Sub

    Private Sub chkAreaColorUR_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAreaColorUR.CheckedChanged
        AreaColor.Enabled = Not chkAreaColorUR.Checked
        DrawPreview()
    End Sub

    Private Sub chkAreaOutlineColorUR_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAreaOutlineColorUR.CheckedChanged
        AreaOutlineColor.Enabled = Not chkAreaOutlineColorUR.Checked
        DrawPreview()
    End Sub

    Private Sub chkAreaOutlineWidthUR_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAreaOutlineWidthUR.CheckedChanged
        AreaOutlineWidth.Enabled = Not chkAreaOutlineWidthUR.Checked
        DrawPreview()
    End Sub

    Private Sub chkLineColorUR_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkLineColorUR.CheckedChanged
        LineColor.Enabled = Not chkLineColorUR.Checked
        DrawPreview()
    End Sub

    Private Sub chkLineWidthUR_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkLineWidthUR.CheckedChanged
        LineWidth.Enabled = Not chkLineWidthUR.Checked
        DrawPreview()
    End Sub

    Private Sub chkLineoutLineColorUR_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkLineOutlineColorUR.CheckedChanged
        LineOutlineColor.Enabled = Not chkLineOutlineColorUR.Checked
        DrawPreview()
    End Sub

    Private Sub chkLineOutlineWidthUR_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkLineOutlineWidthUR.CheckedChanged
        LineOutlineWidth.Enabled = Not chkLineOutlineWidthUR.Checked
        DrawPreview()
    End Sub

    Private Sub chkMarkerSizeUR_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMarkerSizeUR.CheckedChanged
        MarkerSize.Enabled = Not chkMarkerSizeUR.Checked
        DrawPreview()
    End Sub

    Private Sub chkMarkerColorUR_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMarkerColorUR.CheckedChanged
        MarkerColor.Enabled = Not chkMarkerColorUR.Checked
        DrawPreview()
    End Sub

    Private Sub chkMarkerOutlineColorUR_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMarkerOutlineColorUR.CheckedChanged
        MarkerOutlineColor.Enabled = Not chkMarkerOutlineColorUR.Checked
        DrawPreview()
    End Sub

    Private Sub chkMarkerOutlineWidthUR_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMarkerOutlineWidthUR.CheckedChanged
        MarkerOutlineWidth.Enabled = Not chkMarkerOutlineWidthUR.Checked
        DrawPreview()
    End Sub

    Private Sub chkChartSizeUR_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkChartSizeUR.CheckedChanged
        ChartSize.Enabled = Not chkChartSizeUR.Checked
    End Sub

    Private Sub RenderMinValue_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles RenderMinValue.KeyPress
        e.KeyChar = FilterChar(e.KeyChar)
    End Sub

    Private Sub RenderMaxValue_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles RenderMaxValue.KeyPress
        e.KeyChar = FilterChar(e.KeyChar)
    End Sub

    Private Sub RenderMinValueEx_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles RenderMinValueEx.KeyPress
        e.KeyChar = FilterChar(e.KeyChar)
    End Sub

    Private Sub RenderMaxValueEx_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles RenderMaxValueEx.KeyPress
        e.KeyChar = FilterChar(e.KeyChar)
    End Sub

    Private Sub RenderDefaultColor_Changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles RenderDefaultColor.Changed
        RenderDefaultColorEx.Color = RenderDefaultColor.Color
    End Sub

    Private Sub RenderDefaultColorEx_Changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles RenderDefaultColorEx.Changed
        RenderDefaultColor.Color = RenderDefaultColorEx.Color
    End Sub

    Private Sub RenderDefaultSize_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RenderDefaultSize.ValueChanged
        RenderDefaultSizeEx.Value = RenderDefaultSize.Value
    End Sub

    Private Sub RenderDefaultSizeEx_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RenderDefaultSizeEx.ValueChanged
        RenderDefaultSize.Value = RenderDefaultSizeEx.Value
    End Sub

    Private Sub AreaColor_Changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles AreaColor.Changed
        DrawPreview()
    End Sub

    Private Sub AreaOutlineColor_Changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles AreaOutlineColor.Changed
        DrawPreview()
    End Sub

    Private Sub AreaOutlineStyle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AreaOutlineStyle.SelectedIndexChanged
        DrawPreview()
    End Sub

    Private Sub AreaOutlineWidth_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AreaOutlineWidth.ValueChanged
        DrawPreview()
    End Sub

    Private Sub AreaPattern_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AreaPattern.SelectedIndexChanged
        DrawPreview()
    End Sub

    Private Sub LineColor_Changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles LineColor.Changed
        DrawPreview()
    End Sub

    Private Sub LineStyle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles LineStyle.SelectedIndexChanged
        DrawPreview()
    End Sub

    Private Sub LineWidth_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles LineWidth.ValueChanged
        DrawPreview()
    End Sub

    Private Sub LineOutlineColor_Changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles LineOutlineColor.Changed
        DrawPreview()
    End Sub

    Private Sub LineOutlineStyle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles LineOutlineStyle.SelectedIndexChanged
        DrawPreview()
    End Sub

    Private Sub LineOutlineWidth_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles LineOutlineWidth.ValueChanged
        DrawPreview()
    End Sub

    Private Sub MarkerColor_Changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MarkerColor.Changed
        DrawPreview()
    End Sub

    Private Sub MarkerOutlineColor_Changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MarkerOutlineColor.Changed
        DrawPreview()
    End Sub

    Private Sub MarkerOutlineStyle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles MarkerOutlineStyle.SelectedIndexChanged
        DrawPreview()
    End Sub

    Private Sub MarkerOutlineWidth_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles MarkerOutlineWidth.ValueChanged
        DrawPreview()
    End Sub

    Private Sub MarkerSize_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles MarkerSize.ValueChanged
        DrawPreview()
    End Sub

    Private Sub MarkerStyle_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MarkerStyle.SelectedIndexChanged
        If CType(MarkerStyle.SelectedItem, ValueDescriptionPair).Value = TGIS_MarkerStyle.gisMarkerStyleCross Then
            'Set min value for cross, smaller size won't show
            MarkerSize.Minimum = 113
        Else
            MarkerSize.Minimum = 0
        End If

        DrawPreview()
    End Sub

    Private Sub LabelColor_Changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles LabelColor.Changed
        DrawPreview()
    End Sub

    Private Sub LabelField_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles LabelField.SelectedIndexChanged
        DrawPreview()
    End Sub

    Private Sub LabelRotation_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles LabelRotation.ValueChanged
        DrawPreview()
    End Sub

    Private Sub LabelValue_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles LabelValue.TextChanged
        If LabelValue.Text = vbNullString Then
            LabelField.Enabled = True
        Else
            LabelField.Enabled = False
        End If

        DrawPreview()
    End Sub

    Private Sub lblLabelInfo_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles lblLabelInfo.Paint
        If _infoIcon IsNot Nothing Then e.Graphics.DrawIconUnstretched(_infoIcon, New Rectangle(0, 0, lblLabelInfo.Width, lblLabelInfo.Height))
    End Sub

    Private Sub btnFont_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLabelFont.Click
        FontDialog.Font = btnLabelFont.Font
        If FontDialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
            btnLabelFont.Font = FontDialog.Font
            btnLabelFont.Text = FontDialog.Font.Name
        End If

        DrawPreview()
    End Sub

    Private Sub chkLabelColorUR_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkLabelColorUR.CheckedChanged
        LabelColor.Enabled = Not chkLabelColorUR.Checked
        DrawPreview()
    End Sub

    Private Sub chkLabelVisible_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkLabelVisible.CheckedChanged
        DrawPreview()
    End Sub

    Private Sub uxLabelPos_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        DrawPreview()
    End Sub

    Private Sub btnParamsAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnParamsAdd.Click
        SaveCurrentSection() 'Save properties before current param is changed

        'Add new section
        _ll.ParamsList.Add()
        lstParams.Items.Add(GetSectionString(_ll.Params))
        _noSave = True 'Don't save when changing, current param has been changed
        lstParams.SelectedIndex = lstParams.Items.Count - 1 'Select this new section
        _noSave = False
    End Sub

    Private Sub btnParamsRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnParamsRemove.Click
        'Remove selected section
        If lstParams.SelectedIndex > 0 Then
            _ll.ParamsList.Delete()
            _noSave = True 'Change selection, make sure current setting aren't saved
            lstParams.SelectedIndex -= 1
            _noSave = False
            lstParams.Items.RemoveAt(lstParams.SelectedIndex + 1)
        End If
    End Sub

    Private Sub btnParamsRemoveAll_MouseUp(ByVal sender As Object, ByVal e As Windows.Forms.MouseEventArgs) Handles btnParamsRemoveAll.MouseUp
        If e.Button = Windows.Forms.MouseButtons.Left Then RemoveParamsMenu.Show(sender, New Point(e.X, e.Y))
    End Sub

    Private Sub lstParams_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstParams.SelectedIndexChanged
        'Apply settings, change selected section
        If Not _noSave Then
            SaveCurrentSection()
        End If
        _ll.ParamsList.Selected = lstParams.SelectedIndex
        LoadCurrentSection()

        'Enable/disable remove section button
        If lstParams.SelectedIndex > 0 Then
            btnParamsRemove.Enabled = True
        Else
            btnParamsRemove.Enabled = False
        End If
    End Sub

    Private Sub btnParamsAdd_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnParamsAdd.EnabledChanged
        SetButtonImageWhenEnabled(btnParamsAdd, "Add")
    End Sub

    Private Sub btnParamsRemove_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnParamsRemove.EnabledChanged
        SetButtonImageWhenEnabled(btnParamsRemove, "Remove")
    End Sub

    Private Sub btnParamsRemoveAll_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnParamsRemoveAll.EnabledChanged
        SetButtonImageWhenEnabled(btnParamsRemoveAll, "Delete")
    End Sub

    Private Sub btnSectionCurrentScaleMin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSectionCurrentScaleMin.Click
        SectionMinScale.Text = "1:" & Math.Round(1.0 / _gisCtrl.ScaleAsFloat * 1.1)
    End Sub

    Private Sub btnSectionCurrentScaleMax_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSectionCurrentScaleMax.Click
        SectionMaxScale.Text = "1:" & Math.Round(1.0 / _gisCtrl.ScaleAsFloat * 0.9)
    End Sub

    Private Sub btnSectionClearMinScale_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSectionClearMinScale.Click
        SectionMinScale.Text = vbNullString
    End Sub

    Private Sub btnSectionClearMaxScale_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSectionClearMaxScale.Click
        SectionMaxScale.Text = vbNullString
    End Sub

    Private Sub SectionMinScale_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles SectionMinScale.KeyPress
        If Not Char.IsControl(e.KeyChar) AndAlso (Asc(e.KeyChar) < 48 OrElse Asc(e.KeyChar) > 57) AndAlso e.KeyChar <> ":"c Then e.KeyChar = vbNullChar
    End Sub

    Private Sub SectionMaxScale_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles SectionMaxScale.KeyPress
        If Not Char.IsControl(e.KeyChar) AndAlso (Asc(e.KeyChar) < 48 OrElse Asc(e.KeyChar) > 57) AndAlso e.KeyChar <> ":"c Then e.KeyChar = vbNullChar
    End Sub

    Private Sub SectionMinScale_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SectionMinScale.TextChanged
        'Validate value
        If ValidateScaleText(SectionMinScale.Text) Then
            SectionMinScale.ForeColor = Color.Black
        Else
            SectionMinScale.ForeColor = Color.Red
        End If

        'Enable/disable clear button
        If SectionMinScale.Text <> vbNullString Then
            btnSectionClearMinScale.Enabled = True
        Else
            btnSectionClearMinScale.Enabled = False
        End If
        SetButtonImageWhenEnabled(btnSectionClearMinScale, "Delete")

        UpdateSelectedSectionText()
    End Sub

    Private Sub SectionMaxScale_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SectionMaxScale.TextChanged
        'Validate value
        If ValidateScaleText(SectionMaxScale.Text) Then
            SectionMaxScale.ForeColor = Color.Black
        Else
            SectionMaxScale.ForeColor = Color.Red
        End If

        'Enable/disable clear button
        If SectionMaxScale.Text <> vbNullString Then
            btnSectionClearMaxScale.Enabled = True
        Else
            btnSectionClearMaxScale.Enabled = False
        End If
        SetButtonImageWhenEnabled(btnSectionClearMaxScale, "Delete")

        UpdateSelectedSectionText()
    End Sub

    Private Sub chkSectionVisible_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSectionVisible.CheckedChanged
        UpdateSelectedSectionText()
    End Sub

    Private Sub SectionQuery_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SectionQuery.TextChanged
        Dim s As Integer = SectionQuery.SelectionStart 'Make sure cursor positing remains the same during update
        UpdateSelectedSectionText()
        SectionQuery.SelectionStart = s
    End Sub

    Private Sub btnMarkerSymbol_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMarkerSymbol.Click
        Dim ps As TGIS_ParamsSectionVector = Nothing
        ps = TryCast(_lv.ParamsList(_lv.ParamsList.Selected), TGIS_ParamsSectionVector)

        'Load the dialog with the current picture symbol, if any
        Dim dlgPicture As New MarkerSymbolDialog(ps.Marker.Symbol)

        'Show the picture chooser dialog
        If dlgPicture.ShowDialog() = Windows.Forms.DialogResult.OK Then
            'Set picture symbol
            If Not dlgPicture.Symbology Is Nothing Then
                Dim sp As TGIS_SymbolPicture
                Dim sf As TGIS_SymbolFont

                sp = TryCast(dlgPicture.Symbology, TGIS_SymbolPicture)
                If sp Is Nothing Then
                    sf = TryCast(dlgPicture.Symbology, TGIS_SymbolFont)
                    _lv.Params.Marker.Symbol = sf
                Else
                    _lv.Params.Marker.Symbol = sp
                End If
            Else
                _lv.Params.Marker.Symbol = dlgPicture.Symbology
            End If
        End If

        DrawPreview()
    End Sub

    Private Sub Tabs_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Tabs.SelectedIndexChanged
        DrawPreview()
    End Sub

    Private Sub AlphaChannel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AlphaChannel.SelectedIndexChanged
        'Change which band to use as alpha channel
        If TypeOf _ll Is TGIS_LayerPixel Then
            Dim pp As TGIS_ParamsPixel
            pp = CType(_ll, TGIS_LayerPixel).Params.Pixel

            If pp IsNot Nothing Then
                pp.AlphaBand = AlphaChannel.SelectedIndex - 1
            End If

            'Apply to any pixel sublayers
            If _ll.SubLayers IsNot Nothing Then
                For i As Integer = 0 To _ll.SubLayers.Count - 1
                    If TypeOf _ll.SubLayers(i).Params Is TGIS_ParamsSectionPixel Then
                        _ll.SubLayers(i).Params.Pixel.AlphaBand = pp.AlphaBand
                    End If
                Next
            End If
        End If
    End Sub

    Private Sub RemoveAllToolStripMenuItemToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RemoveAllToolStripMenuItemToolStripMenuItem.Click
        ClearSections()
    End Sub

    Private Sub RemoveAllButFirstToolStripMenuItemToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RemoveAllButFirstToolStripMenuItemToolStripMenuItem.Click
        ClearSections(True)
    End Sub
#End Region
End Class
