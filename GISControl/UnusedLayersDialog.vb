﻿Imports System.Data.OleDb
Imports TatukGIS.NDK
Imports XtremeReportControl

Public Class UnusedLayersDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 2100
    Private Const _strIdOK As Integer = 2101
    Private Const _strIdCancel As Integer = 2102
    Private Const _strIdCaption As Integer = 2103
    Private Const _strIdType As Integer = 2104
    Private Const _strIdNative As Integer = 2105
    Private Const _strIdPixelStore2 As Integer = 2106
    Private Const _strIdCheckUncheckAll As Integer = 2107
#End Region

#Region "Declarations"
    Private Const _checkIconId As Integer = 1
#End Region

#Region "Helper functions"
    Private Sub AddRecord(ByVal name As String, ByVal caption As String, ByVal epsg As Integer, ByVal type As String)
        Dim rec As ReportRecord = uxLayers.Records.Add()
        Dim item As ReportRecordItem = rec.AddItem(vbNullString) : item.HasCheckbox = True
        rec.AddItem(caption)
        rec.AddItem(type)
        rec.AddItem(epsg)
        rec.Tag = name
    End Sub

    Private Sub UpdateButtons()
        'Enable OK button only when items are checked
        uxOK.Enabled = False
        For i As Integer = 0 To uxLayers.Records.Count - 1
            If uxLayers.Records(i).Item(0).Checked Then
                uxOK.Enabled = True
                Exit For
            End If
        Next
    End Sub
#End Region

#Region "Event handlers"
    Private Sub UnusedLayersDialog_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Set up language
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        uxOK.Text = GisControlView.LangStr(_strIdOK)
        uxCancel.Text = GisControlView.LangStr(_strIdCancel)
        uxCheckUncheckAll.Text = GisControlView.LangStr(_strIdCheckUncheckAll)

        'Set up layer list
        uxLayers.Columns.Add(0, vbNullString, 5, True).Icon = _checkIconId
        uxLayers.Columns.Add(1, GisControlView.LangStr(_strIdCaption), 70, True)
        uxLayers.Columns.Add(2, GisControlView.LangStr(_strIdType), 20, True)
        uxLayers.Columns.Add(3, vbNullString, 0, False).Visible = False 'Hidden epsg column

        'Load checkbox image
        Dim bmp As Bitmap = My.Resources.ResourceManager.GetObject("Check")
        bmp.MakeTransparent()
        uxLayers.Icons.AddBitmap(bmp.GetHbitmap(), _checkIconId, XTPImageState.xtpImageNormal, True)


        'Query db for layers and check for layers that aren't loaded
        Dim cmd As New OleDbCommand("", GisControlView.Conn)
        Dim reader As OleDbDataReader

        'Native
        cmd.CommandText = "IF EXISTS (SELECT 1 FROM sysobjects WHERE name LIKE N'ttkGISLayerSQL' AND xtype='U') SELECT RIGHT(ttk.name,LEN(ttk.name)-4), ISNULL(caption,ttk.name), ISNULL(epsg,0) FROM ttkGISLayerSQL ttk LEFT JOIN gis_layers gl ON gl.name = RIGHT(ttk.name,LEN(ttk.name)-4)"
        reader = cmd.ExecuteReader()
        Do While reader.Read()
            If GisControlView.Instance.GisCtrl.Get(reader.GetString(0)) Is Nothing Then
                AddRecord(reader.GetString(0), reader.GetString(1), reader.GetInt32(2), GisControlView.LangStr(_strIdNative))
            End If
        Loop
        reader.Close()

        'PixelStore2
        cmd.CommandText = "IF EXISTS (SELECT 1 FROM sysobjects WHERE name LIKE N'ttkGISPixelStore2' AND xtype='U') SELECT RIGHT(ttk.name,LEN(ttk.name)-4), ISNULL(caption,ttk.name), ISNULL(epsg,0) FROM ttkGISPixelStore2 ttk LEFT JOIN gis_layers gl ON gl.name = RIGHT(ttk.name,LEN(ttk.name)-4)"
        reader = cmd.ExecuteReader()
        Do While reader.Read()
            If GisControlView.Instance.GisCtrl.Get(reader.GetString(0)) Is Nothing Then
                AddRecord(reader.GetString(0), reader.GetString(1), reader.GetInt32(2), GisControlView.LangStr(_strIdPixelStore2))
            End If
        Loop
        reader.Close()

        uxLayers.Populate()
    End Sub

    Private Sub uxOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxOK.Click
        'Add all checked layers to viewer
        For i As Integer = 0 To uxLayers.Records.Count - 1
            Dim rec As ReportRecord = uxLayers.Records(i)
            If rec.Item(0).Checked Then
                If rec.Item(2).Value = GisControlView.LangStr(_strIdNative) Then
                    'Native
                    Dim lSql As New TGIS_LayerSqlAdo()

                    'Add to viewer
                    lSql.Caption = rec.Item(1).Value
                    lSql.Name = rec.Tag
                    lSql.Path = "Storage=Native\n" & _
                                "Layer=" & GisControlView.GisTablePrefix & rec.Tag & "\n" & _
                                "Dialect=MSSQL\n" & _
                                "ADO=" & GisControlView.ConnStr & "\n" & _
                                ".ttkls"
                    lSql.ReadConfig()
                    GisControlView.Instance.GisCtrl.Add(lSql)
                    lSql.CS = TGIS_CSFactory.ByEPSG(rec.Item(3).Value)
                    lSql.ZOrder = 0 'Topmost
                Else
                    'PixelStore2
                    Dim lSql As New TGIS_LayerPixelStoreAdo2

                    'Add to viewer
                    lSql.Caption = rec.Item(1).Value
                    lSql.Name = rec.Tag
                    lSql.Path = "Storage=PixelStore2\n" & _
                                "Layer=" & GisControlView.GisTablePrefix & rec.Tag & "\n" & _
                                "Dialect=MSSQL\n" & _
                                "ADO=" & GisControlView.ConnStr & "\n" & _
                                ".ttkps"
                    GisControlView.Instance.GisCtrl.Add(lSql)
                    lSql.CS = TGIS_CSFactory.ByEPSG(rec.Item(3).Value)
                    lSql.ZOrder = 0 'Topmost
                End If
            End If
        Next

        'Save any changes
        GisControlView.Instance.GisCtrl.SaveProject()

        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub uxLayers_ItemCheck(ByVal sender As Object, ByVal e As AxXtremeReportControl._DReportControlEvents_ItemCheckEvent) Handles uxLayers.ItemCheck
        UpdateButtons()
    End Sub

    Private Sub uxMarkUnmarkAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxCheckUncheckAll.Click
        Dim checkState As Boolean = False
        Dim i As Integer

        'Check if any records are unchecked
        For i = 0 To uxLayers.Records.Count - 1
            If Not uxLayers.Records(i).Item(0).Checked Then
                checkState = True
                Exit For
            End If
        Next

        'Check/uncheck all records
        For i = 0 To uxLayers.Records.Count - 1
            uxLayers.Records(i).Item(0).Checked = checkState
        Next

        uxLayers.Populate()
        UpdateButtons()
    End Sub
#End Region
End Class