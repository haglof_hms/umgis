﻿Imports TatukGIS.NDK
Imports XtremeReportControl

Public Class CoordinateSystemDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 1200
    Private Const _strIdOK As Integer = 1201
    Private Const _strIdCancel As Integer = 1202
    Private Const _strIdProjectedCS As Integer = 1203
    Private Const _strIdGeographicCS As Integer = 1204
    Private Const _strIdNone As Integer = 1205
    Private Const _strIdAll As Integer = 1206
    Private Const _strIdSweden As Integer = 1207
    Private Const _strIdFinland As Integer = 1208
    Private Const _strIdNorway As Integer = 1209
    Private Const _strIdUsa As Integer = 1210
    Private Const _strIdWorld As Integer = 1211
    Private Const _strIdFilter As Integer = 1212
#End Region

#Region "Declarations"
    Private _cs As TGIS_CSCoordinateSystem

    Public Sub New(ByVal cs As TGIS_CSCoordinateSystem)
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _cs = cs
    End Sub

    Public ReadOnly Property CS() As TGIS_CSCoordinateSystem
        Get
            Return _cs
        End Get
    End Property
#End Region

#Region "Helper functions"
    Private Sub AddCoordinateSystem(ByRef cs As TGIS_CSAbstract)
        'Helper function (should only by called from ListCoordinateSystems)
        Dim rec As ReportRecord
        If cs IsNot Nothing Then
            rec = uxCSReport.Records.Add()
            rec.AddItem(cs.Description)
            rec.AddItem(cs.EPSG)
            rec.Tag = cs.EPSG
        End If
    End Sub

    Private Sub ListCoordinateSystems()
        Dim i As Integer
        Dim epsg As Integer
        Dim cs As TGIS_CSAbstract

        'List coordinate systems in category, select current CS (if avaliable)
        uxCSReport.Records.DeleteAll()
        If uxPCSOption1.Checked Then
            If uxPCS.SelectedItem Is Nothing Then Exit Sub

            If CType(uxPCS.SelectedItem, ArrayDescriptionPair).Values.GetLength(0) > 0 Then
                'List coordinate systems specified in array
                For Each epsg In CType(uxPCS.SelectedItem, ArrayDescriptionPair).Values
                    cs = TGIS_Utils.CSProjectedCoordinateSystemList.ByEPSG(epsg)
                    AddCoordinateSystem(cs)
                Next
            Else
                'List all avaliable projected coordinate systems
                For i = 0 To TGIS_Utils.CSProjectedCoordinateSystemList.Count() - 1
                    cs = TGIS_Utils.CSProjectedCoordinateSystemList(i)
                    AddCoordinateSystem(cs)
                Next
            End If
        ElseIf uxGCSOption.Checked Then
            If uxGCS.SelectedItem Is Nothing Then Exit Sub

            If CType(uxGCS.SelectedItem, ArrayDescriptionPair).Values.GetLength(0) > 0 Then
                'List coordinate systems specified in array
                For Each epsg In CType(uxGCS.SelectedItem, ArrayDescriptionPair).Values
                    cs = TGIS_Utils.CSGeographicCoordinateSystemList.ByEPSG(epsg)
                    AddCoordinateSystem(cs)
                Next
            Else
                'List all avaliable geographics coordinate systems
                For i = 0 To TGIS_Utils.CSGeographicCoordinateSystemList.Count() - 1
                    cs = TGIS_Utils.CSGeographicCoordinateSystemList(i)
                    AddCoordinateSystem(cs)
                Next
            End If
        End If

        UpdateReport(True)
    End Sub

    Private Sub UpdateForm()
        uxPCS.Enabled = False
        uxGCS.Enabled = False
        uxCSReport.Enabled = False

        'Enable appropriate controls
        If uxPCSOption1.Checked Then
            uxPCS.Enabled = True
            uxCSReport.Enabled = True
        ElseIf uxGCSOption.Checked Then
            uxGCS.Enabled = True
            uxCSReport.Enabled = True
        End If

        'Update coordinate system list
        ListCoordinateSystems()
    End Sub

    Private Sub UpdateReport(Optional ByVal selectCurrent As Boolean = False)
        uxCSReport.Populate()

        'Select current coordinate system
        If selectCurrent AndAlso _cs IsNot Nothing Then
            For Each row As ReportRow In uxCSReport.Rows
                If row.Record.Tag = _cs.EPSG Then
                    uxCSReport.Navigator.MoveToRow(row.Index)
                    row.Selected = True
                    Exit For
                End If
            Next
        End If
    End Sub
#End Region

#Region "Event handlers"
    Private Sub ProjectionSettingsDialog_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim epsg As Integer()

        'Set up languange dependent strings
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        uxPCSOption1.Text = GisControlView.LangStr(_strIdProjectedCS)
        uxGCSOption.Text = GisControlView.LangStr(_strIdGeographicCS)
        uxOK.Text = GisControlView.LangStr(_strIdOK)
        uxCancel.Text = GisControlView.LangStr(_strIdCancel)
        uxFilterLabel.Text = GisControlView.LangStr(_strIdFilter)

        'Set up report
        uxCSReport.Columns.Add(0, "Name", 70, True)
        uxCSReport.Columns.Add(1, "EPSG", 30, True)

        'List categories
        'Projected coordinate systems
        epsg = New Integer() {3006, 3021}
        uxPCS.Items.Add(New ArrayDescriptionPair(epsg, GisControlView.LangStr(_strIdSweden)))

        epsg = New Integer() {2393, 3386, 2391, 2392, 2394, 3387}
        uxPCS.Items.Add(New ArrayDescriptionPair(epsg, GisControlView.LangStr(_strIdFinland)))

        epsg = New Integer() {27391, 27392, 27393, 27394, 27395, 27396, 27397, 27398}
        uxPCS.Items.Add(New ArrayDescriptionPair(epsg, GisControlView.LangStr(_strIdNorway)))

        epsg = New Integer() {}
        uxPCS.Items.Add(New ArrayDescriptionPair(epsg, GisControlView.LangStr(_strIdAll)))

        'Geographics coordinate systems
        epsg = New Integer() {4619, 4124}
        uxGCS.Items.Add(New ArrayDescriptionPair(epsg, GisControlView.LangStr(_strIdSweden)))

        epsg = New Integer() {4123}
        uxGCS.Items.Add(New ArrayDescriptionPair(epsg, GisControlView.LangStr(_strIdFinland)))

        epsg = New Integer() {4273, 4817}
        uxGCS.Items.Add(New ArrayDescriptionPair(epsg, GisControlView.LangStr(_strIdNorway)))

        epsg = New Integer() {4269, 4152, 4267}
        uxGCS.Items.Add(New ArrayDescriptionPair(epsg, GisControlView.LangStr(_strIdUsa)))

        epsg = New Integer() {4326, 4322, 4324, 4760, 4740, 4276}
        uxGCS.Items.Add(New ArrayDescriptionPair(epsg, GisControlView.LangStr(_strIdWorld)))

        epsg = New Integer() {}
        uxGCS.Items.Add(New ArrayDescriptionPair(epsg, GisControlView.LangStr(_strIdAll)))

        'Select default category, list projections
        uxPCS.SelectedIndex = uxPCS.Items.Count - 1
        uxGCS.SelectedIndex = uxGCS.Items.Count - 1

        'Select current category, this will automatically update the coordinate system list
        If TypeOf _cs Is TGIS_CSProjectedCoordinateSystem Then
            uxPCSOption1.Checked = True
        ElseIf TypeOf _cs Is TGIS_CSGeographicCoordinateSystem Then
            uxGCSOption.Checked = True
        Else 'Default to geographics coordinate system
            uxGCSOption.Checked = True
        End If
    End Sub

    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxOK.Click
        'Apply selected coordinate system on layer
        If uxCSReport.SelectedRows.Count > 0 Then
            _cs = TGIS_CSFactory.ByEPSG(uxCSReport.SelectedRows(0).Record.Tag)
        End If

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub cmbPCS_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxPCS.SelectedIndexChanged
        ListCoordinateSystems()
    End Sub

    Private Sub cmbGCS_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxGCS.SelectedIndexChanged
        ListCoordinateSystems()
    End Sub

    Private Sub chkPCS_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxPCSOption1.CheckedChanged
        UpdateForm()
    End Sub

    Private Sub chkGCS_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxGCSOption.CheckedChanged
        UpdateForm()
    End Sub

    Private Sub uxFilterText_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxFilterText.TextChanged
        uxCSReport.FilterText = uxFilterText.Text
        UpdateReport()
    End Sub
#End Region
End Class
