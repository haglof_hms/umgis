Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("GISControl")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Haglöf Sweden AB")> 
<Assembly: AssemblyProduct("GISControl")> 
<Assembly: AssemblyCopyright("Copyright © Haglöf Sweden AB")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("e30e4111-4ece-44ff-9ab6-6cb58e6c2c75")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("2.16.3.6183")> 
<Assembly: AssemblyFileVersion("2.16.3.6183")> 
