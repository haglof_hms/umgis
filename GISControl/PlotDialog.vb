﻿Imports TatukGIS.NDK
Imports System.Globalization
Imports System.Math
Imports System.Data.OleDb

Public Class PlotDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 600
    Private Const _strIdDistance As Integer = 601
    Private Const _strIdDensity As Integer = 602
    Private Const _strIdMethod As Integer = 603
    Private Const _strIdUseGrid As Integer = 604
    Private Const _strIdRandomize As Integer = 605
    Private Const _strIdOK As Integer = 606
    Private Const _strIdCancel As Integer = 607
    Private Const _strIdAngle As Integer = 608
    Private Const _strIdHideOutside As Integer = 609
    Private Const _strIdDestinationLayer As Integer = 610
    Private Const _strIdNewLayer As Integer = 611
    Private Const _strIdUpdate As Integer = 612
    Private Const _strIdConfirmAbortCreatePlots As Integer = 613
    Private Const _strIdMinDistance As Integer = 614
    Private Const _strIdNoiseFactor As Integer = 615
    Private Const _strIdOffset As Integer = 616
    Private Const _strIdMaskLayers As Integer = 617
    Private Const _strIdUnits As Integer = 618
#End Region

#Region "Declarations"
    Private _sourceLayer As TGIS_LayerVector
    Private _sourceShapeUid As Integer
    Private _sourceNames As List(Of ValueDescriptionPair)
    Private _gisCtrl As TatukGIS.NDK.WinForms.TGIS_ViewerWnd
    Private _compassDir As New Point
    Private _angleAutoUpdate As Boolean
    Private _firstUpdateDone As Boolean
    Private _progress As ProgressDialog
    Private _units As TGIS_CSUnits
    Private _distanceX As Double, _distanceY As Double
    Private _numPlots As Integer
    Private _angle As Double
    Private _noise As Double
    Private _minDistance As Double
    Private _offset As Double

    Private ReadOnly _compassPos As New Point(154, 100)
    Private ReadOnly _compassSize As Integer = 40
    Private ReadOnly _compassCenter As New Point(_compassPos.X + _compassSize / 2, _compassPos.Y + _compassSize / 2)

    Public Sub New(ByVal sourceLayer As TGIS_LayerVector, ByVal shapeUid As Integer, ByVal objectNames As List(Of ValueDescriptionPair))
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _sourceLayer = sourceLayer
        _sourceShapeUid = shapeUid
        _sourceNames = objectNames
        _gisCtrl = GisControlView.Instance.GisCtrl 'Shorthand
    End Sub
#End Region

#Region "Helper functions"
    Private Sub AddUnit(ByVal u As TGIS_CSUnits)
        Dim idx As Integer = cmbUnits.Items.Add(New ValueDescriptionPair(u.EPSG, u.Description))
        If u.EPSG = GisControlView.Instance.ControlScale.Units.EPSG Then cmbUnits.SelectedIndex = idx
    End Sub

    Private Sub UpdateForm()
        'Enable/disable fields
        If btnUseGrid.Checked Then
            txtDistanceX.Enabled = True
            txtDistanceY.Enabled = True
            txtAngle.Enabled = True
            txtNoise.Enabled = True
            txtNumPlots.Enabled = False
            txtMinDistance.Enabled = False
            chkHideOutside.Enabled = True

            btnOK.Enabled = (_distanceX > 0 AndAlso _distanceY > 0)
        Else
            txtDistanceX.Enabled = False
            txtDistanceY.Enabled = False
            txtAngle.Enabled = False
            txtNoise.Enabled = False
            txtNumPlots.Enabled = True
            txtMinDistance.Enabled = True
            chkHideOutside.Enabled = False

            btnOK.Enabled = (_numPlots > 0)
        End If
        Me.Invalidate() 'Redraw angle
    End Sub

    Private Sub UpdateDirection(ByVal pt As Point)
        Dim tempX As Integer
        Dim tempY As Integer
        'Move line edge to cursor position
        tempX = _compassDir.X
        tempY = _compassDir.Y

        _compassDir.X = pt.X
        _compassDir.Y = pt.Y

        'Normalize compass line
        Dim r As Double = _compassSize / 2.0
        Dim len As Double = Sqrt((_compassDir.X - _compassCenter.X) * (_compassDir.X - _compassCenter.X) + (_compassDir.Y - _compassCenter.Y) * (_compassDir.Y - _compassCenter.Y))

        'Added check when pulling compass pointer to center then the length can be 0 20100819 JÖ
        If len = 0 OrElse r = 0 Then
            _compassDir.X = tempX
            _compassDir.Y = tempY
            Me.Invalidate()
        Else
            'Scale compass line to length r
            _compassDir.X = _compassCenter.X + (_compassDir.X - _compassCenter.X) / len * r
            _compassDir.Y = _compassCenter.Y + (_compassDir.Y - _compassCenter.Y) / len * r

            'Calculate angle (using adjacent/hypotenuse)
            Dim val As Double = Acos((_compassDir.X - _compassCenter.X) / Sqrt((_compassDir.X - _compassCenter.X) * (_compassDir.X - _compassCenter.X) + (_compassDir.Y - _compassCenter.Y) * (_compassDir.Y - _compassCenter.Y))) / PI * 180
            If Not Double.IsNaN(val) Then
                _angleAutoUpdate = True

                'Use azimuth angles (ie rotate vector so north is 0 deg, east 90 deg, etc.)
                If _compassDir.Y < _compassCenter.Y Then
                    '270 - 90 deg
                    val = Round(90 - val) : If val < 0 Then val += 360
                    txtAngle.Text = val
                Else
                    '90 - 270 deg
                    txtAngle.Text = Round(90 + val)
                End If
                _angleAutoUpdate = False
                Me.Invalidate()
            End If
        End If
    End Sub

    Private Sub UpdatePreview()
        Dim ll As TGIS_LayerVector

        If _gisCtrl Is Nothing Then Exit Sub 'If called from constructor

        'Fill preview layer
        ll = GisControlView.GetPreviewLayer()
        ll.RevertShapes() 'Remove any existing shapes
        If ll.FindField(GisControlView.FieldNamePlotRow) = -1 Then ll.AddField(GisControlView.FieldNamePlotRow, TGIS_FieldType.gisFieldTypeNumber, 0, 0)
        If ll.FindField(GisControlView.FieldNamePlotColumn) = -1 Then ll.AddField(GisControlView.FieldNamePlotColumn, TGIS_FieldType.gisFieldTypeNumber, 0, 0)
        If ll.FindField(GisControlView.FieldNamePlotRelation) = -1 Then ll.AddField(GisControlView.FieldNamePlotRelation, TGIS_FieldType.gisFieldTypeString, 128, 0)
        CreatePlots(ll, chkHideOutside.Checked)
        _firstUpdateDone = True
    End Sub

    Private Function IsPointTooCloseToShape(ByVal previewLayer As TGIS_LayerVector, ByVal pt As TGIS_Shape, ByVal shp As TGIS_Shape, Optional ByVal checkInside As Boolean = True) As Boolean
        Dim i As Integer, j As Integer
        Dim ptg As New TGIS_Point(pt.GetPoint(0, 0).X, pt.GetPoint(0, 0).Y)

        'Make sure point is not inside of polygon (if check should be done inside, optional because of source shape)
        If checkInside AndAlso shp.ShapeType = TGIS_ShapeType.gisShapeTypePolygon AndAlso pt.IsCommonPoint(shp) Then
            Return True
        End If

        If _offset > 0 Then
            'Check distance against each line segment (of polygon or line)
            For i = 0 To shp.GetNumParts() - 1
                For j = 1 To shp.GetPartSize(i) - 1
                    If GisControlView.GetPointToLineDistance(previewLayer.CS, ptg, shp.GetPoint(i, j - 1).X, shp.GetPoint(i, j - 1).Y, shp.GetPoint(i, j).X, shp.GetPoint(i, j).Y, _units) < _offset Then '* _units.Factor Then
                        Return True
                    End If
                Next
            Next
        End If

        Return False
    End Function

    Private Function IsPointOKWithMaskLayers(ByVal previewLayer As TGIS_LayerVector, ByVal pt As TGIS_Shape, ByVal sourceShp As TGIS_Shape, ByVal sourceLayer As TGIS_LayerVector) As Boolean
        Dim tooClose As Boolean
        Dim lt As TGIS_LayerVector
        Dim tmpshp As TGIS_Shape
        Dim dst As Double
        Dim newExtent As TGIS_Extent

        'Check distance from edges
        For Each rec As NameCaptionPair In lstMaskLayers.SelectedItems 'Loop through selected layers
            lt = _gisCtrl.Get(rec.Name)
            If lt.Name <> sourceLayer.Name Then
                'Extend shape extent with offset
                newExtent = sourceShp.ProjectedExtent

                dst = 0.01 * Abs(sourceShp.ProjectedExtent.XMin)
                newExtent.XMin = sourceShp.ProjectedExtent.XMin - dst
                dst = dst / GisControlView.GetPointToPointDistance(sourceLayer.CS, newExtent.XMin, sourceShp.ProjectedExtent.YMin, sourceShp.ProjectedExtent.XMin, sourceShp.ProjectedExtent.YMin, _units)
                If (_offset > 0) Then dst = dst * _offset
                newExtent.XMin = sourceShp.ProjectedExtent.XMin - dst

                dst = 0.01 * Abs(sourceShp.ProjectedExtent.XMax)
                newExtent.XMax = sourceShp.ProjectedExtent.XMax + dst
                dst = dst / GisControlView.GetPointToPointDistance(sourceLayer.CS, sourceShp.ProjectedExtent.XMax, sourceShp.ProjectedExtent.YMin, newExtent.XMax, sourceShp.ProjectedExtent.YMin, _units)
                If (_offset > 0) Then dst = dst * _offset
                newExtent.XMax = sourceShp.ProjectedExtent.XMax + dst

                dst = 0.01 * Abs(sourceShp.ProjectedExtent.YMin)
                newExtent.YMin = sourceShp.ProjectedExtent.YMin - dst
                dst = dst / GisControlView.GetPointToPointDistance(sourceLayer.CS, sourceShp.ProjectedExtent.XMin, newExtent.YMin, sourceShp.ProjectedExtent.XMin, sourceShp.ProjectedExtent.YMin, _units)
                If (_offset > 0) Then dst = dst * _offset
                newExtent.YMin = sourceShp.ProjectedExtent.YMin - dst

                dst = 0.01 * Abs(sourceShp.ProjectedExtent.YMax)
                newExtent.YMax = sourceShp.ProjectedExtent.YMax + dst
                dst = dst / GisControlView.GetPointToPointDistance(sourceLayer.CS, sourceShp.ProjectedExtent.XMin, sourceShp.ProjectedExtent.YMax, sourceShp.ProjectedExtent.XMin, newExtent.YMax, _units)
                If (_offset > 0) Then dst = dst * _offset
                newExtent.YMax = sourceShp.ProjectedExtent.YMax + dst

                'Check only against shapes within this extent
                tmpshp = lt.FindFirst(newExtent)
                Do Until tmpshp Is Nothing
                    Application.DoEvents()
                    If _progress.UserClosed() Then Return False 'Deadlock prevention (too time consuming)

                    If tmpshp Is Nothing OrElse tmpshp.IsDeleted OrElse (tmpshp.ShapeType <> TGIS_ShapeType.gisShapeTypePolygon AndAlso tmpshp.ShapeType <> TGIS_ShapeType.gisShapeTypeArc) Then Continue Do

                    tooClose = IsPointTooCloseToShape(previewLayer, pt, tmpshp)
                    If tooClose Then
                        Exit Do
                    End If

                    tmpshp = lt.FindNext()
                Loop
            Else
                'Source layer, check against source shape only
                tooClose = IsPointTooCloseToShape(previewLayer, pt, sourceShp, False)
            End If
            If tooClose Then Exit For
        Next

        Return Not tooClose
    End Function

    Private Sub CreatePlots(ByVal ll As TGIS_LayerVector, ByVal HideOutside As Boolean)
        Dim distanceX As Double, distanceY As Double
        Dim flipRowCol As Boolean
        Dim pointOK As Boolean
        Dim failCount As Integer
        Dim row As Integer, column As Integer
        Dim i As Integer, j As Integer
        Dim lt As TGIS_LayerVector
        Dim shp As TGIS_Shape, shpPlot As TGIS_Shape, shpTemp As TGIS_Shape
        Dim tpl As New TGIS_Topology
        Dim pt As TGIS_Shape
        Dim ptg As New TGIS_Point
        Dim x As Double, y As Double, oldY As Double
        Dim x1 As Double, y1 As Double 'min
        Dim x2 As Double, y2 As Double 'max
        Dim dx As Double, dy As Double 'delta
        Dim cx As Double, cy As Double 'center of rotated extent
        Dim ox As Double, oy As Double 'center of non-rotated extent
        Dim px As Double, py As Double 'rearranged min point to make rotated extent fit over shape
        Dim theta As Double
        Const maxTries As Integer = 10000

        shp = _sourceLayer.GetShape(_sourceShapeUid) : If shp Is Nothing Then Exit Sub

        If (btnUseGrid.Checked AndAlso (_distanceX <= 0 OrElse _distanceY <= 0)) OrElse (btnRandomize.Checked AndAlso _numPlots <= 0) Then Exit Sub

        'Create test layer (for distance check, we need this because we want to revert the layer for each cycle to save memory without interfering with preview layer)
        lt = New TGIS_LayerVector
        lt.Active = False 'Hidden
        lt.HideFromLegend = True
        lt.CS = ll.CS
        _gisCtrl.Add(lt)

        pt = ll.CreateShape(TGIS_ShapeType.gisShapeTypePoint)

        dx = shp.ProjectedExtent.XMax - shp.ProjectedExtent.XMin
        dy = shp.ProjectedExtent.YMax - shp.ProjectedExtent.YMin

        'Bring up progress bar
        _progress = New ProgressDialog
        _progress.ProgressBar.Value = 0
        _progress.Show(Me, GisControlView.LangStr(_strIdConfirmAbortCreatePlots))
        Me.Enabled = False

        'Place plots
        Cursor.Current = Cursors.WaitCursor
        If btnUseGrid.Checked Then  'Use grid
            'Create plots according to calculated grid
            Try
                'Convert back from compass azimuth to circular angle
                If _compassDir.Y < _compassCenter.Y Then
                    '270 - 90 deg, we also need to switch x/y distance
                    theta = -(_angle - 90.0) / 180.0 * PI
                    distanceX = _distanceY
                    distanceY = _distanceX
                    flipRowCol = True
                Else
                    '90 - 270 deg
                    theta = -_angle / 180.0 * PI
                    distanceX = _distanceX
                    distanceY = _distanceY
                End If
            Catch ex As InvalidCastException
            End Try

            'We need to extent the shape extent to make sure it will always cover the shape when rotated
            'Note that extension of x is based on dy and extension of y is based on dx. This way we make sure a rectangular extent will always cover the shape when rotated 90 degrees.
            x1 = shp.ProjectedExtent.XMin - 0.5 * dy
            y1 = shp.ProjectedExtent.YMin - 0.5 * dx
            x2 = shp.ProjectedExtent.XMax + 0.5 * dy
            y2 = shp.ProjectedExtent.YMax + 0.5 * dx
            dx = x2 - x1 'We must update dx, dy to represent the extended extent
            dy = y2 - y1
            cx = x1 + Cos(theta) * 0.5 * dx - Sin(theta) * 0.5 * dy 'center of rotated extent
            cy = y1 + Sin(theta) * 0.5 * dx + Cos(theta) * 0.5 * dy
            ox = x1 + (x2 - x1) / 2.0 'center of original extent
            oy = y1 + (y2 - y1) / 2.0
            px = x1 - (cx - ox) 'cx - ox gives distance from rotated center to non-rotated center
            py = y1 - (cy - oy) 'px, py is our new starting point
            x = 0

            _gisCtrl.Lock()
            Do While (x + x1) < x2 'We still use x1,x2 and y1,y2 in the loop as the distances are the same; the extent has been rotated around the center of the shape
                'Calculate distance to next point
                Dim dstX As Double
                If TypeOf ll.CS Is TGIS_CSGeographicCoordinateSystem Then
                    'Angular coordinate system, we guess the coordinates of the next point and then scale the distance
                    dstX = Abs(ptg.X) * 0.01 'Start guess
                    dstX = dstX / GisControlView.GetPointToPointDistance(ll.CS, px + Cos(theta) * (x + dstX) - Sin(theta) * oldY, py + Sin(theta) * (x + dstX) + Cos(theta) * oldY, ptg.X, ptg.Y, _units) * distanceX 'Use oldY (y has been incremented since last point)
                Else
                    dstX = distanceX / GisControlView.Instance.GetCSScaleFactor() * _units.Factor
                End If

                y = 0
                Do While (y + y1) < y2
                    'Calculate distance to next point
                    Dim dstY As Double
                    If TypeOf ll.CS Is TGIS_CSGeographicCoordinateSystem Then
                        'Angular coordinate system, we guess the coordinates of the next point and then scale the distance
                        dstY = Abs(ptg.Y) * 0.01 'Start guess
                        dstY = dstY / GisControlView.GetPointToPointDistance(ll.CS, px + Cos(theta) * x - Sin(theta) * (y + dstY), py + Sin(theta) * x + Cos(theta) * (y + dstY), ptg.X, ptg.Y, _units) * distanceY
                    Else
                        dstY = distanceY / GisControlView.Instance.GetCSScaleFactor() * _units.Factor
                    End If

                    'Update progress bar
                    Dim factor As Double
                    If Rnd() < 0.5 Then factor = -1.0 Else factor = 1.0
                    Dim tmpx As Double = x + factor * (Rnd() * dstX * _noise / 200.0) 'A value of 100 gives +/-100% so divide by 200 to normalize
                    If Rnd() < 0.5 Then factor = -1.0 Else factor = 1.0
                    Dim tmpy As Double = y + factor * (Rnd() * dstY * _noise / 200.0)
                    _progress.ProgressBar.Value = ((x * (y2 - y1))) / ((x2 - x1) * (y2 - y1)) * 100
                    ptg.X = px + Cos(theta) * tmpx - Sin(theta) * tmpy
                    ptg.Y = py + Sin(theta) * tmpx + Cos(theta) * tmpy
                    oldY = y

                    'Add point if it complies with mask layers and exist inside of source shape
                    pt.AddPart()
                    pt.AddPoint(ptg)
                    If HideOutside = False OrElse (tpl.Intersect(shp, pt) AndAlso IsPointOKWithMaskLayers(lt, pt, shp, _sourceLayer)) Then
                        shpPlot = ll.CreateShape(TGIS_ShapeType.gisShapeTypePoint)
                        shpPlot.AddPart()
                        shpPlot.AddPoint(ptg)

                        'Set id attribute if point belong to a stand
                        For i = 0 To _sourceNames.Count - 1
                            shpTemp = _sourceLayer.GetShape(_sourceNames(i).Value)
                            If tpl.Intersect(shpPlot, shpTemp) Then
                                shpPlot.SetField(GisControlView.FieldNamePlotRelation, _sourceNames(i).Description)
                                Exit For
                            End If
                        Next

                        If flipRowCol Then
                            'Row/col are flipped (because of compass azimuth to circular angle conversion)
                            shpPlot.SetField(GisControlView.FieldNamePlotRow, column)
                            shpPlot.SetField(GisControlView.FieldNamePlotColumn, row)
                        Else
                            shpPlot.SetField(GisControlView.FieldNamePlotRow, row)
                            shpPlot.SetField(GisControlView.FieldNamePlotColumn, column)
                        End If
                    End If
                    pt.Reset() 'Do this before DoEvents so we don't get any test points rendered
                    lt.RevertAll() 'Free some memory for next cycle

                    'Move on to next point
                    y += dstY
                    row += 1

                    Application.DoEvents()
                    If _progress.UserClosed() Then 'Deadlock prevention (too time consuming)
                        Exit Do
                    End If
                Loop

                If _progress.UserClosed() Then 'Deadlock prevention (too time consuming)
                    ll.RevertShapes()
                    Exit Do
                End If

                'Move on to next point
                x += dstX
                column += 1 : row = 0
            Loop
            _gisCtrl.Unlock()
        Else 'Randomize
            If _numPlots > maxTries Then _numPlots = maxTries

            'Create plots at random positions
            _gisCtrl.Lock()
            For i = 0 To _numPlots - 1
                'Loop until we get a point inside the shape
                _progress.ProgressBar.Value = (i / _numPlots) * 100
                failCount = 0
                Do
                    ptg.X = shp.ProjectedExtent.XMin + Rnd() * dx
                    ptg.Y = shp.ProjectedExtent.YMin + Rnd() * dy

                    pt.Reset() 'Do this before DoEvents so we don't get any test points rendered

                    Application.DoEvents()
                    If _progress.UserClosed OrElse failCount > maxTries Then Exit Do 'Deadlock prevention

                    pt.AddPart()
                    pt.AddPoint(ptg)
                    If tpl.Intersect(shp, pt) Then 'Note shp.IsCommonPoint doesn't work with multiple parts
                        'Check distance against previous points
                        If _minDistance > 0 Then
                            For j = 1 To ll.GetLastUid()
                                shpTemp = ll.GetShape(j)
                                If shpTemp.Uid = pt.Uid Then Continue For 'Skip test point
                                If GisControlView.GetPointToPointDistance(ll.CS, ptg.X, ptg.Y, shpTemp.GetPoint(0, 0).X, shpTemp.GetPoint(0, 0).Y, _units) < (_minDistance * _units.Factor) Then
                                    failCount += 1
                                    Continue Do
                                End If
                            Next
                        End If

                        'Make sure point complies with mask layers
                        pointOK = IsPointOKWithMaskLayers(lt, pt, shp, _sourceLayer)
                        lt.RevertAll() 'Free some memory for next cycle
                        If Not pointOK Then
                            failCount += 1
                            Continue Do
                        End If

                        'Add a point for this plot
                        shpPlot = ll.CreateShape(TGIS_ShapeType.gisShapeTypePoint)
                        shpPlot.AddPart()
                        shpPlot.AddPoint(ptg)

                        'Set id attribute if point belong to a stand
                        For j = 0 To _sourceNames.Count - 1
                            shpTemp = _sourceLayer.GetShape(_sourceNames(j).Value)
                            If tpl.Intersect(shpPlot, shpTemp) Then
                                shpPlot.SetField(GisControlView.FieldNamePlotRelation, _sourceNames(j).Description)
                                Exit For
                            End If
                        Next

                        Exit Do
                    End If
                Loop

                Application.DoEvents()
                If _progress.UserClosed() OrElse failCount > maxTries Then
                    ll.RevertShapes()
                    Exit For
                End If
            Next
            _gisCtrl.Unlock()
        End If

        Me.Enabled = True 'Enable before progress dialog is close so we don't lose focus
        _progress.Close()
        _progress = Nothing
        Cursor.Current = Cursors.Default

        'Remove test point and test layer
        pt.Delete()
        _gisCtrl.Delete(lt.Name)

        _gisCtrl.Update()
    End Sub
#End Region

#Region "Event handlers"
    Private Sub PlotDialog_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'Remove preview layer
        If _gisCtrl.Get(GisControlView.PreviewLayerName) IsNot Nothing Then
            _gisCtrl.Delete(GisControlView.PreviewLayerName)
            _gisCtrl.Update()
        End If
    End Sub

    Private Sub PlotDialog_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim c As New SortedList
        Dim i As Integer, j As Integer
        Dim ulst As New TGIS_CSUnitsList

        'Set up language dependent strings
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        lblDestinationLayer.Text = GisControlView.LangStr(_strIdDestinationLayer)
        lblUnits.Text = GisControlView.LangStr(_strIdUnits)
        lblNoise.Text = GisControlView.LangStr(_strIdNoiseFactor)
        lblDensity.Text = GisControlView.LangStr(_strIdDensity)
        lblMaskLayers.Text = GisControlView.LangStr(_strIdMaskLayers)
        grpMethod.Text = GisControlView.LangStr(_strIdMethod)
        btnUseGrid.Text = GisControlView.LangStr(_strIdUseGrid)
        btnRandomize.Text = GisControlView.LangStr(_strIdRandomize)
        btnOK.Text = GisControlView.LangStr(_strIdOK)
        btnCancel.Text = GisControlView.LangStr(_strIdCancel)
        lblAngle.Text = GisControlView.LangStr(_strIdAngle)
        chkHideOutside.Text = GisControlView.LangStr(_strIdHideOutside)
        btnUpdate.Text = GisControlView.LangStr(_strIdUpdate)

        'List possible destination layers and mask layers
        cmbDestinationLayer.Items.Add(New NameCaptionPair(vbNullString, GisControlView.LangStr(_strIdNewLayer)))
        Dim lsql As TGIS_LayerSqlAdo
        For i = _gisCtrl.Items.Count - 1 To 0 Step -1 'Reverse order to match legend
            lsql = TryCast(_gisCtrl.Items(i), TGIS_LayerSqlAdo)
            If lsql IsNot Nothing Then
                lstMaskLayers.Items.Add(New NameCaptionPair(lsql.Name, lsql.Caption))
                j = cmbDestinationLayer.Items.Add(New NameCaptionPair(lsql.Name, lsql.Caption))
                If lsql.Name = _sourceLayer.Name Then
                    cmbDestinationLayer.SelectedIndex = j 'Select active layer
                    If (j > 0) Then lstMaskLayers.SetSelected(j - 1, True) 'Select active layer as mask layer aswell
                End If
            End If
        Next
        cmbDestinationLayer.SelectedIndex = 0 'Pick 'new layer' as default

        'List units
        AddUnit(ulst.ByEPSG(GisControlView.EpsgMetric)) 'Metric
        AddUnit(ulst.ByEPSG(GisControlView.EpsgUS)) 'US
        For i = 1 To ulst.Count() - 1 'Sort items in alphabetic order
            If ulst(i).EPSG <> GisControlView.EpsgMetric AndAlso ulst(i).EPSG <> GisControlView.EpsgUS AndAlso _
            (ulst(i).UnitsType = TGIS_CSUnitsType.gisCSUnitsTypeLinear OrElse ulst(i).UnitsType = TGIS_CSUnitsType.gisCSUnitsTypeAuto) Then 'Only linear/auto unit types. Metric and US already listed
                c.Add(ulst(i).Description, ulst(i).EPSG)
            End If
        Next
        For i = 0 To c.Count - 1
            AddUnit(ulst.ByEPSG(c.GetByIndex(i)))
        Next

        btnUseGrid.Checked = True
        UpdateForm()
    End Sub

    Private Sub PlotDialog_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseDown
        If (e.Button And Windows.Forms.MouseButtons.Left) AndAlso btnUseGrid.Checked Then
            'Check distance from circle
            If (e.X - _compassCenter.X) * (e.X - _compassCenter.X) + (e.Y - _compassCenter.Y) * (e.Y - _compassCenter.Y) < (_compassSize / 2) * (_compassSize / 2) Then
                Dim pt As New Point(e.X, e.Y)
                UpdateDirection(pt)
            End If
        End If
    End Sub

    Private Sub PlotDialog_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseMove
        If (e.Button And Windows.Forms.MouseButtons.Left) AndAlso btnUseGrid.Checked Then
            'Check distance from circle
            If (e.X - _compassCenter.X) * (e.X - _compassCenter.X) + (e.Y - _compassCenter.Y) * (e.Y - _compassCenter.Y) < (_compassSize / 2) * (_compassSize / 2) Then
                Dim pt As New Point(e.X, e.Y)
                UpdateDirection(pt)
            End If
        End If
    End Sub

    Private Sub PlotDialog_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Me.Paint
        Dim mypen As Pen
        If btnUseGrid.Checked Then
            mypen = Pens.Black
        Else
            mypen = Pens.Gray
        End If
        e.Graphics.DrawEllipse(mypen, New Rectangle(_compassPos.X, _compassPos.Y, _compassSize, _compassSize))
        e.Graphics.DrawLine(mypen, _compassCenter, _compassDir)
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Dim plotIdFieldOK As Boolean, plotRCFieldOK As Boolean
        Dim ll As TGIS_LayerVector, lp As TGIS_LayerVector
        Dim destinationLayerName As String
        Dim shpPrev As TGIS_Shape, shpDest As TGIS_Shape
        Dim i As Integer, num As Integer

        If Not _firstUpdateDone Then UpdatePreview() 'Shortcut, user won't have to press Update before OK

        'Get a handle to destination layer
        Dim val As NameCaptionPair = CType(cmbDestinationLayer.SelectedItem, NameCaptionPair)
        If val.Name = vbNullString Then
            'Create a new layer
            destinationLayerName = GisControlView.Instance.AddNewLayer()
        Else
            'Use specified layer
            destinationLayerName = val.Name
        End If

        'If plots are shown outside the polygon we need to update preview before copying
        If btnUseGrid.Checked AndAlso chkHideOutside.Checked = False Then
            chkHideOutside.Checked = True
            UpdatePreview()
        End If

        If Not _firstUpdateDone Then UpdatePreview() 'Shortcut, user won't have to press Update before OK

        ll = _gisCtrl.Get(destinationLayerName)
        lp = _gisCtrl.Get(GisControlView.PreviewLayerName)
        If ll IsNot Nothing AndAlso lp IsNot Nothing Then
            'Add plot id field (if not already exist) to layer, make sure field type is ok
            If ll.FindField(GisControlView.FieldNamePlotId) = -1 Then
                ll.AddField(GisControlView.FieldNamePlotId, TGIS_FieldType.gisFieldTypeNumber, 0, 0)
            End If
            If CType(ll.Fields(ll.FindField(GisControlView.FieldNamePlotId)), TGIS_FieldInfo).FieldType = TGIS_FieldType.gisFieldTypeNumber Then plotIdFieldOK = True

            'Check row/column fields for grid plots
            If btnUseGrid.Checked Then
                If ll.FindField(GisControlView.FieldNamePlotRow) = -1 Then
                    ll.AddField(GisControlView.FieldNamePlotRow, TGIS_FieldType.gisFieldTypeNumber, 0, 0)
                End If

                If ll.FindField(GisControlView.FieldNamePlotColumn) = -1 Then
                    ll.AddField(GisControlView.FieldNamePlotColumn, TGIS_FieldType.gisFieldTypeNumber, 0, 0)
                End If

                If _
                CType(ll.Fields(ll.FindField(GisControlView.FieldNamePlotRow)), TGIS_FieldInfo).FieldType = TGIS_FieldType.gisFieldTypeNumber AndAlso _
                CType(ll.Fields(ll.FindField(GisControlView.FieldNamePlotColumn)), TGIS_FieldInfo).FieldType = TGIS_FieldType.gisFieldTypeNumber Then
                    plotRCFieldOK = True
                End If
            End If

            'Add plot relation field
            If ll.FindField(GisControlView.FieldNamePlotRelation) = -1 Then
                ll.AddField(GisControlView.FieldNamePlotRelation, TGIS_FieldType.gisFieldTypeString, 128, 0)
            End If

            'Copy plots from preview layer to destination layer
            For i = 1 To lp.GetLastUid()
                shpPrev = lp.GetShape(i)
                If shpPrev IsNot Nothing AndAlso Not shpPrev.IsDeleted Then
                    shpDest = ll.CreateShape(TGIS_ShapeType.gisShapeTypePoint)
                    shpDest.AddPart()
                    shpDest.AddPoint(shpPrev.GetPoint(0, 0))

                    'Add plot relation id
                    shpDest.SetField(GisControlView.FieldNamePlotRelation, shpPrev.GetField(GisControlView.FieldNamePlotRelation))

                    'Set up plot id
                    If plotIdFieldOK Then
                        num += 1
                        shpDest.SetField(GisControlView.FieldNamePlotId, num)
                    End If

                    'Copy row/column number for grid plots
                    If plotRCFieldOK AndAlso btnUseGrid.Checked Then
                        shpDest.SetField(GisControlView.FieldNamePlotRow, shpPrev.GetField(GisControlView.FieldNamePlotRow))
                        shpDest.SetField(GisControlView.FieldNamePlotColumn, shpPrev.GetField(GisControlView.FieldNamePlotColumn))
                    End If
                End If
            Next
            ll.SaveData()
        End If

        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub txtDistanceX_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDistanceX.TextChanged
        Try
            _distanceX = CDbl(txtDistanceX.Text)
        Catch ex As InvalidCastException
            _distanceX = 0
        End Try

        UpdateForm()
    End Sub

    Private Sub txtDistanceY_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDistanceY.TextChanged
        Try
            _distanceY = CDbl(txtDistanceY.Text)
        Catch ex As InvalidCastException
            _distanceY = 0
        End Try

        UpdateForm()
    End Sub

    Private Sub txtNumPlots_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNumPlots.TextChanged
        Try
            _numPlots = CDbl(txtNumPlots.Text)
        Catch ex As InvalidCastException
            _numPlots = 0
        End Try

        UpdateForm()
    End Sub

    Private Sub txtAngle_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAngle.TextChanged
        Try
            _angle = CDbl(txtAngle.Text)
        Catch ex As InvalidCastException
            _angle = 0
        End Try

        If Not _angleAutoUpdate Then
            'Rotate compass line (counter-clockwise but because y increase downwards it will be displayed as clockwise rotation which is what we want)
            Dim dx As Double = _compassSize / 2
            Dim dy As Double = 0
            Dim theta As Double = (_angle - 90.0) / 180.0 * PI
            _compassDir.X = _compassCenter.X + Cos(theta) * dx - Sin(theta) * dy
            _compassDir.Y = _compassCenter.Y + Sin(theta) * dx + Cos(theta) * dy
            Me.Invalidate()
        End If
    End Sub

    Private Sub txtNoise_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNoise.ValueChanged
        Try
            _noise = CDbl(txtNoise.Value)
        Catch ex As InvalidCastException
            _noise = 0
        End Try
    End Sub

    Private Sub txtMinDistance_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMinDistance.TextChanged
        Try
            _minDistance = CDbl(txtMinDistance.Text)
        Catch ex As InvalidCastException
            _minDistance = 0
        End Try
    End Sub

    Private Sub txtOffset_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOffset.TextChanged
        Try
            _offset = CDbl(txtOffset.Text)
        Catch ex As InvalidCastException
            _offset = 0
        End Try
    End Sub

    Private Sub btnUseGrid_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUseGrid.CheckedChanged
        If btnUseGrid.Checked Then
            UpdateForm() 'Prevent multiple updates
        End If
    End Sub

    Private Sub btnRandomize_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRandomize.CheckedChanged
        If btnRandomize.Checked Then
            UpdateForm() 'Prevent multiple updates
        End If
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        UpdatePreview()
    End Sub

    Private Sub cmbUnits_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbUnits.SelectedIndexChanged
        Dim ulst As New TGIS_CSUnitsList

        'Apply unit
        _units = ulst.ByEPSG(CType(cmbUnits.SelectedItem, ValueDescriptionPair).Value)

        'Update labels
        lblDistance.Text = GisControlView.LangStr(_strIdDistance) & " (" & _units.AutoSelect(False, 1.0).Symbol & ")"
        lblMinDistance.Text = GisControlView.LangStr(_strIdMinDistance) & " (" & _units.AutoSelect(False, 1.0).Symbol & ")"
        lblOffset.Text = GisControlView.LangStr(_strIdOffset) & " (" & _units.AutoSelect(False, 1.0).Symbol & ")"
    End Sub
#End Region
End Class
