﻿Imports System.Data.OleDb
Imports TatukGIS.NDK
Imports XtremeReportControl

Public Class RelationDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 700
    Private Const _strIdObjectName As Integer = 701
    Private Const _strIdPropertyName As Integer = 702
    Private Const _strIdStandName As Integer = 703
    Private Const _strIdOK As Integer = 704
    Private Const _strIdCancel As Integer = 705
    Private Const _strIdClear As Integer = 706
    Private Const _strIdConfirmClear As Integer = 707
    Private Const _strIdTcStand As Integer = 708
    Private Const _strIdObjectProperty As Integer = 709
    Private Const _strIdFilter As Integer = 710
    Private Const _strIdObjectId As Integer = 711
    Private Const _strIdPropertyNumber As Integer = 712
    Private Const _strIdPropertyObjId As Integer = 713
    Private Const _strIdStandNumber As Integer = 714
    Private Const _strIdUnconnectedOnly As Integer = 715
    Private Const _strIdCannotConnectSampleTrees As Integer = 716
    Private Const _strIdSectionName As Integer = 717
    Private Const _strIdSectionNumber As Integer = 718
#End Region

#Region "Declarations"
    Private _layerName As String
    Private _shapeId As Integer
    Private _type As RelationType
    Private _gisCtrl As TatukGIS.NDK.WinForms.TGIS_ViewerWnd
    Private _subWidgetId As Integer = -1
    Private _initDone As Boolean

    Private Const ExistingRelationColor As UInteger = &HAAAAAA
    Private Const UnconnectedCode As String = "¤"

    Public Sub New(ByVal layerName As String, ByVal type As RelationType, ByVal shapeId As Integer)
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _layerName = layerName
        _type = type
        _shapeId = shapeId
        _gisCtrl = GisControlView.Instance.GisCtrl
    End Sub
#End Region

#Region "Event handlers"
    Private Sub RelationDialog_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim cmd As New OleDbCommand("", GisControlView.Conn)
        Dim reader As OleDbDataReader = Nothing
        Dim i As Integer
        Dim widgetId As Integer = -1
        Dim rec As ReportRecord = Nothing
        Dim connected As String

        'Disable OK button until an item is selected
        uxOK.Enabled = False

        'Load language dependent string
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        uxOK.Text = GisControlView.LangStr(_strIdOK)
        uxCancel.Text = GisControlView.LangStr(_strIdCancel)
        uxRemoveButton.Text = GisControlView.LangStr(_strIdClear)
        uxFilterLabel.Text = GisControlView.LangStr(_strIdFilter)
        uxUnconnectedOnly.Text = GisControlView.LangStr(_strIdUnconnectedOnly)
        uxSubWidgetLabel.Text = vbNullString

        'Check for existing relation
        If _type = RelationType.ObjectRelation Then
            cmd.CommandText = "SELECT " & GisControlView.FieldNameRelation & "," & GisControlView.FieldNameSubrelation & " FROM " & GisControlView.GisTablePrefix & _layerName & "_FEA WHERE uid = " & _shapeId
        Else
            cmd.CommandText = "SELECT " & GisControlView.FieldNameRelation & " FROM " & GisControlView.GisTablePrefix & _layerName & "_FEA WHERE uid = " & _shapeId
        End If
        reader = cmd.ExecuteReader(CommandBehavior.SingleRow)
        If reader.HasRows Then
            reader.Read()
            If Not reader.IsDBNull(0) Then widgetId = reader.GetInt32(0)
            If _type = RelationType.ObjectRelation AndAlso Not reader.IsDBNull(1) Then
                Dim str As String = reader.GetString(1)
                str = str.Substring(str.IndexOf("/"c) + 1)
                If str <> vbNullString Then _subWidgetId = CLng(str)
            End If
        End If
        reader.Close()

        'We don't need any remove button if relation are not set
        If widgetId = -1 Then uxRemoveButton.Enabled = False

        'Disable subwidget
        If _type <> RelationType.ObjectRelation Then
            uxSubWidgetLabel.Enabled = False
            uxSubWidgetCombo.Enabled = False
        End If

        'List existing widgets, set up columns
        Select Case _type
            Case RelationType.ObjectRelation
                uxWidgetReport.Columns.Add(0, GisControlView.LangStr(_strIdObjectName), 80, True)
                uxWidgetReport.Columns.Add(1, GisControlView.LangStr(_strIdObjectId), 20, True)
                uxWidgetReport.Columns.Add(2, vbNullString, 0, False).Visible = False

                cmd.CommandText = "SELECT object_id," & _
                                  "object_name," & _
                                  "object_id_number," & _
                                  "CASE WHEN object_id IN (SELECT " & GisControlView.FieldNameRelation & " FROM " & GisControlView.GisTablePrefix & _layerName & "_FEA) THEN 1 ELSE 0 END " & _
                                  "FROM elv_object_table ORDER BY object_id"
                reader = cmd.ExecuteReader()
                Do While reader.Read()
                    rec = uxWidgetReport.Records.Add()
                    rec.Tag = reader.GetInt32(0)
                    rec.AddItem(reader.GetString(1))
                    rec.AddItem(reader.GetString(2))

                    connected = UnconnectedCode
                    If reader.GetInt32(3) Then
                        For i = 0 To rec.ItemCount - 1
                            rec(i).ForeColor = ExistingRelationColor
                            connected = vbNullString
                        Next
                    End If
                    rec.AddItem(connected)
                Loop
                reader.Close()
                uxSubWidgetLabel.Text = GisControlView.LangStr(_strIdObjectProperty)

            Case RelationType.PropertyRelation
                uxWidgetReport.Columns.Add(0, GisControlView.LangStr(_strIdPropertyName), 55, True)
                uxWidgetReport.Columns.Add(1, GisControlView.LangStr(_strIdPropertyNumber), 30, True)
                uxWidgetReport.Columns.Add(2, GisControlView.LangStr(_strIdPropertyObjId), 15, True)
                uxWidgetReport.Columns.Add(3, vbNullString, 0, False).Visible = False

                cmd.CommandText = "SELECT id," & _
                                  "CASE WHEN block_number <> '' OR unit_number <> '' THEN prop_name + ' ' + block_number + ':' + unit_number ELSE prop_name END," & _
                                  "prop_number," & _
                                  "obj_id," & _
                                  "CASE WHEN id IN (SELECT " & GisControlView.FieldNameRelation & " FROM " & GisControlView.GisTablePrefix & _layerName & "_FEA) THEN 1 ELSE 0 END " & _
                                  "FROM fst_property_table ORDER BY id"
                reader = cmd.ExecuteReader()
                Do While reader.Read()
                    rec = uxWidgetReport.Records.Add()
                    rec.Tag = reader.GetInt32(0)
                    rec.AddItem(reader.GetString(1))
                    rec.AddItem(reader.GetString(2))
                    If Not reader.IsDBNull(3) Then rec.AddItem(reader.GetString(3)) Else rec.AddItem(vbNullString)

                    connected = UnconnectedCode
                    If reader.GetInt32(4) Then
                        For i = 0 To rec.ItemCount - 1
                            rec(i).ForeColor = ExistingRelationColor
                            connected = vbNullString
                        Next
                    End If
                    rec.AddItem(connected)
                Loop
                reader.Close()


            Case RelationType.StandRelation, RelationType.SectionRelation
                If _type = RelationType.SectionRelation Then
                    uxWidgetReport.Columns.Add(0, GisControlView.LangStr(_strIdSectionName), 75, True)
                    uxWidgetReport.Columns.Add(1, GisControlView.LangStr(_strIdSectionNumber), 25, True)
                Else
                    uxWidgetReport.Columns.Add(0, GisControlView.LangStr(_strIdStandName), 75, True)
                    uxWidgetReport.Columns.Add(1, GisControlView.LangStr(_strIdStandNumber), 25, True)
                End If
                uxWidgetReport.Columns.Add(2, vbNullString, 0, False).Visible = False

                cmd.CommandText = "SELECT trakt_id," & _
                                  "trakt_name," & _
                                  "trakt_num," & _
                                  "CASE WHEN trakt_id IN (SELECT " & GisControlView.FieldNameRelation & " FROM " & GisControlView.GisTablePrefix & _layerName & "_FEA) THEN 1 ELSE 0 END " & _
                                  "FROM esti_trakt_table ORDER BY trakt_id"
                reader = cmd.ExecuteReader()
                Do While reader.Read()
                    rec = uxWidgetReport.Records.Add()
                    rec.Tag = reader.GetInt32(0)
                    rec.AddItem(reader.GetString(1))
                    rec.AddItem(reader.GetString(2))

                    connected = UnconnectedCode
                    If reader.GetInt32(3) Then
                        For i = 0 To rec.ItemCount - 1
                            rec(i).ForeColor = ExistingRelationColor
                            connected = vbNullString
                        Next
                    End If
                    rec.AddItem(connected)
                Loop
                reader.Close()

            Case RelationType.TCTractRelation, RelationType.TCPlotRelation
                uxWidgetReport.Columns.Add(0, GisControlView.LangStr(_strIdTcStand), 100, True)
                uxWidgetReport.Columns.Add(1, vbNullString, 0, False).Visible = False

                cmd.CommandText = "SELECT fileindex AS id," & _
                                  "tractid AS name," & _
                                  "CASE WHEN fileindex IN (SELECT " & GisControlView.FieldNameRelation & " FROM " & GisControlView.GisTablePrefix & _layerName & "_FEA) THEN 1 ELSE 0 END " & _
                                  "FROM tc_stand ORDER BY fileindex"
                reader = cmd.ExecuteReader()
                Do While reader.Read()
                    rec = uxWidgetReport.Records.Add()
                    rec.Tag = reader.GetInt32(0)
                    rec.AddItem(reader.GetString(1))

                    connected = UnconnectedCode
                    If reader.GetInt32(2) Then
                        For i = 0 To rec.ItemCount - 1
                            rec(i).ForeColor = ExistingRelationColor
                            connected = vbNullString
                        Next
                    End If
                    rec.AddItem(connected)
                Loop
                reader.Close()

            Case RelationType.SampleTreeRelation
                MsgBox(GisControlView.LangStr(_strIdCannotConnectSampleTrees), MsgBoxStyle.Information)
                Me.DialogResult = Windows.Forms.DialogResult.Cancel
                Me.Close()
        End Select

        'Populate report, select current value
        uxWidgetReport.Populate()
        uxWidgetReport.SelectedRows.DeleteAll() 'No default selection
        For Each row As ReportRow In uxWidgetReport.Rows
            If row.Record.Tag = widgetId Then
                uxWidgetReport.Navigator.MoveToRow(row.Index)
                row.Selected = True
                Exit For
            End If
        Next

        _initDone = True
    End Sub

    Private Sub BtnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxOK.Click
        Dim ll As TGIS_LayerVector
        Dim shp As TGIS_Shape
        Dim widgetId As Integer = -1, subWidgetId As Integer = -1

        Me.DialogResult = Windows.Forms.DialogResult.OK

        'Get id
        If uxWidgetReport.SelectedRows.Count > 0 Then
            widgetId = uxWidgetReport.SelectedRows(0).Record.Tag
        End If
        If uxSubWidgetCombo.SelectedItem IsNot Nothing Then
            subWidgetId = CType(uxSubWidgetCombo.SelectedItem, ValueDescriptionPair).Value
        End If

        'Update relation id
        ll = _gisCtrl.Get(_layerName)
        If ll IsNot Nothing Then
            'Store widget id in shape
            shp = ll.GetShape(_shapeId).MakeEditable()
            If shp IsNot Nothing Then
                'Relation
                shp.SetField(GisControlView.FieldNameRelation, widgetId)

                'Subrelation
                If _type = RelationType.ObjectRelation Then
                    If uxSubWidgetCombo.SelectedItem IsNot Nothing Then
                        shp.SetField(GisControlView.FieldNameSubrelation, widgetId & "/" & subWidgetId) 'ObjId/PropId
                    Else
                        shp.SetField(GisControlView.FieldNameSubrelation, vbNullString)
                    End If
                End If
                ll.SaveAll()
            End If
        End If

        Me.Close()
    End Sub

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxRemoveButton.Click
        Dim ll As TGIS_LayerVector
        Dim shp As TGIS_Shape

        'Let user confirm this choice
        If MsgBox(GisControlView.LangStr(_strIdConfirmClear), MsgBoxStyle.Exclamation Or MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Me.DialogResult = Windows.Forms.DialogResult.Abort

            'Clear shape relation id
            ll = _gisCtrl.Get(_layerName)
            If ll IsNot Nothing Then
                shp = ll.GetShape(_shapeId).MakeEditable()
                If shp IsNot Nothing Then
                    'Remove any existing relation image
                    GisControlView.RemoveRelationImage(shp, GisControlView.GetLayerRelationType(ll.Name))

                    'Clear relation fields
                    shp.SetField(GisControlView.FieldNameRelation, Nothing)
                    If ll.FindField(GisControlView.FieldNameSubrelation) <> -1 Then shp.SetField(GisControlView.FieldNameSubrelation, vbNullString)
                    ll.SaveAll()
                End If
            End If

            Me.Close()
        End If
    End Sub

    Private Sub uxWidgetReport_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxWidgetReport.SelectionChanged
        If uxWidgetReport.SelectedRows.Count > 0 Then
            uxOK.Enabled = True

            If _type = RelationType.ObjectRelation Then
                Dim idx As Integer
                Dim cmd As New OleDbCommand("", GisControlView.Conn)
                Dim reader As OleDbDataReader

                uxSubWidgetCombo.Items.Clear()

                'List any properties under this object
                cmd.CommandText = "SELECT prop_id, CASE WHEN block_number <> '' OR unit_number <> '' THEN f.prop_name + ' ' + block_number + ':' + unit_number ELSE f.prop_name END " & _
                                  "FROM elv_properties_table e INNER JOIN fst_property_table f ON f.id = e.prop_id  WHERE prop_object_id = " & uxWidgetReport.SelectedRows(0).Record.Tag
                reader = cmd.ExecuteReader()
                Do While reader.Read()
                    idx = uxSubWidgetCombo.Items.Add(New ValueDescriptionPair(reader.GetInt32(0), reader.GetString(1)))
                    If _subWidgetId = reader.GetInt32(0) AndAlso Not _initDone Then uxSubWidgetCombo.SelectedIndex = idx
                Loop
                reader.Close()
            End If
        Else
            uxOK.Enabled = False
        End If
    End Sub

    Private Sub uxFilterText_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxFilterText.TextChanged
        uxWidgetReport.FilterText = uxFilterText.Text
        uxWidgetReport.Populate()
    End Sub

    Private Sub uxUnconnectedOnly_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxUnconnectedOnly.CheckedChanged
        uxFilterText.Enabled = Not uxUnconnectedOnly.Checked 'We use filtering on a hidden column so filter string cannot be applieds simultaneously

        If uxUnconnectedOnly.Checked Then
            uxWidgetReport.FilterText = UnconnectedCode
        Else
            uxWidgetReport.FilterText = uxFilterText.Text
        End If
        uxWidgetReport.Populate()
    End Sub
#End Region
End Class
