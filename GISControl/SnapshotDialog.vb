﻿Imports System.Data.OleDb
Imports System.IO
Imports TatukGIS.NDK
Imports XtremeReportControl

Public Class SnapshotDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 1900
    Private Const _strIdObjectName As Integer = 1901
    Private Const _strIdObjectId As Integer = 1902
    Private Const _strIdPropertyName As Integer = 1903
    Private Const _strIdPropertyNumber As Integer = 1904
    Private Const _strIdPropertyObjId As Integer = 1905
    Private Const _strIdStandName As Integer = 1906
    Private Const _strIdStandNumber As Integer = 1907
    Private Const _strIdName As Integer = 1908
    Private Const _strIdType As Integer = 1909
    Private Const _strIdTypeObject As Integer = 1910
    Private Const _strIdTypeProperty As Integer = 1911
    Private Const _strIdTypeStand As Integer = 1912
    Private Const _strIdFilter As Integer = 1913
    Private Const _strIdOK As Integer = 1914
    Private Const _strIdCancel As Integer = 1915
    Private Const _strIdTCTractName As Integer = 1916
    Private Const _strIdTCTract As Integer = 1917
    Private Const _strIdIncludeLegend As Integer = 1918
#End Region

#Region "Declarations"
    Private _fileName As String
    Private _defaultTitle As String
    Private _objectRecords As New List(Of ReportRecord)
    Private _propertyRecords As New List(Of ReportRecord)
    Private _standRecords As New List(Of ReportRecord)
    Private _tctractRecords As New List(Of ReportRecord)
    Private _gisCtrl As TatukGIS.NDK.WinForms.TGIS_ViewerWnd
    Private _controlLegend As TatukGIS.NDK.WinForms.TGIS_ControlLegend

    Public Sub New(ByVal fileName As String, ByVal title As String)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _gisCtrl = GisControlView.Instance.GisCtrl
        _controlLegend = GisControlView.Instance.ControlLegend
        _fileName = fileName
        _defaultTitle = title
    End Sub
#End Region

#Region "Helper functions"
    Private Sub UpdateList()
        Dim widgetRecords As List(Of ReportRecord) = Nothing

        'Clear list
        uxWidgetReport.Records.DeleteAll()
        uxWidgetReport.Columns.DeleteAll()

        'Set up report
        If uxTypes.SelectedItem().ToString = GisControlView.LangStr(_strIdTypeObject) Then
            uxWidgetReport.Columns.Add(0, GisControlView.LangStr(_strIdObjectName), 80, True)
            uxWidgetReport.Columns.Add(1, GisControlView.LangStr(_strIdObjectId), 20, True)
            widgetRecords = _objectRecords
        ElseIf uxTypes.SelectedItem().ToString = GisControlView.LangStr(_strIdTypeProperty) Then
            uxWidgetReport.Columns.Add(0, GisControlView.LangStr(_strIdPropertyName), 55, True)
            uxWidgetReport.Columns.Add(1, GisControlView.LangStr(_strIdPropertyNumber), 30, True)
            uxWidgetReport.Columns.Add(2, GisControlView.LangStr(_strIdPropertyObjId), 15, True)
            widgetRecords = _propertyRecords
        ElseIf uxTypes.SelectedItem().ToString = GisControlView.LangStr(_strIdTypeStand) Then
            uxWidgetReport.Columns.Add(0, GisControlView.LangStr(_strIdStandName), 75, True)
            uxWidgetReport.Columns.Add(1, GisControlView.LangStr(_strIdStandNumber), 25, True)
            widgetRecords = _standRecords
        ElseIf uxTypes.SelectedItem().ToString = GisControlView.LangStr(_strIdTCTract) Then
            uxWidgetReport.Columns.Add(0, GisControlView.LangStr(_strIdTCTractName), 75, True)
            widgetRecords = _tctractRecords
        End If

        'Copy cached records
        Me.Cursor = Cursors.WaitCursor
        For Each crec As ReportRecord In widgetRecords
            Dim rec As ReportRecord = uxWidgetReport.Records.Add()
            For i As Integer = 0 To crec.ItemCount - 1
                rec.AddItem(crec.Item(i).Caption)
            Next
            rec.Tag = crec.Tag
        Next

        uxWidgetReport.Populate()
        Me.Cursor = Cursors.Default

        ValidateForm()
    End Sub

    Private Sub ValidateForm()
        If uxWidgetReport.SelectedRows.Count > 0 AndAlso uxName.Text <> vbNullString Then
            uxOK.Enabled = True
        Else
            uxOK.Enabled = False
        End If
    End Sub
#End Region

#Region "Event handlers"
    Private Sub SnapshotDialog_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'Remove temp files (png, pgw, tab)
        Try
            System.IO.File.Delete(_fileName)
            System.IO.File.Delete(Mid(_fileName, 1, _fileName.LastIndexOf("."c)) & ".pgw")
            System.IO.File.Delete(Mid(_fileName, 1, _fileName.LastIndexOf("."c)) & ".tab")
        Catch ex As IOException
        End Try
    End Sub

    Private Sub SnapshotDialog_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim cmd As New OleDbCommand("", GisControlView.Conn)
        Dim reader As OleDbDataReader

        'Set up language
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        uxNameLabel.Text = GisControlView.LangStr(_strIdName)
        uxTypesLabel.Text = GisControlView.LangStr(_strIdType)
        uxFilterLabel.Text = GisControlView.LangStr(_strIdFilter)
        uxOK.Text = GisControlView.LangStr(_strIdOK)
        uxCancel.Text = GisControlView.LangStr(_strIdCancel)

        uxName.Text = _defaultTitle

        'Cache widget lists
        'Object
        If GisControlView.LandValueInstalled Then
            cmd.CommandText = "SELECT object_id, object_name, object_id_number FROM elv_object_table ORDER BY object_id"
            reader = cmd.ExecuteReader()
            Do While reader.Read()
                Dim rec As New ReportRecord
                rec.Tag = reader.GetInt32(0)
                rec.AddItem(reader.GetString(1))
                rec.AddItem(reader.GetString(2))
                _objectRecords.Add(rec)
            Loop
            reader.Close()

            uxTypes.Items.Add(New ValueDescriptionPair(RelationType.ObjectRelation, GisControlView.LangStr(_strIdTypeObject)))
        End If

        'Property
        If GisControlView.ForestInstalled Then
            cmd.CommandText = "SELECT id," & _
                              "CASE WHEN block_number <> '' OR unit_number <> '' THEN prop_name + ' ' + block_number + ':' + unit_number ELSE prop_name END," & _
                              "prop_number, obj_id FROM fst_property_table ORDER BY id"
            reader = cmd.ExecuteReader()
            Do While reader.Read()
                Dim rec As New ReportRecord
                rec.Tag = reader.GetInt32(0)
                rec.AddItem(reader.GetString(1))
                rec.AddItem(reader.GetString(2))
                If Not reader.IsDBNull(3) Then rec.AddItem(reader.GetString(3))
                _propertyRecords.Add(rec)
            Loop
            reader.Close()

            uxTypes.Items.Add(New ValueDescriptionPair(RelationType.PropertyRelation, GisControlView.LangStr(_strIdTypeProperty)))
        End If

        'Stand
        If GisControlView.EstimateInstalled Then
            cmd.CommandText = "SELECT trakt_id, trakt_name, trakt_num FROM esti_trakt_table ORDER BY trakt_id"
            reader = cmd.ExecuteReader()
            Do While reader.Read()
                Dim rec As New ReportRecord
                rec.Tag = reader.GetInt32(0)
                rec.AddItem(reader.GetString(1))
                rec.AddItem(reader.GetString(2))
                _standRecords.Add(rec)
            Loop
            reader.Close()

            Dim rt As RelationType
            If GisControlView.EstimateSodraInstalled Then rt = RelationType.SectionRelation Else rt = RelationType.StandRelation
            uxTypes.Items.Add(New ValueDescriptionPair(rt, GisControlView.LangStr(_strIdTypeStand)))
        End If

        'TC stand
        If GisControlView.TCruiseInstalled Then
            cmd.CommandText = "SELECT fileindex, tractid FROM tc_stand ORDER BY fileindex"
            reader = cmd.ExecuteReader()
            Do While reader.Read()
                Dim rec As New ReportRecord
                rec.Tag = reader.GetInt32(0)
                rec.AddItem(reader.GetString(1))
                _tctractRecords.Add(rec)
            Loop
            reader.Close()
            uxTypes.Items.Add(New ValueDescriptionPair(RelationType.TCTractRelation, GisControlView.LangStr(_strIdTCTract)))
        End If
    End Sub

    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxOK.Click
        Dim cmd As New OleDbCommand("", GisControlView.Conn)

        'Read off image file
        Dim fs As New FileStream(_fileName, FileMode.OpenOrCreate, FileAccess.Read)
        Dim data(fs.Length) As Byte
        fs.Read(data, 0, fs.Length)
        fs.Close()

        'Insert image to db
        cmd.CommandText = "INSERT INTO gis_snapshots (name, type, widgetid, img) VALUES('" & _
                          uxName.Text & "'," & CType(uxTypes.SelectedItem, ValueDescriptionPair).Value & "," & uxWidgetReport.SelectedRows.Row(0).Record.Tag & ",?)"
        Dim p As New OleDbParameter("@img", OleDbType.VarBinary, data.Length, ParameterDirection.Input, False, 0, 0, Nothing, DataRowVersion.Current, data)
        cmd.Parameters.Add(p)
        cmd.ExecuteNonQuery()

        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub uxFilter_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxFilter.TextChanged
        uxWidgetReport.FilterText = uxFilter.Text
        uxWidgetReport.Populate()
    End Sub

    Private Sub uxWidgetReport_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxWidgetReport.SelectionChanged
        ValidateForm()
    End Sub

    Private Sub uxName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxName.TextChanged
        ValidateForm()
    End Sub

    Private Sub uxTypes_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxTypes.SelectedIndexChanged
        UpdateList()
    End Sub
#End Region

End Class