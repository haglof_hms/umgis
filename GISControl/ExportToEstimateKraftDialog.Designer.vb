﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ExportToEstimateKraftDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.uxOK = New System.Windows.Forms.Button
        Me.uxCancel = New System.Windows.Forms.Button
        Me.uxLayerLabel = New System.Windows.Forms.Label
        Me.uxLayerList = New System.Windows.Forms.ComboBox
        Me.uxHdistanceList = New System.Windows.Forms.ComboBox
        Me.uxHdistanceLabel = New System.Windows.Forms.Label
        Me.uxTdistanceList = New System.Windows.Forms.ComboBox
        Me.uxTdistanceLabel = New System.Windows.Forms.Label
        Me.uxHeightList = New System.Windows.Forms.ComboBox
        Me.uxHeightLabel = New System.Windows.Forms.Label
        Me.uxAttributeGroupBox = New System.Windows.Forms.GroupBox
        Me.TableLayoutPanel1.SuspendLayout()
        Me.uxAttributeGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.uxOK, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.uxCancel, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(73, 195)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'uxOK
        '
        Me.uxOK.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.uxOK.Location = New System.Drawing.Point(3, 3)
        Me.uxOK.Name = "uxOK"
        Me.uxOK.Size = New System.Drawing.Size(67, 23)
        Me.uxOK.TabIndex = 0
        Me.uxOK.Text = "uxOK"
        '
        'uxCancel
        '
        Me.uxCancel.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.uxCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.uxCancel.Location = New System.Drawing.Point(76, 3)
        Me.uxCancel.Name = "uxCancel"
        Me.uxCancel.Size = New System.Drawing.Size(67, 23)
        Me.uxCancel.TabIndex = 1
        Me.uxCancel.Text = "uxCancel"
        '
        'uxLayerLabel
        '
        Me.uxLayerLabel.AutoSize = True
        Me.uxLayerLabel.Location = New System.Drawing.Point(12, 12)
        Me.uxLayerLabel.Name = "uxLayerLabel"
        Me.uxLayerLabel.Size = New System.Drawing.Size(70, 13)
        Me.uxLayerLabel.TabIndex = 1
        Me.uxLayerLabel.Text = "uxLayerLabel"
        '
        'uxLayerList
        '
        Me.uxLayerList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.uxLayerList.FormattingEnabled = True
        Me.uxLayerList.Location = New System.Drawing.Point(118, 9)
        Me.uxLayerList.Name = "uxLayerList"
        Me.uxLayerList.Size = New System.Drawing.Size(129, 21)
        Me.uxLayerList.TabIndex = 2
        '
        'uxHdistanceList
        '
        Me.uxHdistanceList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.uxHdistanceList.FormattingEnabled = True
        Me.uxHdistanceList.Location = New System.Drawing.Point(121, 23)
        Me.uxHdistanceList.Name = "uxHdistanceList"
        Me.uxHdistanceList.Size = New System.Drawing.Size(129, 21)
        Me.uxHdistanceList.TabIndex = 4
        '
        'uxHdistanceLabel
        '
        Me.uxHdistanceLabel.AutoSize = True
        Me.uxHdistanceLabel.Location = New System.Drawing.Point(4, 26)
        Me.uxHdistanceLabel.Name = "uxHdistanceLabel"
        Me.uxHdistanceLabel.Size = New System.Drawing.Size(92, 13)
        Me.uxHdistanceLabel.TabIndex = 3
        Me.uxHdistanceLabel.Text = "uxHdistanceLabel"
        '
        'uxTdistanceList
        '
        Me.uxTdistanceList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.uxTdistanceList.FormattingEnabled = True
        Me.uxTdistanceList.Location = New System.Drawing.Point(121, 50)
        Me.uxTdistanceList.Name = "uxTdistanceList"
        Me.uxTdistanceList.Size = New System.Drawing.Size(129, 21)
        Me.uxTdistanceList.TabIndex = 6
        '
        'uxTdistanceLabel
        '
        Me.uxTdistanceLabel.AutoSize = True
        Me.uxTdistanceLabel.Location = New System.Drawing.Point(4, 53)
        Me.uxTdistanceLabel.Name = "uxTdistanceLabel"
        Me.uxTdistanceLabel.Size = New System.Drawing.Size(91, 13)
        Me.uxTdistanceLabel.TabIndex = 5
        Me.uxTdistanceLabel.Text = "uxTdistanceLabel"
        '
        'uxHeightList
        '
        Me.uxHeightList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.uxHeightList.FormattingEnabled = True
        Me.uxHeightList.Location = New System.Drawing.Point(121, 77)
        Me.uxHeightList.Name = "uxHeightList"
        Me.uxHeightList.Size = New System.Drawing.Size(129, 21)
        Me.uxHeightList.TabIndex = 8
        '
        'uxHeightLabel
        '
        Me.uxHeightLabel.AutoSize = True
        Me.uxHeightLabel.Location = New System.Drawing.Point(4, 80)
        Me.uxHeightLabel.Name = "uxHeightLabel"
        Me.uxHeightLabel.Size = New System.Drawing.Size(75, 13)
        Me.uxHeightLabel.TabIndex = 7
        Me.uxHeightLabel.Text = "uxHeightLabel"
        '
        'uxAttributeGroupBox
        '
        Me.uxAttributeGroupBox.Controls.Add(Me.uxHeightList)
        Me.uxAttributeGroupBox.Controls.Add(Me.uxHeightLabel)
        Me.uxAttributeGroupBox.Controls.Add(Me.uxTdistanceList)
        Me.uxAttributeGroupBox.Controls.Add(Me.uxTdistanceLabel)
        Me.uxAttributeGroupBox.Controls.Add(Me.uxHdistanceList)
        Me.uxAttributeGroupBox.Controls.Add(Me.uxHdistanceLabel)
        Me.uxAttributeGroupBox.Location = New System.Drawing.Point(12, 42)
        Me.uxAttributeGroupBox.Name = "uxAttributeGroupBox"
        Me.uxAttributeGroupBox.Size = New System.Drawing.Size(266, 120)
        Me.uxAttributeGroupBox.TabIndex = 9
        Me.uxAttributeGroupBox.TabStop = False
        Me.uxAttributeGroupBox.Text = "uxAttributLabel"
        '
        'ExportToEstimateKraftDialog
        '
        Me.AcceptButton = Me.uxOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.uxCancel
        Me.ClientSize = New System.Drawing.Size(293, 236)
        Me.Controls.Add(Me.uxAttributeGroupBox)
        Me.Controls.Add(Me.uxLayerList)
        Me.Controls.Add(Me.uxLayerLabel)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ExportToEstimateKraftDialog"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "ExportToEstimateKraftDialog"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.uxAttributeGroupBox.ResumeLayout(False)
        Me.uxAttributeGroupBox.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents uxOK As System.Windows.Forms.Button
    Friend WithEvents uxCancel As System.Windows.Forms.Button
    Friend WithEvents uxLayerLabel As System.Windows.Forms.Label
    Friend WithEvents uxLayerList As System.Windows.Forms.ComboBox
    Friend WithEvents uxHdistanceLabel As System.Windows.Forms.Label
    Friend WithEvents uxTdistanceList As System.Windows.Forms.ComboBox
    Friend WithEvents uxTdistanceLabel As System.Windows.Forms.Label
    Friend WithEvents uxHeightList As System.Windows.Forms.ComboBox
    Friend WithEvents uxHeightLabel As System.Windows.Forms.Label
    Friend WithEvents uxAttributeGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents uxHdistanceList As System.Windows.Forms.ComboBox

End Class
