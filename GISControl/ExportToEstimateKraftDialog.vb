﻿Imports System.Windows.Forms
Imports System.IO
Imports System.Data.OleDb
Imports TatukGIS.NDK

Public Class ExportToEstimateKraftDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 3200
    Private Const _strIdLayer As Integer = 3201
    Private Const _strIdAttribute As Integer = 3202
    Private Const _strIdHdistance As Integer = 3203
    Private Const _strIdTdistance As Integer = 3204
    Private Const _strIdOK As Integer = 3205
    Private Const _strIdCancel As Integer = 3206
    Private Const _strIdHeight As Integer = 3207
#End Region

#Region "Declarations"
    Private _selectedLayer As TGIS_LayerVector
    Private _strFieldNameHdist As String
    Private _strFieldNameTdist As String
    Private _strFieldNameHeight As String
    Private _strLayerName As String

    Public ReadOnly Property FieldNameHdist() As String
        Get
            Return _strFieldNameHdist
        End Get
    End Property

    Public ReadOnly Property FieldNameTdist() As String
        Get
            Return _strFieldNameTdist
        End Get
    End Property

    Public ReadOnly Property FieldNameHeight() As String
        Get
            Return _strFieldNameHeight
        End Get
    End Property

    Public ReadOnly Property LayerName() As String
        Get
            Return _strLayerName
        End Get
    End Property
#End Region

#Region "Helper functions"

    Private Sub EnableDisableOKButton()
        If uxHeightList.SelectedIndex >= 0 Then
            uxOK.Enabled = True
        Else
            uxOK.Enabled = False
        End If
    End Sub


    Private Sub FillAttributeLists(ByVal layerSelected As TGIS_LayerAbstract)
        Dim i As Integer
        Dim ll As TGIS_LayerVector

        uxHdistanceList.Items.Clear()
        uxTdistanceList.Items.Clear()
        uxHeightList.Items.Clear()

        'List fields of import layer in property field combos
        If layerSelected IsNot Nothing AndAlso TypeOf layerSelected Is TGIS_LayerVector Then
            uxHdistanceList.Items.Add("") 'Empty option
            uxTdistanceList.Items.Add("") 'Empty option

            ll = CType(layerSelected, TGIS_LayerVector)
            For i = 0 To ll.Fields.Count - 1
                If GisControlView.IsRelationField(ll.FieldInfo(i).Name) Then Continue For

                uxHdistanceList.Items.Add(ll.FieldInfo(i).Name)
                uxTdistanceList.Items.Add(ll.FieldInfo(i).Name)
                uxHeightList.Items.Add(ll.FieldInfo(i).Name)
            Next

            'List any joined fields
            If ll.JoinNET IsNot Nothing Then
                For Each col As DataColumn In CType(ll.JoinNET, DataTable).Columns
                    If GisControlView.IsRelationField(col.Caption) Then Continue For

                    uxHdistanceList.Items.Add(col.Caption)
                    uxTdistanceList.Items.Add(col.Caption)
                    uxHeightList.Items.Add(col.Caption)
                Next
            End If
        End If
    End Sub

    Private Sub ListLayers(ByVal cmb As ComboBox)
        Dim i As Integer

        'List all visible vector layers (in reverse order to match legend)
        Dim ll As TGIS_LayerAbstract
        For i = GisControlView.Instance.GisCtrl.Items.Count - 1 To 0 Step -1
            ll = GisControlView.Instance.GisCtrl.Items(i)
            If ll.Active AndAlso TypeOf ll Is TGIS_LayerVector AndAlso ll.Name <> GisControlView.PreviewLayerName Then
                cmb.Items.Add(New NameCaptionPair(ll.Name, ll.Caption))
            End If
        Next

        'If default layer couldn't be found or was not specified pick new layer
        If cmb.SelectedIndex < 0 Then cmb.SelectedIndex = 0 'New layer
    End Sub
#End Region

#Region "Event handlers"
    Private Sub ExportToEstimateKraftDialog_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Set up language
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        uxOK.Text = GisControlView.LangStr(_strIdOK)
        uxCancel.Text = GisControlView.LangStr(_strIdCancel)
        uxLayerLabel.Text = GisControlView.LangStr(_strIdLayer)
        uxAttributeGroupBox.Text = GisControlView.LangStr(_strIdAttribute)
        uxHdistanceLabel.Text = GisControlView.LangStr(_strIdHdistance)
        uxTdistanceLabel.Text = GisControlView.LangStr(_strIdTdistance)
        uxHeightLabel.Text = GisControlView.LangStr(_strIdHeight)

        uxOK.Enabled = False

        ListLayers(uxLayerList)
        FillAttributeLists(_selectedLayer)
    End Sub

    Private Sub uxOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxOK.Click
        'Store chosen values
        _strFieldNameHdist = uxHdistanceList.Text
        _strFieldNameTdist = uxTdistanceList.Text
        _strFieldNameHeight = uxHeightList.Text
        _strLayerName = CType(uxLayerList.SelectedItem, NameCaptionPair).Name

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub uxCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub uxLayerList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxLayerList.SelectedIndexChanged
        Dim ll As TGIS_LayerAbstract

        ll = GisControlView.Instance.GisCtrl.Get(CType(uxLayerList.SelectedItem, NameCaptionPair).Name)
        _selectedLayer = ll

        FillAttributeLists(_selectedLayer)

        EnableDisableOKButton()
    End Sub

    Private Sub uxHeightList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxHeightList.SelectedIndexChanged
        EnableDisableOKButton()
    End Sub
#End Region
End Class
