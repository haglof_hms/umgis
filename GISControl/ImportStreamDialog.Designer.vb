﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ImportStreamDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.uxCancel = New System.Windows.Forms.Button
        Me.uxOK = New System.Windows.Forms.Button
        Me.uxAddress = New System.Windows.Forms.TextBox
        Me.uxProtocol = New System.Windows.Forms.ComboBox
        Me.uxAddressLabel = New System.Windows.Forms.Label
        Me.uxProtocolLabel = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'uxCancel
        '
        Me.uxCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.uxCancel.Location = New System.Drawing.Point(212, 95)
        Me.uxCancel.Name = "uxCancel"
        Me.uxCancel.Size = New System.Drawing.Size(75, 23)
        Me.uxCancel.TabIndex = 3
        Me.uxCancel.Text = "uxCancel"
        Me.uxCancel.UseVisualStyleBackColor = True
        '
        'uxOK
        '
        Me.uxOK.Location = New System.Drawing.Point(131, 95)
        Me.uxOK.Name = "uxOK"
        Me.uxOK.Size = New System.Drawing.Size(75, 23)
        Me.uxOK.TabIndex = 2
        Me.uxOK.Text = "uxOK"
        Me.uxOK.UseVisualStyleBackColor = True
        '
        'uxAddress
        '
        Me.uxAddress.Location = New System.Drawing.Point(12, 25)
        Me.uxAddress.Name = "uxAddress"
        Me.uxAddress.Size = New System.Drawing.Size(394, 20)
        Me.uxAddress.TabIndex = 0
        '
        'uxProtocol
        '
        Me.uxProtocol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.uxProtocol.FormattingEnabled = True
        Me.uxProtocol.Location = New System.Drawing.Point(12, 68)
        Me.uxProtocol.Name = "uxProtocol"
        Me.uxProtocol.Size = New System.Drawing.Size(140, 21)
        Me.uxProtocol.TabIndex = 1
        '
        'uxAddressLabel
        '
        Me.uxAddressLabel.AutoSize = True
        Me.uxAddressLabel.Location = New System.Drawing.Point(9, 9)
        Me.uxAddressLabel.Name = "uxAddressLabel"
        Me.uxAddressLabel.Size = New System.Drawing.Size(82, 13)
        Me.uxAddressLabel.TabIndex = 11
        Me.uxAddressLabel.Text = "uxAddressLabel"
        '
        'uxProtocolLabel
        '
        Me.uxProtocolLabel.AutoSize = True
        Me.uxProtocolLabel.Location = New System.Drawing.Point(9, 52)
        Me.uxProtocolLabel.Name = "uxProtocolLabel"
        Me.uxProtocolLabel.Size = New System.Drawing.Size(83, 13)
        Me.uxProtocolLabel.TabIndex = 12
        Me.uxProtocolLabel.Text = "uxProtocolLabel"
        '
        'ImportStreamDialog
        '
        Me.AcceptButton = Me.uxOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.uxCancel
        Me.ClientSize = New System.Drawing.Size(418, 127)
        Me.Controls.Add(Me.uxProtocol)
        Me.Controls.Add(Me.uxAddress)
        Me.Controls.Add(Me.uxProtocolLabel)
        Me.Controls.Add(Me.uxAddressLabel)
        Me.Controls.Add(Me.uxCancel)
        Me.Controls.Add(Me.uxOK)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ImportStreamDialog"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "ImportStreamDialog"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents uxCancel As System.Windows.Forms.Button
    Friend WithEvents uxOK As System.Windows.Forms.Button
    Friend WithEvents uxAddress As System.Windows.Forms.TextBox
    Friend WithEvents uxProtocol As System.Windows.Forms.ComboBox
    Friend WithEvents uxAddressLabel As System.Windows.Forms.Label
    Friend WithEvents uxProtocolLabel As System.Windows.Forms.Label

End Class
