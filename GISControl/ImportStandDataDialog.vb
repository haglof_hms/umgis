﻿Imports Microsoft.Win32
Imports System.Windows.Forms
Imports TatukGIS.NDK

Public Class ImportStandDataDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 3300
    Private Const _strIdOK As Integer = 3301
    Private Const _strIdCancel As Integer = 3302
    Private Const _strIdStandPoint As Integer = 3303
    Private Const _strIdStandPolygon As Integer = 3304
    Private Const _strIdSampleTreePoint As Integer = 3305
    Private Const _strIdPlotPoint As Integer = 3306
    Private Const _strIdChooseLayers As Integer = 3307
    Private Const _strIdNoAction As Integer = 3308
    Private Const _strIdNewLayer As Integer = 3309
#End Region

#Region "Declarations"
    Private _gisCtrl As TatukGIS.NDK.WinForms.TGIS_ViewerWnd

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _gisCtrl = GisControlView.Instance.GisCtrl 'Shorthand
    End Sub

    Public ReadOnly Property StandPointLayer() As String
        Get
            Return CType(cmbStandPoint.SelectedItem, ValueDescriptionPair).Value
        End Get
    End Property

    Public ReadOnly Property StandPolygonLayer() As String
        Get
            Return CType(cmbStandPolygon.SelectedItem, ValueDescriptionPair).Value
        End Get
    End Property

    Public ReadOnly Property SampleTreePointLayer() As String
        Get
            Return CType(cmbSampleTreePoint.SelectedItem, ValueDescriptionPair).Value
        End Get
    End Property

    Public ReadOnly Property PlotPointLayer() As String
        Get
            Return CType(cmbPlotPoint.SelectedItem, ValueDescriptionPair).Value
        End Get
    End Property
#End Region

#Region "Helper functions"
    Private Sub SaveSettings()
        Dim regKey As RegistryKey

        'Save layer choices to registry
        regKey = Registry.CurrentUser.OpenSubKey(GisControlView.RegKeyGis, True)
        If regKey IsNot Nothing Then
            If CType(cmbStandPoint.SelectedItem, ValueDescriptionPair).Value Is Nothing Then
                If regKey.GetValue(GisControlView.RegValStandPointLayer) IsNot Nothing Then regKey.DeleteValue(GisControlView.RegValStandPointLayer)
            Else
                regKey.SetValue(GisControlView.RegValStandPointLayer, CType(cmbStandPoint.SelectedItem, ValueDescriptionPair).Value)
            End If

            If CType(cmbStandPolygon.SelectedItem, ValueDescriptionPair).Value Is Nothing Then
                If regKey.GetValue(GisControlView.RegValStandPolygonLayer) IsNot Nothing Then regKey.DeleteValue(GisControlView.RegValStandPolygonLayer)
            Else
                regKey.SetValue(GisControlView.RegValStandPolygonLayer, CType(cmbStandPolygon.SelectedItem, ValueDescriptionPair).Value)
            End If

            If CType(cmbSampleTreePoint.SelectedItem, ValueDescriptionPair).Value Is Nothing Then
                If regKey.GetValue(GisControlView.RegValSampleTreePointLayer) IsNot Nothing Then regKey.DeleteValue(GisControlView.RegValSampleTreePointLayer)
            Else
                regKey.SetValue(GisControlView.RegValSampleTreePointLayer, CType(cmbSampleTreePoint.SelectedItem, ValueDescriptionPair).Value)
            End If

            If CType(cmbPlotPoint.SelectedItem, ValueDescriptionPair).Value Is Nothing Then
                If regKey.GetValue(GisControlView.RegValPlotPointLayer) IsNot Nothing Then regKey.DeleteValue(GisControlView.RegValPlotPointLayer)
            Else
                regKey.SetValue(GisControlView.RegValPlotPointLayer, CType(cmbPlotPoint.SelectedItem, ValueDescriptionPair).Value)
            End If
            regKey.Close()
        End If
    End Sub
#End Region

#Region "Event handlers"
    Private Sub ImportStandDataDialog_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim i As Integer
        Dim ll As TGIS_LayerVector
        Dim rt As RelationType
        Dim regKey As RegistryKey
        Dim val As String

        'Set up language
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        uxOK.Text = GisControlView.LangStr(_strIdOK)
        uxCancel.Text = GisControlView.LangStr(_strIdCancel)
        uxChooseLayers.Text = GisControlView.LangStr(_strIdChooseLayers)
        lblStandPoint.Text = GisControlView.LangStr(_strIdStandPoint)
        lblStandPolygon.Text = GisControlView.LangStr(_strIdStandPolygon)
        lblSampleTreePoint.Text = GisControlView.LangStr(_strIdSampleTreePoint)
        lblPlotPoint.Text = GisControlView.LangStr(_strIdPlotPoint)

        uxOK.Enabled = False

        'Add <do nothing> option
        cmbStandPoint.Items.Add(New ValueDescriptionPair(vbNullString, GisControlView.LangStr(_strIdNoAction)))
        cmbStandPolygon.Items.Add(New ValueDescriptionPair(vbNullString, GisControlView.LangStr(_strIdNoAction)))
        cmbSampleTreePoint.Items.Add(New ValueDescriptionPair(vbNullString, GisControlView.LangStr(_strIdNoAction)))
        cmbPlotPoint.Items.Add(New ValueDescriptionPair(vbNullString, GisControlView.LangStr(_strIdNoAction)))

        'Add <new layer> option
        cmbStandPoint.Items.Add(New ValueDescriptionPair(GisControlView.NewLayerName, GisControlView.LangStr(_strIdNewLayer)))
        cmbStandPolygon.Items.Add(New ValueDescriptionPair(GisControlView.NewLayerName, GisControlView.LangStr(_strIdNewLayer)))
        cmbSampleTreePoint.Items.Add(New ValueDescriptionPair(GisControlView.NewLayerName, GisControlView.LangStr(_strIdNewLayer)))
        cmbPlotPoint.Items.Add(New ValueDescriptionPair(GisControlView.NewLayerName, GisControlView.LangStr(_strIdNewLayer)))

        'List existing layers
        For i = 0 To _gisCtrl.Items.Count - 1
            ll = TryCast(_gisCtrl.Items(i), TGIS_LayerVector)
            If ll IsNot Nothing Then
                rt = GisControlView.GetLayerRelationType(ll.Name)
                If (rt = RelationType.StandRelation AndAlso Not GisControlView.EstimateSodraInstalled) OrElse (rt = RelationType.SectionRelation AndAlso GisControlView.EstimateSodraInstalled) Then
                    cmbStandPoint.Items.Add(New ValueDescriptionPair(ll.Name, ll.Caption))
                    cmbStandPolygon.Items.Add(New ValueDescriptionPair(ll.Name, ll.Caption))
                    cmbPlotPoint.Items.Add(New ValueDescriptionPair(ll.Name, ll.Caption))
                ElseIf rt = RelationType.SampleTreeRelation Then
                    cmbSampleTreePoint.Items.Add(New ValueDescriptionPair(ll.Name, ll.Caption))
                End If
            End If
        Next

        'Set up default choices, use last values
        regKey = Registry.CurrentUser.OpenSubKey(GisControlView.RegKeyGis, False)
        If regKey IsNot Nothing Then
            val = regKey.GetValue(GisControlView.RegValStandPointLayer, vbNullString)
            For i = 0 To cmbStandPoint.Items.Count - 1
                If CType(cmbStandPoint.Items(i), ValueDescriptionPair).Value = val Then
                    cmbStandPoint.SelectedIndex = i
                    Exit For
                End If
            Next
            If cmbStandPoint.SelectedIndex < 0 Then cmbStandPoint.SelectedIndex = 0

            val = regKey.GetValue(GisControlView.RegValStandPolygonLayer, vbNullString)
            For i = 0 To cmbStandPolygon.Items.Count - 1
                If CType(cmbStandPolygon.Items(i), ValueDescriptionPair).Value = val Then
                    cmbStandPolygon.SelectedIndex = i
                    Exit For
                End If
            Next
            If cmbStandPolygon.SelectedIndex < 0 Then cmbStandPolygon.SelectedIndex = 0

            val = regKey.GetValue(GisControlView.RegValSampleTreePointLayer, vbNullString)
            For i = 0 To cmbSampleTreePoint.Items.Count - 1
                If CType(cmbSampleTreePoint.Items(i), ValueDescriptionPair).Value = val Then
                    cmbSampleTreePoint.SelectedIndex = i
                    Exit For
                End If
            Next
            If cmbSampleTreePoint.SelectedIndex < 0 Then cmbSampleTreePoint.SelectedIndex = 0

            val = regKey.GetValue(GisControlView.RegValPlotPointLayer, vbNullString)
            For i = 0 To cmbPlotPoint.Items.Count - 1
                If CType(cmbPlotPoint.Items(i), ValueDescriptionPair).Value = val Then
                    cmbPlotPoint.SelectedIndex = i
                    Exit For
                End If
            Next
            If cmbPlotPoint.SelectedIndex < 0 Then cmbPlotPoint.SelectedIndex = 0

            regKey.Close()
        End If
    End Sub

    Private Sub cmb_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbStandPolygon.SelectedIndexChanged, cmbStandPoint.SelectedIndexChanged, cmbSampleTreePoint.SelectedIndexChanged, cmbPlotPoint.SelectedIndexChanged
        uxOK.Enabled = (cmbStandPolygon.SelectedIndex >= 0 AndAlso cmbStandPoint.SelectedIndex >= 0 AndAlso cmbSampleTreePoint.SelectedIndex >= 0 AndAlso (cmbPlotPoint.SelectedIndex >= 0 OrElse Not cmbPlotPoint.Enabled))
    End Sub

    Private Sub uxOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxOK.Click
        SaveSettings()

        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub uxCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxCancel.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub
#End Region
End Class
