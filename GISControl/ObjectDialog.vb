﻿Imports System.Data.OleDb
Imports System.Math
Imports TatukGIS.NDK

Public Class ObjectDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 3400
    Private Const _strIdWidth As Integer = 3401
    Private Const _strIdOK As Integer = 3402
    Private Const _strIdCancel As Integer = 3403
    Private Const _strIdProperties As Integer = 3404
    Private Const _strIdNoPcsTitle As Integer = 3405
    Private Const _strIdDestinationLayer As Integer = 3407
    Private Const _strIdNewLayer As Integer = 3408
    Private Const _strIdHighlight As Integer = 3409
    Private Const _strIdPropertyCount As Integer = 3410
    Private Const _strIdAction As Integer = 3411
    Private Const _strIdNewObject As Integer = 3412
    Private Const _strIdExistingObject As Integer = 3413
    Private Const _strIdNoAction As Integer = 3414
    Private Const _strIdUpdate As Integer = 3415
#End Region

#Region "Declarations"
    Private Const _tooltipTimeout As Integer = 5000

    Private _gisCtrl As TatukGIS.NDK.WinForms.TGIS_ViewerWnd
    Private _destinationLayerName As String
    Private _sourceLayerName As String
    Private _sourceShapeUid As Integer
    Private _lineWidth As Single
    Private _lastWidth As Single
    Private _propertyIdList As New List(Of Integer)
    Private _shapePropertyList As New List(Of IntIntPair)
    Private _shapeIntersectionList As New List(Of ShapeBufferPair)
    Private _properties As New Hashtable

    Private Class ShapeBufferPair
        Private _shpUid As Integer, _bufUid As Integer
        Private _layerName As String

        Public Sub New(ByVal shpUid As Integer, ByVal layerName As String, ByVal bufUid As Integer)
            _shpUid = shpUid
            _bufUid = bufUid
            _layerName = layerName
        End Sub

        Public ReadOnly Property BufferUid() As Integer
            Get
                Return _bufUid
            End Get
        End Property

        Public ReadOnly Property LayerName() As String
            Get
                Return _layerName
            End Get
        End Property

        Public ReadOnly Property ShapeUid() As Integer
            Get
                Return _shpUid
            End Get
        End Property
    End Class

    Public Sub New(ByVal layerName As String, ByVal shapeUid As Integer)
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _gisCtrl = GisControlView.Instance.GisCtrl
        _sourceLayerName = layerName
        _sourceShapeUid = shapeUid
    End Sub

    Public ReadOnly Property DestinationLayer() As String
        Get
            Return _destinationLayerName
        End Get
    End Property

    Public ReadOnly Property PropertyIdList() As List(Of Integer)
        Get
            Return _propertyIdList
        End Get
    End Property

    Public ReadOnly Property ShapePropertyList() As List(Of IntIntPair)
        Get
            Return _shapePropertyList
        End Get
    End Property
#End Region

#Region "Helper functions"
    Private Sub UpdateForm()
        If uxObjectList.Items.Count > 0 Then
            uxExistingObject.Enabled = True
            uxObjectList.Enabled = uxExistingObject.Checked
        Else
            uxExistingObject.Enabled = False
            uxObjectList.Enabled = False
        End If
    End Sub

    Private Sub UpdatePreview()
        Dim cmd As New OleDbCommand("", GisControlView.Conn), cmd2 As New OleDbCommand("", GisControlView.Conn)
        Dim reader As OleDbDataReader, reader2 As OleDbDataReader
        Dim i As Integer
        Dim propId As Integer
        Dim value As String
        Dim idlst As New List(Of Integer)
        Dim propIdlst As New List(Of Integer)
        Dim ll As TGIS_LayerVector, lp As TGIS_LayerVector = GisControlView.GetPreviewLayer()
        Dim shp As TGIS_Shape, tmp As TGIS_Shape, buf As TGIS_Shape
        Dim propIds As New Hashtable

        PropertyList.Items.Clear()

        If _gisCtrl Is Nothing Then Exit Sub
        ll = _gisCtrl.Get(_sourceLayerName) : If ll Is Nothing Then Exit Sub
        shp = ll.GetShape(_sourceShapeUid) : If shp Is Nothing Then Exit Sub

        lp.RevertShapes()

        'Check type of shape
        If shp.ShapeType = TGIS_ShapeType.gisShapeTypeArc Then
            Dim tpl As New TGIS_Topology

            If TypeOf _gisCtrl.CS Is TGIS_CSProjectedCoordinateSystem Then
                'Make one single buffer
                buf = tpl.MakeBuffer(shp, _lineWidth / GisControlView.Instance.GetCSScaleFactor() * GisControlView.Instance.GetScaleUnit().Factor * 0.5)
                lp.AddShape(buf)
            ElseIf TypeOf _gisCtrl.CS Is TGIS_CSGeographicCoordinateSystem Then
                Dim a As Double, dist As Double, length As Double, width As Double
                Dim v As TGIS_Point, p0 As TGIS_Point, p1 As TGIS_Point

                'Make one buffer per line segment as x/y scale might differ
                For i = 1 To shp.GetNumPoints() - 1
                    p0 = shp.GetPoint(0, i - 1)
                    p1 = shp.GetPoint(0, i)

                    v.X = p1.X - p0.X 'Determine direction
                    v.Y = p1.Y - p0.Y

                    length = Sqrt(v.X * v.X + v.Y * v.Y) 'Normalize
                    v.X = v.X / length
                    v.Y = v.Y / length

                    a = v.X 'Make perpendicular
                    v.X = -v.Y
                    v.Y = a

                    'Calculate desired buffer width by measuring distance between (p0+v) and p0
                    dist = GisControlView.GetPointToPointDistance(lp.CS, p0.X + v.X, p0.Y + v.Y, p0.X, p0.Y)
                    width = 1 / dist * _lineWidth

                    tmp = lp.CreateShape(TGIS_ShapeType.gisShapeTypeArc)
                    tmp.AddPart()
                    tmp.AddPoint(p0)
                    tmp.AddPoint(p1)
                    buf = tpl.MakeBuffer(tmp, width * 0.5)
                    tmp.Delete()
                    lp.AddShape(buf)
                Next
            End If
        Else
            'If it's a polygon we don't need to expand it
            lp.AddShape(shp)
        End If


        'List any intersecting properties (check against all property layers)
        _shapeIntersectionList.Clear()
        cmd.CommandText = "SELECT name FROM gis_layers WHERE type = " & RelationType.PropertyRelation
        reader = cmd.ExecuteReader()
        Do While reader.Read()
            'Check each property layer
            ll = _gisCtrl.Get(reader.GetString(0))
            If ll IsNot Nothing Then
                idlst.Clear()
                ll.DeselectAll()

                'Cache property relations (since property id begin with id 0 we need to access the db table to distinguish NULL from 0)
                propIds.Clear()
                cmd2.CommandText = "SELECT " & GisControlView.ColumnNameUid & "," & GisControlView.FieldNameRelation & " FROM " & GisControlView.GisTablePrefix & ll.Name & "_FEA"
                reader2 = cmd2.ExecuteReader()
                Do While reader2.Read()
                    If Not reader2.IsDBNull(1) Then propIds.Add(reader2.GetInt32(0), reader2.GetInt32(1))
                Loop
                reader2.Close()

                'Check each shape in layer against each segment of line buffer
                _gisCtrl.Lock() 'Lock viewer so looping won't be interrupted by any paint event
                buf = lp.FindFirst(lp.ProjectedExtent, "", Nothing, "", True)
                While buf IsNot Nothing
                    tmp = ll.FindFirst(buf.ProjectedExtent, "", Nothing, "", True)
                    While tmp IsNot Nothing
                        'Make sure property still exist
                        If propIds.ContainsKey(tmp.Uid) Then propId = propIds(tmp.Uid) Else propId = -1
                        If _properties.ContainsKey(propId) AndAlso buf.IsCommonPoint(tmp) Then
                            'Only add properties to list once (layer might contain several shapes belonging to the same property)
                            value = _properties(propId)
                            If Not propIdlst.Contains(propId) Then 'The same value can be added several times (e.g. different property number but same name)
                                PropertyList.Items.Add(value)
                                propIdlst.Add(propId)
                            End If
                            idlst.Add(tmp.Uid)
                            _shapeIntersectionList.Add(New ShapeBufferPair(tmp.Uid, tmp.Layer.Name, buf.Uid))
                        End If
                        tmp = ll.FindNext()
                    End While
                    buf = lp.FindNext()
                End While
                _gisCtrl.Unlock()

                'Highlight intersecting properties
                If chkHighlight.Checked Then
                    For Each id As Integer In idlst
                        ll.GetShape(id).MakeEditable().IsSelected = True
                    Next
                    ll.InvalidateScope(vbNullString, vbNullString)
                End If
            End If
        Loop
        reader.Close()

        _lastWidth = _lineWidth 'Remember width used in last update

        'Update view and count label
        lblCount.Text = PropertyList.Items.Count & " " & GisControlView.LangStr(_strIdPropertyCount)
        _gisCtrl.Update()
    End Sub
#End Region

#Region "Event handlers"
    Private Sub ObjectDialog_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'Cleanup
        If _gisCtrl.Get(GisControlView.PreviewLayerName) IsNot Nothing Then
            _gisCtrl.Delete(GisControlView.PreviewLayerName)
        End If

        'Deselect all shapes in all property layers
        Dim cmd As New OleDbCommand("", GisControlView.Conn)
        Dim reader As OleDbDataReader
        Dim ll As TGIS_LayerVector

        cmd.CommandText = "SELECT name FROM gis_layers WHERE type = " & RelationType.PropertyRelation
        reader = cmd.ExecuteReader()
        Do While reader.Read()
            ll = _gisCtrl.Get(reader.GetString(0))
            If ll IsNot Nothing Then ll.DeselectAll()
        Loop
        reader.Close()
    End Sub

    Private Sub ObjectDialog_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim cmd As New OleDbCommand("", GisControlView.Conn)
        Dim reader As OleDbDataReader
        Dim idx As Integer = -1
        Dim ll As TGIS_LayerVector
        Dim shp As TGIS_Shape
        Dim rt As RelationType

        'Set up language dependent strings
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        lblDestinationLayer.Text = GisControlView.LangStr(_strIdDestinationLayer)
        lblWidth.Text = GisControlView.LangStr(_strIdWidth)
        uxOK.Text = GisControlView.LangStr(_strIdOK)
        uxCancel.Text = GisControlView.LangStr(_strIdCancel)
        lblProperties.Text = GisControlView.LangStr(_strIdProperties)
        lblUnit.Text = GisControlView.Instance.GetScaleUnit().Symbol
        chkHighlight.Text = GisControlView.LangStr(_strIdHighlight)
        uxAction.Text = GisControlView.LangStr(_strIdAction)
        uxNewObject.Text = GisControlView.LangStr(_strIdNewObject)
        uxExistingObject.Text = GisControlView.LangStr(_strIdExistingObject)
        uxNoAction.Text = GisControlView.LangStr(_strIdNoAction)
        uxUpdate.Text = GisControlView.LangStr(_strIdUpdate)

        'Cache property names
        cmd.CommandText = "SELECT id, CASE WHEN block_number <> '' OR unit_number <> '' THEN prop_name + ' ' + block_number + ':' + unit_number ELSE prop_name END FROM fst_property_table"
        reader = cmd.ExecuteReader()
        Do While reader.Read()
            _properties.Add(reader.GetInt32(0), reader.GetString(1))
        Loop
        reader.Close()

        'List possible destination layers
        cmbDestinationLayer.Items.Add(New NameCaptionPair(vbNullString, GisControlView.LangStr(_strIdNewLayer)))
        For Each la As TGIS_LayerAbstract In _gisCtrl.Items
            rt = GisControlView.GetLayerRelationType(la.Name)
            If TypeOf la Is TGIS_LayerVector AndAlso (rt = RelationType.ObjectRelation OrElse rt = RelationType.Undefined) AndAlso la.Name <> GisControlView.PreviewLayerName AndAlso _
            CType(la, TGIS_LayerVector).SupportedShapes And TGIS_ShapeType.gisShapeTypePolygon Then
                idx = cmbDestinationLayer.Items.Add(New NameCaptionPair(la.Name, la.Caption))
                If la.Name = _sourceLayerName Then cmbDestinationLayer.SelectedIndex = idx 'Select active layer
            End If
        Next

        'List existing objects
        cmd.CommandText = "SELECT object_id, object_name + ' (' + object_id_number + ')' FROM elv_object_table ORDER BY object_id DESC"
        reader = cmd.ExecuteReader()
        Do While reader.Read()
            uxObjectList.Items.Add(New ValueDescriptionPair(reader.GetInt32(0), reader.GetString(1)))
        Loop
        reader.Close()
        If uxObjectList.Items.Count > 0 Then uxObjectList.SelectedIndex = 0 'Select first item
        UpdateForm() 'Enable existing object option

        'Pick a default layer (if active layer wasn't listed)
        If cmbDestinationLayer.SelectedIndex = -1 Then
            If cmbDestinationLayer.Items.Count > 1 Then
                cmbDestinationLayer.SelectedIndex = 1
            Else
                cmbDestinationLayer.SelectedIndex = 0
            End If
        End If

        'Check shape type
        ll = _gisCtrl.Get(_sourceLayerName)
        If ll IsNot Nothing Then
            shp = ll.GetShape(_sourceShapeUid)
            If shp IsNot Nothing Then
                'Disable expand controls if we aren't working with a line
                If shp.ShapeType <> TGIS_ShapeType.gisShapeTypeArc Then
                    txtWidth.Enabled = False
                End If
            End If
        End If

        'Init preview
        UpdatePreview()
    End Sub

    Private Sub BtnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxOK.Click
        Dim cmd As New OleDbCommand("", GisControlView.Conn)
        Dim reader As OleDbDataReader
        Dim propId As Integer, objId As Integer, uid As Integer
        Dim destLayer As TGIS_LayerVector, ll As TGIS_LayerVector, lp As TGIS_LayerVector
        Dim propRelation As New Hashtable
        Dim shp As TGIS_Shape, buf As TGIS_Shape
        Dim tmp As ShapeBufferPair
        Dim val As NameCaptionPair

        'Make sure cached property list are up to date
        If _lastWidth <> CSng(txtWidth.Text) Then UpdatePreview()

        'Get a handle to destination layer, store name of destination layer
        val = CType(cmbDestinationLayer.SelectedItem, NameCaptionPair)
        If val.Name = vbNullString Then
            'Create a new layer
            _destinationLayerName = GisControlView.Instance.AddNewLayer(False, vbNullString, RelationType.ObjectRelation)
        Else
            'Use specified layer
            _destinationLayerName = val.Name
        End If
        destLayer = _gisCtrl.Get(_destinationLayerName)

        lp = GisControlView.GetPreviewLayer()

        'Check if we need to update relation type of destination layer
        If GisControlView.GetLayerRelationType(_destinationLayerName) = RelationType.Undefined Then
            ll = _gisCtrl.Get(_destinationLayerName)
            GisControlView.UpdateLayerRelation(ll.Name, ll.Caption, ll.CS.EPSG, RelationType.ObjectRelation)
            ll.SaveAll()
        End If

        'Loop through each intersecting property (cached since last update)
        For Each tmp In _shapeIntersectionList
            'Make sure the property still exist in property table
            ll = _gisCtrl.Get(tmp.LayerName)
            shp = ll.GetShape(tmp.ShapeUid)

            'Check property relations against db (since property id begin with id 0 we need to access the db table to distinguish NULL from 0)
            propId = -1
            cmd.CommandText = "SELECT " & GisControlView.FieldNameRelation & " FROM " & GisControlView.GisTablePrefix & tmp.LayerName & "_FEA WHERE " & GisControlView.ColumnNameUid & "=" & tmp.ShapeUid
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                If Not reader.IsDBNull(0) Then propId = reader.GetInt32(0)
            End If
            reader.Close()
            If propId < 0 Then Continue For

            If _properties.ContainsKey(propId) Then
                'Add intersection to destination layer
                buf = lp.GetShape(tmp.BufferUid)
                uid = destLayer.AddShape(buf.Intersection(shp)).Uid

                'Add property id to list only if we're gonna create a new object
                If uxNewObject.Checked Then
                    'Store shape-property id pair (so we can set up the relation later)
                    _shapePropertyList.Add(New IntIntPair(uid, propId))

                    'Only add properties to list once (layer may contain several shapes belonging to the same property)
                    If Not _propertyIdList.Contains(propId) Then _propertyIdList.Add(propId)
                ElseIf uxExistingObject.Checked Then
                    objId = CType(uxObjectList.SelectedItem, ValueDescriptionPair).Value

                    'Add property to object (if not already listed)
                    cmd.CommandText = "IF NOT EXISTS(SELECT 1 FROM elv_properties_table WHERE prop_id = " & propId & " AND prop_object_id = " & objId & ")" & _
                        "INSERT INTO elv_properties_table (prop_id, prop_object_id, created) VALUES(" & propId & "," & objId & ", CURRENT_TIMESTAMP)"
                    cmd.ExecuteNonQuery()

                    'Bind shape to object/property
                    shp = destLayer.GetShape(uid).MakeEditable()
                    shp.SetField(GisControlView.FieldNameRelation, objId)
                    shp.SetField(GisControlView.FieldNameSubrelation, objId & "/" & propId) 'Combined 'ObjectId/PropertyId'
                End If
            End If
        Next

        destLayer.SaveAll() 'Save any changes

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub txtWidth_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtWidth.KeyPress
        If e.KeyChar = ControlChars.Back Then Exit Sub 'Skip backspace

        'Disable any keys but 0 - 9 and one decimal delimiter (,)
        If (Asc(e.KeyChar) < Asc("0") Or Asc(e.KeyChar) > Asc("9")) And e.KeyChar <> "," Then
            e.Handled = True
        ElseIf e.KeyChar = "," And InStr(txtWidth.Text, ",") Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtWidth_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWidth.TextChanged
        'Update width value & preview
        If IsNumeric(txtWidth.Text) Then
            _lineWidth = CSng(txtWidth.Text)
            If _lineWidth < 0 Then
                _lineWidth = 0
                txtWidth.Text = CStr(_lineWidth)
            ElseIf _lineWidth > 10000 Then
                _lineWidth = 10000
                txtWidth.Text = CStr(_lineWidth)
            End If
        End If
    End Sub

    Private Sub uxUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxUpdate.Click
        UpdatePreview()
    End Sub

    Private Sub uxNewObject_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxNewObject.CheckedChanged
        UpdateForm()
    End Sub

    Private Sub uxExistingObject_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxExistingObject.CheckedChanged
        UpdateForm()
    End Sub

    Private Sub uxNoAction_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxNoAction.CheckedChanged
        UpdateForm()
    End Sub
#End Region
End Class
