﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RelationDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RelationDialog))
        Me.uxCancel = New System.Windows.Forms.Button
        Me.uxOK = New System.Windows.Forms.Button
        Me.uxRemoveButton = New System.Windows.Forms.Button
        Me.uxSubWidgetCombo = New System.Windows.Forms.ComboBox
        Me.uxSubWidgetLabel = New System.Windows.Forms.Label
        Me.uxWidgetReport = New AxXtremeReportControl.AxReportControl
        Me.uxFilterLabel = New System.Windows.Forms.Label
        Me.uxFilterText = New System.Windows.Forms.TextBox
        Me.uxUnconnectedOnly = New System.Windows.Forms.CheckBox
        CType(Me.uxWidgetReport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'uxCancel
        '
        Me.uxCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.uxCancel.Location = New System.Drawing.Point(93, 369)
        Me.uxCancel.Name = "uxCancel"
        Me.uxCancel.Size = New System.Drawing.Size(75, 23)
        Me.uxCancel.TabIndex = 3
        Me.uxCancel.Text = "uxCancel"
        Me.uxCancel.UseVisualStyleBackColor = True
        '
        'uxOK
        '
        Me.uxOK.Location = New System.Drawing.Point(12, 369)
        Me.uxOK.Name = "uxOK"
        Me.uxOK.Size = New System.Drawing.Size(75, 23)
        Me.uxOK.TabIndex = 2
        Me.uxOK.Text = "uxOK"
        Me.uxOK.UseVisualStyleBackColor = True
        '
        'uxRemoveButton
        '
        Me.uxRemoveButton.Location = New System.Drawing.Point(205, 369)
        Me.uxRemoveButton.Name = "uxRemoveButton"
        Me.uxRemoveButton.Size = New System.Drawing.Size(75, 23)
        Me.uxRemoveButton.TabIndex = 4
        Me.uxRemoveButton.Text = "uxRemoveButton"
        Me.uxRemoveButton.UseVisualStyleBackColor = True
        '
        'uxSubWidgetCombo
        '
        Me.uxSubWidgetCombo.DropDownHeight = 300
        Me.uxSubWidgetCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.uxSubWidgetCombo.FormattingEnabled = True
        Me.uxSubWidgetCombo.IntegralHeight = False
        Me.uxSubWidgetCombo.Location = New System.Drawing.Point(12, 339)
        Me.uxSubWidgetCombo.Name = "uxSubWidgetCombo"
        Me.uxSubWidgetCombo.Size = New System.Drawing.Size(268, 21)
        Me.uxSubWidgetCombo.TabIndex = 1
        '
        'uxSubWidgetLabel
        '
        Me.uxSubWidgetLabel.AutoSize = True
        Me.uxSubWidgetLabel.Location = New System.Drawing.Point(12, 323)
        Me.uxSubWidgetLabel.Name = "uxSubWidgetLabel"
        Me.uxSubWidgetLabel.Size = New System.Drawing.Size(97, 13)
        Me.uxSubWidgetLabel.TabIndex = 24
        Me.uxSubWidgetLabel.Text = "uxSubWidgetLabel"
        '
        'uxWidgetReport
        '
        Me.uxWidgetReport.Location = New System.Drawing.Point(12, 12)
        Me.uxWidgetReport.Name = "uxWidgetReport"
        Me.uxWidgetReport.OcxState = CType(resources.GetObject("uxWidgetReport.OcxState"), System.Windows.Forms.AxHost.State)
        Me.uxWidgetReport.Size = New System.Drawing.Size(268, 250)
        Me.uxWidgetReport.TabIndex = 5
        '
        'uxFilterLabel
        '
        Me.uxFilterLabel.Location = New System.Drawing.Point(12, 297)
        Me.uxFilterLabel.Name = "uxFilterLabel"
        Me.uxFilterLabel.Size = New System.Drawing.Size(47, 17)
        Me.uxFilterLabel.TabIndex = 25
        Me.uxFilterLabel.Text = "uxFilterLabel"
        '
        'uxFilterText
        '
        Me.uxFilterText.Location = New System.Drawing.Point(65, 294)
        Me.uxFilterText.Name = "uxFilterText"
        Me.uxFilterText.Size = New System.Drawing.Size(215, 20)
        Me.uxFilterText.TabIndex = 0
        '
        'uxUnconnectedOnly
        '
        Me.uxUnconnectedOnly.AutoSize = True
        Me.uxUnconnectedOnly.Location = New System.Drawing.Point(12, 268)
        Me.uxUnconnectedOnly.Name = "uxUnconnectedOnly"
        Me.uxUnconnectedOnly.Size = New System.Drawing.Size(123, 17)
        Me.uxUnconnectedOnly.TabIndex = 6
        Me.uxUnconnectedOnly.Text = "uxUnconnectedOnly"
        Me.uxUnconnectedOnly.UseVisualStyleBackColor = True
        '
        'RelationDialog
        '
        Me.AcceptButton = Me.uxOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.uxCancel
        Me.ClientSize = New System.Drawing.Size(292, 400)
        Me.Controls.Add(Me.uxUnconnectedOnly)
        Me.Controls.Add(Me.uxFilterText)
        Me.Controls.Add(Me.uxFilterLabel)
        Me.Controls.Add(Me.uxWidgetReport)
        Me.Controls.Add(Me.uxSubWidgetCombo)
        Me.Controls.Add(Me.uxSubWidgetLabel)
        Me.Controls.Add(Me.uxRemoveButton)
        Me.Controls.Add(Me.uxCancel)
        Me.Controls.Add(Me.uxOK)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "RelationDialog"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "RelationDialog"
        CType(Me.uxWidgetReport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents uxCancel As System.Windows.Forms.Button
    Friend WithEvents uxOK As System.Windows.Forms.Button
    Friend WithEvents uxRemoveButton As System.Windows.Forms.Button
    Friend WithEvents uxSubWidgetCombo As System.Windows.Forms.ComboBox
    Friend WithEvents uxSubWidgetLabel As System.Windows.Forms.Label
    Friend WithEvents uxWidgetReport As AxXtremeReportControl.AxReportControl
    Friend WithEvents uxFilterLabel As System.Windows.Forms.Label
    Friend WithEvents uxFilterText As System.Windows.Forms.TextBox
    Friend WithEvents uxUnconnectedOnly As System.Windows.Forms.CheckBox
End Class
