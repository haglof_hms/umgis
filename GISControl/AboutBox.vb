﻿Imports System.Reflection

Public NotInheritable Class AboutBox
#Region "Language"
    Private Const _strIdFormCaption As Integer = 2300
    Private Const _strIdProductName As Integer = 2301
    Private Const _strIdVersion As Integer = 2302
    Private Const _strIdOK As Integer = 2303
#End Region

#Region "Event handlers"
    Private Sub AboutBox_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Set up some text
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        Me.LabelProductName.Text = GisControlView.LangStr(_strIdProductName)
        Me.LabelVersion.Text = GisControlView.LangStr(_strIdVersion) & " " & Assembly.GetExecutingAssembly().GetName().Version.ToString()
        Me.LabelCopyright.Text = CType(Assembly.GetExecutingAssembly().GetCustomAttributes(GetType(AssemblyCopyrightAttribute), False)(0), AssemblyCopyrightAttribute).Copyright
        Me.OKButton.Text = GisControlView.LangStr(_strIdOK)
    End Sub

    Private Sub OKButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OKButton.Click
        Me.Close()
    End Sub
#End Region
End Class
