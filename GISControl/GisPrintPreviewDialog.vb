﻿Imports System.Drawing.Printing
Imports System.Windows.Forms

Public Class GisPrintPreviewDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 1500
    Private Const _strIdTitle As Integer = 1501
    Private Const _strIdDisplayTitle As Integer = 1502
    Private Const _strIdUpdatePreview As Integer = 1503
    Private Const _strIdFont As Integer = 1504
    Private Const _strIdPrint As Integer = 1505
    Private Const _strIdPageSetup As Integer = 1506
    Private Const _strIdNoPrinters As Integer = 1507
    Private Const _strIdSave As Integer = 1508
#End Region

#Region "Declarations"
    Private _action As Action
    Private _doc As PrintDocument

    Enum Action As Integer
        Print
        Snapshot
        File
    End Enum

    Public Sub New(Optional ByVal action As Action = Action.Print)
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _action = action
        _doc = New PrintDocument
        uxPrintPreview.GIS_Viewer = GisControlView.Instance.GisCtrl

        If _action = GisPrintPreviewDialog.Action.Print Then 'Print
            'Make sure at least one printer is installed
            If PrinterSettings.InstalledPrinters.Count = 0 Then
                MsgBox(GisControlView.LangStr(_strIdNoPrinters), MsgBoxStyle.Exclamation)
                Me.Close()
            End If
        End If
    End Sub
#End Region

#Region "Helper functions"
    Private Sub ShowSaveDialog()
        Dim dlgSave As New SaveFileDialog
        Dim format As Imaging.ImageFormat

        Preview() 'Apply settings first

        'Save image to a temporary file
        Dim fileName As String
        If _action = Action.Snapshot Then
            fileName = System.IO.Path.GetTempFileName()
            fileName = Mid(fileName, 1, fileName.LastIndexOf("."c)) & ".png" 'Use png extension instead of tmp
        Else
            dlgSave.Filter = "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" & _
                             "PNG (*.png)|*.png|" & _
                             "TIF (*.tif)|*.tif|" & _
                             "BMP (*.bmp)|*.bmp"
            If dlgSave.ShowDialog() = Windows.Forms.DialogResult.Cancel Then Exit Sub
            fileName = dlgSave.FileName

            Select Case LCase(Microsoft.VisualBasic.Right(fileName, 4))
                Case ".jpg", "jpeg"
                    format = Imaging.ImageFormat.Jpeg
                Case ".png"
                    format = Imaging.ImageFormat.Png
                Case ".tif"
                    format = Imaging.ImageFormat.Tiff
                Case ".bmp"
                    format = Imaging.ImageFormat.Bmp
                Case Else
                    format = Imaging.ImageFormat.Bmp
            End Select
        End If

        Dim _gisCtrl As TatukGIS.NDK.WinForms.TGIS_ViewerWnd
        Dim _controlLegend As TatukGIS.NDK.WinForms.TGIS_ControlLegend
        _gisCtrl = GisControlView.Instance.GisCtrl
        _controlLegend = GisControlView.Instance.ControlLegend

        'We need to make our own image to include the legend
        Dim bmp As New System.Drawing.Bitmap(_gisCtrl.Width + _controlLegend.Width, _gisCtrl.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb)

        _gisCtrl.DrawToBitmap(bmp, New Rectangle(0, 0, _gisCtrl.Width, bmp.Height))

        'Draw legend (match viewer back color)
        Dim oldBackcolor As Color = _controlLegend.BackColor
        _controlLegend.BackColor = _gisCtrl.BackColor
        _controlLegend.DrawToBitmap(bmp, New Rectangle(_gisCtrl.Width, 0, _controlLegend.Width, _controlLegend.Height))
        _controlLegend.BackColor = oldBackcolor

        bmp.Save(fileName, Imaging.ImageFormat.Png)

        'Show snapshot save dialog
        If _action = Action.Snapshot Then
            Dim dlg As New SnapshotDialog(fileName, uxTitleText.Text)
            If dlg.ShowDialog() = DialogResult.OK Then
                Me.DialogResult = Windows.Forms.DialogResult.OK
                Me.Close()
            End If
        End If
    End Sub

    Private Sub ShowPrintDialog()
        Preview() 'Apply settings first

        Dim dlg As New PrintDialog()
        dlg.Document = _doc
        If dlg.ShowDialog() = DialogResult.OK Then
            uxPrintPreview.GIS_Viewer.Print(_doc)
            Me.DialogResult = DialogResult.OK
            Me.Close()
        End If
    End Sub

    Private Sub ShowPageSetupDialog()
        Dim dlg As New PageSetupDialog()
        dlg.Document = _doc
        If dlg.ShowDialog() = DialogResult.OK Then
            Preview()
        End If
    End Sub

    Private Sub Preview()
        'Title
        If uxDisplayTitleCheck.Checked Then
            uxPrintPreview.GIS_Viewer.PrintTitle = uxTitleText.Text
        Else
            uxPrintPreview.GIS_Viewer.PrintTitle = vbNullString
        End If

        'Scale
        Try
            GisControlView.Instance.PrintScale = 1.0 / CDbl(uxScaleText.Text)
        Catch ex As InvalidCastException
            GisControlView.Instance.PrintScale = 0
        End Try

        uxPrintPreview.Preview(_doc, 1)
    End Sub
#End Region

#Region "Event handlers"
    Private Sub GisPrintPreviewDialog_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Set up language
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        uxTitleLabel.Text = GisControlView.LangStr(_strIdTitle)
        uxDisplayTitleCheck.Text = GisControlView.LangStr(_strIdDisplayTitle)
        uxUpdateButton.Text = GisControlView.LangStr(_strIdUpdatePreview)
        uxFontButton.Text = GisControlView.LangStr(_strIdFont)
        uxPageSetupButton.Text = GisControlView.LangStr(_strIdPageSetup)
        Select Case _action
            Case Action.Print
                uxPrintButton.Text = GisControlView.LangStr(_strIdPrint)
            Case Action.Snapshot, Action.File
                uxPrintButton.Text = GisControlView.LangStr(_strIdSave)
        End Select

        uxTitleText.Text = uxPrintPreview.GIS_Viewer.PrintTitle
        uxScaleText.Text = Math.Round(1.0 / GisControlView.Instance.GisCtrl.ScaleAsFloat)
        Preview()
    End Sub

    Private Sub GisPrintPreviewDialog_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'Keep title (even when not displayed)
        uxPrintPreview.GIS_Viewer.PrintTitle = uxTitleText.Text
    End Sub

    Private Sub GisPrintPreviewDialog_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        Const offsetY As Integer = 60
        uxMainPanel.SetBounds(0, 0, Me.Width, Me.Height)
        uxPrintPreview.SetBounds(0, offsetY, uxMainPanel.Width, uxMainPanel.Height - offsetY - 30) 'Position relative to Panel
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxUpdateButton.Click
        Preview()
    End Sub

    Private Sub uxFontButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxFontButton.Click
        Dim dlg As New FontDialog
        dlg.Font = uxPrintPreview.GIS_Viewer.PrintTitleFont
        If dlg.ShowDialog() = DialogResult.OK Then
            uxPrintPreview.GIS_Viewer.PrintTitleFont = dlg.Font
            Preview()
        End If
    End Sub

    Private Sub uxPrintButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxPrintButton.Click
        Select Case _action
            Case Action.Print
                ShowPrintDialog()
            Case Action.Snapshot, Action.File
                ShowSaveDialog()
        End Select
    End Sub

    Private Sub uxPageSetupButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxPageSetupButton.Click
        ShowPageSetupDialog()
    End Sub
#End Region
End Class
