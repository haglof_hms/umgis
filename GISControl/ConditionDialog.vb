﻿Imports TatukGIS.NDK

Public Class ConditionDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 300
    Private Const _strIdDescription As Integer = 301
    Private Const _strIdAnd As Integer = 302
    Private Const _strIdOr As Integer = 303
    Private Const _strIdInstructions1 As Integer = 304
    Private Const _strIdInstructions2 As Integer = 305
    Private Const _strIdRemove As Integer = 306
    Private Const _strIdOK As Integer = 307
    Private Const _strIdCancel As Integer = 308
    Private Const _strIdEqual As Integer = 309
    Private Const _strIdNotEqual As Integer = 310
    Private Const _strIdLessThan As Integer = 311
    Private Const _strIdLessThanOrEqual As Integer = 312
    Private Const _strIdGreaterThan As Integer = 313
    Private Const _strIdGreaterThanOrEqual As Integer = 314
    Private Const _strIdBlank As Integer = 315
    Private Const _strIdNotBlank As Integer = 316
    Private Const _strIdLike As Integer = 317
    Private Const _strIdNotLike As Integer = 318
    Private Const _strIdAndOrMissing As Integer = 319
    Private Const _strIdSecondConditionMissing As Integer = 320
#End Region

#Region "Declarations"
    Private _fieldType As TGIS_FieldType
    Private _removeCondition

    Public Sub New(ByVal field As String, ByVal type As TGIS_FieldType, ByVal condition As ConditionSet)
        Dim i As Integer

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _fieldType = type
        lblField.Text = field

        'List possible conditions
        cmbCondition1.Items.Add(GisControlView.LangStr(_strIdEqual))
        cmbCondition1.Items.Add(GisControlView.LangStr(_strIdNotEqual))
        cmbCondition1.Items.Add(GisControlView.LangStr(_strIdLessThan))
        cmbCondition1.Items.Add(GisControlView.LangStr(_strIdLessThanOrEqual))
        cmbCondition1.Items.Add(GisControlView.LangStr(_strIdGreaterThan))
        cmbCondition1.Items.Add(GisControlView.LangStr(_strIdGreaterThanOrEQUAL))
        cmbCondition1.Items.Add(GisControlView.LangStr(_strIdBlank))
        cmbCondition1.Items.Add(GisControlView.LangStr(_strIdNotBlank))
        If _fieldType = TGIS_FieldType.gisFieldTypeString Then
            cmbCondition1.Items.Add(GisControlView.LangStr(_strIdLike))
            cmbCondition1.Items.Add(GisControlView.LangStr(_strIdNotLike))
        End If

        'Copy values to the other combo box
        cmbCondition2.Items.Add("")
        For i = 0 To cmbCondition1.Items.Count - 1
            cmbCondition2.Items.Add(cmbCondition1.Items(i).ToString())
        Next

        'Load current set up (if set)
        If condition IsNot Nothing Then
            'First condition
            If _fieldType = TGIS_FieldType.gisFieldTypeDate AndAlso condition.Condition1 = 8 Then
                cmbCondition1.SelectedIndex = 0 'Substitue LIKE for =
            ElseIf _fieldType = TGIS_FieldType.gisFieldTypeDate AndAlso condition.Condition1 = 9 Then
                cmbCondition1.SelectedIndex = 1 'Substitute NOT LIKE for <>
            Else
                cmbCondition1.SelectedIndex = condition.Condition1
            End If

            'Second condition
            If _fieldType = TGIS_FieldType.gisFieldTypeDate AndAlso condition.Condition2 = 8 Then
                cmbCondition2.SelectedIndex = 1 'Substitue LIKE for =
            ElseIf _fieldType = TGIS_FieldType.gisFieldTypeDate AndAlso condition.Condition2 = 9 Then
                cmbCondition2.SelectedIndex = 2 'Substitute NOT LIKE for <>
            Else
                cmbCondition2.SelectedIndex = condition.Condition2 + 1
            End If

            'AND/OR
            If condition.CommonOperator = "AND" Then
                btnAnd.Checked = True
            ElseIf condition.CommonOperator = "OR" Then
                btnOr.Checked = True
            End If

            'Data values
            txtValue1.Text = condition.Value1
            txtValue2.Text = condition.Value2
        Else
            'Set up default selection
            If _fieldType = TGIS_FieldType.gisFieldTypeString Then
                cmbCondition1.SelectedIndex = 8 'LIKE
            Else
                cmbCondition1.SelectedIndex = 0 'Equal
            End If
        End If
    End Sub

    Public ReadOnly Property ConditionSet() As ConditionSet
        Get
            Dim condition As New ConditionSet

            'Return a conditionet with the condition set up
            'First condition
            If _fieldType = TGIS_FieldType.gisFieldTypeDate AndAlso cmbCondition1.SelectedIndex = 0 Then
                condition.Condition1 = 8 'Substitute = for LIKE
            ElseIf _fieldType = TGIS_FieldType.gisFieldTypeDate AndAlso cmbCondition1.SelectedIndex = 1 Then
                condition.Condition1 = 9 'Substitute <> for NOT LIKE
            Else
                condition.Condition1 = cmbCondition1.SelectedIndex
            End If

            'Second condition
            If _fieldType = TGIS_FieldType.gisFieldTypeDate AndAlso cmbCondition2.SelectedIndex = 1 Then
                condition.Condition2 = 8 'Substitute = for LIKE
            ElseIf _fieldType = TGIS_FieldType.gisFieldTypeDate AndAlso cmbCondition2.SelectedIndex = 2 Then
                condition.Condition2 = 9 'Substitute <> for NOT LIKE
            Else
                condition.Condition2 = cmbCondition2.SelectedIndex - 1
            End If

            'AND/OR
            If btnAnd.Checked Then
                condition.CommonOperator = "AND"
            ElseIf btnOr.Checked Then
                condition.CommonOperator = "OR"
            End If

            'Data values, if second condition is not set clear second value and operator
            condition.Value1 = txtValue1.Text
            If condition.Condition2 >= 0 Then
                condition.Value2 = txtValue2.Text
            Else
                condition.Value2 = vbNullString
                condition.CommonOperator = vbNullString
            End If

            Return condition
        End Get
    End Property

    Public Property RemoveCondition() As Boolean
        Get
            Return _removeCondition
        End Get
        Set(ByVal value As Boolean)
            _removeCondition = value
        End Set
    End Property
#End Region

#Region "Helper functions"
    Protected Sub UpdateInstructions()
        'Show instructions for LIKE/NOT LIKE if selected
        If cmbCondition1.SelectedIndex > 7 OrElse cmbCondition2.SelectedIndex > 8 Then
            lblInstructions.Visible = True
        Else
            lblInstructions.Visible = False
        End If
    End Sub
#End Region

#Region "Event handlers"
    Private Sub ConditionDialog_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If _fieldType = TGIS_FieldType.gisFieldTypeNumber OrElse _fieldType = TGIS_FieldType.gisFieldTypeFloat OrElse _fieldType = TGIS_FieldType.gisFieldTypeBoolean Then
            'These message handlers will prevent non-numeric strings from being pasted into these textboxes
            Dim OnPasteValue1 As New TextBoxOnPaste(Me.txtValue1)
            Dim OnPasteValue2 As New TextBoxOnPaste(Me.txtValue2)
        End If

        'Load language dependent string
        Me.Text = GisControlView.LangStr(_strIDFormCaption)
        lblDescription.Text = GisControlView.LangStr(_strIDDescription)
        btnAnd.Text = GisControlView.LangStr(_strIDAnd)
        btnOr.Text = GisControlView.LangStr(_strIDOr)
        btnRemove.Text = GisControlView.LangStr(_strIDRemove)
        btnOK.Text = GisControlView.LangStr(_strIDOK)
        btnCancel.Text = GisControlView.LangStr(_strIDCancel)
        lblInstructions.Text = GisControlView.LangStr(_strIDInstructions1) & vbCrLf & GisControlView.LangStr(_strIDInstructions2)

        UpdateInstructions()
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        _removeCondition = True
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        'Validation check
        If cmbCondition2.SelectedIndex > 0 AndAlso btnAnd.Checked = False AndAlso btnOr.Checked = False Then
            MsgBox(GisControlView.LangStr(_strIDAndOrMissing), MsgBoxStyle.Exclamation)
            Exit Sub
        ElseIf cmbCondition2.SelectedIndex < 1 AndAlso (btnAnd.Checked = True OrElse btnOr.Checked = True) Then
            MsgBox(GisControlView.LangStr(_strIdSecondConditionMissing), MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub txtValue1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtValue1.KeyPress
        'Filter input (only numeric values for numeric fields)
        If _fieldType = TGIS_FieldType.gisFieldTypeNumber OrElse _fieldType = TGIS_FieldType.gisFieldTypeFloat OrElse _fieldType = TGIS_FieldType.gisFieldTypeBoolean Then
            If Not Char.IsControl(e.KeyChar) AndAlso (Asc(e.KeyChar) < 48 OrElse Asc(e.KeyChar) > 57) Then
                e.KeyChar = vbNullChar
            End If
        End If
    End Sub

    Private Sub txtValue2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtValue2.KeyPress
        'Filter input (only numeric values for numeric fields)
        If _fieldType = TGIS_FieldType.gisFieldTypeNumber OrElse _fieldType = TGIS_FieldType.gisFieldTypeFloat OrElse _fieldType = TGIS_FieldType.gisFieldTypeBoolean Then
            If Not Char.IsControl(e.KeyChar) AndAlso (Asc(e.KeyChar) < 48 OrElse Asc(e.KeyChar) > 57) Then
                e.KeyChar = vbNullChar
            End If
        End If
    End Sub

    Private Sub cmbCondition1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCondition1.SelectedIndexChanged
        UpdateInstructions()
    End Sub

    Private Sub cmbCondition2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCondition2.SelectedIndexChanged
        UpdateInstructions()
    End Sub
#End Region
End Class
