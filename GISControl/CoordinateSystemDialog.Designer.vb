﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CoordinateSystemDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CoordinateSystemDialog))
        Me.uxPCS = New System.Windows.Forms.ComboBox
        Me.uxCancel = New System.Windows.Forms.Button
        Me.uxOK = New System.Windows.Forms.Button
        Me.uxGCS = New System.Windows.Forms.ComboBox
        Me.uxPCSOption1 = New System.Windows.Forms.RadioButton
        Me.uxGCSOption = New System.Windows.Forms.RadioButton
        Me.uxFilterText = New System.Windows.Forms.TextBox
        Me.uxFilterLabel = New System.Windows.Forms.Label
        Me.uxCSReport = New AxXtremeReportControl.AxReportControl
        CType(Me.uxCSReport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'uxPCS
        '
        Me.uxPCS.DropDownHeight = 300
        Me.uxPCS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.uxPCS.FormattingEnabled = True
        Me.uxPCS.IntegralHeight = False
        Me.uxPCS.Location = New System.Drawing.Point(12, 28)
        Me.uxPCS.Name = "uxPCS"
        Me.uxPCS.Size = New System.Drawing.Size(268, 21)
        Me.uxPCS.TabIndex = 1
        '
        'uxCancel
        '
        Me.uxCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.uxCancel.Location = New System.Drawing.Point(149, 448)
        Me.uxCancel.Name = "uxCancel"
        Me.uxCancel.Size = New System.Drawing.Size(75, 23)
        Me.uxCancel.TabIndex = 8
        Me.uxCancel.Text = "uxCancel"
        Me.uxCancel.UseVisualStyleBackColor = True
        '
        'uxOK
        '
        Me.uxOK.Location = New System.Drawing.Point(68, 448)
        Me.uxOK.Name = "uxOK"
        Me.uxOK.Size = New System.Drawing.Size(75, 23)
        Me.uxOK.TabIndex = 7
        Me.uxOK.Text = "uxOK"
        Me.uxOK.UseVisualStyleBackColor = True
        '
        'uxGCS
        '
        Me.uxGCS.DropDownHeight = 300
        Me.uxGCS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.uxGCS.FormattingEnabled = True
        Me.uxGCS.IntegralHeight = False
        Me.uxGCS.Location = New System.Drawing.Point(12, 75)
        Me.uxGCS.Name = "uxGCS"
        Me.uxGCS.Size = New System.Drawing.Size(268, 21)
        Me.uxGCS.TabIndex = 3
        '
        'uxPCSOption1
        '
        Me.uxPCSOption1.AutoSize = True
        Me.uxPCSOption1.Location = New System.Drawing.Point(12, 8)
        Me.uxPCSOption1.Name = "uxPCSOption1"
        Me.uxPCSOption1.Size = New System.Drawing.Size(88, 17)
        Me.uxPCSOption1.TabIndex = 0
        Me.uxPCSOption1.TabStop = True
        Me.uxPCSOption1.Text = "uxPCSOption"
        Me.uxPCSOption1.UseVisualStyleBackColor = True
        '
        'uxGCSOption
        '
        Me.uxGCSOption.AutoSize = True
        Me.uxGCSOption.Location = New System.Drawing.Point(12, 55)
        Me.uxGCSOption.Name = "uxGCSOption"
        Me.uxGCSOption.Size = New System.Drawing.Size(89, 17)
        Me.uxGCSOption.TabIndex = 2
        Me.uxGCSOption.TabStop = True
        Me.uxGCSOption.Text = "uxGCSOption"
        Me.uxGCSOption.UseVisualStyleBackColor = True
        '
        'uxFilterText
        '
        Me.uxFilterText.Location = New System.Drawing.Point(12, 128)
        Me.uxFilterText.Name = "uxFilterText"
        Me.uxFilterText.Size = New System.Drawing.Size(268, 20)
        Me.uxFilterText.TabIndex = 5
        '
        'uxFilterLabel
        '
        Me.uxFilterLabel.AutoSize = True
        Me.uxFilterLabel.Location = New System.Drawing.Point(12, 112)
        Me.uxFilterLabel.Name = "uxFilterLabel"
        Me.uxFilterLabel.Size = New System.Drawing.Size(66, 13)
        Me.uxFilterLabel.TabIndex = 9
        Me.uxFilterLabel.Text = "uxFilterLabel"
        '
        'uxCSReport
        '
        Me.uxCSReport.Location = New System.Drawing.Point(12, 154)
        Me.uxCSReport.Name = "uxCSReport"
        Me.uxCSReport.OcxState = CType(resources.GetObject("uxCSReport.OcxState"), System.Windows.Forms.AxHost.State)
        Me.uxCSReport.Size = New System.Drawing.Size(268, 288)
        Me.uxCSReport.TabIndex = 6
        '
        'CoordinateSystemDialog
        '
        Me.AcceptButton = Me.uxOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.uxCancel
        Me.ClientSize = New System.Drawing.Size(292, 478)
        Me.Controls.Add(Me.uxFilterText)
        Me.Controls.Add(Me.uxCSReport)
        Me.Controls.Add(Me.uxFilterLabel)
        Me.Controls.Add(Me.uxGCS)
        Me.Controls.Add(Me.uxPCS)
        Me.Controls.Add(Me.uxGCSOption)
        Me.Controls.Add(Me.uxPCSOption1)
        Me.Controls.Add(Me.uxCancel)
        Me.Controls.Add(Me.uxOK)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "CoordinateSystemDialog"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "ProjectionSettingsDialog"
        CType(Me.uxCSReport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents uxPCS As System.Windows.Forms.ComboBox
    Friend WithEvents uxCancel As System.Windows.Forms.Button
    Friend WithEvents uxOK As System.Windows.Forms.Button
    Friend WithEvents uxGCS As System.Windows.Forms.ComboBox
    Friend WithEvents uxPCSOption1 As System.Windows.Forms.RadioButton
    Friend WithEvents uxGCSOption As System.Windows.Forms.RadioButton
    Friend WithEvents uxFilterText As System.Windows.Forms.TextBox
    Friend WithEvents uxFilterLabel As System.Windows.Forms.Label
    Friend WithEvents uxCSReport As AxXtremeReportControl.AxReportControl
End Class
