﻿Imports TatukGIS.NDK

Public Class LayerMultiChoiceDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 1800
    Private Const _strIdOK As Integer = 1801
    Private Const _strIdCancel As Integer = 1802
#End Region

#Region "Declarations"
    Private _excludeLayer As String

    Public Sub New(ByVal description As String, ByVal excludeLayer As String)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        uxDescriptionLabel.Text = description
        _excludeLayer = excludeLayer
    End Sub
#End Region

#Region "Helper functions"
    Public ReadOnly Property SelectedLayers() As Collection
        Get
            Dim c As New Collection
            For Each n As NameCaptionPair In uxLayers.SelectedItems
                c.Add(n.Name)
            Next
            Return c
        End Get
    End Property
#End Region

#Region "Event handlers"
    Private Sub LayerMultiChoiceDialog_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ll As TGIS_LayerAbstract

        'Set up language
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        uxOK.Text = GisControlView.LangStr(_strIdOK)
        uxCancel.Text = GisControlView.LangStr(_strIdCancel)

        'List all visible vector layers (in reverse order to match legend)
        For i As Integer = GisControlView.Instance.GisCtrl.Items.Count - 1 To 0 Step -1
            ll = GisControlView.Instance.GisCtrl.Items(i)
            If ll.Active AndAlso ll.Name <> _excludeLayer AndAlso TypeOf ll Is TGIS_LayerVector Then
                uxLayers.Items.Add(New NameCaptionPair(ll.Name, ll.Caption))
            End If
        Next
    End Sub
#End Region
End Class
