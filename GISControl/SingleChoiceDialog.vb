﻿Imports System.Windows.Forms

Public Class SingleChoiceDialog
#Region "Language"
    Private Const _strIdOK As Integer = 2500
    Private Const _strIdCancel As Integer = 2501
#End Region

#Region "Declarations"
    Private _items As ValueDescriptionPair()
    Private _defindex As Integer

    Public Sub New(ByVal caption As String, ByVal items As List(Of ValueDescriptionPair), Optional ByVal defindex As Integer = 0)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.Text = caption
        _items = items.ToArray()
        _defindex = defindex
    End Sub

    Public ReadOnly Property Choice() As Integer
        Get
            Return CType(uxChoice.SelectedItem, ValueDescriptionPair).Value
        End Get
    End Property
#End Region

#Region "Event handlers"
    Private Sub SingleChoiceDialog_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uxOK.Text = GisControlView.LangStr(_strIdOK)
        uxCancel.Text = GisControlView.LangStr(_strIdCancel)

        uxChoice.Items.AddRange(_items)
        uxChoice.SelectedIndex = _defindex
    End Sub

    Private Sub uxChoice_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxChoice.SelectedIndexChanged
        uxOK.Enabled = (uxChoice.SelectedIndex >= 0)
    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub
#End Region
End Class
