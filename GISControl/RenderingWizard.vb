﻿Imports TatukGIS.NDK
Imports System.Data.OleDb

Public Class RenderingWizard
#Region "Language"
    Private Const _strIdFormCaption As Integer = 1700
    Private Const _strIdNext As Integer = 1701
    Private Const _strIdPrevious As Integer = 1702
    Private Const _strIdApply As Integer = 1703
    Private Const _strIdCancel As Integer = 1704
    Private Const _strIdField As Integer = 1705
    Private Const _strIdUniqueValues As Integer = 1706
    Private Const _strIdContinousValues As Integer = 1707
    Private Const _strIdUseAverageValue As Integer = 1708
    Private Const _strIdUseLogScale As Integer = 1709
    Private Const _strIdFeatures As Integer = 1710
    Private Const _strIdMarker As Integer = 1711
    Private Const _strIdLine As Integer = 1712
    Private Const _strIdArea As Integer = 1713
    Private Const _strIdRenderBy As Integer = 1714
    Private Const _strIdSizeWidth As Integer = 1715
    Private Const _strIdColor As Integer = 1716
    Private Const _strIdOutlineWidth As Integer = 1717
    Private Const _strIdOutlineColor As Integer = 1718
    Private Const _strIdFirstValues As Integer = 1719
    Private Const _strIdMinVal As Integer = 1720
    Private Const _strIdMaxVal As Integer = 1721
#End Region

#Region "Declarations"
    Private _ll As TGIS_LayerVector
    Private _view As JoinView
    Private _tabIndex As Integer

    Public Sub New(ByVal ll As TGIS_LayerVector, ByVal view As JoinView)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _ll = ll
        _view = view
    End Sub
#End Region

#Region "Helper functions"
    Private Sub ApplySettings()
        Dim i As Integer
        Dim pv As TGIS_ParamsSectionVector

        _ll.ParamsList.Clear() 'First item is reserved and won't be removed

        'Show affected shape types in legend
        If uxMarker.Checked Then _ll.Params.Marker.ShowLegend = True
        If uxLine.Checked Then _ll.Params.Line.ShowLegend = True
        If uxArea.Checked Then _ll.Params.Area.ShowLegend = True

        If uxUniqueValues.Checked Then
            'Unique value
            For i = 0 To uxValues.Items.Count - 1
                _ll.ParamsList.Add()
                pv = CType(_ll.ParamsList.Items(i + 1), TGIS_ParamsSectionVector)

                'Query
                Dim query As String = "[" & uxField.SelectedItem & "]='" & uxValues.Items(i) & "'"
                If Trim(pv.Query) <> vbNullString Then
                    pv.Query = "(" & pv.Query & ") AND (" & query & ")"
                Else
                    pv.Query = query
                End If

                'Legend
                pv.Legend = uxValues.Items(i)
                pv.Marker.ShowLegend = uxMarker.Checked
                pv.Line.ShowLegend = uxLine.Checked
                pv.Area.ShowLegend = uxArea.Checked

                'Generate new color, size
                Dim color As System.Drawing.Color = System.Drawing.Color.FromArgb(RGB(Rnd() * 255, Rnd() * 255, Rnd() * 255))
                Dim size As Double = i + 1

                If uxSizeWidth.Checked Then
                    If uxMarker.Checked Then pv.Marker.Size = size
                    If uxLine.Checked Then pv.Line.Width = size
                End If
                If uxColor.Checked Then
                    If uxMarker.Checked Then pv.Marker.Color = color
                    If uxLine.Checked Then pv.Line.Color = color
                    If uxArea.Checked Then pv.Area.Color = color
                End If
                If uxOutlineWidth.Checked Then
                    If uxMarker.Checked Then pv.Marker.OutlineWidth = size
                    If uxLine.Checked Then pv.Line.OutlineWidth = size
                    If uxArea.Checked Then pv.Area.OutlineWidth = size
                End If
                If uxOutlineColor.Checked Then
                    If uxMarker.Checked Then pv.Marker.OutlineColor = color
                    If uxLine.Checked Then pv.Line.OutlineColor = color
                    If uxArea.Checked Then pv.Area.OutlineColor = color
                End If
            Next
        Else
            'Continuous values
            pv = _ll.ParamsList.Items(0)
            pv.Render.Expression = uxField.SelectedItem

            'Zones, scale
            If uxUseLogarithmicScale.Checked Then
                pv.Render.Zones = -5
            Else
                pv.Render.Zones = 5
            End If

            'Min/max values
            If uxUseAverageValue.Checked Then
                If Not IsNumeric(uxMinVal.Text) Then uxMinVal.Text = "0"
                If Not IsNumeric(uxMaxVal.Text) Then uxMaxVal.Text = "0"
                If Not IsNumeric(uxAverageValue.Text) Then uxAverageValue.Text = "0"

                pv.Render.MinValEx = uxMinVal.Text
                pv.Render.MaxValEx = uxAverageValue.Text
                pv.Render.MinVal = uxAverageValue.Text
                pv.Render.MaxVal = uxMaxVal.Text
                pv.Render.ZonesEx = pv.Render.Zones
            Else
                pv.Render.MinVal = uxMinVal.Text
                pv.Render.MaxVal = uxMaxVal.Text
            End If

            If uxSizeWidth.Checked Then
                If uxMarker.Checked Then pv.Marker.Size = TGIS_Utils.GIS_RENDER_SIZE()
                If uxLine.Checked Then pv.Line.Width = TGIS_Utils.GIS_RENDER_SIZE()
            End If
            If uxColor.Checked Then
                If uxMarker.Checked Then pv.Marker.Color = TGIS_Utils.GIS_RENDER_COLOR()
                If uxLine.Checked Then pv.Line.Color = TGIS_Utils.GIS_RENDER_COLOR()
                If uxArea.Checked Then pv.Area.Color = TGIS_Utils.GIS_RENDER_COLOR()
            End If
            If uxOutlineWidth.Checked Then
                If uxMarker.Checked Then pv.Marker.OutlineWidth = TGIS_Utils.GIS_RENDER_SIZE()
                If uxLine.Checked Then pv.Line.OutlineWidth = TGIS_Utils.GIS_RENDER_SIZE()
                If uxArea.Checked Then pv.Area.OutlineWidth = TGIS_Utils.GIS_RENDER_SIZE()
            End If
            If uxOutlineColor.Checked Then
                If uxMarker.Checked Then pv.Line.OutlineColor = TGIS_Utils.GIS_RENDER_COLOR()
                If uxLine.Checked Then pv.Line.OutlineColor = TGIS_Utils.GIS_RENDER_COLOR()
                If uxArea.Checked Then pv.Area.OutlineColor = TGIS_Utils.GIS_RENDER_COLOR()
            End If
        End If
    End Sub

    Private Sub SetCurrentTab(ByVal idx As Integer)
        Dim i As Integer

        'Move all controls back to tabcontrol
        For i = uxPanel.Controls.Count - 1 To 0 Step -1
            Dim c As Control = uxPanel.Controls(i)
            uxPanel.Controls.Remove(c)
            uxTabs.TabPages(_tabIndex).Controls.Add(c)
        Next

        _tabIndex = idx

        'Show controls in current tab
        For i = uxTabs.TabPages(_tabIndex).Controls.Count - 1 To 0 Step -1
            Dim c As Control = uxTabs.TabPages(_tabIndex).Controls(i)
            uxTabs.TabPages(_tabIndex).Controls.Remove(c)
            uxPanel.Controls.Add(c)
        Next

        'First page check
        uxPrevious.Enabled = True
        If _tabIndex = 0 Then uxPrevious.Enabled = False

        'Last page check, update button titles
        If _tabIndex >= uxTabs.TabCount - 1 Then
            uxPrevious.Text = GisControlView.LangStr(_strIdPrevious)
            uxNext.Text = GisControlView.LangStr(_strIdApply)
        Else
            uxPrevious.Text = GisControlView.LangStr(_strIdPrevious)
            uxNext.Text = GisControlView.LangStr(_strIdNext)
        End If
    End Sub

    Private Function GetFieldMinMax(ByVal fieldName As String) As MinMaxPair
        Dim minmax As MinMaxPair = Nothing

        If TypeOf _ll Is TGIS_LayerSqlAdo Then
            'Database layer, query the table directly
            Dim cmd As New OleDbCommand("", GisControlView.Conn)
            Dim reader As OleDbDataReader

            'Query min/max values, also include any joined data
            If fieldName = GisControlView.FieldNameGisUid Then fieldName = GisControlView.ColumnNameUid 'GIS_UID has internal column name UID
            cmd.CommandText = "SELECT CAST(MIN([" & fieldName & "]) AS float), CAST(MAX([" & fieldName & "]) AS float) FROM " & GisControlView.GisTablePrefix & _ll.Name & "_FEA f"
            If _view IsNot Nothing AndAlso _ll.FindField(_view.JoinPrimary) >= 0 Then 'Make sure relation field exist in layer (if relation is not applied yet field may not exist)
                cmd.CommandText += " LEFT JOIN " & _view.Name & " ON " & _view.Name & "." & GisControlView.ForeignKeyField & " = f." & _view.JoinPrimary
            End If
            Try 'Try-catch in case min/max is not numeric
                reader = cmd.ExecuteReader()
                If reader.Read() Then
                    If Not reader.IsDBNull(0) AndAlso Not reader.IsDBNull(1) Then
                        minmax = New MinMaxPair(reader.GetDouble(0), reader.GetDouble(1))
                    End If
                End If
            Catch ex As OleDbException
                'Conversion error expected, otherwise rethrow exception
                If ex.ErrorCode <> &H80040E07 Then Throw
            End Try
        Else
            'Layer is based on a file, we need to loop through all shapes
            Dim val As Object
            Dim shp As TGIS_Shape = _ll.FindFirst()
            Do Until shp Is Nothing
                val = shp.GetField(fieldName)
                If IsNumeric(val) Then
                    If minmax Is Nothing Then
                        'First value
                        minmax = New MinMaxPair(val, val)
                    Else
                        'Compare
                        If val < minmax.Min Then
                            minmax.Min = val
                        ElseIf val > minmax.Max Then
                            minmax.Max = val
                        End If
                    End If
                End If

                shp = _ll.FindNext()
            Loop
        End If

        Return minmax
    End Function

    Private Sub EnableRenderBy()
        Dim val As Boolean = uxMarker.Checked Or uxLine.Checked Or uxArea.Checked
        uxSizeWidth.Enabled = uxMarker.Checked Or uxLine.Checked
        uxColor.Enabled = val
        uxOutlineColor.Enabled = val
        uxOutlineWidth.Enabled = val
    End Sub
#End Region

#Region "Event handlers"
    Private Sub RenderingWizard_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Set up language
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        uxCancel.Text = GisControlView.LangStr(_strIdCancel)
        uxFieldLabel.Text = GisControlView.LangStr(_strIdField)
        uxValuesLabel.Text = GisControlView.LangStr(_strIdFirstValues)
        uxUniqueValues.Text = GisControlView.LangStr(_strIdUniqueValues)
        uxContinousValues.Text = GisControlView.LangStr(_strIdContinousValues)
        uxMinValLabel.Text = GisControlView.LangStr(_strIdMinVal)
        uxMaxValLabel.Text = GisControlView.LangStr(_strIdMaxVal)
        uxUseAverageValue.Text = GisControlView.LangStr(_strIdUseAverageValue)
        uxUseLogarithmicScale.Text = GisControlView.LangStr(_strIdUseLogScale)
        uxFeatures.Text = GisControlView.LangStr(_strIdFeatures)
        uxMarker.Text = GisControlView.LangStr(_strIdMarker)
        uxLine.Text = GisControlView.LangStr(_strIdLine)
        uxArea.Text = GisControlView.LangStr(_strIdArea)
        uxRenderBy.Text = GisControlView.LangStr(_strIdRenderBy)
        uxSizeWidth.Text = GisControlView.LangStr(_strIdSizeWidth)
        uxColor.Text = GisControlView.LangStr(_strIdColor)
        uxOutlineWidth.Text = GisControlView.LangStr(_strIdOutlineWidth)
        uxOutlineColor.Text = GisControlView.LangStr(_strIdOutlineColor)

        'List all fields
        uxField.Items.Add(GisControlView.FieldNameGisUid) 'Always add GIS_UID, this make sure internal column name UID is never listed
        For i As Integer = 0 To _ll.Fields.Count - 1
            If Not _ll.FieldInfo(i).IsUID AndAlso Not GisControlView.IsRelationField(_ll.FieldInfo(i).NewName) Then uxField.Items.Add(_ll.FieldInfo(i).NewName)
        Next
        If _ll.JoinNET IsNot Nothing Then
            Dim dt As DataTable = CType(_ll.JoinNET, DataTable)
            For i As Integer = 0 To dt.Columns.Count - 1
                'Add all fields but foreign key column
                If Not GisControlView.IsRelationField(dt.Columns(i).Caption) Then uxField.Items.Add(dt.Columns(i).Caption)
            Next
        End If
        uxField.SelectedIndex = 0 'Default selection

        'Display first tab
        SetCurrentTab(0)
    End Sub

    Private Sub uxField_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxField.SelectedIndexChanged
        Dim ft As TGIS_FieldType
        Dim i As Integer
        Dim shp As TGIS_Shape
        Dim fieldName As String, fieldColumnName As String

        'Fieldname, use internal column name UID instead of GIS_UID
        fieldName = uxField.SelectedItem
        fieldColumnName = GisControlView.GetFieldColumnName(uxField.SelectedItem, _ll, False)

        'Determine field type
        GisControlView.GetFieldType(_ll, fieldName, ft)

        If ft = TGIS_FieldType.gisFieldTypeNumber OrElse ft = TGIS_FieldType.gisFieldTypeFloat OrElse ft = TGIS_FieldType.gisFieldTypeBoolean Then
            'Enable continous values
            uxContinousValues.Checked = True
            uxContinousValues.Enabled = True

            'Numeric, determine min/max and average value
            Dim minmax As MinMaxPair = GetFieldMinMax(fieldColumnName)
            If minmax Is Nothing Then minmax = New MinMaxPair(0, 0)
            uxMinVal.Text = minmax.Min
            uxMaxVal.Text = minmax.Max
            uxAverageValue.Text = minmax.Min + (minmax.Max - minmax.Min) / 2.0
        Else
            'Disable continous values
            uxContinousValues.Enabled = False
            uxUniqueValues.Checked = True
        End If

        'List unique values
        Dim uval As New Hashtable
        If _ll.FindField(fieldName) >= 0 OrElse fieldName = GisControlView.FieldNameGisUid Then
            'Layer field
            For i = 1 To _ll.GetLastUid()
                shp = _ll.GetShape(i)
                If shp IsNot Nothing AndAlso Not uval.ContainsKey(shp.GetField(fieldName)) Then
                    uval.Add(shp.GetField(fieldName), Nothing)
                    If uval.Count > 99 Then Exit For
                End If
            Next
        Else
            'Make sure relation field exist in layer (if relation is not applied yet field may not exist)
            If _ll.FindField(_view.JoinPrimary) >= 0 Then
                'Join field (list only values joined with a shape)
                Dim cmd As New OleDbCommand("", GisControlView.Conn)
                Dim reader As OleDbDataReader

                cmd.CommandText = "SELECT DISTINCT TOP 100 [" & fieldColumnName & "] FROM " & GisControlView.GisTablePrefix & _ll.Name & "_FEA f LEFT JOIN " & _view.Name & " ON " & _view.Name & "." & GisControlView.ForeignKeyField & " = f." & GisControlView.GetJoinPrimary(_view.Name)
                reader = cmd.ExecuteReader()
                Do While reader.Read()
                    If Not IsDBNull(reader.GetValue(0)) Then uval.Add(reader.GetValue(0).ToString(), Nothing)
                Loop
            End If
        End If
        uxValues.Items.Clear()
        For Each val As Object In uval.Keys
            uxValues.Items.Add(val)
        Next
    End Sub

    Private Sub uxNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxNext.Click
        If _tabIndex < uxTabs.TabPages.Count - 1 Then
            SetCurrentTab(_tabIndex + 1)
        Else
            ApplySettings()
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        End If
    End Sub

    Private Sub uxBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxPrevious.Click
        If _tabIndex > 0 Then SetCurrentTab(_tabIndex - 1)
    End Sub

    Private Sub uxUseAverageValue_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxUseAverageValue.CheckedChanged
        uxAverageValue.Visible = uxUseAverageValue.Checked
    End Sub

    Private Sub uxUniqueValues_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxUniqueValues.CheckedChanged
        'Enable/disable controls in tab
        uxValues.Enabled = uxUniqueValues.Checked

        uxMinValLabel.Enabled = uxContinousValues.Checked
        uxMinVal.Enabled = uxContinousValues.Checked
        uxMaxValLabel.Enabled = uxContinousValues.Checked
        uxMaxVal.Enabled = uxContinousValues.Checked
        uxUseAverageValue.Enabled = uxContinousValues.Checked
        uxAverageValue.Enabled = uxContinousValues.Checked
        uxUseLogarithmicScale.Enabled = uxContinousValues.Checked
    End Sub

    Private Sub uxMarker_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxMarker.CheckedChanged
        EnableRenderBy()
    End Sub

    Private Sub uxLine_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxLine.CheckedChanged
        EnableRenderBy()
    End Sub

    Private Sub uxArea_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxArea.CheckedChanged
        EnableRenderBy()
    End Sub
#End Region
End Class
