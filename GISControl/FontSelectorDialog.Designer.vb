﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FontSelectorDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lbFonts = New System.Windows.Forms.ListBox
        Me.lblStyle = New System.Windows.Forms.Label
        Me.lblFont = New System.Windows.Forms.Label
        Me.lbStyles = New System.Windows.Forms.ListBox
        Me.gbEffects = New System.Windows.Forms.GroupBox
        Me.cbUnderline = New System.Windows.Forms.CheckBox
        Me.cbStrikeout = New System.Windows.Forms.CheckBox
        Me.lblCharacter = New System.Windows.Forms.Label
        Me.lvCharacters = New System.Windows.Forms.ListView
        Me.btnOk = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.gbEffects.SuspendLayout()
        Me.SuspendLayout()
        '
        'lbFonts
        '
        Me.lbFonts.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.lbFonts.FormattingEnabled = True
        Me.lbFonts.ItemHeight = 20
        Me.lbFonts.Location = New System.Drawing.Point(12, 24)
        Me.lbFonts.Name = "lbFonts"
        Me.lbFonts.Size = New System.Drawing.Size(207, 144)
        Me.lbFonts.TabIndex = 0
        '
        'lblStyle
        '
        Me.lblStyle.AutoSize = True
        Me.lblStyle.Location = New System.Drawing.Point(222, 7)
        Me.lblStyle.Name = "lblStyle"
        Me.lblStyle.Size = New System.Drawing.Size(40, 13)
        Me.lblStyle.TabIndex = 4
        Me.lblStyle.Text = "lblStyle"
        '
        'lblFont
        '
        Me.lblFont.AutoSize = True
        Me.lblFont.Location = New System.Drawing.Point(12, 7)
        Me.lblFont.Name = "lblFont"
        Me.lblFont.Size = New System.Drawing.Size(38, 13)
        Me.lblFont.TabIndex = 5
        Me.lblFont.Text = "lblFont"
        '
        'lbStyles
        '
        Me.lbStyles.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.lbStyles.FormattingEnabled = True
        Me.lbStyles.ItemHeight = 20
        Me.lbStyles.Location = New System.Drawing.Point(225, 24)
        Me.lbStyles.Name = "lbStyles"
        Me.lbStyles.Size = New System.Drawing.Size(108, 144)
        Me.lbStyles.TabIndex = 6
        '
        'gbEffects
        '
        Me.gbEffects.Controls.Add(Me.cbUnderline)
        Me.gbEffects.Controls.Add(Me.cbStrikeout)
        Me.gbEffects.Location = New System.Drawing.Point(339, 23)
        Me.gbEffects.Name = "gbEffects"
        Me.gbEffects.Size = New System.Drawing.Size(148, 70)
        Me.gbEffects.TabIndex = 7
        Me.gbEffects.TabStop = False
        Me.gbEffects.Text = "gbEffects"
        '
        'cbUnderline
        '
        Me.cbUnderline.AutoSize = True
        Me.cbUnderline.Location = New System.Drawing.Point(10, 45)
        Me.cbUnderline.Name = "cbUnderline"
        Me.cbUnderline.Size = New System.Drawing.Size(83, 17)
        Me.cbUnderline.TabIndex = 0
        Me.cbUnderline.Text = "cbUnderline"
        Me.cbUnderline.UseVisualStyleBackColor = True
        '
        'cbStrikeout
        '
        Me.cbStrikeout.AutoSize = True
        Me.cbStrikeout.Enabled = False
        Me.cbStrikeout.Location = New System.Drawing.Point(10, 22)
        Me.cbStrikeout.Name = "cbStrikeout"
        Me.cbStrikeout.Size = New System.Drawing.Size(80, 17)
        Me.cbStrikeout.TabIndex = 0
        Me.cbStrikeout.Text = "cbStrikeout"
        Me.cbStrikeout.UseVisualStyleBackColor = True
        '
        'lblCharacter
        '
        Me.lblCharacter.AutoSize = True
        Me.lblCharacter.Location = New System.Drawing.Point(9, 181)
        Me.lblCharacter.Name = "lblCharacter"
        Me.lblCharacter.Size = New System.Drawing.Size(63, 13)
        Me.lblCharacter.TabIndex = 8
        Me.lblCharacter.Text = "lblCharacter"
        '
        'lvCharacters
        '
        Me.lvCharacters.Alignment = System.Windows.Forms.ListViewAlignment.SnapToGrid
        Me.lvCharacters.AutoArrange = False
        Me.lvCharacters.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lvCharacters.HideSelection = False
        Me.lvCharacters.LabelWrap = False
        Me.lvCharacters.Location = New System.Drawing.Point(12, 198)
        Me.lvCharacters.MultiSelect = False
        Me.lvCharacters.Name = "lvCharacters"
        Me.lvCharacters.OwnerDraw = True
        Me.lvCharacters.ShowGroups = False
        Me.lvCharacters.Size = New System.Drawing.Size(475, 143)
        Me.lvCharacters.TabIndex = 9
        Me.lvCharacters.TileSize = New System.Drawing.Size(30, 30)
        Me.lvCharacters.UseCompatibleStateImageBehavior = False
        Me.lvCharacters.View = System.Windows.Forms.View.List
        Me.lvCharacters.VirtualMode = True
        '
        'btnOk
        '
        Me.btnOk.Location = New System.Drawing.Point(171, 357)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(75, 23)
        Me.btnOk.TabIndex = 11
        Me.btnOk.Text = "btnOk"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(252, 357)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 12
        Me.btnCancel.Text = "btnCancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'FontSelectorDialog
        '
        Me.AcceptButton = Me.btnOk
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(499, 392)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOk)
        Me.Controls.Add(Me.lvCharacters)
        Me.Controls.Add(Me.lblCharacter)
        Me.Controls.Add(Me.gbEffects)
        Me.Controls.Add(Me.lbStyles)
        Me.Controls.Add(Me.lblFont)
        Me.Controls.Add(Me.lblStyle)
        Me.Controls.Add(Me.lbFonts)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FontSelectorDialog"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "FontSelectorDialog"
        Me.gbEffects.ResumeLayout(False)
        Me.gbEffects.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lbFonts As System.Windows.Forms.ListBox
    Friend WithEvents lblStyle As System.Windows.Forms.Label
    Friend WithEvents lblFont As System.Windows.Forms.Label
    Friend WithEvents lbStyles As System.Windows.Forms.ListBox
    Friend WithEvents gbEffects As System.Windows.Forms.GroupBox
    Friend WithEvents cbUnderline As System.Windows.Forms.CheckBox
    Friend WithEvents cbStrikeout As System.Windows.Forms.CheckBox
    Friend WithEvents lblCharacter As System.Windows.Forms.Label
    Friend WithEvents lvCharacters As System.Windows.Forms.ListView
    Friend WithEvents btnOk As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button

End Class
