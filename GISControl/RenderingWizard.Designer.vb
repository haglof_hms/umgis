﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RenderingWizard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.uxTabs = New System.Windows.Forms.TabControl
        Me.uxTab1 = New System.Windows.Forms.TabPage
        Me.uxFieldLabel = New System.Windows.Forms.Label
        Me.uxField = New System.Windows.Forms.ComboBox
        Me.uxTab2 = New System.Windows.Forms.TabPage
        Me.uxValuesLabel = New System.Windows.Forms.Label
        Me.uxUseLogarithmicScale = New System.Windows.Forms.CheckBox
        Me.uxAverageValue = New System.Windows.Forms.TextBox
        Me.uxUseAverageValue = New System.Windows.Forms.CheckBox
        Me.uxMaxValLabel = New System.Windows.Forms.Label
        Me.uxMinValLabel = New System.Windows.Forms.Label
        Me.uxMaxVal = New System.Windows.Forms.TextBox
        Me.uxMinVal = New System.Windows.Forms.TextBox
        Me.uxValues = New System.Windows.Forms.ListBox
        Me.uxContinousValues = New System.Windows.Forms.RadioButton
        Me.uxUniqueValues = New System.Windows.Forms.RadioButton
        Me.uxTab3 = New System.Windows.Forms.TabPage
        Me.uxRenderBy = New System.Windows.Forms.GroupBox
        Me.uxOutlineColor = New System.Windows.Forms.CheckBox
        Me.uxOutlineWidth = New System.Windows.Forms.CheckBox
        Me.uxColor = New System.Windows.Forms.CheckBox
        Me.uxSizeWidth = New System.Windows.Forms.CheckBox
        Me.uxFeatures = New System.Windows.Forms.GroupBox
        Me.uxArea = New System.Windows.Forms.CheckBox
        Me.uxLine = New System.Windows.Forms.CheckBox
        Me.uxMarker = New System.Windows.Forms.CheckBox
        Me.uxPanel = New System.Windows.Forms.Panel
        Me.uxCancel = New System.Windows.Forms.Button
        Me.uxPrevious = New System.Windows.Forms.Button
        Me.uxNext = New System.Windows.Forms.Button
        Me.uxTabs.SuspendLayout()
        Me.uxTab1.SuspendLayout()
        Me.uxTab2.SuspendLayout()
        Me.uxTab3.SuspendLayout()
        Me.uxRenderBy.SuspendLayout()
        Me.uxFeatures.SuspendLayout()
        Me.SuspendLayout()
        '
        'uxTabs
        '
        Me.uxTabs.Controls.Add(Me.uxTab1)
        Me.uxTabs.Controls.Add(Me.uxTab2)
        Me.uxTabs.Controls.Add(Me.uxTab3)
        Me.uxTabs.Location = New System.Drawing.Point(7, 7)
        Me.uxTabs.Name = "uxTabs"
        Me.uxTabs.SelectedIndex = 0
        Me.uxTabs.Size = New System.Drawing.Size(344, 219)
        Me.uxTabs.TabIndex = 7
        Me.uxTabs.Visible = False
        '
        'uxTab1
        '
        Me.uxTab1.Controls.Add(Me.uxFieldLabel)
        Me.uxTab1.Controls.Add(Me.uxField)
        Me.uxTab1.Location = New System.Drawing.Point(4, 22)
        Me.uxTab1.Name = "uxTab1"
        Me.uxTab1.Padding = New System.Windows.Forms.Padding(3)
        Me.uxTab1.Size = New System.Drawing.Size(336, 193)
        Me.uxTab1.TabIndex = 0
        Me.uxTab1.Text = "uxTab1"
        Me.uxTab1.UseVisualStyleBackColor = True
        '
        'uxFieldLabel
        '
        Me.uxFieldLabel.Location = New System.Drawing.Point(6, 3)
        Me.uxFieldLabel.Name = "uxFieldLabel"
        Me.uxFieldLabel.Size = New System.Drawing.Size(278, 13)
        Me.uxFieldLabel.TabIndex = 1
        Me.uxFieldLabel.Text = "uxFieldLabel"
        '
        'uxField
        '
        Me.uxField.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.uxField.FormattingEnabled = True
        Me.uxField.Location = New System.Drawing.Point(7, 19)
        Me.uxField.Name = "uxField"
        Me.uxField.Size = New System.Drawing.Size(323, 21)
        Me.uxField.TabIndex = 2
        '
        'uxTab2
        '
        Me.uxTab2.Controls.Add(Me.uxValuesLabel)
        Me.uxTab2.Controls.Add(Me.uxUseLogarithmicScale)
        Me.uxTab2.Controls.Add(Me.uxAverageValue)
        Me.uxTab2.Controls.Add(Me.uxUseAverageValue)
        Me.uxTab2.Controls.Add(Me.uxMaxValLabel)
        Me.uxTab2.Controls.Add(Me.uxMinValLabel)
        Me.uxTab2.Controls.Add(Me.uxMaxVal)
        Me.uxTab2.Controls.Add(Me.uxMinVal)
        Me.uxTab2.Controls.Add(Me.uxValues)
        Me.uxTab2.Controls.Add(Me.uxContinousValues)
        Me.uxTab2.Controls.Add(Me.uxUniqueValues)
        Me.uxTab2.Location = New System.Drawing.Point(4, 22)
        Me.uxTab2.Name = "uxTab2"
        Me.uxTab2.Padding = New System.Windows.Forms.Padding(3)
        Me.uxTab2.Size = New System.Drawing.Size(336, 193)
        Me.uxTab2.TabIndex = 1
        Me.uxTab2.Text = "uxTab2"
        Me.uxTab2.UseVisualStyleBackColor = True
        '
        'uxValuesLabel
        '
        Me.uxValuesLabel.AutoSize = True
        Me.uxValuesLabel.Location = New System.Drawing.Point(13, 30)
        Me.uxValuesLabel.Name = "uxValuesLabel"
        Me.uxValuesLabel.Size = New System.Drawing.Size(76, 13)
        Me.uxValuesLabel.TabIndex = 8
        Me.uxValuesLabel.Text = "uxValuesLabel"
        '
        'uxUseLogarithmicScale
        '
        Me.uxUseLogarithmicScale.AutoSize = True
        Me.uxUseLogarithmicScale.Location = New System.Drawing.Point(178, 177)
        Me.uxUseLogarithmicScale.Name = "uxUseLogarithmicScale"
        Me.uxUseLogarithmicScale.Size = New System.Drawing.Size(137, 17)
        Me.uxUseLogarithmicScale.TabIndex = 7
        Me.uxUseLogarithmicScale.Text = "uxUseLogarithmicScale"
        Me.uxUseLogarithmicScale.UseVisualStyleBackColor = True
        '
        'uxAverageValue
        '
        Me.uxAverageValue.Location = New System.Drawing.Point(185, 147)
        Me.uxAverageValue.Name = "uxAverageValue"
        Me.uxAverageValue.Size = New System.Drawing.Size(120, 20)
        Me.uxAverageValue.TabIndex = 6
        Me.uxAverageValue.Visible = False
        '
        'uxUseAverageValue
        '
        Me.uxUseAverageValue.AutoSize = True
        Me.uxUseAverageValue.Location = New System.Drawing.Point(178, 124)
        Me.uxUseAverageValue.Name = "uxUseAverageValue"
        Me.uxUseAverageValue.Size = New System.Drawing.Size(123, 17)
        Me.uxUseAverageValue.TabIndex = 5
        Me.uxUseAverageValue.Text = "uxUseAverageValue"
        Me.uxUseAverageValue.UseVisualStyleBackColor = True
        '
        'uxMaxValLabel
        '
        Me.uxMaxValLabel.AutoSize = True
        Me.uxMaxValLabel.Location = New System.Drawing.Point(182, 78)
        Me.uxMaxValLabel.Name = "uxMaxValLabel"
        Me.uxMaxValLabel.Size = New System.Drawing.Size(79, 13)
        Me.uxMaxValLabel.TabIndex = 7
        Me.uxMaxValLabel.Text = "uxMaxValLabel"
        '
        'uxMinValLabel
        '
        Me.uxMinValLabel.AutoSize = True
        Me.uxMinValLabel.Location = New System.Drawing.Point(182, 30)
        Me.uxMinValLabel.Name = "uxMinValLabel"
        Me.uxMinValLabel.Size = New System.Drawing.Size(76, 13)
        Me.uxMinValLabel.TabIndex = 6
        Me.uxMinValLabel.Text = "uxMinValLabel"
        '
        'uxMaxVal
        '
        Me.uxMaxVal.Location = New System.Drawing.Point(185, 94)
        Me.uxMaxVal.Name = "uxMaxVal"
        Me.uxMaxVal.Size = New System.Drawing.Size(120, 20)
        Me.uxMaxVal.TabIndex = 4
        '
        'uxMinVal
        '
        Me.uxMinVal.Location = New System.Drawing.Point(185, 49)
        Me.uxMinVal.Name = "uxMinVal"
        Me.uxMinVal.Size = New System.Drawing.Size(120, 20)
        Me.uxMinVal.TabIndex = 3
        '
        'uxValues
        '
        Me.uxValues.FormattingEnabled = True
        Me.uxValues.Location = New System.Drawing.Point(16, 46)
        Me.uxValues.Name = "uxValues"
        Me.uxValues.Size = New System.Drawing.Size(122, 121)
        Me.uxValues.TabIndex = 1
        '
        'uxContinousValues
        '
        Me.uxContinousValues.AutoSize = True
        Me.uxContinousValues.Location = New System.Drawing.Point(178, 6)
        Me.uxContinousValues.Name = "uxContinousValues"
        Me.uxContinousValues.Size = New System.Drawing.Size(115, 17)
        Me.uxContinousValues.TabIndex = 2
        Me.uxContinousValues.Text = "uxContinousValues"
        Me.uxContinousValues.UseVisualStyleBackColor = True
        '
        'uxUniqueValues
        '
        Me.uxUniqueValues.AutoSize = True
        Me.uxUniqueValues.Checked = True
        Me.uxUniqueValues.Location = New System.Drawing.Point(6, 6)
        Me.uxUniqueValues.Name = "uxUniqueValues"
        Me.uxUniqueValues.Size = New System.Drawing.Size(102, 17)
        Me.uxUniqueValues.TabIndex = 0
        Me.uxUniqueValues.TabStop = True
        Me.uxUniqueValues.Text = "uxUniqueValues"
        Me.uxUniqueValues.UseVisualStyleBackColor = True
        '
        'uxTab3
        '
        Me.uxTab3.Controls.Add(Me.uxRenderBy)
        Me.uxTab3.Controls.Add(Me.uxFeatures)
        Me.uxTab3.Location = New System.Drawing.Point(4, 22)
        Me.uxTab3.Name = "uxTab3"
        Me.uxTab3.Padding = New System.Windows.Forms.Padding(3)
        Me.uxTab3.Size = New System.Drawing.Size(336, 193)
        Me.uxTab3.TabIndex = 2
        Me.uxTab3.Text = "uxTab3"
        Me.uxTab3.UseVisualStyleBackColor = True
        '
        'uxRenderBy
        '
        Me.uxRenderBy.Controls.Add(Me.uxOutlineColor)
        Me.uxRenderBy.Controls.Add(Me.uxOutlineWidth)
        Me.uxRenderBy.Controls.Add(Me.uxColor)
        Me.uxRenderBy.Controls.Add(Me.uxSizeWidth)
        Me.uxRenderBy.Location = New System.Drawing.Point(176, 7)
        Me.uxRenderBy.Name = "uxRenderBy"
        Me.uxRenderBy.Size = New System.Drawing.Size(144, 118)
        Me.uxRenderBy.TabIndex = 1
        Me.uxRenderBy.TabStop = False
        Me.uxRenderBy.Text = "uxRenderBy"
        '
        'uxOutlineColor
        '
        Me.uxOutlineColor.AutoSize = True
        Me.uxOutlineColor.Enabled = False
        Me.uxOutlineColor.Location = New System.Drawing.Point(6, 65)
        Me.uxOutlineColor.Name = "uxOutlineColor"
        Me.uxOutlineColor.Size = New System.Drawing.Size(94, 17)
        Me.uxOutlineColor.TabIndex = 7
        Me.uxOutlineColor.Text = "uxOutlineColor"
        Me.uxOutlineColor.UseVisualStyleBackColor = True
        '
        'uxOutlineWidth
        '
        Me.uxOutlineWidth.AutoSize = True
        Me.uxOutlineWidth.Enabled = False
        Me.uxOutlineWidth.Location = New System.Drawing.Point(6, 88)
        Me.uxOutlineWidth.Name = "uxOutlineWidth"
        Me.uxOutlineWidth.Size = New System.Drawing.Size(98, 17)
        Me.uxOutlineWidth.TabIndex = 6
        Me.uxOutlineWidth.Text = "uxOutlineWidth"
        Me.uxOutlineWidth.UseVisualStyleBackColor = True
        '
        'uxColor
        '
        Me.uxColor.AutoSize = True
        Me.uxColor.Enabled = False
        Me.uxColor.Location = New System.Drawing.Point(6, 42)
        Me.uxColor.Name = "uxColor"
        Me.uxColor.Size = New System.Drawing.Size(61, 17)
        Me.uxColor.TabIndex = 5
        Me.uxColor.Text = "uxColor"
        Me.uxColor.UseVisualStyleBackColor = True
        '
        'uxSizeWidth
        '
        Me.uxSizeWidth.AutoSize = True
        Me.uxSizeWidth.Enabled = False
        Me.uxSizeWidth.Location = New System.Drawing.Point(6, 19)
        Me.uxSizeWidth.Name = "uxSizeWidth"
        Me.uxSizeWidth.Size = New System.Drawing.Size(85, 17)
        Me.uxSizeWidth.TabIndex = 4
        Me.uxSizeWidth.Text = "uxSizeWidth"
        Me.uxSizeWidth.UseVisualStyleBackColor = True
        '
        'uxFeatures
        '
        Me.uxFeatures.Controls.Add(Me.uxArea)
        Me.uxFeatures.Controls.Add(Me.uxLine)
        Me.uxFeatures.Controls.Add(Me.uxMarker)
        Me.uxFeatures.Location = New System.Drawing.Point(7, 7)
        Me.uxFeatures.Name = "uxFeatures"
        Me.uxFeatures.Size = New System.Drawing.Size(144, 118)
        Me.uxFeatures.TabIndex = 0
        Me.uxFeatures.TabStop = False
        Me.uxFeatures.Text = "uxFeatures"
        '
        'uxArea
        '
        Me.uxArea.AutoSize = True
        Me.uxArea.Location = New System.Drawing.Point(6, 66)
        Me.uxArea.Name = "uxArea"
        Me.uxArea.Size = New System.Drawing.Size(59, 17)
        Me.uxArea.TabIndex = 3
        Me.uxArea.Text = "uxArea"
        Me.uxArea.UseVisualStyleBackColor = True
        '
        'uxLine
        '
        Me.uxLine.AutoSize = True
        Me.uxLine.Location = New System.Drawing.Point(6, 43)
        Me.uxLine.Name = "uxLine"
        Me.uxLine.Size = New System.Drawing.Size(57, 17)
        Me.uxLine.TabIndex = 2
        Me.uxLine.Text = "uxLine"
        Me.uxLine.UseVisualStyleBackColor = True
        '
        'uxMarker
        '
        Me.uxMarker.AutoSize = True
        Me.uxMarker.Location = New System.Drawing.Point(7, 20)
        Me.uxMarker.Name = "uxMarker"
        Me.uxMarker.Size = New System.Drawing.Size(70, 17)
        Me.uxMarker.TabIndex = 1
        Me.uxMarker.Text = "uxMarker"
        Me.uxMarker.UseVisualStyleBackColor = True
        '
        'uxPanel
        '
        Me.uxPanel.Location = New System.Drawing.Point(7, 7)
        Me.uxPanel.Name = "uxPanel"
        Me.uxPanel.Size = New System.Drawing.Size(344, 219)
        Me.uxPanel.TabIndex = 8
        '
        'uxCancel
        '
        Me.uxCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.uxCancel.Location = New System.Drawing.Point(114, 232)
        Me.uxCancel.Name = "uxCancel"
        Me.uxCancel.Size = New System.Drawing.Size(75, 23)
        Me.uxCancel.TabIndex = 100
        Me.uxCancel.Text = "uxCancel"
        Me.uxCancel.UseVisualStyleBackColor = True
        '
        'uxPrevious
        '
        Me.uxPrevious.Location = New System.Drawing.Point(195, 232)
        Me.uxPrevious.Name = "uxPrevious"
        Me.uxPrevious.Size = New System.Drawing.Size(75, 23)
        Me.uxPrevious.TabIndex = 101
        Me.uxPrevious.Text = "uxPrevious"
        Me.uxPrevious.UseVisualStyleBackColor = True
        '
        'uxNext
        '
        Me.uxNext.Location = New System.Drawing.Point(276, 232)
        Me.uxNext.Name = "uxNext"
        Me.uxNext.Size = New System.Drawing.Size(75, 23)
        Me.uxNext.TabIndex = 102
        Me.uxNext.Text = "uxNext"
        Me.uxNext.UseVisualStyleBackColor = True
        '
        'RenderingWizard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.uxCancel
        Me.ClientSize = New System.Drawing.Size(361, 264)
        Me.Controls.Add(Me.uxNext)
        Me.Controls.Add(Me.uxPrevious)
        Me.Controls.Add(Me.uxCancel)
        Me.Controls.Add(Me.uxTabs)
        Me.Controls.Add(Me.uxPanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "RenderingWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "RenderingWizard"
        Me.uxTabs.ResumeLayout(False)
        Me.uxTab1.ResumeLayout(False)
        Me.uxTab2.ResumeLayout(False)
        Me.uxTab2.PerformLayout()
        Me.uxTab3.ResumeLayout(False)
        Me.uxRenderBy.ResumeLayout(False)
        Me.uxRenderBy.PerformLayout()
        Me.uxFeatures.ResumeLayout(False)
        Me.uxFeatures.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents uxTabs As System.Windows.Forms.TabControl
    Friend WithEvents uxTab1 As System.Windows.Forms.TabPage
    Friend WithEvents uxTab2 As System.Windows.Forms.TabPage
    Friend WithEvents uxTab3 As System.Windows.Forms.TabPage
    Friend WithEvents uxPanel As System.Windows.Forms.Panel
    Friend WithEvents uxCancel As System.Windows.Forms.Button
    Friend WithEvents uxPrevious As System.Windows.Forms.Button
    Friend WithEvents uxNext As System.Windows.Forms.Button
    Friend WithEvents uxFieldLabel As System.Windows.Forms.Label
    Friend WithEvents uxField As System.Windows.Forms.ComboBox
    Friend WithEvents uxUseLogarithmicScale As System.Windows.Forms.CheckBox
    Friend WithEvents uxAverageValue As System.Windows.Forms.TextBox
    Friend WithEvents uxUseAverageValue As System.Windows.Forms.CheckBox
    Friend WithEvents uxMaxValLabel As System.Windows.Forms.Label
    Friend WithEvents uxMinValLabel As System.Windows.Forms.Label
    Friend WithEvents uxMaxVal As System.Windows.Forms.TextBox
    Friend WithEvents uxMinVal As System.Windows.Forms.TextBox
    Friend WithEvents uxValues As System.Windows.Forms.ListBox
    Friend WithEvents uxContinousValues As System.Windows.Forms.RadioButton
    Friend WithEvents uxUniqueValues As System.Windows.Forms.RadioButton
    Friend WithEvents uxRenderBy As System.Windows.Forms.GroupBox
    Friend WithEvents uxFeatures As System.Windows.Forms.GroupBox
    Friend WithEvents uxValuesLabel As System.Windows.Forms.Label
    Friend WithEvents uxArea As System.Windows.Forms.CheckBox
    Friend WithEvents uxLine As System.Windows.Forms.CheckBox
    Friend WithEvents uxMarker As System.Windows.Forms.CheckBox
    Friend WithEvents uxSizeWidth As System.Windows.Forms.CheckBox
    Friend WithEvents uxOutlineColor As System.Windows.Forms.CheckBox
    Friend WithEvents uxOutlineWidth As System.Windows.Forms.CheckBox
    Friend WithEvents uxColor As System.Windows.Forms.CheckBox
End Class
