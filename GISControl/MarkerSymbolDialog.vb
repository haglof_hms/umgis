﻿Imports TatukGIS.NDK

Public Class MarkerSymbolDialog
    Private _imageFile As String
    Private _symbology As TGIS_SymbolAbstract

#Region "Language"
    Private Const _strIdFormCaption As Integer = 3100
    Private Const _strIdFont As Integer = 3101
    Private Const _strIdPicture As Integer = 3102
    Private Const _strIdRemove As Integer = 3103
    Private Const _strIdTransparent As Integer = 3104
    Private Const _strIdOk As Integer = 3105
    Private Const _strIdCancel As Integer = 3106
#End Region

#Region "Declarations"
    Public Sub New(ByVal symbology As TGIS_SymbolAbstract)
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _symbology = symbology
        DrawPreview()
    End Sub

    Friend Property Symbology() As TGIS_SymbolAbstract
        Get
            Return _symbology
        End Get
        Set(ByVal symbology As TGIS_SymbolAbstract)
            _symbology = symbology
        End Set
    End Property
#End Region

#Region "Helper functions"
    Private Sub DrawPreview()
        'Check if it is a picture or font symbology that we have
        If TryCast(_symbology, TGIS_SymbolPicture) Is Nothing And TryCast(_symbology, TGIS_SymbolFont) Is Nothing Then
            Exit Sub
        End If

        'No need to draw preview if we are not on the polygon, line or point tab
        Try
            Dim viewerBmp As TGIS_ViewerBmp
            viewerBmp = New TGIS_ViewerBmp(128, 128)

            'Create a new hidden layer
            Dim lv As TGIS_LayerVector = Nothing
            lv = New TGIS_LayerVector
            lv.Name = "PreviewLayer"
            lv.AddField("Label", TGIS_FieldType.gisFieldTypeString, 5, 0)
            viewerBmp.Add(lv)

            'Copy parameters
            lv.Params.Marker.Symbol = _symbology
            If Not TryCast(_symbology, TGIS_SymbolFont) Is Nothing Then
                lv.Params.Marker.Size = 300
            End If

            Dim shp As TGIS_Shape
            shp = lv.CreateShape(TGIS_ShapeType.gisShapeTypePoint)
            shp.Lock(TGIS_Lock.gisLockExtent)
            shp.SetField("Label", "Label")
            shp.AddPart()
            shp.AddPoint(New TGIS_Point(64, 64))
            shp.Unlock()

            'Export layer to a bitmap
            viewerBmp.Center = New TGIS_Point(lv.Extent.XMax - lv.Extent.XMin / 2, lv.Extent.YMax - lv.Extent.YMin / 2)
            viewerBmp.Draw()
            Dim myMemoryStream As New System.IO.MemoryStream()
            viewerBmp.Bitmap.Save(myMemoryStream, System.Drawing.Imaging.ImageFormat.Bmp)

            'Populate PictureBox1 with exported bitmap
            pbPreview.Image = Image.FromStream(myMemoryStream)
            myMemoryStream.Close()
            myMemoryStream.Dispose()
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub
#End Region

#Region "Event handlers"
    Private Sub MarkerSymbolDialog_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Set up language dependent strings
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        btnFont.Text = GisControlView.LangStr(_strIdFont)
        btnPictureChoose.Text = GisControlView.LangStr(_strIdPicture)
        btnRemovePicture.Text = GisControlView.LangStr(_strIdRemove)
        cbTransparent.Text = GisControlView.LangStr(_strIdTransparent)
        btnOk.Text = GisControlView.LangStr(_strIdOk)
        btnCancel.Text = GisControlView.LangStr(_strIdCancel)

        If Not String.IsNullOrEmpty(_imageFile) Then
            pbPreview.Load(_imageFile)
        End If
    End Sub

    Private Sub btnPictureChoose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPictureChoose.Click
        'Show a picture chooser window
        Dim dlg As New OpenFileDialog()
        dlg.Filter = "BMP files (*.bmp)|*.bmp|" & _
                     "GIF files (*.gif)|*.gif|" & _
                     "JPEG files (*.jpg;*.jpeg)|*.jpg;*.jpeg|" & _
                     "PNG files (*.png)|*.png|" & _
                     "All supported formats|*.bmp;*.jpg;*.jpeg;*.png"
        dlg.FilterIndex = 5
        dlg.Multiselect = False

        If dlg.ShowDialog() = DialogResult.OK Then
            'Set image file as picture
            pbPreview.Load(dlg.FileName())

            _symbology = New TGIS_SymbolPicture(dlg.FileName())
        End If
    End Sub

    Private Sub btnRemovePicture_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemovePicture.Click
        'Remove the picture binding
        If Not pbPreview.Image Is Nothing Then
            pbPreview.Image = Nothing
            _imageFile = Nothing
        End If

        'Remove all symbology
        _symbology = Nothing
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub btnFont_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFont.Click
        'Select font and symbol to use
        Dim arg As String = vbNullString
        Dim sf As TGIS_SymbolFont
        Dim dlgFont As FontSelectorDialog

        sf = TryCast(_symbology, TGIS_SymbolFont)
        If sf IsNot Nothing Then arg = _symbology.Name
        dlgFont = New FontSelectorDialog(arg)

        If dlgFont.ShowDialog() = Windows.Forms.DialogResult.OK Then
            'Remove any picture
            pbPreview.Image = Nothing

            'Set the new font symbol style
            Dim tmp As TGIS_SymbolFont = New TGIS_SymbolFont(dlgFont.FontSelected)
            _symbology = tmp

            'Preview the font symbol in the picturebox
            DrawPreview()
        End If
    End Sub
#End Region
End Class
