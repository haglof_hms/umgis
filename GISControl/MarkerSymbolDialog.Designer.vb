﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MarkerSymbolDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnPictureChoose = New System.Windows.Forms.Button
        Me.btnOk = New System.Windows.Forms.Button
        Me.btnRemovePicture = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.pbPreview = New System.Windows.Forms.PictureBox
        Me.btnFont = New System.Windows.Forms.Button
        Me.cbTransparent = New System.Windows.Forms.CheckBox
        CType(Me.pbPreview, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnPictureChoose
        '
        Me.btnPictureChoose.Location = New System.Drawing.Point(157, 52)
        Me.btnPictureChoose.Name = "btnPictureChoose"
        Me.btnPictureChoose.Size = New System.Drawing.Size(75, 23)
        Me.btnPictureChoose.TabIndex = 4
        Me.btnPictureChoose.Text = "btnPictureChoose"
        Me.btnPictureChoose.UseVisualStyleBackColor = True
        '
        'btnOk
        '
        Me.btnOk.Location = New System.Drawing.Point(44, 153)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(75, 23)
        Me.btnOk.TabIndex = 1
        Me.btnOk.Text = "btnOk"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnRemovePicture
        '
        Me.btnRemovePicture.Location = New System.Drawing.Point(157, 81)
        Me.btnRemovePicture.Name = "btnRemovePicture"
        Me.btnRemovePicture.Size = New System.Drawing.Size(75, 23)
        Me.btnRemovePicture.TabIndex = 5
        Me.btnRemovePicture.Text = "btnRemovePicture"
        Me.btnRemovePicture.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(125, 153)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "btnCancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'pbPreview
        '
        Me.pbPreview.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pbPreview.Location = New System.Drawing.Point(12, 12)
        Me.pbPreview.Name = "pbPreview"
        Me.pbPreview.Size = New System.Drawing.Size(140, 102)
        Me.pbPreview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pbPreview.TabIndex = 4
        Me.pbPreview.TabStop = False
        '
        'btnFont
        '
        Me.btnFont.Location = New System.Drawing.Point(157, 23)
        Me.btnFont.Name = "btnFont"
        Me.btnFont.Size = New System.Drawing.Size(75, 23)
        Me.btnFont.TabIndex = 3
        Me.btnFont.Text = "btnFont"
        Me.btnFont.UseVisualStyleBackColor = True
        '
        'cbTransparent
        '
        Me.cbTransparent.AutoSize = True
        Me.cbTransparent.Location = New System.Drawing.Point(12, 126)
        Me.cbTransparent.Name = "cbTransparent"
        Me.cbTransparent.Size = New System.Drawing.Size(95, 17)
        Me.cbTransparent.TabIndex = 6
        Me.cbTransparent.Text = "cbTransparent"
        Me.cbTransparent.UseVisualStyleBackColor = True
        Me.cbTransparent.Visible = False
        '
        'MarkerSymbolDialog
        '
        Me.AcceptButton = Me.btnOk
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(244, 188)
        Me.Controls.Add(Me.cbTransparent)
        Me.Controls.Add(Me.btnFont)
        Me.Controls.Add(Me.pbPreview)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnRemovePicture)
        Me.Controls.Add(Me.btnOk)
        Me.Controls.Add(Me.btnPictureChoose)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "MarkerSymbolDialog"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "MarkerPictureDialog"
        CType(Me.pbPreview, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnPictureChoose As System.Windows.Forms.Button
    Friend WithEvents btnOk As System.Windows.Forms.Button
    Friend WithEvents btnRemovePicture As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents pbPreview As System.Windows.Forms.PictureBox
    Friend WithEvents btnFont As System.Windows.Forms.Button
    Friend WithEvents cbTransparent As System.Windows.Forms.CheckBox
End Class
