﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LayerMultiChoiceDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.uxCancel = New System.Windows.Forms.Button
        Me.uxOK = New System.Windows.Forms.Button
        Me.uxLayers = New System.Windows.Forms.ListBox
        Me.uxDescriptionLabel = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'uxCancel
        '
        Me.uxCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.uxCancel.Location = New System.Drawing.Point(128, 301)
        Me.uxCancel.Name = "uxCancel"
        Me.uxCancel.Size = New System.Drawing.Size(75, 23)
        Me.uxCancel.TabIndex = 5
        Me.uxCancel.Text = "uxCancel"
        Me.uxCancel.UseVisualStyleBackColor = True
        '
        'uxOK
        '
        Me.uxOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.uxOK.Location = New System.Drawing.Point(47, 301)
        Me.uxOK.Name = "uxOK"
        Me.uxOK.Size = New System.Drawing.Size(75, 23)
        Me.uxOK.TabIndex = 4
        Me.uxOK.Text = "uxOK"
        Me.uxOK.UseVisualStyleBackColor = True
        '
        'uxLayers
        '
        Me.uxLayers.FormattingEnabled = True
        Me.uxLayers.Location = New System.Drawing.Point(13, 26)
        Me.uxLayers.Name = "uxLayers"
        Me.uxLayers.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.uxLayers.Size = New System.Drawing.Size(226, 264)
        Me.uxLayers.TabIndex = 6
        '
        'uxDescriptionLabel
        '
        Me.uxDescriptionLabel.AutoSize = True
        Me.uxDescriptionLabel.Location = New System.Drawing.Point(13, 13)
        Me.uxDescriptionLabel.Name = "uxDescriptionLabel"
        Me.uxDescriptionLabel.Size = New System.Drawing.Size(97, 13)
        Me.uxDescriptionLabel.TabIndex = 7
        Me.uxDescriptionLabel.Text = "uxDescriptionLabel"
        '
        'LayerMultiChoiceDialog
        '
        Me.AcceptButton = Me.uxOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.uxCancel
        Me.ClientSize = New System.Drawing.Size(251, 336)
        Me.Controls.Add(Me.uxLayers)
        Me.Controls.Add(Me.uxDescriptionLabel)
        Me.Controls.Add(Me.uxCancel)
        Me.Controls.Add(Me.uxOK)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "LayerMultiChoiceDialog"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "LayerMultiChoiceDialog"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents uxCancel As System.Windows.Forms.Button
    Friend WithEvents uxOK As System.Windows.Forms.Button
    Friend WithEvents uxLayers As System.Windows.Forms.ListBox
    Friend WithEvents uxDescriptionLabel As System.Windows.Forms.Label
End Class
