﻿Imports TatukGIS.NDK
Imports System.Windows.Forms

Public Class ExportAttributeDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 3700
    Private Const _strIdOK As Integer = 3701
    Private Const _strIdCancel As Integer = 3702
    Private Const _strIdPrevious As Integer = 3703
    Private Const _strIdNext As Integer = 3704
    Private Const _strIdToggle As Integer = 3705
#End Region

#Region "Declarations"
    Private _layers As List(Of LayerShapeList)
    Private _tabIndex As Integer
    Private _attributes As Hashtable
    Private _toggle As List(Of Boolean)

    Public Sub New(ByVal layers As List(Of LayerShapeList))
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _layers = layers
        _attributes = New Hashtable
        _tabIndex = 0
        _toggle = New List(Of Boolean)
    End Sub

    Public ReadOnly Property Attributes(ByVal layername As String) As List(Of StringBoolPair)
        Get
            If _attributes.ContainsKey(layername) Then
                Return _attributes(layername)
            Else
                Return Nothing
            End If
        End Get
    End Property
#End Region

#Region "Helper functions"
    Private Sub SetCurrentTab(ByVal idx As Integer)
        Dim i As Integer

        'Move all controls back to tabcontrol
        For i = uxPanel.Controls.Count - 1 To 0 Step -1
            Dim c As Control = uxPanel.Controls(i)
            uxPanel.Controls.Remove(c)
            uxTabs.TabPages(_tabIndex).Controls.Add(c)
        Next

        _tabIndex = idx
        uxLayer.Text = _layers(_tabIndex).Layer.Caption

        'Show controls in current tab
        For i = uxTabs.TabPages(_tabIndex).Controls.Count - 1 To 0 Step -1
            Dim c As Control = uxTabs.TabPages(_tabIndex).Controls(i)
            uxTabs.TabPages(_tabIndex).Controls.Remove(c)
            uxPanel.Controls.Add(c)
        Next

        'First page check
        uxPrevious.Enabled = True
        If _tabIndex = 0 Then uxPrevious.Enabled = False

        'Last page check, update button titles
        If _tabIndex >= uxTabs.TabCount - 1 Then
            uxPrevious.Text = GisControlView.LangStr(_strIdPrevious)
            uxNext.Text = GisControlView.LangStr(_strIdOK)
        Else
            uxPrevious.Text = GisControlView.LangStr(_strIdPrevious)
            uxNext.Text = GisControlView.LangStr(_strIdNext)
        End If
    End Sub

    Private Sub DoOK()
        Dim alist As CheckedListBox
        Dim attr As List(Of StringBoolPair)
        Dim i As Integer

        'Move all controls back to tabcontrol
        For i = uxPanel.Controls.Count - 1 To 0 Step -1
            Dim c As Control = uxPanel.Controls(i)
            uxPanel.Controls.Remove(c)
            uxTabs.TabPages(_tabIndex).Controls.Add(c)
        Next

        'Make a table of all layers and attributes
        For i = 0 To uxTabs.TabCount - 1
            alist = CType(uxTabs.TabPages(i).Controls(0), CheckedListBox)
            attr = New List(Of StringBoolPair)

            'List all layer attributes
            For j As Integer = 0 To alist.Items.Count - 1
                attr.Add(New StringBoolPair(alist.GetItemText(alist.Items(j)), alist.GetItemChecked(j)))
            Next

            _attributes(uxTabs.TabPages(i).Name) = attr
        Next
    End Sub
#End Region

#Region "Event handlers"
    Private Sub ExportAttributeDialog_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim alist As CheckedListBox
        Dim tab As TabPage
        Dim caption As String

        'Set up language
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        uxCancel.Text = GisControlView.LangStr(_strIdCancel)
        uxToggle.Text = GisControlView.LangStr(_strIdToggle)

        'Show only OK/Cancel if only one layer
        If _layers.Count = 1 Then
            Dim pt As Point = uxNext.Location
            uxNext.Location = uxPrevious.Location
            uxCancel.Location = pt
        End If

        'Create a new tab for each layer
        For i As Integer = 0 To _layers.Count - 1
            uxTabs.TabPages.Add(_layers(i).Layer.Name, _layers(i).Layer.Caption)
            tab = uxTabs.TabPages(uxTabs.TabPages.Count - 1)
            alist = New CheckedListBox
            alist.Location = New Drawing.Point(0, 0)
            alist.Size = New Drawing.Size(uxPanel.Size.Width, uxPanel.Height)

            'List layer attributes and joined fields in tab
            For j As Integer = 0 To _layers(i).Layer.Fields.Count - 1
                caption = _layers(i).Layer.Fields(j).newName
                If Not GisControlView.IsRelationField(caption) Then alist.Items.Add(caption, True)
            Next
            If _layers(i).DT IsNot Nothing Then
                For j As Integer = 0 To _layers(i).DT.Columns.Count - 1
                    caption = _layers(i).DT.Columns(j).Caption
                    If caption <> GisControlView.ColumnNameUid AndAlso Not GisControlView.IsRelationField(caption) Then alist.Items.Add(caption, True)
                Next
            End If
            tab.Controls.Add(alist)
            _toggle.Add(False) 'Mark/unmark flag for each page
        Next

        SetCurrentTab(0)
    End Sub

    Private Sub uxNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxNext.Click
        If _tabIndex < uxTabs.TabCount - 1 Then
            SetCurrentTab(_tabIndex + 1)
        Else
            DoOK()
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        End If
    End Sub

    Private Sub uxPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxPrevious.Click
        If _tabIndex > 0 Then SetCurrentTab(_tabIndex - 1)
    End Sub

    Private Sub uxCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxCancel.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub uxToggle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxToggle.Click
        Dim chklst As CheckedListBox

        'Toggle current checkboxes on/off
        For i As Integer = 0 To uxPanel.Controls.Count - 1
            If TypeOf uxPanel.Controls(i) Is CheckedListBox Then
                chklst = uxPanel.Controls(i)
                For j As Integer = 0 To chklst.Items.Count - 1
                    chklst.SetItemChecked(j, _toggle(_tabIndex))
                Next
            End If
        Next

        _toggle(_tabIndex) = Not _toggle(_tabIndex)
    End Sub
#End Region
End Class
