﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ExportAttributeDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.uxCancel = New System.Windows.Forms.Button
        Me.uxTabs = New System.Windows.Forms.TabControl
        Me.uxNext = New System.Windows.Forms.Button
        Me.uxPrevious = New System.Windows.Forms.Button
        Me.uxPanel = New System.Windows.Forms.Panel
        Me.uxLayer = New System.Windows.Forms.Label
        Me.uxToggle = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'uxCancel
        '
        Me.uxCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.uxCancel.Location = New System.Drawing.Point(102, 409)
        Me.uxCancel.Name = "uxCancel"
        Me.uxCancel.Size = New System.Drawing.Size(75, 23)
        Me.uxCancel.TabIndex = 101
        Me.uxCancel.Text = "uxCancel"
        '
        'uxTabs
        '
        Me.uxTabs.Location = New System.Drawing.Point(13, 32)
        Me.uxTabs.Name = "uxTabs"
        Me.uxTabs.SelectedIndex = 0
        Me.uxTabs.Size = New System.Drawing.Size(326, 371)
        Me.uxTabs.TabIndex = 1
        Me.uxTabs.Visible = False
        '
        'uxNext
        '
        Me.uxNext.Location = New System.Drawing.Point(264, 409)
        Me.uxNext.Name = "uxNext"
        Me.uxNext.Size = New System.Drawing.Size(75, 23)
        Me.uxNext.TabIndex = 100
        Me.uxNext.Text = "uxNext"
        Me.uxNext.UseVisualStyleBackColor = True
        '
        'uxPrevious
        '
        Me.uxPrevious.Location = New System.Drawing.Point(183, 409)
        Me.uxPrevious.Name = "uxPrevious"
        Me.uxPrevious.Size = New System.Drawing.Size(75, 23)
        Me.uxPrevious.TabIndex = 102
        Me.uxPrevious.Text = "uxPrevious"
        Me.uxPrevious.UseVisualStyleBackColor = True
        '
        'uxPanel
        '
        Me.uxPanel.Location = New System.Drawing.Point(13, 32)
        Me.uxPanel.Name = "uxPanel"
        Me.uxPanel.Size = New System.Drawing.Size(326, 371)
        Me.uxPanel.TabIndex = 103
        '
        'uxLayer
        '
        Me.uxLayer.AutoSize = True
        Me.uxLayer.Location = New System.Drawing.Point(13, 13)
        Me.uxLayer.Name = "uxLayer"
        Me.uxLayer.Size = New System.Drawing.Size(44, 13)
        Me.uxLayer.TabIndex = 104
        Me.uxLayer.Text = "uxLayer"
        '
        'uxToggle
        '
        Me.uxToggle.Location = New System.Drawing.Point(264, 8)
        Me.uxToggle.Name = "uxToggle"
        Me.uxToggle.Size = New System.Drawing.Size(75, 23)
        Me.uxToggle.TabIndex = 103
        Me.uxToggle.Text = "uxToggle"
        Me.uxToggle.UseVisualStyleBackColor = True
        '
        'ExportAttributeDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.uxCancel
        Me.ClientSize = New System.Drawing.Size(351, 444)
        Me.Controls.Add(Me.uxNext)
        Me.Controls.Add(Me.uxToggle)
        Me.Controls.Add(Me.uxCancel)
        Me.Controls.Add(Me.uxPrevious)
        Me.Controls.Add(Me.uxLayer)
        Me.Controls.Add(Me.uxTabs)
        Me.Controls.Add(Me.uxPanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ExportAttributeDialog"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "ExportAttributeDialog"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents uxCancel As System.Windows.Forms.Button
    Friend WithEvents uxTabs As System.Windows.Forms.TabControl
    Friend WithEvents uxNext As System.Windows.Forms.Button
    Friend WithEvents uxPrevious As System.Windows.Forms.Button
    Friend WithEvents uxPanel As System.Windows.Forms.Panel
    Friend WithEvents uxLayer As System.Windows.Forms.Label
    Friend WithEvents uxToggle As System.Windows.Forms.Button

End Class
