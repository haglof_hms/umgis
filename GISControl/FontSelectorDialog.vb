﻿Public Class FontSelectorDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 3000
    Private Const _strIdFont As Integer = 3001
    Private Const _strIdStyle As Integer = 3002
    Private Const _strIdEffects As Integer = 3003
    Private Const _strIdStrikeout As Integer = 3005
    Private Const _strIdUnderline As Integer = 3006
    Private Const _strIdCharacter As Integer = 3004
    Private Const _strIdOk As Integer = 3007
    Private Const _strIdCancel As Integer = 3008
#End Region

#Region "Declarations"
    Private Const FirstChar As Integer = 33 'First readable char in ASCII table
    Private Const LastChar As Integer = 65532
    Private _fonts As List(Of Font) = New List(Of Font)
    Private _foreBrush As Brush = New SolidBrush(ForeColor)
    Private _fontStyle As FontStyle
    Private _fontName As String
    Private _fontSize As Integer
    Private _fontChar As Integer
    Private _font As Font

    Public Sub New(Optional ByVal value As String = vbNullString)
        Dim tmp As String()
        Dim i As Integer

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        If value <> vbNullString Then
            tmp = value.Split(":")
            For i = 0 To tmp.Length() - 1
                If i = 0 Then
                    _fontName = tmp(i)
                ElseIf i = 1 Then
                    _fontChar = Convert.ToInt32(tmp(i))
                Else
                    If tmp(i) = "normal" Then
                        _fontStyle = FontStyle.Regular
                    ElseIf tmp(i) = "bold" Then
                        _fontStyle = _fontStyle Or FontStyle.Bold
                    ElseIf tmp(i) = "italic" Then
                        _fontStyle = _fontStyle Or FontStyle.Italic
                    ElseIf tmp(i) = "underline" Then
                        _fontStyle = _fontStyle Or FontStyle.Underline
                    End If
                End If
            Next
        Else
            _fontChar = FirstChar
            _fontName = vbNullString
            _fontStyle = FontStyle.Regular
        End If

        _fontSize = 12
    End Sub

    Public ReadOnly Property FontSelected() As String
        Get
            Dim tmp As String
            Dim style As String = vbNullString

            'Return font name and attributes in the form of: font_name:char_code[:bold][:italic][:underline]
            If _fontStyle = FontStyle.Regular Then
                style = ":normal"
            Else
                If _fontStyle And FontStyle.Bold Then style = style + ":bold"
                If _fontStyle And FontStyle.Italic Then style = style + ":italic"
                If _fontStyle And FontStyle.Underline Then style = style + ":underline"
            End If

            tmp = String.Format("{0}:{1}{2}", _fontName, _fontChar, style)
            Return tmp
        End Get
    End Property
#End Region

#Region "Helper functions"
    Private Sub SetupLanguage()
        'Set up language dependent strings
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        lblFont.Text = GisControlView.LangStr(_strIdFont)
        lblStyle.Text = GisControlView.LangStr(_strIdStyle)
        gbEffects.Text = GisControlView.LangStr(_strIdEffects)
        cbStrikeout.Text = GisControlView.LangStr(_strIdStrikeout)
        cbUnderline.Text = GisControlView.LangStr(_strIdUnderline)
        lblCharacter.Text = GisControlView.LangStr(_strIdCharacter)
        btnOk.Text = GisControlView.LangStr(_strIdOk)
        btnCancel.Text = GisControlView.LangStr(_strIdCancel)
    End Sub

    'Draw the fonts listbox
    Private Sub lbFonts_DrawItem(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DrawItemEventArgs) Handles lbFonts.DrawItem
        Try
            e.DrawBackground()
            e.DrawFocusRectangle()

            If e.Index >= 0 Then
                Dim font As Font = _fonts.Item(e.Index)
                e.Graphics.DrawString(font.Name, font, _foreBrush, e.Bounds.Left, e.Bounds.Top)
            End If
        Finally
        End Try
    End Sub

    'Draw the styles listbox
    Private Sub lbStyles_DrawItem(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DrawItemEventArgs) Handles lbStyles.DrawItem
        Try
            e.DrawBackground()
            e.DrawFocusRectangle()

            If e.Index >= 0 Then
                Dim style As FontStyle = (lbStyles.Items(e.Index))
                Dim font As Font = New Font(_fonts.Item(lbFonts.SelectedIndex).Name, _fontSize, style)
                e.Graphics.DrawString(font.Style.ToString(), font, _foreBrush, e.Bounds.Left, e.Bounds.Top)
            End If
        Finally
        End Try
    End Sub

    'Draw the characters listview
    Private Sub lvCharacters_DrawItem(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DrawListViewItemEventArgs) Handles lvCharacters.DrawItem
        Dim sf As New StringFormat()
        Dim indexes As ListView.SelectedIndexCollection = lvCharacters.SelectedIndices

        Try
            If indexes.Count > 0 Then
                If indexes.Item(0) = e.ItemIndex Then
                    e.Graphics.FillRectangle(New SolidBrush(System.Drawing.Color.FromKnownColor(System.Drawing.KnownColor.Highlight)), e.Bounds)
                End If
            Else
                If e.State And ListViewItemStates.Focused Then
                    e.Graphics.FillRectangle(New SolidBrush(System.Drawing.Color.FromKnownColor(System.Drawing.KnownColor.Highlight)), e.Bounds)
                ElseIf e.State And ListViewItemStates.Selected Then
                    e.Graphics.FillRectangle(New SolidBrush(System.Drawing.Color.FromKnownColor(System.Drawing.KnownColor.Menu)), e.Bounds)
                    e.DrawBackground()
                End If
            End If

            Dim font As Font = New Font(_fontName, _fontSize, _fontStyle)
            e.Graphics.DrawString(e.Item.Text, font, _foreBrush, e.Bounds, sf)
        Finally
            sf.Dispose()
        End Try
    End Sub
#End Region

#Region "Event handlers"
    Private Sub FontSelectorDialog_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim font As Font

        SetupLanguage()

        'Populate lbFonts with all available fonts
        For Each ff As FontFamily In FontFamily.Families
            If ff.IsStyleAvailable(FontStyle.Regular) Then
                font = New Font(ff, _fontSize, FontStyle.Regular)
                _fonts.Add(font)
                lbFonts.Items().Add(font)
            End If
        Next

        'Select default font
        If lbFonts.Items.Count > 0 Then
            If String.IsNullOrEmpty(_fontName) Then
                lbFonts.SelectedIndex = 0
            Else
                Dim sel As Integer = lbFonts.FindString(_fontName)
                If sel >= 0 Then lbFonts.SelectedIndex = sel
            End If
        End If

        'Set the default style(s)
        If lbStyles.Items.Count > 0 Then
            If _fontStyle = FontStyle.Regular Then
                lbStyles.SelectedIndex = 0
            Else
                If _fontStyle And FontStyle.Bold Then
                    Dim sel As Integer = lbStyles.FindString(FontStyle.Bold.ToString())
                    If sel >= 0 Then lbStyles.SelectedIndex = sel
                End If
                If _fontStyle And FontStyle.Italic Then
                    Dim sel As Integer = lbStyles.FindString(FontStyle.Italic.ToString())
                    If sel >= 0 Then lbStyles.SelectedIndex = sel
                End If
                If _fontStyle And FontStyle.Underline Then
                    cbUnderline.Checked = True
                End If
            End If
        End If

        'Tell character listview to use virtual mode for speed
        lvCharacters.VirtualMode = True
        lvCharacters.VirtualListSize = (LastChar - FirstChar)

        'Set the default character
        If _fontChar > 0 Then
            Dim lv As ListViewItem = lvCharacters.FindItemWithText(Convert.ToChar(_fontChar))
            lv.Focused = True
            lv.EnsureVisible()
        End If
        lvCharacters.Select()
    End Sub

    Private Sub lvCharacters_ItemSelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles lvCharacters.ItemSelectionChanged
        If e.IsSelected = True Then _fontChar = Convert.ToInt32(e.Item.Text(0))
    End Sub

    Private Sub lvCharacters_RetrieveVirtualItem(ByVal sender As Object, ByVal e As RetrieveVirtualItemEventArgs) Handles lvCharacters.RetrieveVirtualItem
        Dim item As New ListViewItem(Convert.ToChar(e.ItemIndex + FirstChar))
        e.Item = item
    End Sub

    Private Sub lvCharacters_SearchForVirtualItem(ByVal sender As Object, ByVal e As SearchForVirtualItemEventArgs) Handles lvCharacters.SearchForVirtualItem
        Dim search As Integer = Convert.ToInt32(e.Text(0))
        e.Index = search - FirstChar
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub lbFonts_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbFonts.SelectedIndexChanged
        'Get selected font and set font name
        Dim ff As FontFamily = _fonts.Item(lbFonts.SelectedIndex).FontFamily
        _fontName = ff.Name

        'Populate lbStyles with available styles for selected font
        lbStyles.Items.Clear()

        If ff.IsStyleAvailable(FontStyle.Regular) Then lbStyles.Items.Add(FontStyle.Regular)
        If ff.IsStyleAvailable(FontStyle.Bold) Then lbStyles.Items.Add(FontStyle.Bold)
        If ff.IsStyleAvailable(FontStyle.Italic) Then lbStyles.Items.Add(FontStyle.Italic)

        'Select previously selected style
        If _fontStyle = FontStyle.Regular Then lbStyles.SelectedIndex = lbStyles.Items.IndexOf(FontStyle.Regular)
        If _fontStyle And FontStyle.Bold Then lbStyles.SelectedIndex = lbStyles.Items.IndexOf(FontStyle.Bold)
        If _fontStyle And FontStyle.Italic Then lbStyles.SelectedIndex = lbStyles.Items.IndexOf(FontStyle.Italic)

        'Redraw the characters listview
        Dim font As Font = New Font(_fontName, _fontSize, _fontStyle)
        _font = font
        lvCharacters.Font = _font
        lvCharacters.Invalidate()
    End Sub

    Private Sub lbStyles_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbStyles.SelectedIndexChanged
        If lbStyles.SelectedIndex < 0 Then Exit Sub

        _fontStyle = lbStyles.Items(lbStyles.SelectedIndex)
        _fontStyle = _fontStyle Or If(cbStrikeout.Checked = False, 0, FontStyle.Strikeout)
        _fontStyle = _fontStyle Or If(cbUnderline.Checked = False, 0, FontStyle.Underline)

        lvCharacters.Invalidate()
    End Sub

    Private Sub cbStrikeout_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbStrikeout.CheckedChanged
        'Apply strikeout to font style
        If cbStrikeout.Checked = True Then
            _fontStyle = _fontStyle Or FontStyle.Strikeout
        Else
            _fontStyle = _fontStyle And Not FontStyle.Strikeout
        End If

        lvCharacters.Invalidate()
    End Sub

    Private Sub cbUnderline_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbUnderline.CheckedChanged
        'Apply underline to font style
        If cbUnderline.Checked = True Then
            _fontStyle = _fontStyle Or FontStyle.Underline
        Else
            _fontStyle = _fontStyle And Not FontStyle.Underline
        End If

        lvCharacters.Invalidate()
    End Sub
#End Region
End Class
