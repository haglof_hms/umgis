﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NavigateToDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.uxOK = New System.Windows.Forms.Button
        Me.uxCancel = New System.Windows.Forms.Button
        Me.uxCoordinate1 = New System.Windows.Forms.TextBox
        Me.uxCoordinate2 = New System.Windows.Forms.TextBox
        Me.uxDescription = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'uxOK
        '
        Me.uxOK.Location = New System.Drawing.Point(66, 77)
        Me.uxOK.Name = "uxOK"
        Me.uxOK.Size = New System.Drawing.Size(67, 23)
        Me.uxOK.TabIndex = 3
        Me.uxOK.Text = "uxOK"
        '
        'uxCancel
        '
        Me.uxCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.uxCancel.Location = New System.Drawing.Point(139, 77)
        Me.uxCancel.Name = "uxCancel"
        Me.uxCancel.Size = New System.Drawing.Size(67, 23)
        Me.uxCancel.TabIndex = 4
        Me.uxCancel.Text = "uxCancel"
        '
        'uxCoordinate1
        '
        Me.uxCoordinate1.Location = New System.Drawing.Point(12, 25)
        Me.uxCoordinate1.Name = "uxCoordinate1"
        Me.uxCoordinate1.Size = New System.Drawing.Size(248, 20)
        Me.uxCoordinate1.TabIndex = 1
        '
        'uxCoordinate2
        '
        Me.uxCoordinate2.Location = New System.Drawing.Point(12, 51)
        Me.uxCoordinate2.Name = "uxCoordinate2"
        Me.uxCoordinate2.Size = New System.Drawing.Size(248, 20)
        Me.uxCoordinate2.TabIndex = 2
        '
        'uxDescription
        '
        Me.uxDescription.AutoSize = True
        Me.uxDescription.Location = New System.Drawing.Point(12, 9)
        Me.uxDescription.Name = "uxDescription"
        Me.uxDescription.Size = New System.Drawing.Size(71, 13)
        Me.uxDescription.TabIndex = 4
        Me.uxDescription.Text = "uxDescription"
        '
        'NavigateToDialog
        '
        Me.AcceptButton = Me.uxOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.uxCancel
        Me.ClientSize = New System.Drawing.Size(272, 109)
        Me.Controls.Add(Me.uxCoordinate2)
        Me.Controls.Add(Me.uxCoordinate1)
        Me.Controls.Add(Me.uxDescription)
        Me.Controls.Add(Me.uxOK)
        Me.Controls.Add(Me.uxCancel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "NavigateToDialog"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "NavigateToDialog"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents uxOK As System.Windows.Forms.Button
    Friend WithEvents uxCancel As System.Windows.Forms.Button
    Friend WithEvents uxCoordinate1 As System.Windows.Forms.TextBox
    Friend WithEvents uxCoordinate2 As System.Windows.Forms.TextBox
    Friend WithEvents uxDescription As System.Windows.Forms.Label

End Class
