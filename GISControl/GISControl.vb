Option Explicit On
Option Strict Off

Imports Microsoft.Win32
Imports System.Data.OleDb
Imports System.Globalization
Imports System.IO
Imports System.Math
Imports System.Reflection.Assembly
Imports System.Runtime.InteropServices
Imports System.Xml
Imports XtremeCommandBars
Imports XtremeDockingPane
Imports XtremeReportControl
Imports TatukGIS.NDK
Imports TatukGIS.NDK.WinForms

Public Class GisControlView
    Inherits System.Windows.Forms.UserControl

#Region "Language"
    Private Const _strIdLength As Integer = 10000
    Private Const _strIdArea As Integer = 10001
    Private Const _strIdAddRelation As Integer = 10002
    Private Const _strIdViewInForest As Integer = 10004
    Private Const _strIdZoomToLayer As Integer = 10005
    Private Const _strIdRemoveLayer As Integer = 10006
    Private Const _strIdProperties As Integer = 10007
    Private Const _strIdZoomToShape As Integer = 10008
    Private Const _strIdAutoZoom As Integer = 10009
    Private Const _strIdFlashCurrentShape As Integer = 10010
    Private Const _strIdConditions As Integer = 10011
    Private Const _strIdSelections As Integer = 10012
    Private Const _strIdSelectGroup As Integer = 10013
    Private Const _strIdDeselectGroup As Integer = 10014
    Private Const _strIdSelectAll As Integer = 10015
    Private Const _strIdInvertSelection As Integer = 10016
    Private Const _strIdDeselectAll As Integer = 10017
    Private Const _strIdPaneMeasure As Integer = 10019
    Private Const _strIdPaneData As Integer = 10020
    Private Const _strIdUnsupportedFileFormat As Integer = 10021
    Private Const _strIdPlotUseVectorLayer As Integer = 10026
    Private Const _strIdHaglofPlotFiles As Integer = 10027
    Private Const _strIdInvalidFile As Integer = 10028
    Private Const _strIdLayersMissingCS As Integer = 10029
    Private Const _strIdUnknownFileExtension As Integer = 10030
    Private Const _strIdCountPlotsImportSuccessful As Integer = 10031
    Private Const _strIdSplitUnionWarning1 As Integer = 10032
    Private Const _strIdSplitUnionWarning2 As Integer = 10033
    Private Const _strIdConfirmRemoveShape As Integer = 10035
    Private Const _strIdNoShapesToExport As Integer = 10036
    Private Const _strIdShapeExportSuccessful As Integer = 10037
    Private Const _strIdCountPoints As Integer = 10038
    Private Const _strIdCountLines As Integer = 10039
    Private Const _strIdCountPolygons As Integer = 10040
    Private Const _strIdPlotExportSuccessful As Integer = 10041
    Private Const _strIdScale As Integer = 10043
    Private Const _strIdConfirmRemoveLayer As Integer = 10044
    Private Const _strIdNoShapeRelation As Integer = 10045
    Private Const _strIdRecords As Integer = 10046
    Private Const _strIdRemoveShape As Integer = 10047
    Private Const _strIdMissingShapeOnMap As Integer = 10048
    Private Const _strIdAddRelationQuestion As Integer = 10049
    Private Const _strIdPaneLayers As Integer = 10050
    Private Const _strIdPaneHierarchy As Integer = 10051
    Private Const _strIdPaneHierarchyMenu As Integer = 10052
    Private Const _strIdPaneHierarchyAddLayer As Integer = 10053
    Private Const _strIdPaneHierarchyAddUnusedLayers As Integer = 10054
    Private Const _strIdPaneHierarchyRemoveLayer As Integer = 10055
    Private Const _strIdPaneHierarchyAddGroup As Integer = 10056
    Private Const _strIdPaneHierarchyRemoveGroup As Integer = 10057
    Private Const _strIdNewLayer As Integer = 10058
    Private Const _strIdLayerCaption As Integer = 10059
    Private Const _strIdCountMultiPoints As Integer = 10060
    Private Const _strIdApplyRelationWrongType As Integer = 10061
    Private Const _strIdViewPropertyData = 10063
    Private Const _strIdViewPlotData As Integer = 10064
    Private Const _strIdViewStandData As Integer = 10065
    Private Const _strIdViewObjectData As Integer = 10066
    Private Const _strIdViewTcPlotData As Integer = 10067
    Private Const _strIdViewSpeciesPerPlot As Integer = 10068
    Private Const _strIdViewSpeciesPerStand As Integer = 10069
    Private Const _strIdStandLayer As Integer = 10070
    Private Const _strIdPlotLayer As Integer = 10071
    Private Const _strIdTcTractLayer As Integer = 10072
    Private Const _strIdHplFileTooHighVersion As Integer = 10073
    Private Const _strIdAutoSelect As Integer = 10074
    Private Const _strIdToolImportLayerFromFile As Integer = 10075
    Private Const _strIdToolZoomToFullExtent As Integer = 10076
    Private Const _strIdToolZoom As Integer = 10077
    Private Const _strIdToolDrag As Integer = 10078
    Private Const _strIdToolSelect As Integer = 10079
    Private Const _strIdToolViewInfo As Integer = 10080
    Private Const _strIdToolNewShape As Integer = 10081
    Private Const _strIdToolEditShape As Integer = 10082
    Private Const _strIdToolRemoveShape As Integer = 10083
    Private Const _strIdToolMiscTools As Integer = 10084
    Private Const _strIdToolApply As Integer = 10085
    Private Const _strIdToolRevert As Integer = 10086
    Private Const _strIdToolUndo As Integer = 10087
    Private Const _strIdToolRedo As Integer = 10088
    Private Const _strIdToolPolygon As Integer = 10089
    Private Const _strIdToolLine As Integer = 10090
    Private Const _strIdToolPoint As Integer = 10091
    Private Const _strIdToolNewObject As Integer = 10092
    Private Const _strIdToolExportView As Integer = 10093
    Private Const _strIdToolExportShp As Integer = 10094
    Private Const _strIdToolExportToTCNavigator As Integer = 10183
    Private Const _strIdToolCreatePlots As Integer = 10095
    Private Const _strIdToolImportPlots As Integer = 10096
    Private Const _strIdToolExportRoute As Integer = 10097
    Private Const _strIdToolSplitShape As Integer = 10098
    Private Const _strIdToolMeasure As Integer = 10099
    Private Const _strIdToolLength As Integer = 10100
    Private Const _strIdToolArea As Integer = 10101
    Private Const _strIdToolMultipoint As Integer = 10102
    Private Const _strIdToolApplyRelation As Integer = 10103
    Private Const _strIdToolNewLayer As Integer = 10107
    Private Const _strIdToolAdd As Integer = 10108
    Private Const _strIdToolPreviousExtent As Integer = 10109
    Private Const _strIdToolExportViewToImageWithProj As Integer = 10110
    Private Const _strIdToolExportViewToReport As Integer = 10111
    Private Const _strIdToolPrint As Integer = 10114
    Private Const _strIdToolbarUpdated As Integer = 10115
    Private Const _strIdToolSumAttributes As Integer = 10116
    Private Const _strIdSumFromLayer As Integer = 10117
    Private Const _strIdFilesMissingProjection As Integer = 10118
    Private Const _strIdToolExportViewToSnapshot As Integer = 10120
    Private Const _strIdProjectFileAlreadyExist As Integer = 10122
    Private Const _strIdSomeLayersDoesntExist As Integer = 10123
    Private Const _strIdAddUnusedLayers As Integer = 10124
    Private Const _strIdToolViewUnusedLayers As Integer = 10125
    Private Const _strIdLayerProperty As Integer = 10126
    Private Const _strIdLayerStand As Integer = 10127
    Private Const _strIdLayerObject As Integer = 10128
    Private Const _strIdLayerPlot As Integer = 10129
    Private Const _strIdLayerDeleted As Integer = 10130
    Private Const _strIdToolUnionShapes As Integer = 10131
    Private Const _strIdStandCoordImportSuccessful As Integer = 10132
    Private Const _strIdImportStandCoordQues As Integer = 10133
    Private Const _strIdViewTcStandData As Integer = 10134
    Private Const _strIdForPlotCoords As Integer = 10135
    Private Const _strIdForStandCoords As Integer = 10136
    Private Const _strIdToolImportLayerFromStream As Integer = 10137
    Private Const _strIdToolAbout As Integer = 10140
    Private Const _strIdNoDatabase As Integer = 10142
    Private Const _strIdToolImportProperties As Integer = 10143
    Private Const _strIdNoSnapping As Integer = 10144
    Private Const _strIdToolGenerateRoute As Integer = 10148
    Private Const _strIdForRoute As Integer = 10149
    Private Const _strIdDescriptionDrag As Integer = 10150
    Private Const _strIdDescriptionSelectShp As Integer = 10151
    Private Const _strIdDescriptionZoom As Integer = 10152
    Private Const _strIdDescriptionViewInfo As Integer = 10153
    Private Const _strIdDescriptionAdd As Integer = 10154
    Private Const _strIdDescriptionEditStep1 As Integer = 10155
    Private Const _strIdDescriptionRemove As Integer = 10156
    Private Const _strIdDescriptionMeasure As Integer = 10157
    Private Const _strIdDescriptionSplit As Integer = 10158
    Private Const _strIdDescriptionCreatePlots As Integer = 10159
    Private Const _strIdDescriptionGenerateRouteStep1WithoutCenterLine As Integer = 10160
    Private Const _strIdDescriptionGenerateRouteStep2 As Integer = 10161
    Private Const _strIdDescriptionExportRoute As Integer = 10162
    Private Const _strIdDescriptionCreateElvObject As Integer = 10163
    Private Const _strIdDescriptionExportShp As Integer = 10164
    Private Const _strIdDescriptionApplyRelation As Integer = 10165
    Private Const _strIdDescriptionSumAttributes As Integer = 10166
    Private Const _strIdDescriptionImportProperties As Integer = 10167
    Private Const _strIdConfirmRemoveMultipleShapes As Integer = 10168
    Private Const _strIdPlotLayerForRoute As Integer = 10172
    Private Const _strIdNoPlotsWithinBoundary As Integer = 10173
    Private Const _strIdToolSettings As Integer = 10174
    Private Const _strIdToolNavigateTo As Integer = 10175
    Private Const _strIdApplyEdit As Integer = 10176
    Private Const _strIdRevertEdit As Integer = 10177
    Private Const _strIdAddPart As Integer = 10178
    Private Const _strIdRemovePart As Integer = 10179
    Private Const _strIdDescriptionEditStep2 As Integer = 10180
    Private Const _strIdToolCreateStand As Integer = 10181
    Private Const _strIdDescriptionCreateStand As Integer = 10182
    Private Const _strIdCreateRelation As Integer = 10184
    Private Const _strIdViewSampletreedata As Integer = 10188
    Private Const _strIdStandPoint As Integer = 10190
    Private Const _strIdStandPolygon As Integer = 10191
    Private Const _strIdSampleTreePoint As Integer = 10192
    Private Const _strIdToolGenerateRouteWithoutCenterLine As Integer = 10193
    Private Const _strIdToolGenerateRouteWithCenterLine As Integer = 10194
    Private Const _strIdDescriptionGenerateRouteStep1WithCenterLine As Integer = 10195
    Private Const _strIdToolLandValueTools As Integer = 10196
    Private Const _strIdViewSectionData As Integer = 10197
    Private Const _strIdToolCreateSection As Integer = 10198
    Private Const _strIdDescriptionCreateSection As Integer = 10199
    Private Const _strIdShapeAlreadyConnected As Integer = 10200
    Private Const _strIdLayerTitleStandPoint As Integer = 10201
    Private Const _strIdLayerTitleStandPolygon As Integer = 10202
    Private Const _strIdLayerTitleSampleTreePoint As Integer = 10203
    Private Const _strIdLayerTitlePlotPoint As Integer = 10204
    Private Const _strIdUpdateStandArea As Integer = 10205
    Private Const _strIdViewSectionPlotData As Integer = 10206
    Private Const _strIdLayerTrees As Integer = 10207
    Private Const _strIdToolExportViewToImageWithScale As Integer = 10208
    Private Const _strIdUpdateMap As Integer = 10288
    Private Const _strIdOpenStreamFailed As Integer = 10289
    Private Const _strIdToolImportLayerFromStream1 As Integer = 10290
    Private Const _strIdToolImportLayerFromStreamCustom As Integer = 10291
    Private Const _strChooseCoordinateSystem As Integer = 10292
    Private Const _strIdToolRemoveLayer As Integer = 10293
    Private Const _strIdToolLayerProperties As Integer = 10294
    Private Const _strIdCopy As Integer = 10295
    Private Const _strIdSelectAttributes As Integer = 10296
    Private Const _strIdCachedMode As Integer = 10297
#End Region

#Region "Declarations"
#Region "Constants"
    Friend Const GisTablePrefix As String = "gis_" 'DB table prefix
    Friend Const ForeignKeyField As String = "key1"
    Friend Const VersionField As String = "version"
    Friend Const PreviewLayerName As String = "Preview [0]" 'Use an index within [] so we don't get a name conflit with a user layer (indexing start from 1 on user layers)
    Friend Const ColumnNameUid As String = "UID" 'DB column name for internal storage of GIS_UID
    Friend Const ViewerWndName As String = "viewerwnd" 'Viewer window record in layer index table
    Friend Const NewLayerName As String = "[NewLayer]" 'Indicate a new layer

    Friend Const FieldNameGisUid As String = "GIS_UID"
    Friend Const FieldNameOrigId As String = "ORIG_ID"
    Friend Const FieldNameRelation As String = "RELATIONID"
    Friend Const FieldNameSubrelation As String = "SUBRELATIONID"
    Friend Const FieldNamePlotId As String = "PLOTID"
    Friend Const FieldNamePlotRow As String = "PLOTROW"
    Friend Const FieldNamePlotColumn As String = "PLOTCOLUMN"
    Friend Const FieldNamePlotRelation As String = "PLOTRELATION"
    Friend Const FieldNameObjectProperty As String = "Fastighet"
    Friend Const FieldNameProperty As String = "Fastighetsnamn"
    Friend Const FieldNameStand As String = "Best�nd"
    Friend Const FieldNameSection As String = "Inv. Avdelningnr"
    Friend Const FieldNameSectionId As String = "ID"
    Friend Const FieldNameSectionAvdnr As String = "AVDNR"
    Friend Const FieldNameSectionAgoslag As String = "AGOSLAG"
    Friend Const FieldNameSectionMalklass As String = "MALKLASS"
    Friend Const FieldNameSectionHkl As String = "HKL"
    Friend Const FieldNameSectionAlder As String = "ALDER"
    Friend Const FieldNameSectionAtgard As String = "ATGARD"
    Friend Const FieldNameSectionAtgardof As String = "ATGARDOF"
    Friend Const FieldNameSectionHasof As String = "HASOF"
    Friend Const FieldNameSectionSkifte As String = "SKIFTE"
    Friend Const FieldNameSectionMedelhojd As String = "MEDELHOJD"
    Friend Const FieldNameSectionVolym As String = "VOLYM"
    Friend Const FieldNameSectionFuktklass As String = "FUKTKLASS"
    Friend Const FieldNameSectionStandortsi As String = "STANDORTSI"
    Friend Const FieldNameSectionVegitation As String = "VEGITATION"
    Friend Const FieldNameSectionGrundyta As String = "GRUNDYTA"
    Friend Const FieldNameSectionFramskrive As String = "FRAMSKRIVE"
    Friend Const FieldNameSectionDGV As String = "DGV"
    Friend Const FieldNewNameSectionId As String = "ID"
    Friend Const FieldNewNameSectionAvdnr As String = "avdnr"
    Friend Const FieldNewNameSectionAgoslag As String = "agoslag"
    Friend Const FieldNewNameSectionMalklass As String = "malklass"
    Friend Const FieldNewNameSectionHkl As String = "hkl"
    Friend Const FieldNewNameSectionAlder As String = "alder"
    Friend Const FieldNewNameSectionAtgard As String = "atgard"
    Friend Const FieldNewNameSectionAtgardof As String = "atgardOF"
    Friend Const FieldNewNameSectionHasof As String = "hasOF"
    Friend Const FieldNewNameSectionSkifte As String = "Skifte"
    Friend Const FieldNewNameSectionMedelhojd As String = "Medelhojd"
    Friend Const FieldNewNameSectionVolym As String = "Volym"
    Friend Const FieldNewNameSectionFuktklass As String = "FuktKlass"
    Friend Const FieldNewNameSectionStandortsi As String = "StandortsIndex"
    Friend Const FieldNewNameSectionVegitation As String = "VegitationsTyp"
    Friend Const FieldNewNameSectionGrundyta As String = "Grundyta"
    Friend Const FieldNewNameSectionFramskrive As String = "Framskriven"
    Friend Const FieldNewNameSectionDGV As String = "Dgv"

    Friend Const ViewNameObjectData As String = "gis_objectdata"
    Friend Const ViewNamePlotData As String = "gis_plotdata"
    Friend Const ViewNamePropertyData As String = "gis_propertydata"
    Friend Const ViewNameStandData As String = "gis_standdata"
    Friend Const ViewNameTcPlotData As String = "gis_tcplotdata"
    Friend Const ViewNameTcStandData As String = "gis_tcstanddata"
    Friend Const ViewNameSampleTreeData As String = "gis_sampletreedata"
    Friend Const ViewNameSectionData As String = "gis_sectiondata"
    Friend Const ViewNameSectionPlotData As String = "gis_sectionplotdata"
    Friend Const ViewNameSpeciesPerStand As String = "gis_speciesperstand"
    Friend Const ViewNameSpeciesPerPlot As String = "gis_speciesperplot"

    Friend Const FieldLengthLayerCaption As Integer = 64

    Friend Shared ReadOnly Operators As String() = {"=", "<>", "<", "<=", ">", ">=", "IS NULL", "IS NOT NULL", "LIKE", "NOT LIKE"}

    Friend Const RegKeyHms As String = "Software\HaglofManagmentSystem"
    Friend Const RegKeyDB As String = "Software\HaglofManagmentSystem\Database"
    Friend Const RegKeyGis As String = "Software\HaglofManagmentSystem\UMGIS"
    Friend Const RegKeyLanguage As String = "Software\HaglofManagmentSystem\Language"
    Friend Const RegKeyViewport As String = "Software\HaglofManagmentSystem\UMGIS\Viewport"

    Friend Const RegSectionHms As String = "HaglofManagmentSystem"
    Friend Const RegSectionGis As String = "UMGIS"
    Friend Const RegSectionViewport As String = "Viewport"
    Friend Const RegSectionCommandBarLayout As String = "CommandBarLayout"
    Friend Const RegSectionDockingPaneLayout As String = "DockingPaneLayout"

    Friend Const RegValCachedMode As String = "CachedMode"
    Friend Const RegValEditMode As String = "EditMode"
    Friend Const RegValSnapType As String = "SnapType"
    Friend Const RegValRouteDirection As String = "RouteDirection"
    Friend Const RegValLengthUnits As String = "ScaleUnits"
    Friend Const RegValAreaUnits As String = "AreaUnits"
    Friend Const RegValLastImportDir As String = "LastImportDir"
    Friend Const RegValImportChoice As String = "ImportChoice"
    Friend Const RegValLastVersion As String = "LastVersion"
    Friend Const RegValLastLanguage As String = "LastLanguage"
    Friend Const RegValLangSet As String = "LANGSET"
    Friend Const RegValProjectDir As String = "ProjectDir"
    Friend Const RegValNorthArrow As String = "NorthArrow"
    Friend Const RegValMapRotation As String = "MapRotation"
    Friend Const RegValStandPointLayer As String = "StandPointLayer"
    Friend Const RegValStandPolygonLayer As String = "StandPolygonLayer"
    Friend Const RegValSampleTreePointLayer As String = "SampleTreePointLayer"
    Friend Const RegValPlotPointLayer As String = "PlotPointLayer"

    Friend Const EpsgMetric As Integer = 904201
    Friend Const EpsgUS As Integer = 904202

    Private Const _selectTolerance As Double = 5.0 'Pixels
    Private Const _defaultScaleUnitsEpsg As Integer = 904201 'Metric

    Private ReadOnly _toolbarVersion As New System.Version(2, 15, 4) 'This value indicate version of last toolbar changes. If LastVersion is less than this value we need to reset the layout

    Private Const _viewVersionObjectData As Integer = 2
    Private Const _viewVersionPlotData As Integer = 3
    Private Const _viewVersionPropertyData As Integer = 2
    Private Const _viewVersionStandData As Integer = 2
    Private Const _viewVersionTcPlotData As Integer = 4
    Private Const _viewVersionTcStandData As Integer = 2
    Private Const _viewVersionSampletreedata As Integer = 5
    Private Const _viewVersionSectionData As Integer = 2
    Private Const _viewVersionSectionPlotData As Integer = 1

    Private Const _controlScaleHeight As Integer = 50
    Private Const _controlScaleWidth As Integer = 200
    Private Const _controlNorthArrowHeight As Integer = 64
    Private Const _controlNorthArrowWidth As Integer = 64

    Private Const _hplFileVersion As UInteger = &H2
    Private Const _sodFileVersion As UInteger = &H2

    Private Const _tcgpsnavUuid As String = "{CCAEBC35-7F70-457d-AE0C-44EF7F268DFF}"
    Private Const _tcgpsnavVersion As String = "1.00"
    Private Const _tcgpsnavWaypointTypeCount As Int32 = 6
    Private ReadOnly _tcgpsnavWaypointType() As String = {"Mrk", "Plt", "Stn", "Mrk00", "Plt00", "Stn00"}
    Private ReadOnly _tcgpsnavWaypointTypeLN() As String = {"Mark", "Plot", "Station", "Mark00", "Plot00", "Station00"}

    Private Const MapStream1 As String = "http://ows.terrestris.de/osm/service?"
    Private Const LayerNameOSM As String = "ows.terrestris.de"

    'Forest database fields
    Private Const _fieldLengthCounty As Integer = 31
    Private Const _fieldLengthMunicipal As Integer = 31
    Private Const _fieldLengthParish As Integer = 31
    Private Const _fieldLengthPropNumber As Integer = 20
    Private Const _fieldLengthPropName As Integer = 40
    Private Const _fieldLengthBlock As Integer = 5
    Private Const _fieldLengthUnit As Integer = 5
    Private Const _fieldLengthObjID As Integer = 30

    Private Const _listingFieldDataText As String = "..."
#End Region

#Region "Variables"
    Friend Shared ConnStr As String
    Friend Shared Conn As OleDbConnection
    Friend Shared LangStr As New Hashtable
    Friend Shared Views As New List(Of JoinView)
    Friend Shared Instance As GisControlView
    Friend Shared ForestInstalled As Boolean
    Friend Shared EstimateInstalled As Boolean
    Friend Shared EstimateSodraInstalled As Boolean
    Friend Shared LandValueInstalled As Boolean
    Friend Shared TCruiseInstalled As Boolean

    Private _paneIdLegend As Integer
    Private _paneIdHierarchy As Integer
    Private _paneIdMeasure As Integer
    Private _paneIdData As Integer

    Private _activeLayerName As String
    Private _activeSubLayerName As String
    Private _mode As GisMode
    Private _newShapeType As ShapeType
    Private _customShapeUid As Integer, _selectedShapeUid As Integer
    Private _selectionStarted As Boolean, _selectionEndPointSet As Boolean
    Private _selectedOrigin As TGIS_Point
    Private _selectedEndPoint As TGIS_Point
    Private _objectShapePropertyList As List(Of IntIntPair)
    Private _objectLayerName As String
    Private _filterConditions As New Hashtable
    Private _layerChanged As Boolean
    Private _quitting As Boolean
    Private _curLayerData As TGIS_LayerVector
    Private _curDataTable As DataTable
    Private _curDataTableIndex As Integer
    Private _listFieldsRunning As Boolean
    Private _initializeViewport As Boolean
    Private _viewportInitialized As Boolean
    Private _applyRelationId As Integer
    Private _applyRelationType As RelationType
    Private _projectPath As String
    Private _scalePane As StatusBarPane, _shapeCountPane As StatusBarPane
    Private _latLongPane As StatusBarPane, _mapCoordinatesPane As StatusBarPane
    Private _coordinateSystemPane As StatusBarPane
    Private _actionPane As StatusBarPane
    Private _previousExtent(4) As TGIS_Extent
    Private _extentChanged As Boolean
    Private _ignoreZoomChange As Boolean
    Private _componentInitialized As Boolean
    Private _printExtent As TGIS_Extent
    Private _printScale As Double
    Private _snapLayerGallery As CommandBarGallery
    Private _snapLayerItems As CommandBarGalleryItems
    Private _areaUnits As TGIS_CSUnits
    Private _plotLayerForRoute As String
    Private _routeMiddleLine As TGIS_Shape
    Private _routeDirection As RouteDirection
    Private _drawNavigationPin As Boolean
    Private _navigationPinDrawn As Boolean
    Private _navigationPinPos As Point
    Private _addPart As Boolean
    Private _lastSnapLayer As TGIS_LayerVector
    Private _standShapeId As Integer
    Private _maprotation As Integer
    Private _sectionDestinationLayer As String
    Private _shapeList As New List(Of Integer)
    Private _currentJoinView As String
    Private _cachedMode As Boolean
#End Region

#Region "Events"
    Public Event CreateElvObject(ByVal sender As Object, ByVal e As ObjectEventArgs)
    Public Event CreateStand(ByVal sender As Object, ByVal e As StandEventArgs)
    Public Event CreateSection(ByVal sender As Object, ByVal e As SectionEventArgs)
    Public Event UpdateSection(ByVal sender As Object, ByVal e As SectionEventArgs)
    Public Event RefreshStand(ByVal sender As Object, ByVal e As EventArgs)
    Public Event ShowRelationTooltip(ByVal sender As Object, ByVal e As RelationTooltipEventArgs)
    Public Event ViewForestData(ByVal sender As Object, ByVal e As ForestEventArgs)

    Public Class ForestEventArgs
        Inherits System.EventArgs

        Private _id As Integer
        Private _type As RelationType

        Public ReadOnly Property Id() As Integer
            Get
                Return _id
            End Get
        End Property

        Public ReadOnly Property TypeOfRelation() As RelationType
            Get
                Return _type
            End Get
        End Property

        Public Sub New(ByVal id As Integer, ByVal type As RelationType)
            _id = id
            _type = type
        End Sub
    End Class

    Public Class ObjectEventArgs
        Inherits System.EventArgs

        Private _propertyList As List(Of Integer)

        Public ReadOnly Property PropertyList() As List(Of Integer)
            Get
                Return _propertyList
            End Get
        End Property

        Public Sub New(ByVal propertyList As List(Of Integer))
            _propertyList = propertyList
        End Sub
    End Class

    Public Class RelationTooltipEventArgs
        Inherits System.EventArgs

        Private _position As Point

        Public ReadOnly Property Position() As Point
            Get
                Return _position
            End Get
        End Property

        Public Sub New(ByVal position As Point)
            _position = position
        End Sub
    End Class

    Public Class StandEventArgs
        Inherits System.EventArgs

        Private _position As Point
        Private _area As Double

        Public ReadOnly Property Position() As Point
            Get
                Return _position
            End Get
        End Property

        Public ReadOnly Property Area() As Double
            Get
                Return _area
            End Get
        End Property

        Public Sub New(ByVal position As Point, ByVal area As Double)
            _position = position
            _area = area
        End Sub
    End Class

    Public Class SectionEventArgs
        Inherits System.EventArgs
        Private _data As List(Of SectionData)

        Public Sub New()
            _data = New List(Of SectionData)
        End Sub

        Public Sub New(ByVal data As List(Of SectionData))
            _data = data
        End Sub

        Public ReadOnly Property Data() As List(Of SectionData)
            Get
                Return _data
            End Get
        End Property

        Public Class SectionData
            Private _gisuid As Integer
            Private _id As Integer
            Private _avdnr As String
            Private _agoslag As Integer
            Private _malklass As Integer
            Private _hkl As Integer
            Private _alder As Integer
            Private _atgard As Integer
            Private _atgardof As Integer
            Private _hasof As Integer
            Private _skifte As Integer
            Private _medelhojd As Integer
            Private _volym As Integer
            Private _fuktklass As Integer
            Private _standortsi As Integer
            Private _vegetation As Integer
            Private _latitud As Integer
            Private _longitud As Integer
            Private _grundyta As Double
            Private _framskrive As String
            Private _dgv As Double

            Public Sub New(ByVal gisuid As Integer, ByVal id As Integer, ByVal avdnr As String, ByVal agoslag As Integer, ByVal malklass As Integer, ByVal hkl As Integer, ByVal alder As Integer, _
                           ByVal atgard As Integer, ByVal atgardof As Integer, ByVal hasof As Integer, ByVal skifte As Integer, ByVal medelhojd As Integer, ByVal volym As Integer, _
                           ByVal fuktklass As Integer, ByVal standordsi As Integer, ByVal vegetation As Integer, ByVal latitud As Integer, ByVal longitud As Integer, ByVal grundyta As Integer, _
                           ByVal framskrive As String, ByVal dgv As Double)
                _gisuid = gisuid
                _id = id
                _avdnr = avdnr
                _agoslag = agoslag
                _malklass = malklass
                _hkl = hkl
                _alder = alder
                _atgard = atgard
                _atgardof = atgardof
                _hasof = hasof
                _skifte = skifte
                _medelhojd = medelhojd
                _volym = volym
                _fuktklass = fuktklass
                _standortsi = standordsi
                _vegetation = vegetation
                _latitud = latitud
                _longitud = longitud
                _grundyta = grundyta
                _framskrive = framskrive
                _dgv = dgv
            End Sub

            Public ReadOnly Property Gisuid() As Integer
                Get
                    Return _gisuid
                End Get
            End Property

            Public ReadOnly Property Id() As Integer
                Get
                    Return _id
                End Get
            End Property

            Public ReadOnly Property Avdnr() As String
                Get
                    Return _avdnr
                End Get
            End Property

            Public ReadOnly Property Agoslag() As Integer
                Get
                    Return _agoslag
                End Get
            End Property

            Public ReadOnly Property Malklass() As Integer
                Get
                    Return _malklass
                End Get
            End Property

            Public ReadOnly Property Hkl() As Integer
                Get
                    Return _hkl
                End Get
            End Property

            Public ReadOnly Property Alder() As Integer
                Get
                    Return _alder
                End Get
            End Property

            Public ReadOnly Property Atgard() As Integer
                Get
                    Return _atgard
                End Get
            End Property

            Public ReadOnly Property Atgardof() As Integer
                Get
                    Return _atgardof
                End Get
            End Property

            Public ReadOnly Property Hasof() As Integer
                Get
                    Return _hasof
                End Get
            End Property

            Public ReadOnly Property Skifte() As Integer
                Get
                    Return _skifte
                End Get
            End Property

            Public ReadOnly Property Medelhojd() As Integer
                Get
                    Return _medelhojd
                End Get
            End Property

            Public ReadOnly Property Volym() As Integer
                Get
                    Return _volym
                End Get
            End Property

            Public ReadOnly Property Fuktklass() As Integer
                Get
                    Return _fuktklass
                End Get
            End Property

            Public ReadOnly Property Standortsi() As Integer
                Get
                    Return _standortsi
                End Get
            End Property

            Public ReadOnly Property Vegetation() As Integer
                Get
                    Return _vegetation
                End Get
            End Property

            Public ReadOnly Property Latitud() As Integer
                Get
                    Return _latitud
                End Get
            End Property

            Public ReadOnly Property Longitud() As Integer
                Get
                    Return _longitud
                End Get
            End Property

            Public ReadOnly Property Grundyta() As Double
                Get
                    Return _grundyta
                End Get
            End Property

            Public ReadOnly Property Framskrive() As String
                Get
                    Return _framskrive
                End Get
            End Property

            Public ReadOnly Property Dgv() As Double
                Get
                    Return _dgv
                End Get
            End Property
        End Class
    End Class
#End Region

#Region "Properties"
    Friend Property PrintScale() As Double
        Get
            Return _printScale
        End Get
        Set(ByVal value As Double)
            _printScale = value
        End Set
    End Property
#End Region

    Friend Enum ToolbarId 'Toolbar IDs
        Layer = 100
        NewLayer = 101
        ImportLayerFromFile = 102
        FullExtent = 103
        Zoom = 104
        Drag = 105
        SelectShape = 106
        Info = 107
        NewShape = 108
        NewShapePolygon = 109
        NewShapeLine = 110
        NewShapePoint = 111
        NewShapeMultiPoint = 112
        EditShape = 113
        RemoveShape = 114
        Measure = 115
        MeasureLength = 116
        MeasureArea = 117
        Undo = 118
        Redo = 119
        MiscTools = 120
        MiscToolsExportView = 122
        MiscToolsExportToShp = 123
        MiscToolsExportToTCNavigator = 127
        MiscToolsCreatePlots = 124
        MiscToolsImportPlots = 125
        MiscToolsExportRoute = 126
        MiscToolsSplitShape = 130
        Apply = 132
        Revert = 133
        ApplyRelation = 134
        PreviousExtent = 135
        MiscToolsExportViewToImageWithProj = 136
        MiscToolsExportViewToReport = 137
        Print = 140
        MiscToolsSumAttributes = 141
        MiscToolsExportViewToSnapShot = 142
        MiscToolsViewUnusedLayers = 144
        MiscToolsUnionShapes = 145
        ImportLayerFromStream = 146
        MiscToolsAbout = 147
        MiscToolsImportProperties = 148
        SnapLayer = 149
        MiscToolsGenerateRoute = 153
        MiscToolsSettings = 154
        NavigateTo = 155
        MiscToolsCreateStand = 157
        MiscToolsCreateSection = 158
        MiscToolsGenerateRouteWithoutCenterLine = 159
        MiscToolsGenerateRouteWithCenterLine = 160
        LandValueTools = 161
        LandValueToolsCreateObject = 162
        LandValueToolsImportProperties = 163
        LandValueToolsGenerateRoute = 164
        LandValueToolsGenerateRouteWithoutCenterLine = 165
        LandValueToolsGenerateRouteWithCenterLine = 166
        LandValueToolsExportRoute = 167
        MiscToolsExportViewToImageWithScale = 168
        UpdateMap = 169
        ImportLayerFromStream1 = 170
        ImportLayerFromStreamCustom = 171
        RemoveLayer = 172
        LayerProperties = 173
    End Enum

    Public Sub New()
        'Set up licences (required for Codejock ActiveX compontents to work)
        Dim CommandBarsSettings As New XtremeCommandBars.CommandBarsGlobalSettingsClass
        CommandBarsSettings.License = "CommandBars Control Copyright (c) 2003-2010 Codejock Software" & vbCrLf & _
                                        "PRODUCT-ID: Codejock.CommandBars.ActiveX.v13.4" + vbCrLf & _
                                        "VALIDATE-CODE: QQS-PNF-OJV-VBX"

        Dim DockingPaneSettings As New XtremeDockingPane.DockingPaneGlobalSettings
        DockingPaneSettings.License = "Docking Panes Control Copyright (c) 2003-2010 Codejock Software" & vbCrLf & _
                                        "PRODUCT-ID: Codejock.DockingPane.ActiveX.v13.4" & vbCrLf & _
                                        "VALIDATE-CODE: UCY-KMS-CII-OCF"

        Dim ReportControlSettings As New XtremeReportControl.ReportControlGlobalSettings
        ReportControlSettings.License = "Report Control Copyright (c) 2003-2010 Codejock Software" & vbCrLf & _
                                        "PRODUCT-ID: Codejock.ReportControl.ActiveX.v13.4" & vbCrLf & _
                                        "VALIDATE-CODE: HIF-MPA-DRR-OPF"

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _componentInitialized = True
    End Sub
#End Region

#Region "Helper functions"
    Friend Shared Sub AddCentroidToPropertyTable(ByRef applyRelationId As Integer, ByRef shp As TGIS_ShapeAbstract)
        Dim cmd As New OleDbCommand("", GisControlView.Conn)
        Dim reader As OleDbDataReader = Nothing
        Dim deg As Double, min As Double, sec As Double, frac As Double
        Dim sign As Integer
        Dim lat As String, lon As String
        Dim shpPoly As TGIS_ShapePolygon
        Dim pt As TGIS_Point
        Dim cs As TGIS_CSCoordinateSystem

        shpPoly = TryCast(shp, TGIS_ShapePolygon)
        If shpPoly IsNot Nothing Then
            'Use layer cs during import as layer has not yet been attached to viewer)
            If shpPoly.Viewer IsNot Nothing Then
                cs = shpPoly.Viewer.CS
            Else
                cs = shpPoly.Layer.CS
            End If

            'Add centroid to property table
            If cs.EPSG <> 0 AndAlso applyRelationId > 0 Then
                Try
                    pt = cs.ToWGS(shpPoly.Centroid()) 'Make sure to use layer CS (during import, layer data might not have been saved yet and thus Tatuk won't handle conversion of coordinates automatically)

                    TGIS_Utils.GisDecodeLatitude(pt.Y, deg, min, sec, frac, sign, 4)
                    If sign < 0 Then lat = "-" Else lat = vbNullString
                    lat += CStr(deg).PadLeft(2, "0"c) & CStr(min).PadLeft(2, "0"c) & CStr(Int(((sec * 10000) + frac) / 60)).PadLeft(4, "0"c)

                    TGIS_Utils.GisDecodeLongitude(pt.X, deg, min, sec, frac, sign, 4)
                    If sign < 0 Then lon = "-" Else lon = vbNullString
                    lon += CStr(deg).PadLeft(2, "0"c) & CStr(min).PadLeft(2, "0"c) & CStr(Int(((sec * 10000) + frac) / 60)).PadLeft(4, "0"c)

                    cmd.CommandText = "UPDATE fst_property_table SET prop_coord = '" & lat & "," & lon & "' WHERE id = " & applyRelationId
                    cmd.ExecuteNonQuery()
                Catch ex As EGIS_Exception
                    MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                End Try
            End If
        End If
    End Sub

    Friend Shared Sub CreateLayerInstance(ByRef ll As TGIS_LayerAbstract, ByVal fileName As String)
        ll = TGIS_Utils.GisCreateLayer(GetUniqueLayerName(), fileName)
        If ll Is Nothing Then MsgBox(LangStr(_strIdUnsupportedFileFormat), MsgBoxStyle.Exclamation) 'Unsupported file format
    End Sub

    Friend Shared Function GetLayerRelationType(ByVal layerName As String) As RelationType
        Dim type As RelationType = RelationType.Undefined

        Dim cmd As New OleDbCommand("", Conn)
        Dim reader As OleDbDataReader

        'Get layer relation type from database
        cmd.CommandText = "SELECT type FROM gis_layers WHERE name = '" & layerName & "'"
        reader = cmd.ExecuteReader(CommandBehavior.SingleRow)
        If reader.HasRows Then
            reader.Read()
            If Not reader.IsDBNull(0) Then type = reader.GetInt16(0)
        End If
        reader.Close()

        Return type
    End Function

    Friend Shared Sub GetLayerRelationType(ByVal layerName As String, ByRef type As RelationType, ByRef joinView As String)
        Dim cmd As New OleDbCommand("", Conn)
        Dim reader As OleDbDataReader

        'Assign default values in case values are not set
        type = RelationType.Undefined
        joinView = vbNullString

        'Get layer relation type and joined view from database
        cmd.CommandText = "SELECT type, join_view FROM gis_layers WHERE name = '" & layerName & "'"
        reader = cmd.ExecuteReader(CommandBehavior.SingleRow)
        If reader.HasRows Then
            reader.Read()
            If Not reader.IsDBNull(0) Then type = reader.GetInt16(0)
            If Not reader.IsDBNull(1) Then joinView = reader.GetString(1)
        End If
        reader.Close()
    End Sub

    Friend Shared Function GetShapeRelationName(ByVal shp As TGIS_Shape) As String
        Dim cmd As New OleDbCommand("", Conn)
        Dim reader As OleDbDataReader
        Dim objectname As String = vbNullString

        'Query db for name of related object
        Select Case GisControlView.GetLayerRelationType(shp.Layer.Name)
            Case RelationType.StandRelation
                cmd.CommandText = "SELECT trakt_name FROM esti_trakt_table WHERE trakt_id = " & shp.GetField(GisControlView.FieldNameRelation)
            Case RelationType.PropertyRelation
                cmd.CommandText = "SELECT prop_name + ' ' + block_number + ':' + unit_number FROM fst_property_table WHERE id = " & shp.GetField(GisControlView.FieldNameRelation)
            Case RelationType.ObjectRelation
                cmd.CommandText = "SELECT object_name FROM elv_object_table WHERE object_id = " & shp.GetField(GisControlView.FieldNameRelation)
        End Select
        If Not String.IsNullOrEmpty(cmd.CommandText) Then
            reader = cmd.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read() Then objectname = reader.GetString(0)
            reader.Close()
        End If

        Return objectname
    End Function

    Friend Shared Function GetJoinPrimary(ByVal view As String) As String
        Dim i As Integer

        'Determine JoinPrimary key for specified view
        For i = 0 To Views.Count - 1
            If Views(i).Name = view Then Return Views(i).JoinPrimary
        Next

        Return vbNullString
    End Function

    Friend Shared Function GetJoinDT(ByVal view As String) As DataTable
        Dim i As Integer

        'Get recorset for specified view
        For i = 0 To Views.Count - 1
            If Views(i).Name = view Then Return Views(i).JoinDT
        Next

        Return Nothing
    End Function

    Friend Shared Function GetFieldType(ByVal layer As TGIS_LayerVector, ByVal fieldName As String, ByRef fieldType As TGIS_FieldType) As Boolean
        'Grab layer, check field
        If layer.FindField(fieldName) <> -1 Then
            fieldType = layer.FieldInfo(layer.FindField(fieldName)).FieldType
        ElseIf fieldName = FieldNameGisUid Then
            fieldType = TGIS_FieldType.gisFieldTypeNumber
        ElseIf layer.JoinNET IsNot Nothing Then
            'Joined field
            Select Case System.Type.GetTypeCode(CType(layer.JoinNET, DataTable).Columns(fieldName).DataType)
                Case TypeCode.Boolean, TypeCode.Byte, TypeCode.SByte
                    fieldType = TGIS_FieldType.gisFieldTypeBoolean 'Bool
                Case TypeCode.DateTime
                    fieldType = TGIS_FieldType.gisFieldTypeDate 'Date
                Case TypeCode.Decimal, TypeCode.Double, TypeCode.Single
                    fieldType = TGIS_FieldType.gisFieldTypeFloat 'Float
                Case TypeCode.Int16, TypeCode.Int32, TypeCode.Int64, TypeCode.UInt16, TypeCode.UInt32, TypeCode.UInt64
                    fieldType = TGIS_FieldType.gisFieldTypeNumber 'Number
                Case TypeCode.String, TypeCode.Char
                    fieldType = TGIS_FieldType.gisFieldTypeString 'String
                Case Else
                    Return False
            End Select
        Else
            Return False
        End If

        Return True
    End Function

    Friend Function GetCSScaleFactor() As Double
        'Returns coordinate system scale factor (i.e. NAD83_NSR2007_Mississippi_East_ftUS have a scale factor of 0.017...)
        If TypeOf GisCtrl.CS Is TGIS_CSGeographicCoordinateSystem Then
            Return CType(GisCtrl.CS, TGIS_CSGeographicCoordinateSystem).Units.Factor
        ElseIf TypeOf _GisCtrl.CS Is TGIS_CSProjectedCoordinateSystem Then
            Return CType(GisCtrl.CS, TGIS_CSProjectedCoordinateSystem).Units.Factor
        Else
            Return 1.0
        End If
    End Function

    Friend Function GetScaleUnit(Optional ByVal area As Boolean = False, Optional ByVal value As Double = 1.0) As TGIS_CSUnits
        If area Then
            Return _areaUnits.AutoSelect(True, value)
        Else
            Return ControlScale.Units.AutoSelect(False, value)
        End If
    End Function

    Friend Shared Function GetUniqueLayerName() As String
        'Generate a truely unique name so if one user remove one layer and add a new one it won't be mistaken to be the original layer
        Return Guid.NewGuid().ToString().Replace("-", vbNullString)
    End Function

    Friend Shared Function IsCtrlPressed() As Boolean
        'Determine if Ctrl is pressed, implemented as TGIS_ViewerWnd.KeyDown doesn't seem to be triggered
        Const vkControl As Integer = &H11
        If NativeMethods.GetKeyState(vkControl) < 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Friend Shared Function IsRelationField(ByVal fieldName As String) As Boolean
        fieldName = fieldName.ToUpperInvariant()

        If _
        String.Compare(fieldName, FieldNameRelation, StringComparison.OrdinalIgnoreCase) = 0 OrElse _
        String.Compare(fieldName, FieldNameSubrelation, StringComparison.OrdinalIgnoreCase) = 0 OrElse _
        String.Compare(fieldName, ForeignKeyField, StringComparison.OrdinalIgnoreCase) = 0 OrElse _
        String.Compare(fieldName, VersionField, StringComparison.OrdinalIgnoreCase) = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Friend Shared Sub UpdateLayerRelation(ByVal name As String, ByVal caption As String, ByVal epsg As Integer, ByVal rt As RelationType, Optional ByVal jv As String = vbNullString)
        Dim cmd As New OleDbCommand("", GisControlView.Conn)
        Dim reader As OleDbDataReader
        Dim ll As TGIS_LayerVector

        'Check if layer exist in index table
        cmd.CommandText = "SELECT 1 FROM gis_layers WHERE name = '" & name & "'"
        reader = cmd.ExecuteReader(CommandBehavior.SingleRow)
        If reader.HasRows Then
            'Update existing record (record should always exist from v1.0.8, this check is only for backwards compability)
            cmd.CommandText = "UPDATE gis_layers SET caption = '" & Microsoft.VisualBasic.Left(caption, GisControlView.FieldLengthLayerCaption).Replace("'", "''") & "', type = " & rt & ", join_view = '" & jv & "' WHERE name = '" & name & "'"
        Else
            'Add new record
            cmd.CommandText = "INSERT INTO gis_layers (name, caption, epsg, type, join_view) VALUES('" & name & "', '" & Microsoft.VisualBasic.Left(caption, GisControlView.FieldLengthLayerCaption).Replace("'", "''") & "'," & epsg & "," & rt & ", '" & jv & "')"
        End If
        reader.Close()
        cmd.ExecuteNonQuery()

        'Add/remove relation fields (vector layers only)
        ll = TryCast(GisControlView.Instance.GisCtrl.Get(name), TGIS_LayerVector)
        If ll IsNot Nothing Then
            If rt <> RelationType.Undefined Then
                'Add relation field
                If ll.FindField(GisControlView.FieldNameRelation) = -1 Then
                    ll.AddField(GisControlView.FieldNameRelation, TGIS_FieldType.gisFieldTypeNumber, 0, 0)
                End If

                Select Case rt
                    Case RelationType.StandRelation, RelationType.SectionRelation, RelationType.ObjectRelation, RelationType.TCTractRelation, RelationType.TCPlotRelation, RelationType.SampleTreeRelation
                        'Subrelation
                        If ll.FindField(GisControlView.FieldNameSubrelation) = -1 Then ll.AddField(GisControlView.FieldNameSubrelation, TGIS_FieldType.gisFieldTypeString, 50, 0)
                End Select
            Else
                'Remove any relation fields
                If ll IsNot Nothing Then
                    If ll.FindField(GisControlView.FieldNameRelation) <> -1 Then ll.DeleteField(GisControlView.FieldNameRelation)
                    If ll.FindField(GisControlView.FieldNameSubrelation) <> -1 Then ll.DeleteField(GisControlView.FieldNameSubrelation)
                End If
            End If
        End If
    End Sub

    Friend Shared Function AddProperty(ByVal county As String, ByVal municipal As String, ByVal parish As String, ByVal propNumber As String, ByVal propName As String, ByVal block As String, ByVal unit As String, ByVal objId As String, ByVal area As Double, ByVal measuredArea As Double, ByVal shp As TGIS_ShapeAbstract) As Integer
        Dim cmd As New OleDbCommand("", GisControlView.Conn)
        Dim reader As OleDbDataReader = Nothing
        Dim propId As Integer = -1

        'If name field contain block:unit we need to extract the name only
        If propName <> vbNullString AndAlso propName.Contains(":") Then propName = propName.Substring(0, propName.LastIndexOf(" "c))

        'Extract block:unit values
        If block <> vbNullString Then
            Dim blockunit As String() = Split(block, ":")
            If blockunit.Length >= 2 Then
                block = blockunit(0)
                'Remove any leading text (if extracted from a concatenated property name block:unit string)
                If block.Contains(" ") Then block = block.Substring(block.LastIndexOf(" "c) + 1)
            End If
        End If
        If unit <> vbNullString Then
            Dim blockunit As String() = Split(unit, ":")
            If blockunit.Length >= 2 Then
                unit = blockunit(1)
            End If
        End If

        'Check if this property already exist in the property table
        If propName <> vbNullString AndAlso block <> vbNullString AndAlso unit <> vbNullString Then
            cmd.CommandText = "SELECT id FROM fst_property_table WHERE prop_name = '" & propName & "' AND block_number = '" & block & "' AND unit_number = '" & unit & "'"
        Else
            If block <> vbNullString AndAlso unit <> vbNullString Then
                cmd.CommandText = "SELECT id FROM fst_property_table WHERE prop_number = '" & propNumber & "' AND block_number = '" & block & "' AND unit_number = '" & unit & "'"
            Else
                cmd.CommandText = "SELECT id FROM fst_property_table WHERE prop_number = '" & propNumber & "'"
            End If
        End If
        reader = cmd.ExecuteReader(CommandBehavior.SingleRow)
        If reader.Read() Then propId = reader.GetInt32(0)
        reader.Close()

        'If not, we will add it
        If propId < 0 Then
            If county <> vbNullString Then county = county.Replace("'", "''")
            If municipal <> vbNullString Then municipal = municipal.Replace("'", "''")
            If parish <> vbNullString Then parish = parish.Replace("'", "''")
            If propNumber <> vbNullString Then propNumber = propNumber.Replace("'", "''")
            If propName <> vbNullString Then propName = propName.Replace("'", "''")
            If block <> vbNullString Then block = block.Replace("'", "''")
            If unit <> vbNullString Then unit = unit.Replace("'", "''")
            If objId <> vbNullString Then objId = objId.Replace("'", "''")

            cmd.CommandText = "INSERT INTO fst_property_table (county_name, municipal_name, parish_name, prop_number,  prop_name, block_number, unit_number, areal_ha, areal_measured_ha, obj_id, created)" & _
                              "VALUES(" & _
                              "'" & Microsoft.VisualBasic.Left(county, _fieldLengthCounty) & "'," & _
                              "'" & Microsoft.VisualBasic.Left(municipal, _fieldLengthMunicipal) & "'," & _
                              "'" & Microsoft.VisualBasic.Left(parish, _fieldLengthParish) & "'," & _
                              "'" & Microsoft.VisualBasic.Left(propNumber, _fieldLengthPropNumber) & "'," & _
                              "'" & Microsoft.VisualBasic.Left(propName, _fieldLengthPropName) & "'," & _
                              "'" & Microsoft.VisualBasic.Left(block, _fieldLengthBlock) & "'," & _
                              "'" & Microsoft.VisualBasic.Left(unit, _fieldLengthUnit) & "'," & _
                              area.ToString(CultureInfo.InvariantCulture) & "," & _
                              measuredArea.ToString(CultureInfo.InvariantCulture) & "," & _
                              "'" & Microsoft.VisualBasic.Left(objId, _fieldLengthObjID) & "'," & _
                              "CURRENT_TIMESTAMP)"
            cmd.ExecuteNonQuery()

            'Get id of newly created property
            cmd.CommandText = "SELECT MAX(id) FROM fst_property_table"
            reader = cmd.ExecuteReader()
            If reader.Read() Then propId = reader.GetInt32(0)
            reader.Close()

            '#3724
            AddCentroidToPropertyTable(propId, shp)
        End If

        Return propId
    End Function

    Friend Shared Sub RemoveRelationCoord(ByVal shp As TGIS_Shape, ByVal type As RelationType)
        Dim sql As String = vbNullString

        'Make sure relation id is set
        If shp IsNot Nothing Then
            'Check kind of relation
            If type = RelationType.PropertyRelation Then
                sql = "UPDATE fst_property_table SET prop_coord = NULL WHERE id = " & shp.GetField(FieldNameRelation)

                'Clear image
                If sql <> vbNullString Then
                    Dim cmd As New OleDbCommand("", Conn)
                    cmd.CommandText = sql
                    cmd.ExecuteNonQuery()
                End If
            End If
        End If
    End Sub

    Friend Shared Sub RemoveRelationImage(ByVal shp As TGIS_Shape, ByVal type As RelationType)
        Dim sql As String = vbNullString

        'Make sure relation id is set
        If shp IsNot Nothing AndAlso IsNumeric(shp.GetField(FieldNameRelation)) Then
            'Check kind of relation
            If type = RelationType.StandRelation Then
                sql = "UPDATE esti_trakt_table SET trakt_img = NULL WHERE trakt_id = " & shp.GetField(FieldNameRelation)
            ElseIf type = RelationType.ObjectRelation Then
                sql = "UPDATE elv_object_table SET object_img = NULL WHERE object_id = " & shp.GetField(FieldNameRelation)
            End If

            'Clear image
            If sql <> vbNullString Then
                Dim cmd As New OleDbCommand("", Conn)
                cmd.CommandText = "UPDATE esti_trakt_table SET trakt_img = NULL WHERE trakt_id = " & shp.GetField(FieldNameRelation)
                cmd.ExecuteNonQuery()
            End If
        End If
    End Sub

    Friend Sub FindLayerAndShape(ByVal widgetId As Integer, ByVal type As RelationType, ByRef layerName As String, ByRef shapeId As Integer, Optional ByVal shapeType As TGIS_ShapeType = TGIS_ShapeType.gisShapeTypeUnknown)
        Dim cmd1 As New OleDbCommand("", Conn), cmd2 As New OleDbCommand("", Conn)
        Dim r1 As OleDbDataReader, r2 As OleDbDataReader
        Dim ll As TGIS_LayerVector
        Dim skipShape As Boolean

        shapeId = -1

        'Query db to see if this widget has a shape relation
        'Find all layers of specified type
        cmd1.CommandText = "SELECT name FROM gis_layers WHERE type = " & type
        r1 = cmd1.ExecuteReader()
        Do While r1.Read()
            'Check each layer for a relation to this widget
            layerName = r1.GetString(0)
            cmd2.CommandText = "SELECT uid FROM " & GisTablePrefix & layerName & "_FEA WHERE " & FieldNameRelation & " = " & widgetId
            Try
                r2 = cmd2.ExecuteReader()
            Catch ex As OleDbException
                'If this layer doesn't exist we better remove it from the index table
                cmd2.CommandText = "DELETE FROM gis_layers WHERE name = '" & layerName & "'"
                cmd2.ExecuteNonQuery()
                Continue Do
            End Try
            Do While r2.Read()
                'Make sure shape type match if specified
                skipShape = False
                If shapeType <> TGIS_ShapeType.gisShapeTypeUnknown Then
                    ll = GisCtrl.Get(layerName)
                    If ll IsNot Nothing AndAlso ll.GetShape(r2.GetInt32(0)).ShapeType <> shapeType Then
                        skipShape = True
                    End If
                End If

                If Not skipShape Then
                    shapeId = r2.GetInt32(0)
                    Exit Do 'Shape found
                End If
            Loop
            r2.Close()
            If shapeId <> -1 Then Exit Do
        Loop
        r1.Close()

        'Clear value if widget wasn't found
        If shapeId = -1 Then layerName = vbNullString
    End Sub

    Private Shared Function FilterPath(ByVal path As String)
        'Remove any illegal characters
        path = path.Replace("\"c, "_"c)
        path = path.Replace(""""c, "_"c)
        path = path.Replace("/"c, "_"c)
        path = path.Replace(":"c, "_"c)
        path = path.Replace("*"c, "_"c)
        path = path.Replace("?"c, "_"c)
        path = path.Replace("<"c, "_"c)
        path = path.Replace(">"c, "_"c)
        path = path.Replace("|"c, "_")
        Return path
    End Function

    Private Shared Function LatLongDdMmmmmmToCS(ByVal lat As String, ByVal lon As String, ByVal CS As TGIS_CSCoordinateSystem) As TGIS_Point
        Dim ptcs As New TGIS_Point

        'Make sure coordinates have the proper format
        If lat.Length < 9 Or lon.Length < 9 Then
            Return ptcs
        End If

        'Convert lat/long in format ddmm.mmmm to degrees
        lat = lat.Trim() : lon = lon.Trim() 'Make sure to remove any white-spaces first
        Dim latd As Double = Double.Parse(lat.Substring(0, 2), CultureInfo.InvariantCulture) + Double.Parse(lat.Substring(2, 7), CultureInfo.InvariantCulture) / 60.0
        Dim lond As Double = Double.Parse(lon.Substring(0, 2), CultureInfo.InvariantCulture) + Double.Parse(lon.Substring(2, 7), CultureInfo.InvariantCulture) / 60.0

        'Convert lat/long to current coordinate system
        Dim pt As New TGIS_Point(lond / 180.0 * Math.PI, latd / 180.0 * Math.PI)
        ptcs = CS.FromWGS(pt)

        Return ptcs
    End Function

    Private Shared Function ConvertStringDecimal(ByVal strValue As String) As String
        Dim strResult As String
        Dim fTmpValue As Decimal

        fTmpValue = Decimal.Parse(strValue)
        fTmpValue += 0.005
        fTmpValue *= 10.0
        strResult = Convert.ToInt32(fTmpValue)

        Return strResult
    End Function

    Friend Shared Function GetAngleBetweenLines(ByVal p1 As TGIS_Point, ByVal p2 As TGIS_Point, ByVal p3 As TGIS_Point, ByVal p4 As TGIS_Point)
        Dim v1 As TGIS_Point, v2 As TGIS_Point

        'Make vectors out of the line segements
        v1 = New TGIS_Point(p1.X - p2.X, p1.Y - p2.Y)
        v2 = New TGIS_Point(p4.X - p3.X, p4.Y - p3.Y)

        Return GetAngleBetweenVectors(v1, v2)
    End Function

    Friend Shared Function GetAngleBetweenVectors(ByVal v1 As TGIS_Point, ByVal v2 As TGIS_Point)
        Dim l As Double

        'Normalize both vectors
        l = Sqrt(v1.X * v1.X + v1.Y * v1.Y)
        v1.X /= l : v1.Y /= l

        l = Sqrt(v2.X * v2.X + v2.Y * v2.Y)
        v2.X /= l : v2.Y /= l

        'Return angle (use atan2 instead of acos to get +/- pi radians)
        Return Math.Atan2(v2.Y, v2.X) - Math.Atan2(v1.Y, v1.X)
    End Function

    Friend Shared Function GetClosestLineSegment(ByVal cs As TGIS_CSCoordinateSystem, ByVal pt As TGIS_Point, ByVal shpLine As TGIS_Shape, Optional ByRef isLeft As Boolean = False)
        Dim i As Integer, idx As Integer = -1
        Dim mind As Double, dist As Double
        Dim outside As Boolean, belongsLine1 As Boolean

        'Check point against every line segment
        mind = Double.MaxValue
        For i = 1 To shpLine.GetNumPoints() - 1
            'Check distance to current line segment
            dist = GetPointToLineDistance(cs, pt, shpLine.GetPoint(0, i - 1).X, shpLine.GetPoint(0, i - 1).Y, shpLine.GetPoint(0, i).X, shpLine.GetPoint(0, i).Y, Nothing, outside)

            'Determine closest line segment
            If Not outside AndAlso dist < mind Then
                'Determine side of this line segment
                isLeft = GetPointSideOfLine(pt, shpLine.GetPoint(0, i - 1).X, shpLine.GetPoint(0, i - 1).Y, shpLine.GetPoint(0, i).X, shpLine.GetPoint(0, i).Y)
                mind = dist
                idx = i - 1
            End If
        Next

        'Check if point is between two line segments
        If Not mind < Double.MaxValue Then
            'Determine point on line closest to current point
            For i = 0 To shpLine.GetNumPoints() - 1
                dist = GetPointToPointDistance(cs, pt, shpLine.GetPoint(0, i))
                If dist < mind Then
                    mind = dist
                    idx = i
                End If
            Next

            'Check if line start or end is closest to current point, in that case point is outside of the whole line
            If idx = 0 OrElse idx = shpLine.GetNumPoints() - 1 Then
                idx = -1 'Outside
            Else
                isLeft = GetPointSideOfLine(pt, shpLine.GetPoint(0, idx - 1), shpLine.GetPoint(0, idx), shpLine.GetPoint(0, idx + 1), belongsLine1)
                If belongsLine1 Then idx = idx - 1 'Use first line
            End If
        End If

        Return idx 'Return index of closest line segment
    End Function

    Friend Shared Function GetPointSideOfLine(ByVal pt As TGIS_Point, ByVal x1 As Double, ByVal y1 As Double, ByVal x2 As Double, ByVal y2 As Double) As Boolean
        'This function will return true if point is left of line, otherwise false. Result of formula is 0 if point are colinear to line
        Return ((x2 - x1) * (pt.Y - y1) - (y2 - y1) * (pt.X - x1)) > 0
    End Function

    Friend Shared Function GetPointSideOfLine(ByVal pt As TGIS_Point, ByVal p1 As TGIS_Point, ByVal p2 As TGIS_Point, ByVal p3 As TGIS_Point, ByRef belongsLine1 As Boolean) As Boolean
        Dim P As TGIS_Point

        'Determine side of line when point in between two line segments
        '                           LINE 2
        '                          /
        '                         /
        '                        /
        '                       / ) theta
        '          ------------/---------------- LINE 1
        '                     /|
        '           .        / |
        '        POINT      /  | P
        '      (epsilon)   /   |
        '                      V
        '
        ' *epsilon = angle between P and POINT

        'Determine counter-clockwise perpendicular vector to first line (P)
        P = New TGIS_Point(-(p2.Y - p1.Y), p2.X - p1.X)

        'Check angle between line 1 and line 2 (theta)
        If GetAngleBetweenLines(p1, p2, p2, p3) < 0 Then
            'Use clockwise perpendicular vector (rotate P 180 degrees)
            P.X = -P.X : P.Y = -P.Y
        End If

        'Determine angle between perpendicular vector (P) and vector from line joint to point (epsilon)
        If GetAngleBetweenVectors(P, New TGIS_Point(pt.X - p2.X, pt.Y - p2.Y)) < Math.PI Then
            'In front of line 1. Use line 1
            belongsLine1 = True
            Return GetPointSideOfLine(pt, p1.X, p1.Y, p2.X, p2.Y)
        Else
            'Behind line 1, in front of line 2. Use line 2
            belongsLine1 = False
            Return GetPointSideOfLine(pt, p2.X, p2.Y, p3.X, p3.Y)
        End If
    End Function

    Friend Shared Function GetPointToLineDistance(ByVal cs As TGIS_CSCoordinateSystem, ByVal pt As TGIS_Point, ByVal x1 As Double, ByVal y1 As Double, ByVal x2 As Double, ByVal y2 As Double, _
                                                  Optional ByVal units As TGIS_CSUnits = Nothing, Optional ByRef outside As Boolean = False, Optional ByRef nx As Double = 0, Optional ByRef ny As Double = 0) As Double
        Dim newx As Double, newy As Double, u As Double
        Dim minx As Double, miny As Double, maxx As Double, maxy As Double

        'Make sure end points of the line doesn't coincide
        If x2 - x1 = 0 AndAlso y2 - y1 = 0 OrElse (Double.IsNaN(x1) OrElse Double.IsNaN(x2) OrElse Double.IsNaN(y1) OrElse Double.IsNaN(y2)) Then 'HACKHACK: sometimes we get strange values, this will prevent calculation on them
            Return Double.MaxValue
        End If

        'Get line min/max
        minx = Min(x1, x2)
        miny = Min(y1, y2)
        maxx = Max(x1, x2)
        maxy = Max(y1, y2)

        'Find the point nearest on the line to the given point
        u = ((pt.X - x1) * (x2 - x1) + (pt.Y - y1) * (y2 - y1)) / ((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))
        newx = x1 + u * (x2 - x1)
        newy = y1 + u * (y2 - y1)

        'Check if point is parallell to line segment
        If (newx < minx OrElse newx > maxx) AndAlso (newy < miny OrElse newy > maxy) Then outside = True Else outside = False

        'If intersection point is outside of line segment, bind to nearest point on segment
        If newx < minx Then newx = minx Else If newx > maxx Then newx = maxx
        If newy < miny Then newy = miny Else If newy > maxy Then newy = maxy

        Return GetPointToPointDistance(cs, pt.X, pt.Y, newx, newy, units)
    End Function

    Friend Shared Function GetPointToPointDistance(ByVal cs As TGIS_CSCoordinateSystem, ByVal x1 As Double, ByVal y1 As Double, ByVal x2 As Double, ByVal y2 As Double, Optional ByVal u As TGIS_CSUnits = Nothing) As Double
        Dim dist As Double

        dist = cs.Distance(New TGIS_Point(x1, y1), New TGIS_Point(x2, y2))

        If u Is Nothing Then 'Use default scale unit if nothing else is specified
            dist /= GisControlView.Instance.GetScaleUnit().Factor
        Else
            dist /= u.Factor
        End If

        Return dist
    End Function

    Friend Shared Function GetPointToPointDistance(ByVal cs As TGIS_CSCoordinateSystem, ByVal pt1 As TGIS_Point, ByVal pt2 As TGIS_Point, Optional ByVal u As TGIS_CSUnits = Nothing) As Double
        Return GetPointToPointDistance(cs, pt1.X, pt1.Y, pt2.X, pt2.Y, u)
    End Function

    Private Sub EditShape(ByVal shp As TGIS_Shape, ByVal part As Integer)
        GisCtrl.Editor.EditShape(shp, part)

        'Make sure snap layer is consistent (Integer.MaxValue represent no snapping)
        If _snapLayerGallery.SelectedItem IsNot Nothing AndAlso _snapLayerGallery.SelectedItem.Id <> Integer.MaxValue Then GisCtrl.Editor.SnapLayer = GisCtrl.Items(_snapLayerGallery.SelectedItem.Id)
    End Sub

    Public Sub UpdateStandArea(ByVal standId As Integer)
        Dim layer As String = vbNullString
        Dim uid As Integer
        Dim ll As TGIS_LayerVector

        'Find associated stand polygon
        FindLayerAndShape(standId, RelationType.StandRelation, layer, uid, TGIS_ShapeType.gisShapeTypePolygon)

        'Update area in db, this will also refresh UMEstimate
        ll = GisCtrl.Get(layer)
        If ll IsNot Nothing Then
            UpdateStandArea(standId, ll.GetShape(uid))
        End If
    End Sub

    Private Sub UpdateStandArea(ByVal standId As Integer, ByVal shp As TGIS_Shape)
        Dim area As String
        Dim cmd As New OleDbCommand("", Conn)

        area = Math.Round((shp.AreaCS() / 10000.0), 3).ToString(CultureInfo.InvariantCulture)
        cmd.CommandText = "UPDATE esti_trakt_table SET trakt_areal_handled = " & area & ", trakt_areal = " & area & " + trakt_areal_consider WHERE trakt_id = " & standId
        cmd.ExecuteNonQuery()

        'Tell UMEstimate to reload without saving
        RaiseEvent RefreshStand(Me, EventArgs.Empty)
    End Sub

    Private Function GenerateShapeFromStand(ByVal id As Integer, ByVal updateCoords As Boolean) As Boolean
        Dim layerName As String
        Dim standChunk As String = vbNullString
        Dim standName As String = vbNullString
        Dim plotCoords As New List(Of ValueDescriptionPair)
        Dim cmd As New OleDbCommand("", Conn), cmd2 As New OleDbCommand("", Conn)
        Dim reader As OleDbDataReader

        'Get coordinates and name of stand from db
        cmd.CommandText = "SELECT trakt_coordinates, trakt_name FROM esti_trakt_table WHERE trakt_id = " & id
        reader = cmd.ExecuteReader()
        If reader.Read() Then
            If Not reader.IsDBNull(0) Then standChunk = reader.GetString(0)
            standName = reader.GetString(1)
        End If
        reader.Close()

        'Get any plot coordinates for this stand from db
        cmd.CommandText = "SELECT tplot_id, tplot_coord FROM esti_trakt_plot_table WHERE tplot_trakt_id = " & id
        reader = cmd.ExecuteReader()
        Do While reader.Read()
            If Not reader.IsDBNull(1) Then
                plotCoords.Add(New ValueDescriptionPair(reader.GetInt32(0), reader.GetString(1)))
            End If
        Loop
        reader.Close()

        'Parse coordinate chunks
        If standChunk <> vbNullString OrElse plotCoords.Count > 0 Then
            Dim llStand As TGIS_LayerVector = Nothing, llPlot As TGIS_LayerVector = Nothing
            Dim shpStand As TGIS_Shape = Nothing
            Dim rt As RelationType

            Dim dlg As New LayerChoiceDialog(RelationType.StandRelation, LangStr(_strIdStandLayer), standChunk <> vbNullString, TGIS_ShapeType.gisShapeTypePolygon, True, _
                        RelationType.StandRelation, LangStr(_strIdPlotLayer), plotCoords.Count > 0)
            If dlg.ShowDialog() = DialogResult.OK Then
                'Remove any previous stand shapes on update
                If updateCoords Then
                    If EstimateSodraInstalled Then rt = RelationType.SectionRelation Else rt = RelationType.StandRelation
                    cmd.CommandText = "SELECT name FROM gis_layers WHERE type = " & rt
                    reader = cmd.ExecuteReader()
                    Do While reader.Read()
                        Try
                            cmd2.CommandText = "DELETE FROM " & GisTablePrefix & reader.GetString(0) & "_GEO WHERE uid IN (SELECT uid FROM " & GisTablePrefix & reader.GetString(0) & "_FEA WHERE " & FieldNameRelation & " = " & id & ")"
                            cmd2.ExecuteNonQuery()
                            cmd2.CommandText = "DELETE FROM " & GisTablePrefix & reader.GetString(0) & "_FEA WHERE " & FieldNameRelation & " = " & id
                            cmd2.ExecuteNonQuery()
                        Catch ex As OleDbException
                            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                        End Try
                    Loop
                    reader.Close()
                End If

                'Generate a shape in the stand layer
                Try
                    If standChunk <> vbNullString Then
                        If dlg.LayerOne = NewLayerName Then
                            'Create a new layer
                            layerName = AddNewLayer(False, standName, RelationType.StandRelation)
                            llStand = GisCtrl.Get(layerName)
                        Else
                            'Use specified layer
                            llStand = GisCtrl.Get(dlg.LayerOne)
                        End If

                        If llStand IsNot Nothing Then
                            'Add stand boundary
                            Dim latlong As String() = standChunk.Split(vbCrLf)
                            For Each pts As String In latlong
                                Dim coords As String() = pts.Split(",")
                                If coords.Length = 2 Then
                                    'Create a new shape for this stand
                                    If shpStand Is Nothing Then
                                        shpStand = llStand.CreateShape(TGIS_ShapeType.gisShapeTypePolygon)
                                        shpStand.AddPart()
                                        shpStand.SetField(FieldNameRelation, id) 'Stand id
                                        AddShpToAttributeTable(shpStand) 'Add to attribute viewer
                                    End If
                                    'Add point to shape
                                    If shpStand IsNot Nothing Then shpStand.AddPoint(LatLongDdMmmmmmToCS(coords(0), coords(1), GisCtrl.CS))
                                End If
                            Next
                        End If
                    End If

                    'Generate a shape for each plot
                    If plotCoords.Count > 0 Then
                        If dlg.LayerTwo = NewLayerName Then
                            'Create a new layer for this stand
                            layerName = AddNewLayer(False, standName, RelationType.StandRelation)
                            llPlot = GisCtrl.Get(layerName)
                        Else
                            'Use specified layer
                            llPlot = GisCtrl.Get(dlg.LayerTwo)
                        End If

                        If llPlot IsNot Nothing Then
                            Dim shp As TGIS_Shape = Nothing
                            For Each plot As ValueDescriptionPair In plotCoords
                                Dim coords As String() = plot.Description.Split(",")
                                If coords.Length = 2 Then
                                    'Create a new point for each plot
                                    shp = llPlot.CreateShape(TGIS_ShapeType.gisShapeTypePoint)
                                    shp.AddPart()
                                    shp.AddPoint(LatLongDdMmmmmmToCS(coords(0), coords(1), GisCtrl.CS))
                                    shp.SetField(FieldNameRelation, id) 'Stand id
                                    shp.SetField(FieldNameSubrelation, id & "/" & plot.Value) 'Combined 'StandId/PlotIndex'
                                    AddShpToAttributeTable(shp) 'Add to attribute viewer
                                End If
                            Next
                        End If
                    End If

                    If shpStand IsNot Nothing Then
                        'Zoom to created shape and save project
                        GisCtrl.VisibleExtent = shpStand.ProjectedExtent
                    ElseIf llPlot IsNot Nothing Then
                        'Zoom in on whole layer (in case we have some plots but no stand boundary)
                        GisCtrl.VisibleExtent = llPlot.ProjectedExtent
                    End If

                    'Save changes
                    If llStand IsNot Nothing Then llStand.SaveAll()
                    If llPlot IsNot Nothing Then llPlot.SaveAll()
                Catch ex As FormatException 'May be thrown from LatLongDdMmmmmmToCS
                    MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                End Try
            End If

            'Success (coordinates exist, no check if a shape was created or not)
            If llStand IsNot Nothing Then 'Select target layer
                SetActiveLayer(llStand)
            ElseIf llPlot IsNot Nothing Then
                SetActiveLayer(llPlot)
            End If
            Return True
        End If

        Return False
    End Function

    Private Function GenerateShapeFromTcTract(ByVal id As Integer) As Boolean
        Dim plotsExist As Boolean
        Dim cmd As New OleDbCommand("", Conn)
        Dim reader As OleDbDataReader = Nothing

        'First make sure lat/long is given for some plots
        cmd.CommandText = "SELECT MAX(latitude), MAX(longitude) FROM tc_plot WHERE fileindex = " & id
        reader = cmd.ExecuteReader(CommandBehavior.SingleRow)
        If reader.Read() AndAlso Not reader.IsDBNull(0) AndAlso Not reader.IsDBNull(1) Then
            If reader.GetDouble(0) <> 0 OrElse reader.GetDouble(1) <> 0 Then
                plotsExist = True
            End If
        End If
        reader.Close()

        If plotsExist Then
            Dim lat As Double, lon As Double
            Dim standName As String = vbNullString, layerName As String
            Dim ll As TGIS_LayerVector = Nothing
            Dim pt As TGIS_Point, ptcs As TGIS_Point
            Dim shp As TGIS_Shape = Nothing

            'Use stand name as layer title
            cmd.CommandText = "SELECT tractid FROM tc_stand WHERE fileindex = " & id
            reader = cmd.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read() Then
                standName = reader.GetString(0)
            End If
            reader.Close()

            Dim dlg As New LayerChoiceDialog(RelationType.TCPlotRelation, LangStr(_strIdPlotLayer), True, TGIS_ShapeType.gisShapeTypePoint)
            If dlg.ShowDialog() = DialogResult.OK Then
                If dlg.LayerOne = NewLayerName Then
                    'Create a new layer
                    layerName = AddNewLayer(False, standName, RelationType.TCPlotRelation, ViewNameTcPlotData)
                    ll = GisCtrl.Get(layerName)
                Else
                    'Find specified layer
                    ll = GisCtrl.Get(dlg.LayerOne)
                End If

                If ll IsNot Nothing Then
                    'Get plot coordinates from db
                    cmd.CommandText = "SELECT latitude, longitude, plotindex FROM tc_plot WHERE fileindex = " & id
                    reader = cmd.ExecuteReader()
                    Do While reader.Read()
                        lat = reader.GetDouble(0)
                        lon = reader.GetDouble(1)

                        'Convert lat/long to current coordinate system (if specified for this plot)
                        If lat <> 0 OrElse lon <> 0 Then
                            pt = New TGIS_Point(lon / 180.0 * Math.PI, lat / 180.0 * Math.PI)
                            ptcs = GisCtrl.CS.FromWGS(pt)

                            shp = ll.CreateShape(TGIS_ShapeType.gisShapeTypePoint)
                            If shp IsNot Nothing Then
                                shp.SetField(FieldNameRelation, id) 'Tract id
                                shp.SetField(FieldNameSubrelation, id & "/" & reader.GetInt32(2)) 'Combined "TractId/PlotIndex"
                                shp.AddPart()
                                shp.AddPoint(ptcs)
                                AddShpToAttributeTable(shp) 'Add to attribute viewer
                            End If
                        End If
                    Loop

                    'Zoom to destination layer
                    GisCtrl.VisibleExtent = ll.ProjectedExtent
                    ll.SaveAll()
                End If
            End If

            'Success (coordinates exist, no check whether a shape was created or not)
            If ll IsNot Nothing Then SetActiveLayer(ll) 'Select target layer
            Return True
        End If

        Return False
    End Function

    Private Sub CreateDefaultLayers()
        Dim ll As TGIS_LayerVector

        If ForestInstalled Then
            'Property
            ll = GisCtrl.Get(AddNewLayer(True, LangStr(_strIdLayerProperty), RelationType.PropertyRelation, ViewNamePropertyData))
            If ll IsNot Nothing Then
                ll.Params.Area.Color = Color.Maroon
                ll.Params.Line.Color = Color.Maroon
                ll.Params.Marker.Color = Color.Maroon
                ll.Params.Labels.Field = FieldNameProperty
            End If
        End If
        If EstimateInstalled Then
            If EstimateSodraInstalled Then
                'Stand
                ll = GisCtrl.Get(AddNewLayer(True, LangStr(_strIdLayerStand), RelationType.SectionRelation, ViewNameSectionData))
                If ll IsNot Nothing Then
                    ll.Params.Area.Color = Color.Navy
                    ll.Params.Line.Color = Color.Navy
                    ll.Params.Marker.Color = Color.Navy
                    ll.Params.Labels.Field = FieldNameSection
                End If
            Else
                'Section (S�dra)
                ll = GisCtrl.Get(AddNewLayer(True, LangStr(_strIdLayerStand), RelationType.StandRelation, ViewNameStandData))
                If ll IsNot Nothing Then
                    ll.Params.Area.Color = Color.Navy
                    ll.Params.Line.Color = Color.Navy
                    ll.Params.Marker.Color = Color.Navy
                    ll.Params.Labels.Field = FieldNameStand
                End If
            End If
        End If
        If LandValueInstalled Then
            'Object
            ll = GisCtrl.Get(AddNewLayer(True, LangStr(_strIdLayerObject), RelationType.ObjectRelation, ViewNameObjectData))
            If ll IsNot Nothing Then
                ll.Params.Area.Color = System.Drawing.Color.FromArgb(&HFF6600)
                ll.Params.Line.Color = System.Drawing.Color.FromArgb(&HFF6600)
                ll.Params.Marker.Color = System.Drawing.Color.FromArgb(&HFF6600)
                ll.Params.Labels.Field = FieldNameObjectProperty
            End If
        End If
        If EstimateInstalled Then 'Intentionally placed last to get on top
            'Plot
            ll = GisCtrl.Get(AddNewLayer(True, LangStr(_strIdLayerPlot), RelationType.StandRelation, ViewNamePlotData))
            If ll IsNot Nothing Then
                ll.Params.Area.Color = System.Drawing.Color.FromArgb(&H33CCCC)
                ll.Params.Line.Color = System.Drawing.Color.FromArgb(&H33CCCC)
                ll.Params.Marker.Color = System.Drawing.Color.FromArgb(&H33CCCC)
            End If

            'Trees
            ll = GisCtrl.Get(AddNewLayer(True, LangStr(_strIdLayerTrees), RelationType.SampleTreeRelation, ViewNameSampleTreeData))
            If ll IsNot Nothing Then
                ll.Params.Area.Color = System.Drawing.Color.FromArgb(&HFF00)
                ll.Params.Line.Color = System.Drawing.Color.FromArgb(&HFF00)
                ll.Params.Marker.Color = System.Drawing.Color.FromArgb(&HFF00)
            End If
        End If
    End Sub

#Region "DB Handling"
    Private Shared Sub CreateDbTables(ByRef firstStart As Boolean)
        Dim cmd As New OleDbCommand("", Conn)

        firstStart = False

        'Determine whether this is a new database or not
        If Not ExistsSql("SELECT 1 FROM sysobjects WHERE name LIKE 'gis_layers' AND xtype='U'") Then firstStart = True

        'Create layer index table (holds relation type for each layer)
        cmd.CommandText = "IF NOT EXISTS(SELECT 1 FROM sysobjects WHERE name LIKE 'gis_layers' AND xtype='U')" & _
                          "CREATE TABLE [dbo].[gis_layers](" & _
                          "[name] [nvarchar](64) NOT NULL," & _
                          "[caption] [nvarchar](" & FieldLengthLayerCaption & ") NOT NULL," & _
                          "[epsg] [int] NOT NULL," & _
                          "[type] [smallint] NULL," & _
                          "[join_view] [nvarchar](64) NULL," & _
                          "CONSTRAINT [PK_gis_relations] PRIMARY KEY CLUSTERED" & _
                          "(" & _
                          "[name] ASC" & _
                          ")" & _
                          ")" & _
                          "ELSE " & _
                          "BEGIN " & _
                          "IF NOT EXISTS(SELECT 1 FROM syscolumns WHERE name LIKE 'join_view' AND id = OBJECT_ID('gis_layers'))" & _
                          "ALTER TABLE [dbo].[gis_layers] ADD [join_view] [nvarchar](64) NULL " & _
                          "IF NOT EXISTS(SELECT 1 FROM syscolumns WHERE name LIKE 'caption' AND id = OBJECT_ID('gis_layers'))" & _
                          "ALTER TABLE [dbo].[gis_layers] ADD [caption] [nvarchar](" & FieldLengthLayerCaption & ") NULL " & _
                          "IF NOT EXISTS(SELECT 1 FROM syscolumns WHERE name LIKE 'epsg' AND id = OBJECT_ID('gis_layers'))" & _
                          "ALTER TABLE [dbo].[gis_layers] ADD [epsg] [int] DEFAULT 0 NOT NULL " & _
                          "END"
        cmd.ExecuteNonQuery()

        'Create project file table
        cmd.CommandText = "IF NOT EXISTS(SELECT 1 FROM sysobjects WHERE name LIKE 'gis_project_files' AND xtype='U') " & _
                          "BEGIN " & _
                          "CREATE TABLE [dbo].[gis_project_files](" & _
                          "[project_file] [nvarchar](MAX) NOT NULL, " & _
                          "[user] [nvarchar](50) NULL, " & _
                          "[updated] [datetime] NOT NULL) " & _
                          "END"
        cmd.ExecuteNonQuery()


        'Create product specific views for installed products
        'Property data
        If IsViewCreationRequired(ViewNamePropertyData, _viewVersionPropertyData, "fst_property_table") Then
            'Create view
            DropView(ViewNamePropertyData)
            cmd.CommandText = "CREATE VIEW dbo.gis_propertydata AS " & _
                              "SELECT " & _
                              "id [key1]," & _
                              "prop_number [10209]," & _
                              "prop_name [10210]," & _
                              "block_number [10211]," & _
                              "unit_number [10212]," & _
                              "areal_ha [10213]," & _
                              "areal_measured_ha [10214] " & _
                              "FROM fst_property_table"
            cmd.ExecuteNonQuery()
            SetViewVersion(ViewNamePropertyData, _viewVersionPropertyData)
        End If

        'Estimate plot data
        If IsViewCreationRequired(ViewNamePlotData, _viewVersionPlotData, "esti_trakt_table") Then
            'Create view
            DropView(ViewNamePlotData)
            cmd.CommandText = "CREATE VIEW dbo.gis_plotdata AS " & _
                              "SELECT " & _
                              "CAST(tdcls_trakt_id AS varchar) + '/' + CAST(tdcls_plot_id AS varchar) [key1]," & _
                              "trakt_name [10215]," & _
                              "tdcls_plot_id [10216]," & _
                              "SUM(tdcls_numof + tdcls_numof_randtrees) [10217]," & _
                              "SUM(DISTINCT tplot_area) [10218]," & _
                              "CASE WHEN (SUM(DISTINCT tplot_area) / 10000) <> 0 THEN ROUND(SUM(SQUARE((tdcls_dcls_from + tdcls_dcls_to) / 400) * PI() * (tdcls_numof + tdcls_numof_randtrees)) / (SUM(DISTINCT tplot_area) / 10000), 2) ELSE NULL END [10219]," & _
                              "ROUND(SUM((tdcls_dcls_from + tdcls_dcls_to) / 2 * (tdcls_numof + tdcls_numof_randtrees)) / SUM(tdcls_numof + tdcls_numof_randtrees), 2) [10220]," & _
                              "ROUND(SQRT(SUM(SQUARE((tdcls_dcls_from + tdcls_dcls_to) / 2) * (tdcls_numof + tdcls_numof_randtrees)) / SUM(tdcls_numof + tdcls_numof_randtrees)), 2) [10221]," & _
                              "ROUND(SUM(POWER(((tdcls_dcls_from + tdcls_dcls_to) / 2), 3) * (tdcls_numof + tdcls_numof_randtrees)) / SUM(SQUARE((tdcls_dcls_from + tdcls_dcls_to) / 2) * (tdcls_numof + tdcls_numof_randtrees)), 2) [10222]," & _
                              "CASE WHEN SUM(tdcls_m3sk) <> 0 THEN ROUND(SUM(tdcls_hgt * 0.1 * tdcls_m3sk) / SUM(tdcls_m3sk), 2) ELSE NULL END [10223]," & _
                              "ROUND(SUM(tdcls_hgt * 0.1 * (tdcls_numof + tdcls_numof_randtrees)) / SUM(tdcls_numof + tdcls_numof_randtrees), 2) [10224]," & _
                              "ROUND(SUM(tdcls_m3sk), 2) [10225]," & _
                              "ROUND(SUM(tdcls_m3fub), 2) [10226]," & _
                              "ROUND(SUM(tdcls_m3sk) / SUM(tdcls_numof + tdcls_numof_randtrees), 2) [10227]," & _
                              "ROUND(SUM(tdcls_m3fub) / SUM(tdcls_numof + tdcls_numof_randtrees), 2) [10228] " & _
                              "FROM esti_trakt_plot_table p " & _
                              "INNER JOIN esti_trakt_dcls_trees_table d ON d.tdcls_trakt_id = p.tplot_trakt_id AND d.tdcls_plot_id = p.tplot_id " & _
                              "INNER JOIN esti_trakt_table t ON t.trakt_id = p.tplot_trakt_id " & _
                              "GROUP BY tdcls_plot_id, tdcls_trakt_id, trakt_name"
            cmd.ExecuteNonQuery()
            SetViewVersion(ViewNamePlotData, _viewVersionPlotData)
        End If

        'Estimate stand data
        If IsViewCreationRequired(ViewNameStandData, _viewVersionStandData, "esti_trakt_table") Then
            'Create view
            DropView(ViewNameStandData)
            cmd.CommandText = "CREATE VIEW dbo.gis_standdata AS " & _
                              "SELECT " & _
                              "trakt_id [key1]," & _
                              "trakt_name [10229]," & _
                              "SUM(tdata_numof) [10230]," & _
                              "SUM(DISTINCT trakt_areal) [10231]," & _
                              "SUM(DISTINCT trakt_age) [10232]," & _
                              "MIN(trakt_si_h100) [10233]," & _
                              "ROUND((SELECT SUM(tdcls_grot) FROM esti_trakt_dcls_trees_table WHERE tdcls_trakt_id = t.trakt_id), 2) [10234]," & _
                              "(SELECT SUM(tdcls_growth) FROM esti_trakt_dcls_trees_table WHERE tdcls_trakt_id = t.trakt_id) [10235]," & _
                              "ROUND(SUM(tdata_gy), 2) [10236]," & _
                              "CASE WHEN SUM(tdata_numof) <> 0 THEN ROUND(SUM(tdata_da * tdata_numof) / SUM(tdata_numof), 2) ELSE NULL END [10237]," & _
                              "ROUND((SELECT SQRT(SUM(SQUARE((tdcls_dcls_from + tdcls_dcls_to) / 2) * (tdcls_numof + tdcls_numof_randtrees)) / SUM(tdcls_numof + tdcls_numof_randtrees)) FROM esti_trakt_dcls_trees_table d WHERE d.tdcls_trakt_id = t.trakt_id), 2) [10238]," & _
                              "ROUND((SELECT SUM(POWER(((tdcls_dcls_from + tdcls_dcls_to) / 2), 3) * (tdcls_numof + tdcls_numof_randtrees)) / SUM(SQUARE((tdcls_dcls_from + tdcls_dcls_to) / 2) * (tdcls_numof + tdcls_numof_randtrees)) FROM esti_trakt_dcls_trees_table d WHERE d.tdcls_trakt_id = t.trakt_id), 2) [10239]," & _
                              "CASE WHEN SUM(tdata_m3sk_vol) <> 0 THEN ROUND(SUM(tdata_hgv * tdata_m3sk_vol) / SUM(tdata_m3sk_vol), 2) ELSE NULL END [10240]," & _
                              "CASE WHEN SUM(tdata_numof) <> 0 THEN ROUND(SUM(tdata_avg_hgt * tdata_numof) / SUM(tdata_numof), 2) ELSE NULL END [10241]," & _
                              "ROUND(SUM(tdata_m3sk_vol), 2) [10242]," & _
                              "ROUND(SUM(tdata_m3fub_vol), 2) [10243]," & _
                              "CASE WHEN SUM(tdata_numof) <> 0 THEN ROUND(SUM(tdata_m3sk_vol) / SUM(tdata_numof), 2) ELSE NULL END [10244]," & _
                              "CASE WHEN SUM(tdata_numof) <> 0 THEN ROUND(SUM(tdata_m3fub_vol) / SUM(tdata_numof), 2) ELSE NULL END [10245]," & _
                              "ROUND(SUM(DISTINCT rot_post_value), 0) [10246]," & _
                              "ROUND(SUM(DISTINCT rot_post_value - rot_post_cost1 - rot_post_cost2 - rot_post_cost3 - rot_post_cost5 - rot_post_cost6 - rot_post_cost7 - rot_post_cost8), 0) [10247]," & _
                              "ROUND(SUM(DISTINCT rot_post_cost1), 0) [10248]," & _
                              "ROUND(SUM(DISTINCT rot_post_cost3), 0) [10249]," & _
                              "ROUND(SUM(DISTINCT rot_post_cost5 + rot_post_cost8), 0) [10250]," & _
                              "ROUND(SUM(DISTINCT rot_post_cost7), 0) [10251] " & _
                              "FROM esti_trakt_table t " & _
                              "INNER JOIN esti_trakt_data_table d ON d.tdata_trakt_id = t.trakt_id " & _
                              "LEFT JOIN esti_trakt_rotpost_table r ON r.rot_trakt_id = t.trakt_id " & _
                              "GROUP BY trakt_id, trakt_name"
            cmd.ExecuteNonQuery()
            SetViewVersion(ViewNameStandData, _viewVersionStandData)
        End If

        'Estimate section data
        If IsViewCreationRequired(ViewNameSectionData, _viewVersionSectionData, "esti_trakt_sodra_gis_table") Then
            'Create view
            DropView(ViewNameSectionData)
            cmd.CommandText = "CREATE VIEW dbo.gis_sectiondata AS " & _
                              "SELECT trakt_id [key1]," & _
                              "trakt_name [Inv. Avdelningnr]," & _
                              "trakt_age [Inv. �lder]," & _
                              "trakt_sodra_ovreHojd*10 [Inv. Medelh�jd]," & _
                              "ROUND(m3sk,3) [Inv. Volym (m3sk)]," & _
                              "REPLACE(REPLACE(REPLACE(trakt_si_h100,'T','1'),'G','2'),'B','3') [Inv. SI]," & _
                              "ROUND(gy,0) [Inv. GY] " & _
                              "FROM esti_trakt_table t " & _
                              "INNER JOIN (SELECT tdata_trakt_id, SUM(tdata_m3sk_vol) m3sk, SUM(tdata_gy) gy FROM esti_trakt_data_table WHERE tdata_data_type = 1 GROUP BY(tdata_trakt_id)) d ON d.tdata_trakt_id = t.trakt_id"
            cmd.ExecuteNonQuery()
            SetViewVersion(ViewNameSectionData, _viewVersionSectionData)
        End If

        'Estimate section plot data
        If IsViewCreationRequired(ViewNameSectionPlotData, _viewVersionSectionPlotData, "esti_trakt_sodra_gis_table") Then
            'Create view
            DropView(ViewNameSectionPlotData)
            cmd.CommandText = "CREATE VIEW dbo.gis_sectionplotdata AS " & _
                              "SELECT " & _
                              "CAST(tdcls_trakt_id AS varchar) + '/' + CAST(tdcls_plot_id AS varchar) [key1]," & _
                              "trakt_name [Best�nd]," & _
                              "tdcls_plot_id [Yta]," & _
                              "SUM(tdcls_numof + tdcls_numof_randtrees) [Antal_stam]," & _
                              "SUM(DISTINCT tplot_area) [Areal]," & _
                              "CASE WHEN (SUM(DISTINCT tplot_area) / 10000) <> 0 THEN ROUND(SUM(SQUARE((tdcls_dcls_from + tdcls_dcls_to) / 400) * PI() * (tdcls_numof + tdcls_numof_randtrees)) / (SUM(DISTINCT tplot_area) / 10000), 2) ELSE NULL END [Gy]," & _
                              "ROUND(SUM((tdcls_dcls_from + tdcls_dcls_to) / 2 * (tdcls_numof + tdcls_numof_randtrees)) / SUM(tdcls_numof + tdcls_numof_randtrees), 2) [Da]," & _
                              "ROUND(SQRT(SUM(SQUARE((tdcls_dcls_from + tdcls_dcls_to) / 2) * (tdcls_numof + tdcls_numof_randtrees)) / SUM(tdcls_numof + tdcls_numof_randtrees)), 2) [Dg]," & _
                              "ROUND(SUM(POWER(((tdcls_dcls_from + tdcls_dcls_to) / 2), 3) * (tdcls_numof + tdcls_numof_randtrees)) / SUM(SQUARE((tdcls_dcls_from + tdcls_dcls_to) / 2) * (tdcls_numof + tdcls_numof_randtrees)), 2) [Dgv]," & _
                              "CASE WHEN SUM(tdcls_m3sk) <> 0 THEN ROUND(SUM(tdcls_hgt * 0.1 * tdcls_m3sk) / SUM(tdcls_m3sk), 2) ELSE NULL END [Hgv]," & _
                              "ROUND(SUM(tdcls_hgt * 0.1 * (tdcls_numof + tdcls_numof_randtrees)) / SUM(tdcls_numof + tdcls_numof_randtrees), 2) [Medelh�jd]," & _
                              "ROUND(SUM(tdcls_m3sk), 3) [Volym_m3sk]," & _
                              "ROUND(SUM(tdcls_m3fub), 3) [Volym_m3fub]," & _
                              "ROUND(SUM(tdcls_m3sk) / SUM(tdcls_numof + tdcls_numof_randtrees), 2) [M.volym_m3sk]," & _
                              "ROUND(SUM(tdcls_m3fub) / SUM(tdcls_numof + tdcls_numof_randtrees), 2) [M.vol_m3fub]," & _
                              "AVG(tplot_sodra_ohgt) [�vre h�jd]," & _
                              "AVG(tplot_sodra_dbhage) [BHD �lder]," & _
                              "REPLACE(REPLACE(REPLACE(AVG(tplot_sodra_sispec),'1','T'),'2','G'),'3','B') + LTRIM(STR(AVG(tplot_sodra_si) / 10.0, 10, 1)) [SI] " & _
                              "FROM esti_trakt_plot_table p " & _
                              "INNER JOIN esti_trakt_dcls_trees_table d ON d.tdcls_trakt_id = p.tplot_trakt_id AND d.tdcls_plot_id = p.tplot_id " & _
                              "INNER JOIN esti_trakt_table t ON t.trakt_id = p.tplot_trakt_id " & _
                              "GROUP BY tdcls_plot_id, tdcls_trakt_id, trakt_name"
            cmd.ExecuteNonQuery()
            SetViewVersion(ViewNameSectionPlotData, _viewVersionSectionPlotData)
        End If

        'LandValue property data per object
        If IsViewCreationRequired(ViewNameObjectData, _viewVersionObjectData, "elv_properties_table") Then
            'Create view
            DropView(ViewNameObjectData)
            cmd.CommandText = "CREATE VIEW dbo.gis_objectdata AS " & _
                              "SELECT " & _
                              "CAST(prop_object_id AS varchar) + '/' + CAST(prop_id AS varchar) [key1]," & _
                              "(SELECT CASE WHEN block_number <> '' OR unit_number <> '' THEN prop_name + ' ' + block_number + ':' + unit_number ELSE prop_name END FROM fst_property_table WHERE id = prop_id) [10252]," & _
                              "p.prop_number [10253]," & _
                              "s.name [10254]," & _
                              "county_name [10255]," & _
                              "municipal_name [10256]," & _
                              "prop_numof_trees [10257]," & _
                              "prop_areal [10258]," & _
                              "prop_volume_m3sk [10259]," & _
                              "prop_wood_volume [10260]," & _
                              "prop_numof_stands [10261]," & _
                              "CASE WHEN prop_type_of_action = 0 THEN 'Ej vald' WHEN prop_type_of_action = 1 THEN 'F�rs�ljning' WHEN prop_type_of_action = 2 THEN 'Egen regi' END [10262]," & _
                              "ROUND(prop_wood_value, 0) [10263]," & _
                              "ROUND(prop_rotpost_value, 0) [10264]," & _
                              "ROUND(prop_cost_value, 0) [10265]," & _
                              "ROUND(prop_land_value, 0) [10266]," & _
                              "ROUND(prop_early_cut_value, 0) [10267]," & _
                              "ROUND(prop_storm_dry_value, 0) [10268]," & _
                              "ROUND(prop_randtrees_value, 0) [10269]," & _
                              "ROUND(prop_voluntary_deal_value, 0) [10270]," & _
                              "ROUND(prop_high_cost_value, 0) [10271]," & _
                              "ROUND(prop_other_comp_value, 0) [10272]," & _
                              "ROUND(prop_grot_value, 0) [10273]," & _
                              "ROUND(prop_grot_value, 0) [10274]," & _
                              "ROUND(prop_grot_cost, 0) [10275] " & _
                              "FROM elv_properties_table e " & _
                              "INNER JOIN fst_property_table p ON p.id = e.prop_id " & _
                              "INNER JOIN elv_property_status_table s ON s.id = e.prop_status"
            cmd.ExecuteNonQuery()
            SetViewVersion(ViewNameObjectData, _viewVersionObjectData)
        End If

        'Check if Estimate is installed
        If ExistsSql("SELECT 1 FROM sysobjects WHERE name LIKE 'esti_trakt_dcls_trees_table' AND xtype = 'U'") Then
            'Estimate distribution of species per stand and per plot (we need to recreate this view to make sure only used species are included)
            'This could also be solved using a cross-tab stored procedure but that cannot be called from a view.
            'Also, using the PIVOT statement would eliminate compability with SQL Server 2000 and required all columns (species) to be explicitly written in the query.
            Dim pivot As String = vbNullString
            cmd.CommandText = "SELECT DISTINCT tdcls_spc_name, tdcls_spc_id FROM esti_trakt_dcls_trees_table ORDER BY tdcls_spc_id"
            Dim reader As OleDbDataReader = cmd.ExecuteReader()
            Do While reader.Read()
                If Not String.IsNullOrEmpty(reader.GetString(0)) Then pivot += "ROUND(SUM(CASE WHEN tdcls_spc_name = '" & reader.GetString(0) & "' THEN part END), 2) [" & reader.GetString(0) & "],"
            Loop
            reader.Close()

            If pivot <> vbNullString Then
                'Per stand
                cmd.CommandText = "IF EXISTS (SELECT 1 FROM sysobjects WHERE name LIKE 'gis_speciesperstand' AND xtype = 'V') DROP VIEW dbo.gis_speciesperstand"
                cmd.ExecuteNonQuery()

                cmd.CommandText = "CREATE VIEW dbo.gis_speciesperstand AS " & _
                                  "SELECT " & _
                                  "tdcls_trakt_id [key1]," & _
                                  "trakt_name [10276]," & _
                                  pivot & _
                                  "SUM(m3sk) [10277] " & _
                                  "FROM " & _
                                  "(" & _
                                  "SELECT " & _
                                  "tdcls_trakt_id," & _
                                  "tdcls_spc_name," & _
                                  "CASE WHEN (SELECT SUM(tdcls_m3sk) FROM esti_trakt_dcls_trees_table ds WHERE ds.tdcls_trakt_id = d.tdcls_trakt_id) <> 0 THEN SUM(tdcls_m3sk) / (SELECT SUM(tdcls_m3sk) FROM esti_trakt_dcls_trees_table ds WHERE ds.tdcls_trakt_id = d.tdcls_trakt_id) ELSE 0 END part," & _
                                  "SUM(tdcls_m3sk) m3sk " & _
                                  "FROM esti_trakt_dcls_trees_table d " & _
                                  "GROUP BY tdcls_trakt_id, tdcls_spc_name" & _
                                  ") dt " & _
                                  "INNER JOIN esti_trakt_table t ON t.trakt_id = dt.tdcls_trakt_id " & _
                                  "GROUP BY tdcls_trakt_id, trakt_name"
                cmd.ExecuteNonQuery()

                'Per plot
                cmd.CommandText = "IF EXISTS (SELECT 1 FROM sysobjects WHERE name LIKE 'gis_speciesperplot' AND xtype = 'V') DROP VIEW dbo.gis_speciesperplot"
                cmd.ExecuteNonQuery()

                cmd.CommandText = "CREATE VIEW dbo.gis_speciesperplot AS " & _
                                  "SELECT " & _
                                  "CAST(tdcls_trakt_id AS varchar) + '/' + CAST(tdcls_plot_id AS varchar) [key1]," & _
                                  "trakt_name [10278]," & _
                                  "tdcls_plot_id [10279]," & _
                                  pivot & _
                                  "SUM(m3sk) [10280] " & _
                                  "FROM " & _
                                  "(" & _
                                  "SELECT " & _
                                  "tdcls_trakt_id," & _
                                  "tdcls_plot_id," & _
                                  "tdcls_spc_name," & _
                                  "CASE WHEN (SELECT SUM(tdcls_m3sk) FROM esti_trakt_dcls_trees_table ds WHERE ds.tdcls_trakt_id = d.tdcls_trakt_id) <> 0 THEN SUM(tdcls_m3sk) / (SELECT SUM(tdcls_m3sk) FROM esti_trakt_dcls_trees_table ds WHERE ds.tdcls_trakt_id = d.tdcls_trakt_id) ELSE 0 END part," & _
                                  "SUM(tdcls_m3sk) m3sk " & _
                                  "FROM esti_trakt_dcls_trees_table d " & _
                                  "GROUP BY tdcls_plot_id, tdcls_trakt_id, tdcls_spc_name" & _
                                  ") dt " & _
                                  "INNER JOIN esti_trakt_table t ON t.trakt_id = dt.tdcls_trakt_id " & _
                                  "GROUP BY tdcls_plot_id, tdcls_trakt_id, trakt_name"
                cmd.ExecuteNonQuery()
            End If


            'Forest sample tree data
            If IsViewCreationRequired(ViewNameSampleTreeData, _viewVersionSampletreedata, "elv_cruise_table") Then
                'Create view
                DropView(ViewNameSampleTreeData)
                cmd.CommandText = "CREATE VIEW dbo.gis_sampletreedata AS " & _
                              "SELECT " & _
                              "CAST(ttree_trakt_id AS varchar) + '/' + CAST(ttree_id AS varchar) [key1], " & _
                              "t.trakt_name [10281], " & _
                              "t.trakt_wside [10282], " & _
                              "sp.spc_name [10283], " & _
                              "tr.ttree_dbh [10284], " & _
                              "ROUND(tr.ttree_hgt,0) [10285], " & _
                              "ROUND(tr.ttree_m3sk,3) [Volym (m3sk)], " & _
                              "tr.ttree_age [10286], " & _
                              "tr.ttree_bonitet [10287], " & _
                              "tr.ttree_tree_type [Typ], " & _
                              "[Typ namn] = " & _
                                "CASE " & _
                                    "WHEN tr.ttree_tree_type = 0 THEN 'Provtr�d (Uttag)' " & _
                                    "WHEN tr.ttree_tree_type = 1 THEN 'Kanttr�d (Provtr�d)' " & _
                                    "WHEN tr.ttree_tree_type = 2 THEN 'Kanttr�d (Ber�knad h�jd)' " & _
                                    "WHEN tr.ttree_tree_type = 3 THEN 'Provtr�d (Kvarl�mnat)' " & _
                                    "WHEN tr.ttree_tree_type = 4 THEN 'Provtr�d (Extra inl�st)' " & _
                                    "WHEN tr.ttree_tree_type = 5 THEN 'Kanttr�d l�mnas (Provtr�d)' " & _
                                    "WHEN tr.ttree_tree_type = 6 THEN 'Kanttr�d l�mnas (Ber�knad h�jd)' " & _
                                    "WHEN tr.ttree_tree_type = 7 THEN 'Pos.tr�d (Ber�knad h�jd)' " & _
                                    "WHEN tr.ttree_tree_type = 8 THEN 'Tr�d i gatan l�mnas (Provtr�d)' " & _
                                    "WHEN tr.ttree_tree_type = 9 THEN 'Tr�d i gatan l�mnas (Ber�knad h�jd)' " & _
                                    "ELSE '' " & _
                                "End, " & _
                              "tr.ttree_distcable AS [Fasavst�nd], " & _
                              "tr.ttree_topcut AS [Toppningsv�rde], " & _
                              "tr.ttree_category AS Kategori, " & _
                              "aa.category AS Kategorinamn " & _
                              "FROM esti_trakt_sample_trees_table tr " & _
                              "INNER JOIN fst_species_table sp ON sp.spc_id = tr.ttree_spc_id " & _
                              "LEFT JOIN esti_trakt_table t ON t.trakt_id = tr.ttree_trakt_id " & _
                              "LEFT OUTER JOIN esti_sample_trees_category_table AS aa ON aa.id = tr.ttree_category"
                cmd.ExecuteNonQuery()
                SetViewVersion(ViewNameSampleTreeData, _viewVersionSampletreedata)
            End If
        End If

        'TCruise plot data
        If IsViewCreationRequired(ViewNameTcPlotData, _viewVersionTcPlotData, "tc_joinedplotdata") Then
            'Create view
            DropView(ViewNameTcPlotData)
            cmd.CommandText = "CREATE VIEW dbo.gis_tcplotdata AS " & _
                              "SELECT " & _
                              "CAST(j.fileindex AS varchar) + '/' + CAST(j.plotindex AS varchar) key1," & _
                              "TractId Stand," & _
                              "j.PlotId PlotId," & _
                              "ROUND(SumOfTPA, 2) SumOfTPA," & _
                              "ROUND(SumOfBA, 2) SumOfBA," & _
                              "ROUND(SumOfPW_GreenTons, 3) SumOfPW_GreenTons," & _
                              "ROUND(SumOfSW_GreenTons, 3) SumOfSW_GreenTons," & _
                              "ROUND(SumOfPW_Dollars, 2) SumOfPW_Dollars," & _
                              "ROUND(SumOfSW_Dollars, 2) SumOfSW_Dollars," & _
                              "ROUND(SumOf_Doyle, 2) SumOf_Doyle," & _
                              "ROUND(SumOf_Scribner, 2) SumOf_Scribner," & _
                              "ROUND(SumOf_IntQuarter, 2) SumOf_IntQuarter," & _
                              "j.CR01 CR01," & _
                              "j.CR02 CR02," & _
                              "j.CR03 CR03," & _
                              "j.CR04 CR04," & _
                              "j.CR05 CR05," & _
                              "j.CR06 CR06," & _
                              "j.CR07 CR07," & _
                              "j.CR08 CR08," & _
                              "j.CR09 CR09," & _
                              "j.CR10 CR10," & _
                              "j.CR11 CR11," & _
                              "j.CR12 CR12," & _
                              "j.CR13 CR13," & _
                              "j.CR14 CR14," & _
                              "j.CR15 CR15," & _
                              "j.CR16 CR16," & _
                              "j.CR17 CR17," & _
                              "j.CR18 CR18," & _
                              "j.CR19 CR19," & _
                              "j.CR20 CR20," & _
                              "j.CR21 CR21," & _
                              "j.CR22 CR22," & _
                              "j.CR23 CR23," & _
                              "j.CR24 CR24," & _
                              "j.CR25 CR25," & _
                              "j.CR26 CR26," & _
                              "j.CR27 CR27," & _
                              "j.CR28 CR28," & _
                              "j.CR29 CR29," & _
                              "j.CR30 CR30," & _
                              "j.CR31 CR31," & _
                              "j.CR32 CR32," & _
                              "j.CR33 CR33," & _
                              "j.CR34 CR34," & _
                              "j.CR35 CR35," & _
                              "j.CR36 CR36," & _
                              "j.CR37 CR37," & _
                              "j.CR38 CR38," & _
                              "j.CR39 CR39," & _
                              "j.CR40 CR40," & _
                              "j.CR41 CR41," & _
                              "j.CR42 CR42," & _
                              "j.CR43 CR43," & _
                              "j.CR44 CR44," & _
                              "j.CR45 CR45," & _
                              "j.CR46 CR46," & _
                              "j.CR47 CR47," & _
                              "j.CR48 CR48," & _
                              "j.CR49 CR49," & _
                              "j.CR50 CR50 " & _
                              "FROM tc_joinedplotdata j " & _
                              "INNER JOIN tc_stand s ON s.fileindex = j.fileindex"
            cmd.ExecuteNonQuery()
            SetViewVersion(ViewNameTcPlotData, _viewVersionTcPlotData)
        End If

        'Add view for TCruise stand data J�
        'TCruise Stand data
        If IsViewCreationRequired(ViewNameTcStandData, _viewVersionTcStandData, "tc_joinedplotdata") Then
            'Create view
            DropView(ViewNameTcStandData)
            cmd.CommandText = "CREATE VIEW dbo.gis_tcstanddata AS " & _
                            "SELECT dbo.tc_Stand.FileIndex AS key1, dbo.tc_Stand.TractID, dbo.tc_Stand.TractAcres, dbo.tc_Stand.CruiseType, dbo.tc_Stand.CruiseDate, dbo.tc_Stand.TractLoc, " & _
                            "dbo.tc_Stand.TractOwner, dbo.tc_Stand.Cruiser, dbo.tc_Stand.OtherInfo, ROUND(SUM(dbo.tc_JoinedPlotData.SumOf_Doyle), 2) AS Doyle, " & _
                            "ROUND(SUM(dbo.tc_JoinedPlotData.SumOfPW_GreenTons), 2) AS [PW GreenTons], ROUND(SUM(dbo.tc_JoinedPlotData.SumOfSW_GreenTons), 2) " & _
                            "AS [SW GreenTons], ROUND(SUM(dbo.tc_JoinedPlotData.SumOfPW_Dollars), 2) AS [PW $], ROUND(SUM(dbo.tc_JoinedPlotData.SumOfSW_Dollars), 2) AS [SW $], " & _
                            "ROUND(SUM(dbo.tc_JoinedPlotData.SumOf_Scribner), 2) AS Scribner, ROUND(SUM(dbo.tc_JoinedPlotData.SumOf_IntQuarter), 2) AS IntQuarter, " & _
                            "COUNT(dbo.tc_JoinedPlotData.PlotIndex) AS [Number Of Plots], ROUND(AVG(dbo.tc_JoinedPlotData.SumOfBA), 2) AS [Avg BA] " & _
                            "FROM dbo.tc_JoinedPlotData INNER JOIN " & _
                            "dbo.tc_Stand ON dbo.tc_JoinedPlotData.FileIndex = dbo.tc_Stand.FileIndex " & _
                            "GROUP BY dbo.tc_Stand.TractID, dbo.tc_Stand.TractAcres, dbo.tc_Stand.FileIndex, dbo.tc_Stand.CruiseType, dbo.tc_Stand.CruiseDate, dbo.tc_Stand.TractLoc, " & _
                            "dbo.tc_Stand.TractOwner, dbo.tc_Stand.Cruiser, dbo.tc_Stand.OtherInfo "
            cmd.ExecuteNonQuery()
            SetViewVersion(ViewNameTcStandData, _viewVersionTcStandData)
        End If



        'Check for installed modules
        'Forest
        If ExistsSql("SELECT 1 FROM sysobjects WHERE name LIKE 'fst_property_table' AND xtype='U'") Then ForestInstalled = True
        'Estimate
        If ExistsSql("SELECT 1 FROM sysobjects WHERE name LIKE 'esti_trakt_table' AND xtype='U'") Then EstimateInstalled = True
        'LandValue
        If ExistsSql("SELECT 1 FROM sysobjects WHERE name LIKE 'elv_cruise_table' AND xtype='U'") Then LandValueInstalled = True
        'TCruise
        If ExistsSql("SELECT 1 FROM sysobjects WHERE name LIKE 'tc_stand' AND xtype='U'") Then TCruiseInstalled = True
    End Sub

    Private Shared Function ExistsSql(ByVal sql As String)
        Dim cmd As New OleDbCommand(sql, Conn)
        Dim reader As OleDbDataReader = cmd.ExecuteReader()
        Dim ret As Boolean = False
        If reader.HasRows Then ret = True
        Return ret
    End Function

    Private Shared Function IsViewCreationRequired(ByVal viewName As String, ByVal viewVersion As Integer, ByVal requiredTable As String) As Boolean
        Dim createView As Boolean = False
        Dim sql As String

        'Check if required table exist
        sql = "SELECT 1 FROM sysobjects WHERE name LIKE '" & requiredTable & "' AND xtype = 'U'"
        If ExistsSql(sql) Then
            'Check if specified view exist
            sql = "SELECT 1 FROM sysobjects WHERE name LIKE '" & viewName & "' AND xtype = 'V'"
            If ExistsSql(sql) Then
                'Check version of view
                Dim cmd As New OleDbCommand("", Conn)
                Dim reader As OleDbDataReader
                sql = "SELECT value FROM ::fn_listextendedproperty(NULL, 'user', 'dbo', 'view', '" & viewName & "', NULL, NULL) WHERE name = 'version'"
                cmd.CommandText = sql
                reader = cmd.ExecuteReader()
                If reader.Read() Then
                    If reader.GetInt32(0) < viewVersion Then
                        'Existing view is older
                        createView = True
                    End If
                Else
                    'Undefined version
                    createView = True
                End If
            Else
                'View does not exist
                createView = True
            End If
        End If

        Return createView
    End Function

    Private Shared Function IsThereUnusedLayers() As Boolean
        'Query db for layers and check for layers that aren't loaded
        Dim ret As Boolean = False
        Dim cmd As New OleDbCommand("", GisControlView.Conn)
        Dim reader As OleDbDataReader

        'Native
        cmd.CommandText = "IF EXISTS (SELECT 1 FROM sysobjects WHERE name LIKE N'ttkGISLayerSQL' AND xtype='U') SELECT RIGHT(name,LEN(name)-4) FROM ttkGISLayerSQL"
        reader = cmd.ExecuteReader()
        Do While reader.Read()
            If GisControlView.Instance.GisCtrl.Get(reader.GetString(0)) Is Nothing Then
                ret = True
                Exit Do
            End If
        Loop
        reader.Close()

        'PixelStore2
        cmd.CommandText = "IF EXISTS (SELECT 1 FROM sysobjects WHERE name LIKE N'ttkGISPixelStore2' AND xtype='U') SELECT RIGHT(name,LEN(name)-4) FROM ttkGISPixelStore2"
        reader = cmd.ExecuteReader()
        Do While reader.Read()
            If GisControlView.Instance.GisCtrl.Get(reader.GetString(0)) Is Nothing Then
                ret = True
                Exit Do
            End If
        Loop
        reader.Close()

        Return ret
    End Function

    Private Shared Sub DropView(ByVal name As String)
        'Drop view if exist
        Dim cmd As New OleDbCommand("IF EXISTS(SELECT 1 FROM sysobjects WHERE name LIKE '" & name & "' AND xtype = 'V') EXEC('DROP VIEW " & name & "')", Conn)
        cmd.ExecuteNonQuery()
    End Sub

    Private Shared Sub SetViewVersion(ByVal name As String, ByVal version As Integer)
        'Use extended properties to store version
        Dim cmd As New OleDbCommand("sp_addextendedproperty 'version', " & version & ", 'user', 'dbo', 'view', '" & name & "', NULL, NULL", Conn)
        cmd.ExecuteNonQuery()
    End Sub
#End Region

    Private Sub ExportViewToImageWithProj()
        Dim dlg As New SaveFileDialog()

        'Show file chooser dialog
        dlg.Filter = "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" & _
                     "PNG (*.png)|*.png|" & _
                     "TIF (*.tif)|*.tif|" & _
                     "BMP (*.bmp)|*.bmp"
        If dlg.ShowDialog() = DialogResult.OK Then
            'Export view to image
            GisCtrl.ExportToImage(dlg.FileName, GisCtrl.VisibleExtent, GisCtrl.Width, GisCtrl.Height, 80, 0, 96)

            'Rename the PRJ file from filename.extension.prj to filename.prj
            If File.Exists(dlg.FileName + ".prj") Then
                Dim destFilename As String = Mid(dlg.FileName, 1, dlg.FileName.LastIndexOf("."c)) & ".prj"

                'Delete the old file if it exists
                If File.Exists(destFilename) Then
                    File.Delete(destFilename)
                End If

                'Rename (move) the newly created PRJ-file
                File.Move(dlg.FileName + ".prj", destFilename)
            End If
        End If
    End Sub

    Private Sub ExportViewToReport()
        _printExtent = GisCtrl.VisibleExtent 'We need to cache this value as visible extent won't be the same when data is sent to the printer
        Dim dlg As New GisPrintPreviewDialog
        dlg.ShowDialog()
    End Sub

    Private Shared Sub ExportViewToSnapshot()
        Dim dlg As New GisPrintPreviewDialog(GisPrintPreviewDialog.Action.Snapshot)
        dlg.ShowDialog()
    End Sub

    Private Shared Sub ExportViewToImageWithScale()
        Dim dlg As New GisPrintPreviewDialog(GisPrintPreviewDialog.Action.File)
        dlg.ShowDialog()
    End Sub

    Private Sub UpdateLengthArea(ByRef shp As TGIS_Shape)
        Dim area, length As Double
        Const epsgUS As Integer = 904202

        'Update measurement
        If shp IsNot Nothing Then
            'Use area and length based on ellipsoid if coordinate system is set
            If shp.Layer.CS.EPSG > 0 Then
                area = shp.AreaCS() 'Convert to scale units
                length = shp.LengthCS()
            Else
                area = shp.Area()
                length = shp.Length()
            End If

            'Length
            txtLength.Text = ControlScale.Units.AsLinear(length, True)

            'Area
            If _areaUnits.EPSG = epsgUS Then
                'US formatting
                txtArea.Text = _areaUnits.AsAreal(area, True, "sq. %s")
            Else
                'Metric or other
                txtArea.Text = _areaUnits.AsAreal(area, True, "%s�")
            End If
        Else
            txtLength.Text = vbNullString
            txtArea.Text = vbNullString
        End If
    End Sub

    Private Sub UpdateScalePane()
        _scalePane.Text = LangStr(_strIdScale) & " " & GisCtrl.ScaleAsText
    End Sub

    Private Shared Sub WriteTCGPSString(ByRef s As BinaryWriter, ByRef value As String)
        'Write string length including null char prior to string (2 byte WORD Big Endian)
        Dim len As UShort
        len = CType(value.Length + 1, UShort)
        s.Write(CType((len >> 0) And &HFF, Byte)) 'LSB
        s.Write(CType((len >> 8) And &HFF, Byte)) 'MSB
        s.Write(value.ToCharArray()) 'ToCharArray will remove leading string length and null char
        s.Write(Chr(0)) 'Add ending null char
    End Sub

    Private Shared Sub WriteTCGPSInt32(ByRef s As BinaryWriter, ByRef value As Int32)
        'Write Int32 with Big Endian encoding
        s.Write(CType((value >> 0) And &HFF, Byte)) 'LSB
        s.Write(CType((value >> 8) And &HFF, Byte))
        s.Write(CType((value >> 16) And &HFF, Byte))
        s.Write(CType((value >> 24) And &HFF, Byte)) 'MSB
    End Sub

    Private Shared Sub WriteTCGPSDouble(ByRef s As BinaryWriter, ByRef value As Double)
        'Write Double with Big Endian encoding
        Dim i As Integer
        Dim bytes As Byte() = BitConverter.GetBytes(value)
        For i = 0 To 7
            s.Write(bytes(i)) 'Start with LSB, end with MSB
        Next
    End Sub

    Private Sub AddNewShape(ByVal ll As TGIS_LayerVector, ByVal pmap As TGIS_Point)
        Dim shp As TGIS_Shape

        'Only add to visible layers
        If ll.Active Then
            'Create new shape if not exist
            shp = ll.GetShape(_customShapeUid)
            If shp Is Nothing Then
                If _newShapeType = ShapeType.Point Then
                    shp = ll.CreateShape(TGIS_ShapeType.gisShapeTypePoint)
                ElseIf _newShapeType = ShapeType.MultiPoint Then
                    shp = ll.CreateShape(TGIS_ShapeType.gisShapeTypeMultiPoint)
                ElseIf _newShapeType = ShapeType.Polygon Then
                    shp = ll.CreateShape(TGIS_ShapeType.gisShapeTypePolygon)
                ElseIf _newShapeType = ShapeType.Line Then
                    shp = ll.CreateShape(TGIS_ShapeType.gisShapeTypeArc)
                End If

                If shp IsNot Nothing Then
                    'Add initial point, begin editing
                    shp.AddPart()
                    shp.AddPoint(TGIS_Utils.GisPoint(pmap.X, pmap.Y))
                    _customShapeUid = shp.Uid
                    EditShape(shp, 0)
                End If
            End If
        End If
    End Sub

    Private Sub LocateAndEditShape(ByVal ll As TGIS_LayerVector, ByVal pmap As TGIS_Point)
        Dim dist As Double, part As Integer, proj As TGIS_Point
        Dim shp As TGIS_Shape

        'Look for a nearby shape within layer
        shp = ll.LocateEx(pmap, _selectTolerance / GisCtrl.Zoom, -1, dist, part, proj)
        If ll.Active AndAlso shp IsNot Nothing Then
            shp.MakeEditable()
            EditShape(shp, part)
            SetGisMode(GisMode.EditStep2)
        End If
    End Sub

    Private Sub LocateAndRemoveShape(ByVal ll As TGIS_LayerVector, ByVal pmap As TGIS_Point)
        Dim i As Integer
        Dim shp As TGIS_Shape

        'Located nearest shape within active layer, no remove on invisible layers
        shp = ll.Locate(pmap, _selectTolerance / GisCtrl.Zoom, True)
        If ll.Active AndAlso shp IsNot Nothing Then
            'Mark selected shape
            shp = shp.MakeEditable()
            shp.IsSelected = True
            shp.Invalidate()

            'Let user confirm
            If MsgBox(LangStr(_strIdConfirmRemoveShape), MsgBoxStyle.Exclamation Or MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                'Remove any thumbnail image for stand/object layers
                RemoveRelationImage(shp, GetLayerRelationType(ll.Name))

                '#3724: Remove coordinates
                RemoveRelationCoord(shp, GetLayerRelationType(ll.Name))

                If _cachedMode Then
                    'Remove shape record from data viewer
                    For i = 0 To ReportData.Rows.Count - 1
                        If ReportData.Rows(i).Record.Item(0).Value = shp.Uid Then
                            ReportData.RemoveRowEx(ReportData.Rows(i))
                            ReportData.Update()
                            _shapeCountPane.Text = ReportData.Rows.Count & LangStr(_strIdRecords)
                            Exit For
                        End If
                    Next
                Else
                    'Remove shape from virtual list
                    i = _shapeList.FindIndex(Function(p) p = shp.Uid)
                    If i >= 0 Then
                        _shapeList.RemoveAt(i)
                        ReportData.SetVirtualMode(_shapeList.Count)
                        ReportData.Update()
                        _shapeCountPane.Text = _shapeList.Count & LangStr(_strIdRecords)
                    End If
                End If

                'Remove the shape
                ll.Delete(shp.Uid)
                ll.SaveData()
                GisCtrl.Update()

                'Update field data viewer if layer is empty
                If ll.Items.Count = 0 Then
                    ListFieldData()
                End If
            Else
                shp.IsSelected = False
                shp.Invalidate()
            End If
        End If
    End Sub

    Private Sub NavigateToExtent(ByVal ext As TGIS_Extent)
        Dim dx As Double, dy As Double

        If ext.XMin = ext.XMax AndAlso ext.YMin = ext.YMax Then
            'Center viewer on point, keep zoom level
            dx = GisCtrl.VisibleExtent.XMax - GisCtrl.VisibleExtent.XMin
            dy = GisCtrl.VisibleExtent.YMax - GisCtrl.VisibleExtent.YMin

            GisCtrl.VisibleExtent = New TGIS_Extent(ext.XMin - dx / 2.0, ext.YMin - dy / 2.0, ext.XMin + dx / 2.0, ext.YMin + dy / 2.0)
        Else
            GisCtrl.VisibleExtent = ext
        End If
    End Sub

    Private Sub BeginMeasure(ByVal pmap As TGIS_Point)
        Dim ll As TGIS_LayerVector
        Dim shp As TGIS_Shape

        'Create temp layer if it doesn't exist
        ll = GetPreviewLayer()
        ll.Params.Area.Pattern = TGIS_BrushStyle.gisBsClear

        'Create temp shape if it doesn't exist
        shp = ll.GetShape(_customShapeUid)
        If shp Is Nothing Then
            If _newShapeType = ShapeType.Polygon Then
                shp = ll.CreateShape(TGIS_ShapeType.gisShapeTypePolygon)
            ElseIf _newShapeType = ShapeType.Line Then
                shp = ll.CreateShape(TGIS_ShapeType.gisShapeTypeArc)
            End If

            If shp IsNot Nothing Then
                'Add initial point, begin edit of this shape
                shp.AddPart()
                shp.AddPoint(TGIS_Utils.GisPoint(pmap.X, pmap.Y))
                _customShapeUid = shp.Uid
                EditShape(shp, 0)
            End If
        End If
    End Sub

    Private Sub DoCreateElvObject(ByVal ll As TGIS_LayerVector, ByVal pmap As TGIS_Point)
        Dim shp As TGIS_Shape

        'Locate nearest shape, make sure layer is visible
        shp = ll.Locate(pmap, _selectTolerance / GisCtrl.Zoom, True)
        If ll.Active AndAlso shp IsNot Nothing Then
            If shp.ShapeType = TGIS_ShapeType.gisShapeTypeArc Or shp.ShapeType = TGIS_ShapeType.gisShapeTypePolygon Then
                'Show new object dialog
                Dim dlg As New ObjectDialog(ll.Name, shp.Uid)
                If dlg.ShowDialog() = DialogResult.OK AndAlso dlg.PropertyIdList.Count > 0 Then
                    _objectShapePropertyList = dlg.ShapePropertyList 'Store shape-property ids so we can set up the relations later
                    _objectLayerName = dlg.DestinationLayer
                    'Raise new object event (include id collection)
                    Dim ea As New ObjectEventArgs(dlg.PropertyIdList)
                    RaiseEvent CreateElvObject(Me, ea)
                End If
                ListFieldData()
                GisCtrl.Update()
                SetGisMode(GisMode.Drag)
            End If
        End If
    End Sub

    Private Sub DoImportProperties(ByVal ll As TGIS_LayerVector, ByVal pmap As TGIS_Point)
        Dim shp As TGIS_Shape

        'Locate nearest shape, make sure layer is visible
        shp = ll.Locate(pmap, _selectTolerance / GisCtrl.Zoom, True)
        If ll.Active AndAlso shp IsNot Nothing Then
            Dim dlg As New ImportPropertiesDialog(ll.Name, shp.Uid)
            dlg.ShowDialog()
            SetGisMode(GisMode.Drag)
        End If
    End Sub

    Private Sub DoSumAttributes(ByVal ll As TGIS_LayerVector, ByVal pmap As TGIS_Point)
        Dim shp As TGIS_Shape

        'Locate a nearby shape and make sure layer is visible
        shp = ll.Locate(pmap, _selectTolerance / GisCtrl.Zoom, True)
        If ll.Active AndAlso shp IsNot Nothing AndAlso shp.ShapeType = TGIS_ShapeType.gisShapeTypePolygon Then
            'Let user choose which layers to sum attributes from
            Dim dlg As New LayerMultiChoiceDialog(LangStr(_strIdSumFromLayer), ll.Name)
            If dlg.ShowDialog() = DialogResult.OK Then
                SumLayersToPolygon(dlg.SelectedLayers, shp)
            End If
            SetGisMode(GisMode.Drag)
        End If
    End Sub

    Private Sub DoCreatePlots(ByVal ll As TGIS_LayerVector, ByVal pmap As TGIS_Point)
        Dim shp As TGIS_Shape
        Dim objectlist As New List(Of ValueDescriptionPair)
        Dim objectname As String

        'Locate a nearby shape and make sure layer is visible
        shp = ll.Locate(pmap, _selectTolerance / GisCtrl.Zoom, True)
        If ll.Active AndAlso shp IsNot Nothing AndAlso shp.ShapeType = TGIS_ShapeType.gisShapeTypePolygon Then
            'Get relation name
            objectname = GetShapeRelationName(shp)
            If objectname <> vbNullString Then objectlist.Add(New ValueDescriptionPair(shp.Uid, objectname))

            Dim dlg As New PlotDialog(ll, shp.Uid, objectlist)
            dlg.ShowDialog()
            SetGisMode(GisMode.Drag)
        End If
    End Sub

    Private Sub DoGenerateRouteStep1(ByVal ll As TGIS_LayerVector, ByVal pmap As TGIS_Point, ByVal useMiddleLine As Boolean)
        Dim shp As TGIS_Shape, shpTemp As TGIS_Shape
        Dim tpl As New TGIS_Topology
        Dim isLeft As Boolean

        'Locate a nearby shape and make sure layer is visible
        shp = ll.Locate(pmap, _selectTolerance / GisCtrl.Zoom, True)
        If ll.Active AndAlso shp IsNot Nothing Then
            If useMiddleLine AndAlso shp.ShapeType = TGIS_ShapeType.gisShapeTypeArc Then
                'Highlight selected line
                If _routeMiddleLine IsNot Nothing Then _routeMiddleLine.MakeEditable().IsSelected = False
                shp.MakeEditable().IsSelected = True
                _routeMiddleLine = shp
                GisCtrl.Update()
            ElseIf shp.ShapeType = TGIS_ShapeType.gisShapeTypePolygon AndAlso ((useMiddleLine AndAlso _routeMiddleLine IsNot Nothing) OrElse Not useMiddleLine) Then
                'Let user choose a plot layer
                Dim dlg As New LayerChoiceDialog(RelationType.Undefined, LangStr(_strIdPlotLayerForRoute), True, TGIS_ShapeType.gisShapeTypeNull, False)
                If dlg.ShowDialog() = DialogResult.OK Then
                    _plotLayerForRoute = dlg.LayerOne

                    ll = GisCtrl.Get(dlg.LayerOne)
                    If ll IsNot Nothing Then
                        'First unmark any selected shapes in the layer
                        ll.DeselectAll()

                        If _routeMiddleLine Is Nothing Then
                            'Mark intersecting plots
                            shpTemp = ll.FindFirst()
                            Do Until shpTemp Is Nothing
                                'Check only points
                                If (shpTemp.ShapeType = TGIS_ShapeType.gisShapeTypePoint OrElse shpTemp.ShapeType = TGIS_ShapeType.gisShapeTypeMultiPoint) AndAlso tpl.Intersect(shp, shpTemp) Then
                                    shpTemp.MakeEditable().IsSelected = True
                                End If
                                shpTemp = ll.FindNext()
                            Loop
                        Else
                            'Use only intersecting part of line
                            _routeMiddleLine.MakeEditable().IsSelected = False
                            _routeMiddleLine = tpl.Intersection(shp, _routeMiddleLine)

                            'Mark intersecting points, separate by side of line
                            shpTemp = ll.FindFirst()
                            Do Until shpTemp Is Nothing
                                If shpTemp.ShapeType = TGIS_ShapeType.gisShapeTypePoint AndAlso tpl.Intersect(shp, shpTemp) Then
                                    'Check if point belong to any line segment
                                    If GetClosestLineSegment(ll.CS, shpTemp.GetPoint(0, 0), _routeMiddleLine, isLeft) >= 0 Then
                                        shpTemp.MakeEditable().IsSelected = True
                                        shpTemp.MakeEditable().Tag = isLeft
                                    End If
                                End If
                                shpTemp = ll.FindNext()
                            Loop
                        End If

                        'Check if any intersecting plots were found, otherwise abort route generation procedure
                        If ll.GetSelectedCount() > 0 Then
                            GisCtrl.Update()
                            SetGisMode(GisMode.GenerateRouteStep2)
                        Else
                            MsgBox(LangStr(_strIdNoPlotsWithinBoundary), MsgBoxStyle.Exclamation)
                            SetGisMode(GisMode.Drag)
                        End If
                    End If
                Else
                    SetGisMode(GisMode.Drag)
                End If
            End If
        End If
    End Sub

    Private Sub DoGenerateRouteStep2(ByVal pmap As TGIS_Point)
        Dim shp As TGIS_Shape
        Dim ll As TGIS_LayerVector = GisCtrl.Get(_plotLayerForRoute)

        'Locate a nearby shape
        shp = ll.Locate(pmap, _selectTolerance / GisCtrl.Zoom, True)
        If shp IsNot Nothing AndAlso shp.IsSelected AndAlso (shp.ShapeType = TGIS_ShapeType.gisShapeTypePoint OrElse shp.ShapeType = TGIS_ShapeType.gisShapeTypeMultiPoint) Then
            If _routeMiddleLine Is Nothing Then
                DoGenerateRoutePolygon(pmap, shp, ll)
            Else
                DoGenerateRouteArc(shp, ll)
            End If

            SetGisMode(GisMode.Drag)
        End If
    End Sub

    Private Sub DoGenerateRoutePolygon(ByVal pmap As TGIS_Point, ByVal shp As TGIS_Shape, ByVal ll As TGIS_LayerVector)
        Dim i As Integer, startIdx As Integer
        Dim d As Double, dmin As Double = Double.MaxValue
        Dim lDest As TGIS_LayerVector
        Dim shpPlot As TGIS_Shape, shpPath As TGIS_Shape
        Dim pt As TGIS_Point
        Dim pts As New List(Of TGIS_Point)
        Dim rc As List(Of IntIntPair) = Nothing

        'Check if row/column field exist
        If _
        ll.FindField(FieldNamePlotRow) >= 0 AndAlso ll.FieldInfo(ll.FindField(FieldNamePlotRow)).FieldType = TGIS_FieldType.gisFieldTypeNumber AndAlso _
        ll.FindField(FieldNamePlotColumn) >= 0 AndAlso ll.FieldInfo(ll.FindField(FieldNamePlotColumn)).FieldType = TGIS_FieldType.gisFieldTypeNumber Then
            rc = New List(Of IntIntPair)
        End If

        'Loop through marked shapes
        shpPlot = ll.FindFirst(ll.ProjectedExtent, "GIS_SELECTED=True")
        Do Until shpPlot Is Nothing
            'Loop through all points in this shape
            For i = 0 To shpPlot.GetNumPoints() - 1
                If shpPlot.Uid = shp.Uid Then
                    'Find selected point in shape (for multipoint shapes)
                    d = TGIS_Utils.GisPoint2Point(pmap, shpPlot.GetPoint(0, i))
                    If d < dmin Then
                        dmin = d
                        startIdx = pts.Count 'Start point index
                    End If
                End If

                If rc IsNot Nothing Then rc.Add(New IntIntPair(shpPlot.GetField(FieldNamePlotRow), shpPlot.GetField(FieldNamePlotColumn))) 'Cache row/column
                pts.Add(shpPlot.GetPoint(0, i))
            Next
            shpPlot = ll.FindNext()
        Loop

        'We need at least two points to construct a path
        If pts.Count > 1 Then
            'Generate path
            Dim iorder() As Integer
            Dim pathgen As New PathGenerator(pts.ToArray(), Nothing, rc, startIdx, False, False, GisCtrl.CS, _routeDirection, Me)
            iorder = pathgen.GeneratePath()
            If iorder IsNot Nothing Then
                'Let user choose destination layer
                Dim dlgLayer As New LayerChoiceDialog(RelationType.Undefined, LangStr(_strIdForRoute), True, TGIS_ShapeType.gisShapeTypeArc)
                If dlgLayer.ShowDialog() = DialogResult.OK Then
                    If dlgLayer.LayerOne = NewLayerName Then
                        'Create a new layer
                        Dim layerName As String = AddNewLayer()
                        lDest = GisCtrl.Get(layerName)
                    Else
                        lDest = GisCtrl.Get(dlgLayer.LayerOne)
                    End If

                    'Add route to layer
                    If lDest IsNot Nothing Then
                        shpPath = lDest.CreateShape(TGIS_ShapeType.gisShapeTypeArc)
                        shpPath.AddPart()
                        For i = 0 To iorder.Length - 1
                            pt = pts(iorder(i))
                            shpPath.AddPoint(pt)
                        Next
                        lDest.SaveAll() 'Save path before editing
                    End If

                    GisCtrl.Update()
                End If
            End If
        End If
    End Sub

    Private Sub DoGenerateRouteArc(ByVal shp As TGIS_Shape, ByVal ll As TGIS_LayerVector)
        Dim shpPoint As TGIS_Shape, shpPath As TGIS_Shape
        Dim lDest As TGIS_LayerVector
        Dim lpts As New List(Of TGIS_Point), rpts As New List(Of TGIS_Point)
        Dim i As Integer
        Dim startIdx As Integer = 0, startSecondIdx As Integer
        Dim startLeft As Boolean, reverseOrder As Boolean = False
        Dim dist As Double, mind As Double = Double.MaxValue
        Dim lorder As Integer(), rorder As Integer()
        Dim lpathgen As PathGenerator, rpathgen As PathGenerator

        'Build up two lists of points (left and right side of line)
        shpPoint = ll.FindFirst(ll.ProjectedExtent, "GIS_SELECTED=True")
        Do Until shpPoint Is Nothing
            'Store start point index, side of start point
            If shpPoint.Uid = shp.Uid Then
                If CBool(shpPoint.Tag) Then
                    startIdx = lpts.Count
                    startLeft = True
                Else
                    startIdx = rpts.Count
                    startLeft = False
                End If
            End If

            If CBool(shpPoint.Tag) Then lpts.Add(shpPoint.GetPoint(0, 0)) Else rpts.Add(shpPoint.GetPoint(0, 0))

            shpPoint = ll.FindNext()
        Loop

        'Generate one path in each direction, arrange return path so start point will be where first path ends
        If startLeft Then
            'Generate first path
            lpathgen = New PathGenerator(lpts.ToArray(), _routeMiddleLine, Nothing, startIdx, True, False, ll.CS, RouteDirection.Unspecified, Me)
            lorder = lpathgen.GenerateSequentialPath()

            'Check whether middle line is traversed in forward or reverse order
            If GetPointToPointDistance(ll.CS, lpts(startIdx), _routeMiddleLine.GetPoint(0, 0)) > GetPointToPointDistance(ll.CS, lpts(startIdx), _routeMiddleLine.GetPoint(0, _routeMiddleLine.GetNumPoints() - 1)) Then
                reverseOrder = True
            End If

            'Find the point closest to end of the path (start/end of middle line) on the right side
            For i = 0 To rpts.Count - 1
                If reverseOrder Then
                    dist = GetPointToPointDistance(ll.CS, rpts(i), _routeMiddleLine.GetPoint(0, 0))
                Else
                    dist = GetPointToPointDistance(ll.CS, rpts(i), _routeMiddleLine.GetPoint(0, _routeMiddleLine.GetNumPoints() - 1))
                End If
                If dist < mind Then
                    startSecondIdx = i
                    mind = dist
                End If
            Next

            'Generate return path
            rpathgen = New PathGenerator(rpts.ToArray(), _routeMiddleLine, Nothing, startSecondIdx, False, True, ll.CS, RouteDirection.Unspecified, Me)
            rorder = rpathgen.GenerateSequentialPath()
        Else
            'Generate first path
            rpathgen = New PathGenerator(rpts.ToArray(), _routeMiddleLine, Nothing, startIdx, False, False, ll.CS, RouteDirection.Unspecified, Me)
            rorder = rpathgen.GenerateSequentialPath()

            'Check whether middle line is traversed in forward or reverse order
            If GetPointToPointDistance(ll.CS, rpts(startIdx), _routeMiddleLine.GetPoint(0, 0)) > GetPointToPointDistance(ll.CS, rpts(startIdx), _routeMiddleLine.GetPoint(0, _routeMiddleLine.GetNumPoints() - 1)) Then
                reverseOrder = True
            End If

            'Find the point closest to the end of the path (start/end of middle line) on the left side
            For i = 0 To lpts.Count - 1
                If reverseOrder Then
                    dist = GetPointToPointDistance(ll.CS, lpts(i), _routeMiddleLine.GetPoint(0, 0))
                Else
                    dist = GetPointToPointDistance(ll.CS, lpts(i), _routeMiddleLine.GetPoint(0, _routeMiddleLine.GetNumPoints() - 1))
                End If
                If dist < mind Then
                    startSecondIdx = i
                    mind = dist
                End If
            Next

            'Generate return path
            lpathgen = New PathGenerator(lpts.ToArray(), _routeMiddleLine, Nothing, startSecondIdx, True, True, ll.CS, RouteDirection.Unspecified, Me)
            lorder = lpathgen.GenerateSequentialPath()
        End If

        'Let user choose destination layer
        Dim dlgLayer As New LayerChoiceDialog(RelationType.Undefined, LangStr(_strIdForRoute), True, TGIS_ShapeType.gisShapeTypeArc)
        If dlgLayer.ShowDialog() = DialogResult.OK Then
            If dlgLayer.LayerOne = NewLayerName Then
                'Create a new layer
                Dim layerName As String = AddNewLayer()
                lDest = GisCtrl.Get(layerName)
            Else
                lDest = GisCtrl.Get(dlgLayer.LayerOne)
            End If

            'Add route to layer
            If lDest IsNot Nothing Then
                shpPath = lDest.CreateShape(TGIS_ShapeType.gisShapeTypeArc)
                shpPath.AddPart()
                If startLeft Then
                    'Traverse left, right
                    If lorder IsNot Nothing Then
                        For i = 0 To lorder.Length - 1
                            shpPath.AddPoint(lpts(lorder(i)))
                        Next
                    End If
                    shpPath = lDest.CreateShape(TGIS_ShapeType.gisShapeTypeArc)
                    shpPath.AddPart()
                    If rorder IsNot Nothing Then
                        For i = 0 To rorder.Length - 1
                            shpPath.AddPoint(rpts(rorder(i)))
                        Next
                    End If
                Else
                    'Traverse right, left
                    If rorder IsNot Nothing Then
                        For i = 0 To rorder.Length - 1
                            shpPath.AddPoint(rpts(rorder(i)))
                        Next
                    End If
                    shpPath = lDest.CreateShape(TGIS_ShapeType.gisShapeTypeArc)
                    shpPath.AddPart()
                    If lorder IsNot Nothing Then
                        For i = 0 To lorder.Length - 1
                            shpPath.AddPoint(lpts(lorder(i)))
                        Next
                    End If
                End If

                lDest.SaveAll() 'Save path before editing

                GisCtrl.Update()
            End If
        End If
    End Sub

    Private Sub DoExportRoute(ByVal ll As TGIS_LayerVector, ByVal pmap As TGIS_Point)
        Dim i As Integer, j As Integer, k As Integer
        Dim lt As TGIS_LayerVector
        Dim shp As TGIS_Shape, shpTmp As TGIS_Shape, shpPt As TGIS_ShapePoint
        Dim tpl As New TGIS_Topology
        Dim defaultName As String = vbNullString, sectionName As String = vbNullString, sectionNumber As String = vbNullString
        Dim area As Double

        'Located nearest shape within active layer, make sure layer is visible
        shp = ll.Locate(pmap, _selectTolerance / GisCtrl.Zoom, True)
        If shp IsNot Nothing AndAlso shp.ShapeType = TGIS_ShapeType.gisShapeTypeArc Then
            'Check if route intersect any section
            For i = 0 To GisCtrl.Items.Count - 1
                lt = TryCast(GisCtrl.Items(i), TGIS_LayerVector)
                If lt IsNot Nothing AndAlso GetLayerRelationType(lt.Name) = RelationType.SectionRelation Then
                    For j = 0 To lt.GetLastUid()
                        shpTmp = lt.GetShape(j)
                        'Check each point on line for intersection (there might be other shapes in between those points)
                        For k = 0 To shp.GetNumPoints() - 1
                            shpPt = New TGIS_ShapePoint
                            shpPt.AddPart()
                            shpPt.AddPoint(shp.GetPoint(0, k))
                            If shpTmp IsNot Nothing AndAlso tpl.Intersect(shpPt, shpTmp) Then
                                'Use name of section as default file name
                                defaultName = lt.Caption & "-" & shpTmp.GetField("AVDNR")
                                sectionName = lt.Caption
                                sectionNumber = shpTmp.GetField("AVDNR")
                                area = shpTmp.AreaCS()
                            End If
                        Next
                    Next
                End If
            Next

            'Bring up file save dialog
            Dim dlg As New System.Windows.Forms.SaveFileDialog
            dlg.DefaultExt = ".hpl"
            dlg.Filter = "Haglof Plot Files (*.hpl)|*.hpl|Estimate Kraft Files (*.sup2)|*.sup2|Solo Waypoint Files (*.way)|*.way|TCGPSNavigator Waypoint Files (*.twp)|*.twp"
            If EstimateSodraInstalled Then dlg.Filter = "S�dra Plot Files (*.sod)|*.sod|" & dlg.Filter
            If defaultName <> vbNullString AndAlso EstimateSodraInstalled Then dlg.FileName = defaultName & ".sod"
            If dlg.ShowDialog() = DialogResult.OK Then
                Dim fi As FileInfo = New FileInfo(dlg.FileName)
                Dim pt As TGIS_Point
                Dim isPlot As Boolean
                Dim plotId As Integer
                Dim plotIdList As New List(Of Integer)
                Dim tmp As TGIS_Shape

                'Go through each vertex, check whether it belongs to a plot or if it's just a waypoint
                For i = 0 To shp.GetNumPoints() - 1
                    isPlot = False
                    plotId = 0

                    'Look for a pinned point in each layer
                    For j = 0 To GisCtrl.Items.Count - 1
                        lt = TryCast(GisCtrl.Items(j), TGIS_LayerVector)
                        If lt IsNot Nothing Then
                            tmp = lt.Locate(shp.GetPoint(0, i), 0) 'Only points with zero distance (route must be snapped to point)
                            If tmp IsNot Nothing AndAlso tmp.ShapeType = TGIS_ShapeType.gisShapeTypePoint Then
                                isPlot = True
                                If lt.FindField(FieldNamePlotId) <> -1 Then
                                    Try 'In case field type mismatch
                                        plotId = CInt(tmp.GetField(FieldNamePlotId))
                                    Catch ex As InvalidCastException
                                    End Try
                                End If
                                Exit For
                            End If
                        End If
                    Next

                    'Add plot number for this vertex to list
                    If isPlot AndAlso plotId > 0 Then 'Plot id start from 1 (0 is considered to be a waypoint)
                        plotIdList.Add(plotId)
                    Else
                        plotIdList.Add(-1)
                    End If
                Next

                If fi.Extension.ToUpperInvariant() = ".HPL" Then
                    'Write plot data to file
                    Dim fs As New FileStream(dlg.FileName, IO.FileMode.Create, IO.FileAccess.Write)
                    Dim s As New BinaryWriter(fs)

                    'Write file header
                    s.Write(_hplFileVersion) 'File version
                    s.Write(shp.GetNumPoints()) 'Num points

                    'Write vertices
                    For i = 0 To shp.GetNumPoints() - 1
                        pt = GisCtrl.CS.ToWGS(shp.GetPoint(0, i))
                        s.Write(pt.X)
                        s.Write(pt.Y)
                        s.Write(plotIdList(i))
                    Next

                    s.Close()

                    MsgBox(CStr(LangStr(_strIdPlotExportSuccessful)).Replace("(:1)", shp.GetNumPoints), MsgBoxStyle.Information)
                ElseIf fi.Extension.ToUpperInvariant() = ".SUP2" Then
                    'Let user choose custom attributes
                    Dim dlgAttributes As New ExportToEstimateKraftDialog
                    If dlgAttributes.ShowDialog() = DialogResult.OK Then
                        'Save shp points to file
                        Dim fs As New FileStream(dlg.FileName, IO.FileMode.Create, IO.FileAccess.Write)
                        Dim s As New StreamWriter(fs)
                        Dim sign As Integer
                        Dim lat As String, lon As String
                        Dim deg As Double, min As Double, sec As Double, frac As Double
                        Dim strHeight As String, strHdist As String, strTdist As String
                        Dim dt As DataTable
                        Dim curRow As DataRow
                        Dim rt As RelationType = RelationType.Undefined
                        Dim jv As String = vbNullString

                        'Write file header
                        s.Write("102" & vbCrLf)

                        'Write waypoints
                        For i = 0 To shp.GetNumPoints() - 1
                            'Get point coordinates in lat/lon format
                            pt = GisCtrl.CS.ToWGS(shp.GetPoint(0, i))

                            TGIS_Utils.GisDecodeLatitude(pt.Y, deg, min, sec, frac, sign, 4)
                            If sign < 0 Then lat = "-" Else lat = vbNullString
                            lat += CStr(deg).PadLeft(2, "0"c) & CStr(min).PadLeft(2, "0"c) & CStr(Int(((sec * 10000) + frac) / 60)).PadLeft(4, "0"c)

                            TGIS_Utils.GisDecodeLongitude(pt.X, deg, min, sec, frac, sign, 4)
                            If sign < 0 Then lon = "-" Else lon = vbNullString
                            lon += CStr(deg).PadLeft(2, "0"c) & CStr(min).PadLeft(2, "0"c) & CStr(Int(((sec * 10000) + frac) / 60)).PadLeft(4, "0"c)

                            strHdist = vbNullString
                            strTdist = vbNullString
                            strHeight = vbNullString

                            'Look for a matching point in the data layer
                            lt = GisCtrl.Get(dlgAttributes.LayerName)
                            dt = CType(lt.JoinNET, DataTable)
                            GisControlView.GetLayerRelationType(lt.Name, rt, jv)
                            tmp = lt.Locate(shp.GetPoint(0, i), 0)
                            If tmp IsNot Nothing AndAlso tmp.ShapeType = TGIS_ShapeType.gisShapeTypePoint Then
                                'Find any joined data
                                curRow = Nothing
                                If dt IsNot Nothing Then
                                    For Each row As DataRow In dt.Rows
                                        If row.Item(GisControlView.ForeignKeyField) = tmp.GetField(GisControlView.GetJoinPrimary(jv)) Then
                                            curRow = row
                                            Exit For
                                        End If
                                    Next
                                End If

                                'Match attributes from shape or join table
                                If lt.FindField(dlgAttributes.FieldNameHdist) >= 0 Then
                                    strHdist = ConvertStringDecimal(tmp.GetField(dlgAttributes.FieldNameHdist))
                                ElseIf curRow IsNot Nothing AndAlso dt.Columns.Contains(dlgAttributes.FieldNameHdist) Then
                                    strHdist = curRow.Item(dlgAttributes.FieldNameHdist)
                                End If

                                If lt.FindField(dlgAttributes.FieldNameTdist) >= 0 Then
                                    strTdist = ConvertStringDecimal(tmp.GetField(dlgAttributes.FieldNameTdist))
                                ElseIf curRow IsNot Nothing AndAlso dt.Columns.Contains(dlgAttributes.FieldNameTdist) Then
                                    strTdist = curRow.Item(dlgAttributes.FieldNameTdist)
                                End If

                                If lt.FindField(dlgAttributes.FieldNameHeight) >= 0 Then
                                    strHeight = ConvertStringDecimal(tmp.GetField(dlgAttributes.FieldNameHeight))
                                ElseIf curRow IsNot Nothing AndAlso dt.Columns.Contains(dlgAttributes.FieldNameHeight) Then
                                    strHeight = curRow.Item(dlgAttributes.FieldNameHeight)
                                End If
                            End If

                            'Height, Lat, Lon, Hdist, Tdist (where lat/lon is in format ddmm.ssssss)
                            s.Write(strHeight & " " & lat & " " & lon & " " & strHdist & " " & strTdist & vbCrLf)
                        Next

                        s.Close()

                        MsgBox(CStr(LangStr(_strIdPlotExportSuccessful)).Replace("(:1)", shp.GetNumPoints), MsgBoxStyle.Information)
                    End If
                ElseIf fi.Extension.ToUpperInvariant() = ".SOD" Then
                    'Write plot data to file (S�dra specific)
                    Dim fs As New FileStream(dlg.FileName, IO.FileMode.Create, IO.FileAccess.Write)
                    Dim s As New BinaryWriter(fs, System.Text.Encoding.GetEncoding(1252))
                    Dim nameBuffer(49) As Char
                    Dim sectionBuffer(2) As Char

                    'Fill name buffer
                    For i = 0 To 49
                        If sectionName = vbNullString OrElse i > sectionName.Length - 1 Then Exit For
                        nameBuffer(i) = sectionName.Substring(i, 1)
                    Next
                    For i = 0 To 2
                        If sectionNumber = vbNullString OrElse i > sectionNumber.Length - 1 Then Exit For
                        sectionBuffer(i) = sectionNumber.Substring(i, 1)
                    Next

                    'Write file header
                    s.Write(_sodFileVersion) 'File version
                    s.Write(area) 'Shape area (of underlying)
                    s.Write(nameBuffer) 'Section name
                    s.Write(sectionBuffer) 'Section number
                    s.Write(shp.GetNumPoints()) 'Num points

                    'Write vertices
                    For i = 0 To shp.GetNumPoints() - 1
                        pt = GisCtrl.CS.ToWGS(shp.GetPoint(0, i))
                        s.Write(pt.X)
                        s.Write(pt.Y)
                        s.Write(plotIdList(i))
                    Next

                    s.Close()

                    MsgBox(CStr(LangStr(_strIdPlotExportSuccessful)).Replace("(:1)", shp.GetNumPoints), MsgBoxStyle.Information)
                ElseIf fi.Extension.ToUpperInvariant() = ".WAY" Then
                    'Write plot data to file
                    Dim fs As New FileStream(dlg.FileName, IO.FileMode.Create, IO.FileAccess.Write)
                    Dim s As New StreamWriter(fs)
                    Dim deg As Double, min As Double, sec As Double, frac As Double
                    Dim sign As Integer
                    Dim lat As String, lon As String, desc As String

                    'Write waypoints
                    For i = 0 To shp.GetNumPoints() - 1
                        pt = GisCtrl.CS.ToWGS(shp.GetPoint(0, i))

                        TGIS_Utils.GisDecodeLatitude(pt.Y, deg, min, sec, frac, sign, 4)
                        If sign < 0 Then lat = "-" Else lat = vbNullString
                        lat += CStr(deg).PadLeft(2, "0"c) & CStr(min).PadLeft(2, "0"c) & "." & CStr(sec).PadLeft(2, "0"c) & CStr(frac).PadLeft(4, "0"c)

                        TGIS_Utils.GisDecodeLongitude(pt.X, deg, min, sec, frac, sign, 4)
                        If sign < 0 Then lon = "-" Else lon = vbNullString
                        lon += CStr(deg).PadLeft(2, "0"c) & CStr(min).PadLeft(2, "0"c) & "." & CStr(sec).PadLeft(2, "0"c) & CStr(frac).PadLeft(4, "0"c)

                        If plotIdList(i) >= 0 Then desc = plotIdList(i) Else desc = vbNullString

                        'Format is waypoint name,lat,lon,elevation,description (where lat/lon is in format ddmm.ssssss)
                        s.Write(CStr(i) & "," & lat & "," & lon & ",0," & desc & "," & vbCrLf)
                    Next

                    s.Close()

                    MsgBox(CStr(LangStr(_strIdPlotExportSuccessful)).Replace("(:1)", shp.GetNumPoints), MsgBoxStyle.Information)
                ElseIf fi.Extension.ToUpperInvariant() = ".TWP" Then
                    'Write plot data to file. Note! We need to reverse the byte order as this file is supposed to be used with an ARM processor (i.e. Big Endian encoding).
                    Dim fs As New FileStream(dlg.FileName, IO.FileMode.Create, IO.FileAccess.Write)
                    Dim s As New BinaryWriter(fs)

                    'Write file header
                    WriteTCGPSString(s, _tcgpsnavUuid)
                    WriteTCGPSString(s, _tcgpsnavVersion)
                    For i = 1 To 20 'Empty string x 20
                        WriteTCGPSString(s, String.Empty)
                    Next
                    For i = 1 To 40 'Empty WORD x 20 (options) + Empty WORD x 20 (size)
                        s.Write(CType(0, UShort))
                    Next
                    WriteTCGPSInt32(s, _tcgpsnavWaypointTypeCount) 'Waypoint type count
                    For i = 0 To _tcgpsnavWaypointTypeCount - 1
                        WriteTCGPSString(s, _tcgpsnavWaypointType(i)) 'Waypoint type string
                        WriteTCGPSString(s, _tcgpsnavWaypointTypeLN(i)) 'Waypoint type ln string
                    Next

                    'Write coordinate
                    WriteTCGPSInt32(s, shp.GetNumPoints()) 'Waypoint id count
                    For i = 0 To shp.GetNumPoints() - 1
                        'Unproject coordinate to get lat/long
                        pt = GisCtrl.CS.ToWGS(shp.GetPoint(0, i))
                        pt.X = pt.X / Math.PI * 180 'Convert from radians to degrees
                        pt.Y = pt.Y / Math.PI * 180

                        WriteTCGPSString(s, "Wp" & CStr(i + 1)) 'Waypoint id
                        WriteTCGPSString(s, "G" & CStr(i + 1)) 'Waypoint group
                        WriteTCGPSString(s, String.Empty) 'UTC Time
                        WriteTCGPSString(s, String.Empty) 'Date
                        WriteTCGPSString(s, String.Empty) 'Empty
                        WriteTCGPSString(s, String.Empty) 'Empty
                        WriteTCGPSDouble(s, pt.Y) 'Latitude (degrees)
                        WriteTCGPSDouble(s, pt.X) 'Longitude (degrees)
                        s.Write(CType(0, Byte)) 'Waypoint type
                        WriteTCGPSDouble(s, 0) 'Elevation
                        WriteTCGPSDouble(s, 0) 'Zero
                        WriteTCGPSDouble(s, 0) 'Zero
                        WriteTCGPSDouble(s, 0) 'Zero
                    Next
                    ll.SaveAll()

                    s.Close()

                    MsgBox(shp.GetNumPoints() & LangStr(_strIdPlotExportSuccessful), MsgBoxStyle.Information)
                Else
                    'Unsupported extension
                    MsgBox(CStr(LangStr(_strIdPlotExportSuccessful)).Replace("(:1)", shp.GetNumPoints), MsgBoxStyle.Exclamation)
                End If
            End If

            SetGisMode(GisMode.Drag)
        End If
    End Sub

    Private Sub DoSplitShape(ByVal ll As TGIS_LayerVector, ByVal pmap As TGIS_Point)
        Dim shp As TGIS_Shape

        'Create new shape if not exist, make sure layer is visible
        shp = ll.GetShape(_customShapeUid)
        If ll.Active AndAlso shp Is Nothing Then
            shp = ll.CreateShape(TGIS_ShapeType.gisShapeTypeArc)
            If shp IsNot Nothing Then
                shp.AddPart()
                _customShapeUid = shp.Uid

                'Create a starting point for the line
                shp.AddPoint(TGIS_Utils.GisPoint(pmap.X, pmap.Y))
                GisCtrl.Editor.EditShape(shp, 0)
            End If
        End If
    End Sub

    Private Sub DoSelectShape(ByVal ll As TGIS_LayerVector, ByVal pmap As TGIS_Point)
        Dim shp As TGIS_Shape

        'Located nearest shape within active layer, make sure layer is visible
        shp = ll.Locate(pmap, _selectTolerance / GisCtrl.Zoom, True)
        If ll.Active AndAlso shp IsNot Nothing Then
            'Multiselect with Ctrl, otherwise clear any existing selection
            If Not IsCtrlPressed() Then ll.DeselectAll()

            'Invert selection
            shp = shp.MakeEditable()
            shp.IsSelected = Not shp.IsSelected
            shp.Invalidate()

            'Update measurement info
            UpdateLengthArea(shp)
        End If
    End Sub

    Private Sub DoViewShapeInfo(ByVal ll As TGIS_LayerVector, ByVal pmap As TGIS_Point)
        Dim shp As TGIS_Shape

        'Display attributes of nearest shape, make sure layer is visible
        shp = ll.Locate(pmap, _selectTolerance / GisCtrl.Zoom, True)

        If ll.Active AndAlso shp IsNot Nothing Then
            If shp.IsModified = True Then shp.Layer.SaveData()

            If Not shp.IsEditable = True Then shp = shp.MakeEditable()

            Dim dlg As New AttributeDialog(shp)
            If dlg.ShowDialog() = DialogResult.OK Then
                'Update the current shape's record in the list field
                UpdateListFieldData(shp)

                'Update the Sodra table as well
                If EstimateSodraInstalled Then
                    'Check relation type
                    Dim rt As RelationType = GetLayerRelationType(ll.Name)
                    Dim dgv As Double
                    If rt = RelationType.SectionRelation Then
                        'Make sure shape is already connected to a section
                        If shp.GetField(FieldNameRelation) > 0 Then

                            'Get DGV if value exist
                            If shp.Layer.FindField(FieldNameSectionDGV) >= 0 Then
                                dgv = shp.GetField(FieldNameSectionDGV)
                            Else
                                dgv = 0
                            End If

                            Try
                                'Update the database
                                Dim cmd As New OleDbCommand("", Conn)
                                cmd.CommandText = "UPDATE esti_trakt_sodra_gis_table SET " & _
                                                "trakt_sodra_agoslag = " & shp.GetField(FieldNameSectionAgoslag) & ", " & _
                                                "trakt_sodra_malklass = " & shp.GetField(FieldNameSectionMalklass) & ", " & _
                                                "trakt_sodra_hkl = " & shp.GetField(FieldNameSectionHkl) & ", " & _
                                                "trakt_sodra_alder = " & shp.GetField(FieldNameSectionAlder) & ", " & _
                                                "trakt_sodra_atgard = " & shp.GetField(FieldNameSectionAtgard) & ", " & _
                                                "trakt_sodra_atgardof = " & shp.GetField(FieldNameSectionAtgardof) & ", " & _
                                                "trakt_sodra_hasof = " & shp.GetField(FieldNameSectionHasof) & ", " & _
                                                "trakt_sodra_skifte = " & shp.GetField(FieldNameSectionSkifte) & ", " & _
                                                "trakt_sodra_medelhojd = " & shp.GetField(FieldNameSectionMedelhojd) & ", " & _
                                                "trakt_sodra_volym = " & shp.GetField(FieldNameSectionVolym) & ", " & _
                                                "trakt_sodra_fuktklass = " & shp.GetField(FieldNameSectionFuktklass) & ", " & _
                                                "trakt_sodra_standortsi = " & shp.GetField(FieldNameSectionStandortsi) & ", " & _
                                                "trakt_sodra_vegitation = " & shp.GetField(FieldNameSectionVegitation) & ", " & _
                                                "trakt_sodra_gryndyta = " & shp.GetField(FieldNameSectionGrundyta) & ", " & _
                                                "trakt_sodra_framskrive = '" & shp.GetField(FieldNameSectionFramskrive) & "', " & _
                                                "trakt_sodra_dgv = " & dgv & _
                                                " WHERE id = " & shp.GetField(FieldNameRelation) & " AND trakt_sodra_gis_uid = " & shp.GetField(ColumnNameUid)
                                cmd.ExecuteNonQuery()

                            Catch ex As EGIS_Exception
                                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                            End Try
                            SetGisMode(GisMode.Drag)
                        End If
                    End If
                End If 'Sodra?
            End If
        End If
    End Sub

    Private Sub DoApplyRelation(ByVal ll As TGIS_LayerVector, ByVal pmap As TGIS_Point)
        Dim cmd As New OleDbCommand("", Conn)
        Dim reader As OleDbDataReader = Nothing
        Dim label As String = vbNullString
        Dim ans As Microsoft.VisualBasic.MsgBoxResult
        Dim shp As TGIS_Shape

        If _applyRelationId > 0 Then
            If GetLayerRelationType(ll.Name) <> _applyRelationType Then
                MsgBox(LangStr(_strIdApplyRelationWrongType), MsgBoxStyle.Information)
                Exit Sub
            End If

            'Locate nearest shape
            shp = ll.Locate(pmap, _selectTolerance / GisCtrl.Zoom, True)
            If ll.Active AndAlso shp IsNot Nothing Then
                shp = shp.MakeEditable()
                shp.IsSelected = True
                shp.Invalidate()

                'Get name from database
                If _applyRelationType = RelationType.StandRelation OrElse _applyRelationType = RelationType.SectionRelation Then
                    cmd.CommandText = "SELECT trakt_name FROM esti_trakt_table WHERE trakt_id = " & _applyRelationId
                ElseIf _applyRelationType = RelationType.PropertyRelation Then
                    cmd.CommandText = "SELECT prop_name + ' ' + block_number + ':' + unit_number FROM fst_property_table WHERE id = " & _applyRelationId
                ElseIf _applyRelationType = RelationType.ObjectRelation Then
                    cmd.CommandText = "SELECT object_name FROM elv_object_table WHERE object_id = " & _applyRelationId
                End If

                If cmd.CommandText <> vbNullString Then
                    reader = cmd.ExecuteReader(CommandBehavior.SingleRow)
                    If reader.Read() Then
                        label = reader.GetString(0)
                    End If
                    SetUpVisualizationParams() 'Update any joined data

                    'Ask user whether to create relation or not
                    ans = MsgBox(label & vbCrLf & LangStr(_strIdAddRelationQuestion), MsgBoxStyle.Question Or MsgBoxStyle.YesNo)
                    shp.IsSelected = False
                    shp.Invalidate()
                    If ans = MsgBoxResult.Yes Then
                        shp.SetField(FieldNameRelation, _applyRelationId)
                        ll.SaveAll()

                        '#3724: Add centroid to property table
                        If _applyRelationType = RelationType.PropertyRelation Then
                            AddCentroidToPropertyTable(_applyRelationId, shp)
                        End If
                    ElseIf _applyRelationType = RelationType.StandRelation Then
                        'Ask to update stand area
                        If MsgBox(LangStr(_strIdUpdateStandArea), MsgBoxStyle.Question Or MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then UpdateStandArea(shp.GetField(FieldNameRelation), shp)
                    End If
                End If
            End If

            'Always fall back to drag mode
            SetGisMode(GisMode.Drag)

            If ans = MsgBoxResult.Yes OrElse cmd.CommandText = vbNullString Then
                'Disable add relation guide
                _applyRelationId = 0
                _applyRelationType = RelationType.Undefined
            End If
        End If
    End Sub

    Private Sub CreatePlots()
        Dim ll As TGIS_LayerVector = TryCast(GisCtrl.Get(_activeLayerName), TGIS_LayerVector)
        Dim shp As TGIS_Shape
        Dim tpl As New TGIS_Topology
        Dim shapes As New List(Of TGIS_Shape)
        Dim objectlist As New List(Of ValueDescriptionPair)
        Dim objectname As String
        Dim uid As Integer

        'Wrapper function to enable plot generation with multiple shapes
        If ll IsNot Nothing AndAlso ll.GetSelectedCount() > 0 AndAlso Not ll.IsReadOnly Then
            'Merge selected shapes into a temp shape
            For i As Integer = 0 To ll.Items.Count - 1
                shp = CType(ll.Items(i), TGIS_Shape)
                If shp.IsSelected Then
                    shapes.Add(shp)

                    'Add relation name to list
                    objectname = GetShapeRelationName(shp)
                    If objectname <> vbNullString Then objectlist.Add(New ValueDescriptionPair(shp.Uid, objectname))
                End If
            Next
            shp = tpl.UnionOnList(shapes)
            ll.AddShape(shp)
            uid = ll.GetLastUid()

            'Show plot dialog
            Dim dlg As New PlotDialog(ll, uid, objectlist)
            dlg.ShowDialog()

            'Remove temp shape
            ll.Delete(uid)
            ll.SaveAll()
            SetGisMode(GisMode.Drag)
        Else
            SetGisMode(GisMode.CreatePlots)
        End If
    End Sub

    Private Sub GenerateRoute()
        Dim ll As TGIS_LayerVector = TryCast(GisCtrl.Get(_activeLayerName), TGIS_LayerVector), lt As TGIS_LayerVector
        Dim shp As TGIS_Shape, shpTemp As TGIS_Shape
        Dim tpl As New TGIS_Topology

        'Wrapper function to enable route generation with multiple shapes
        If ll IsNot Nothing AndAlso ll.GetSelectedCount() > 0 Then
            'Let user choose a plot layer
            Dim dlg As New LayerChoiceDialog(RelationType.Undefined, LangStr(_strIdPlotLayerForRoute), True, TGIS_ShapeType.gisShapeTypeNull, False)
            If dlg.ShowDialog() = DialogResult.OK Then
                _plotLayerForRoute = dlg.LayerOne
                lt = GisCtrl.Get(dlg.LayerOne)

                For i As Integer = 0 To ll.Items.Count - 1
                    shp = CType(ll.Items(i), TGIS_Shape)
                    If shp.IsSelected Then
                        'Mark intersecting plots
                        shpTemp = lt.FindFirst()
                        Do Until shpTemp Is Nothing
                            'Check only points
                            If (shpTemp.ShapeType = TGIS_ShapeType.gisShapeTypePoint OrElse shpTemp.ShapeType = TGIS_ShapeType.gisShapeTypeMultiPoint) AndAlso tpl.Intersect(shp, shpTemp) Then
                                shpTemp.MakeEditable().IsSelected = True
                            End If
                            shpTemp = lt.FindNext()
                        Loop
                    End If
                Next

                'Check if any intersecting plots were found, otherwise abort route generation procedure
                If ll.GetSelectedCount() > 0 Then
                    GisCtrl.Update()
                    SetGisMode(GisMode.GenerateRouteStep2)
                Else
                    MsgBox(LangStr(_strIdNoPlotsWithinBoundary), MsgBoxStyle.Exclamation)
                    SetGisMode(GisMode.Drag)
                End If
            Else
                SetGisMode(GisMode.Drag)
            End If
        Else
            SetGisMode(GisMode.GenerateRouteStep1WithoutCenterLine)
        End If
    End Sub

    Private Sub ExportToTCNavigator()
        Dim ll As TGIS_LayerVector = TryCast(GisCtrl.Get(_activeLayerName), TGIS_LayerVector)

        'Export vector data that intersects with the selection box
        'Store polygons in "<name>_bnd.shp", points in "<name>_point.shp" and lines in "<name>_line.shp"
        If ll IsNot Nothing Then
            Dim dlg As New ExportToTCNavigatorDialog(ll.Name, _selectedOrigin, _selectedEndPoint)
            dlg.ShowDialog()
        End If

        'Fall back to drag mode
        SetGisMode(GisMode.Drag)
        GisCtrl.Update()

        _selectionStarted = False
        _selectionEndPointSet = False
    End Sub

    Private Function FilterCharacters(ByVal value As String) As String
        'TatukGIS cannot handle special characters (eg. ���) so we need to replace them
        If value IsNot Nothing Then
            value = value.Replace("�"c, "a"c)
            value = value.Replace("�"c, "a"c)
            value = value.Replace("�"c, "o"c)
            value = value.Replace("�"c, "A"c)
            value = value.Replace("�"c, "A"c)
            value = value.Replace("�"c, "O"c)
        End If

        Return value
    End Function

    Private Sub AddJoinViewFieldsToLayer(ByVal lDest As TGIS_LayerVector, ByVal sourceLayer As String)
        Dim cmd As New OleDbCommand("", Conn)
        Dim reader As OleDbDataReader
        Dim dt As TGIS_FieldType
        Dim rt As RelationType
        Dim joinView As String = vbNullString
        Dim flen As Integer
        Dim caption As String

        Const DefaultNumberLength As Integer = 38
        Const DefaultStringLength As Integer = 50

        GetLayerRelationType(sourceLayer, rt, joinView)

        'Get view columns and data types from db
        cmd.CommandText = "SELECT COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" & joinView & "'"
        reader = cmd.ExecuteReader()
        Do While reader.Read()
            'Check field type
            flen = 0
            Select Case reader.GetString(1)
                Case "bit"
                    dt = TGIS_FieldType.gisFieldTypeBoolean
                Case "date", "smalldatetime"
                    dt = TGIS_FieldType.gisFieldTypeDate
                Case "float", "real"
                    dt = TGIS_FieldType.gisFieldTypeFloat
                Case "bigint", "int", "smallint", "tinyint"
                    dt = TGIS_FieldType.gisFieldTypeNumber
                    flen = DefaultNumberLength
                Case Else
                    dt = TGIS_FieldType.gisFieldTypeString
                    If Not reader.IsDBNull(2) Then flen = reader.GetInt32(2) Else flen = DefaultStringLength
            End Select

            'Set up any language dependent columns
            If IsNumeric(reader.GetString(0)) Then
                caption = LangStr(CInt(reader.GetString(0)))
            Else
                caption = reader.GetString(0)
            End If

            lDest.AddField(FilterCharacters(caption), dt, flen, 0)
        Loop
        reader.Close()
    End Sub

    Private Sub ExportShp()
        Dim dlgFolder As New FolderBrowserDialog
        Dim i As Integer, j As Integer, k As Integer, l As Integer
        Dim ctPoint As Integer = 0, ctMultiPoint As Integer = 0, ctLine As Integer = 0, ctPolygon As Integer = 0
        Dim lPoint As TGIS_LayerSHP = Nothing, lMultiPoint As TGIS_LayerSHP = Nothing, lLine As TGIS_LayerSHP = Nothing, lPolygon As TGIS_LayerSHP = Nothing
        Dim ll As TGIS_LayerVector, lp As TGIS_LayerVector, la As TGIS_LayerAbstract, lc As TGIS_CompoundLayerVector
        Dim shp As TGIS_Shape, shpRect As TGIS_Shape, shpNew As TGIS_Shape = Nothing
        Dim cancel As Boolean = False, removeAttributes As Boolean = False
        Dim fileName As String, fieldname As String
        Dim rt As RelationType = RelationType.Undefined
        Dim jv As String = vbNullString
        Dim cmd As New OleDbCommand("", Conn)
        Dim da As New OleDbDataAdapter(cmd)
        Dim layers As New List(Of LayerShapeList)
        Dim lsl As LayerShapeList

        If _selectionEndPointSet Then
            'Let user choose destination path
            If dlgFolder.ShowDialog() = DialogResult.OK Then
                Dim oldCursor As Cursor = GisCtrl.Cursor
                GisCtrl.Cursor = Cursors.WaitCursor

                'Create temp layer with selection shape
                lp = GetPreviewLayer()
                lp.Transparency = 0 'Hide this layer

                'Selected rectangle
                shpRect = lp.CreateShape(TGIS_ShapeType.gisShapeTypePolygon)
                shpRect.AddPart()
                shpRect.AddPoint(GisCtrl.ScreenToMap(TGIS_Utils.Point(_selectedOrigin.X, _selectedOrigin.Y)))     'x1y1
                shpRect.AddPoint(GisCtrl.ScreenToMap(TGIS_Utils.Point(_selectedEndPoint.X, _selectedOrigin.Y)))   'x2y1
                shpRect.AddPoint(GisCtrl.ScreenToMap(TGIS_Utils.Point(_selectedEndPoint.X, _selectedEndPoint.Y))) 'x2y2
                shpRect.AddPoint(GisCtrl.ScreenToMap(TGIS_Utils.Point(_selectedOrigin.X, _selectedEndPoint.Y)))   'x1y2

                'Lock the GIS control
                GisCtrl.Lock()

                'Loop through all layers
                For i = 0 To GisCtrl.Items.Count - 1
                    'Make sure layer is visible (and exclude preview layer)
                    la = GisCtrl.Items.Items(i)
                    If la.Active AndAlso la.Name <> lp.Name Then
                        lsl = Nothing

                        'Make a list of all intersecting layers and shapes
                        If TypeOf la Is TGIS_LayerVector Then
                            'Typical vector layer
                            ll = CType(la, TGIS_LayerVector)
                            shp = ll.FindFirst()
                            Do Until shp Is Nothing
                                'Check for intersection (make sure shape is not within temp layer)
                                If shpRect.Intersect(shp) Then
                                    If lsl Is Nothing Then lsl = New LayerShapeList(ll)
                                    lsl.AddShapeId(shp.Uid)

                                    'Cache any joined data
                                    If lsl.DT Is Nothing Then
                                        GetLayerRelationType(ll.Name, rt, jv)
                                        If rt <> RelationType.Undefined AndAlso jv <> vbNullString Then
                                            cmd.CommandText = "SELECT UID, " & jv & ".* FROM " & GisTablePrefix & ll.Name & "_FEA f LEFT JOIN " & jv & " ON " & jv & "." & ForeignKeyField & " = f." & GetJoinPrimary(jv)
                                            lsl.DT = New DataTable
                                            lsl.DT.Locale = CultureInfo.InvariantCulture
                                            da.Fill(lsl.DT)
                                            For j = 0 To lsl.DT.Columns.Count - 1 'Set up any language dependent columns
                                                If IsNumeric(lsl.DT.Columns(j).Caption) AndAlso LangStr(CInt(lsl.DT.Columns(j).Caption)) IsNot Nothing Then
                                                    lsl.DT.Columns(j).Caption = LangStr(CInt(lsl.DT.Columns(j).Caption))
                                                End If
                                            Next
                                        End If
                                    End If
                                End If

                                shp = ll.FindNext()
                            Loop
                        ElseIf TypeOf la Is TGIS_CompoundLayerVector Then
                            'Compound vector layer with sublayers
                            lc = CType(la, TGIS_CompoundLayerVector)
                            For j = 0 To lc.SubLayers.Count - 1
                                ll = CType(lc.SubLayers(j), TGIS_LayerVector)
                                If ll.Active Then
                                    shp = ll.FindFirst()
                                    Do Until shp Is Nothing
                                        'Check for intersection (make sure shape is not within temp layer)
                                        'Compound layers won't have any joined data
                                        If shpRect.Intersect(shp) Then
                                            If lsl Is Nothing Then lsl = New LayerShapeList(ll)
                                            lsl.AddShapeId(shp.Uid)
                                        End If
                                        shp = ll.FindNext()
                                    Loop
                                    If lsl IsNot Nothing Then layers.Add(lsl) 'Add each sublayer separately
                                    lsl = Nothing
                                End If
                            Next
                        End If

                        If lsl IsNot Nothing Then layers.Add(lsl)
                    End If
                Next

                'Ask user to select attributes
                Dim dlg As New ExportAttributeDialog(layers)
                If layers.Count > 0 Then
                    If MsgBox(LangStr(_strIdSelectAttributes), MsgBoxStyle.Question Or MsgBoxStyle.YesNo Or MsgBoxStyle.DefaultButton2) = MsgBoxResult.Yes Then
                        If dlg.ShowDialog() = DialogResult.OK Then
                            removeAttributes = True
                        Else
                            cancel = True
                        End If
                    End If
                End If

                If Not cancel Then 'User aborted?
                    For i = 0 To layers.Count - 1
                        ll = layers(i).Layer

                        'Prepare filename
                        fileName = ll.Caption
                        For Each ch As Char In Path.GetInvalidFileNameChars()
                            fileName = fileName.Replace(ch, "_"c) 'Replace any illegal chars
                        Next

                        For j = 0 To layers(i).ShapeCount - 1
                            shp = ll.GetShape(layers(i).ShapeId(j))

                            'Create one shape file per layer and shape type
                            Select Case shp.ShapeType
                                Case TGIS_ShapeType.gisShapeTypePoint
                                    If lPoint Is Nothing Then
                                        lPoint = New TGIS_LayerSHP
                                        lPoint.CS = ll.CS
                                        lPoint.Path = dlgFolder.SelectedPath & "\" & fileName & "_point.shp"
                                        AddJoinViewFieldsToLayer(lPoint, ll.Name)
                                    End If
                                    shpNew = lPoint.AddShape(shp, True)
                                    ctPoint += 1

                                Case TGIS_ShapeType.gisShapeTypeMultiPoint
                                    If lMultiPoint Is Nothing Then
                                        lMultiPoint = New TGIS_LayerSHP
                                        lMultiPoint.CS = ll.CS
                                        lMultiPoint.Path = dlgFolder.SelectedPath & "\" & fileName & "_multipoint.shp"
                                        AddJoinViewFieldsToLayer(lMultiPoint, ll.Name)
                                    End If
                                    shpNew = lMultiPoint.AddShape(shp, True)
                                    ctMultiPoint += 1

                                Case TGIS_ShapeType.gisShapeTypeArc
                                    If lLine Is Nothing Then
                                        lLine = New TGIS_LayerSHP
                                        lLine.CS = ll.CS
                                        lLine.Path = dlgFolder.SelectedPath & "\" & fileName & "_line.shp"
                                        AddJoinViewFieldsToLayer(lLine, ll.Name)
                                    End If
                                    shpNew = lLine.AddShape(shp, True)
                                    ctLine += 1

                                Case TGIS_ShapeType.gisShapeTypePolygon
                                    If lPolygon Is Nothing Then
                                        lPolygon = New TGIS_LayerSHP
                                        lPolygon.CS = ll.CS
                                        lPolygon.Path = dlgFolder.SelectedPath & "\" & fileName & "_polygon.shp"
                                        AddJoinViewFieldsToLayer(lPolygon, ll.Name)
                                    End If
                                    shpNew = lPolygon.AddShape(shp, True)
                                    ctPolygon += 1
                            End Select
                            If shpNew.Layer.FindField(FieldNameOrigId) < 0 Then shpNew.Layer.AddField(FieldNameOrigId, TGIS_FieldType.gisFieldTypeNumber, 10, 0)
                            shpNew.SetField(FieldNameOrigId, shp.Uid) 'Copy UID

                            'Add any data from join view
                            If layers(i).DT IsNot Nothing Then
                                For k = 0 To layers(i).DT.Rows.Count - 1
                                    If layers(i).DT.Rows(k).Item(0) = shp.Uid Then
                                        For l = 1 To layers(i).DT.Columns.Count - 1
                                            If layers(i).DT.Columns.Item(l).Caption = ForeignKeyField Then Continue For 'Skip key field
                                            shpNew.SetField(FilterCharacters(layers(i).DT.Columns.Item(l).Caption), layers(i).DT.Rows(k).Item(l))
                                        Next
                                        Exit For
                                    End If
                                Next
                            End If
                        Next

                        'Remove any deselected attributes from layer
                        If removeAttributes Then
                            Dim lattr As List(Of StringBoolPair)
                            lattr = dlg.Attributes(ll.Name)
                            For j = 0 To lattr.Count - 1
                                If lattr(j).Value = False Then
                                    fieldname = FilterCharacters(lattr(j).Name)
                                    If fieldname = ColumnNameUid Then fieldname = FieldNameOrigId 'ORIG_ID is used for original GIS_UID
                                    Try
                                        If lPoint IsNot Nothing Then lPoint.DeleteField(fieldname)
                                    Catch ex As EGIS_Exception
                                    End Try
                                    Try
                                        If lMultiPoint IsNot Nothing Then lMultiPoint.DeleteField(fieldname)
                                    Catch ex As EGIS_Exception
                                    End Try
                                    Try
                                        If lLine IsNot Nothing Then lLine.DeleteField(fieldname)
                                    Catch ex As EGIS_Exception
                                    End Try
                                    Try
                                        If lPolygon IsNot Nothing Then lPolygon.DeleteField(fieldname)
                                    Catch ex As EGIS_Exception
                                    End Try
                                End If
                            Next
                        End If

                        'Save any created layers
                        If lPoint IsNot Nothing Then
                            Try
                                If lPoint.FindField(FieldNameRelation) >= 0 Then lPoint.DeleteField(FieldNameRelation)
                                If lPoint.FindField(FieldNameSubrelation) >= 0 Then lPoint.DeleteField(FieldNameSubrelation)
                                If lPoint.FindField(ForeignKeyField) >= 0 Then lPoint.DeleteField(ForeignKeyField)
                                lPoint.SaveAll()
                                lPoint.Dispose()
                            Catch ex As IOException
                                ctPoint = 0
                                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                            Catch ex As EGIS_Exception
                                ctPoint = 0
                                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                            End Try
                            lPoint = Nothing
                        End If
                        If lMultiPoint IsNot Nothing Then
                            Try
                                If lMultiPoint.FindField(FieldNameRelation) >= 0 Then lMultiPoint.DeleteField(FieldNameRelation)
                                If lMultiPoint.FindField(FieldNameSubrelation) >= 0 Then lMultiPoint.DeleteField(FieldNameSubrelation)
                                If lMultiPoint.FindField(ForeignKeyField) >= 0 Then lMultiPoint.DeleteField(ForeignKeyField)
                                lMultiPoint.SaveAll()
                                lMultiPoint.Dispose()
                            Catch ex As IOException
                                ctMultiPoint = 0
                                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                            Catch ex As EGIS_Exception
                                ctMultiPoint = 0
                                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                            End Try
                            lMultiPoint = Nothing
                        End If
                        If lLine IsNot Nothing Then
                            Try
                                If lLine.FindField(FieldNameRelation) >= 0 Then lLine.DeleteField(FieldNameRelation)
                                If lLine.FindField(FieldNameSubrelation) >= 0 Then lLine.DeleteField(FieldNameSubrelation)
                                If lLine.FindField(ForeignKeyField) >= 0 Then lLine.DeleteField(ForeignKeyField)
                                lLine.SaveAll()
                                lLine.Dispose()
                            Catch ex As IOException
                                ctLine = 0
                                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                            Catch ex As EGIS_Exception
                                ctLine = 0
                                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                            End Try
                            lLine = Nothing
                        End If
                        If lPolygon IsNot Nothing Then
                            Try
                                If lPolygon.FindField(FieldNameRelation) >= 0 Then lPolygon.DeleteField(FieldNameRelation)
                                If lPolygon.FindField(FieldNameSubrelation) >= 0 Then lPolygon.DeleteField(FieldNameSubrelation)
                                If lPolygon.FindField(ForeignKeyField) >= 0 Then lPolygon.DeleteField(ForeignKeyField)
                                lPolygon.SaveAll()
                                lPolygon.Dispose()
                            Catch ex As IOException
                                ctPolygon = 0
                                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                            Catch ex As EGIS_Exception
                                ctPolygon = 0
                                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                            End Try
                            lPolygon = Nothing
                        End If
                    Next

                    'Display summary
                    If ctPoint = 0 AndAlso ctMultiPoint = 0 AndAlso ctLine = 0 AndAlso ctPolygon = 0 Then
                        MsgBox(LangStr(_strIdNoShapesToExport), MsgBoxStyle.Information)
                    Else
                        MsgBox(LangStr(_strIdShapeExportSuccessful) & vbCrLf & _
                               ctPoint & LangStr(_strIdCountPoints) & vbCrLf & _
                               ctMultiPoint & LangStr(_strIdCountMultiPoints) & vbCrLf & _
                               ctLine & LangStr(_strIdCountLines) & vbCrLf & _
                               ctPolygon & LangStr(_strIdCountPolygons), _
                               MsgBoxStyle.Information)
                    End If
                End If

                'Unlock the GIS control again
                GisCtrl.Unlock()

                'Remove temp layer
                If GisCtrl.Get(PreviewLayerName) IsNot Nothing Then GisCtrl.Delete(PreviewLayerName)

                Cursor.Current = oldCursor
            End If

            'Redraw the GIS control
            GisCtrl.Update()
        End If

        'Fall back to drag mode
        SetGisMode(GisMode.Drag)

        _selectionStarted = False
        _selectionEndPointSet = False
    End Sub

    Private Sub SumLayersToPolygon(ByVal layerCollection As Collection, ByVal poly As TGIS_Shape)
        Dim fields As New Hashtable
        Dim ll As TGIS_LayerVector

        Dim oldCursor As Cursor = GisCtrl.Cursor
        GisCtrl.Cursor = Cursors.WaitCursor

        'Go through each layer
        For Each lname As String In layerCollection
            'List numeric fields in layer
            ll = GisCtrl.Get(lname)
            For i As Integer = 0 To ll.Fields.Count - 1
                If ll.FieldInfo(i).FieldType = TGIS_FieldType.gisFieldTypeFloat OrElse ll.FieldInfo(i).FieldType = TGIS_FieldType.gisFieldTypeNumber Then
                    If ll.FieldInfo(i).IsUID OrElse IsRelationField(ll.FieldInfo(i).Name) Then Continue For 'Skip internal fields
                    If poly.Layer.FindField(ll.FieldInfo(i).Name) >= 0 AndAlso poly.Layer.FieldInfo(poly.Layer.FindField(ll.FieldInfo(i).Name)).FieldType <> ll.FieldInfo(i).FieldType Then Continue For 'Skip if field exist with mismatching field type
                    If Not fields.ContainsKey(ll.FieldInfo(i).Name) Then fields.Add(ll.FieldInfo(i).Name, ll.FieldInfo(i))
                End If
            Next

            'List any numeric joined fields
            If ll.JoinNET IsNot Nothing Then
                Dim ft As TGIS_FieldType
                For Each col As DataColumn In CType(ll.JoinNET, DataTable).Columns
                    If IsRelationField(col.Caption) Then Continue For 'Skip internal fields
                    GetFieldType(ll, col.Caption, ft)
                    If poly.Layer.FindField(col.Caption) >= 0 AndAlso poly.Layer.FieldInfo(poly.Layer.FindField(col.Caption)).FieldType <> ft Then Continue For 'Skip if field exist with mismatching field type
                    If ft = TGIS_FieldType.gisFieldTypeFloat OrElse ft = TGIS_FieldType.gisFieldTypeNumber Then
                        Dim fi As New TGIS_FieldInfo
                        fi.Name = col.Caption
                        fi.FieldType = ft
                        fi.Width = 38
                        fi.Decimal = 0
                        If Not fields.ContainsKey(col.Caption) Then fields.Add(col.Caption, fi)
                    End If
                Next
            End If
        Next

        'Add all numeric fields to polygon layer
        For Each fname As String In fields.Keys
            Dim fi As TGIS_FieldInfo = CType(fields(fname), TGIS_FieldInfo)

            'Check if field exist
            If poly.Layer.FindField(fname) > 0 Then
                poly.SetField(fname, 0) 'Zero any value
                Continue For
            End If

            'Add field
            poly.Layer.AddField(fname, fi.FieldType, fi.Width, fi.Decimal)
        Next

        'Sum up all attributes from each layer contained within the polygon
        For Each lname As String In layerCollection
            ll = GisCtrl.Get(lname)
            Dim shp As TGIS_Shape = ll.FindFirst()
            Do Until shp Is Nothing
                If shp.Intersect(poly) Then
                    'Add any values to polygon
                    For Each fname As String In fields.Keys
                        Dim val As Object = Nothing
                        If ll.FindField(fname) >= 0 Then
                            'Make sure field type match (different layers)
                            If CType(fields(fname), TGIS_FieldInfo).FieldType <> ll.FieldInfo(ll.FindField(fname)).FieldType Then Continue For
                            val = shp.GetField(fname) 'Layer field
                        ElseIf ll.JoinNET IsNot Nothing AndAlso CType(ll.JoinNET, DataTable).Columns.Contains(fname) Then
                            'Make sure field type match (different layers)
                            Dim ft As TGIS_FieldType
                            GetFieldType(ll, fname, ft)
                            If CType(fields(fname), TGIS_FieldInfo).FieldType <> ft Then Continue For

                            'Joined field - find matching record
                            Dim rt As RelationType
                            Dim jv As String = vbNullString
                            GetLayerRelationType(ll.Name, rt, jv)
                            For Each row As DataRow In CType(ll.JoinNET, DataTable).Rows
                                If row.Item(ForeignKeyField) = shp.GetField(GetJoinPrimary(jv)) Then
                                    val = row.Item(fname)
                                    Exit For
                                End If
                            Next
                        End If
                        If val IsNot Nothing AndAlso Not IsDBNull(val) Then poly.SetField(fname, poly.GetField(fname) + val)
                    Next
                End If
                shp = ll.FindNext()
            Loop
        Next

        'Restore cursor
        GisCtrl.Cursor = oldCursor

        'Show attributes
        Dim dlg As New AttributeDialog(poly)
        If dlg.ShowDialog() = DialogResult.Cancel Then poly.Layer.RevertAll() 'Revert if canceled. AttributeDialog will save any changes.
    End Sub

    Private Sub SelectShapes(ByVal filter As Boolean, ByVal state As SelectState)
        Dim ll As TGIS_LayerVector
        Dim shp As TGIS_Shape

        Dim oldCursor As Cursor = GisCtrl.Cursor
        GisCtrl.Cursor = Cursors.WaitCursor

        'Select/deselect shapes in current layer
        ll = TryCast(GetActiveLayer(), TGIS_LayerVector)
        If ll IsNot Nothing Then
            If filter = False AndAlso state = SelectState.Deselected Then
                'Use optimized function to deselect all shapes within layer when possible
                ll.DeselectAll()
            Else
                If filter Then
                    'Select any listed shapes
                    If _cachedMode Then
                        'Get UID's from list records
                        For Each rec As ReportRecord In ReportData.Records
                            shp = ll.GetShape(rec.Item(0).Value)
                            If shp IsNot Nothing Then SetShapeSelectedState(shp, state)
                        Next
                    Else
                        'Get UID's from virtual list
                        For Each rec As ReportRecord In ReportData.Records
                            If rec.Index < _shapeList.Count Then
                                shp = ll.GetShape(rec.Index)
                                If shp IsNot Nothing Then SetShapeSelectedState(shp, state)
                            End If
                        Next
                    End If
                Else
                    'Select all shapes
                    shp = ll.FindFirst()
                    Do While shp IsNot Nothing
                        SetShapeSelectedState(shp, state)
                        shp = ll.FindNext()
                    Loop
                End If

                GisCtrl.Update()
            End If
        End If

        GisCtrl.Cursor = oldCursor
    End Sub

    Private Shared Sub SetShapeSelectedState(ByVal shp As TGIS_Shape, ByVal state As SelectState)
        Select Case state
            Case SelectState.Selected
                shp.MakeEditable().IsSelected = True

            Case SelectState.Deselected
                shp.MakeEditable().IsSelected = False

            Case SelectState.Inverted
                shp.MakeEditable().IsSelected = Not shp.IsSelected
        End Select
    End Sub

    Private Sub UnionShapes()
        'Union selected shapes
        Dim ll As TGIS_LayerVector = TryCast(GetActiveLayer(), TGIS_LayerVector)
        Dim shp As TGIS_Shape, shpNew As TGIS_Shape = Nothing, tmp As TGIS_Shape = Nothing
        Dim lastUid As Integer

        If TypeOf ll Is TGIS_LayerSqlAdo AndAlso ll.GetSelectedCount() >= 2 Then 'We need at least two shapes
            'Display a warning if layer is connected to a suite
            If GetLayerRelationType(ll.Name) <> RelationType.Undefined Then
                If MsgBox(LangStr(_strIdSplitUnionWarning1) & vbCrLf & LangStr(_strIdSplitUnionWarning2), MsgBoxStyle.Exclamation Or MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                    Exit Sub 'Don't union
                End If
            End If

            lastUid = ll.GetLastUid()
            For i As Integer = 1 To lastUid
                shp = ll.GetShape(i)
                If shp IsNot Nothing AndAlso shp.IsSelected Then
                    If tmp Is Nothing Then
                        tmp = shp
                    Else
                        'Generate a new shape
                        Dim tpl As New TGIS_Topology
                        shpNew = tpl.Union(shp, tmp)

                        'Remove original shapes
                        shp.Delete()
                        tmp.Delete()
                        tmp = shpNew
                    End If
                End If
            Next

            'Add our union to layer
            If shpNew IsNot Nothing Then ll.AddShape(shpNew)
            ll.SaveAll()
            GisCtrl.Update()
        End If
    End Sub

    Friend Shared Function GetFilterConditionString(ByVal lv As TGIS_LayerVector, ByVal conditionsHash As Hashtable) As String
        Dim sql As String = vbNullString
        Dim de As DictionaryEntry
        Dim encloser As String
        Dim key As String
        Dim condition As ConditionSet
        Dim type As TGIS_FieldType
        Dim fieldName As String

        'Put together conditions for sql query
        If conditionsHash.Keys.Count > 0 Then
            For Each de In conditionsHash
                If sql <> vbNullString Then sql += " AND "
                sql += "("

                key = GetFieldColumnName(de.Key, lv)
                condition = CType(de.Value, ConditionSet)

                'Determine field type (use de.Key here)
                If Not GetFieldType(lv, de.Key, type) Then Continue For 'Skip this field if type cannot be checked
                If type = TGIS_FieldType.gisFieldTypeString Then
                    encloser = "'"
                Else
                    encloser = vbNullString
                End If

                'Add conditions
                'First (GIS_UID is named UID in sql layers)
                If key = FieldNameGisUid AndAlso TypeOf lv Is TGIS_LayerSqlAbstract Then
                    fieldName = ColumnNameUid
                Else
                    fieldName = key
                End If
                If condition.Condition1 = 6 OrElse condition.Condition1 = 7 Then
                    'IS NULL/IS NOT NULL
                    sql += fieldName & " " & Operators(condition.Condition1)
                ElseIf condition.Condition1 >= 0 Then
                    sql += fieldName & " " & Operators(condition.Condition1) & " " & encloser & condition.Value1 & encloser
                End If

                'Second (GIS_UID is named UID in sql layers)
                If key = FieldNameGisUid AndAlso TypeOf lv Is TGIS_LayerSqlAbstract Then
                    fieldName = ColumnNameUid
                Else
                    fieldName = key
                End If
                If condition.CommonOperator <> vbNullString Then
                    If condition.Condition2 = 6 OrElse condition.Condition2 = 7 Then
                        'IS NULL/IS NOT NULL
                        sql += " " & condition.CommonOperator & " " & fieldName & " " & Operators(condition.Condition2)
                    ElseIf condition.Condition2 >= 0 Then
                        sql += " " & condition.CommonOperator & " " & fieldName & " " & Operators(condition.Condition2) & " " & encloser & condition.Value2 & encloser
                    End If
                End If

                sql += ")"
            Next
        End If

        Return sql
    End Function

    Friend Shared Function GetFieldColumnName(ByVal key As String, ByVal ll As TGIS_LayerVector, Optional ByVal addBrackets As Boolean = True) As String
        Dim found As Boolean
        Dim i As Integer
        Dim viewName As String = vbNullString
        Dim cmd As New OleDbCommand("", Conn)
        Dim reader As OleDbDataReader

        'Find language string id if column name is mapped
        If ll.FindField(key) = -1 AndAlso ll.JoinNET IsNot Nothing Then
            found = False
            For i = 0 To CType(ll.JoinNET, DataTable).Columns.Count - 1
                If CType(ll.JoinNET, DataTable).Columns(i).Caption = key Then
                    found = True
                    Exit For
                End If
            Next

            'Get columns from db, match LangStr
            If found Then
                cmd.CommandText = "SELECT join_view FROM gis_layers WHERE name = '" & ll.Name & "'"
                reader = cmd.ExecuteReader()
                reader.Read()
                viewName = reader.GetString(0)
                reader.Close()

                cmd.CommandText = "SELECT TOP 0 * FROM " & viewName
                reader = cmd.ExecuteReader()
                For i = 0 To reader.FieldCount - 1
                    If IsNumeric(reader.GetName(i)) AndAlso LangStr(CInt(reader.GetName(i))) = key Then
                        If addBrackets Then
                            key = "[" & reader.GetName(i) & "]"
                        Else
                            key = reader.GetName(i)
                        End If
                        Exit For
                    End If
                Next
                reader.Close()
            End If
        End If

        Return key
    End Function

    Private Sub PrepareHierarchyMenuStrip()
        Dim i As Integer
        Dim ll As TGIS_LayerAbstract
        Dim hierarchy As String = GisCtrl.Hierarchy.GetHierarchy()
        Dim item As ToolStripItem

        'Add layers caption to 'Add layer' hierarchy menu
        'Remove any existing items in menu strip
        AddHierarchyLayerToolStripMenuItem.DropDownItems.Clear()

        'Disable in case there are no unused layers
        AddUnusedLayersToolStripMenuItem.Enabled = False

        'Loop through all layers in viewer
        For i = 0 To GisCtrl.Items.Count - 1
            ll = CType(GisCtrl.Items(i), TGIS_LayerAbstract)

            'Check if layer is already added to hierarchy
            If Not hierarchy.Contains(ll.Name) Then
                item = AddHierarchyLayerToolStripMenuItem.DropDownItems.Add(ll.Caption, Nothing, AddressOf AddHierarchyLayerEventHandler)
                item.Name = ll.Name 'We need to store the name of the layer
                AddUnusedLayersToolStripMenuItem.Enabled = True 'Unused layer exist so enable this feature
            End If
        Next

        'If there are no layers to add we can disable this menu
        If AddHierarchyLayerToolStripMenuItem.DropDownItems.Count = 0 Then AddHierarchyLayerToolStripMenuItem.Enabled = False
    End Sub

    Private Sub DockRightOf(ByVal barToDock As CommandBar, ByVal barOnLeft As CommandBar)
        Dim Left As Integer
        Dim Top As Integer
        Dim Right As Integer
        Dim Bottom As Integer

        CommandBars.RecalcLayout()
        barOnLeft.GetWindowRect(Left, Top, Right, Bottom)

        CommandBars.DockToolBar(barToDock, Right, (Bottom + Top) / 2, barOnLeft.Position)
    End Sub

    Private Sub AdjustSize()
        Dim left As Integer
        Dim top As Integer
        Dim right As Integer
        Dim bottom As Integer

        'We shouldn't do this before component is initialized
        If _componentInitialized Then
            CommandBars.GetClientRect(left, top, right, bottom)
            PanelMain.SetBounds(left, top, right - left, bottom - top)
            GisCtrl.SetBounds(0, 0, right - left, bottom - top) 'Top, left is relative to PanelMain
            ControlScale.SetBounds(right - left - _controlScaleWidth, bottom - top - _controlScaleHeight, _controlScaleWidth, _controlScaleHeight)
            ControlNorthArrow.SetBounds(right - left - _controlNorthArrowWidth, 0, _controlNorthArrowWidth, _controlNorthArrowHeight)

            If PanelCopyright.Visible Then
                ControlScale.Location = New System.Drawing.Point(ControlScale.Location.X, ControlScale.Location.Y - PanelCopyright.Height) 'Move scale above copyright
                PanelCopyright.Left = right - PanelCopyright.Width
                PanelCopyright.Top = bottom - PanelCopyright.Height
            End If

            ReportData.Left = 0
            ReportData.Top = 0
            ReportData.Width = PanelData.Width
            ReportData.Height = PanelData.Height
            ReportData.AutoColumnSizing = False

            DataProgressBar.Left = 0
            DataProgressBar.Top = PanelData.Height - DataProgressBar.Height
            DataProgressBar.Width = PanelData.Width
            DataProgressBar.Height = 20
        End If
    End Sub

    Private Sub UpdateToolbars()
        Dim mustReset As Boolean = False
        Dim regKey As RegistryKey
        Dim ver As System.Version
        Dim curLang As String = vbNullString
        Dim lastVer As String, lastLang As String

        ver = System.Reflection.Assembly.GetCallingAssembly().GetName().Version

        'Get last version and language from registry
        regKey = Registry.CurrentUser.OpenSubKey(RegKeyGis, False)
        If regKey IsNot Nothing Then 'Setting not yet saved so don't need to update
            lastVer = regKey.GetValue(RegValLastVersion, vbNullString)
            lastLang = regKey.GetValue(RegValLastLanguage, vbNullString)
            regKey.Close()

            'Get current language from registry
            regKey = Registry.CurrentUser.OpenSubKey(RegKeyLanguage, False)
            If regKey IsNot Nothing Then
                curLang = regKey.GetValue(RegValLangSet)
            End If

            'Compare version
            If lastVer IsNot Nothing Then
                Dim verReg = New Version(lastVer)
                If _toolbarVersion > verReg Then mustReset = True
            End If

            'Compare language
            If lastLang <> vbNullString AndAlso lastLang <> curLang Then mustReset = True

            'Write current version and language to registry
            regKey = Registry.CurrentUser.OpenSubKey(RegKeyGis, True)
            If regKey IsNot Nothing Then
                regKey.SetValue(RegValLastVersion, ver.ToString())
                regKey.SetValue(RegValLastLanguage, curLang)
                regKey.Close()
            End If

            'Reset toolbar & save new version to regisitry (if updated)
            If mustReset Then
                Try 'Try-Catch in case keys doesn't exist
                    Registry.CurrentUser.DeleteSubKey(RegKeyGis + "\" & RegSectionCommandBarLayout)
                    Registry.CurrentUser.DeleteSubKey(RegKeyGis + "\" & RegSectionCommandBarLayout & "-Bar0")
                    Registry.CurrentUser.DeleteSubKey(RegKeyGis + "\" & RegSectionCommandBarLayout & "-Bar1")
                    Registry.CurrentUser.DeleteSubKey(RegKeyGis + "\" & RegSectionCommandBarLayout & "-Bar2")
                    Registry.CurrentUser.DeleteSubKey(RegKeyGis + "\" & RegSectionCommandBarLayout & "-Bar3")
                    Registry.CurrentUser.DeleteSubKey(RegKeyGis + "\" & RegSectionCommandBarLayout & "-Controls")
                    Registry.CurrentUser.DeleteSubKey(RegKeyGis + "\" & RegSectionCommandBarLayout & "-DockBar0")
                    Registry.CurrentUser.DeleteSubKey(RegKeyGis + "\" & RegSectionCommandBarLayout & "-DockBar1")
                    Registry.CurrentUser.DeleteSubKey(RegKeyGis + "\" & RegSectionCommandBarLayout & "-DockBar2")
                    Registry.CurrentUser.DeleteSubKey(RegKeyGis + "\" & RegSectionCommandBarLayout & "-DockBar3")
                    Registry.CurrentUser.DeleteSubKey(RegKeyGis + "\" & RegSectionCommandBarLayout & "-Options")
                    Registry.CurrentUser.DeleteSubKey(RegKeyGis + "\" & RegSectionCommandBarLayout & "-Summary")
                Catch ex As ArgumentException
                End Try

                'Notify user
                MsgBox(LangStr(_strIdToolbarUpdated), MsgBoxStyle.Information)
            End If
        End If
    End Sub

    Private Sub LoadIcons()
        Dim hInstance As IntPtr = Marshal.GetHINSTANCE(System.Reflection.Assembly.GetExecutingAssembly.GetModules()(0))
        Dim hIcon As IntPtr

        Const IconFile As String = "HMSIcons.icl"

        'Layer (new, import)
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 73)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.Layer, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If

        'Zoom to full extent
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 79)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.FullExtent, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If

        'Zoom to previous extent
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 60)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.PreviousExtent, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If

        'Navigate to
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 62)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.NavigateTo, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If

        'Zoom
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 71)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.Zoom, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If

        'Drag
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 70)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.Drag, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If

        'Select
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 68)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.SelectShape, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If

        'View shape info
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 20)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.Info, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If

        'New shape
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 75)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.NewShape, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If

        'Polygon
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 80)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.NewShapePolygon, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If

        'Line
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 81)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.NewShapeLine, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If

        'Point
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 82)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.NewShapePoint, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If

        'Multipoint
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 83)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.NewShapeMultiPoint, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If

        'Edit shape
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 7)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.EditShape, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If

        'Remove shape
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 2)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.RemoveShape, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If

        'Measure
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 69)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.Measure, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If

        'Print
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 36)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.Print, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If

        'Update map
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 43)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.UpdateMap, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If

        'LandValue tools
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 84)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.LandValueTools, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If

        'Misc. tools
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 47)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.MiscTools, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If

        'Apply
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 29)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.Apply, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If

        'Revert
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 2)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.Revert, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If

        'Undo
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 42)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.Undo, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If

        'Redo
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 33)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.Redo, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If

        'Apply relation
        hIcon = NativeMethods.ExtractIcon(hInstance, IconFile, 61)
        If Not hIcon.Equals(IntPtr.Zero) Then
            ImageManager.Icons.AddIcon(hIcon, ToolbarId.ApplyRelation, XtremeCommandBars.XTPImageState.xtpImageNormal)
            NativeMethods.DestroyIcon(hIcon)
        End If
    End Sub

    Private Shared Function AddButton(ByVal controls As CommandBarControls, ByVal controlType As XTPControlType, ByVal id As Integer, ByVal caption As String, Optional ByVal beginGroup As Boolean = False) As CommandBarControl
        Dim Control As CommandBarControl = controls.Add(controlType, id, caption, -1, False)
        Control.BeginGroup = beginGroup
        Return Control
    End Function

    Private Shared Function InchToPixels(ByVal value As Double, ByVal dpi As Integer) As Integer
        Return Convert.ToInt32(Math.Round(value * dpi))
    End Function

    Private Shared Sub FrameRect(ByVal canvas As TGIS_GDI, ByVal rect As Rectangle, ByVal width As Integer)
        canvas.Brush.Color = Color.Black
        canvas.Brush.Style = TGIS_BrushStyle.gisBsSolid

        'Draw rectangle with specified width around given rectangle
        canvas.Rectangle(rect.X - width, rect.Y - width, rect.X + rect.Width + width, rect.Y) 'Top
        canvas.Rectangle(rect.X - width, rect.Y - width, rect.X, rect.Y + rect.Height + width) 'Left
        canvas.Rectangle(rect.X - width, rect.Y + rect.Height, rect.X + rect.Width + width, rect.Y + rect.Height + width) 'Bottom
        canvas.Rectangle(rect.X + rect.Width, rect.Y - width, rect.X + rect.Width + width, rect.Y + rect.Height + width) 'Right
    End Sub

    Friend Sub AddShpToAttributeTable(ByVal shp As TGIS_Shape)
        Dim i As Integer
        Dim record As ReportRecord

        If shp IsNot Nothing Then
            If _cachedMode Then
                'Add new record
                record = ReportData.Records.Add()
                If Not TypeOf shp.Layer Is TGIS_LayerSqlAbstract Then
                    'If this is a file layer we need to add shape UId manually
                    record.AddItem(shp.Uid)
                End If

                'Loop through fields in layer
                For i = 0 To shp.Layer.Fields.Count - 1
                    If shp.Layer.FieldInfo(i).FieldType = TGIS_FieldType.gisFieldTypeDate Then
                        record.AddItem(Microsoft.VisualBasic.FormatDateTime(Convert.ToDateTime(shp.GetField(shp.Layer.FieldInfo(i).Name), CultureInfo.CurrentCulture), DateFormat.GeneralDate))
                    Else
                        record.AddItem(CStr(shp.GetField(shp.Layer.FieldInfo(i).Name)))
                    End If
                Next
                ReportData.Populate()

                'Update status bar
                _shapeCountPane.Text = ReportData.Rows.Count & LangStr(_strIdRecords)
            Else
                'Add shape to virtual list
                _shapeList.Add(shp.Uid)
                ReportData.SetVirtualMode(_shapeList.Count)

                'Add template record if this is first record
                If _shapeList.Count = 1 Then
                    record = ReportData.Records(0)
                    For i = 0 To ReportData.Columns.Count
                        record.AddItem("")
                    Next
                End If

                ReportData.Populate()

                'Update status bar
                _shapeCountPane.Text = _shapeList.Count & LangStr(_strIdRecords)
            End If
        End If
    End Sub

    Friend Shared Function GetPreviewLayer() As TGIS_LayerVector
        Dim ll As TGIS_LayerVector

        If GisControlView.Instance.GisCtrl.Get(PreviewLayerName) Is Nothing Then
            'Create new layer
            ll = New TGIS_LayerVector
            ll.CS = GisControlView.Instance.GisCtrl.CS
            ll.Name = PreviewLayerName
            ll.HideFromLegend = True
            GisControlView.Instance.GisCtrl.Add(ll)
        Else
            'Use existing layer
            ll = GisControlView.Instance.GisCtrl.Get(PreviewLayerName)
        End If

        Return ll
    End Function

    Private Function GetActiveLayer() As TGIS_LayerAbstract
        Dim ll As TGIS_LayerAbstract = Nothing, lt As TGIS_LayerAbstract

        'Return active layer or active sublayer
        If String.IsNullOrEmpty(_activeSubLayerName) Then
            ll = GisCtrl.Get(_activeLayerName)
        Else 'Sublayer
            lt = GisCtrl.Get(_activeLayerName)
            For i As Integer = 0 To lt.SubLayers.Count - 1
                If lt.SubLayers(i).Name = _activeSubLayerName Then
                    ll = lt.SubLayers(i)
                    Exit For
                End If
            Next
        End If

        Return ll
    End Function

    Private Function GetReportDataSelectedShapeUid() As Integer
        If ReportData.SelectedRows.Count > 0 Then
            If _cachedMode Then
                'Shape id stored in record
                Return ReportData.SelectedRows(0).Record.Item(0).Value
            Else
                'Shape id stored in list
                If _shapeList.Count > ReportData.SelectedRows(0).Index Then Return _shapeList(ReportData.SelectedRows(0).Index)
            End If
        End If

        Return 0 'Shape id not found
    End Function

    Private Function SaveProjectToDatabase() As Boolean
        Dim cmd As New OleDbCommand("", Conn)

        'Save project file to database
        Try
            Dim strrOutput As New IO.StreamReader(_projectPath)
            Dim data As String = strrOutput.ReadToEnd()
            strrOutput.Close()

            'Delete old database entry for current user
            cmd.CommandText = "DELETE FROM gis_project_files WHERE [user] LIKE '" & Environment.UserName() & "'"
            cmd.ExecuteNonQuery()

            'Insert new project file
            cmd.CommandText = "INSERT INTO gis_project_files ([project_file], [user], [updated]) VALUES(?,'" & Environment.UserName() & "',GetDate())"
            Dim p As New OleDbParameter("@project_file", OleDbType.VarChar, data.Length, ParameterDirection.Input, False, 0, 0, Nothing, DataRowVersion.Current, data)
            cmd.Parameters.Add(p)
            cmd.ExecuteNonQuery()
        Catch ex As Exception
        End Try
    End Function

    Private Function OpenProject(Optional ByVal strict As Boolean = False, Optional ByVal silent As Boolean = False) As Boolean
        Dim cmd As New OleDbCommand("", Conn)
        Dim reader As OleDbDataReader
        Dim layers As New Hashtable
        Dim params As TGIS_ParamsSectionVector
        Dim i As Integer, j As Integer, ct As Integer
        Dim ll As TGIS_LayerAbstract
        Dim lv As TGIS_LayerVector
        Dim copyUser As String = vbNullString

        'Check if user has a project file, otherwise copy from other user if possible
        Try
            cmd.CommandText = "SELECT CASE WHEN NOT EXISTS (SELECT * FROM gis_project_files WHERE [user] = '" & Environment.UserName() & _
                              "') AND EXISTS (SELECT * FROM gis_project_files) THEN (SELECT COUNT(*) FROM gis_project_files) ELSE 0 END"
            reader = cmd.ExecuteReader(CommandBehavior.SingleRow)
            reader.Read()
            ct = reader.GetInt32(0)
            reader.Close()
            If ct > 0 Then
                If ct > 1 Then
                    'Let user choose which user to copy from
                    Dim dlg As New ProjectChoiceDialog
                    If dlg.ShowDialog() = DialogResult.OK Then copyUser = dlg.User
                Else
                    'Only one project file exist, get it
                    cmd.CommandText = "SELECT [user] FROM gis_project_files"
                    reader = cmd.ExecuteReader(CommandBehavior.SingleRow)
                    If reader.Read() Then copyUser = reader.GetString(0)
                    reader.Close()
                End If
            End If

            If copyUser <> vbNullString Then
                cmd.CommandText = "INSERT INTO gis_project_files (project_file, [user], updated) SELECT project_file, '" & Environment.UserName() & _
                                  "', CURRENT_TIMESTAMP FROM gis_project_files WHERE [user] = '" & copyUser & "'"
                cmd.ExecuteNonQuery()
            End If
        Catch ex As Exception
        End Try

        'Copy project file, if it exists, from database
        Try
            cmd.CommandText = "SELECT project_file FROM gis_project_files WHERE [user] = '" & Environment.UserName() & "'"
            reader = cmd.ExecuteReader(CommandBehavior.SingleRow)
            If reader.HasRows Then
                reader.Read()
                If Not reader.IsDBNull(0) Then
                    Dim projectFileData As String = reader.GetString(0)

                    If Not projectFileData Is Nothing Then
                        'Write data into project file
                        Dim strwOutput As New IO.StreamWriter(_projectPath, False)
                        strwOutput.Write(projectFileData)
                        strwOutput.Close()
                    End If
                End If
            End If
            reader.Close()
        Catch ex As Exception
        End Try


        'Update connection string in file
        Try
            Dim sr As New StreamReader(_projectPath)
            Dim ini As String = sr.ReadToEnd()
            sr.Close()

            Dim start As Integer = 0, last As Integer = 0
            Dim tmp As String = vbNullString
            Do
                start = ini.IndexOf("\\nADO=", start, StringComparison.OrdinalIgnoreCase)
                If start < 0 Then Exit Do

                tmp += ini.Substring(last, start - last)
                tmp += "\\nADO=" & ConnStr
                start += 1 : If start >= ini.Length Then Exit Do
                last = ini.IndexOf("\\n", start, StringComparison.OrdinalIgnoreCase)
            Loop
            tmp += ini.Substring(last, ini.Length - last)
            ini = tmp

            Dim utf8EmitBOM As New System.Text.UTF8Encoding(True)
            Dim writer As New StreamWriter(_projectPath, False, utf8EmitBOM)
            writer.Write(utf8EmitBOM.GetPreamble()) 'Write UTF-8 BOM
            writer.Write(ini)
            writer.Close()
        Catch ex As FileNotFoundException
            'In case project file doesn't exist yet
        End Try

        'Load project
        Try
            GisCtrl.Open(_projectPath, True)
        Catch ex As EGIS_Exception
            If Not strict Then
                'Notify user of missing layers, open project non strictly
                If Not silent Then MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                Try
                    GisCtrl.Open(_projectPath, False)
                Catch ex1 As EGIS_Exception
                    Return False
                End Try
            Else
                'Failure
                Return False
            End If
        End Try

        'Fix line width in layers - use twips instead of pixels (#4432)
        For i = 0 To GisCtrl.Items.Count - 1
            lv = TryCast(GisCtrl.Items(i), TGIS_LayerVector)
            If lv IsNot Nothing Then
                For j = 0 To lv.ParamsList.Count - 1
                    params = DirectCast(lv.ParamsList(j), TGIS_ParamsSectionVector)
                    If params.Area.OutlineWidth < 0 Then
                        params.Area.OutlineWidth *= -15
                    End If
                    If params.Line.OutlineWidth < 0 Then
                        params.Line.OutlineWidth *= -15
                    End If
                    If params.Line.Width < 0 Then
                        params.Line.Width *= -15
                    End If
                Next
            End If
        Next

        'Make sure all layer captions and epsg are stored in the db as well
        cmd.CommandText = "SELECT name, ISNULL(caption,''), ISNULL(epsg,0) FROM gis_layers"
        reader = cmd.ExecuteReader()
        While reader.Read()
            layers.Add(reader.GetString(0), New ValueDescriptionPair(reader.GetInt32(2), reader.GetString(1)))
        End While
        reader.Close()

        For i = 0 To GisCtrl.Items.Count - 1
            ll = CType(GisCtrl.Items(i), TGIS_LayerAbstract)
            If Not layers.ContainsKey(ll.Name) Then 'Check if layer record exist
                'Add layer to index table
                cmd.CommandText = "INSERT INTO gis_layers (name, caption, epsg, type, join_view) VALUES('" & ll.Name & "','" & ll.Caption.Replace("'", "''") & "'," & ll.CS.EPSG & ",0,'')"
                cmd.ExecuteNonQuery()
            ElseIf CType(layers(ll.Name), ValueDescriptionPair).Description = vbNullString OrElse CType(layers(ll.Name), ValueDescriptionPair).Value <> ll.CS.EPSG Then 'Check if record has a caption and epsg match
                'Update record
                cmd.CommandText = "UPDATE gis_layers SET caption = '" & ll.Caption.Replace("'", "''") & "', epsg = " & ll.CS.EPSG & " WHERE name = '" & ll.Name & "'"
                cmd.ExecuteNonQuery()
            End If
        Next

        'If viewer cs is missing in project file, try getting it from db
        If GisCtrl.CS.EPSG = 0 Then
            cmd.CommandText = "SELECT epsg FROM gis_layers WHERE name = '" & ViewerWndName & "'"
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                GisCtrl.CS = TGIS_CSFactory.ByEPSG(reader.GetInt32(0))
            End If
            reader.Close()
        End If

        'Make sure viewer cs is stored in db
        cmd.CommandText = "IF EXISTS(SELECT 1 FROM gis_layers WHERE name = '" & ViewerWndName & "') " & _
                          "UPDATE gis_layers SET epsg = " & GisCtrl.CS.EPSG & " WHERE name = '" & ViewerWndName & "' " & _
                          "ELSE " & _
                          "INSERT INTO gis_layers (name, caption, epsg) VALUES('" & ViewerWndName & "', '', " & GisCtrl.CS.EPSG & ")"
        cmd.ExecuteNonQuery()

        'Let user add any layers not contained in the project file
        If IsThereUnusedLayers() Then
            Dim msgdlg As New MessageBoxCheck
            If msgdlg.ShowDialog(LangStr(_strIdAddUnusedLayers), vbNullString, MessageBoxIcon.Question, "ViewUnusedLayers") = DialogResult.Yes Then
                Dim dlg As New UnusedLayersDialog
                dlg.ShowDialog()
            End If
        End If

        Return True
    End Function

    'Update a single row in ReportData for a shape 20110221 J�
    'This not to need to update the whole ReportData just the information about the current shape being set in a relation to something(property or stand etc..)
    '#1986 #1696
    Private Sub UpdateListFieldData(ByVal shp As TGIS_Shape)
        Dim row As Integer
        Dim j As Integer
        Dim ll As TGIS_LayerAbstract
        Dim fieldCount As Integer
        Dim rowCount As Integer
        Dim cmd As New OleDbCommand("", Conn)
        Dim da As New OleDbDataAdapter(cmd)
        Dim rt As RelationType
        Dim jv As String = vbNullString

        'We cannot run this sub parallel to ListFieldData #2652
        If Not _listFieldsRunning Then
            If _cachedMode Then
                ll = GetActiveLayer()

                'Is this a database layer?
                If TypeOf ll Is TGIS_LayerSqlAdo Then
                    'We need to load data directly from db to get all joined fields for the shape
                    GetLayerRelationType(ll.Name, rt, jv)

                    'Copy one row in the data table for the selected shape uid, also include any joined data
                    cmd.CommandText = "SELECT * FROM " & GisTablePrefix & ll.Name & "_FEA f"
                    If rt <> RelationType.Undefined AndAlso jv <> vbNullString Then
                        cmd.CommandText += " LEFT JOIN " & jv & " ON " & jv & "." & ForeignKeyField & " = f." & GetJoinPrimary(jv)
                    End If

                    'Only select the uid of the shape
                    cmd.CommandText += " WHERE UID=" & shp.Uid

                    _curDataTable = New DataTable
                    _curDataTable.Locale = CultureInfo.InvariantCulture
                    da.Fill(_curDataTable)
                    _curDataTableIndex = 0

                    'Get field count
                    fieldCount = _curDataTable.Columns.Count
                    rowCount = _curDataTable.Rows.Count
                ElseIf TypeOf ll Is TGIS_LayerSHP Then  'It is a shapefile layer
                    fieldCount = shp.Layer.Fields.Count
                    rowCount = shp.Layer.Items.Count
                End If

                'Locate the shape in the ReportData
                For row = 0 To ReportData.Rows.Count - 1
                    If ReportData.Rows(row).Record.Item(0).Value = shp.Uid Then
                        'Update the information about the shape in the ReportData
                        'List the field data of the shape
                        If TypeOf ll Is TGIS_LayerSqlAdo Then
                            If _curDataTable IsNot Nothing AndAlso fieldCount > 0 Then
                                'Use data table
                                For j = 0 To _curDataTable.Columns.Count - 1
                                    If j >= ReportData.Rows(row).Record.ItemCount Then Exit For 'In case row doesn't contain joined columns (newly created shapes)
                                    ReportData.Rows(row).Record.Item(j).Value = _curDataTable.Rows(0).ItemArray(j)
                                Next
                                ReportData.Update()
                                Exit For
                            End If
                        ElseIf TypeOf ll Is TGIS_LayerSHP Then
                            For j = 0 To fieldCount - 1
                                ReportData.Rows(row).Record.Item(j + 1).Value = shp.GetField(shp.Layer.FieldInfo(j).Name)
                            Next
                            ReportData.Update()
                            Exit For
                        End If
                    End If
                Next

                ReportData.Populate()
                _curDataTable = Nothing
            Else
                ReportData.Populate()
            End If
        End If
    End Sub

    Private Sub SetupSnapLayer()
        If _snapLayerGallery.SelectedItem.Id <= GisCtrl.Items.Count - 1 Then
            _lastSnapLayer = GisCtrl.Items(_snapLayerGallery.SelectedItem.Id) 'We need to keep track of this as the editor will assign a default snap layer if no layer is chosen when editing
        Else
            _lastSnapLayer = Nothing
        End If
        GisCtrl.Editor.SnapLayer = _lastSnapLayer
    End Sub

    Private Sub ListSnapLayers(Optional ByVal layerAdd As TGIS_LayerAbstract = Nothing, Optional ByVal layerRemove As TGIS_LayerAbstract = Nothing)
        Dim i As Integer, j As Integer
        Dim ll As TGIS_LayerVector
        Dim item As CommandBarGalleryItem, firstItem As CommandBarGalleryItem

        _snapLayerItems.DeleteAll()

        'No snapping option
        firstItem = _snapLayerItems.AddItem(Integer.MaxValue, LangStr(_strIdNoSnapping)) 'We cannot use a negative value here thus Integer.MaxValue is used instead

        If layerAdd IsNot Nothing AndAlso TypeOf layerAdd Is TGIS_LayerVector Then _snapLayerItems.AddItem(GisCtrl.Items.Count, layerAdd.Caption) 'Add any layer about to be created

        'List any vector layers (in reverse order to match legend)
        For i = GisCtrl.Items.Count - 1 To 0 Step -1
            ll = TryCast(GisCtrl.Items(i), TGIS_LayerVector)
            If ll IsNot Nothing AndAlso ll IsNot layerRemove Then 'Don't add layer if it's about to be removed
                item = _snapLayerItems.AddItem(i, ll.Caption)
            End If
        Next

        'Restore last selection (if any)
        If _lastSnapLayer IsNot Nothing Then
            For i = 0 To GisCtrl.Items.Count - 1
                'Find snap layer in viewer collection
                ll = TryCast(GisCtrl.Items(i), TGIS_LayerVector)
                If _lastSnapLayer Is ll AndAlso ll IsNot layerRemove Then
                    'Find the item representing this layer
                    For j = 0 To _snapLayerItems.Count - 1
                        If i = _snapLayerItems(j).Id Then
                            _snapLayerGallery.SelectedItem = _snapLayerItems(j)
                            Exit For
                        End If
                    Next
                    Exit For
                End If
            Next
        End If

        'Otherwise pick first item as default
        If _snapLayerGallery.SelectedItem Is Nothing Then _snapLayerGallery.SelectedItem = firstItem

        'Redraw toolbar
        CommandBars.RecalcLayout()
    End Sub

    Private Sub ToggleCopyright()
        PanelCopyright.Visible = False

        'Turn on copyright notice only when any OSM layer is visible
        For Each ll As TGIS_LayerAbstract In GisCtrl.Items
            If ll.Name.Contains(LayerNameOSM) AndAlso ll.Active Then
                PanelCopyright.Visible = True
                Exit For
            End If
        Next

        AdjustSize()
    End Sub

    Private Sub OnLayerActiveChange(ByVal ll As TGIS_LayerAbstract)
        'Activate only first sublayer on stream layers (WMS WFS WMTS)
        If ll.Active AndAlso (TypeOf ll Is TGIS_LayerWMS OrElse TypeOf ll Is TGIS_LayerWFS OrElse TypeOf ll Is TGIS_LayerWMTS) Then
            For i As Integer = 1 To ll.SubLayers.Count - 1
                ll.SubLayers(i).Active = False
            Next
        End If
    End Sub

    Private Sub ListFieldData()
        If _cachedMode Then
            ListFieldDataCached()
        Else
            ListFieldDataVirtual()
        End If
    End Sub

    Private Sub ListFieldDataVirtual()
        Dim ll As TGIS_LayerVector
        Dim shp As TGIS_Shape
        Dim col As ReportColumn
        Dim rec As ReportRecord
        Dim dt As DataTable
        Dim i As Integer

        _layerChanged = False

        'Clear any cached records
        ReportData.Records.DeleteAll()

        'Disable custom draw while messing with list
        ReportData.SetVirtualMode(0)
        _shapeList.Clear()

        ll = TryCast(GetActiveLayer(), TGIS_LayerVector)
        If ll IsNot Nothing Then
            'Count shapes
            For i = 1 To ll.GetLastUid()
                shp = ll.GetShape(i)
                If shp IsNot Nothing Then _shapeList.Add(shp.Uid)
            Next

            'Add columns
            ReportData.Columns.DeleteAll()
            ReportData.Columns.Add(0, FieldNameGisUid, 80, True)
            For i = 0 To ll.Fields.Count - 1
                If Not ll.FieldInfo(i).IsUID Then 'UID already added
                    col = ReportData.Columns.Add(i, CType(ll.Fields(i), TGIS_FieldInfo).Name, 80, True)
                    If IsRelationField(CType(ll.Fields(i), TGIS_FieldInfo).Name) Then col.Visible = False 'Hide any relation columns
                End If
            Next

            'Add joined columns
            If ll.JoinNET IsNot Nothing Then
                dt = CType(ll.JoinNET, DataTable)
                For i = 0 To dt.Columns.Count - 1
                    col = ReportData.Columns.Add(i + ll.Fields.Count, dt.Columns(i).Caption, 80, True)
                    If IsRelationField(dt.Columns(i).Caption) Then col.Visible = False 'Hide any relation columns
                Next
            End If

            ReportData.SetVirtualMode(_shapeList.Count)

            'Add template record
            If _shapeList.Count > 0 Then
                rec = ReportData.Records(0)
                For i = 0 To ReportData.Columns.Count
                    rec.AddItem("")
                Next
            End If

            Dim rt As RelationType
            GetLayerRelationType(ll.Name, rt, _currentJoinView)

            _shapeCountPane.Text = _shapeList.Count & LangStr(_strIdRecords)

            'Enable tool strip menu items
            Dim enableMenus As Boolean = _shapeList.Count > 0
            ZoomToShapeToolStripMenuItem.Enabled = enableMenus
            FlashCurrentShapeToolStripMenuItem.Enabled = enableMenus
            RemoveShapeToolStripMenuItem.Enabled = enableMenus AndAlso Not ll.IsReadOnly
            SelectGroupToolStripMenuItem.Enabled = False
            DeselectGroupToolStripMenuItem.Enabled = False
            SelectAllToolStripMenuItem.Enabled = enableMenus
            InvertSelectionToolStripMenuItem.Enabled = enableMenus
            DeselectAllToolStripMenuItem.Enabled = enableMenus
            ConditionsToolStripMenuItem.Enabled = False 'Always disable filtering in virtual mode

            ReportData.Populate()
        End If

        'Use custom draw
        ReportData.SetCustomDraw(XTPReportCustomDraw.xtpCustomBeforeDrawRow)
    End Sub

    Private Sub ListFieldDataCached()
        Dim i As Integer, j As Integer
        Dim ll As TGIS_LayerVector
        Dim shp As TGIS_Shape = Nothing
        Dim record As ReportRecord
        Dim sql As String
        Dim done As Boolean
        Dim tmi As ToolStripMenuItem
        Dim fieldCount As Integer
        Dim fieldName As String
        Dim lastUid As Integer

        'Make sure we don't enter this until the first call has exited
        If _listFieldsRunning Then Exit Sub
        _listFieldsRunning = True

        'Disable virtual mode
        ReportData.SetCustomDraw(0)
        ReportData.SetVirtualMode(0)
        _shapeList.Clear()

Start:
        _layerChanged = False

        'Clear field data viewer
        ReportData.Records.DeleteAll()
        ReportData.Columns.DeleteAll()
        ReportData.Populate()

        _curDataTable = Nothing
        _curLayerData = Nothing

        DataProgressBar.Visible = False

        ZoomToShapeToolStripMenuItem.Enabled = False
        FlashCurrentShapeToolStripMenuItem.Enabled = False
        RemoveShapeToolStripMenuItem.Enabled = False
        SelectGroupToolStripMenuItem.Enabled = False
        DeselectGroupToolStripMenuItem.Enabled = False
        SelectAllToolStripMenuItem.Enabled = False
        InvertSelectionToolStripMenuItem.Enabled = False
        DeselectAllToolStripMenuItem.Enabled = False

        ConditionsToolStripMenuItem.DropDownItems.Clear()

        'Check if this is a vector layer
        ll = TryCast(GetActiveLayer(), TGIS_LayerVector)
        If ll IsNot Nothing Then
            'Get SQL query conditions
            sql = GetFilterConditionString(ll, _filterConditions)

            'Load layer data
            If TypeOf ll Is TGIS_LayerSqlAbstract Then
                _curLayerData = Nothing
                _curDataTableIndex = 0

                'We need to load data directly from db to get all joined fields for each shape
                Dim cmd As New OleDbCommand("", Conn)
                Dim da As New OleDbDataAdapter(cmd)

                Dim rt As RelationType
                Dim jv As String = vbNullString
                GetLayerRelationType(ll.Name, rt, jv)

                'Copy whole data table, also include any joined data
                cmd.CommandText = "SELECT * FROM " & GisTablePrefix & ll.Name & "_FEA f"
                If rt <> RelationType.Undefined AndAlso jv <> vbNullString Then
                    cmd.CommandText += " LEFT JOIN " & jv & " ON " & jv & "." & ForeignKeyField & " = f." & GetJoinPrimary(jv)
                End If
                If sql <> vbNullString Then cmd.CommandText += " WHERE " & sql
                _curDataTable = New DataTable
                _curDataTable.Locale = CultureInfo.InvariantCulture
                da.Fill(_curDataTable)

                'Set up any language dependent columns
                For i = ll.Fields.Count To _curDataTable.Columns.Count - 1
                    If IsNumeric(_curDataTable.Columns(i).Caption) AndAlso LangStr(CInt(_curDataTable.Columns(i).Caption)) IsNot Nothing Then
                        _curDataTable.Columns(i).Caption = LangStr(CInt(_curDataTable.Columns(i).Caption))
                    End If
                Next

                'Get field count
                fieldCount = _curDataTable.Columns.Count
            Else
                'We need to create a new instance for this layer (so the loop won't be interrupted by Locate, PaintScope, etc.)
                If ll.ParentLayer Is Nothing Then
                    _curLayerData = New TGIS_LayerVector()
                    _curLayerData.ImportLayer(ll, ll.Extent, ll.DefaultShapeType(), "", False)
                Else
                    _curLayerData = ll 'Streamed (sublayers) cannot be copied in the same way
                End If

                'We need to cache the number of shapes to avoid interrupting the loop (should be done before call to FindFirst)
                lastUid = _curLayerData.GetLastUid()

                'Find to first shape
                shp = _curLayerData.FindFirst(_curLayerData.Extent, sql)

                'Get field count
                fieldCount = _curLayerData.Fields.Count
            End If

            'Add columns to preview grid and conditions menu (explicitly add id field)
            ReportData.Columns.Add(0, FieldNameGisUid, 80, True)
            tmi = ConditionsToolStripMenuItem.DropDownItems.Add(FieldNameGisUid, Nothing, AddressOf ConditionToolStripEventHandler)
            If _filterConditions(FieldNameGisUid) IsNot Nothing Then tmi.Checked = True 'Check if filter exist on this column

            Dim colIdx As Integer = 1 'Column index counter (in case of any skipped field)
            For i = 0 To fieldCount - 1
                'Get name of field
                If TypeOf ll Is TGIS_LayerSqlAbstract Then
                    fieldName = _curDataTable.Columns(i).Caption
                    If fieldName = ColumnNameUid Then Continue For 'Skip uid and relation fields
                Else
                    fieldName = _curLayerData.FieldInfo(i).Name
                    If _curLayerData.FieldInfo(i).IsUID Then Continue For 'Skip uid
                End If

                'Add field
                Dim col As ReportColumn = ReportData.Columns.Add(colIdx, fieldName, 80, True)
                If IsRelationField(fieldName) Then 'Hide any relation column (database layers contain an internal UID field which should be ignored)
                    col.Visible = False
                Else
                    tmi = ConditionsToolStripMenuItem.DropDownItems.Add(fieldName, Nothing, AddressOf ConditionToolStripEventHandler)
                    If _filterConditions(fieldName) IsNot Nothing Then tmi.Checked = True 'Check if filter exist on this column
                End If
                colIdx += 1
            Next

            DataProgressBar.Value = 0
            DataProgressBar.Visible = True
            _shapeCountPane.Text = _listingFieldDataText
        Else
            _shapeCountPane.Text = vbNullString
        End If

        'List field data
        If _curDataTable IsNot Nothing AndAlso fieldCount > 0 Then
            'Use data table
            For i = _curDataTableIndex To _curDataTable.Rows.Count - 1
                record = ReportData.Records.Add()
                For j = 0 To _curDataTable.Columns.Count - 1
                    record.AddItem(_curDataTable.Rows(i).ItemArray(j))
                Next

                'Update progress bar
                DataProgressBar.Value = i / _curDataTable.Rows.Count * DataProgressBar.Maximum

                Application.DoEvents()

                'We need to restart if active layer was changed
                If _quitting Then
                    Exit For 'Form closing, quit immediately
                ElseIf Not _cachedMode Then
                    Exit For 'User switched to virtual mode
                ElseIf _layerChanged Then
                    _curLayerData = Nothing
                    GoTo Start
                End If
            Next
            _curDataTableIndex = i + 1 'Make sure current record won't be added twice

            'Check if all fields are listed
            If _curDataTableIndex >= _curDataTable.Rows.Count - 1 Then
                done = True
            End If
        ElseIf _curLayerData IsNot Nothing Then
            'Use layer instance
            Do Until shp Is Nothing
                'Add values for current shape
                record = ReportData.Records.Add()
                record.AddItem(shp.Uid)
                For i = 0 To _curLayerData.Fields.Count - 1
                    If _curLayerData.FieldInfo(i).FieldType = TGIS_FieldType.gisFieldTypeDate Then
                        record.AddItem(Microsoft.VisualBasic.FormatDateTime(Convert.ToDateTime(shp.GetField(_curLayerData.FieldInfo(i).Name), CultureInfo.CurrentCulture), DateFormat.GeneralDate))
                    Else
                        record.AddItem(CStr(shp.GetField(_curLayerData.FieldInfo(i).Name)))
                    End If
                Next

                'Update progress bar
                DataProgressBar.Value = shp.Uid / lastUid * DataProgressBar.Maximum

                Application.DoEvents()

                'We need to restart if active layer was changed
                If _quitting Then
                    Exit Do 'Form closing, quit immediately
                ElseIf Not _cachedMode Then
                    Exit Do 'User switched to virtual mode
                ElseIf _layerChanged Then
                    _curLayerData = Nothing
                    GoTo Start
                End If

                shp = _curLayerData.FindNext()
            Loop

            'Check if all fields are listed
            If shp Is Nothing Then
                _curLayerData = Nothing
                done = True
            End If
        End If

        'Check if listing is done
        If done = True Then
            ReportData.Populate()
            _shapeCountPane.Text = ReportData.Rows.Count & LangStr(_strIdRecords)

            'Enable tool strip menu items
            If ReportData.Records.Count > 0 Then
                ZoomToShapeToolStripMenuItem.Enabled = True
                FlashCurrentShapeToolStripMenuItem.Enabled = True
                RemoveShapeToolStripMenuItem.Enabled = ll IsNot Nothing AndAlso Not ll.IsReadOnly
                ConditionsToolStripMenuItem.Enabled = True
                SelectGroupToolStripMenuItem.Enabled = True
                DeselectGroupToolStripMenuItem.Enabled = True
                SelectAllToolStripMenuItem.Enabled = True
                InvertSelectionToolStripMenuItem.Enabled = True
                DeselectAllToolStripMenuItem.Enabled = True
            End If
        End If

        DataProgressBar.Visible = False

        _curLayerData = Nothing
        _curDataTable = Nothing

        _listFieldsRunning = False
    End Sub
#End Region

#Region "Main functions"
    Private Sub AddFile()
        Dim ll As TGIS_LayerAbstract = Nothing
        Dim i As Integer
        Dim dlg As New OpenFileDialog()
        Dim regKey As RegistryKey

        'Fall back to drag mode - this will make sure any added shape is saved properly before importing (in case import is canceled and layer reverted)
        SetGisMode(GisMode.Drag)
        _sectionDestinationLayer = vbNullString

        'Get last import dir from registry
        regKey = Registry.CurrentUser.OpenSubKey(RegKeyGis, False)
        If regKey IsNot Nothing Then
            dlg.InitialDirectory = regKey.GetValue(RegValLastImportDir)
            regKey.Close()
        End If

        dlg.Filter = "Bitmap (*.bmp)|*.bmp|" & _
                     "CSV points (*.csv)|*.csv|" & _
                     "ECW (*.ecw)|*.ecw|" & _
                     "Google Earth (*.kml)|*.kml|" & _
                     "GPS GPX (*.gpx)|*.gpx|" & _
                     "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" & _
                     "MapInfo Tab (*.tab)|*.tab|" & _
                     "MrSID (*.sid)|*.sid|" & _
                     "PNG (*.png)|*.png|" & _
                     "Shape (*.shp)|*.shp|" & _
                     "TIFF (*.tif;*.tiff)|*.tif;*.tiff|" & _
                     "All supported formats|*.shp;*.csv;*.kml;*.gpx;*.tab;*.bmp;*.jpg;*.jpeg;*.png;*.tif;*.tiff;*.ecw;*.sid|" & _
                     "All files (*.*)|*.*"
        dlg.FilterIndex = 12
        dlg.Multiselect = True

        If dlg.ShowDialog() = DialogResult.OK Then
            'Save specified directory so we can use it as initial dir on the next import
            Dim defaultCS As TGIS_CSCoordinateSystem = Nothing
            Dim fi As New FileInfo(dlg.FileName)
            regKey = Registry.CurrentUser.OpenSubKey(RegKeyGis, True)
            If regKey IsNot Nothing Then
                regKey.SetValue(RegValLastImportDir, fi.DirectoryName)
                regKey.Close()
            End If

            If dlg.FileNames.Length > 1 Then
                Dim cmd As New OleDbCommand("", Conn)

                'Import multiple files - just add files from drive, no import dialog
                For i = 0 To dlg.FileNames.Length - 1
                    CreateLayerInstance(ll, dlg.FileNames(i))
                    fi = New FileInfo(dlg.FileNames(i))
                    ll.Caption = Microsoft.VisualBasic.Left(fi.Name, fi.Name.Length - fi.Extension.Length) 'Assign default caption

                    'Check projection settings, let user specify if missing
                    Try
                        ll.Open()
                        If ll.CS.EPSG = 0 Then
                            If defaultCS Is Nothing Then
                                Select Case MsgBox(LangStr(_strIdFilesMissingProjection) & " '" & _GisCtrl.CS.Description & "'?", MsgBoxStyle.YesNoCancel Or MsgBoxStyle.Question)
                                    Case MsgBoxResult.Yes
                                        defaultCS = GisCtrl.CS
                                    Case MsgBoxResult.No
                                        Dim dlgCS As New CoordinateSystemDialog(Nothing)
                                        If dlgCS.ShowDialog() = DialogResult.OK Then
                                            defaultCS = dlgCS.CS
                                        Else
                                            Exit For
                                        End If
                                    Case MsgBoxResult.Cancel
                                        Exit For
                                End Select
                            End If
                            ll.CS = defaultCS
                        End If

                        GisCtrl.Add(ll)
                        ll.ZOrder = 0 'Topmost
                    Catch ex As EGIS_Exception
                        MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                    End Try

                    'Add to layer index table
                    cmd.CommandText = "INSERT INTO gis_layers (name, caption, epsg) VALUES('" & ll.Name & "', '" & Microsoft.VisualBasic.Left(ll.Caption, FieldLengthLayerCaption) & "', " & ll.CS.EPSG & ")"
                    cmd.ExecuteNonQuery()
                Next
            Else
                'Bring up import dialog
                Dim di As New ImportDialog(dlg.FileName, _activeLayerName)
                If di.ShowDialog() = DialogResult.OK Then
                    'Check for section data to be created
                    If di.SectionData.Count > 0 Then
                        _sectionDestinationLayer = di.DestinationLayer
                        Dim ea As New SectionEventArgs(di.SectionData)
                        RaiseEvent CreateSection(Me, ea)
                    End If
                    SetUpVisualizationParams() 'Refresh any joined data
                End If
            End If
        End If

        'Refresh and save
        GisCtrl.Update()
        GisCtrl.SaveProject()
    End Sub

    Private Sub AddStream(Optional ByVal url As String = vbNullString, Optional ByVal protocol As ServiceProtocol = ServiceProtocol.Auto)
        Dim lwms As TGIS_LayerWMS, lwfs As TGIS_LayerWFS, lwmts As TGIS_LayerWMTS, lecwp As TGIS_LayerECW
        Dim added As Boolean = False
        Dim dlg As New ImportStreamDialog

        If url = vbNullString Then
            If dlg.ShowDialog() = DialogResult.OK Then
                url = dlg.Address
                protocol = dlg.Protocol
            End If
        End If

        If url <> vbNullString Then
            'WMS
            If protocol = ServiceProtocol.WMS OrElse (protocol = ServiceProtocol.Auto AndAlso Not added) Then
                Try
                    lwms = New TGIS_LayerWMS
                    lwms.Path = url
                    lwms.Open()
                    GisCtrl.Add(lwms)
                    added = True
                Catch ex As EGIS_Exception
                End Try
            End If

            'WFS
            If protocol = ServiceProtocol.WFS OrElse (protocol = ServiceProtocol.Auto AndAlso Not added) Then
                Try
                    lwfs = New TGIS_LayerWFS
                    lwfs.Path = url
                    lwfs.Open()
                    GisCtrl.Add(lwfs)
                    added = True
                Catch ex As EGIS_Exception
                End Try
            End If

            'WMTS
            If protocol = ServiceProtocol.WMTS OrElse (protocol = ServiceProtocol.Auto AndAlso Not added) Then
                Try
                    lwmts = New TGIS_LayerWMTS
                    lwmts.Path = url
                    lwmts.Open()
                    GisCtrl.Add(lwmts)
                    added = True
                Catch ex As EGIS_Exception
                End Try
            End If

            'ECWP
            If protocol = ServiceProtocol.ECWP OrElse (protocol = ServiceProtocol.Auto AndAlso Not added) Then
                Try
                    lecwp = New TGIS_LayerECW
                    lecwp.Path = url
                    lecwp.Open()
                    GisCtrl.Add(lecwp)
                    added = True
                Catch ex As EGIS_Exception
                End Try
            End If

            'Unknown protocol
            If Not added Then
                MsgBox(LangStr(_strIdOpenStreamFailed), MsgBoxStyle.Exclamation)
            Else
                GisCtrl.SaveProject()
                GisCtrl.Update()
            End If
        End If
    End Sub

    Friend Function AddNewLayer(Optional ByVal silent As Boolean = False, _
                                Optional ByVal title As String = vbNullString, _
                                Optional ByVal rt As RelationType = RelationType.Undefined, _
                                Optional ByVal joinView As String = vbNullString, _
                                Optional ByVal question As String = vbNullString) As String
        Dim layerName As String = vbNullString

        'Fall back to drag mode - this will make sure any added shape is saved properly before importing (in case import is canceled and layer reverted)
        SetGisMode(GisMode.Drag)

        If question = vbNullString Then question = LangStr(_strIdLayerCaption)

        'If a title is already specified we shouldn't ask the user
        If title = vbNullString Then title = InputBox(question, LangStr(_strIdNewLayer))
        If title <> vbNullString Then
            'Find a new name for this layer
            layerName = GetUniqueLayerName()

            Dim ll As New TGIS_LayerVector
            Dim lSql As TGIS_LayerSqlAdo

            'Create a new sql layer
            lSql = New TGIS_LayerSqlAdo
            lSql.SQLParameter("LAYER") = GisTablePrefix & layerName
            lSql.SQLParameter("DIALECT") = "MSSQL"
            lSql.SQLParameter("ADO") = ConnStr
            lSql.ImportLayer(ll, ll.Extent, TGIS_ShapeType.gisShapeTypeUnknown, "", False)
            lSql.Name = layerName
            lSql.Path = "Storage=Native\n" & _
              "Layer=" & GisTablePrefix & layerName & "\n" & _
              "Dialect=MSSQL\n" & _
              "ADO=" & ConnStr & "\n" & _
              ".ttkls"
            lSql.Caption = title
            lSql.CS = GisCtrl.CS    'Assign default coordinate system
            GisCtrl.Add(lSql)

            'Add join view
            lSql.JoinNET = GetJoinDT(joinView)
            lSql.JoinPrimary = GetJoinPrimary(joinView)
            lSql.JoinForeign = ForeignKeyField

            'Add layer to index table, add appropriate fields
            UpdateLayerRelation(lSql.Name, lSql.Caption, lSql.CS.EPSG, rt, joinView)

            lSql.SaveAll()
            GisCtrl.SaveProject()

            'Update legend controls
            ControlLegend.Update()
            ControlHierarchy.Update()
        End If

        Return layerName
    End Function

    Private Function ImportPlots() As Boolean
        Dim pct As Integer = 0
        Dim ll As TGIS_LayerVector = Nothing
        Dim layerName As String
        Dim shpStand As TGIS_Shape = Nothing

        'Show file open dialog
        Dim dlg As New System.Windows.Forms.OpenFileDialog
        If EstimateSodraInstalled Then
            dlg.Filter = "Haglof Plot files (*.hpl; *.hxl; *.sod)|*.hpl;*.hxl;*.sod"
        Else
            dlg.Filter = "Haglof Plot files (*.hpl; *.hxl)|*.hpl;*.hxl"
        End If
        If dlg.ShowDialog() = DialogResult.Cancel Then Return False

        'Take care of any stand coordinates before handling plot data
        Dim fi As New FileInfo(dlg.FileName)
        If fi.Extension.ToUpperInvariant() = ".HXL" Then
            Try
                Dim xmld As New XmlDocument
                Dim nodelist As XmlNodeList
                Dim node As XmlNode

                'Load xml document and extract coordinate nodes
                xmld.Load(dlg.FileName)
                'Add function to retreive stand coordinates if user wants to and if there exists such 20101208 J�

                nodelist = xmld.SelectNodes("/HXL/Object/Placemark/Polygone/outerBoundaryIs/LinearRing/coordinates")

                'Check if there are any coordinates in the Xml file
                If nodelist.Count > 0 Then
                    'Ask to read the stand coordinates or not
                    If MsgBox(LangStr(_strIdImportStandCoordQues), MsgBoxStyle.Question Or MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then

                        'Show layer dialog to choose layer to import the stands to 20100812 J�
                        Dim dlgLayer1 As New LayerChoiceDialog(RelationType.Undefined, LangStr(_strIdForStandCoords), True, _
                                                               TGIS_ShapeType.gisShapeTypePolygon, True, RelationType.Undefined, vbNullString, False, _activeLayerName)
                        If dlgLayer1.ShowDialog() = DialogResult.OK Then
                            If dlgLayer1.LayerOne = NewLayerName Then
                                'Create a new layer
                                layerName = AddNewLayer()
                                ll = GisCtrl.Get(layerName)
                            Else
                                ll = GisCtrl.Get(dlgLayer1.LayerOne)
                            End If
                            'Check if addnewlayer has succeeded
                            If ll Is Nothing Then Return False
                        Else
                            Return False
                        End If
                        'Make sure this is a layer vector
                        If Not TypeOf ll Is TGIS_LayerVector Then
                            MsgBox(LangStr(_strIdPlotUseVectorLayer), MsgBoxStyle.Exclamation)
                            Return False
                        End If

                        node = nodelist.Item(0)
                        If node.ChildNodes.Item(0).InnerText() <> vbNullString Then
                            Dim latlong As String() = Split(node.ChildNodes.Item(0).InnerText(), vbCrLf)
                            For Each pts As String In latlong
                                Dim coords As String() = pts.Split(",")
                                If coords.Length = 2 Then
                                    'Create a new shape for this stand
                                    If shpStand Is Nothing Then
                                        shpStand = ll.CreateShape(TGIS_ShapeType.gisShapeTypePolygon)
                                        shpStand.AddPart()
                                    End If
                                    'Add point to shape
                                    If shpStand IsNot Nothing Then shpStand.AddPoint(LatLongDdMmmmmmToCS(coords(0), coords(1), GisCtrl.CS))
                                End If
                            Next
                        End If
                        If shpStand IsNot Nothing Then
                            ll.SaveAll()
                        End If
                    End If
                End If
            Catch ex As FormatException 'May be thrown from LatLongDdMmmmmmToCS
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Catch ex As IOException
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Catch ex As XmlException
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            End Try
        End If

        'Handle plot data
        If fi.Extension.ToUpperInvariant() = ".HPL" OrElse fi.Extension.ToUpperInvariant() = ".SOD" Then
            Try
                Dim s As Stream = dlg.OpenFile()
                Dim r As New BinaryReader(s)
                Dim ptg As TGIS_Point
                Dim shp As TGIS_Shape
                Dim plotIdFieldOK As Boolean
                Dim plotId As Integer

                'Read off header
                Dim ver As UInteger = r.ReadUInt32()
                If (ver > _hplFileVersion AndAlso fi.Extension.ToUpperInvariant() = ".HPL") OrElse (ver > _sodFileVersion AndAlso fi.Extension.ToUpperInvariant() = ".SOD") Then 'Version check
                    MsgBox(LangStr(_strIdHplFileTooHighVersion), MsgBoxStyle.Exclamation)
                Else
                    If fi.Extension.ToUpperInvariant() = ".SOD" Then
                        'Read off S�dra specific vars (not used)
                        r.ReadDouble() 'Shape area
                        r.ReadChars(50) 'Section name
                        r.ReadChars(3) 'Section number
                    End If

                    pct = r.ReadInt32() 'Point count
                    If pct > 0 Then

                        'Show layer dialog too choose layer to import the plots to 20100812 J�
                        Dim dlgLayer As New LayerChoiceDialog(RelationType.Undefined, LangStr(_strIdForPlotCoords), True, _
                                                              TGIS_ShapeType.gisShapeTypePoint, True, RelationType.Undefined, vbNullString, False, _activeLayerName)
                        If dlgLayer.ShowDialog() = DialogResult.OK Then
                            If dlgLayer.LayerOne = NewLayerName Then
                                'Create a new layer
                                layerName = AddNewLayer()
                                ll = GisCtrl.Get(layerName)
                                ll.AddField(GisControlView.FieldNamePlotId, TGIS_FieldType.gisFieldTypeNumber, 0, 0) 'Add plot id field
                            Else
                                ll = GisCtrl.Get(dlgLayer.LayerOne)
                            End If
                            'Check if addnewlayer has succeeded
                            If ll Is Nothing Then Return False
                        Else
                            Return False
                        End If

                        'Make sure this is a layer vector
                        If Not TypeOf ll Is TGIS_LayerVector Then
                            MsgBox(LangStr(_strIdPlotUseVectorLayer), MsgBoxStyle.Exclamation)
                            Return False
                        End If

                        'Check for plot id field
                        If ll.FindField(FieldNamePlotId) <> -1 Then
                            If ll.FieldInfo(ll.FindField(FieldNamePlotId)).FieldType = TGIS_FieldType.gisFieldTypeNumber Then plotIdFieldOK = True
                        End If

                        'Read off plot data, create a new shape for each plot
                        Try
                            For i As Integer = 0 To pct - 1
                                ptg.X = r.ReadDouble()
                                ptg.Y = r.ReadDouble()
                                If ver > 1 Then plotId = r.ReadInt32() 'Plot id exist from version 2 and above

                                shp = ll.CreateShape(TGIS_ShapeType.gisShapeTypePoint)
                                shp.AddPart()
                                shp.AddPoint(GisCtrl.CS.FromWGS(ptg))
                                If plotIdFieldOK Then shp.SetField(FieldNamePlotId, plotId)
                            Next
                        Catch ex As EGIS_Exception
                            ll.RevertShapes() : pct = 0 'Undo any changes
                            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                        End Try
                    End If
                End If
            Catch ex As IOException
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            End Try
        ElseIf fi.Extension.ToUpperInvariant() = ".HXL" Then
            Try
                Dim xmld As New XmlDocument
                Dim nodelist As XmlNodeList
                Dim node As XmlNode
                Dim shp As TGIS_Shape

                'Load xml document and extract coordinate nodes
                xmld.Load(dlg.FileName)
                nodelist = xmld.SelectNodes("/HXL/Object/PlotSet/Plot/Point/coordinates")
                If nodelist.Count() > 0 Then
                    'Show layer dialog too choose layer to import the plots to 20100812 J�
                    Dim dlgLayer As New LayerChoiceDialog(RelationType.Undefined, LangStr(_strIdForPlotCoords), True, _
                                                          TGIS_ShapeType.gisShapeTypePoint, True, RelationType.Undefined, vbNullString, False, _activeLayerName)
                    If dlgLayer.ShowDialog() = DialogResult.OK Then
                        If dlgLayer.LayerOne = NewLayerName Then
                            'Create a new layer
                            layerName = AddNewLayer()
                            ll = GisCtrl.Get(layerName)
                        Else
                            ll = GisCtrl.Get(dlgLayer.LayerOne)
                        End If
                        'Check if addnewlayer has succeeded
                        If ll Is Nothing Then Return False
                    Else
                        Return False
                    End If

                    'Make sure this is a layer vector
                    If Not TypeOf ll Is TGIS_LayerVector Then
                        MsgBox(LangStr(_strIdPlotUseVectorLayer), MsgBoxStyle.Exclamation)
                        Return False
                    End If

                    'Read off plot data, create a new shape for each plot
                    pct = 0
                    Try
                        For Each node In nodelist
                            Dim coords() As String = Split(node.ChildNodes.Item(0).InnerText, ",")
                            Dim ptg As TGIS_Point = LatLongDdMmmmmmToCS(coords(0), coords(1), GisCtrl.CS)

                            shp = ll.CreateShape(TGIS_ShapeType.gisShapeTypePoint)
                            shp.AddPart()
                            shp.AddPoint(ptg)
                            pct += 1
                        Next
                    Catch ex As EGIS_Exception
                        ll.RevertShapes() : pct = 0 'Undo any changes
                        MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                    End Try
                End If
            Catch ex As FormatException 'May be thrown from LatLongDdMmmmmmToCS
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Catch ex As IOException
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Catch ex As XmlException
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            End Try
        Else
            MsgBox(LangStr(_strIdUnknownFileExtension), MsgBoxStyle.Exclamation)
        End If

        If pct > 0 Then
            ll.SaveAll()
        End If

        If pct > 0 OrElse shpStand IsNot Nothing Then
            GisCtrl.Update()
        End If

        If shpStand IsNot Nothing Then
            MsgBox(pct & LangStr(_strIdCountPlotsImportSuccessful) & LangStr(_strIdStandCoordImportSuccessful), MsgBoxStyle.Information)
        Else
            MsgBox(pct & LangStr(_strIdCountPlotsImportSuccessful), MsgBoxStyle.Information)
        End If

        Return True
    End Function

    Private Function SetLayerZOrder(ByVal layerName As String, ByVal zorder As Integer) As Boolean
        Dim ll As TGIS_LayerAbstract

        'Make sure layer exist, update zorder
        ll = GisCtrl.Get(layerName)
        If ll Is Nothing Then Return False
        ll.ZOrder = zorder

        Return True
    End Function

    Private Sub SetActiveLayer(ByVal ll As TGIS_LayerAbstract)
        Dim la As TGIS_LayerVector
        Static inProgress As Boolean = False

        If Not inProgress Then
            'Make sure to editing is ended
            EndEditing()

            'Clear any selection in current layer before switching
            la = TryCast(GetActiveLayer(), TGIS_LayerVector)
            If la IsNot Nothing AndAlso _mode <> GisMode.GenerateRouteStep1WithCenterLine Then la.DeselectAll()

            If ll IsNot Nothing Then
                'Store active layer name
                If ll.ParentLayer Is Nothing Then
                    _activeLayerName = ll.Name
                    _activeSubLayerName = vbNullString
                Else
                    _activeLayerName = ll.ParentLayer.Name
                    _activeSubLayerName = ll.Name

                    'Disable menus
                    CommandBars.Actions(ToolbarId.NewShape).Enabled = False
                    CommandBars.Actions(ToolbarId.EditShape).Enabled = False
                    CommandBars.Actions(ToolbarId.RemoveShape).Enabled = False
                End If

                'Display current coordinate system in status bar
                _coordinateSystemPane.Text = ll.CS.WKT

                'Mark active layer in both legends so this remain consistent
                inProgress = True 'This flag will prevent iteration deadlock (setting GIS_Layer will trigger an event that will call this sub)
                ControlLegend.GIS_Layer = ll
                ControlHierarchy.GIS_Layer = ll
                inProgress = False
            Else
                _activeLayerName = vbNullString
                _activeSubLayerName = vbNullString
                _coordinateSystemPane.Text = vbNullString
            End If

            'Set up menu items
            If TypeOf ll Is TGIS_LayerVector Then
                'Enable layer menu items
                ConditionsToolStripMenuItem.Enabled = _cachedMode

                'Enable all add shape menus
                CommandBars.Actions(ToolbarId.NewShape).Enabled = Not ll.IsReadOnly
                CommandBars.Actions(ToolbarId.EditShape).Enabled = Not ll.IsReadOnly
                CommandBars.Actions(ToolbarId.RemoveShape).Enabled = Not ll.IsReadOnly

                CommandBars.Actions(ToolbarId.NewShapePolygon).Enabled = Not ll.IsReadOnly
                CommandBars.Actions(ToolbarId.NewShapeLine).Enabled = Not ll.IsReadOnly
                CommandBars.Actions(ToolbarId.NewShapePoint).Enabled = Not ll.IsReadOnly
                CommandBars.Actions(ToolbarId.NewShapeMultiPoint).Enabled = Not ll.IsReadOnly

                'Disable unsupported shape types
                Dim lv As TGIS_LayerVector = ll 'Type cast to vector layer
                If Not lv.SupportedShapes And TGIS_ShapeType.gisShapeTypePolygon Then
                    CommandBars.Actions(ToolbarId.NewShapePolygon).Enabled = False
                End If
                If Not lv.SupportedShapes And TGIS_ShapeType.gisShapeTypeArc Then
                    CommandBars.Actions(ToolbarId.NewShapeLine).Enabled = False
                End If
                If Not lv.SupportedShapes And TGIS_ShapeType.gisShapeTypePoint Then
                    CommandBars.Actions(ToolbarId.NewShapePoint).Enabled = False
                End If
                If Not lv.SupportedShapes And TGIS_ShapeType.gisShapeTypeMultiPoint Then
                    CommandBars.Actions(ToolbarId.NewShapeMultiPoint).Enabled = False
                End If
            Else
                'Disable layer menu items
                ConditionsToolStripMenuItem.Enabled = _cachedMode

                'Disable all add shape menues
                CommandBars.Actions(ToolbarId.NewShape).Enabled = False
                CommandBars.Actions(ToolbarId.EditShape).Enabled = False
                CommandBars.Actions(ToolbarId.RemoveShape).Enabled = False
            End If
            CommandBars.Actions(ToolbarId.RemoveLayer).Enabled = String.IsNullOrEmpty(_activeSubLayerName) AndAlso Not String.IsNullOrEmpty(_activeLayerName) 'Enable only if a main layer is selected
            CommandBars.Actions(ToolbarId.LayerProperties).Enabled = Not String.IsNullOrEmpty(_activeLayerName)

            'Clear filter conditions
            _filterConditions.Clear()

            'Refresh field data viewer (we do this by flagging for a layer change, if we call ListFieldData here the ControlLegend OnLayerSelect event continued handling will be delayed)
            _layerChanged = True
        End If
    End Sub

    Private Sub AddShape(ByVal st As ShapeType)
        SetGisMode(GisMode.Add)
        _newShapeType = st
    End Sub

    Private Sub MeasureShape(ByVal st As ShapeType)
        SetGisMode(GisMode.Measure)
        _newShapeType = st
    End Sub

    Private Sub EndEditing()
        Dim ll As TGIS_LayerAbstract = GetActiveLayer()

        'Check current mode
        If _mode = GisMode.Add Then
            'Save any changes
            ApplyEdit(False)
            If ll IsNot Nothing Then
                Try
                    ll.SaveData()
                Catch ex As EGIS_Exception
                    MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                    ll.RevertAll()
                    GisCtrl.Update()
                End Try
            End If
        ElseIf _mode = GisMode.EditStep2 Then
            'Save any changes
            ApplyEdit(False)
            If ll IsNot Nothing Then
                Try
                    ll.SaveData()
                Catch ex As EGIS_Exception
                    MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                    ll.RevertAll()
                    GisCtrl.Update()
                End Try
            End If
        ElseIf _mode = GisMode.Measure Then
            'Remove temp layer (make sure to end editing first)
            RevertEdit(False)
            If GisCtrl.Get(PreviewLayerName) IsNot Nothing Then GisCtrl.Delete(PreviewLayerName)
        ElseIf _mode = GisMode.Split Then
            'Split should not take effect when user haven't explicitly pressed Apply
            RevertEdit(False)
        ElseIf _mode = GisMode.SelectShp Then
            'Clear any selection within active layer
            If ll IsNot Nothing AndAlso TypeOf ll Is TGIS_LayerVector Then
                CType(ll, TGIS_LayerVector).DeselectAll()
            End If
        End If

        UpdateLengthArea(Nothing)

        'Restore settings
        _newShapeType = -1
        _customShapeUid = -1
    End Sub

    Private Sub ApplyEdit(Optional ByVal changeMode As Boolean = True)
        If GisCtrl.Editor.Layer IsNot Nothing Then
            Dim ll As TGIS_LayerVector = GisCtrl.Get(CType(GisCtrl.Editor.Layer, TGIS_LayerVector).Name)
            Dim shp As TGIS_Shape

            'Save any changes
            If ll IsNot Nothing Then
                Dim oldCursor As Cursor = GisCtrl.Cursor
                GisCtrl.Cursor = Cursors.WaitCursor

                If _mode = GisMode.Split Then
                    Dim n As Integer
                    Dim shape_list As ArrayList
                    Dim topology_obj As New TGIS_Topology
                    Dim arc As TGIS_Shape
                    Dim newShapes As New List(Of ArrayList)
                    Dim removeList As New List(Of Integer)
                    Dim userCancel As Boolean = False

                    'Display a warning if layer is connected to a suite
                    If GetLayerRelationType(ll.Name) <> RelationType.Undefined Then
                        If MsgBox(LangStr(_strIdSplitUnionWarning1) & vbCrLf & LangStr(_strIdSplitUnionWarning2), MsgBoxStyle.Exclamation Or MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                            userCancel = True   'Don't split
                            GisCtrl.Editor.RevertShape()
                        End If
                    End If

                    'Don't end editing before warning is shown so we can revert shape
                    GisCtrl.Editor.EndEdit()

                    'Get last custom shape
                    arc = ll.GetShape(_customShapeUid)
                    If arc IsNot Nothing AndAlso userCancel = False Then
                        'Remove split line
                        ll.Delete(_customShapeUid)

                        'Cache all shape ids in layer, so we can call shp.MakeEditable in the loop below
                        Dim uidlst As New List(Of Integer)
                        shp = ll.FindFirst()
                        Do Until shp Is Nothing
                            uidlst.Add(shp.Uid)
                            shp = ll.FindNext()
                        Loop

                        'Split all intersecting shapes within selected layer
                        For Each uid As Integer In uidlst
                            shp = ll.GetShape(uid).MakeEditable()
                            Try 'Points for example cannot be split, it will throw an exception
                                shape_list = topology_obj.SplitByArc(shp, arc, True) : If shape_list Is Nothing Then Continue For
                                If topology_obj.Intersect(arc, shp) AndAlso shape_list.Count > 0 Then
                                    'Remove original shape -- add to remove list (we don't want to interrupt the shape loop)
                                    removeList.Add(shp.Uid)

                                    'Add to list and add shapes later (don't interrupt the loop)
                                    newShapes.Add(shape_list)
                                End If
                            Catch ex As EGIS_Exception
                            End Try
                        Next

                        'Add new splitted shapes to layer
                        For Each list As ArrayList In newShapes
                            For n = 0 To list.Count - 1
                                ll.AddShape(list.Item(n))
                            Next n
                        Next

                        'Remove any listed shapes
                        For Each item As Integer In removeList
                            ll.Delete(item)
                        Next

                        'Save & update view
                        ll.SaveData()
                        GisCtrl.Update()
                    End If
                Else
                    GisCtrl.Editor.EndEdit()
                    Try
                        ll.SaveData()
                    Catch ex As EGIS_Exception
                        MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                        ll.RevertAll()
                        GisCtrl.Update()
                    End Try

                    'When adding a shape also add shape data to field viewer
                    If _mode = GisMode.Add Then
                        shp = ll.GetShape(_customShapeUid)
                        If shp IsNot Nothing Then
                            'Check relation type
                            Dim type As RelationType = RelationType.Undefined
                            type = GetLayerRelationType(ll.Name)
                            If Not type = RelationType.Undefined Then
                                _selectedShapeUid = shp.Uid

                                'Ask if we should relate the new shape to a database object
                                If MsgBox(LangStr(_strIdCreateRelation), MsgBoxStyle.YesNo Or MsgBoxStyle.Question) = MsgBoxResult.Yes Then
                                    OnAddRelation(0, System.EventArgs.Empty)
                                End If

                                '#3724: Add centroid to property table
                                If type = RelationType.PropertyRelation Then
                                    AddCentroidToPropertyTable(_applyRelationId, shp)
                                End If
                            End If

                            AddShpToAttributeTable(shp)

                            'Update field data viewer if this is the first shape in layer (in this case count will still be 0)
                            If ll.Items.Count = 0 Then ListFieldData()
                        End If
                    End If
                End If

                GisCtrl.Cursor = oldCursor
            End If
        End If

        'Fall back to drag mode
        If changeMode Then SetGisMode(GisMode.Drag)
    End Sub

    Private Sub RevertEdit(Optional ByVal changeMode As Boolean = True)
        'Undo any changes
        GisCtrl.Editor.RevertShape()
        GisCtrl.Editor.EndEdit()

        'Fall back to drag mode
        If changeMode Then SetGisMode(GisMode.Drag)
    End Sub

    Private Sub Redo()
        'Redo last editor change
        If GisCtrl.Editor.CanRedo Then
            GisCtrl.Editor.Redo()
            GisCtrl.Update()
        End If
    End Sub

    Private Sub Undo()
        'Undo last editor change
        If GisCtrl.Editor.CanUndo Then
            GisCtrl.Editor.Undo()
            GisCtrl.Update()
        End If
    End Sub

    Private Sub SetCachedMode(ByVal newMode As Boolean)
        _cachedMode = newMode
        CachedModeToolStripMenuItem.Checked = newMode

        'Save new value to registry
        Dim regKey As RegistryKey = Registry.CurrentUser.OpenSubKey(RegKeyGis, True)
        If regKey IsNot Nothing Then
            regKey.SetValue(RegValCachedMode, newMode)
            regKey.Close()
        End If

        'Refresh attribute list
        ListFieldData()
    End Sub

    Private Sub SetGisMode(ByVal newMode As GisMode)
        'Handle transition from current mode
        EndEditing()

        'Turn off selection, we don't need it anymore
        _selectionStarted = False
        _selectionEndPointSet = False

        'Deselect and forget middle line when we don't need it anymore
        If newMode <> GisMode.GenerateRouteStep1WithCenterLine AndAlso newMode <> GisMode.GenerateRouteStep1WithoutCenterLine AndAlso newMode <> GisMode.GenerateRouteStep2 AndAlso _routeMiddleLine IsNot Nothing Then
            _routeMiddleLine.MakeEditable().IsSelected = False
            _routeMiddleLine = Nothing
            GisCtrl.Update()
        End If

        'Handle deselection of shapes when user right click to leave a mode
        If _mode = GisMode.GenerateRouteStep2 Then
            Dim ll As TGIS_LayerVector = TryCast(GisCtrl.Get(_plotLayerForRoute), TGIS_LayerVector)
            If ll IsNot Nothing Then
                ll.DeselectAll()
                GisCtrl.Update()
            End If
        End If

        'Disable apply relation when manually creating stand/section
        If newMode = GisMode.CreateStand OrElse newMode = GisMode.CreateSection Then
            _applyRelationId = 0
            _applyRelationType = RelationType.Undefined
        End If

        'Set GIS control mode
        Select Case newMode
            Case GisMode.ViewInfo, GisMode.Add, GisMode.EditStep2, GisMode.Remove, GisMode.Measure, GisMode.Split
                GisCtrl.Mode = TGIS_ViewerMode.gisEdit
            Case GisMode.EditStep1, GisMode.SelectShp, GisMode.CreateElvObject, GisMode.CreateStand, GisMode.CreateSection, GisMode.CreatePlots, GisMode.GenerateRouteStep1WithoutCenterLine, GisMode.GenerateRouteStep1WithCenterLine, GisMode.GenerateRouteStep2, GisMode.ExportRoute, GisMode.ExportShp, GisMode.ExportToTCNavigator, GisMode.ApplyRelation, GisMode.SumAttributes, GisMode.ImportProperties
                GisCtrl.Mode = TGIS_ViewerMode.gisSelect
            Case GisMode.Drag
                GisCtrl.Mode = TGIS_ViewerMode.gisDrag
            Case GisMode.Zoom
                GisCtrl.Mode = TGIS_ViewerMode.gisZoom
        End Select

        'Update help text in action pane
        If _actionPane IsNot Nothing Then 'During initialization
            Select Case newMode
                Case GisMode.Drag
                    _actionPane.Text = LangStr(_strIdDescriptionDrag)
                Case GisMode.SelectShp
                    _actionPane.Text = LangStr(_strIdDescriptionSelectShp)
                Case GisMode.Zoom
                    _actionPane.Text = LangStr(_strIdDescriptionZoom)
                Case GisMode.ViewInfo
                    _actionPane.Text = LangStr(_strIdDescriptionViewInfo)
                Case GisMode.Add
                    _actionPane.Text = LangStr(_strIdDescriptionAdd)
                Case GisMode.EditStep1
                    _actionPane.Text = LangStr(_strIdDescriptionEditStep1)
                Case GisMode.EditStep2
                    _actionPane.Text = LangStr(_strIdDescriptionEditStep2)
                Case GisMode.Remove
                    _actionPane.Text = LangStr(_strIdDescriptionRemove)
                Case GisMode.Measure
                    _actionPane.Text = LangStr(_strIdDescriptionMeasure)
                Case GisMode.Split
                    _actionPane.Text = LangStr(_strIdDescriptionSplit)
                Case GisMode.CreatePlots
                    _actionPane.Text = LangStr(_strIdDescriptionCreatePlots)
                Case GisMode.GenerateRouteStep1WithoutCenterLine
                    _actionPane.Text = LangStr(_strIdDescriptionGenerateRouteStep1WithoutCenterLine)
                Case GisMode.GenerateRouteStep1WithCenterLine
                    _actionPane.Text = LangStr(_strIdDescriptionGenerateRouteStep1WithCenterLine)
                Case GisMode.GenerateRouteStep2
                    _actionPane.Text = LangStr(_strIdDescriptionGenerateRouteStep2)
                Case GisMode.ExportRoute
                    _actionPane.Text = LangStr(_strIdDescriptionExportRoute)
                Case GisMode.CreateElvObject
                    _actionPane.Text = LangStr(_strIdDescriptionCreateElvObject)
                Case GisMode.ExportShp, GisMode.ExportToTCNavigator
                    _actionPane.Text = LangStr(_strIdDescriptionExportShp)
                Case GisMode.ApplyRelation
                    _actionPane.Text = LangStr(_strIdDescriptionApplyRelation)
                Case GisMode.SumAttributes
                    _actionPane.Text = LangStr(_strIdDescriptionSumAttributes)
                Case GisMode.ImportProperties
                    _actionPane.Text = LangStr(_strIdDescriptionImportProperties)
                Case GisMode.CreateStand
                    _actionPane.Text = LangStr(_strIdDescriptionCreateStand)
                Case GisMode.CreateSection
                    _actionPane.Text = LangStr(_strIdDescriptionCreateSection)
                Case Else
                    _actionPane.Text = vbNullString 'All modes should have a description so we shouldn't get here
            End Select
        End If

        'Set mode indicator
        _mode = newMode
    End Sub

    Private Function GetEditMode() As EditMode
        If GisCtrl.Editor.Mode = TGIS_EditorMode.gisEditorModeAfterActivePoint Or GisCtrl.Editor.Mode = TGIS_EditorMode.gisEditorModeReversed Then
            Return EditMode.AfterActivePoint
        ElseIf GisCtrl.Editor.Mode = TGIS_EditorMode.gisEditorModeNearestPoint Or GisCtrl.Editor.Mode = TGIS_EditorMode.gisEditorModeDefault Then
            Return EditMode.NearestPoint
        End If
    End Function

    Private Sub SetEditMode(ByVal newMode As EditMode)
        'Change edit mode (use reversed/default so user can press Ctrl to quick-switch between these modes)
        If newMode = EditMode.AfterActivePoint Then
            GisCtrl.Editor.Mode = TGIS_EditorMode.gisEditorModeReversed
        ElseIf newMode = EditMode.NearestPoint Then
            GisCtrl.Editor.Mode = TGIS_EditorMode.gisEditorModeDefault
        End If

        'Save new value to registry
        Dim regKey As RegistryKey = Registry.CurrentUser.OpenSubKey(RegKeyGis, True)
        If regKey IsNot Nothing Then
            regKey.SetValue(RegValEditMode, CInt(GisCtrl.Editor.Mode))
            regKey.Close()
        End If
    End Sub

    Private Sub SetSnapType(ByVal newMode As TGIS_EditorSnapType)
        'Change mode
        GisCtrl.Editor.SnapType = newMode

        'Save new value to registry
        Dim regKey As RegistryKey = Registry.CurrentUser.OpenSubKey(RegKeyGis, True)
        If regKey IsNot Nothing Then
            regKey.SetValue(RegValSnapType, CInt(GisCtrl.Editor.SnapType))
            regKey.Close()
        End If
    End Sub

    Private Sub SetRouteDirection(ByVal newDirection As RouteDirection)
        'Change mode
        _routeDirection = newDirection

        'Save new value to registry
        Dim regKey As RegistryKey = Registry.CurrentUser.OpenSubKey(RegKeyGis, True)
        If regKey IsNot Nothing Then
            regKey.SetValue(RegValRouteDirection, CInt(_routeDirection))
            regKey.Close()
        End If
    End Sub

    Private Sub SetNorthArrow(ByVal visible As Boolean)
        ControlNorthArrow.Visible = visible

        'Save to registry
        Dim regKey As RegistryKey = Registry.CurrentUser.OpenSubKey(RegKeyGis, True)
        If regKey IsNot Nothing Then
            regKey.SetValue(RegValNorthArrow, visible)
            regKey.Close()
        End If
    End Sub

    Private Sub SetMapRotation(ByVal maprotation As Integer)
        _maprotation = maprotation
        GisCtrl.RotationPoint = TGIS_Utils.GisCenterPoint(GisCtrl.VisibleExtent)
        GisCtrl.RotationAngle = Decimal.ToDouble(_maprotation) * (Math.PI / 180)

        'Save to registry
        Dim regKey As RegistryKey = Registry.CurrentUser.OpenSubKey(RegKeyGis, True)
        If regKey IsNot Nothing Then
            regKey.SetValue(RegValMapRotation, _maprotation)
            regKey.Close()
        End If
    End Sub

    Private Sub StoreCurrentExtent()
        Dim i As Integer

        'Move down all previous extents one step in the array
        For i = _previousExtent.Length - 2 To 0 Step -1
            _previousExtent(i + 1) = _previousExtent(i)
        Next

        'Store current extent
        _previousExtent(0) = GisCtrl.VisibleExtent
    End Sub

    Private Sub NavigateTo()
        Dim dlg As New NavigateToDialog
        Dim dx As Double, dy As Double
        Dim newCenter As TGIS_Point

        If dlg.ShowDialog() = DialogResult.OK Then
            'Use width/height of current extent
            dx = (GisCtrl.VisibleExtent.XMax - GisCtrl.VisibleExtent.XMin) / 2.0
            dy = (GisCtrl.VisibleExtent.YMax - GisCtrl.VisibleExtent.YMin) / 2.0

            'Calculate new visible extent with chosen coordinates in center
            If dlg.IsWGS Then
                'Values are in radians
                newCenter = GisCtrl.CS.FromWGS(New TGIS_Point(dlg.X, dlg.Y))
            Else
                'Values contain plain x/y coordinates
                newCenter = New TGIS_Point(dlg.X, dlg.Y)
            End If
            GisCtrl.Lock() 'For some reason we need to lock the viewer for new extent to be applied with a geographic coordinate system
            GisCtrl.VisibleExtent = New TGIS_Extent(newCenter.X - dx, newCenter.Y - dy, newCenter.X + dx, newCenter.Y + dy)
            GisCtrl.Unlock()

            'Mark position on map
            _drawNavigationPin = True
            _navigationPinDrawn = False 'Navigation pin is removed when visible extent is changed, however visible extent will change after this sub so this flag tell we should keep it once
            _navigationPinPos = GisCtrl.MapToScreen(newCenter)
        End If
    End Sub

    Private Sub ZoomToPreviousExtent()
        Dim i As Integer
        Dim nullExtent As TGIS_Extent, tempExtent As TGIS_Extent

        'Go back to previous extent if set
        If Not _previousExtent(1).Equals(nullExtent) Then
            _ignoreZoomChange = True

            'First rotate the elements in the array one step backwards
            tempExtent = _previousExtent(0)
            For i = 0 To _previousExtent.Length - 2
                _previousExtent(i) = _previousExtent(i + 1)
            Next
            _previousExtent(_previousExtent.Length - 1) = tempExtent

            'Then apply previous extent (_previousExtent(0) should always contain the current extent)
            GisCtrl.VisibleExtent = _previousExtent(0)
        End If
    End Sub

    Private Sub ZoomToLayer(ByVal layerName As String, ByVal subLayerName As String)
        Dim ll As TGIS_LayerAbstract

        'Zoom to layer if found
        ll = GisCtrl.Get(layerName)
        If ll IsNot Nothing Then
            If subLayerName = vbNullString Then
                ll.RecalcExtent()
                GisCtrl.VisibleExtent = ll.ProjectedExtent
            Else
                For i As Integer = 0 To ll.SubLayers.Count - 1
                    If ll.SubLayers(i).Name = subLayerName Then
                        GisCtrl.VisibleExtent = ll.SubLayers(i).ProjectedExtent
                        Exit For
                    End If
                Next
            End If
        End If
    End Sub

    Private Sub ZoomToFullExtent()
        GisCtrl.FullExtent()
        GisCtrl.Update()
    End Sub

    Public Sub ZoomBy(ByVal delta As Double, ByVal x As Integer, ByVal y As Integer)
        'Calculate position relative to viewer
        Dim l As Integer, r As Integer, t As Integer, b As Integer
        CommandBars.GetClientRect(l, t, r, b)
        x -= l
        y -= t

        'Change zoom only when viewer is the active control
        If Me.ActiveControl Is GisCtrl Then
            'Zoom in/out
            If delta < 0 Then
                GisCtrl.ZoomBy(5 / 3, x, y)
            Else
                GisCtrl.ZoomBy(3 / 5, x, y)
            End If
        End If
    End Sub

    Public Sub ZoomToWidget(ByVal id As Integer, ByVal type As RelationType, ByVal updateStandCoords As Boolean)
        Dim shapeId As Integer = -1
        Dim layerName As String = vbNullString

        'Check if this stand exist in any layer
        FindLayerAndShape(id, type, layerName, shapeId)

        If shapeId <> -1 AndAlso Not updateStandCoords Then
            'Find related shape and zoom in to it
            Dim ll As TGIS_LayerVector = GisCtrl.Get(layerName)
            If ll IsNot Nothing Then
                Dim shp As TGIS_Shape = ll.GetShape(shapeId)
                If shp IsNot Nothing Then
                    If shp.ShapeType <> TGIS_ShapeType.gisShapeTypePoint Then
                        GisCtrl.VisibleExtent = shp.ProjectedExtent
                    Else
                        'Point has no extent so just center on it instead
                        NavigateToExtent(shp.ProjectedExtent)
                    End If
                    _viewportInitialized = True 'Set this so we won't jump back to last viewport in case window was opened from another module
                End If
                SetActiveLayer(ll)
            End If
        Else
            'Generate a shape for this widget if possible
            Dim ret As Boolean
            Select Case type
                Case RelationType.ObjectRelation, RelationType.StandRelation, RelationType.SectionRelation
                    ret = ImportStandShapes(id, type)
                Case RelationType.TCPlotRelation
                    ret = GenerateShapeFromTcTract(id)
            End Select

            If ret = False Then
                'Let user draw the boundaries or pick an existing shape (if shape wasn't automatically created)
                MsgBox(LangStr(_strIdMissingShapeOnMap), MsgBoxStyle.Information)
                _applyRelationId = id 'This will enable 'Apply relation'
                _applyRelationType = type

                Dim l As Integer = 0, r As Integer = 0, t As Integer = 0, b As Integer = 0
                Dim pos As Point

                'Determine position of relation toolbar
                CommandBars(4).Visible = True   'Make sure this tool is visible
                CommandBars(4).GetWindowRect(l, t, r, b)
                pos = New Point(l, b)
                pos = PointToClient(pos)
                pos.X += (r - l) / 2
                pos.Y -= 10

                'Show balloon tip
                Dim e As New RelationTooltipEventArgs(pos)
                RaiseEvent ShowRelationTooltip(Me, e)
            End If
        End If
    End Sub

    Friend Sub RemoveLayer(ByVal layerName As String)
        Dim ll As TGIS_LayerAbstract
        Dim shp As TGIS_Shape
        Dim type As RelationType
        Dim cmd As New OleDbCommand("", Conn)

        Dim oldCursor As Cursor = GisCtrl.Cursor
        GisCtrl.Cursor = Cursors.WaitCursor

        ll = GisCtrl.Get(layerName)

        If ll IsNot Nothing Then
            'Make sure editor is not active
            EndEditing()

            'Remove any thumbnail images for stand/object layers
            type = GetLayerRelationType(ll.Name)
            If type = RelationType.StandRelation Then
                Dim lv As TGIS_LayerVector = ll

                'Loop through any shapes that are related to a stand
                shp = lv.FindFirst(ll.ProjectedExtent, FieldNameRelation & " IS NOT NULL")
                Do Until shp Is Nothing
                    cmd.CommandText = "UPDATE esti_trakt_table SET trakt_img = NULL WHERE trakt_id = " & shp.GetField(FieldNameRelation)
                    cmd.ExecuteNonQuery()
                    shp = lv.FindNext()
                Loop
            ElseIf type = RelationType.ObjectRelation Then
                Dim lv As TGIS_LayerVector = ll

                'Loop through any shapes that are related to an object
                shp = lv.FindFirst(ll.ProjectedExtent, FieldNameRelation & " IS NOT NULL")
                Do Until shp Is Nothing
                    cmd.CommandText = "UPDATE elv_object_table SET object_img = NULL WHERE object_id = " & shp.GetField(FieldNameRelation)
                    cmd.ExecuteNonQuery()
                    shp = lv.FindNext()
                Loop
            ElseIf type = RelationType.PropertyRelation Then    '#3724
                Dim lv As TGIS_LayerVector = ll

                'Loop through any shapes that are related to an object
                shp = lv.FindFirst(ll.ProjectedExtent, FieldNameRelation & " IS NOT NULL")
                Do Until shp Is Nothing
                    cmd.CommandText = "UPDATE fst_property_table SET prop_coord = NULL WHERE id = " & shp.GetField(FieldNameRelation)
                    cmd.ExecuteNonQuery()
                    shp = lv.FindNext()
                Loop
            End If

            'Check if this layer is stored in db or on disk
            If TypeOf ll Is TGIS_LayerSqlAdo Or TypeOf ll Is TGIS_LayerPixelStoreAdo2 Then
                'Remove layer from db
                If TypeOf ll Is TGIS_LayerVector Then
                    'Typical shape layer
                    cmd.CommandText = "DELETE FROM ttkGISLayerSQL WHERE name LIKE '" & GisTablePrefix & ll.Name & "'"
                    cmd.ExecuteNonQuery()

                    cmd.CommandText = "IF EXISTS (SELECT 1 FROM sysobjects WHERE name LIKE N'" & GisTablePrefix & ll.Name & "_GEO' AND xtype='U') DROP TABLE " & GisTablePrefix & ll.Name & "_GEO"
                    cmd.ExecuteNonQuery()

                    cmd.CommandText = "IF EXISTS (SELECT 1 FROM sysobjects WHERE name LIKE N'" & GisTablePrefix & ll.Name & "_FEA' AND xtype='U') DROP TABLE " & GisTablePrefix & ll.Name & "_FEA"
                    cmd.ExecuteNonQuery()
                ElseIf TypeOf ll Is TGIS_LayerPixelStoreAdo2 Then
                    'PixelStore2 layer
                    cmd.CommandText = "DELETE FROM ttkGISPixelStore2 WHERE name LIKE '" & GisTablePrefix & ll.Name & "'"
                    cmd.ExecuteNonQuery()

                    cmd.CommandText = "IF EXISTS (SELECT 1 FROM sysobjects WHERE name LIKE N'" & GisTablePrefix & ll.Name & "' AND xtype='U') DROP TABLE " & GisTablePrefix & ll.Name
                    cmd.ExecuteNonQuery()
                End If
            End If

            'Remove from index table
            cmd.CommandText = "DELETE FROM gis_layers WHERE name LIKE '" & ll.Name & "'"
            cmd.ExecuteNonQuery()

            'Remove layer from viewer
            GisCtrl.Delete(ll.Name) 'Note! Delete function is case sensitive, use ll.Name

            'Clear field data viewer if we're deleting the active layer
            If ll.Name = _activeLayerName OrElse ll.Name = _activeSubLayerName Then
                'No active layer
                SetActiveLayer(Nothing)
                ListFieldData()
            End If

            GisCtrl.SaveProject()
            GisCtrl.Update()
        End If

        GisCtrl.Cursor = oldCursor
    End Sub

    Private Sub ShowSettingsDialog()
        Dim dlg As New SettingsDialog(ControlScale.Units, _areaUnits, GisCtrl.CS, _projectPath, GetEditMode(), GisCtrl.Editor.SnapType, _routeDirection, ControlNorthArrow.Visible, _maprotation)
        If dlg.ShowDialog() = DialogResult.OK Then
            'Apply any changes
            SetScaleUnits(dlg.LengthUnits, dlg.AreaUnits)
            SetViewerCS(dlg.CS)
            SetProjectPath(dlg.ProjectDir)
            SetEditMode(dlg.EditMode)
            SetSnapType(dlg.SnapType)
            SetRouteDirection(dlg.RouteDirection)
            SetNorthArrow(dlg.NorthArrowVisible)
            SetMapRotation(dlg.MapRotation)


            'Save changes
            GisCtrl.SaveProject()
            GisCtrl.Update()
        End If
    End Sub

    Private Sub SetScaleUnits(ByVal lu As TGIS_CSUnits, ByVal au As TGIS_CSUnits)
        'Apply selected unit type
        ControlScale.Units = lu
        _areaUnits = au

        'Save new value to registry
        Dim regKey As RegistryKey = Registry.CurrentUser.OpenSubKey(RegKeyGis, True)
        If regKey IsNot Nothing Then
            regKey.SetValue(RegValLengthUnits, lu.EPSG)
            regKey.SetValue(RegValAreaUnits, au.EPSG)
            regKey.Close()
        End If
    End Sub

    Private Sub SetViewerCS(ByVal cs As TGIS_CSCoordinateSystem)
        Dim cmd As New OleDbCommand("", Conn)
        Dim i As Integer
        Dim ll As TGIS_LayerAbstract

        'We don't need to do anything if cs is not changed
        If cs.EPSG <> GisCtrl.CS.EPSG Then
            'Check if any layer is missing a proper coordinate system
            For i = 0 To GisCtrl.Items.Count - 1
                If CType(GisCtrl.Items(i), TGIS_LayerAbstract).CS.EPSG = 0 Then
                    If MsgBox(LangStr(_strIdLayersMissingCS), MsgBoxStyle.Question Or MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub
                    Exit For
                End If
            Next

            'Apply selected settings
            Try
                GisCtrl.CS = cs
            Catch ex As EGIS_Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                Exit Sub
            End Try

            'Save cs setting to db
            cmd.CommandText = "IF EXISTS(SELECT 1 FROM gis_layers WHERE name = '" & ViewerWndName & "') " & _
                              "UPDATE gis_layers SET epsg = " & cs.EPSG & " WHERE name = '" & ViewerWndName & "' " & _
                              "ELSE " & _
                              "INSERT INTO gis_layers (name, caption, epsg) VALUES('" & ViewerWndName & "', '', " & cs.EPSG & ")"
            cmd.ExecuteNonQuery()

            'Apply to any layer missing a proper coordinate system
            For i = 0 To GisCtrl.Items.Count - 1
                ll = CType(GisCtrl.Items(i), TGIS_LayerAbstract)
                If ll.CS.EPSG = 0 Then
                    ll.CS = cs
                End If
            Next

            'Display current layers coordinate system
            ll = GetActiveLayer()
            If ll IsNot Nothing Then _coordinateSystemPane.Text = ll.CS.WKT
        End If
    End Sub

    Private Sub SetProjectPath(ByVal newDir As String)
        'Set up new project path
        Dim regKey As RegistryKey = Registry.CurrentUser.OpenSubKey(RegKeyDB, False)
        Dim server As String = vbNullString
        Dim database As String = vbNullString

        If regKey IsNot Nothing Then
            server = regKey.GetValue("DBLocation")
            database = regKey.GetValue("DBSelected")
            regKey.Close()
        End If

        Dim oldProjectPath As String = _projectPath
        _projectPath = newDir & "\" & FilterPath(server) & "-" & FilterPath(database) & ".ttkgp"

        'We don't need to do anything if the path is not changed
        If String.Compare(_projectPath, oldProjectPath, True) <> 0 Then
            If File.Exists(_projectPath) Then
                Select Case MsgBox(LangStr(_strIdProjectFileAlreadyExist), MsgBoxStyle.Question Or MsgBoxStyle.YesNoCancel)
                    Case MsgBoxResult.Yes
                        'Load existing project file
                        If Not OpenProject(True, True) Then 'Strict
                            'Let user decide whether to load layers that can be found only
                            If MsgBox(LangStr(_strIdSomeLayersDoesntExist), MsgBoxStyle.Exclamation Or MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                                'Reload previous project file
                                _projectPath = oldProjectPath
                            End If
                            OpenProject(False, True)
                        End If

                        If _projectPath <> oldProjectPath Then
                            GisCtrl.SaveProject() 'Make sure project file is up-to-date
                        End If

                    Case MsgBoxResult.No
                        'Apply new path
                        GisCtrl.SaveProjectAs(_projectPath)

                    Case MsgBoxResult.Cancel
                        Exit Sub
                End Select
            Else
                'Apply new path
                GisCtrl.SaveProjectAs(_projectPath)
            End If

            'Store new path in registry
            regKey = Registry.CurrentUser.OpenSubKey(RegKeyGis, True)
            If regKey IsNot Nothing Then
                regKey.SetValue(RegValProjectDir, Path.GetDirectoryName(_projectPath))
            End If
        End If
    End Sub

    Private Sub SetUpVisualizationParams()
        Dim cmd As New OleDbCommand("", Conn)
        Dim reader As OleDbDataReader = Nothing
        Dim da As New OleDbDataAdapter
        Dim ds As DataSet
        Dim ll As TGIS_LayerVector
        Dim l1 As TGIS_LayerAbstract
        Dim i As Integer, j As Integer
        Dim lang As Integer
        Dim viewname As String

        Dim oldCursor As Cursor = GisCtrl.Cursor
        GisCtrl.Cursor = Cursors.WaitCursor

        'Prepare data tables
        For i = 0 To Views.Count - 1
            'Create a data table for each view
            cmd.CommandText = "SELECT 1 FROM sysobjects WHERE name LIKE '" & Views(i).Name & "' AND xtype = 'V'"
            reader = cmd.ExecuteReader()
            If reader.Read() Then
                reader.Close()
                cmd.CommandText = "SELECT * FROM " & Views(i).Name & " ORDER BY " & ForeignKeyField
                da.SelectCommand = cmd
                ds = New DataSet
                ds.Locale = CultureInfo.InvariantCulture
                da.Fill(ds)
                For j = 0 To ds.Tables(0).Columns.Count - 1 'Set up any language dependent column names
                    Integer.TryParse(ds.Tables(0).Columns(j).Caption, lang)
                    If lang > 0 AndAlso LangStr(lang) IsNot Nothing Then
                        ds.Tables(0).Columns(j).Caption = LangStr(CInt(ds.Tables(0).Columns(j).Caption))
                        ds.Tables(0).Columns(j).ColumnName = ds.Tables(0).Columns(j).Caption
                    End If
                Next
                ds.Tables(0).PrimaryKey = New DataColumn() {ds.Tables(0).Columns(0)}
                Views(i).JoinDT = ds.Tables(0)
            End If
            reader.Close()
        Next

        'Assign joined data tables to layers
        cmd.CommandText = "SELECT name, join_view FROM gis_layers"
        reader = cmd.ExecuteReader()
        Do While reader.Read()
            'Makes sure specified view is valid
            If reader.IsDBNull(1) OrElse reader.GetString(1) = vbNullString Then Continue Do
            viewname = reader.GetString(1)

            'Make sure layer still exist in viewer and it's a layer vector
            l1 = GisCtrl.Get(reader.GetString(0))
            If l1 Is Nothing OrElse Not TypeOf l1 Is TGIS_LayerVector Then Continue Do
            ll = CType(l1, TGIS_LayerVector)

            'Set up visualization params
            ll.JoinNET = GetJoinDT(viewname)
            ll.JoinPrimary = GetJoinPrimary(viewname)
            ll.JoinForeign = ForeignKeyField
        Loop
        reader.Close()

        GisCtrl.Update()

        GisCtrl.Cursor = oldCursor
    End Sub

    Public Sub SetRunningEstimateSodra()
        'Public access function to enable specific features
        EstimateSodraInstalled = True
    End Sub

    Private Function ShowPropertiesDialog(ByVal layerName As String, ByVal subLayerName As String) As DialogResult
        Dim ll As TGIS_LayerAbstract
        Dim grp As String = vbNullString
        Dim result As DialogResult
        Dim rt As RelationType = RelationType.Undefined, oldRt As RelationType = RelationType.Undefined
        Dim jv As String = vbNullString, oldJv As String = vbNullString

        'Get layer
        ll = GisCtrl.Get(layerName)
        If subLayerName <> vbNullString Then
            For i As Integer = 0 To ll.SubLayers.Count - 1
                If ll.SubLayers.Items(i).Name = subLayerName Then
                    ll = ll.SubLayers.Items(i)
                    Exit For
                End If
            Next
        End If

        'If layer doesn't exist - check if this is a group
        If ll Is Nothing Then
            If GisCtrl.Hierarchy.Groups(layerName) Is Nothing Then Return DialogResult.Cancel
            grp = layerName
        Else
            'Determine joined view before bringing up the dialog
            GetLayerRelationType(ll.Name, oldRt, oldJv)
        End If

        'Make sure to end editing (saving layer properties will invalidate any shape instance)
        EndEditing()

        'Save layer properties on OK
        Dim dlg As New LayerPropertiesDialog(ll, grp)
        result = dlg.ShowDialog()
        If result = DialogResult.OK Then
            If ll IsNot Nothing Then
                GetLayerRelationType(ll.Name, rt, jv)

                'Update visualization if relation or joined view was changed
                If rt <> oldRt OrElse jv <> oldJv Then
                    SetUpVisualizationParams()
                    If ll.Name = _activeLayerName OrElse ll.Name = _activeSubLayerName Then SetActiveLayer(ll) 'Also update menu items (if this is the active layer)
                End If

                'Update field data viewer if joined view was changed
                If jv <> oldJv Then
                    ListFieldData()
                End If

                'Display current coordinate system
                _coordinateSystemPane.Text = ll.CS.WKT

                'Refresh snap layer list (in case of a title change)
                ListSnapLayers()

                'Layer legend and viewer don't need to be updated upon editing a group
                ControlLegend.Update()
                GisCtrl.Update()
            End If

            ControlHierarchy.Update()
            GisCtrl.SaveProject()
        End If

        Return result
    End Function

    Public Sub SetUpNewRelation(ByVal id As Integer, ByVal rt As RelationType)
        Dim ll As TGIS_LayerVector = Nothing
        Dim shp As TGIS_Shape

        'Use specified destination layer (this call should be done immediately after widget has been created)
        Select Case rt
            Case RelationType.ObjectRelation
                ll = GisCtrl.Get(_objectLayerName)
                If ll IsNot Nothing Then
                    'Set up the relation for each shape
                    For Each val As IntIntPair In _objectShapePropertyList
                        shp = ll.GetShape(val.Val1).MakeEditable()
                        If shp IsNot Nothing Then
                            shp.SetField(FieldNameRelation, id)  'Object id
                            shp.SetField(FieldNameSubrelation, id & "/" & val.Val2) 'Combined 'ObjectId/PropertyId'
                        End If
                    Next
                End If

            Case RelationType.StandRelation
                ll = TryCast(GetActiveLayer(), TGIS_LayerVector)
                If ll IsNot Nothing Then
                    'Set up relation for selected shape
                    shp = ll.GetShape(_standShapeId).MakeEditable()
                    If shp IsNot Nothing Then
                        shp.SetField(FieldNameRelation, id) 'Stand id
                    End If
                End If
        End Select

        If ll IsNot Nothing Then ll.SaveAll()
    End Sub

    Public Sub SetUpNewSectionRelation(ByVal sectionId As Integer, ByVal shapeId As Integer)
        Dim ll As TGIS_LayerVector
        Dim shp As TGIS_Shape

        'Used to set up section relations, should be called immediately after section has been created
        ll = GisCtrl.Get(_sectionDestinationLayer) 'Use specifiec section layer instead of active layer as this call may be caused by an import to another layer
        If ll IsNot Nothing Then
            shp = ll.GetShape(shapeId)
            If shp IsNot Nothing Then
                shp.MakeEditable().SetField(FieldNameRelation, sectionId) 'Section id
            End If
            ll.SaveAll()
        End If
    End Sub

    Private Function ImportStandShapes(ByVal id As Integer, ByVal rt As RelationType) As Boolean
        Dim msgdlg As New MessageBoxCheck
        Dim dlg As New ImportStandDataDialog
        Dim coords As String(), latlong As String()
        Dim sqlStand As String = vbNullString, sqlPlot As String = vbNullString, sqlTree As String = vbNullString
        Dim cmd As New OleDbCommand("", Conn)
        Dim da As New OleDbDataAdapter(cmd)
        Dim dsStand As New DataSet, dsPlot As New DataSet, dsTree As New DataSet
        Dim row As System.Data.DataRow
        Dim ll As TGIS_LayerVector
        Dim shp As TGIS_Shape = Nothing
        Dim d As Double = 0
        Dim extStandPoint As New TGIS_Extent(Double.MaxValue, Double.MaxValue, Double.MinValue, Double.MinValue)
        Dim extStandPoly As New TGIS_Extent(Double.MaxValue, Double.MaxValue, Double.MinValue, Double.MinValue)
        Dim extTreePoint As New TGIS_Extent(Double.MaxValue, Double.MaxValue, Double.MinValue, Double.MinValue)
        Dim extPlotPoint As New TGIS_Extent(Double.MaxValue, Double.MaxValue, Double.MinValue, Double.MinValue)

        'Query SQL
        Select Case rt
            Case RelationType.ObjectRelation
                sqlStand = "SELECT trakt_id, trakt_point, trakt_coordinates " & _
                           "FROM elv_cruise_table " & _
                           "INNER JOIN esti_trakt_table " & _
                           "ON ecru_id = trakt_id " & _
                           "WHERE ecru_object_id = " & id & " AND ((trakt_point <> '' AND trakt_point IS NOT NULL) OR trakt_coordinates IS NOT NULL)"

                sqlPlot = "SELECT tplot_trakt_id, tplot_id, tplot_coord " & _
                          "FROM elv_cruise_table " & _
                          "INNER JOIN esti_trakt_plot_table " & _
                          "ON ecru_id = tplot_trakt_id " & _
                          "WHERE tplot_trakt_id = " & id & " AND tplot_coord <> '' AND tplot_coord IS NOT NULL"

                sqlTree = "SELECT ttree_trakt_id, ttree_id, ttree_coord " & _
                          "FROM elv_cruise_table " & _
                          "INNER JOIN esti_trakt_sample_trees_table " & _
                          "ON ecru_id = ttree_trakt_id " & _
                          "WHERE ecru_object_id = " & id & " AND ttree_coord <> '' AND ttree_coord IS NOT NULL"

            Case RelationType.StandRelation, RelationType.SectionRelation
                sqlStand = "SELECT trakt_id, trakt_point, trakt_coordinates " & _
                           "FROM esti_trakt_table " & _
                           "WHERE trakt_id = " & id & " AND ((trakt_point <> '' AND trakt_point IS NOT NULL) OR trakt_coordinates IS NOT NULL)"

                sqlPlot = "SELECT tplot_trakt_id, tplot_id, tplot_coord " & _
                          "FROM esti_trakt_plot_table " & _
                          "WHERE tplot_trakt_id = " & id & " AND tplot_coord <> '' AND tplot_coord IS NOT NULL"

                sqlTree = "SELECT ttree_trakt_id, ttree_id, ttree_coord " & _
                          "FROM esti_trakt_sample_trees_table " & _
                          "WHERE ttree_trakt_id = " & id & " AND ttree_coord <> '' AND ttree_coord IS NOT NULL"

            Case Else
                Return False
        End Select

        da.SelectCommand.CommandText = sqlStand
        da.Fill(dsStand)

        da.SelectCommand.CommandText = sqlPlot
        da.Fill(dsPlot)

        da.SelectCommand.CommandText = sqlTree
        da.Fill(dsTree)

        'Let user choose destination layers
        If dlg.ShowDialog() <> DialogResult.OK Then Return False

        'Import data
        If dlg.StandPointLayer <> vbNullString Then
            ll = Nothing
            For Each row In dsStand.Tables(0).Rows
                If Not IsDBNull(row.Item(1)) AndAlso row.Item(1) <> vbNullString Then
                    coords = row.Item(1).Split(",")
                    If coords.Length = 2 AndAlso _
                    Double.TryParse(coords(0), NumberStyles.Float, CultureInfo.InvariantCulture, d) AndAlso d <> 0 AndAlso _
                    Double.TryParse(coords(1), NumberStyles.Float, CultureInfo.InvariantCulture, d) AndAlso d <> 0 Then
                        'Assign layer
                        If ll Is Nothing Then
                            If dlg.StandPointLayer <> NewLayerName Then
                                'Remove any existing data from this stand in layer
                                For Each r As System.Data.DataRow In dsStand.Tables(0).Rows
                                    cmd.CommandText = "DELETE FROM " & GisTablePrefix & dlg.StandPointLayer & "_GEO WHERE uid IN " & _
                                                      "(SELECT uid FROM " & GisTablePrefix & dlg.StandPointLayer & "_FEA WHERE " & FieldNameRelation & " = " & r.Item(0) & ")"
                                    cmd.ExecuteNonQuery()

                                    cmd.CommandText = "DELETE FROM " & GisTablePrefix & dlg.StandPointLayer & "_FEA WHERE " & FieldNameRelation & " = " & r.Item(0)
                                    cmd.ExecuteNonQuery()
                                Next

                                'Use existing layer
                                ll = GisCtrl.Get(dlg.StandPointLayer)
                            Else
                                'Create new layer
                                If EstimateSodraInstalled Then
                                    ll = GisCtrl.Get(AddNewLayer(False, vbNullString, RelationType.SectionRelation, ViewNameSectionData, LangStr(_strIdLayerTitleStandPoint)))
                                Else
                                    ll = GisCtrl.Get(AddNewLayer(False, vbNullString, RelationType.StandRelation, ViewNameStandData, LangStr(_strIdLayerTitleStandPoint)))
                                End If
                            End If
                        End If

                        'Create a new point for each stand
                        shp = ll.CreateShape(TGIS_ShapeType.gisShapeTypePoint)
                        shp.AddPart()
                        shp.AddPoint(LatLongDdMmmmmmToCS(coords(0), coords(1), GisCtrl.CS))
                        shp.SetField(FieldNameRelation, row.Item(0)) 'Stand id

                        'Store extent
                        If shp.ProjectedExtent.XMin < extStandPoint.XMin Then extStandPoint.XMin = shp.ProjectedExtent.XMin
                        If shp.ProjectedExtent.XMax > extStandPoint.XMax Then extStandPoint.XMax = shp.ProjectedExtent.XMax
                        If shp.ProjectedExtent.YMin < extStandPoint.YMin Then extStandPoint.YMin = shp.ProjectedExtent.YMin
                        If shp.ProjectedExtent.YMax > extStandPoint.YMax Then extStandPoint.YMax = shp.ProjectedExtent.YMax
                    End If
                End If
            Next

            If ll IsNot Nothing Then ll.SaveAll()
        End If

        If dlg.StandPolygonLayer <> vbNullString Then
            ll = Nothing
            For Each row In dsStand.Tables(0).Rows
                If Not IsDBNull(row.Item(2)) AndAlso row.Item(2) <> vbNullString Then
                    latlong = row.Item(2).Split(vbCrLf)
                    For Each pts As String In latlong
                        coords = pts.Split(",")
                        If coords.Length = 2 AndAlso _
                        Double.TryParse(coords(0), NumberStyles.Float, CultureInfo.InvariantCulture, d) AndAlso d <> 0 AndAlso _
                        Double.TryParse(coords(1), NumberStyles.Float, CultureInfo.InvariantCulture, d) AndAlso d <> 0 Then
                            'Assign layer
                            If ll Is Nothing Then
                                If dlg.StandPolygonLayer <> NewLayerName Then
                                    'Remove any existing data from this stand in layer
                                    For Each r As System.Data.DataRow In dsStand.Tables(0).Rows
                                        cmd.CommandText = "DELETE FROM " & GisTablePrefix & dlg.StandPolygonLayer & "_GEO WHERE uid IN " & _
                                                          "(SELECT uid FROM " & GisTablePrefix & dlg.StandPolygonLayer & "_FEA WHERE " & FieldNameRelation & " = " & r.Item(0) & ")"
                                        cmd.ExecuteNonQuery()

                                        cmd.CommandText = "DELETE FROM " & GisTablePrefix & dlg.StandPolygonLayer & "_FEA WHERE " & FieldNameRelation & " = " & r.Item(0)
                                        cmd.ExecuteNonQuery()
                                    Next

                                    'Use existing layer
                                    ll = GisCtrl.Get(dlg.StandPolygonLayer)
                                Else
                                    'Create new layer
                                    If EstimateSodraInstalled Then
                                        ll = GisCtrl.Get(AddNewLayer(False, vbNullString, RelationType.SectionRelation, ViewNameSectionData, LangStr(_strIdLayerTitleStandPolygon)))
                                    Else
                                        ll = GisCtrl.Get(AddNewLayer(False, vbNullString, RelationType.StandRelation, ViewNameStandData, LangStr(_strIdLayerTitleStandPolygon)))
                                    End If
                                End If
                            End If

                            'Create a new shape for this stand
                            If shp Is Nothing Then
                                shp = ll.CreateShape(TGIS_ShapeType.gisShapeTypePolygon)
                                shp.AddPart()
                                shp.SetField(FieldNameRelation, row.Item(0)) 'Stand id
                            End If

                            'Add point to shape
                            If shp IsNot Nothing Then shp.AddPoint(LatLongDdMmmmmmToCS(coords(0), coords(1), GisCtrl.CS))

                            'Store extent
                            If shp.ProjectedExtent.XMin < extStandPoly.XMin Then extStandPoly.XMin = shp.ProjectedExtent.XMin
                            If shp.ProjectedExtent.XMax > extStandPoly.XMax Then extStandPoly.XMax = shp.ProjectedExtent.XMax
                            If shp.ProjectedExtent.YMin < extStandPoly.YMin Then extStandPoly.YMin = shp.ProjectedExtent.YMin
                            If shp.ProjectedExtent.YMax > extStandPoly.YMax Then extStandPoly.YMax = shp.ProjectedExtent.YMax
                        End If
                    Next
                End If
            Next

            If ll IsNot Nothing Then ll.SaveAll()
        End If

        If dlg.SampleTreePointLayer <> vbNullString Then
            ll = Nothing
            For Each row In dsTree.Tables(0).Rows
                If Not IsDBNull(row.Item(2)) AndAlso row.Item(2) <> vbNullString Then
                    coords = row.Item(2).Split(",")
                    If coords.Length = 2 AndAlso _
                    Double.TryParse(coords(0), NumberStyles.Float, CultureInfo.InvariantCulture, d) AndAlso d <> 0 AndAlso _
                    Double.TryParse(coords(1), NumberStyles.Float, CultureInfo.InvariantCulture, d) AndAlso d <> 0 Then
                        'Assign layer
                        If ll Is Nothing Then
                            If dlg.SampleTreePointLayer <> NewLayerName Then
                                'Remove any existing data from this stand in layer
                                For Each r As System.Data.DataRow In dsTree.Tables(0).Rows
                                    cmd.CommandText = "DELETE FROM " & GisTablePrefix & dlg.SampleTreePointLayer & "_GEO WHERE uid IN " & _
                                                      "(SELECT uid FROM " & GisTablePrefix & dlg.SampleTreePointLayer & "_FEA WHERE " & FieldNameRelation & " = " & r.Item(0) & ")"
                                    cmd.ExecuteNonQuery()

                                    cmd.CommandText = "DELETE FROM " & GisTablePrefix & dlg.SampleTreePointLayer & "_FEA WHERE " & FieldNameRelation & " = " & r.Item(0)
                                    cmd.ExecuteNonQuery()
                                Next

                                'Use existing layer
                                ll = GisCtrl.Get(dlg.SampleTreePointLayer)
                            Else
                                'Create new layer
                                ll = GisCtrl.Get(AddNewLayer(False, vbNullString, RelationType.SampleTreeRelation, ViewNameSampleTreeData, LangStr(_strIdLayerTitleSampleTreePoint)))
                            End If
                        End If

                        'Create a new point for each tree
                        shp = ll.CreateShape(TGIS_ShapeType.gisShapeTypePoint)
                        shp.AddPart()
                        shp.AddPoint(LatLongDdMmmmmmToCS(coords(0), coords(1), GisCtrl.CS))
                        shp.SetField(FieldNameRelation, row.Item(0)) 'Stand id
                        shp.SetField(FieldNameSubrelation, row.Item(0) & "/" & row.Item(1)) 'Combined stand id/tree id

                        'Store extent
                        If shp.ProjectedExtent.XMin < extTreePoint.XMin Then extTreePoint.XMin = shp.ProjectedExtent.XMin
                        If shp.ProjectedExtent.XMax > extTreePoint.XMax Then extTreePoint.XMax = shp.ProjectedExtent.XMax
                        If shp.ProjectedExtent.YMin < extTreePoint.YMin Then extTreePoint.YMin = shp.ProjectedExtent.YMin
                        If shp.ProjectedExtent.YMax > extTreePoint.YMax Then extTreePoint.YMax = shp.ProjectedExtent.YMax
                    End If
                End If
            Next

            If ll IsNot Nothing Then ll.SaveAll()
        End If

        If dlg.PlotPointLayer <> vbNullString Then
            ll = Nothing
            For Each row In dsPlot.Tables(0).Rows
                If Not IsDBNull(row.Item(2)) AndAlso row.Item(2) <> vbNullString Then
                    coords = row.Item(2).Split(",")
                    If coords.Length = 2 AndAlso _
                    Double.TryParse(coords(0), NumberStyles.Float, CultureInfo.InvariantCulture, d) AndAlso d <> 0 AndAlso _
                    Double.TryParse(coords(1), NumberStyles.Float, CultureInfo.InvariantCulture, d) AndAlso d <> 0 Then
                        'Assign layer
                        If ll Is Nothing Then
                            If dlg.PlotPointLayer <> NewLayerName Then
                                'Remove any existing data from this stand in layer
                                For Each r As System.Data.DataRow In dsPlot.Tables(0).Rows
                                    cmd.CommandText = "DELETE FROM " & GisTablePrefix & dlg.PlotPointLayer & "_GEO WHERE uid IN " & _
                                                      "(SELECT uid FROM " & GisTablePrefix & dlg.PlotPointLayer & "_FEA WHERE " & FieldNameRelation & " = " & r.Item(0) & ")"
                                    cmd.ExecuteNonQuery()

                                    cmd.CommandText = "DELETE FROM " & GisTablePrefix & dlg.PlotPointLayer & "_FEA WHERE " & FieldNameRelation & " = " & r.Item(0)
                                    cmd.ExecuteNonQuery()
                                Next

                                'Use existing layer
                                ll = GisCtrl.Get(dlg.PlotPointLayer)
                            Else
                                'Create new layer
                                If EstimateSodraInstalled Then
                                    ll = GisCtrl.Get(AddNewLayer(False, vbNullString, RelationType.SectionRelation, vbNullString, LangStr(_strIdLayerTitlePlotPoint)))
                                Else
                                    ll = GisCtrl.Get(AddNewLayer(False, vbNullString, RelationType.StandRelation, vbNullString, LangStr(_strIdLayerTitlePlotPoint)))
                                End If
                            End If
                        End If

                        'Create a new point for each plot
                        shp = ll.CreateShape(TGIS_ShapeType.gisShapeTypePoint)
                        shp.AddPart()
                        shp.AddPoint(LatLongDdMmmmmmToCS(coords(0), coords(1), GisCtrl.CS))
                        shp.SetField(FieldNameRelation, row.Item(0)) 'Stand id
                        shp.SetField(FieldNameSubrelation, row.Item(0) & "/" & row.Item(1)) 'Combined stand id/plot id

                        'Store extent
                        If shp.ProjectedExtent.XMin < extPlotPoint.XMin Then extPlotPoint.XMin = shp.ProjectedExtent.XMin
                        If shp.ProjectedExtent.XMax > extPlotPoint.XMax Then extPlotPoint.XMax = shp.ProjectedExtent.XMax
                        If shp.ProjectedExtent.YMin < extPlotPoint.YMin Then extPlotPoint.YMin = shp.ProjectedExtent.YMin
                        If shp.ProjectedExtent.YMax > extPlotPoint.YMax Then extPlotPoint.YMax = shp.ProjectedExtent.YMax
                    End If
                End If
            Next

            If ll IsNot Nothing Then ll.SaveAll()
        End If

        'Zoom to created shapes (in the following order: stand polygon, plot points, sample tree points, stand point)
        If extStandPoly.XMin < Double.MaxValue AndAlso extStandPoly.XMax > Double.MinValue AndAlso extStandPoly.YMin < Double.MaxValue AndAlso extStandPoly.YMax > Double.MinValue Then
            NavigateToExtent(extStandPoly)
        ElseIf extPlotPoint.XMin < Double.MaxValue AndAlso extPlotPoint.XMax > Double.MinValue AndAlso extPlotPoint.YMin < Double.MaxValue AndAlso extPlotPoint.YMax > Double.MinValue Then
            NavigateToExtent(extPlotPoint)
        ElseIf extTreePoint.XMin < Double.MaxValue AndAlso extTreePoint.XMax > Double.MinValue AndAlso extTreePoint.YMin < Double.MaxValue AndAlso extTreePoint.YMax > Double.MinValue Then
            NavigateToExtent(extTreePoint)
        ElseIf extStandPoint.XMin < Double.MaxValue AndAlso extStandPoint.XMax > Double.MinValue AndAlso extStandPoint.YMin < Double.MaxValue AndAlso extStandPoint.YMax > Double.MinValue Then
            NavigateToExtent(extStandPoint)
        End If

        Return True
    End Function
#End Region

#Region "Event handlers"
    Private Sub GisControlView_HandleDestroyed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.HandleDestroyed
        Dim regKey As RegistryKey

        _quitting = True

        'Make sure project file is up-to-date
        GisCtrl.SaveProject()

        'Save project file to database
        SaveProjectToDatabase()

        'Close main connection to db
        If Conn IsNot Nothing Then Conn.Close()
        Conn = Nothing

        'This variable is Shared so we must clear it explicitly or it's contents will remain if user re-open this control
        Views.Clear()

        'Save viewport in registry
        regKey = Registry.CurrentUser.OpenSubKey(RegKeyGis, True)
        regKey = regKey.CreateSubKey(RegSectionViewport)
        If regKey IsNot Nothing Then
            regKey.SetValue("XMax", GisCtrl.VisibleExtent.XMax)
            regKey.SetValue("XMin", GisCtrl.VisibleExtent.XMin)
            regKey.SetValue("YMax", GisCtrl.VisibleExtent.YMax)
            regKey.SetValue("YMin", GisCtrl.VisibleExtent.YMin)
            regKey.Close()
        End If

        'Make sure to release any memory allocated by TGIS_ViewerWnd. This is also required to prevent a crash when app is closed (NDK 9.1.2)
        GisCtrl.Dispose()

        CommandBars.SaveCommandBars(RegSectionHms, RegSectionGis, RegSectionCommandBarLayout)
        DockingPaneManager.SaveState(RegSectionHms, RegSectionGis, RegSectionDockingPaneLayout)
    End Sub

    Private Sub GisControlView_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim paneA As Pane, paneB As Pane, paneC As Pane, paneD As Pane
        Dim langFile As String
        Dim server As String = vbNullString, database As String = vbNullString, usr As String = vbNullString, pwd As String = vbNullString
        Dim winAuth As Boolean
        Dim regKey As RegistryKey
        Dim xmldoc As XmlDocument, nodelist As XmlNodeList, node As XmlNode
        Dim toolbar As CommandBar, shapeToolbar As CommandBar
        Dim combo As CommandBarComboBox
        Dim popup As CommandBarPopup, popup2 As CommandBarPopup
        Dim firstStart As Boolean
        Dim dat As TGIS_CSDatum

#If DEBUG Then
        Try
            'Check version of TatukGIS_DK dll, for testing purposes (so it won't be compiled and tested against an old version of Tatuk DK)
            Dim asm As System.Reflection.Assembly = System.Reflection.Assembly.LoadFile("C:\Program Files (x86)\Haglof Sweden AB\Haglof Management Systems\TatukGIS_DK10_NET.dll")
            If asm IsNot Nothing Then
                Dim targetVersion As New Version(10, 50, 2, 15734)
                If asm.GetName().Version <> targetVersion Then
                    Throw New Exception("Incorrect version of TatukGIS_DK10_NET.dll!")
                End If
            End If
        Catch ex As System.IO.FileNotFoundException
            Throw New FileNotFoundException("Could not find TatukGIS_DK10_NET.dll!")
        End Try
#End If


        Static Initialized As Boolean = False

        'We don't want to do this every time GisControlView_Load is called
        If Not Initialized Then
            'Shared instance handle
            Instance = Me

            'Seed random number generator
            Randomize()

            'Init settings
            _customShapeUid = -1
            _selectedShapeUid = -1
            _newShapeType = -1

            ControlScale.Transparent = True
            ControlNorthArrow.Transparent = True
            GisCtrl.Editor.ShowPoints3D = False 'No need to see z coordinates
            GisCtrl.RestrictedDrag = False  'We want to drag outside the viewers extent

            'Fix RT90 datum to comply with ESRI
            dat = TGIS_Utils.CSDatumList.ByEPSG(6124)
            TGIS_Utils.CSDatumList.Fix(6124, "D_RT_1990", dat.Ellipsoid.EPSG, dat.Transform.EPSG)

            'Create registry section for app
            regKey = Registry.CurrentUser.OpenSubKey(RegKeyHms, True)
            If regKey IsNot Nothing Then
                regKey.CreateSubKey(RegSectionGis)
                regKey.Close()
            End If

            Try
                'Set up language file path
                langFile = System.AppDomain.CurrentDomain.BaseDirectory & "Language\UMGIS"
                regKey = Registry.CurrentUser.OpenSubKey(RegKeyLanguage, False)
                If regKey IsNot Nothing Then
                    langFile += regKey.GetValue("LANGSET") & ".xml"
                    regKey.Close()
                End If

                'Load language file
                xmldoc = New XmlDocument()
                xmldoc.Load(langFile)
                nodelist = xmldoc.SelectNodes("resource/string")
                For Each node In nodelist
                    LangStr(CType(node.Attributes.GetNamedItem("id").Value, Integer)) = Replace(node.Attributes.GetNamedItem("value").Value, "\n", vbCrLf)
                Next
            Catch ex As XmlException
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            End Try

            'Load last saved settings
            regKey = Registry.CurrentUser.OpenSubKey(RegKeyGis, False)
            If regKey IsNot Nothing Then
                _cachedMode = regKey.GetValue(RegValCachedMode, False)
                GisCtrl.Editor.Mode = regKey.GetValue(RegValEditMode, TGIS_EditorMode.gisEditorModeAfterActivePoint)
                GisCtrl.Editor.SnapType = regKey.GetValue(RegValSnapType, TGIS_EditorSnapType.gisEditorSnapTypePoint)
                _routeDirection = regKey.GetValue(RegValRouteDirection, RouteDirection.Row)
                Dim ulst As New TGIS_CSUnitsList
                ControlScale.Units = ulst.ByEPSG(regKey.GetValue(RegValLengthUnits, _defaultScaleUnitsEpsg))
                _areaUnits = ulst.ByEPSG(regKey.GetValue(RegValAreaUnits, _defaultScaleUnitsEpsg))
                ControlNorthArrow.Visible = regKey.GetValue(RegValNorthArrow, True)
                _maprotation = regKey.GetValue(RegValMapRotation, 0)
                regKey.Close()
            End If

            'List avaliable views
            Views.Clear()   'Make sure that Views are empty
            Dim v As JoinView
            v = New JoinView(RelationType.PropertyRelation, FieldNameRelation, ViewNamePropertyData, LangStr(_strIdViewPropertyData))
            Views.Add(v)
            v = New JoinView(RelationType.StandRelation, FieldNameSubrelation, ViewNamePlotData, LangStr(_strIdViewPlotData))
            Views.Add(v)
            v = New JoinView(RelationType.StandRelation, FieldNameRelation, ViewNameStandData, LangStr(_strIdViewStandData))
            Views.Add(v)
            v = New JoinView(RelationType.ObjectRelation, FieldNameSubrelation, ViewNameObjectData, LangStr(_strIdViewObjectData))
            Views.Add(v)
            v = New JoinView(RelationType.TCPlotRelation, FieldNameSubrelation, ViewNameTcPlotData, LangStr(_strIdViewTcPlotData))
            Views.Add(v)
            v = New JoinView(RelationType.TCTractRelation, FieldNameRelation, ViewNameTcStandData, LangStr(_strIdViewTcStandData))
            Views.Add(v)
            v = New JoinView(RelationType.StandRelation, FieldNameSubrelation, ViewNameSpeciesPerPlot, LangStr(_strIdViewSpeciesPerPlot))
            Views.Add(v)
            v = New JoinView(RelationType.StandRelation, FieldNameRelation, ViewNameSpeciesPerStand, LangStr(_strIdViewSpeciesPerStand))
            Views.Add(v)
            v = New JoinView(RelationType.SampleTreeRelation, FieldNameSubrelation, ViewNameSampleTreeData, LangStr(_strIdViewSampletreedata))
            Views.Add(v)
            v = New JoinView(RelationType.SectionRelation, FieldNameRelation, ViewNameSectionData, LangStr(_strIdViewSectionData))
            Views.Add(v)
            v = New JoinView(RelationType.SectionRelation, FieldNameSubrelation, ViewNameSectionPlotData, LangStr(_strIdViewSectionPlotData))
            Views.Add(v)

            'Set up toolbar
            LoadIcons()
            CommandBars(1).Visible = False
            CommandBars.ActiveMenuBar.DefaultButtonStyle = XTPButtonStyle.xtpButtonIcon
            CommandBars.Icons = ImageManager.Icons
            CommandBars.Options.LargeIcons = True
            CommandBars.EnableCustomization(True)
            CommandBars.EnableActions()
            DockingPaneManager.SetCommandBars(CommandBars.ActiveMenuBar.CommandBars)

            toolbar = CommandBars.Add("Tools", XTPBarPosition.xtpBarTop)
            popup = AddButton(toolbar.Controls, XTPControlType.xtpControlButtonPopup, ToolbarId.Layer, LangStr(_strIdToolAdd))
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.NewLayer, LangStr(_strIdToolNewLayer))
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.ImportLayerFromFile, LangStr(_strIdToolImportLayerFromFile))
            popup2 = popup.CommandBar.Controls.Add(XTPControlType.xtpControlButtonPopup, ToolbarId.ImportLayerFromStream, LangStr(_strIdToolImportLayerFromStream))
            popup2.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.ImportLayerFromStream1, LangStr(_strIdToolImportLayerFromStream1))
            popup2.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.ImportLayerFromStreamCustom, LangStr(_strIdToolImportLayerFromStreamCustom))
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.RemoveLayer, LangStr(_strIdToolRemoveLayer)).BeginGroup = True
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.LayerProperties, LangStr(_strIdToolLayerProperties))
            '---
            AddButton(toolbar.Controls, XTPControlType.xtpControlButton, ToolbarId.FullExtent, LangStr(_strIdToolZoomToFullExtent))
            AddButton(toolbar.Controls, XTPControlType.xtpControlButton, ToolbarId.PreviousExtent, LangStr(_strIdToolPreviousExtent))
            AddButton(toolbar.Controls, XTPControlType.xtpControlButton, ToolbarId.NavigateTo, LangStr(_strIdToolNavigateTo))
            AddButton(toolbar.Controls, XTPControlType.xtpControlButton, ToolbarId.Zoom, LangStr(_strIdToolZoom), True)
            AddButton(toolbar.Controls, XTPControlType.xtpControlButton, ToolbarId.Drag, LangStr(_strIdToolDrag))
            AddButton(toolbar.Controls, XTPControlType.xtpControlButton, ToolbarId.SelectShape, LangStr(_strIdToolSelect))
            AddButton(toolbar.Controls, XTPControlType.xtpControlButton, ToolbarId.Info, LangStr(_strIdToolViewInfo))
            popup = AddButton(toolbar.Controls, XTPControlType.xtpControlButtonPopup, ToolbarId.Measure, LangStr(_strIdToolMeasure))
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.MeasureLength, LangStr(_strIdToolLength))
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.MeasureArea, LangStr(_strIdToolArea))
            '---
            AddButton(toolbar.Controls, XTPControlType.xtpControlButton, ToolbarId.Print, LangStr(_strIdToolPrint), True)
            AddButton(toolbar.Controls, XTPControlType.xtpControlButton, ToolbarId.UpdateMap, LangStr(_strIdUpdateMap))
            popup = AddButton(toolbar.Controls, XTPControlType.xtpControlButtonPopup, ToolbarId.LandValueTools, LangStr(_strIdToolLandValueTools))
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.LandValueToolsCreateObject, LangStr(_strIdToolNewObject))
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.LandValueToolsImportProperties, LangStr(_strIdToolImportProperties))
            popup2 = popup.CommandBar.Controls.Add(XTPControlType.xtpControlButtonPopup, ToolbarId.LandValueToolsGenerateRoute, LangStr(_strIdToolGenerateRoute))
            popup2.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.LandValueToolsGenerateRouteWithoutCenterLine, LangStr(_strIdToolGenerateRouteWithoutCenterLine))
            popup2.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.LandValueToolsGenerateRouteWithCenterLine, LangStr(_strIdToolGenerateRouteWithCenterLine))
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.LandValueToolsExportRoute, LangStr(_strIdToolExportRoute))
            '---
            popup = AddButton(toolbar.Controls, XTPControlType.xtpControlButtonPopup, ToolbarId.MiscTools, LangStr(_strIdToolMiscTools))
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.MiscToolsCreateStand, LangStr(_strIdToolCreateStand))
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.MiscToolsCreateSection, LangStr(_strIdToolCreateSection)).Visible = EstimateSodraInstalled
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.MiscToolsImportProperties, LangStr(_strIdToolImportProperties))
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.MiscToolsImportPlots, LangStr(_strIdToolImportPlots)).BeginGroup = True
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.MiscToolsCreatePlots, LangStr(_strIdToolCreatePlots))
            popup2 = popup.CommandBar.Controls.Add(XTPControlType.xtpControlButtonPopup, ToolbarId.MiscToolsGenerateRoute, LangStr(_strIdToolGenerateRoute))
            popup2.BeginGroup = True
            popup2.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.MiscToolsGenerateRouteWithoutCenterLine, LangStr(_strIdToolGenerateRouteWithoutCenterLine))
            popup2.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.MiscToolsGenerateRouteWithCenterLine, LangStr(_strIdToolGenerateRouteWithCenterLine))
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.MiscToolsExportRoute, LangStr(_strIdToolExportRoute))
            popup2 = popup.CommandBar.Controls.Add(XTPControlType.xtpControlButtonPopup, ToolbarId.MiscToolsExportView, LangStr(_strIdToolExportView))
            popup2.BeginGroup = True
            popup2.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.MiscToolsExportViewToImageWithProj, LangStr(_strIdToolExportViewToImageWithProj))
            popup2.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.MiscToolsExportViewToImageWithScale, LangStr(_strIdToolExportViewToImageWithScale))
            popup2.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.MiscToolsExportViewToReport, LangStr(_strIdToolExportViewToReport))
            popup2.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.MiscToolsExportViewToSnapShot, LangStr(_strIdToolExportViewToSnapshot))
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.MiscToolsExportToShp, LangStr(_strIdToolExportShp))
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.MiscToolsExportToTCNavigator, LangStr(_strIdToolExportToTCNavigator))
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.MiscToolsSplitShape, LangStr(_strIdToolSplitShape))
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.MiscToolsUnionShapes, LangStr(_strIdToolUnionShapes))
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.MiscToolsSumAttributes, LangStr(_strIdToolSumAttributes)).BeginGroup = True
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.MiscToolsViewUnusedLayers, LangStr(_strIdToolViewUnusedLayers)).BeginGroup = True
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.MiscToolsSettings, LangStr(_strIdToolSettings))
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.MiscToolsAbout, LangStr(_strIdToolAbout)).BeginGroup = True
            '---

            toolbar = CommandBars.Add("Shape", XTPBarPosition.xtpBarTop) : shapeToolbar = toolbar 'For use below
            popup = AddButton(toolbar.Controls, XTPControlType.xtpControlButtonPopup, ToolbarId.NewShape, LangStr(_strIdToolNewShape), True)
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.NewShapePolygon, LangStr(_strIdToolPolygon))
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.NewShapeLine, LangStr(_strIdToolLine))
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.NewShapePoint, LangStr(_strIdToolPoint))
            popup.CommandBar.Controls.Add(XTPControlType.xtpControlButton, ToolbarId.NewShapeMultiPoint, LangStr(_strIdToolMultipoint))
            '---
            AddButton(toolbar.Controls, XTPControlType.xtpControlButton, ToolbarId.EditShape, LangStr(_strIdToolEditShape))
            AddButton(toolbar.Controls, XTPControlType.xtpControlButton, ToolbarId.RemoveShape, LangStr(_strIdToolRemoveShape))
            AddButton(toolbar.Controls, XTPControlType.xtpControlButton, ToolbarId.Undo, LangStr(_strIdToolUndo), True)
            AddButton(toolbar.Controls, XTPControlType.xtpControlButton, ToolbarId.Redo, LangStr(_strIdToolRedo))
            AddButton(toolbar.Controls, XTPControlType.xtpControlButton, ToolbarId.Apply, LangStr(_strIdToolApply), True)
            AddButton(toolbar.Controls, XTPControlType.xtpControlButton, ToolbarId.Revert, LangStr(_strIdToolRevert))
            '---

            toolbar = CommandBars.Add("Relation", XTPBarPosition.xtpBarTop)
            AddButton(toolbar.Controls, XTPControlType.xtpControlButton, ToolbarId.ApplyRelation, LangStr(_strIdToolApplyRelation))

            DockRightOf(CommandBars(3), CommandBars(2)) 'Edit
            DockRightOf(CommandBars(4), CommandBars(3)) 'Relation
            CommandBars(4).Visible = False 'Relation will show up when required
            '---

            'Make sure to reset toolbar layout before loading (only if layout has been changed since last version)
            UpdateToolbars()
            CommandBars.LoadCommandBars(RegSectionHms, RegSectionGis, RegSectionCommandBarLayout)
            '---

            'Add snap-to-layer combo (first we need to remove existing combo, otherwise references will be invalid so we cannot determine selected item)
            For i As Integer = 1 To shapeToolbar.Controls.Count
                If shapeToolbar.Controls(i).Id = ToolbarId.SnapLayer Then
                    shapeToolbar.Controls(i).Delete()
                    Exit For
                End If
            Next

            combo = shapeToolbar.Controls.Add(XtremeCommandBars.XTPControlType.xtpControlComboBox, ToolbarId.SnapLayer, "", False, False)
            combo.BeginGroup = True
            combo.Width = 130

            shapeToolbar = CommandBars.Add("Combo Popup", XtremeCommandBars.XTPBarPosition.xtpBarComboBoxGalleryPopup)
            _snapLayerGallery = shapeToolbar.Controls.Add(XtremeCommandBars.XTPControlType.xtpControlGallery, ToolbarId.SnapLayer, "", -1, False)
            _snapLayerGallery.Width = 130
            _snapLayerGallery.Height = 16 * 17

            _snapLayerItems = CommandBars.CreateGalleryItems(ToolbarId.SnapLayer)
            _snapLayerItems.ItemWidth = 0
            _snapLayerItems.ItemHeight = 17

            _snapLayerGallery.Items = _snapLayerItems
            combo.CommandBar = shapeToolbar
            '---

            'Set up status bar
            _scalePane = CommandBars.StatusBar.AddPane(1)
            _shapeCountPane = CommandBars.StatusBar.AddPane(2)
            _latLongPane = CommandBars.StatusBar.AddPane(3)
            _mapCoordinatesPane = CommandBars.StatusBar.AddPane(4)
            _coordinateSystemPane = CommandBars.StatusBar.AddPane(5)
            _actionPane = CommandBars.StatusBar.AddPane(6)
            CommandBars.StatusBar.Visible = True

            _scalePane.Text = vbNullString
            _shapeCountPane.Text = vbNullString
            _latLongPane.Text = vbNullString
            _mapCoordinatesPane.Text = vbNullString
            _coordinateSystemPane.Text = vbNullString
            _actionPane.Text = vbNullString

            'Set up language dependent strings
            lblLength.Text = LangStr(_strIdLength)
            lblArea.Text = LangStr(_strIdArea)
            AddRelationToolStripMenuItem.Text = LangStr(_strIdAddRelation)
            ViewInForestSuiteToolStripMenuItem.Text = LangStr(_strIdViewInForest)
            ZoomToLayerToolStripMenuItem.Text = LangStr(_strIdZoomToLayer)
            RemoveLayerToolStripMenuItem.Text = LangStr(_strIdRemoveLayer)
            PropertiesToolStripMenuItem.Text = LangStr(_strIdProperties)
            HierarchyToolStripMenuItem.Text = LangStr(_strIdPaneHierarchyMenu)
            AddHierarchyLayerToolStripMenuItem.Text = LangStr(_strIdPaneHierarchyAddLayer)
            AddUnusedLayersToolStripMenuItem.Text = LangStr(_strIdPaneHierarchyAddUnusedLayers)
            RemoveHierarchyLayerToolStripMenuItem.Text = LangStr(_strIdPaneHierarchyRemoveLayer)
            AddGroupToolStripMenuItem.Text = LangStr(_strIdPaneHierarchyAddGroup)
            RemoveGroupToolStripMenuItem.Text = LangStr(_strIdPaneHierarchyRemoveGroup)
            ZoomToShapeToolStripMenuItem.Text = LangStr(_strIdZoomToShape)
            AutoZoomToolStripMenuItem.Text = LangStr(_strIdAutoZoom)
            FlashCurrentShapeToolStripMenuItem.Text = LangStr(_strIdFlashCurrentShape)
            RemoveShapeToolStripMenuItem.Text = LangStr(_strIdRemoveShape)
            ConditionsToolStripMenuItem.Text = LangStr(_strIdConditions)
            CopyToolStripMenuItem.Text = LangStr(_strIdCopy)
            RelationToolStripMenuItem.Text = LangStr(_strIdAddRelation)
            SelectionToolStripMenuItem.Text = LangStr(_strIdSelections)
            SelectGroupToolStripMenuItem.Text = LangStr(_strIdSelectGroup)
            DeselectGroupToolStripMenuItem.Text = LangStr(_strIdDeselectGroup)
            SelectAllToolStripMenuItem.Text = LangStr(_strIdSelectAll)
            InvertSelectionToolStripMenuItem.Text = LangStr(_strIdInvertSelection)
            DeselectAllToolStripMenuItem.Text = LangStr(_strIdDeselectAll)
            AutoSelectToolStripMenuItem.Text = LangStr(_strIdAutoSelect)
            CachedModeToolStripMenuItem.Text = LangStr(_strIdCachedMode)
            ApplyEditToolStripMenuItemToolStripMenuItem.Text = LangStr(_strIdApplyEdit)
            RevertEditToolStripMenuItemToolStripMenuItem.Text = LangStr(_strIdRevertEdit)
            AddPartToolStripMenuItemToolStripMenuItem.Text = LangStr(_strIdAddPart)
            RemovePartToolStripMenuItemToolStripMenuItem.Text = LangStr(_strIdRemovePart)

            'Get database connection settings from registry
            regKey = Registry.CurrentUser.OpenSubKey(RegKeyDB, False)
            If regKey IsNot Nothing Then
                server = regKey.GetValue("DBLocation")
                database = regKey.GetValue("DBSelected")
                usr = regKey.GetValue("Username")
                pwd = regKey.GetValue("Password")
                winAuth = Not CBool(regKey.GetValue("IsWindowsAuthentication"))
                regKey.Close()
            End If

            'Set up connection for HMS database. Disable connection pooling (OLE DB Services=-4)
            If database = vbNullString Then Throw New Exception(LangStr(_strIdNoDatabase))
            ConnStr = "Provider=SQLOLEDB;OLE DB Services=-4;Server=" & server & ";Database=" & database & ";"
            If winAuth Then
                ConnStr += "Trusted_Connection=yes;"
            Else
                ConnStr += "Uid=" & usr & ";Pwd=" & pwd & ";"
            End If
            Conn = New OleDbConnection(ConnStr)
            Conn.Open()


            'Set up project path
            regKey = Registry.CurrentUser.OpenSubKey(RegKeyGis, False)
            If regKey IsNot Nothing Then
                _projectPath = regKey.GetValue(RegValProjectDir)
                If regKey.GetValue(RegValProjectDir) Is Nothing Then
                    'Use default directory when value doesn't exist
                    _projectPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
                    _projectPath += "\HMS"
                    FileIO.FileSystem.CreateDirectory(_projectPath)
                    _projectPath += "\GIS"
                    FileIO.FileSystem.CreateDirectory(_projectPath)
                End If
                regKey.Close()
            End If
            _projectPath += "\" & FilterPath(server) & "-" & FilterPath(database) & ".ttkgp"

            'Open project
            CreateDbTables(firstStart)
            OpenProject()
            SetUpVisualizationParams()

            If GisCtrl.CS.EPSG = 0 Then
                MsgBox(LangStr(_strChooseCoordinateSystem), MsgBoxStyle.Information)
                Dim dlgCS As New CoordinateSystemDialog(Nothing)
                If dlgCS.ShowDialog() = DialogResult.OK Then
                    SetViewerCS(dlgCS.CS)
                End If
            End If

            'Add default layers on first start
            If firstStart Then CreateDefaultLayers()

            'Add event handler for popup menus
            AddHandler LayerMenu.Items(0).Click, AddressOf OnZoomToLayer
            AddHandler LayerMenu.Items(1).Click, AddressOf OnRemoveLayer
            AddHandler LayerMenu.Items(2).Click, AddressOf OnLayerProperties
            AddHandler HierarchyToolStripMenuItem.DropDownItems(1).Click, AddressOf OnHierarchyAddUnusedLayers
            AddHandler HierarchyToolStripMenuItem.DropDownItems(2).Click, AddressOf OnHierarchyRemoveLayer
            AddHandler HierarchyToolStripMenuItem.DropDownItems(4).Click, AddressOf OnHierarchyAddGroup 'Item 3 is a separator
            AddHandler HierarchyToolStripMenuItem.DropDownItems(5).Click, AddressOf OnHierarchyRemoveGroup
            AddHandler ShapeMenu.Items(0).Click, AddressOf OnAddRelation
            AddHandler ShapeMenu.Items(1).Click, AddressOf OnViewForestData
            AddHandler ShapeDataMenu.Items(0).Click, AddressOf OnZoomToShape
            AddHandler ShapeDataMenu.Items(1).Click, AddressOf OnAutoZoom
            AddHandler ShapeDataMenu.Items(2).Click, AddressOf OnFlashShape
            AddHandler ShapeDataMenu.Items(3).Click, AddressOf OnRemoveShape
            AddHandler ShapeDataMenu.Items(6).Click, AddressOf OnCopyFieldData 'Item 4 is a separator
            AddHandler ShapeDataMenu.Items(7).Click, AddressOf OnAddRelation
            AddHandler ShapeDataMenu.Items(9).Click, AddressOf OnAutoSelect
            AddHandler EditMenu.Items(0).Click, AddressOf OnApplyEdit
            AddHandler EditMenu.Items(1).Click, AddressOf OnRevertEdit
            AddHandler EditMenu.Items(3).Click, AddressOf OnAddPart 'Item 2 is a separator
            AddHandler EditMenu.Items(4).Click, AddressOf OnRemovePart

            'Update field data viewer, enable/disable menu items
            SetActiveLayer(Nothing)
            SetCachedMode(_cachedMode) 'This will call ListFieldData
            ListSnapLayers()
            ToggleCopyright() 'This will hide the notice when layer is not active


            'Set up docking panes
            paneA = DockingPaneManager.CreatePane(1, 200, 120, DockingDirection.DockLeftOf, Nothing)
            paneB = DockingPaneManager.CreatePane(2, 200, 120, DockingDirection.DockRightOf, paneA)
            paneC = DockingPaneManager.CreatePane(3, 200, 15, DockingDirection.DockBottomOf, paneA)
            paneD = DockingPaneManager.CreatePane(4, 200, 120, DockingDirection.DockBottomOf, Nothing)

            paneA.Options = paneA.Options Or PaneOptions.PaneNoCloseable
            paneB.Options = paneB.Options Or PaneOptions.PaneNoCloseable
            paneC.Options = paneC.Options Or PaneOptions.PaneNoCloseable
            paneD.Options = paneD.Options Or PaneOptions.PaneNoCloseable

            paneB.AttachTo(paneA)
            paneA.Select()

            _paneIdLegend = paneA.Id
            _paneIdHierarchy = paneB.Id
            _paneIdMeasure = paneC.Id
            _paneIdData = paneD.Id

            'Load layout (this will invalidate the pane handles)
            DockingPaneManager.LoadState(RegSectionHms, RegSectionGis, RegSectionDockingPaneLayout)

            'Rotate map if needed
            SetMapRotation(_maprotation)

            'Set initial mode after language & action pane
            SetGisMode(GisMode.Drag)

            DataTimer.Start()
        End If

        'We need to set these handles every time GisControlView_Load is called
        'Also make sure pane titles are consistent with current language
        paneA = DockingPaneManager.FindPane(_paneIdLegend)
        If paneA IsNot Nothing Then
            paneA.SetHandle(ControlLegend.Handle)
            paneA.Title = LangStr(_strIdPaneLayers)
        End If

        paneB = DockingPaneManager.FindPane(_paneIdHierarchy)
        If paneB IsNot Nothing Then
            paneB.SetHandle(ControlHierarchy.Handle)
            paneB.Title = LangStr(_strIdPaneHierarchy)
        End If

        paneC = DockingPaneManager.FindPane(_paneIdMeasure)
        If paneC IsNot Nothing Then
            paneC.SetHandle(PanelMeasure.Handle)
            paneC.Title = LangStr(_strIdPaneMeasure)
        End If

        paneD = DockingPaneManager.FindPane(_paneIdData)
        If paneD IsNot Nothing Then
            paneD.SetHandle(PanelData.Handle)
            paneD.Title = LangStr(_strIdPaneData)
        End If

        Initialized = True
    End Sub

    Private Sub GisCtrl_AfterPaint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles GisCtrl.AfterPaint
        Dim dist As Double
        Dim i As Integer
        Dim ll As TGIS_LayerVector
        Dim pos As System.Drawing.Point
        Dim shp As TGIS_Shape
        Dim text As String = vbNullString

        'Measure & draw part distances
        If _mode = GisMode.Measure AndAlso GisCtrl.Editor.InEdit Then
            ll = CType(GisCtrl.Editor.Layer, TGIS_LayerVector)
            shp = GisCtrl.Editor.CurrentShape

            For i = 0 To shp.GetNumPoints() - 1
                pos = GisCtrl.MapToScreen(shp.GetPoint(0, i))
                If i > 0 Then
                    dist = GetPointToPointDistance(ll.CS, shp.GetPoint(0, i - 1), shp.GetPoint(0, i))

                    If shp.ShapeType = TGIS_ShapeType.gisShapeTypePolygon AndAlso i = shp.GetNumPoints() - 1 Then
                        'Special case for last point (same position as first point) of area, draw part distance and total distance
                        text = Math.Round(dist) & " " & GetScaleUnit().Symbol & "/" & Math.Round(shp.LengthCS()) / GetScaleUnit().Factor & " " & GetScaleUnit().Symbol
                    Else
                        'Draw distance from previous point just beside this point
                        text = Math.Round(dist) & " " & GetScaleUnit().Symbol
                    End If
                Else
                    'Draw total distance at first vertex of line
                    If shp.ShapeType = TGIS_ShapeType.gisShapeTypeArc Then
                        text = "0 " & GetScaleUnit().Symbol & "/" & Math.Round(shp.LengthCS()) / GetScaleUnit().Factor & " " & GetScaleUnit().Symbol
                    End If
                End If

                If text <> vbNullString Then
                    'Draw actual numbers
                    Dim myfont As New Font("Arial", 7, FontStyle.Regular)
                    Dim left As Integer = pos.X + 3, top As Integer = pos.Y - 8
                    Dim size As SizeF = e.Graphics.MeasureString(text, myfont)
                    e.Graphics.FillRectangle(Brushes.White, left, top, size.Width, size.Height)
                    e.Graphics.DrawString(text, myfont, Brushes.Black, left, top)
                End If
            Next
        End If

        'Draw navigation pin
        If _drawNavigationPin Then
            Dim bmp As Bitmap = My.Resources.ResourceManager.GetObject("Pin")
            bmp.MakeTransparent()
            Dim img As Image = bmp
            e.Graphics.DrawImage(img, New Point(_navigationPinPos.X - 1, _navigationPinPos.Y - img.Height + 1)) 'Adjust position so bottom of pin will point at coordinate, +/-1 for transparent edge
            _navigationPinDrawn = True
        End If
    End Sub

    Private Sub GisCtrl_BeforePaint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles GisCtrl.BeforePaint
        _initializeViewport = True
    End Sub

    Private Sub GisCtrl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles GisCtrl.Click
        Me.ActiveControl = GisCtrl
    End Sub

    Private Sub GisCtrl_EditorChange(ByVal sender As Object, ByVal e As System.EventArgs) Handles GisCtrl.EditorChange
        UpdateLengthArea(GisCtrl.Editor.CurrentShape)
    End Sub

    Private Sub GisCtrl_LayerAdd(ByVal _sender As Object, ByVal _e As TatukGIS.NDK.TGIS_LayerEventArgs) Handles GisCtrl.LayerAdd
        ListSnapLayers(_e.Layer)

        AddHandler _e.Layer.PaintLayer, AddressOf PaintLayer 'Our custom paint handler

        'Turn on copyright notice on OSM layer
        If _e.Layer.Name.Contains(LayerNameOSM) Then
            PanelCopyright.Visible = True
            AdjustSize()
        End If
    End Sub

    Private Sub GisCtrl_LayerDelete(ByVal _sender As Object, ByVal _e As TatukGIS.NDK.TGIS_LayerEventArgs) Handles GisCtrl.LayerDelete
        Dim ct As Integer = 0

        ListSnapLayers(Nothing, _e.Layer)

        If _e.Layer.Name.Contains(LayerNameOSM) Then
            'Determine whether we have a copyrighted layer or not
            For Each ll As TGIS_LayerAbstract In GisCtrl.Items
                If ll.Name.Contains(LayerNameOSM) Then
                    ct += 1
                End If
            Next

            'Last OSM layer, turn off copyright notice
            If ct = 1 Then
                PanelCopyright.Visible = False
                AdjustSize()
            End If
        End If
    End Sub

    Private Sub PaintLayer(ByVal _sender As Object, ByVal _e As TGIS_PaintLayerEventArgs)
        _e.Layer.Draw()

        'Populate attributes when all layer data has been downloaded (after first paint, WFS layers only)
        If TypeOf _e.Layer Is TGIS_CompoundLayerVector Then
            Dim lc As TGIS_CompoundLayerVector = CType(_e.Layer, TGIS_CompoundLayerVector)
            For i As Integer = 0 To lc.LayersCount - 1
                If lc.Layer(i).Name = _activeSubLayerName AndAlso ReportData.Records.Count = 0 Then ListFieldData()
            Next
        End If
    End Sub

    Private Sub GisCtrl_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles GisCtrl.MouseDown
        Dim ll As TGIS_LayerVector
        Dim lsql As TGIS_LayerSqlAdo = Nothing
        Dim lshp As TGIS_LayerSHP = Nothing
        Dim pmap As TGIS_Point
        Dim shp As TGIS_Shape

        'Get active layer
        ll = TryCast(GetActiveLayer(), TGIS_LayerVector)
        lsql = TryCast(ll, TGIS_LayerSqlAdo) 'For editing (db layer)
        If lsql Is Nothing Then lshp = TryCast(ll, TGIS_LayerSHP) 'For Editing (SHP layer)

        pmap = GisCtrl.ScreenToMap(TGIS_Utils.Point(e.X, e.Y))

        'Use right mouse button to bring up edit menu
        If e.Button = Windows.Forms.MouseButtons.Right Then
            If _mode = GisMode.Add OrElse (_mode = GisMode.EditStep2 AndAlso GisCtrl.Editor.CurrentShape IsNot Nothing) Then
                EditMenu.Show(sender, New Point(e.X, e.Y))
            Else
                SetGisMode(GisMode.Drag)
            End If
        Else
            If _mode = GisMode.Add Then
                If lsql IsNot Nothing Then
                    If _addPart AndAlso lsql.GetShape(_customShapeUid) IsNot Nothing Then
                        'Add part to current shape
                        GisCtrl.Editor.CreatePart(TGIS_Utils.GisPoint3DFrom2D(pmap))
                        _addPart = False
                    Else
                        'Begin new shape
                        AddNewShape(lsql, pmap)
                    End If
                ElseIf lshp IsNot Nothing Then
                    If _addPart AndAlso lshp.GetShape(_customShapeUid) IsNot Nothing Then
                        'Add part to current shape
                        GisCtrl.Editor.CreatePart(TGIS_Utils.GisPoint3DFrom2D(pmap))
                        _addPart = False
                    Else
                        'Begin new shape
                        AddNewShape(lshp, pmap)
                    End If
                End If
            ElseIf _mode = GisMode.EditStep2 AndAlso _addPart Then
                GisCtrl.Editor.CreatePart(TGIS_Utils.GisPoint3DFrom2D(pmap))
                _addPart = False
            ElseIf _mode = GisMode.Remove Then
                If lsql IsNot Nothing Then LocateAndRemoveShape(lsql, pmap)
                If lshp IsNot Nothing Then LocateAndRemoveShape(lshp, pmap)
            ElseIf _mode = GisMode.Measure Then 'We don't need to handle MouseUp for this event because we are in edit mode and thus TatukGIS take care of adding additional points
                BeginMeasure(pmap)
            ElseIf _mode = GisMode.CreateElvObject Then
                If ll IsNot Nothing Then DoCreateElvObject(ll, pmap)
            ElseIf _mode = GisMode.CreateStand Then
                If ll IsNot Nothing Then
                    'Check relation type
                    Dim rt As RelationType = GetLayerRelationType(ll.Name)
                    If rt = RelationType.StandRelation OrElse rt = RelationType.Undefined Then
                        shp = ll.Locate(pmap, _selectTolerance / GisCtrl.Zoom, True)
                        If ll.Active AndAlso shp IsNot Nothing Then
                            'Update relation type if not set
                            If rt = RelationType.Undefined Then UpdateLayerRelation(ll.Name, ll.Caption, ll.CS.EPSG, RelationType.StandRelation)

                            'Make sure shape isn't already connected to a stand
                            If shp.GetField(FieldNameRelation) > 0 Then
                                MsgBox(LangStr(_strIdShapeAlreadyConnected), MsgBoxStyle.Exclamation)
                            Else
                                Dim area As Double
                                Dim pos As Point
                                Dim pcenter As TGIS_Point
                                Dim s As String

                                'Stand position (center of shape)
                                pcenter.X = shp.ProjectedExtent.XMax - (shp.ProjectedExtent.XMax - shp.ProjectedExtent.XMin) / 2.0
                                pcenter.Y = shp.ProjectedExtent.YMax - (shp.ProjectedExtent.YMax - shp.ProjectedExtent.YMin) / 2.0
                                pcenter = GisCtrl.CS.ToWGS(pcenter)

                                'Extract lat/long
                                s = TGIS_Utils.GisLongitudeToStr(pcenter.X)
                                If s.IndexOf("�"c) >= 0 Then pos.X = s.Substring(0, s.IndexOf("�"c))
                                s = TGIS_Utils.GisLatitudeToStr(pcenter.Y)
                                If s.IndexOf("�"c) >= 0 Then pos.Y = s.Substring(0, s.IndexOf("�"c))

                                'Stand area
                                If ll.CS.EPSG > 0 Then
                                    area = shp.AreaCS()
                                Else
                                    area = shp.Area()
                                End If

                                _standShapeId = shp.Uid 'We need to set this before the event is raised so shape id is set when we get the stand id back

                                Dim ea As New StandEventArgs(New Point(pos.X, pos.Y), area)
                                RaiseEvent CreateStand(Me, ea)

                                SetGisMode(GisMode.Drag)
                            End If
                        End If
                    End If
                End If
            ElseIf _mode = GisMode.CreateSection Then
                If ll IsNot Nothing Then
                    'Check relation type
                    Dim rt As RelationType = GetLayerRelationType(ll.Name)
                    Dim dgv As Double
                    If rt = RelationType.SectionRelation OrElse rt = RelationType.Undefined Then
                        shp = ll.Locate(pmap, _selectTolerance / GisCtrl.Zoom, True)
                        If ll.Active AndAlso shp IsNot Nothing Then
                            'Update relation type if not set
                            If rt = RelationType.Undefined Then UpdateLayerRelation(ll.Name, ll.Caption, ll.CS.EPSG, RelationType.SectionRelation)

                            'Make sure shape isn't already connected to a section
                            If shp.GetField(FieldNameRelation) > 0 Then
                                MsgBox(LangStr(_strIdShapeAlreadyConnected), MsgBoxStyle.Exclamation)
                            Else
                                _sectionDestinationLayer = ll.Name

                                'Get DGV if value exist
                                If shp.Layer.FindField(FieldNameSectionDGV) >= 0 Then
                                    dgv = shp.GetField(FieldNameSectionDGV)
                                Else
                                    dgv = 0
                                End If

                                Dim ea As New SectionEventArgs
                                ea.Data.Add(New SectionEventArgs.SectionData(shp.Uid, _
                                                                             shp.GetField(FieldNameSectionId), _
                                                                             shp.GetField(FieldNameSectionAvdnr), _
                                                                             shp.GetField(FieldNameSectionAgoslag), _
                                                                             shp.GetField(FieldNameSectionMalklass), _
                                                                             shp.GetField(FieldNameSectionHkl), _
                                                                             shp.GetField(FieldNameSectionAlder), _
                                                                             shp.GetField(FieldNameSectionAtgard), _
                                                                             shp.GetField(FieldNameSectionAtgardof), _
                                                                             shp.GetField(FieldNameSectionHasof), _
                                                                             shp.GetField(FieldNameSectionSkifte), _
                                                                             shp.GetField(FieldNameSectionMedelhojd), _
                                                                             shp.GetField(FieldNameSectionVolym), _
                                                                             shp.GetField(FieldNameSectionFuktklass), _
                                                                             shp.GetField(FieldNameSectionStandortsi), _
                                                                             shp.GetField(FieldNameSectionVegitation), _
                                                                         Math.Round(shp.Viewer.CS.ToWGS(shp.Centroid()).Y / Math.PI * 180.0), _
                                                                         Math.Round(shp.Viewer.CS.ToWGS(shp.Centroid()).X / Math.PI * 180.0), _
                                                                             shp.GetField(FieldNameSectionGrundyta), _
                                                                             shp.GetField(FieldNameSectionFramskrive), _
                                                                             dgv))
                                RaiseEvent CreateSection(Me, ea)

                                SetGisMode(GisMode.Drag)
                            End If
                        End If
                    End If
                End If
            ElseIf _mode = GisMode.ImportProperties Then
                If ll IsNot Nothing Then DoImportProperties(ll, pmap)
            ElseIf _mode = GisMode.SumAttributes Then
                If ll IsNot Nothing Then DoSumAttributes(ll, pmap)
            ElseIf _mode = GisMode.CreatePlots Then
                If ll IsNot Nothing Then DoCreatePlots(ll, pmap)
            ElseIf _mode = GisMode.ExportShp Or _mode = GisMode.ExportToTCNavigator Then
                _selectionStarted = True
                _selectedOrigin = TGIS_Utils.GisPoint(e.X, e.Y)
                _selectionEndPointSet = False
            ElseIf _mode = GisMode.GenerateRouteStep1WithoutCenterLine Then
                If ll IsNot Nothing Then DoGenerateRouteStep1(ll, pmap, False)
            ElseIf _mode = GisMode.GenerateRouteStep1WithCenterLine Then
                If ll IsNot Nothing Then DoGenerateRouteStep1(ll, pmap, True)
            ElseIf _mode = GisMode.GenerateRouteStep2 Then
                If ll IsNot Nothing Then DoGenerateRouteStep2(pmap)
            ElseIf _mode = GisMode.ExportRoute Then
                If ll IsNot Nothing Then DoExportRoute(ll, pmap)
            ElseIf _mode = GisMode.Split Then
                If ll IsNot Nothing Then DoSplitShape(ll, pmap)
            ElseIf _mode = GisMode.SelectShp Then
                If ll IsNot Nothing Then DoSelectShape(ll, pmap)
            ElseIf _mode = GisMode.ViewInfo Then
                If ll IsNot Nothing Then DoViewShapeInfo(ll, pmap)
            ElseIf _mode = GisMode.ApplyRelation Then
                If ll IsNot Nothing Then DoApplyRelation(ll, pmap)
            End If
        End If
    End Sub

    Private Sub GisCtrl_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles GisCtrl.MouseMove
        Dim pmap As TGIS_Point, pll As TGIS_Point

        'Show map coordinates (fixed amount of decimals to prevent flickering)
        pmap = GisCtrl.ScreenToMap(TGIS_Utils.Point(e.X, e.Y))
        pmap = GisCtrl.UnrotatedPoint(pmap)
        Dim x As String = System.Math.Round(pmap.X, 4)
        Dim y As String = System.Math.Round(pmap.Y, 4)
        If x.IndexOf(","c) < 0 Then x += ","
        If y.IndexOf(","c) < 0 Then y += ","
        x = x.PadRight(x.IndexOf(","c) + 5, "0")
        y = y.PadRight(y.IndexOf(","c) + 5, "0")
        _mapCoordinatesPane.Text = "X: " & x & " | Y: " & y

        'Unproject coordinates to get lat/long (if a projected coordinate system is used)
        If GisCtrl.CS IsNot Nothing Then
            'Make sure coordinates are within bounds
            pll = GisCtrl.CS.ToWGS(pmap)
            If pll.X >= -Math.PI / 2.0 AndAlso pll.X <= Math.PI / 2.0 AndAlso pll.Y >= -Math.PI / 2.0 AndAlso pll.Y <= Math.PI / 2.0 Then
                _latLongPane.Text = TGIS_Utils.GisLatitudeToStr(pll.Y, True, 2, True) & " | " & TGIS_Utils.GisLongitudeToStr(pll.X, True, 2, True)
            Else
                _latLongPane.Text = vbNullString
            End If
        Else
            _latLongPane.Text = vbNullString
        End If

        'Draw selection rectangle
        If ((_mode = GisMode.ExportShp) Or (_mode = GisMode.ExportToTCNavigator)) AndAlso _selectionStarted Then
            Dim canvasSet As Boolean
            Dim gr As System.Drawing.Graphics

            GisCtrl.Canvas.Pen.Width = 2
            GisCtrl.Canvas.Brush.Style = TGIS_BrushStyle.gisBsSolid
            GisCtrl.Canvas.Pen.Mode = TGIS_PenMode.gisPmXor

            gr = GisCtrl.CreateGraphics()
            canvasSet = GisCtrl.Canvas.AssignCanvas(gr, GisCtrl.ClientRectangle)

            Try
                'Restore last rectangle
                If _selectionEndPointSet Then
                    'GisCtrl.Canvas.Rectangle(_selectedOrigin.X, _selectedOrigin.Y, _selectedEndPoint.X, _selectedEndPoint.Y)
                    GisCtrl.Canvas.DrawRectangleFrame(_selectedOrigin.X, _selectedOrigin.Y, _selectedEndPoint.X, _selectedEndPoint.Y)
                End If
                _selectedEndPoint = TGIS_Utils.GisPoint(e.X, e.Y)
                _selectionEndPointSet = True

                'Draw new rectangle
                'GisCtrl.Canvas.Rectangle(_selectedOrigin.X, _selectedOrigin.Y, _selectedEndPoint.X, _selectedEndPoint.Y)
                GisCtrl.Canvas.DrawRectangleFrame(_selectedOrigin.X, _selectedOrigin.Y, _selectedEndPoint.X, _selectedEndPoint.Y)
            Finally
                GisCtrl.Canvas.ReleaseCanvas(canvasSet)
                gr.Dispose()
            End Try
        End If
    End Sub

    Private Sub GisCtrl_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles GisCtrl.MouseUp
        Dim ll As TGIS_LayerVector
        Dim pmap As TGIS_Point
        Dim shp As TGIS_Shape
        Dim type As RelationType

        pmap = GisCtrl.ScreenToMap(TGIS_Utils.Point(e.X, e.Y))

        'If a drag has ended update previous extent so we can jump back
        If _mode = GisMode.Drag AndAlso _extentChanged Then
            _extentChanged = False
            StoreCurrentExtent()
        End If

        If e.Button = Windows.Forms.MouseButtons.Right Then
            If _mode = GisMode.Drag Then
                'Find nearest shape within layer, make sure layer is visible
                ll = TryCast(GetActiveLayer(), TGIS_LayerVector)
                If ll IsNot Nothing Then
                    shp = ll.Locate(pmap, 5 / GisCtrl.Zoom) '5 pixels precision
                    If ll.Active AndAlso shp IsNot Nothing Then
                        _selectedShapeUid = shp.Uid

                        'Check layer relation type - enable/disable 'Relation' and 'Open form' menu items
                        type = GetLayerRelationType(ll.Name)
                        If type <> RelationType.Undefined Then
                            AddRelationToolStripMenuItem.Enabled = True
                            ViewInForestSuiteToolStripMenuItem.Enabled = True
                        Else
                            AddRelationToolStripMenuItem.Enabled = False
                            ViewInForestSuiteToolStripMenuItem.Enabled = False
                        End If
                        ShapeMenu.Show(sender, New Point(e.X, e.Y))
                    End If
                End If
            End If
        ElseIf _mode = GisMode.EditStep1 Then
            'Editing is handled in MouseUp to prevent first click from moving a vertex
            ll = TryCast(GetActiveLayer(), TGIS_LayerVector)
            If TypeOf ll Is TGIS_LayerSqlAdo Then 'Only db vector layers
                LocateAndEditShape(ll, pmap)
            ElseIf ll IsNot Nothing Then
                LocateAndEditShape(ll, pmap)
            End If
        ElseIf _mode = GisMode.ExportShp Then
            ExportShp()
        ElseIf _mode = GisMode.ExportToTCNavigator Then
            ExportToTCNavigator()
        End If
    End Sub

    Private Sub GisCtrl_PrintPage(ByVal sender As Object, ByVal e As TatukGIS.NDK.TGIS_PrintPageEventArgs) Handles GisCtrl.PrintPage
        Dim r As Rectangle
        Dim x1, y1, x2, y2 As Integer
        Dim printer As TGIS_Printer = e.Printer
        Dim canvas As TGIS_GDI = e.Canvas
        Dim dpi As Integer = printer.DpiX

        Dim marginX As Integer = InchToPixels(0.05, dpi)
        Dim marginY As Integer = InchToPixels(0.05, dpi)

        'Whole page
        x1 = marginX
        y1 = marginY
        x2 = printer.PageWidth - marginX
        y2 = printer.PageHeight - marginY
        r = New Rectangle(x1, y1, x2 - x1, y2 - y1)

        'Title
        If GisCtrl.PrintTitle <> vbNullString Then
            canvas.Font.Assign(GisCtrl.PrintTitleFont)
            canvas.TextOut(((x2 - x1) - canvas.TextWidth(GisCtrl.PrintTitle)) / 2, y1, GisCtrl.PrintTitle)
            y1 += canvas.TextHeight(GisCtrl.PrintTitle) + InchToPixels(0.1, dpi)
        End If

        'Legend
        x1 = r.Right - InchToPixels(1.28, dpi) - marginX
        r = New Rectangle(x1, y1, x2 - x1, y2 - y1)
        FrameRect(canvas, r, InchToPixels(0.02, dpi))
        r = New Rectangle(x1 + InchToPixels(0.02, dpi), y1 + InchToPixels(0.02, dpi), x2 - x1 - InchToPixels(0.02, dpi), y2 - y1 - InchToPixels(0.02, dpi))
        ControlLegend.PrintRect(canvas, r, 0)

        'Viewer
        x1 = marginX
        x2 = printer.PageWidth - 2 * marginX - InchToPixels(1.4, dpi)
        r = New Rectangle(x1, y1, x2 - x1, y2 - y1)
        FrameRect(canvas, r, InchToPixels(0.02, dpi))
        GisCtrl.PrintRect(canvas, r, _printExtent, _printScale) 'Use cached extent

        'North arrow
        x1 = x2 - InchToPixels(0.667, dpi)
        y2 = y1 + InchToPixels(0.667, dpi)
        r = New Rectangle(x1, y1, x2 - x1, y2 - y1)
        ControlNorthArrow.PrintRect(canvas, r)

        'Save current drawing color and set a new
        Dim oldColor As Color = canvas.Font.Color
        canvas.Font.Color = Color.Black

        'Scale bar
        x1 = x2 - InchToPixels(2.0, dpi)
        y1 = printer.PageHeight - InchToPixels(0.5, dpi)
        y2 = printer.PageHeight - marginY
        r = New Rectangle(x1, y1, x2 - x1, y2 - y1)
        ControlScale.PrintRect(canvas, r, _printScale)

        'Scale text
        Dim scale As String = "1:" & Math.Round(1.0 / _printScale)
        x1 = x1 + (x2 - x1 - canvas.TextWidth(scale)) / 2.0
        y1 = printer.PageHeight - InchToPixels(0.45, dpi) - canvas.TextHeight(scale)
        canvas.Font.Assign(New Font("Arial", 10, FontStyle.Bold))
        canvas.TextOut(x1, y1, scale)

        'Set old color again
        canvas.Font.Color = oldColor
    End Sub

    Private Sub GisCtrl_VisibleExtentChange(ByVal sender As Object, ByVal e As System.EventArgs) Handles GisCtrl.VisibleExtentChange
        UpdateScalePane()

        If _navigationPinDrawn Then _drawNavigationPin = False 'Remove navigation pin

        'When dragging the map we need to flag extent has changed so we can update previous extent when dragging has ended
        If _mode = GisMode.Drag AndAlso System.Windows.Forms.Control.MouseButtons And Windows.Forms.MouseButtons.Left Then
            _extentChanged = True
        End If
    End Sub

    Private Sub GisCtrl_ZoomChange(ByVal sender As Object, ByVal e As System.EventArgs) Handles GisCtrl.ZoomChange
        UpdateScalePane()

        If Not _ignoreZoomChange Then
            'Store current extent
            StoreCurrentExtent()
        Else
            'This flag is set when zooming to previous extent
            _ignoreZoomChange = False
        End If
    End Sub

    Private Sub CommandBars_Execute(ByVal sender As Object, ByVal e As AxXtremeCommandBars._DCommandBarsEvents_ExecuteEvent) Handles CommandBars.Execute
        Select Case e.control.Id
            Case ToolbarId.NewLayer
                AddNewLayer()
            Case ToolbarId.ImportLayerFromFile
                AddFile()
            Case ToolbarId.ImportLayerFromStream1
                AddStream(MapStream1)
            Case ToolbarId.ImportLayerFromStreamCustom
                AddStream()
            Case ToolbarId.RemoveLayer
                RemoveLayerEventHandler()
            Case ToolbarId.LayerProperties
                LayerPropertiesEventHandler()
            Case ToolbarId.FullExtent
                ZoomToFullExtent()
            Case ToolbarId.PreviousExtent
                ZoomToPreviousExtent()
            Case ToolbarId.NavigateTo
                NavigateTo()
            Case ToolbarId.Zoom
                SetGisMode(GisMode.Zoom)
            Case ToolbarId.Drag
                SetGisMode(GisMode.Drag)
            Case ToolbarId.SelectShape
                SetGisMode(GisMode.SelectShp)
            Case ToolbarId.Info
                SetGisMode(GisMode.ViewInfo)
            Case ToolbarId.NewShapePolygon
                AddShape(ShapeType.Polygon)
            Case ToolbarId.NewShapeLine
                AddShape(ShapeType.Line)
            Case ToolbarId.NewShapePoint
                AddShape(ShapeType.Point)
            Case ToolbarId.NewShapeMultiPoint
                AddShape(ShapeType.MultiPoint)
            Case ToolbarId.EditShape
                SetGisMode(GisMode.EditStep1)
            Case ToolbarId.RemoveShape
                SetGisMode(GisMode.Remove)
            Case ToolbarId.MeasureLength
                MeasureShape(ShapeType.Line)
            Case ToolbarId.MeasureArea
                MeasureShape(ShapeType.Polygon)
            Case ToolbarId.Print
                ExportViewToReport()
            Case ToolbarId.UpdateMap
                SetUpVisualizationParams()
                _layerChanged = True 'This will update field data list (and about current operation if running)
            Case ToolbarId.LandValueToolsCreateObject
                SetGisMode(GisMode.CreateElvObject)
            Case ToolbarId.MiscToolsCreateStand
                SetGisMode(GisMode.CreateStand)
            Case ToolbarId.MiscToolsCreateSection
                SetGisMode(GisMode.CreateSection)
            Case ToolbarId.MiscToolsImportProperties, ToolbarId.LandValueToolsImportProperties
                SetGisMode(GisMode.ImportProperties)
            Case ToolbarId.MiscToolsSumAttributes
                SetGisMode(GisMode.SumAttributes)
            Case ToolbarId.MiscToolsExportViewToImageWithProj
                ExportViewToImageWithProj()
            Case ToolbarId.MiscToolsExportViewToImageWithScale
                ExportViewToImageWithScale()
            Case ToolbarId.MiscToolsExportViewToReport
                ExportViewToReport()
            Case ToolbarId.MiscToolsExportViewToSnapShot
                ExportViewToSnapshot()
            Case ToolbarId.MiscToolsExportToShp
                SetGisMode(GisMode.ExportShp)
            Case ToolbarId.MiscToolsExportToTCNavigator
                SetGisMode(GisMode.ExportToTCNavigator)
            Case ToolbarId.MiscToolsCreatePlots
                CreatePlots()
            Case ToolbarId.MiscToolsImportPlots
                ImportPlots()
            Case ToolbarId.MiscToolsGenerateRouteWithoutCenterLine, ToolbarId.LandValueToolsGenerateRouteWithoutCenterLine
                GenerateRoute()
            Case ToolbarId.MiscToolsGenerateRouteWithCenterLine, ToolbarId.LandValueToolsGenerateRouteWithCenterLine
                SetGisMode(GisMode.GenerateRouteStep1WithCenterLine)
            Case ToolbarId.MiscToolsExportRoute, ToolbarId.LandValueToolsExportRoute
                SetGisMode(GisMode.ExportRoute)
            Case ToolbarId.MiscToolsSplitShape
                SetGisMode(GisMode.Split)
            Case ToolbarId.MiscToolsUnionShapes
                UnionShapes()
            Case ToolbarId.MiscToolsViewUnusedLayers
                Dim dlg As New UnusedLayersDialog : dlg.ShowDialog()
            Case ToolbarId.MiscToolsSettings
                ShowSettingsDialog()
            Case ToolbarId.MiscToolsAbout
                AboutBox.ShowDialog()
            Case ToolbarId.Apply
                ApplyEdit()
            Case ToolbarId.Revert
                RevertEdit()
            Case ToolbarId.Undo
                Undo()
            Case ToolbarId.Redo
                Redo()
            Case ToolbarId.SnapLayer
                SetupSnapLayer()
            Case ToolbarId.ApplyRelation
                SetGisMode(GisMode.ApplyRelation)
        End Select
    End Sub

    Private Sub CommandBars_ResizeEvent(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandBars.ResizeEvent
        AdjustSize()
    End Sub

    Private Sub GisControlView_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        'We also need to adjust size when form is resized (to avoid toolbar growing downwards when switching MDI forms)
        AdjustSize()
    End Sub

    Private Sub CommandBars_UpdateEvent(ByVal sender As Object, ByVal e As AxXtremeCommandBars._DCommandBarsEvents_UpdateEvent) Handles CommandBars.UpdateEvent
        Dim nullExtent As TGIS_Extent

        Select Case e.control.Id
            Case ToolbarId.PreviousExtent : e.control.Enabled = Not (_previousExtent.Equals(nullExtent)) 'Only enabled if previous extent is set
            Case ToolbarId.Zoom : e.control.Checked = (_mode = GisMode.Zoom)
            Case ToolbarId.Drag : e.control.Checked = (_mode = GisMode.Drag)
            Case ToolbarId.SelectShape : e.control.Checked = (_mode = GisMode.SelectShp)
            Case ToolbarId.Info : e.control.Checked = (_mode = GisMode.ViewInfo)
            Case ToolbarId.NewShape : e.control.Checked = (_mode = GisMode.Add)
            Case ToolbarId.EditShape : e.control.Checked = (_mode = GisMode.EditStep1 OrElse _mode = GisMode.EditStep2)
            Case ToolbarId.RemoveShape : e.control.Checked = (_mode = GisMode.Remove)
            Case ToolbarId.Measure : e.control.Checked = (_mode = GisMode.Measure)

            Case ToolbarId.Apply, ToolbarId.Revert : e.control.Enabled = (_mode = GisMode.Add OrElse _mode = GisMode.EditStep2 OrElse _mode = GisMode.Split)
            Case ToolbarId.Undo : e.control.Enabled = GisCtrl.Editor.CanUndo
            Case ToolbarId.Redo : e.control.Enabled = GisCtrl.Editor.CanRedo

            Case ToolbarId.ApplyRelation
                e.control.Checked = (_mode = GisMode.ApplyRelation)
                e.control.Enabled = (_applyRelationId <> 0 AndAlso _applyRelationType <> RelationType.Undefined)
        End Select
    End Sub

    Private Sub RemoveLayerEventHandler()
        If MsgBox(LangStr(_strIdConfirmRemoveLayer), MsgBoxStyle.Exclamation Or MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Dim ll As TGIS_LayerAbstract = GetActiveLayer()
            If ll IsNot Nothing Then RemoveLayer(ll.Name)
        End If
    End Sub

    Private Sub LayerPropertiesEventHandler()
        ShowPropertiesDialog(_activeLayerName, _activeSubLayerName)
    End Sub

    Private Sub OnZoomToLayer(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ZoomToLayer(_activeLayerName, _activeSubLayerName)
    End Sub

    Private Sub OnRemoveLayer(ByVal sender As System.Object, ByVal e As System.EventArgs)
        RemoveLayerEventHandler()
    End Sub

    Private Sub OnLayerProperties(ByVal sender As System.Object, ByVal e As System.EventArgs)
        LayerPropertiesEventHandler()
    End Sub

    Private Sub OnHierarchyAddUnusedLayers(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim grp As TGIS_HierarchyGroup
        Dim hierarchy As String
        Dim ll As TGIS_LayerAbstract
        Dim i As Integer

        'Get hierarchy string list
        hierarchy = GisCtrl.Hierarchy.GetHierarchy()

        'Add any layers not displayed in the hierarchy to selected group
        grp = GisCtrl.Hierarchy.Groups(_activeLayerName)
        If grp IsNot Nothing Then
            'Check each layer in viewer
            For i = 0 To GisCtrl.Items.Count - 1
                ll = CType(GisCtrl.Items(i), TGIS_LayerAbstract)
                If Not hierarchy.Contains(ll.Name) Then
                    'Add layer to selected group
                    grp.AddLayer(GisCtrl.Get(ll.Name))
                End If
            Next

            ControlHierarchy.Update()
            GisCtrl.SaveProject()
        End If
    End Sub

    Private Sub OnHierarchyRemoveLayer(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim ll As TGIS_LayerAbstract

        'Remove selected layer from hierarchy
        ll = GetActiveLayer()
        If ll IsNot Nothing Then
            GisCtrl.Hierarchy.DeleteLayer(ll)
            ControlHierarchy.Update()
            GisCtrl.SaveProject()
        End If
    End Sub

    Private Sub OnHierarchyAddGroup(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim grp As TGIS_HierarchyGroup
        Dim i As Integer = 1

        'Find a new group name
        Do Until GisCtrl.Hierarchy.Groups("Group" & i) Is Nothing
            i += 1
        Loop

        'Create group
        grp = New TGIS_HierarchyGroup("Group" & i)
        GisCtrl.Hierarchy.AddGroup(grp)
        ControlHierarchy.Update()
        GisCtrl.SaveProject()
    End Sub

    Private Sub OnHierarchyRemoveGroup(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Remove selected group from hierarchy
        GisCtrl.Hierarchy.DeleteGroup(_activeLayerName)
        ControlHierarchy.Update()
        GisCtrl.SaveProject()
    End Sub

    Private Sub OnAddRelation(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim cmd As New OleDbCommand("", Conn)
        Dim da As New OleDbDataAdapter("", Conn)
        Dim cb As New OleDbCommandBuilder(da)
        Dim ds As New DataSet()
        Dim fs As FileStream
        Dim fileName As String
        Dim extent As TGIS_Extent
        Dim ll As TGIS_LayerVector
        Dim shp As TGIS_Shape
        Dim type As RelationType = RelationType.Undefined
        Dim prevRelationId As Integer

        'We need to obtain the current relation id in case relation will be cleared
        ll = TryCast(GetActiveLayer(), TGIS_LayerVector)
        If ll IsNot Nothing Then
            shp = ll.GetShape(_selectedShapeUid).MakeEditable() 'MakeEditable, we need a persistent shape when calling ExportToImage
            prevRelationId = shp.GetField(GisControlView.FieldNameRelation)
            type = GetLayerRelationType(ll.Name)

            'Show relation dialog
            Dim dlg As New RelationDialog(ll.Name, type, _selectedShapeUid)
            Dim dr As DialogResult = dlg.ShowDialog()
            If dr = DialogResult.OK Then
                Dim oldCursor As Cursor = GisCtrl.Cursor
                GisCtrl.Cursor = Cursors.WaitCursor

                'Ask to update stand area
                If type = RelationType.StandRelation AndAlso shp.GetField(FieldNameRelation) >= 0 AndAlso shp.AreaCS >= 0 Then
                    If MsgBox(LangStr(_strIdUpdateStandArea), MsgBoxStyle.Question Or MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then UpdateStandArea(shp.GetField(FieldNameRelation), shp)
                End If

                'Generate images for stands and objects
                If (type = RelationType.StandRelation OrElse type = RelationType.ObjectRelation) AndAlso shp.GetField(FieldNameRelation) > 0 Then
                    Try
                        'Save image to file
                        fileName = Path.GetTempFileName()
                        fileName = Mid(fileName, 1, fileName.LastIndexOf("."c)) & ".png" 'Use png extension instead of tmp
                        If type = RelationType.StandRelation Then
                            'Use 0.5x zoom
                            extent.XMin = shp.ProjectedExtent.XMin - (shp.ProjectedExtent.XMax - shp.ProjectedExtent.XMin)
                            extent.XMax = shp.ProjectedExtent.XMax + (shp.ProjectedExtent.XMax - shp.ProjectedExtent.XMin)
                            extent.YMin = shp.ProjectedExtent.YMin - (shp.ProjectedExtent.YMax - shp.ProjectedExtent.YMin)
                            extent.YMax = shp.ProjectedExtent.YMax + (shp.ProjectedExtent.YMax - shp.ProjectedExtent.YMin)
                        Else
                            extent = shp.ProjectedExtent
                        End If

                        Try
                            GisCtrl.ExportToImage(fileName, extent, 800, 670, 80, 0, 96)
                        Catch ex As EGIS_Exception
                            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                        End Try

                        'Read off image file
                        fs = New FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Read)
                        Dim data(fs.Length) As Byte
                        fs.Read(data, 0, fs.Length)
                        fs.Close()

                        'Remove temp files (png, pgw, tab)
                        Try
                            File.Delete(fileName)
                            File.Delete(Mid(fileName, 1, fileName.LastIndexOf("."c)) & ".pgw")
                            File.Delete(Mid(fileName, 1, fileName.LastIndexOf("."c)) & ".tab")
                            File.Delete(fileName & ".prj")
                        Catch ex As IOException
                        End Try

                        'Insert image to db
                        ds.Locale = CultureInfo.InvariantCulture
                        If type = RelationType.StandRelation Then
                            da.SelectCommand.CommandText = "SELECT * FROM esti_trakt_table WHERE trakt_id = " & shp.GetField(FieldNameRelation)
                            da.Fill(ds)
                            ds.Tables(0).Rows(0).Item("trakt_img") = data
                            da.Update(ds)
                        ElseIf type = RelationType.ObjectRelation Then
                            da.SelectCommand.CommandText = "SELECT * FROM elv_object_table WHERE object_id = " & shp.GetField(FieldNameRelation)
                            da.Fill(ds)
                            ds.Tables(0).Rows(0).Item("object_img") = data
                            da.Update(ds)
                        End If
                    Catch ex As IOException
                        MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                    End Try

                    'Ask if the sample trees should be updated/inserted
                    If type = RelationType.ObjectRelation Then ImportStandShapes(shp.GetField(FieldNameRelation), type)
                ElseIf type = RelationType.PropertyRelation Then
                    AddCentroidToPropertyTable(shp.GetField(FieldNameRelation), shp)
                End If

                'Refresh any joined data
                SetUpVisualizationParams()

                'Added an update of the ReportData for the shape to show the data of the newly created relation #1696 #1986
                UpdateListFieldData(shp)
                GisCtrl.Update()
                GisCtrl.Cursor = oldCursor
            ElseIf dr = DialogResult.Cancel Then
                'Abort means relation was cleared so we also need to clear the previously generated image
                If type = RelationType.StandRelation Then
                    cmd.CommandText = "UPDATE esti_trakt_table SET trakt_img = NULL WHERE trakt_id = " & prevRelationId
                    cmd.ExecuteNonQuery()
                ElseIf type = RelationType.ObjectRelation Then
                    cmd.CommandText = "UPDATE elv_object_table SET object_img = NULL WHERE object_id = " & prevRelationId
                    cmd.ExecuteNonQuery()
                ElseIf type = RelationType.PropertyRelation Then
                    cmd.CommandText = "UPDATE fst_property_table SET prop_coord = NULL WHERE id = " & prevRelationId    '#3724
                    cmd.ExecuteNonQuery()
                End If
                'Added an update of the ReportData for the shape to show the data of the newly aborted relation #1696 #1986
                UpdateListFieldData(shp)
                GisCtrl.Update()
            End If
        End If
    End Sub

    Private Sub OnViewForestData(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim ea As ForestEventArgs
        Dim WidgetId As Integer = -1
        Dim type As RelationType
        Dim ll As TGIS_LayerVector

        ll = TryCast(GisCtrl.Get(_activeLayerName), TGIS_LayerVector)
        If ll IsNot Nothing Then
            'Get layer relation type
            type = GetLayerRelationType(ll.Name)

            'Check if this shape is bound to a stand
            Dim cmd As New OleDbCommand("", Conn)
            Dim reader As OleDbDataReader
            cmd.CommandText = "SELECT " & FieldNameRelation & " FROM " & GisTablePrefix & ll.Name & "_FEA WHERE uid = " & _selectedShapeUid
            reader = cmd.ExecuteReader(CommandBehavior.SingleRow)
            If reader.HasRows Then
                reader.Read()
                If Not reader.IsDBNull(0) Then WidgetId = reader.GetInt32(0)
            End If
            reader.Close()

            'Raise an event sending id and type back to suite
            If WidgetId <> -1 Then
                If type = RelationType.SampleTreeRelation Then type = RelationType.StandRelation 'Open belonging stand
                ea = New ForestEventArgs(WidgetId, type)
                RaiseEvent ViewForestData(Me, ea)
            Else
                MsgBox(LangStr(_strIdNoShapeRelation), MsgBoxStyle.Information)
            End If
        End If
    End Sub

    Private Sub OnZoomToShape(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim ll As TGIS_LayerVector
        Dim shp As TGIS_Shape

        'Zoom to selected shape (use UID in first column to locate selected shape)
        If ReportData.SelectedRows.Count > 0 Then
            ll = TryCast(GetActiveLayer(), TGIS_LayerVector)
            If ll IsNot Nothing Then
                shp = ll.GetShape(GetReportDataSelectedShapeUid())
                If shp IsNot Nothing Then
                    If Not TypeOf shp Is TGIS_ShapePoint Then
                        GisCtrl.VisibleExtent = shp.ProjectedExtent
                    Else
                        'A point has no extent so we use a specialized method to zoom in to a point
                        'Center point in viewer
                        Dim pt As TGIS_Point = shp.GetPoint(0, 0)
                        Dim dx As Double = (GisCtrl.VisibleExtent.XMax - GisCtrl.VisibleExtent.XMin) / 2.0
                        Dim dy As Double = (GisCtrl.VisibleExtent.YMax - GisCtrl.VisibleExtent.YMin) / 2.0
                        Dim oldZoom As Double

                        Dim newExtent As New TGIS_Extent
                        newExtent.XMin = pt.X - dx
                        newExtent.XMax = pt.X + dx
                        newExtent.YMin = pt.Y - dy
                        newExtent.YMax = pt.Y + dy

                        'Lock viewer so it won't refresh each time visible extent is changed
                        GisCtrl.Lock()
                        Try
                            GisCtrl.VisibleExtent = newExtent

                            'Zoom in until current shape is the only visible shape in layer
                            Dim sf As TGIS_Shape
                            sf = ll.FindFirst(GisCtrl.VisibleExtent)
                            Do Until sf Is Nothing
                                If sf.Uid <> shp.Uid Then
                                    'Another shape is visible - we need to zoom in more
                                    oldZoom = GisCtrl.Zoom
                                    GisCtrl.Zoom *= 2
                                    If GisCtrl.Zoom = oldZoom Then Exit Do 'We have reached the max zoom level
                                    sf = ll.FindFirst(GisCtrl.VisibleExtent)
                                    Continue Do
                                End If

                                sf = ll.FindNext()
                            Loop
                        Catch ex As EGIS_Exception
                            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                        End Try
                        GisCtrl.Unlock()
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub OnAutoZoom(ByVal sender As System.Object, ByVal e As System.EventArgs)
        AutoZoomToolStripMenuItem.Checked = Not AutoZoomToolStripMenuItem.Checked
    End Sub

    Private Sub OnAutoSelect(ByVal sender As System.Object, ByVal e As System.EventArgs)
        AutoSelectToolStripMenuItem.Checked = Not AutoSelectToolStripMenuItem.Checked
    End Sub

    Private Sub OnApplyEdit(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ApplyEdit()
    End Sub

    Private Sub OnRevertEdit(ByVal sender As System.Object, ByVal e As System.EventArgs)
        RevertEdit()
    End Sub

    Private Sub OnAddPart(ByVal sender As System.Object, ByVal e As System.EventArgs)
        _addPart = True
    End Sub

    Private Sub OnRemovePart(ByVal sender As System.Object, ByVal e As System.EventArgs)
        GisCtrl.Editor.DeletePart()
    End Sub

    Private Sub OnFlashShape(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim ll As TGIS_LayerVector
        Dim shp As TGIS_Shape

        'Flash selected shape (use UID in first column to locate selected shape)
        If ReportData.SelectedRows.Count > 0 Then
            ll = TryCast(GetActiveLayer(), TGIS_LayerVector)
            If ll IsNot Nothing Then
                shp = ll.GetShape(GetReportDataSelectedShapeUid())
                If shp IsNot Nothing Then
                    Try
                        shp.Flash()
                    Catch ex As Exception
                    End Try
                End If
            End If
        End If
    End Sub

    Private Sub OnRemoveShape(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim i As Integer
        Dim ll As TGIS_LayerVector
        Dim shp As TGIS_Shape
        Dim question As String
        Dim uidlst As New List(Of Integer)
        Dim reclst As New List(Of ReportRecord)

        'Remove selected shapes (use UID in first column to locate selected shape)
        If ReportData.SelectedRows.Count > 0 Then
            ll = TryCast(GetActiveLayer(), TGIS_LayerVector)
            If ll IsNot Nothing Then
                'Get selected shape UID's
                If _cachedMode Then
                    'Get UID from first column
                    For i = 0 To ReportData.SelectedRows.Count - 1
                        uidlst.Add(ReportData.SelectedRows(i).Record.Item(0).Value)
                        reclst.Add(ReportData.SelectedRows(i).Record)
                    Next
                Else
                    'Get UID from virtual list
                    For i = 0 To ReportData.SelectedRows.Count - 1
                        If ReportData.SelectedRows(i).Index < _shapeList.Count Then
                            uidlst.Add(_shapeList(ReportData.SelectedRows(i).Index))
                        End If
                    Next
                End If

                'Mark selected shapes
                For i = 0 To uidlst.Count - 1
                    shp = ll.GetShape(uidlst(i))
                    If shp IsNot Nothing Then
                        shp = shp.MakeEditable()
                        shp.IsSelected = True
                        shp.Invalidate()
                    End If
                Next

                'Let user confirm removal
                If uidlst.Count > 1 Then
                    question = CType(LangStr(_strIdConfirmRemoveMultipleShapes), String).Replace("%d", ReportData.SelectedRows.Count)
                Else
                    question = LangStr(_strIdConfirmRemoveShape)
                End If
                If MsgBox(question, MsgBoxStyle.Exclamation Or MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                    For i = 0 To uidlst.Count - 1
                        shp = ll.GetShape(uidlst(i))
                        If shp IsNot Nothing Then
                            'Remove any relation image
                            RemoveRelationImage(shp, GetLayerRelationType(ll.Name))

                            'Remove any coord
                            RemoveRelationCoord(shp, GetLayerRelationType(ll.Name))

                            'Remove actual shape
                            ll.Delete(shp.Uid)
                        End If
                    Next

                    'Save changes
                    ll.SaveData()

                    'Update report data
                    If _cachedMode Then
                        For i = 0 To reclst.Count - 1
                            ReportData.RemoveRecordEx(reclst(i))
                        Next
                        ReportData.Update()
                    Else
                        ListFieldDataVirtual()
                    End If

                    GisCtrl.Update()
                Else
                    ll.DeselectAll()
                End If
            End If
        End If
    End Sub

    Private Sub OnCopyFieldData(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim data As String = vbNullString
        Dim i As Integer
        Dim row As XtremeReportControl.ReportRow
        Dim ll As TGIS_LayerVector
        Dim shp As TGIS_Shape

        'Add column headers
        For i = 0 To ReportData.Columns.Count - 1
            If ReportData.Columns(i).Visible Then
                If i > 0 Then data += vbTab 'Avoid trailing tabs
                data += ReportData.Columns(i).Caption
            End If
        Next
        data += vbCrLf

        'Make a buffer from selected rows
        If _cachedMode Then
            For Each row In ReportData.SelectedRows
                For i = 0 To row.Record.ItemCount - 1
                    If i > 0 Then data += vbTab 'Avoid trailing tabs
                    data += row.Record.Item(i).Value.ToString()
                Next
                data += vbCrLf
            Next
        Else
            ll = TryCast(GetActiveLayer(), TGIS_LayerVector)
            If ll IsNot Nothing Then
                For Each row In ReportData.SelectedRows
                    If row.Record.Index < _shapeList.Count Then
                        shp = ll.GetShape(_shapeList(row.Record.Index))
                        If shp IsNot Nothing Then
                            'Shape data
                            data += shp.Uid.ToString()
                            For i = 0 To ll.Fields.Count - 1
                                If Not ll.FieldInfo(i).IsUID AndAlso Not IsRelationField(ll.FieldInfo(i).Name) Then
                                    data += vbTab 'Avoid trailing tabs
                                    data += shp.GetField(ll.FieldInfo(i).Name).ToString()
                                End If
                            Next

                            'Joined data
                            If ll.JoinNET IsNot Nothing Then
                                Dim dt As DataTable = CType(ll.JoinNET, DataTable)
                                If Not String.IsNullOrEmpty(_currentJoinView) Then
                                    Dim key As String = shp.GetField(GisControlView.GetJoinPrimary(_currentJoinView))
                                    Dim datarow As DataRow = dt.Rows.Find(key)
                                    If datarow IsNot Nothing Then
                                        For i = 0 To dt.Columns.Count - 1
                                            If Not IsRelationField(dt.Columns(i).Caption) Then
                                                data += vbTab
                                                data += datarow.Item(i).ToString()
                                            End If
                                        Next
                                    End If
                                End If
                            End If
                        End If
                    End If
                Next
            End If
        End If

        Clipboard.SetText(data)
    End Sub

    Private Sub OSMCopyrightLink_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles OSMCopyrightLink.LinkClicked
        Process.Start(OSMCopyrightLink.Tag)
    End Sub

    Private Sub ControlLegend_LayerActiveChange(ByVal _sender As Object, ByVal _e As TatukGIS.NDK.TGIS_LayerEventArgs) Handles ControlLegend.LayerActiveChange
        OnLayerActiveChange(_e.Layer)
        ToggleCopyright()
    End Sub

    Private Sub ControlLegend_OnOrderChange(ByVal sender As Object, ByVal e As System.EventArgs) Handles ControlLegend.OrderChange
        GisCtrl.SaveProject()
        ListSnapLayers()
    End Sub

    Private Sub ControlLegend_OnLayerSelect(ByVal sender As Object, ByVal e As TGIS_LayerEventArgs) Handles ControlLegend.LayerSelect
        If e.Layer IsNot Nothing Then SetActiveLayer(e.Layer)
    End Sub

    Private Sub ControlHierarchy_LayerActiveChange(ByVal _sender As Object, ByVal _e As TatukGIS.NDK.TGIS_LayerEventArgs) Handles ControlHierarchy.LayerActiveChange
        OnLayerActiveChange(_e.Layer)
        ToggleCopyright()
    End Sub

    Private Sub ControlHierarchy_OnLayerSelect(ByVal sender As Object, ByVal e As TGIS_LayerEventArgs) Handles ControlHierarchy.LayerSelect
        If e.Layer IsNot Nothing Then SetActiveLayer(e.Layer)
    End Sub

    Private Sub ControlLegend_OnMouseUp(ByVal sender As Object, ByVal e As Windows.Forms.MouseEventArgs) Handles ControlLegend.MouseUp
        If e.Button = Windows.Forms.MouseButtons.Right Then
            'Disable hierarchy menu items
            AddHierarchyLayerToolStripMenuItem.Enabled = False
            AddUnusedLayersToolStripMenuItem.Enabled = False
            RemoveHierarchyLayerToolStripMenuItem.Enabled = False
            AddGroupToolStripMenuItem.Enabled = False
            RemoveGroupToolStripMenuItem.Enabled = False

            'Enable layer menu items
            If GetActiveLayer() IsNot Nothing Then
                ZoomToLayerToolStripMenuItem.Enabled = True
                RemoveLayerToolStripMenuItem.Enabled = String.IsNullOrEmpty(_activeSubLayerName)
                PropertiesToolStripMenuItem.Enabled = True
            Else
                ZoomToLayerToolStripMenuItem.Enabled = False
                RemoveLayerToolStripMenuItem.Enabled = False
                PropertiesToolStripMenuItem.Enabled = False
            End If

            'Show popup menu
            LayerMenu.Show(sender, New Point(e.X, e.Y))
        End If

        'Save project in case visibility changed
        GisCtrl.SaveProject()
    End Sub

    Private Sub ControlHierarchy_OnMouseUp(ByVal sender As Object, ByVal e As Windows.Forms.MouseEventArgs) Handles ControlHierarchy.MouseUp
        If e.Button = Windows.Forms.MouseButtons.Right Then
            'Enable disable hierarchy menu items
            If GetActiveLayer() IsNot Nothing Then
                'Layer
                AddHierarchyLayerToolStripMenuItem.Enabled = False
                RemoveHierarchyLayerToolStripMenuItem.Enabled = True
                RemoveGroupToolStripMenuItem.Enabled = False

                'Enable layer menu items
                ZoomToLayerToolStripMenuItem.Enabled = True
                RemoveLayerToolStripMenuItem.Enabled = String.IsNullOrEmpty(_activeSubLayerName)
                PropertiesToolStripMenuItem.Enabled = True
            Else
                'Group
                AddHierarchyLayerToolStripMenuItem.Enabled = True
                RemoveHierarchyLayerToolStripMenuItem.Enabled = False
                RemoveGroupToolStripMenuItem.Enabled = True

                'Add layer captions to 'Add layer' submenu
                PrepareHierarchyMenuStrip()

                'Disable layer menu items
                ZoomToLayerToolStripMenuItem.Enabled = False
                RemoveLayerToolStripMenuItem.Enabled = False
            End If
            AddGroupToolStripMenuItem.Enabled = True
            PropertiesToolStripMenuItem.Enabled = True

            'Show popup menu
            LayerMenu.Show(sender, New Point(e.X, e.Y))
        End If

        'Save project in case visibility changed
        GisCtrl.SaveProject()
    End Sub

    Private Sub ReportData_BeforeDrawRow(ByVal sender As Object, ByVal e As AxXtremeReportControl._DReportControlEvents_BeforeDrawRowEvent) Handles ReportData.BeforeDrawRow
        Dim ll As TGIS_LayerVector
        Dim shp As TGIS_Shape = Nothing
        Dim dt As DataTable
        Dim key As String

        ll = TryCast(GetActiveLayer(), TGIS_LayerVector)
        If ll IsNot Nothing AndAlso _shapeList.Count > e.row.Index Then
            shp = ll.GetShape(_shapeList(e.row.Index))
            If shp IsNot Nothing Then
                If e.item.Index = 0 Then
                    e.metrics.Text = shp.Uid
                Else
                    If e.item.Index < ll.Fields.Count Then
                        e.metrics.Text = shp.GetField(ll.FieldInfo(e.item.Index).Name)
                    Else
                        'Joined field
                        dt = CType(ll.JoinNET, DataTable)
                        If Not String.IsNullOrEmpty(_currentJoinView) Then
                            key = shp.GetField(GisControlView.GetJoinPrimary(_currentJoinView))
                            Dim row As DataRow = dt.Rows.Find(key)
                            If row IsNot Nothing Then
                                e.metrics.Text = row.Item(e.item.Index - ll.Fields.Count).ToString()
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub ReportData_MouseDownEvent(ByVal sender As Object, ByVal e As AxXtremeReportControl._DReportControlEvents_MouseDownEvent) Handles ReportData.MouseDownEvent
        Me.ActiveControl = ReportData 'Prevent zooming and scrolling simultaneously

        If AutoSelectToolStripMenuItem.Checked Then
            Dim ll As TGIS_LayerVector
            Dim shp As TGIS_Shape

            'Select current shape
            ll = TryCast(GetActiveLayer(), TGIS_LayerVector)
            If ll IsNot Nothing Then
                shp = ll.GetShape(GetReportDataSelectedShapeUid())
                If shp IsNot Nothing Then
                    ll.DeselectAll()
                    shp.MakeEditable().IsSelected = True
                    shp.Invalidate()
                End If
            End If
        End If
    End Sub

    Private Sub ReportData_MouseUpEvent(ByVal sender As Object, ByVal e As AxXtremeReportControl._DReportControlEvents_MouseUpEvent) Handles ReportData.MouseUpEvent
        Dim ll As TGIS_LayerVector

        If e.button = 2 Then 'Right mouse button
            'Enable relation only if layer has a relation
            RelationToolStripMenuItem.Enabled = False
            ll = TryCast(GetActiveLayer(), TGIS_LayerVector)
            If ll IsNot Nothing AndAlso GetLayerRelationType(ll.Name) <> RelationType.Undefined AndAlso ReportData.SelectedRows.Count > 0 Then
                _selectedShapeUid = GetReportDataSelectedShapeUid()
                RelationToolStripMenuItem.Enabled = True
            End If

            ShapeDataMenu.Show(sender, New Point(e.x, e.y))
        End If
    End Sub

    Private Sub ReportData_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ReportData.SelectionChanged
        If AutoZoomToolStripMenuItem.Checked Then
            OnZoomToShape(sender, e)
        End If
    End Sub

    Private Sub SelectAllToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SelectAllToolStripMenuItem.Click
        SelectShapes(False, SelectState.Selected)
    End Sub

    Private Sub DeselectAllToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeselectAllToolStripMenuItem.Click
        SelectShapes(False, SelectState.Deselected)
    End Sub

    Private Sub InvertSelectionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InvertSelectionToolStripMenuItem.Click
        SelectShapes(False, SelectState.Inverted)
    End Sub

    Private Sub SelectGroupToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SelectGroupToolStripMenuItem.Click
        'Deselect all shapes before selecting group
        SelectShapes(False, SelectState.Deselected)
        SelectShapes(True, SelectState.Selected)
    End Sub

    Private Sub DeselectGroupToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeselectGroupToolStripMenuItem.Click
        SelectShapes(True, SelectState.Deselected)
    End Sub

    Private Sub CachedModeToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CachedModeToolStripMenuItem.Click
        SetCachedMode(Not _cachedMode)
    End Sub

    Private Sub AddHierarchyLayerEventHandler(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim i As Integer
        Dim grp As TGIS_HierarchyGroup

        'Find out which menu item caused this event
        For i = 0 To AddHierarchyLayerToolStripMenuItem.DropDownItems.Count - 1
            If sender Is AddHierarchyLayerToolStripMenuItem.DropDownItems(i) Then
                'Add selected layer to selected group in hierarchy
                grp = GisCtrl.Hierarchy.Groups(_activeLayerName)
                If grp IsNot Nothing Then
                    grp.AddLayer(GisCtrl.Get(AddHierarchyLayerToolStripMenuItem.DropDownItems(i).Name))
                    ControlHierarchy.Update()
                    GisCtrl.SaveProject()
                End If

                Exit For
            End If
        Next
    End Sub

    Private Sub ConditionToolStripEventHandler(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim i As Integer
        Dim field As String
        Dim type As TGIS_FieldType
        Dim ll As TGIS_LayerVector

        ll = TryCast(GetActiveLayer(), TGIS_LayerVector)
        If ll IsNot Nothing Then
            'Find out which menu item caused this event
            For i = 0 To ConditionsToolStripMenuItem.DropDownItems.Count - 1
                If sender Is ConditionsToolStripMenuItem.DropDownItems(i) Then
                    'Determine field type
                    field = ConditionsToolStripMenuItem.DropDownItems(i).Text
                    If Not GetFieldType(ll, field, type) Then Continue For 'Skip this field if type cannot be checked

                    'Show filter condition dialog for current field
                    Dim dlg As New ConditionDialog(field, type, _filterConditions(field))
                    If dlg.ShowDialog() = DialogResult.OK Then
                        If dlg.RemoveCondition Then
                            'Remove filter condition
                            _filterConditions.Remove(field)
                            CType(ConditionsToolStripMenuItem.DropDownItems(i), ToolStripMenuItem).Checked = False
                        Else
                            'Add/update filter condition
                            _filterConditions(field) = dlg.ConditionSet
                            CType(ConditionsToolStripMenuItem.DropDownItems(i), ToolStripMenuItem).Checked = True
                        End If

                        'Update field data viewer (we do this by flagging for a layer change so if ListFieldData is already running it will restart with the new conditions)
                        _layerChanged = True
                    End If

                    Exit For
                End If
            Next
        End If
    End Sub

    Private Sub DataTimer_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataTimer.Tick
        If GisCtrl IsNot Nothing Then
            'Refresh field data view in case active layer was changed
            If _layerChanged = True Then
                'Make sure layer still exist
                Dim cmd As New OleDbCommand("", Conn)
                Dim ll As TGIS_LayerAbstract = GetActiveLayer()

                If ll IsNot Nothing Then
                    If TypeOf ll Is TGIS_LayerSqlAdo Then
                        cmd.CommandText = "SELECT 1 FROM ttkGISLayerSQL WHERE NAME = '" & GisTablePrefix & ll.Name & "'"
                    ElseIf TypeOf ll Is TGIS_LayerPixelStoreAdo2 Then
                        cmd.CommandText = "SELECT 1 FROM ttkGISPixelStore2 WHERE NAME = '" & GisTablePrefix & ll.Name & "'"
                    End If
                    If cmd.CommandText <> vbNullString Then
                        Dim reader As OleDbDataReader = cmd.ExecuteReader()
                        If Not reader.HasRows Then
                            'Layer has been removed (by another user)
                            Dim title As String = ll.Caption
                            GisCtrl.Delete(_activeLayerName)
                            GisCtrl.SaveProject()
                            ControlLegend.Update()
                            ControlHierarchy.Update()
                            SetActiveLayer(Nothing)
                            MsgBox(title & vbCrLf & LangStr(_strIdLayerDeleted), MsgBoxStyle.Exclamation)
                        End If
                    End If
                End If

                ListFieldData()
            End If

            'This is a workaround to get the same exact visible extent as when the window was closed
            'On the first BeforePaint event _initializeViewport will be flagged and this timer will restore the extent
            If _initializeViewport AndAlso Not _viewportInitialized Then
                Dim ext As TGIS_Extent
                Dim regkey As RegistryKey

                regkey = Registry.CurrentUser.OpenSubKey(RegKeyViewport, False)
                If regkey IsNot Nothing Then
                    ext.XMax = regkey.GetValue("XMax")
                    ext.XMin = regkey.GetValue("XMin")
                    ext.YMax = regkey.GetValue("YMax")
                    ext.YMin = regkey.GetValue("YMin")
                    regkey.Close()

                    GisCtrl.VisibleExtent = ext
                End If
                _viewportInitialized = True
            End If
        End If
    End Sub
#End Region
End Class
