﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ObjectDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.uxCancel = New System.Windows.Forms.Button
        Me.uxOK = New System.Windows.Forms.Button
        Me.lblWidth = New System.Windows.Forms.Label
        Me.PropertyList = New System.Windows.Forms.ListBox
        Me.lblUnit = New System.Windows.Forms.Label
        Me.txtWidth = New System.Windows.Forms.TextBox
        Me.lblProperties = New System.Windows.Forms.Label
        Me.cmbDestinationLayer = New System.Windows.Forms.ComboBox
        Me.lblDestinationLayer = New System.Windows.Forms.Label
        Me.chkHighlight = New System.Windows.Forms.CheckBox
        Me.lblCount = New System.Windows.Forms.Label
        Me.uxUpdate = New System.Windows.Forms.Button
        Me.uxAction = New System.Windows.Forms.GroupBox
        Me.uxObjectList = New System.Windows.Forms.ComboBox
        Me.uxNoAction = New System.Windows.Forms.RadioButton
        Me.uxExistingObject = New System.Windows.Forms.RadioButton
        Me.uxNewObject = New System.Windows.Forms.RadioButton
        Me.uxAction.SuspendLayout()
        Me.SuspendLayout()
        '
        'uxCancel
        '
        Me.uxCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.uxCancel.Location = New System.Drawing.Point(248, 275)
        Me.uxCancel.Name = "uxCancel"
        Me.uxCancel.Size = New System.Drawing.Size(75, 23)
        Me.uxCancel.TabIndex = 11
        Me.uxCancel.Text = "uxCancel"
        Me.uxCancel.UseVisualStyleBackColor = True
        '
        'uxOK
        '
        Me.uxOK.Location = New System.Drawing.Point(167, 275)
        Me.uxOK.Name = "uxOK"
        Me.uxOK.Size = New System.Drawing.Size(75, 23)
        Me.uxOK.TabIndex = 10
        Me.uxOK.Text = "uxOK"
        Me.uxOK.UseVisualStyleBackColor = True
        '
        'lblWidth
        '
        Me.lblWidth.AutoSize = True
        Me.lblWidth.Location = New System.Drawing.Point(9, 177)
        Me.lblWidth.Name = "lblWidth"
        Me.lblWidth.Size = New System.Drawing.Size(45, 13)
        Me.lblWidth.TabIndex = 17
        Me.lblWidth.Text = "lblWidth"
        '
        'PropertyList
        '
        Me.PropertyList.FormattingEnabled = True
        Me.PropertyList.Location = New System.Drawing.Point(268, 25)
        Me.PropertyList.Name = "PropertyList"
        Me.PropertyList.Size = New System.Drawing.Size(212, 225)
        Me.PropertyList.TabIndex = 9
        '
        'lblUnit
        '
        Me.lblUnit.AutoSize = True
        Me.lblUnit.Location = New System.Drawing.Point(73, 197)
        Me.lblUnit.Name = "lblUnit"
        Me.lblUnit.Size = New System.Drawing.Size(15, 13)
        Me.lblUnit.TabIndex = 19
        Me.lblUnit.Text = "m"
        '
        'txtWidth
        '
        Me.txtWidth.Location = New System.Drawing.Point(12, 193)
        Me.txtWidth.Name = "txtWidth"
        Me.txtWidth.Size = New System.Drawing.Size(59, 20)
        Me.txtWidth.TabIndex = 6
        Me.txtWidth.Text = "1,0"
        '
        'lblProperties
        '
        Me.lblProperties.AutoSize = True
        Me.lblProperties.Location = New System.Drawing.Point(265, 9)
        Me.lblProperties.Name = "lblProperties"
        Me.lblProperties.Size = New System.Drawing.Size(64, 13)
        Me.lblProperties.TabIndex = 23
        Me.lblProperties.Text = "lblProperties"
        '
        'cmbDestinationLayer
        '
        Me.cmbDestinationLayer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDestinationLayer.FormattingEnabled = True
        Me.cmbDestinationLayer.Location = New System.Drawing.Point(12, 25)
        Me.cmbDestinationLayer.Name = "cmbDestinationLayer"
        Me.cmbDestinationLayer.Size = New System.Drawing.Size(237, 21)
        Me.cmbDestinationLayer.TabIndex = 0
        '
        'lblDestinationLayer
        '
        Me.lblDestinationLayer.AutoSize = True
        Me.lblDestinationLayer.Location = New System.Drawing.Point(9, 9)
        Me.lblDestinationLayer.Name = "lblDestinationLayer"
        Me.lblDestinationLayer.Size = New System.Drawing.Size(96, 13)
        Me.lblDestinationLayer.TabIndex = 25
        Me.lblDestinationLayer.Text = "lblDestinationLayer"
        '
        'chkHighlight
        '
        Me.chkHighlight.AutoSize = True
        Me.chkHighlight.Location = New System.Drawing.Point(12, 220)
        Me.chkHighlight.Name = "chkHighlight"
        Me.chkHighlight.Size = New System.Drawing.Size(85, 17)
        Me.chkHighlight.TabIndex = 8
        Me.chkHighlight.Text = "chkHighlight"
        Me.chkHighlight.UseVisualStyleBackColor = True
        '
        'lblCount
        '
        Me.lblCount.Location = New System.Drawing.Point(355, 253)
        Me.lblCount.Name = "lblCount"
        Me.lblCount.Size = New System.Drawing.Size(125, 13)
        Me.lblCount.TabIndex = 27
        Me.lblCount.Text = "lblCount"
        Me.lblCount.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'uxUpdate
        '
        Me.uxUpdate.Location = New System.Drawing.Point(94, 191)
        Me.uxUpdate.Name = "uxUpdate"
        Me.uxUpdate.Size = New System.Drawing.Size(75, 23)
        Me.uxUpdate.TabIndex = 7
        Me.uxUpdate.Text = "uxUpdate"
        Me.uxUpdate.UseVisualStyleBackColor = True
        '
        'uxAction
        '
        Me.uxAction.Controls.Add(Me.uxObjectList)
        Me.uxAction.Controls.Add(Me.uxNoAction)
        Me.uxAction.Controls.Add(Me.uxExistingObject)
        Me.uxAction.Controls.Add(Me.uxNewObject)
        Me.uxAction.Location = New System.Drawing.Point(12, 52)
        Me.uxAction.Name = "uxAction"
        Me.uxAction.Size = New System.Drawing.Size(237, 122)
        Me.uxAction.TabIndex = 1
        Me.uxAction.TabStop = False
        Me.uxAction.Text = "uxAction"
        '
        'uxObjectList
        '
        Me.uxObjectList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.uxObjectList.FormattingEnabled = True
        Me.uxObjectList.Location = New System.Drawing.Point(7, 67)
        Me.uxObjectList.Name = "uxObjectList"
        Me.uxObjectList.Size = New System.Drawing.Size(214, 21)
        Me.uxObjectList.TabIndex = 4
        '
        'uxNoAction
        '
        Me.uxNoAction.AutoSize = True
        Me.uxNoAction.Location = New System.Drawing.Point(7, 94)
        Me.uxNoAction.Name = "uxNoAction"
        Me.uxNoAction.Size = New System.Drawing.Size(80, 17)
        Me.uxNoAction.TabIndex = 5
        Me.uxNoAction.Text = "uxNoAction"
        Me.uxNoAction.UseVisualStyleBackColor = True
        '
        'uxExistingObject
        '
        Me.uxExistingObject.AutoSize = True
        Me.uxExistingObject.Location = New System.Drawing.Point(7, 43)
        Me.uxExistingObject.Name = "uxExistingObject"
        Me.uxExistingObject.Size = New System.Drawing.Size(103, 17)
        Me.uxExistingObject.TabIndex = 3
        Me.uxExistingObject.Text = "uxExistingObject"
        Me.uxExistingObject.UseVisualStyleBackColor = True
        '
        'uxNewObject
        '
        Me.uxNewObject.AutoSize = True
        Me.uxNewObject.Checked = True
        Me.uxNewObject.Location = New System.Drawing.Point(7, 20)
        Me.uxNewObject.Name = "uxNewObject"
        Me.uxNewObject.Size = New System.Drawing.Size(89, 17)
        Me.uxNewObject.TabIndex = 2
        Me.uxNewObject.TabStop = True
        Me.uxNewObject.Text = "uxNewObject"
        Me.uxNewObject.UseVisualStyleBackColor = True
        '
        'ObjectDialog
        '
        Me.AcceptButton = Me.uxOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.uxCancel
        Me.ClientSize = New System.Drawing.Size(491, 305)
        Me.Controls.Add(Me.uxAction)
        Me.Controls.Add(Me.uxUpdate)
        Me.Controls.Add(Me.lblCount)
        Me.Controls.Add(Me.chkHighlight)
        Me.Controls.Add(Me.cmbDestinationLayer)
        Me.Controls.Add(Me.lblDestinationLayer)
        Me.Controls.Add(Me.PropertyList)
        Me.Controls.Add(Me.lblProperties)
        Me.Controls.Add(Me.txtWidth)
        Me.Controls.Add(Me.lblUnit)
        Me.Controls.Add(Me.lblWidth)
        Me.Controls.Add(Me.uxCancel)
        Me.Controls.Add(Me.uxOK)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ObjectDialog"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "ObjectDialog"
        Me.uxAction.ResumeLayout(False)
        Me.uxAction.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents uxCancel As System.Windows.Forms.Button
    Friend WithEvents uxOK As System.Windows.Forms.Button
    Friend WithEvents lblWidth As System.Windows.Forms.Label
    Friend WithEvents PropertyList As System.Windows.Forms.ListBox
    Friend WithEvents lblUnit As System.Windows.Forms.Label
    Friend WithEvents txtWidth As System.Windows.Forms.TextBox
    Friend WithEvents lblProperties As System.Windows.Forms.Label
    Friend WithEvents cmbDestinationLayer As System.Windows.Forms.ComboBox
    Friend WithEvents lblDestinationLayer As System.Windows.Forms.Label
    Friend WithEvents chkHighlight As System.Windows.Forms.CheckBox
    Friend WithEvents lblCount As System.Windows.Forms.Label
    Friend WithEvents uxUpdate As System.Windows.Forms.Button
    Friend WithEvents uxAction As System.Windows.Forms.GroupBox
    Friend WithEvents uxNoAction As System.Windows.Forms.RadioButton
    Friend WithEvents uxExistingObject As System.Windows.Forms.RadioButton
    Friend WithEvents uxNewObject As System.Windows.Forms.RadioButton
    Friend WithEvents uxObjectList As System.Windows.Forms.ComboBox
End Class
