﻿Imports System.IO
Imports System.Windows.Forms
Imports TatukGIS.NDK

Public Class SettingsDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 2700
    Private Const _strIdOK As Integer = 2701
    Private Const _strIdCancel As Integer = 2702
    Private Const _strIdScaleUnits As Integer = 2703
    Private Const _strIdLength As Integer = 2704
    Private Const _strIdArea As Integer = 2705
    Private Const _strIdProjection As Integer = 2706
    Private Const _strIdProjectionLabel As Integer = 2707
    Private Const _strIdChooseProjection As Integer = 2708
    Private Const _strIdPaths As Integer = 2709
    Private Const _strIdProjectDir As Integer = 2710
    Private Const _strIdEditMode As Integer = 2711
    Private Const _strIdNearestPoint As Integer = 2712
    Private Const _strIdAfterActivePoint As Integer = 2713
    Private Const _strIdSnapType As Integer = 2714
    Private Const _strIdSnapToPoint As Integer = 2715
    Private Const _strIdSnapToLine As Integer = 2716
    Private Const _strIdRouteMode As Integer = 2717
    Private Const _strIdPreferRows As Integer = 2718
    Private Const _strIdPreferColumns As Integer = 2719
    Private Const _strIdPathNotFound As Integer = 2720
    Private Const _strIdDirectoryAccessError As Integer = 2721
    Private Const _strIdOther As Integer = 2722
    Private Const _strIdNorthArrow As Integer = 2723
    Private Const _strIdMapRotation As Integer = 2724
#End Region

#Region "Declarations"
    Private _lengthUnits As TGIS_CSUnits
    Private _areaUnits As TGIS_CSUnits
    Private _cs As TGIS_CSCoordinateSystem
    Private _projectDir As String
    Private _em As EditMode
    Private _st As TGIS_EditorSnapType
    Private _rd As RouteDirection
    Private _northArrowVisible As Boolean
    Private _maprotation As Integer

    Public Sub New(ByVal lunit As TGIS_CSUnits, ByVal aunit As TGIS_CSUnits, ByVal cs As TGIS_CSCoordinateSystem, ByVal projectPath As String, _
                   ByVal em As EditMode, ByVal st As TGIS_EditorSnapType, ByVal rd As RouteDirection, ByVal northArrowVisible As Boolean, ByVal maprotation As Integer)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _lengthUnits = lunit
        _areaUnits = aunit
        _cs = cs
        _em = em
        _st = st
        _rd = rd
        _northArrowVisible = northArrowVisible
        _maprotation = maprotation

        'Extract directory for project path
        Dim fi As New FileInfo(projectPath)
        _projectDir = fi.DirectoryName
    End Sub

    Public ReadOnly Property LengthUnits() As TGIS_CSUnits
        Get
            Return _lengthUnits
        End Get
    End Property

    Public ReadOnly Property AreaUnits() As TGIS_CSUnits
        Get
            Return _areaUnits
        End Get
    End Property

    Public ReadOnly Property CS() As TGIS_CSCoordinateSystem
        Get
            Return _cs
        End Get
    End Property

    Public ReadOnly Property ProjectDir() As String
        Get
            Return _projectDir
        End Get
    End Property

    Public ReadOnly Property EditMode() As EditMode
        Get
            Return _em
        End Get
    End Property

    Public ReadOnly Property SnapType() As TGIS_EditorSnapType
        Get
            Return _st
        End Get
    End Property

    Public ReadOnly Property RouteDirection() As RouteDirection
        Get
            Return _rd
        End Get
    End Property

    Public ReadOnly Property NorthArrowVisible() As Boolean
        Get
            Return _northArrowVisible
        End Get
    End Property

    Public ReadOnly Property MapRotation() As Integer
        Get
            Return _maprotation
        End Get
    End Property
#End Region

#Region "Helper functions"
    Private Sub AddLengthUnit(ByVal u As TGIS_CSUnits)
        Dim idx As Integer = uxLength.Items.Add(New ValueDescriptionPair(u.EPSG, u.Description))
        If _lengthUnits.EPSG = u.EPSG Then uxLength.SelectedIndex = idx
    End Sub

    Private Sub AddAreaUnit(ByVal u As TGIS_CSUnits)
        Dim idx As Integer = uxArea.Items.Add(New ValueDescriptionPair(u.EPSG, u.Description))
        If _areaUnits.EPSG = u.EPSG Then uxArea.SelectedIndex = idx
    End Sub

    Private Function ValidatePath() As Boolean
        Dim filePath As String = uxProjectDir.Text & "\" & System.Guid.NewGuid().ToString()
        Dim ret As Boolean

        Try
            'Check directory
            Dim fstream As New FileStream(filePath, FileMode.Create)
            Dim writer As New StreamWriter(fstream)
            writer.WriteLine(vbNullString)
            fstream.Close()

            _projectDir = uxProjectDir.Text
            ret = True
        Catch ex As DirectoryNotFoundException
            'Invalid path
            MsgBox(GisControlView.LangStr(_strIdPathNotFound), MsgBoxStyle.Exclamation)
        Catch ex As UnauthorizedAccessException
            'No write access to directory
            MsgBox(GisControlView.LangStr(_strIdDirectoryAccessError), MsgBoxStyle.Exclamation)
        Finally
            'Delete test file (if created)
            Try
                FileIO.FileSystem.DeleteFile(filePath)
            Catch ex As DirectoryNotFoundException
            Catch ex As FileNotFoundException
            End Try
        End Try

        Return ret
    End Function
#End Region

#Region "Event handlers"
    Private Sub SettingsDialog_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim i As Integer
        Dim ulst As New TGIS_CSUnitsList
        Dim cl As New SortedList, ca As New SortedList

        'Set up language
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        uxOK.Text = GisControlView.LangStr(_strIdOK)
        uxCancel.Text = GisControlView.LangStr(_strIdCancel)
        uxScaleUnits.Text = GisControlView.LangStr(_strIdScaleUnits)
        uxLengthLabel.Text = GisControlView.LangStr(_strIdLength)
        uxAreaLabel.Text = GisControlView.LangStr(_strIdArea)
        uxProjection.Text = GisControlView.LangStr(_strIdProjection)
        uxProjectionLabel.Text = GisControlView.LangStr(_strIdProjectionLabel)
        uxChooseProjection.Text = GisControlView.LangStr(_strIdChooseProjection)
        uxPaths.Text = GisControlView.LangStr(_strIdPaths)
        uxProjectDirLabel.Text = GisControlView.LangStr(_strIdProjectDir)
        uxEditMode.Text = GisControlView.LangStr(_strIdEditMode)
        uxNearestPoint.Text = GisControlView.LangStr(_strIdNearestPoint)
        uxAfterActivePoint.Text = GisControlView.LangStr(_strIdAfterActivePoint)
        uxSnapType.Text = GisControlView.LangStr(_strIdSnapType)
        uxSnapToPoint.Text = GisControlView.LangStr(_strIdSnapToPoint)
        uxSnapToLine.Text = GisControlView.LangStr(_strIdSnapToLine)
        uxRouteMode.Text = GisControlView.LangStr(_strIdRouteMode)
        uxPreferRows.Text = GisControlView.LangStr(_strIdPreferRows)
        uxPreferColumns.Text = GisControlView.LangStr(_strIdPreferColumns)
        uxOther.Text = GisControlView.LangStr(_strIdOther)
        uxNorthArrow.Text = GisControlView.LangStr(_strIdNorthArrow)
        uxMapRotationLabel.Text = GisControlView.LangStr(_strIdMapRotation)

        'List avaliable unit types
        AddLengthUnit(ulst.ByEPSG(GisControlView.EpsgMetric)) 'Metric
        AddLengthUnit(ulst.ByEPSG(GisControlView.EpsgUS)) 'US
        AddAreaUnit(ulst.ByEPSG(GisControlView.EpsgMetric)) 'Metric
        AddAreaUnit(ulst.ByEPSG(GisControlView.EpsgUS)) 'US

        'Sort items in alphabetic order
        For i = 1 To ulst.Count() - 1
            If ulst(i).EPSG <> GisControlView.EpsgMetric AndAlso ulst(i).EPSG <> GisControlView.EpsgUS Then 'Metric and US already listed
                If ulst(i).UnitsType = TGIS_CSUnitsType.gisCSUnitsTypeLinear OrElse ulst(i).UnitsType = TGIS_CSUnitsType.gisCSUnitsTypeAuto Then
                    cl.Add(ulst(i).Description, ulst(i).EPSG)
                    ca.Add(ulst(i).Description, ulst(i).EPSG)
                ElseIf ulst(i).UnitsType = TGIS_CSUnitsType.gisCSUnitsTypeAreal Then
                    ca.Add(ulst(i).Description, ulst(i).EPSG)
                End If
            End If
        Next

        For i = 0 To cl.Count - 1
            AddLengthUnit(ulst.ByEPSG(cl.GetByIndex(i)))
        Next
        For i = 0 To ca.Count - 1
            AddAreaUnit(ulst.ByEPSG(ca.GetByIndex(i)))
        Next

        'Projection, project path
        uxProjectionDesc.Text = _cs.Description
        uxProjectDir.Text = _projectDir

        'Modes
        If _em = GisControl.EditMode.NearestPoint Then
            uxNearestPoint.Checked = True
        Else
            uxAfterActivePoint.Checked = True
        End If
        If _st = TGIS_EditorSnapType.gisEditorSnapTypePoint Then
            uxSnapToPoint.Checked = True
        Else
            uxSnapToLine.Checked = True
        End If
        If _rd = GisControl.RouteDirection.Row Then
            uxPreferRows.Checked = True
        Else
            uxPreferColumns.Checked = True
        End If

        'Other settings
        uxNorthArrow.Checked = _northArrowVisible

        uxMapRotation.Value = _maprotation
    End Sub

    Private Sub uxOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxOK.Click
        If ValidatePath() Then
            'Store Settings
            Dim ulst As New TGIS_CSUnitsList
            _lengthUnits = ulst.ByEPSG(CType(uxLength.SelectedItem, ValueDescriptionPair).Value)
            _areaUnits = ulst.ByEPSG(CType(uxArea.SelectedItem, ValueDescriptionPair).Value)
            '_cs is already set
            _projectDir = uxProjectDir.Text
            If uxNearestPoint.Checked Then _em = GisControl.EditMode.NearestPoint Else _em = GisControl.EditMode.AfterActivePoint
            If uxSnapToPoint.Checked Then _st = TGIS_EditorSnapType.gisEditorSnapTypePoint Else _st = TGIS_EditorSnapType.gisEditorSnapTypeLine
            If uxPreferRows.Checked Then _rd = RouteDirection.Row Else _rd = RouteDirection.Column
            _northArrowVisible = uxNorthArrow.Checked
            _maprotation = uxMapRotation.Value

            Me.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Close()
        End If
    End Sub

    Private Sub uxCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxCancel.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub uxChooseProjection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxChooseProjection.Click
        Dim dlg As New CoordinateSystemDialog(_cs)
        If dlg.ShowDialog() = Windows.Forms.DialogResult.OK Then
            _cs = dlg.CS
            uxProjectionDesc.Text = dlg.CS.Description
        End If
    End Sub

    Private Sub uxBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxBrowse.Click
        Dim dlg As New FolderBrowserDialog
        If dlg.ShowDialog() = Windows.Forms.DialogResult.OK Then
            uxProjectDir.Text = dlg.SelectedPath
        End If
    End Sub
#End Region
End Class
