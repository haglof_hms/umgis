﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GisPrintPreviewDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.uxPrintPreview = New TatukGIS.NDK.WinForms.TGIS_ControlPrintPreview
        Me.uxTitleText = New System.Windows.Forms.TextBox
        Me.uxTitleLabel = New System.Windows.Forms.Label
        Me.uxDisplayTitleCheck = New System.Windows.Forms.CheckBox
        Me.uxMainPanel = New System.Windows.Forms.Panel
        Me.uxScaleLabel = New System.Windows.Forms.Label
        Me.uxScaleText = New System.Windows.Forms.TextBox
        Me.uxFontButton = New System.Windows.Forms.Button
        Me.uxUpdateButton = New System.Windows.Forms.Button
        Me.uxPageSetupButton = New System.Windows.Forms.Button
        Me.uxPrintButton = New System.Windows.Forms.Button
        Me.uxMainPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'uxPrintPreview
        '
        Me.uxPrintPreview.BackColor = System.Drawing.Color.Gray
        Me.uxPrintPreview.GIS_Viewer = Nothing
        Me.uxPrintPreview.Location = New System.Drawing.Point(3, 56)
        Me.uxPrintPreview.Name = "uxPrintPreview"
        Me.uxPrintPreview.Size = New System.Drawing.Size(596, 160)
        Me.uxPrintPreview.TabIndex = 1
        '
        'uxTitleText
        '
        Me.uxTitleText.Location = New System.Drawing.Point(48, 7)
        Me.uxTitleText.Name = "uxTitleText"
        Me.uxTitleText.Size = New System.Drawing.Size(188, 20)
        Me.uxTitleText.TabIndex = 4
        '
        'uxTitleLabel
        '
        Me.uxTitleLabel.Location = New System.Drawing.Point(3, 10)
        Me.uxTitleLabel.Name = "uxTitleLabel"
        Me.uxTitleLabel.Size = New System.Drawing.Size(39, 16)
        Me.uxTitleLabel.TabIndex = 5
        Me.uxTitleLabel.Text = "uxTitleLabel"
        '
        'uxDisplayTitleCheck
        '
        Me.uxDisplayTitleCheck.AutoSize = True
        Me.uxDisplayTitleCheck.Checked = True
        Me.uxDisplayTitleCheck.CheckState = System.Windows.Forms.CheckState.Checked
        Me.uxDisplayTitleCheck.Location = New System.Drawing.Point(48, 33)
        Me.uxDisplayTitleCheck.Name = "uxDisplayTitleCheck"
        Me.uxDisplayTitleCheck.Size = New System.Drawing.Size(122, 17)
        Me.uxDisplayTitleCheck.TabIndex = 6
        Me.uxDisplayTitleCheck.Text = "uxDisplayTitleCheck"
        Me.uxDisplayTitleCheck.UseVisualStyleBackColor = True
        '
        'uxMainPanel
        '
        Me.uxMainPanel.Controls.Add(Me.uxScaleLabel)
        Me.uxMainPanel.Controls.Add(Me.uxScaleText)
        Me.uxMainPanel.Controls.Add(Me.uxFontButton)
        Me.uxMainPanel.Controls.Add(Me.uxUpdateButton)
        Me.uxMainPanel.Controls.Add(Me.uxPageSetupButton)
        Me.uxMainPanel.Controls.Add(Me.uxPrintButton)
        Me.uxMainPanel.Controls.Add(Me.uxPrintPreview)
        Me.uxMainPanel.Controls.Add(Me.uxDisplayTitleCheck)
        Me.uxMainPanel.Controls.Add(Me.uxTitleLabel)
        Me.uxMainPanel.Controls.Add(Me.uxTitleText)
        Me.uxMainPanel.Location = New System.Drawing.Point(12, 12)
        Me.uxMainPanel.Name = "uxMainPanel"
        Me.uxMainPanel.Size = New System.Drawing.Size(603, 219)
        Me.uxMainPanel.TabIndex = 7
        '
        'uxScaleLabel
        '
        Me.uxScaleLabel.Location = New System.Drawing.Point(244, 10)
        Me.uxScaleLabel.Name = "uxScaleLabel"
        Me.uxScaleLabel.Size = New System.Drawing.Size(16, 16)
        Me.uxScaleLabel.TabIndex = 12
        Me.uxScaleLabel.Text = "1:"
        '
        'uxScaleText
        '
        Me.uxScaleText.Location = New System.Drawing.Point(260, 7)
        Me.uxScaleText.Name = "uxScaleText"
        Me.uxScaleText.Size = New System.Drawing.Size(105, 20)
        Me.uxScaleText.TabIndex = 11
        '
        'uxFontButton
        '
        Me.uxFontButton.Location = New System.Drawing.Point(379, 29)
        Me.uxFontButton.Name = "uxFontButton"
        Me.uxFontButton.Size = New System.Drawing.Size(107, 23)
        Me.uxFontButton.TabIndex = 8
        Me.uxFontButton.Text = "uxFontButton"
        Me.uxFontButton.UseVisualStyleBackColor = True
        '
        'uxUpdateButton
        '
        Me.uxUpdateButton.Location = New System.Drawing.Point(379, 3)
        Me.uxUpdateButton.Name = "uxUpdateButton"
        Me.uxUpdateButton.Size = New System.Drawing.Size(107, 23)
        Me.uxUpdateButton.TabIndex = 7
        Me.uxUpdateButton.Text = "uxUpdateButton"
        Me.uxUpdateButton.UseVisualStyleBackColor = True
        '
        'uxPageSetupButton
        '
        Me.uxPageSetupButton.Location = New System.Drawing.Point(492, 29)
        Me.uxPageSetupButton.Name = "uxPageSetupButton"
        Me.uxPageSetupButton.Size = New System.Drawing.Size(107, 23)
        Me.uxPageSetupButton.TabIndex = 10
        Me.uxPageSetupButton.Text = "uxPageSetupButton"
        Me.uxPageSetupButton.UseVisualStyleBackColor = True
        '
        'uxPrintButton
        '
        Me.uxPrintButton.Location = New System.Drawing.Point(492, 3)
        Me.uxPrintButton.Name = "uxPrintButton"
        Me.uxPrintButton.Size = New System.Drawing.Size(107, 23)
        Me.uxPrintButton.TabIndex = 9
        Me.uxPrintButton.Text = "uxPrintButton"
        Me.uxPrintButton.UseVisualStyleBackColor = True
        '
        'GisPrintPreviewDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(774, 435)
        Me.Controls.Add(Me.uxMainPanel)
        Me.MinimizeBox = False
        Me.Name = "GisPrintPreviewDialog"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "GisPrintPreviewDialog"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.uxMainPanel.ResumeLayout(False)
        Me.uxMainPanel.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents uxPrintPreview As TatukGIS.NDK.WinForms.TGIS_ControlPrintPreview
    Friend WithEvents uxTitleText As System.Windows.Forms.TextBox
    Friend WithEvents uxTitleLabel As System.Windows.Forms.Label
    Friend WithEvents uxDisplayTitleCheck As System.Windows.Forms.CheckBox
    Friend WithEvents uxMainPanel As System.Windows.Forms.Panel
    Friend WithEvents uxPageSetupButton As System.Windows.Forms.Button
    Friend WithEvents uxPrintButton As System.Windows.Forms.Button
    Friend WithEvents uxUpdateButton As System.Windows.Forms.Button
    Friend WithEvents uxFontButton As System.Windows.Forms.Button
    Friend WithEvents uxScaleLabel As System.Windows.Forms.Label
    Friend WithEvents uxScaleText As System.Windows.Forms.TextBox

End Class
