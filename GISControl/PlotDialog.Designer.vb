﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PlotDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnOK = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.grpMethod = New System.Windows.Forms.GroupBox
        Me.btnRandomize = New System.Windows.Forms.RadioButton
        Me.btnUseGrid = New System.Windows.Forms.RadioButton
        Me.txtDistanceX = New System.Windows.Forms.TextBox
        Me.txtNumPlots = New System.Windows.Forms.TextBox
        Me.lblDistance = New System.Windows.Forms.Label
        Me.lblDensity = New System.Windows.Forms.Label
        Me.txtAngle = New System.Windows.Forms.TextBox
        Me.lblAngle = New System.Windows.Forms.Label
        Me.chkHideOutside = New System.Windows.Forms.CheckBox
        Me.cmbDestinationLayer = New System.Windows.Forms.ComboBox
        Me.lblDestinationLayer = New System.Windows.Forms.Label
        Me.txtDistanceY = New System.Windows.Forms.TextBox
        Me.lblX = New System.Windows.Forms.Label
        Me.lblY = New System.Windows.Forms.Label
        Me.btnUpdate = New System.Windows.Forms.Button
        Me.txtMinDistance = New System.Windows.Forms.TextBox
        Me.lblMinDistance = New System.Windows.Forms.Label
        Me.txtNoise = New System.Windows.Forms.NumericUpDown
        Me.lblNoise = New System.Windows.Forms.Label
        Me.txtOffset = New System.Windows.Forms.TextBox
        Me.lblOffset = New System.Windows.Forms.Label
        Me.lstMaskLayers = New System.Windows.Forms.ListBox
        Me.lblMaskLayers = New System.Windows.Forms.Label
        Me.cmbUnits = New System.Windows.Forms.ComboBox
        Me.lblUnits = New System.Windows.Forms.Label
        Me.grpMethod.SuspendLayout()
        CType(Me.txtNoise, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(34, 255)
        Me.btnOK.Margin = New System.Windows.Forms.Padding(2)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(63, 23)
        Me.btnOK.TabIndex = 12
        Me.btnOK.Text = "btnOK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(100, 255)
        Me.btnCancel.Margin = New System.Windows.Forms.Padding(2)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(63, 23)
        Me.btnCancel.TabIndex = 13
        Me.btnCancel.Text = "btnCancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'grpMethod
        '
        Me.grpMethod.Controls.Add(Me.btnRandomize)
        Me.grpMethod.Controls.Add(Me.btnUseGrid)
        Me.grpMethod.Location = New System.Drawing.Point(12, 159)
        Me.grpMethod.Name = "grpMethod"
        Me.grpMethod.Size = New System.Drawing.Size(180, 67)
        Me.grpMethod.TabIndex = 10
        Me.grpMethod.TabStop = False
        Me.grpMethod.Text = "grpMethod"
        '
        'btnRandomize
        '
        Me.btnRandomize.AutoSize = True
        Me.btnRandomize.Location = New System.Drawing.Point(6, 43)
        Me.btnRandomize.Name = "btnRandomize"
        Me.btnRandomize.Size = New System.Drawing.Size(93, 17)
        Me.btnRandomize.TabIndex = 32
        Me.btnRandomize.TabStop = True
        Me.btnRandomize.Text = "btnRandomize"
        Me.btnRandomize.UseVisualStyleBackColor = True
        '
        'btnUseGrid
        '
        Me.btnUseGrid.AutoSize = True
        Me.btnUseGrid.Location = New System.Drawing.Point(6, 20)
        Me.btnUseGrid.Name = "btnUseGrid"
        Me.btnUseGrid.Size = New System.Drawing.Size(78, 17)
        Me.btnUseGrid.TabIndex = 31
        Me.btnUseGrid.TabStop = True
        Me.btnUseGrid.Text = "btnUseGrid"
        Me.btnUseGrid.UseVisualStyleBackColor = True
        '
        'txtDistanceX
        '
        Me.txtDistanceX.Location = New System.Drawing.Point(25, 73)
        Me.txtDistanceX.Name = "txtDistanceX"
        Me.txtDistanceX.Size = New System.Drawing.Size(40, 20)
        Me.txtDistanceX.TabIndex = 2
        '
        'txtNumPlots
        '
        Me.txtNumPlots.Location = New System.Drawing.Point(9, 121)
        Me.txtNumPlots.Name = "txtNumPlots"
        Me.txtNumPlots.Size = New System.Drawing.Size(125, 20)
        Me.txtNumPlots.TabIndex = 6
        '
        'lblDistance
        '
        Me.lblDistance.AutoSize = True
        Me.lblDistance.Location = New System.Drawing.Point(6, 56)
        Me.lblDistance.Name = "lblDistance"
        Me.lblDistance.Size = New System.Drawing.Size(59, 13)
        Me.lblDistance.TabIndex = 5
        Me.lblDistance.Text = "lblDistance"
        '
        'lblDensity
        '
        Me.lblDensity.AutoSize = True
        Me.lblDensity.Location = New System.Drawing.Point(6, 104)
        Me.lblDensity.Name = "lblDensity"
        Me.lblDensity.Size = New System.Drawing.Size(52, 13)
        Me.lblDensity.TabIndex = 6
        Me.lblDensity.Text = "lblDensity"
        '
        'txtAngle
        '
        Me.txtAngle.Location = New System.Drawing.Point(154, 73)
        Me.txtAngle.Name = "txtAngle"
        Me.txtAngle.Size = New System.Drawing.Size(40, 20)
        Me.txtAngle.TabIndex = 4
        Me.txtAngle.Text = "0"
        '
        'lblAngle
        '
        Me.lblAngle.AutoSize = True
        Me.lblAngle.Location = New System.Drawing.Point(151, 56)
        Me.lblAngle.Name = "lblAngle"
        Me.lblAngle.Size = New System.Drawing.Size(44, 13)
        Me.lblAngle.TabIndex = 8
        Me.lblAngle.Text = "lblAngle"
        '
        'chkHideOutside
        '
        Me.chkHideOutside.AutoSize = True
        Me.chkHideOutside.Checked = True
        Me.chkHideOutside.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkHideOutside.Location = New System.Drawing.Point(12, 232)
        Me.chkHideOutside.Name = "chkHideOutside"
        Me.chkHideOutside.Size = New System.Drawing.Size(102, 17)
        Me.chkHideOutside.TabIndex = 11
        Me.chkHideOutside.Text = "chkHideOutside"
        Me.chkHideOutside.UseVisualStyleBackColor = True
        '
        'cmbDestinationLayer
        '
        Me.cmbDestinationLayer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDestinationLayer.FormattingEnabled = True
        Me.cmbDestinationLayer.Location = New System.Drawing.Point(9, 25)
        Me.cmbDestinationLayer.Name = "cmbDestinationLayer"
        Me.cmbDestinationLayer.Size = New System.Drawing.Size(183, 21)
        Me.cmbDestinationLayer.TabIndex = 0
        '
        'lblDestinationLayer
        '
        Me.lblDestinationLayer.AutoSize = True
        Me.lblDestinationLayer.Location = New System.Drawing.Point(6, 9)
        Me.lblDestinationLayer.Name = "lblDestinationLayer"
        Me.lblDestinationLayer.Size = New System.Drawing.Size(96, 13)
        Me.lblDestinationLayer.TabIndex = 27
        Me.lblDestinationLayer.Text = "lblDestinationLayer"
        '
        'txtDistanceY
        '
        Me.txtDistanceY.Location = New System.Drawing.Point(94, 73)
        Me.txtDistanceY.Name = "txtDistanceY"
        Me.txtDistanceY.Size = New System.Drawing.Size(40, 20)
        Me.txtDistanceY.TabIndex = 3
        '
        'lblX
        '
        Me.lblX.AutoSize = True
        Me.lblX.Location = New System.Drawing.Point(6, 76)
        Me.lblX.Name = "lblX"
        Me.lblX.Size = New System.Drawing.Size(17, 13)
        Me.lblX.TabIndex = 29
        Me.lblX.Text = "X:"
        '
        'lblY
        '
        Me.lblY.AutoSize = True
        Me.lblY.Location = New System.Drawing.Point(71, 76)
        Me.lblY.Name = "lblY"
        Me.lblY.Size = New System.Drawing.Size(17, 13)
        Me.lblY.TabIndex = 30
        Me.lblY.Text = "Y:"
        '
        'btnUpdate
        '
        Me.btnUpdate.AutoSize = True
        Me.btnUpdate.Location = New System.Drawing.Point(166, 255)
        Me.btnUpdate.Margin = New System.Windows.Forms.Padding(2)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(67, 23)
        Me.btnUpdate.TabIndex = 14
        Me.btnUpdate.Text = "btnUpdate"
        Me.btnUpdate.UseVisualStyleBackColor = True
        '
        'txtMinDistance
        '
        Me.txtMinDistance.Location = New System.Drawing.Point(214, 121)
        Me.txtMinDistance.Name = "txtMinDistance"
        Me.txtMinDistance.Size = New System.Drawing.Size(40, 20)
        Me.txtMinDistance.TabIndex = 7
        Me.txtMinDistance.Text = "0"
        '
        'lblMinDistance
        '
        Me.lblMinDistance.AutoSize = True
        Me.lblMinDistance.Location = New System.Drawing.Point(211, 104)
        Me.lblMinDistance.Name = "lblMinDistance"
        Me.lblMinDistance.Size = New System.Drawing.Size(76, 13)
        Me.lblMinDistance.TabIndex = 33
        Me.lblMinDistance.Text = "lblMinDistance"
        '
        'txtNoise
        '
        Me.txtNoise.Location = New System.Drawing.Point(214, 73)
        Me.txtNoise.Name = "txtNoise"
        Me.txtNoise.Size = New System.Drawing.Size(40, 20)
        Me.txtNoise.TabIndex = 5
        '
        'lblNoise
        '
        Me.lblNoise.AutoSize = True
        Me.lblNoise.Location = New System.Drawing.Point(211, 56)
        Me.lblNoise.Name = "lblNoise"
        Me.lblNoise.Size = New System.Drawing.Size(44, 13)
        Me.lblNoise.TabIndex = 35
        Me.lblNoise.Text = "lblNoise"
        '
        'txtOffset
        '
        Me.txtOffset.Location = New System.Drawing.Point(214, 169)
        Me.txtOffset.Name = "txtOffset"
        Me.txtOffset.Size = New System.Drawing.Size(40, 20)
        Me.txtOffset.TabIndex = 8
        Me.txtOffset.Text = "0"
        '
        'lblOffset
        '
        Me.lblOffset.AutoSize = True
        Me.lblOffset.Location = New System.Drawing.Point(211, 152)
        Me.lblOffset.Name = "lblOffset"
        Me.lblOffset.Size = New System.Drawing.Size(45, 13)
        Me.lblOffset.TabIndex = 37
        Me.lblOffset.Text = "lblOffset"
        '
        'lstMaskLayers
        '
        Me.lstMaskLayers.FormattingEnabled = True
        Me.lstMaskLayers.Location = New System.Drawing.Point(311, 25)
        Me.lstMaskLayers.Name = "lstMaskLayers"
        Me.lstMaskLayers.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.lstMaskLayers.Size = New System.Drawing.Size(196, 251)
        Me.lstMaskLayers.TabIndex = 9
        '
        'lblMaskLayers
        '
        Me.lblMaskLayers.AutoSize = True
        Me.lblMaskLayers.Location = New System.Drawing.Point(308, 9)
        Me.lblMaskLayers.Name = "lblMaskLayers"
        Me.lblMaskLayers.Size = New System.Drawing.Size(74, 13)
        Me.lblMaskLayers.TabIndex = 39
        Me.lblMaskLayers.Text = "lblMaskLayers"
        '
        'cmbUnits
        '
        Me.cmbUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbUnits.DropDownWidth = 200
        Me.cmbUnits.FormattingEnabled = True
        Me.cmbUnits.Location = New System.Drawing.Point(214, 25)
        Me.cmbUnits.Name = "cmbUnits"
        Me.cmbUnits.Size = New System.Drawing.Size(91, 21)
        Me.cmbUnits.TabIndex = 1
        '
        'lblUnits
        '
        Me.lblUnits.AutoSize = True
        Me.lblUnits.Location = New System.Drawing.Point(212, 9)
        Me.lblUnits.Name = "lblUnits"
        Me.lblUnits.Size = New System.Drawing.Size(41, 13)
        Me.lblUnits.TabIndex = 41
        Me.lblUnits.Text = "lblUnits"
        '
        'PlotDialog
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(515, 284)
        Me.Controls.Add(Me.cmbUnits)
        Me.Controls.Add(Me.lblUnits)
        Me.Controls.Add(Me.lstMaskLayers)
        Me.Controls.Add(Me.lblMaskLayers)
        Me.Controls.Add(Me.txtOffset)
        Me.Controls.Add(Me.lblOffset)
        Me.Controls.Add(Me.lblNoise)
        Me.Controls.Add(Me.txtNoise)
        Me.Controls.Add(Me.txtMinDistance)
        Me.Controls.Add(Me.lblMinDistance)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.lblY)
        Me.Controls.Add(Me.lblX)
        Me.Controls.Add(Me.txtDistanceY)
        Me.Controls.Add(Me.cmbDestinationLayer)
        Me.Controls.Add(Me.lblDestinationLayer)
        Me.Controls.Add(Me.txtAngle)
        Me.Controls.Add(Me.chkHideOutside)
        Me.Controls.Add(Me.lblAngle)
        Me.Controls.Add(Me.lblDensity)
        Me.Controls.Add(Me.lblDistance)
        Me.Controls.Add(Me.txtNumPlots)
        Me.Controls.Add(Me.txtDistanceX)
        Me.Controls.Add(Me.grpMethod)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "PlotDialog"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "PlotDialog"
        Me.grpMethod.ResumeLayout(False)
        Me.grpMethod.PerformLayout()
        CType(Me.txtNoise, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents grpMethod As System.Windows.Forms.GroupBox
    Friend WithEvents btnRandomize As System.Windows.Forms.RadioButton
    Friend WithEvents btnUseGrid As System.Windows.Forms.RadioButton
    Friend WithEvents txtDistanceX As System.Windows.Forms.TextBox
    Friend WithEvents txtNumPlots As System.Windows.Forms.TextBox
    Friend WithEvents lblDistance As System.Windows.Forms.Label
    Friend WithEvents lblDensity As System.Windows.Forms.Label
    Friend WithEvents txtAngle As System.Windows.Forms.TextBox
    Friend WithEvents lblAngle As System.Windows.Forms.Label
    Friend WithEvents chkHideOutside As System.Windows.Forms.CheckBox
    Friend WithEvents cmbDestinationLayer As System.Windows.Forms.ComboBox
    Friend WithEvents lblDestinationLayer As System.Windows.Forms.Label
    Friend WithEvents txtDistanceY As System.Windows.Forms.TextBox
    Friend WithEvents lblX As System.Windows.Forms.Label
	Friend WithEvents lblY As System.Windows.Forms.Label
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    Friend WithEvents txtMinDistance As System.Windows.Forms.TextBox
    Friend WithEvents lblMinDistance As System.Windows.Forms.Label
    Friend WithEvents txtNoise As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblNoise As System.Windows.Forms.Label
    Friend WithEvents txtOffset As System.Windows.Forms.TextBox
    Friend WithEvents lblOffset As System.Windows.Forms.Label
    Friend WithEvents lstMaskLayers As System.Windows.Forms.ListBox
    Friend WithEvents lblMaskLayers As System.Windows.Forms.Label
    Friend WithEvents cmbUnits As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnits As System.Windows.Forms.Label
End Class
