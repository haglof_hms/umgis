﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GeoreferenceDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnOK = New System.Windows.Forms.Button
        Me.txtScaleX = New System.Windows.Forms.TextBox
        Me.txtScaleY = New System.Windows.Forms.TextBox
        Me.txtReferenceX = New System.Windows.Forms.TextBox
        Me.txtReferenceY = New System.Windows.Forms.TextBox
        Me.lblScale = New System.Windows.Forms.Label
        Me.lblCoordinates = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(95, 143)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 8
        Me.btnCancel.Text = "btnCancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(14, 143)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 7
        Me.btnOK.Text = "btnOK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'txtScaleX
        '
        Me.txtScaleX.Location = New System.Drawing.Point(35, 25)
        Me.txtScaleX.Name = "txtScaleX"
        Me.txtScaleX.Size = New System.Drawing.Size(40, 20)
        Me.txtScaleX.TabIndex = 9
        Me.txtScaleX.Text = "1"
        '
        'txtScaleY
        '
        Me.txtScaleY.Location = New System.Drawing.Point(104, 25)
        Me.txtScaleY.Name = "txtScaleY"
        Me.txtScaleY.Size = New System.Drawing.Size(40, 20)
        Me.txtScaleY.TabIndex = 10
        Me.txtScaleY.Text = "1"
        '
        'txtReferenceX
        '
        Me.txtReferenceX.Location = New System.Drawing.Point(35, 81)
        Me.txtReferenceX.Name = "txtReferenceX"
        Me.txtReferenceX.Size = New System.Drawing.Size(121, 20)
        Me.txtReferenceX.TabIndex = 11
        '
        'txtReferenceY
        '
        Me.txtReferenceY.Location = New System.Drawing.Point(35, 107)
        Me.txtReferenceY.Name = "txtReferenceY"
        Me.txtReferenceY.Size = New System.Drawing.Size(121, 20)
        Me.txtReferenceY.TabIndex = 12
        '
        'lblScale
        '
        Me.lblScale.AutoSize = True
        Me.lblScale.Location = New System.Drawing.Point(12, 6)
        Me.lblScale.Name = "lblScale"
        Me.lblScale.Size = New System.Drawing.Size(44, 13)
        Me.lblScale.TabIndex = 13
        Me.lblScale.Text = "lblScale"
        '
        'lblCoordinates
        '
        Me.lblCoordinates.AutoSize = True
        Me.lblCoordinates.Location = New System.Drawing.Point(12, 61)
        Me.lblCoordinates.Name = "lblCoordinates"
        Me.lblCoordinates.Size = New System.Drawing.Size(73, 13)
        Me.lblCoordinates.TabIndex = 14
        Me.lblCoordinates.Text = "lblCoordinates"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(17, 13)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "X:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(81, 28)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(17, 13)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "Y:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 84)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(17, 13)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "X:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 110)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(17, 13)
        Me.Label4.TabIndex = 18
        Me.Label4.Text = "Y:"
        '
        'GeoreferenceDialog
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(184, 174)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblCoordinates)
        Me.Controls.Add(Me.lblScale)
        Me.Controls.Add(Me.txtReferenceY)
        Me.Controls.Add(Me.txtReferenceX)
        Me.Controls.Add(Me.txtScaleY)
        Me.Controls.Add(Me.txtScaleX)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "GeoreferenceDialog"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "GeoreferenceDialog"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents txtScaleX As System.Windows.Forms.TextBox
    Friend WithEvents txtScaleY As System.Windows.Forms.TextBox
    Friend WithEvents txtReferenceX As System.Windows.Forms.TextBox
    Friend WithEvents txtReferenceY As System.Windows.Forms.TextBox
    Friend WithEvents lblScale As System.Windows.Forms.Label
    Friend WithEvents lblCoordinates As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
End Class
