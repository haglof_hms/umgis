@ECHO OFF
SET NETREACTOR=C:\Program Files (x86)\Eziriz\.NET Reactor\dotNET_Reactor.exe

:: Remove quotes
SET PROJECTDIR=%2
SET PROJECTDIR=%PROJECTDIR:~1,-1%
SET OUTFILE=%PROJECTDIR%bin\Release\GISControl_Secure\GISControl.dll

IF "%1" == "Release" (
  IF NOT EXIST "%NETREACTOR%" (
    ECHO Could not find .NET Reactor. Make sure it's installed.
    GOTO EXIT_ERR
  )

  ECHO Running .NET Reactor...
  IF EXIST "%OUTFILE%" (DEL "%OUTFILE%")
  "%NETREACTOR%" -licensed -file "%PROJECTDIR%bin\Release\GisControl.dll"
  IF EXIST "%OUTFILE%" (
    copy "%OUTFILE%" "%HMSROOT%"
  ) ELSE (
    ECHO .NET Reactor failed. Make sure it is registered for current user.
  )
) ELSE IF "%1" == "Debug" (
  copy "%PROJECTDIR%bin\Debug\GisControl.dll" "C:\Program Files (x86)\Haglof Sweden AB\Haglof Management Systems\"
  copy "%PROJECTDIR%bin\Debug\GisControl.pdb" "C:\Program Files (x86)\Haglof Sweden AB\Haglof Management Systems\"
) ELSE (
  ECHO Unknown configuration.
  GOTO EXIT_ERR
)

EXIT /B 0


:EXIT_ERR
EXIT /B 1