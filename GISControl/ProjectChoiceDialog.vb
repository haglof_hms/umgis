﻿Imports System.Data.OleDb

Public Class ProjectChoiceDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 3600
    Private Const _strIdOK As Integer = 3601
    Private Const _strIdCancel As Integer = 3602
    Private Const _strIdDescription As Integer = 3603
#End Region

#Region "Declarations"
    Private _user As String

    Public ReadOnly Property User()
        Get
            Return _user
        End Get
    End Property
#End Region

#Region "Event handlers"
    Private Sub ProjectChoiceDialog_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim cmd As New OleDbCommand("", GisControlView.Conn)
        Dim reader As OleDbDataReader

        uxOK.Enabled = False

        'Set up language
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        uxOK.Text = GisControlView.LangStr(_strIdOK)
        uxCancel.Text = GisControlView.LangStr(_strIdCancel)
        uxDescription.Text = GisControlView.LangStr(_strIdDescription)

        'List users with project files
        cmd.CommandText = "SELECT [user] FROM gis_project_files"
        reader = cmd.ExecuteReader()
        Do While reader.Read()
            uxUser.Items.Add(reader.GetString(0))
        Loop
        reader.Close()
    End Sub

    Private Sub uxOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxOK.Click
        _user = uxUser.SelectedItem
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub uxUser_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxUser.SelectedIndexChanged
        uxOK.Enabled = uxUser.SelectedIndex >= 0
    End Sub
#End Region
End Class
