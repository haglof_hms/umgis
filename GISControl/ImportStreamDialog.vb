﻿Public Class ImportStreamDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 3500
    Private Const _strIdAddress As Integer = 3501
    Private Const _strIdProtocol As Integer = 3502
    Private Const _strIdOK As Integer = 3503
    Private Const _strIdCancel As Integer = 3504
    Private Const _strIdProtocolAuto As Integer = 3505
#End Region

#Region "Declarations"
    Private _address As String
    Private _protocol As ServiceProtocol

    Public ReadOnly Property Address() As String
        Get
            Return _address
        End Get
    End Property

    Public ReadOnly Property Protocol() As ServiceProtocol
        Get
            Return _protocol
        End Get
    End Property
#End Region

#Region "Event handlers"
    Private Sub ImportStreamDialog_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Set up language
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        uxAddressLabel.Text = GisControlView.LangStr(_strIdAddress)
        uxProtocolLabel.Text = GisControlView.LangStr(_strIdProtocol)
        uxOK.Text = GisControlView.LangStr(_strIdOK)
        uxCancel.Text = GisControlView.LangStr(_strIdCancel)

        'List protocols
        uxProtocol.Items.Add(New ValueDescriptionPair(ServiceProtocol.Auto, GisControlView.LangStr(_strIdProtocolAuto)))
        uxProtocol.Items.Add(New ValueDescriptionPair(ServiceProtocol.WMS, "WMS"))
        uxProtocol.Items.Add(New ValueDescriptionPair(ServiceProtocol.WFS, "WFS"))
        uxProtocol.Items.Add(New ValueDescriptionPair(ServiceProtocol.WMTS, "WMTS"))
        uxProtocol.Items.Add(New ValueDescriptionPair(ServiceProtocol.ECWP, "ECWP"))
        uxProtocol.SelectedIndex = 0 'Default Auto

        uxOK.Enabled = False
    End Sub

    Private Sub uxOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxOK.Click
        _address = uxAddress.Text
        _protocol = CType(uxProtocol.SelectedItem, ValueDescriptionPair).Value

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub uxAddress_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxAddress.TextChanged
        uxOK.Enabled = (uxAddress.Text <> vbNullString)
    End Sub
#End Region
End Class
