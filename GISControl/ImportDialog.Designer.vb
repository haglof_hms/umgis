﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ImportDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ImportDialog))
        Me.cmbLayers = New System.Windows.Forms.ComboBox
        Me.txtTitle = New System.Windows.Forms.TextBox
        Me.uxNext = New System.Windows.Forms.Button
        Me.uxCancel = New System.Windows.Forms.Button
        Me.txtFilterShapes = New System.Windows.Forms.TextBox
        Me.btnCheckAllShapes = New System.Windows.Forms.Button
        Me.btnUncheckAllShapes = New System.Windows.Forms.Button
        Me.lblFilterShapes = New System.Windows.Forms.Label
        Me.uxShapeList = New AxXtremeReportControl.AxReportControl
        Me.FieldMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmbCounty = New System.Windows.Forms.ComboBox
        Me.lblCounty = New System.Windows.Forms.Label
        Me.cmbMunicipal = New System.Windows.Forms.ComboBox
        Me.lblMunicipal = New System.Windows.Forms.Label
        Me.cmbParish = New System.Windows.Forms.ComboBox
        Me.lblParish = New System.Windows.Forms.Label
        Me.cmbPropNumber = New System.Windows.Forms.ComboBox
        Me.lblPropNumber = New System.Windows.Forms.Label
        Me.cmbPropName = New System.Windows.Forms.ComboBox
        Me.lblPropName = New System.Windows.Forms.Label
        Me.cmbBlock = New System.Windows.Forms.ComboBox
        Me.lblBlock = New System.Windows.Forms.Label
        Me.cmbUnit = New System.Windows.Forms.ComboBox
        Me.lblUnit = New System.Windows.Forms.Label
        Me.cmbArea = New System.Windows.Forms.ComboBox
        Me.lblArea = New System.Windows.Forms.Label
        Me.cmbMeasuredArea = New System.Windows.Forms.ComboBox
        Me.lblMeasuredArea = New System.Windows.Forms.Label
        Me.cmbObjID = New System.Windows.Forms.ComboBox
        Me.lblObjID = New System.Windows.Forms.Label
        Me.grpProperties = New System.Windows.Forms.GroupBox
        Me.chkAddProperties = New System.Windows.Forms.CheckBox
        Me.grpShapes = New System.Windows.Forms.GroupBox
        Me.btnFilterShapes = New System.Windows.Forms.Button
        Me.grpLayer = New System.Windows.Forms.GroupBox
        Me.optExistingLayer = New System.Windows.Forms.RadioButton
        Me.optNewHDLayer = New System.Windows.Forms.RadioButton
        Me.optNewDBLayer = New System.Windows.Forms.RadioButton
        Me.grpAttributes = New System.Windows.Forms.GroupBox
        Me.uxFieldPanel = New System.Windows.Forms.Panel
        Me.uxTabs = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.grpSections = New System.Windows.Forms.GroupBox
        Me.txtFilterSections = New System.Windows.Forms.TextBox
        Me.uxSectionList = New AxXtremeReportControl.AxReportControl
        Me.btnCheckAllSections = New System.Windows.Forms.Button
        Me.lblFilterSections = New System.Windows.Forms.Label
        Me.btnUncheckAllSections = New System.Windows.Forms.Button
        Me.TabPage4 = New System.Windows.Forms.TabPage
        Me.TabPage5 = New System.Windows.Forms.TabPage
        Me.uxPrevious = New System.Windows.Forms.Button
        Me.uxPanel = New System.Windows.Forms.Panel
        Me.lblDescription = New System.Windows.Forms.Label
        Me.uxCreateSections = New System.Windows.Forms.CheckBox
        CType(Me.uxShapeList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpProperties.SuspendLayout()
        Me.grpShapes.SuspendLayout()
        Me.grpLayer.SuspendLayout()
        Me.grpAttributes.SuspendLayout()
        Me.uxTabs.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.grpSections.SuspendLayout()
        CType(Me.uxSectionList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmbLayers
        '
        Me.cmbLayers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbLayers.FormattingEnabled = True
        Me.cmbLayers.Location = New System.Drawing.Point(24, 114)
        Me.cmbLayers.Name = "cmbLayers"
        Me.cmbLayers.Size = New System.Drawing.Size(233, 21)
        Me.cmbLayers.TabIndex = 4
        '
        'txtTitle
        '
        Me.txtTitle.Location = New System.Drawing.Point(24, 65)
        Me.txtTitle.Name = "txtTitle"
        Me.txtTitle.Size = New System.Drawing.Size(233, 20)
        Me.txtTitle.TabIndex = 2
        '
        'uxNext
        '
        Me.uxNext.Location = New System.Drawing.Point(477, 461)
        Me.uxNext.Name = "uxNext"
        Me.uxNext.Size = New System.Drawing.Size(75, 23)
        Me.uxNext.TabIndex = 102
        Me.uxNext.Text = "uxNext"
        Me.uxNext.UseVisualStyleBackColor = True
        '
        'uxCancel
        '
        Me.uxCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.uxCancel.Location = New System.Drawing.Point(315, 461)
        Me.uxCancel.Name = "uxCancel"
        Me.uxCancel.Size = New System.Drawing.Size(75, 23)
        Me.uxCancel.TabIndex = 100
        Me.uxCancel.Text = "uxCancel"
        Me.uxCancel.UseVisualStyleBackColor = True
        '
        'txtFilterShapes
        '
        Me.txtFilterShapes.Location = New System.Drawing.Point(84, 343)
        Me.txtFilterShapes.Name = "txtFilterShapes"
        Me.txtFilterShapes.Size = New System.Drawing.Size(147, 20)
        Me.txtFilterShapes.TabIndex = 5
        '
        'btnCheckAllShapes
        '
        Me.btnCheckAllShapes.Location = New System.Drawing.Point(341, 343)
        Me.btnCheckAllShapes.Name = "btnCheckAllShapes"
        Me.btnCheckAllShapes.Size = New System.Drawing.Size(88, 23)
        Me.btnCheckAllShapes.TabIndex = 7
        Me.btnCheckAllShapes.Text = "btnCheckAllShapes"
        Me.btnCheckAllShapes.UseVisualStyleBackColor = True
        '
        'btnUncheckAllShapes
        '
        Me.btnUncheckAllShapes.Location = New System.Drawing.Point(435, 343)
        Me.btnUncheckAllShapes.Name = "btnUncheckAllShapes"
        Me.btnUncheckAllShapes.Size = New System.Drawing.Size(88, 23)
        Me.btnUncheckAllShapes.TabIndex = 8
        Me.btnUncheckAllShapes.Text = "btnUncheckAllShapes"
        Me.btnUncheckAllShapes.UseVisualStyleBackColor = True
        '
        'lblFilterShapes
        '
        Me.lblFilterShapes.AutoSize = True
        Me.lblFilterShapes.Location = New System.Drawing.Point(11, 349)
        Me.lblFilterShapes.Name = "lblFilterShapes"
        Me.lblFilterShapes.Size = New System.Drawing.Size(75, 13)
        Me.lblFilterShapes.TabIndex = 12
        Me.lblFilterShapes.Text = "lblFilterShapes"
        '
        'uxShapeList
        '
        Me.uxShapeList.Location = New System.Drawing.Point(6, 19)
        Me.uxShapeList.Name = "uxShapeList"
        Me.uxShapeList.OcxState = CType(resources.GetObject("uxShapeList.OcxState"), System.Windows.Forms.AxHost.State)
        Me.uxShapeList.Size = New System.Drawing.Size(520, 318)
        Me.uxShapeList.TabIndex = 4
        '
        'FieldMenu
        '
        Me.FieldMenu.Name = "FieldMenu"
        Me.FieldMenu.Size = New System.Drawing.Size(61, 4)
        '
        'cmbCounty
        '
        Me.cmbCounty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCounty.Enabled = False
        Me.cmbCounty.FormattingEnabled = True
        Me.cmbCounty.Location = New System.Drawing.Point(94, 40)
        Me.cmbCounty.Name = "cmbCounty"
        Me.cmbCounty.Size = New System.Drawing.Size(150, 21)
        Me.cmbCounty.TabIndex = 10
        '
        'lblCounty
        '
        Me.lblCounty.AutoSize = True
        Me.lblCounty.Enabled = False
        Me.lblCounty.Location = New System.Drawing.Point(6, 48)
        Me.lblCounty.Name = "lblCounty"
        Me.lblCounty.Size = New System.Drawing.Size(50, 13)
        Me.lblCounty.TabIndex = 15
        Me.lblCounty.Text = "lblCounty"
        '
        'cmbMunicipal
        '
        Me.cmbMunicipal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMunicipal.Enabled = False
        Me.cmbMunicipal.FormattingEnabled = True
        Me.cmbMunicipal.Location = New System.Drawing.Point(94, 64)
        Me.cmbMunicipal.Name = "cmbMunicipal"
        Me.cmbMunicipal.Size = New System.Drawing.Size(150, 21)
        Me.cmbMunicipal.TabIndex = 11
        '
        'lblMunicipal
        '
        Me.lblMunicipal.AutoSize = True
        Me.lblMunicipal.Enabled = False
        Me.lblMunicipal.Location = New System.Drawing.Point(6, 72)
        Me.lblMunicipal.Name = "lblMunicipal"
        Me.lblMunicipal.Size = New System.Drawing.Size(62, 13)
        Me.lblMunicipal.TabIndex = 17
        Me.lblMunicipal.Text = "lblMunicipal"
        '
        'cmbParish
        '
        Me.cmbParish.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbParish.Enabled = False
        Me.cmbParish.FormattingEnabled = True
        Me.cmbParish.Location = New System.Drawing.Point(94, 88)
        Me.cmbParish.Name = "cmbParish"
        Me.cmbParish.Size = New System.Drawing.Size(150, 21)
        Me.cmbParish.TabIndex = 12
        '
        'lblParish
        '
        Me.lblParish.AutoSize = True
        Me.lblParish.Enabled = False
        Me.lblParish.Location = New System.Drawing.Point(6, 96)
        Me.lblParish.Name = "lblParish"
        Me.lblParish.Size = New System.Drawing.Size(46, 13)
        Me.lblParish.TabIndex = 19
        Me.lblParish.Text = "lblParish"
        '
        'cmbPropNumber
        '
        Me.cmbPropNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPropNumber.Enabled = False
        Me.cmbPropNumber.FormattingEnabled = True
        Me.cmbPropNumber.Location = New System.Drawing.Point(94, 112)
        Me.cmbPropNumber.Name = "cmbPropNumber"
        Me.cmbPropNumber.Size = New System.Drawing.Size(150, 21)
        Me.cmbPropNumber.TabIndex = 13
        '
        'lblPropNumber
        '
        Me.lblPropNumber.AutoSize = True
        Me.lblPropNumber.Enabled = False
        Me.lblPropNumber.Location = New System.Drawing.Point(6, 120)
        Me.lblPropNumber.Name = "lblPropNumber"
        Me.lblPropNumber.Size = New System.Drawing.Size(76, 13)
        Me.lblPropNumber.TabIndex = 21
        Me.lblPropNumber.Text = "lblPropNumber"
        '
        'cmbPropName
        '
        Me.cmbPropName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPropName.Enabled = False
        Me.cmbPropName.FormattingEnabled = True
        Me.cmbPropName.Location = New System.Drawing.Point(94, 136)
        Me.cmbPropName.Name = "cmbPropName"
        Me.cmbPropName.Size = New System.Drawing.Size(150, 21)
        Me.cmbPropName.TabIndex = 14
        '
        'lblPropName
        '
        Me.lblPropName.AutoSize = True
        Me.lblPropName.Enabled = False
        Me.lblPropName.Location = New System.Drawing.Point(6, 144)
        Me.lblPropName.Name = "lblPropName"
        Me.lblPropName.Size = New System.Drawing.Size(67, 13)
        Me.lblPropName.TabIndex = 23
        Me.lblPropName.Text = "lblPropName"
        '
        'cmbBlock
        '
        Me.cmbBlock.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBlock.Enabled = False
        Me.cmbBlock.FormattingEnabled = True
        Me.cmbBlock.Location = New System.Drawing.Point(360, 40)
        Me.cmbBlock.Name = "cmbBlock"
        Me.cmbBlock.Size = New System.Drawing.Size(150, 21)
        Me.cmbBlock.TabIndex = 15
        '
        'lblBlock
        '
        Me.lblBlock.AutoSize = True
        Me.lblBlock.Enabled = False
        Me.lblBlock.Location = New System.Drawing.Point(272, 48)
        Me.lblBlock.Name = "lblBlock"
        Me.lblBlock.Size = New System.Drawing.Size(44, 13)
        Me.lblBlock.TabIndex = 25
        Me.lblBlock.Text = "lblBlock"
        '
        'cmbUnit
        '
        Me.cmbUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbUnit.Enabled = False
        Me.cmbUnit.FormattingEnabled = True
        Me.cmbUnit.Location = New System.Drawing.Point(360, 64)
        Me.cmbUnit.Name = "cmbUnit"
        Me.cmbUnit.Size = New System.Drawing.Size(150, 21)
        Me.cmbUnit.TabIndex = 16
        '
        'lblUnit
        '
        Me.lblUnit.AutoSize = True
        Me.lblUnit.Enabled = False
        Me.lblUnit.Location = New System.Drawing.Point(272, 72)
        Me.lblUnit.Name = "lblUnit"
        Me.lblUnit.Size = New System.Drawing.Size(36, 13)
        Me.lblUnit.TabIndex = 27
        Me.lblUnit.Text = "lblUnit"
        '
        'cmbArea
        '
        Me.cmbArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbArea.Enabled = False
        Me.cmbArea.FormattingEnabled = True
        Me.cmbArea.Location = New System.Drawing.Point(360, 88)
        Me.cmbArea.Name = "cmbArea"
        Me.cmbArea.Size = New System.Drawing.Size(150, 21)
        Me.cmbArea.TabIndex = 17
        '
        'lblArea
        '
        Me.lblArea.AutoSize = True
        Me.lblArea.Enabled = False
        Me.lblArea.Location = New System.Drawing.Point(272, 96)
        Me.lblArea.Name = "lblArea"
        Me.lblArea.Size = New System.Drawing.Size(39, 13)
        Me.lblArea.TabIndex = 29
        Me.lblArea.Text = "lblArea"
        '
        'cmbMeasuredArea
        '
        Me.cmbMeasuredArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMeasuredArea.Enabled = False
        Me.cmbMeasuredArea.FormattingEnabled = True
        Me.cmbMeasuredArea.Location = New System.Drawing.Point(360, 112)
        Me.cmbMeasuredArea.Name = "cmbMeasuredArea"
        Me.cmbMeasuredArea.Size = New System.Drawing.Size(150, 21)
        Me.cmbMeasuredArea.TabIndex = 18
        '
        'lblMeasuredArea
        '
        Me.lblMeasuredArea.AutoSize = True
        Me.lblMeasuredArea.Enabled = False
        Me.lblMeasuredArea.Location = New System.Drawing.Point(272, 120)
        Me.lblMeasuredArea.Name = "lblMeasuredArea"
        Me.lblMeasuredArea.Size = New System.Drawing.Size(86, 13)
        Me.lblMeasuredArea.TabIndex = 31
        Me.lblMeasuredArea.Text = "lblMeasuredArea"
        '
        'cmbObjID
        '
        Me.cmbObjID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbObjID.Enabled = False
        Me.cmbObjID.FormattingEnabled = True
        Me.cmbObjID.Location = New System.Drawing.Point(360, 136)
        Me.cmbObjID.Name = "cmbObjID"
        Me.cmbObjID.Size = New System.Drawing.Size(150, 21)
        Me.cmbObjID.TabIndex = 19
        '
        'lblObjID
        '
        Me.lblObjID.AutoSize = True
        Me.lblObjID.Enabled = False
        Me.lblObjID.Location = New System.Drawing.Point(272, 144)
        Me.lblObjID.Name = "lblObjID"
        Me.lblObjID.Size = New System.Drawing.Size(44, 13)
        Me.lblObjID.TabIndex = 33
        Me.lblObjID.Text = "lblObjID"
        '
        'grpProperties
        '
        Me.grpProperties.Controls.Add(Me.chkAddProperties)
        Me.grpProperties.Controls.Add(Me.lblCounty)
        Me.grpProperties.Controls.Add(Me.cmbObjID)
        Me.grpProperties.Controls.Add(Me.cmbCounty)
        Me.grpProperties.Controls.Add(Me.lblObjID)
        Me.grpProperties.Controls.Add(Me.lblMunicipal)
        Me.grpProperties.Controls.Add(Me.cmbMeasuredArea)
        Me.grpProperties.Controls.Add(Me.cmbMunicipal)
        Me.grpProperties.Controls.Add(Me.lblMeasuredArea)
        Me.grpProperties.Controls.Add(Me.lblParish)
        Me.grpProperties.Controls.Add(Me.cmbArea)
        Me.grpProperties.Controls.Add(Me.cmbParish)
        Me.grpProperties.Controls.Add(Me.lblArea)
        Me.grpProperties.Controls.Add(Me.lblPropNumber)
        Me.grpProperties.Controls.Add(Me.cmbUnit)
        Me.grpProperties.Controls.Add(Me.cmbPropNumber)
        Me.grpProperties.Controls.Add(Me.lblUnit)
        Me.grpProperties.Controls.Add(Me.lblPropName)
        Me.grpProperties.Controls.Add(Me.cmbBlock)
        Me.grpProperties.Controls.Add(Me.cmbPropName)
        Me.grpProperties.Controls.Add(Me.lblBlock)
        Me.grpProperties.Location = New System.Drawing.Point(6, 6)
        Me.grpProperties.Name = "grpProperties"
        Me.grpProperties.Size = New System.Drawing.Size(523, 171)
        Me.grpProperties.TabIndex = 35
        Me.grpProperties.TabStop = False
        Me.grpProperties.Text = "grpProperties"
        '
        'chkAddProperties
        '
        Me.chkAddProperties.AutoSize = True
        Me.chkAddProperties.Location = New System.Drawing.Point(9, 19)
        Me.chkAddProperties.Name = "chkAddProperties"
        Me.chkAddProperties.Size = New System.Drawing.Size(110, 17)
        Me.chkAddProperties.TabIndex = 9
        Me.chkAddProperties.Text = "chkAddProperties"
        Me.chkAddProperties.UseVisualStyleBackColor = True
        '
        'grpShapes
        '
        Me.grpShapes.Controls.Add(Me.txtFilterShapes)
        Me.grpShapes.Controls.Add(Me.btnFilterShapes)
        Me.grpShapes.Controls.Add(Me.uxShapeList)
        Me.grpShapes.Controls.Add(Me.btnCheckAllShapes)
        Me.grpShapes.Controls.Add(Me.lblFilterShapes)
        Me.grpShapes.Controls.Add(Me.btnUncheckAllShapes)
        Me.grpShapes.Location = New System.Drawing.Point(6, 6)
        Me.grpShapes.Name = "grpShapes"
        Me.grpShapes.Size = New System.Drawing.Size(532, 372)
        Me.grpShapes.TabIndex = 36
        Me.grpShapes.TabStop = False
        Me.grpShapes.Text = "grpShapes"
        '
        'btnFilterShapes
        '
        Me.btnFilterShapes.Location = New System.Drawing.Point(260, 343)
        Me.btnFilterShapes.Name = "btnFilterShapes"
        Me.btnFilterShapes.Size = New System.Drawing.Size(75, 23)
        Me.btnFilterShapes.TabIndex = 6
        Me.btnFilterShapes.Text = "btnFilterShapes"
        Me.btnFilterShapes.UseVisualStyleBackColor = True
        '
        'grpLayer
        '
        Me.grpLayer.Controls.Add(Me.optExistingLayer)
        Me.grpLayer.Controls.Add(Me.optNewHDLayer)
        Me.grpLayer.Controls.Add(Me.optNewDBLayer)
        Me.grpLayer.Controls.Add(Me.cmbLayers)
        Me.grpLayer.Controls.Add(Me.txtTitle)
        Me.grpLayer.Location = New System.Drawing.Point(6, 6)
        Me.grpLayer.Name = "grpLayer"
        Me.grpLayer.Size = New System.Drawing.Size(286, 144)
        Me.grpLayer.TabIndex = 37
        Me.grpLayer.TabStop = False
        Me.grpLayer.Text = "grpLayer"
        '
        'optExistingLayer
        '
        Me.optExistingLayer.AutoSize = True
        Me.optExistingLayer.Location = New System.Drawing.Point(7, 91)
        Me.optExistingLayer.Name = "optExistingLayer"
        Me.optExistingLayer.Size = New System.Drawing.Size(102, 17)
        Me.optExistingLayer.TabIndex = 3
        Me.optExistingLayer.TabStop = True
        Me.optExistingLayer.Text = "optExistingLayer"
        Me.optExistingLayer.UseVisualStyleBackColor = True
        '
        'optNewHDLayer
        '
        Me.optNewHDLayer.AutoSize = True
        Me.optNewHDLayer.Location = New System.Drawing.Point(7, 19)
        Me.optNewHDLayer.Name = "optNewHDLayer"
        Me.optNewHDLayer.Size = New System.Drawing.Size(104, 17)
        Me.optNewHDLayer.TabIndex = 0
        Me.optNewHDLayer.TabStop = True
        Me.optNewHDLayer.Text = "optNewHDLayer"
        Me.optNewHDLayer.UseVisualStyleBackColor = True
        '
        'optNewDBLayer
        '
        Me.optNewDBLayer.AutoSize = True
        Me.optNewDBLayer.Location = New System.Drawing.Point(7, 42)
        Me.optNewDBLayer.Name = "optNewDBLayer"
        Me.optNewDBLayer.Size = New System.Drawing.Size(103, 17)
        Me.optNewDBLayer.TabIndex = 1
        Me.optNewDBLayer.TabStop = True
        Me.optNewDBLayer.Text = "optNewDBLayer"
        Me.optNewDBLayer.UseVisualStyleBackColor = True
        '
        'grpAttributes
        '
        Me.grpAttributes.Controls.Add(Me.uxFieldPanel)
        Me.grpAttributes.Location = New System.Drawing.Point(6, 6)
        Me.grpAttributes.Name = "grpAttributes"
        Me.grpAttributes.Size = New System.Drawing.Size(285, 391)
        Me.grpAttributes.TabIndex = 38
        Me.grpAttributes.TabStop = False
        Me.grpAttributes.Text = "grpAttributes"
        '
        'uxFieldPanel
        '
        Me.uxFieldPanel.AutoScroll = True
        Me.uxFieldPanel.Location = New System.Drawing.Point(6, 19)
        Me.uxFieldPanel.Name = "uxFieldPanel"
        Me.uxFieldPanel.Size = New System.Drawing.Size(273, 366)
        Me.uxFieldPanel.TabIndex = 0
        '
        'uxTabs
        '
        Me.uxTabs.Controls.Add(Me.TabPage1)
        Me.uxTabs.Controls.Add(Me.TabPage2)
        Me.uxTabs.Controls.Add(Me.TabPage3)
        Me.uxTabs.Controls.Add(Me.TabPage4)
        Me.uxTabs.Controls.Add(Me.TabPage5)
        Me.uxTabs.Location = New System.Drawing.Point(7, 7)
        Me.uxTabs.Name = "uxTabs"
        Me.uxTabs.SelectedIndex = 0
        Me.uxTabs.Size = New System.Drawing.Size(549, 410)
        Me.uxTabs.TabIndex = 39
        Me.uxTabs.Visible = False
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.grpLayer)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(541, 384)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "TabPage1"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.grpShapes)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(541, 384)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "TabPage2"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.grpSections)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(541, 384)
        Me.TabPage3.TabIndex = 3
        Me.TabPage3.Text = "TabPage3"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'grpSections
        '
        Me.grpSections.Controls.Add(Me.txtFilterSections)
        Me.grpSections.Controls.Add(Me.uxSectionList)
        Me.grpSections.Controls.Add(Me.btnCheckAllSections)
        Me.grpSections.Controls.Add(Me.lblFilterSections)
        Me.grpSections.Controls.Add(Me.btnUncheckAllSections)
        Me.grpSections.Location = New System.Drawing.Point(6, 6)
        Me.grpSections.Name = "grpSections"
        Me.grpSections.Size = New System.Drawing.Size(532, 372)
        Me.grpSections.TabIndex = 37
        Me.grpSections.TabStop = False
        Me.grpSections.Text = "grpSections"
        '
        'txtFilterSections
        '
        Me.txtFilterSections.Location = New System.Drawing.Point(84, 343)
        Me.txtFilterSections.Name = "txtFilterSections"
        Me.txtFilterSections.Size = New System.Drawing.Size(147, 20)
        Me.txtFilterSections.TabIndex = 5
        '
        'uxSectionList
        '
        Me.uxSectionList.Location = New System.Drawing.Point(6, 19)
        Me.uxSectionList.Name = "uxSectionList"
        Me.uxSectionList.OcxState = CType(resources.GetObject("uxSectionList.OcxState"), System.Windows.Forms.AxHost.State)
        Me.uxSectionList.Size = New System.Drawing.Size(520, 318)
        Me.uxSectionList.TabIndex = 4
        '
        'btnCheckAllSections
        '
        Me.btnCheckAllSections.Location = New System.Drawing.Point(341, 343)
        Me.btnCheckAllSections.Name = "btnCheckAllSections"
        Me.btnCheckAllSections.Size = New System.Drawing.Size(88, 23)
        Me.btnCheckAllSections.TabIndex = 7
        Me.btnCheckAllSections.Text = "btnCheckAllSections"
        Me.btnCheckAllSections.UseVisualStyleBackColor = True
        '
        'lblFilterSections
        '
        Me.lblFilterSections.AutoSize = True
        Me.lblFilterSections.Location = New System.Drawing.Point(11, 349)
        Me.lblFilterSections.Name = "lblFilterSections"
        Me.lblFilterSections.Size = New System.Drawing.Size(80, 13)
        Me.lblFilterSections.TabIndex = 12
        Me.lblFilterSections.Text = "lblFilterSections"
        '
        'btnUncheckAllSections
        '
        Me.btnUncheckAllSections.Location = New System.Drawing.Point(435, 343)
        Me.btnUncheckAllSections.Name = "btnUncheckAllSections"
        Me.btnUncheckAllSections.Size = New System.Drawing.Size(88, 23)
        Me.btnUncheckAllSections.TabIndex = 8
        Me.btnUncheckAllSections.Text = "btnUncheckAllSections"
        Me.btnUncheckAllSections.UseVisualStyleBackColor = True
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.grpAttributes)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(541, 384)
        Me.TabPage4.TabIndex = 0
        Me.TabPage4.Text = "TabPage4"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.grpProperties)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(541, 384)
        Me.TabPage5.TabIndex = 2
        Me.TabPage5.Text = "TabPage5"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'uxPrevious
        '
        Me.uxPrevious.Location = New System.Drawing.Point(396, 461)
        Me.uxPrevious.Name = "uxPrevious"
        Me.uxPrevious.Size = New System.Drawing.Size(75, 23)
        Me.uxPrevious.TabIndex = 101
        Me.uxPrevious.Text = "uxPrevious"
        Me.uxPrevious.UseVisualStyleBackColor = True
        '
        'uxPanel
        '
        Me.uxPanel.Location = New System.Drawing.Point(7, 7)
        Me.uxPanel.Name = "uxPanel"
        Me.uxPanel.Size = New System.Drawing.Size(549, 398)
        Me.uxPanel.TabIndex = 103
        '
        'lblDescription
        '
        Me.lblDescription.Location = New System.Drawing.Point(7, 408)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(296, 76)
        Me.lblDescription.TabIndex = 105
        Me.lblDescription.Text = "lblDescription"
        '
        'uxCreateSections
        '
        Me.uxCreateSections.AutoSize = True
        Me.uxCreateSections.Location = New System.Drawing.Point(358, 408)
        Me.uxCreateSections.Name = "uxCreateSections"
        Me.uxCreateSections.Size = New System.Drawing.Size(109, 17)
        Me.uxCreateSections.TabIndex = 106
        Me.uxCreateSections.Text = "uxCreateSections"
        Me.uxCreateSections.UseVisualStyleBackColor = True
        '
        'ImportDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.uxCancel
        Me.ClientSize = New System.Drawing.Size(564, 493)
        Me.Controls.Add(Me.uxTabs)
        Me.Controls.Add(Me.uxCreateSections)
        Me.Controls.Add(Me.uxPanel)
        Me.Controls.Add(Me.uxNext)
        Me.Controls.Add(Me.uxPrevious)
        Me.Controls.Add(Me.uxCancel)
        Me.Controls.Add(Me.lblDescription)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ImportDialog"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "ImportDialog"
        CType(Me.uxShapeList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpProperties.ResumeLayout(False)
        Me.grpProperties.PerformLayout()
        Me.grpShapes.ResumeLayout(False)
        Me.grpShapes.PerformLayout()
        Me.grpLayer.ResumeLayout(False)
        Me.grpLayer.PerformLayout()
        Me.grpAttributes.ResumeLayout(False)
        Me.uxTabs.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.grpSections.ResumeLayout(False)
        Me.grpSections.PerformLayout()
        CType(Me.uxSectionList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage5.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmbLayers As System.Windows.Forms.ComboBox
    Friend WithEvents txtTitle As System.Windows.Forms.TextBox
    Friend WithEvents uxNext As System.Windows.Forms.Button
    Friend WithEvents uxCancel As System.Windows.Forms.Button
    Friend WithEvents txtFilterShapes As System.Windows.Forms.TextBox
    Friend WithEvents btnCheckAllShapes As System.Windows.Forms.Button
    Friend WithEvents btnUncheckAllShapes As System.Windows.Forms.Button
    Friend WithEvents lblFilterShapes As System.Windows.Forms.Label
    Friend WithEvents uxShapeList As AxXtremeReportControl.AxReportControl
    Friend WithEvents FieldMenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cmbCounty As System.Windows.Forms.ComboBox
    Friend WithEvents lblCounty As System.Windows.Forms.Label
    Friend WithEvents cmbMunicipal As System.Windows.Forms.ComboBox
    Friend WithEvents lblMunicipal As System.Windows.Forms.Label
    Friend WithEvents cmbParish As System.Windows.Forms.ComboBox
    Friend WithEvents lblParish As System.Windows.Forms.Label
    Friend WithEvents cmbPropNumber As System.Windows.Forms.ComboBox
    Friend WithEvents lblPropNumber As System.Windows.Forms.Label
    Friend WithEvents cmbPropName As System.Windows.Forms.ComboBox
    Friend WithEvents lblPropName As System.Windows.Forms.Label
    Friend WithEvents cmbBlock As System.Windows.Forms.ComboBox
    Friend WithEvents lblBlock As System.Windows.Forms.Label
    Friend WithEvents cmbUnit As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnit As System.Windows.Forms.Label
    Friend WithEvents cmbArea As System.Windows.Forms.ComboBox
    Friend WithEvents lblArea As System.Windows.Forms.Label
    Friend WithEvents cmbMeasuredArea As System.Windows.Forms.ComboBox
    Friend WithEvents lblMeasuredArea As System.Windows.Forms.Label
    Friend WithEvents cmbObjID As System.Windows.Forms.ComboBox
    Friend WithEvents lblObjID As System.Windows.Forms.Label
    Friend WithEvents grpProperties As System.Windows.Forms.GroupBox
    Friend WithEvents chkAddProperties As System.Windows.Forms.CheckBox
    Friend WithEvents grpShapes As System.Windows.Forms.GroupBox
    Friend WithEvents grpLayer As System.Windows.Forms.GroupBox
    Friend WithEvents optExistingLayer As System.Windows.Forms.RadioButton
    Friend WithEvents optNewHDLayer As System.Windows.Forms.RadioButton
    Friend WithEvents optNewDBLayer As System.Windows.Forms.RadioButton
    Friend WithEvents grpAttributes As System.Windows.Forms.GroupBox
    Friend WithEvents uxFieldPanel As System.Windows.Forms.Panel
    Friend WithEvents uxTabs As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents uxPrevious As System.Windows.Forms.Button
    Friend WithEvents uxPanel As System.Windows.Forms.Panel
    Friend WithEvents btnFilterShapes As System.Windows.Forms.Button
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents grpSections As System.Windows.Forms.GroupBox
    Friend WithEvents uxSectionList As AxXtremeReportControl.AxReportControl
    Friend WithEvents btnCheckAllSections As System.Windows.Forms.Button
    Friend WithEvents lblFilterSections As System.Windows.Forms.Label
    Friend WithEvents txtFilterSections As System.Windows.Forms.TextBox
    Friend WithEvents btnUncheckAllSections As System.Windows.Forms.Button
    Friend WithEvents uxCreateSections As System.Windows.Forms.CheckBox
End Class
