﻿Imports System.Data.OleDb
Imports TatukGIS.NDK

Public Class LayerChoiceDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 800
    Private Const _strIdOK As Integer = 801
    Private Const _strIdCancel As Integer = 802
    Private Const _strIdNewLayer As Integer = 803
#End Region

#Region "Declarations"
    Private _rtOne As RelationType
    Private _rtTwo As RelationType
    Private _gisCtrl As TatukGIS.NDK.WinForms.TGIS_ViewerWnd
    Private _defaultLayerName As String
    Private _newLayerOption As Boolean
    Private _shapeTypes As TGIS_ShapeType

    Public ReadOnly Property LayerOne() As String
        Get
            If uxLayerOne.SelectedItem IsNot Nothing Then
                Return CType(uxLayerOne.SelectedItem, NameCaptionPair).Name
            Else
                Return vbNullString
            End If
        End Get
    End Property

    Public ReadOnly Property LayerTwo() As String
        Get
            If uxLayerTwo.SelectedItem IsNot Nothing Then
                Return CType(uxLayerTwo.SelectedItem, NameCaptionPair).Name
            Else
                Return vbNullString
            End If
        End Get
    End Property

    Public Sub New(ByVal rtOne As RelationType, ByVal textOne As String, ByVal enabledOne As Boolean, ByVal shapeTypes As TGIS_ShapeType, Optional ByVal newLayerOption As Boolean = True, _
         Optional ByVal rtTwo As RelationType = RelationType.Undefined, Optional ByVal textTwo As String = vbNullString, Optional ByVal enabledTwo As Boolean = False, Optional ByVal defaultLayer As String = vbNullString)
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _gisCtrl = GisControlView.Instance.GisCtrl 'Shorthand
        _rtOne = rtOne
        _rtTwo = rtTwo
        _defaultLayerName = defaultLayer
        _newLayerOption = newLayerOption
        _shapeTypes = shapeTypes
        uxLayerOneLabel.Text = textOne
        uxLayerTwoLabel.Text = textTwo
        uxLayerOne.Enabled = enabledOne
        uxLayerTwo.Enabled = enabledTwo
    End Sub
#End Region

#Region "Helper functions"
    Private Sub ListLayers(ByVal rt As RelationType, ByVal cmb As ComboBox)
        Dim i As Integer

        'Add 'new layer' as an option
        If _newLayerOption Then cmb.Items.Add(New NameCaptionPair(GisControlView.NewLayerName, GisControlView.LangStr(_strIdNewLayer)))

        If rt = RelationType.Undefined Then
            'List all visible vector layers (in reverse order to match legend)
            Dim ll As TGIS_LayerAbstract
            For i = _gisCtrl.Items.Count - 1 To 0 Step -1
                ll = _gisCtrl.Items(i)
                If ll.Active AndAlso TypeOf ll Is TGIS_LayerVector AndAlso ll.Name <> GisControlView.PreviewLayerName Then
                    'Make sure shape types match (if specified)
                    If _shapeTypes And CType(ll, TGIS_LayerVector).SupportedShapes OrElse _shapeTypes = TGIS_ShapeType.gisShapeTypeNull Then
                        cmb.Items.Add(New NameCaptionPair(ll.Name, ll.Caption))
                    End If
                End If
            Next
        Else
            'Query db for specific layer type
            Dim ll As TGIS_LayerVector
            Dim cmd As New OleDbCommand("SELECT name FROM gis_layers WHERE type = " & rt, GisControlView.Conn)
            Dim reader As OleDbDataReader = cmd.ExecuteReader()
            Do While reader.Read()
                ll = _gisCtrl.Get(reader.GetString(0))  'We need the layer instance to get the title
                If ll IsNot Nothing AndAlso ll.Active Then
                    'Make sure shape types match (if specified)
                    If _shapeTypes And ll.SupportedShapes OrElse _shapeTypes = TGIS_ShapeType.gisShapeTypeNull Then
                        cmb.Items.Add(New NameCaptionPair(ll.Name, ll.Caption)) 'Only visible layers
                    End If
                End If
            Loop
            reader.Close()
        End If

        'Pick default layer
        If _defaultLayerName <> vbNullString Then
            For i = 0 To cmb.Items.Count - 1
                If _defaultLayerName = CType(cmb.Items(i), NameCaptionPair).Name Then
                    cmb.SelectedIndex = i
                    Exit For
                End If
            Next
        End If

        'If default layer couldn't be found or was not specified pick new layer
        If cmb.SelectedIndex < 0 Then cmb.SelectedIndex = 0 'New layer
    End Sub
#End Region

#Region "Event handlers"
    Private Sub LayerChoiceDialog_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Set up language
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        uxOK.Text = GisControlView.LangStr(_strIdOK)
        uxCancel.Text = GisControlView.LangStr(_strIdCancel)

        'List appropriate layers
        If uxLayerOne.Enabled Then ListLayers(_rtOne, uxLayerOne)
        If uxLayerTwo.Enabled Then ListLayers(_rtTwo, uxLayerTwo)
    End Sub
#End Region
End Class
