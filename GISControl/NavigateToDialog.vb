﻿Imports System.Globalization
Imports System.Windows.Forms
Imports TatukGIS.NDK

Public Class NavigateToDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 2800
    Private Const _strIdOK As Integer = 2801
    Private Const _strIdCancel As Integer = 2802
    Private Const _strIdDescription As Integer = 2803
#End Region

#Region "Declarations"
    Private _x As Double, _y As Double
    Private _wgs As Boolean

    Public ReadOnly Property X() As Double
        Get
            Return _x
        End Get
    End Property

    Public ReadOnly Property Y() As Double
        Get
            Return _y
        End Get
    End Property

    Public ReadOnly Property IsWGS() As Boolean
        Get
            Return _wgs
        End Get
    End Property
#End Region

#Region "Event handlers"
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxOK.Click
        Try
            'See if values are plain coordinates
            _x = Convert.ToDouble(uxCoordinate1.Text.Replace(","c, "."c), CultureInfo.InvariantCulture)
            _y = Convert.ToDouble(uxCoordinate2.Text.Replace(","c, "."c), CultureInfo.InvariantCulture)
        Catch ex As FormatException
            'Otherwise try to parse lat/long
            Try
                _y = TGIS_Utils.GisStrToLatitude(uxCoordinate1.Text)
                _x = TGIS_Utils.GisStrToLongitude(uxCoordinate2.Text)
                _wgs = True
            Catch ex1 As EGIS_Exception
                MsgBox(ex1.Message, MsgBoxStyle.Exclamation)
                Exit Sub
            End Try
        End Try

        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxCancel.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub NavigateToDialog_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        uxOK.Text = GisControlView.LangStr(_strIdOK)
        uxCancel.Text = GisControlView.LangStr(_strIdCancel)
        uxDescription.Text = GisControlView.LangStr(_strIdDescription)
    End Sub
#End Region
End Class
