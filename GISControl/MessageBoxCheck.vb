﻿Imports Microsoft.Win32
Imports System.Windows.Forms

Public Class MessageBoxCheck
#Region "Language"
    Private Const _strIdDontShowAgain As Integer = 2200
    Private Const _strIdYes As Integer = 2201
    Private Const _strIdNo As Integer = 2202
#End Region

#Region "Event handlers"
    Public Overloads Function ShowDialog(ByVal text As String, ByVal caption As String, ByVal icon As MessageBoxIcon, ByVal regVal As String) As DialogResult
        Dim ans As DialogResult
        Dim regKey As Microsoft.Win32.RegistryKey

        'Check registry to see if we should remember an earlier choice
        regKey = Registry.CurrentUser.OpenSubKey(GisControlView.RegKeyGis, False)
        If regKey IsNot Nothing Then
            ans = regKey.GetValue(regVal, DialogResult.None)
            regKey.Close()
        End If
        If ans <> DialogResult.None Then Return ans

        'Set up language
        If caption <> vbNullString Then Me.Text = caption Else Me.Text = System.Reflection.Assembly.GetExecutingAssembly.GetName.Name()
        uxMessage.Text = text
        uxCheck.Text = GisControlView.LangStr(_strIdDontShowAgain)
        uxYes.Text = GisControlView.LangStr(_strIdYes)
        uxNo.Text = GisControlView.LangStr(_strIdNo)

        'Icon
        Select Case icon
            Case MessageBoxIcon.Asterisk
            Case MessageBoxIcon.Error, MessageBoxIcon.Stop
                uxIcon.Image = System.Drawing.SystemIcons.Error.ToBitmap()
            Case MessageBoxIcon.Exclamation
                uxIcon.Image = System.Drawing.SystemIcons.Exclamation.ToBitmap()
            Case MessageBoxIcon.Hand
                uxIcon.Image = System.Drawing.SystemIcons.Hand.ToBitmap()
            Case MessageBoxIcon.Information
                uxIcon.Image = System.Drawing.SystemIcons.Information.ToBitmap()
            Case MessageBoxIcon.Question
                uxIcon.Image = System.Drawing.SystemIcons.Question.ToBitmap()
            Case MessageBoxIcon.Warning
                uxIcon.Image = System.Drawing.SystemIcons.Warning.ToBitmap()
        End Select

        'Show dialog, remember choice if checkbox was checked
        ans = Me.ShowDialog()
        If uxCheck.Checked Then
            regKey = Registry.CurrentUser.OpenSubKey(GisControlView.RegKeyGis, True)
            If regKey IsNot Nothing Then
                regKey.SetValue(regVal, DirectCast(ans, Integer))
                regKey.Close()
            End If
        End If

        Return ans
    End Function

    Private Sub uxYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxYes.Click
        Me.DialogResult = DialogResult.Yes
    End Sub

    Private Sub uxNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxNo.Click
        Me.DialogResult = DialogResult.No
    End Sub
#End Region
End Class
