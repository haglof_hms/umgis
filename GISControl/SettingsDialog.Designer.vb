﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SettingsDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.uxOK = New System.Windows.Forms.Button
        Me.uxCancel = New System.Windows.Forms.Button
        Me.uxScaleUnits = New System.Windows.Forms.GroupBox
        Me.uxArea = New System.Windows.Forms.ComboBox
        Me.uxAreaLabel = New System.Windows.Forms.Label
        Me.uxLength = New System.Windows.Forms.ComboBox
        Me.uxLengthLabel = New System.Windows.Forms.Label
        Me.uxPaths = New System.Windows.Forms.GroupBox
        Me.uxProjectDir = New System.Windows.Forms.TextBox
        Me.uxBrowse = New System.Windows.Forms.Button
        Me.uxProjectDirLabel = New System.Windows.Forms.Label
        Me.uxEditMode = New System.Windows.Forms.GroupBox
        Me.uxAfterActivePoint = New System.Windows.Forms.RadioButton
        Me.uxNearestPoint = New System.Windows.Forms.RadioButton
        Me.uxSnapType = New System.Windows.Forms.GroupBox
        Me.uxSnapToLine = New System.Windows.Forms.RadioButton
        Me.uxSnapToPoint = New System.Windows.Forms.RadioButton
        Me.uxRouteMode = New System.Windows.Forms.GroupBox
        Me.uxPreferColumns = New System.Windows.Forms.RadioButton
        Me.uxPreferRows = New System.Windows.Forms.RadioButton
        Me.uxProjectionDesc = New System.Windows.Forms.TextBox
        Me.uxProjectionLabel = New System.Windows.Forms.Label
        Me.uxChooseProjection = New System.Windows.Forms.Button
        Me.uxProjection = New System.Windows.Forms.GroupBox
        Me.LineGeneral = New System.Windows.Forms.Label
        Me.uxNorthArrow = New System.Windows.Forms.CheckBox
        Me.uxOther = New System.Windows.Forms.GroupBox
        Me.uxMapRotation = New System.Windows.Forms.NumericUpDown
        Me.uxMapRotationLabel = New System.Windows.Forms.Label
        Me.uxScaleUnits.SuspendLayout()
        Me.uxPaths.SuspendLayout()
        Me.uxEditMode.SuspendLayout()
        Me.uxSnapType.SuspendLayout()
        Me.uxRouteMode.SuspendLayout()
        Me.uxProjection.SuspendLayout()
        Me.uxOther.SuspendLayout()
        CType(Me.uxMapRotation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'uxOK
        '
        Me.uxOK.Location = New System.Drawing.Point(147, 445)
        Me.uxOK.Name = "uxOK"
        Me.uxOK.Size = New System.Drawing.Size(67, 23)
        Me.uxOK.TabIndex = 7
        Me.uxOK.Text = "uxOK"
        '
        'uxCancel
        '
        Me.uxCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.uxCancel.Location = New System.Drawing.Point(220, 445)
        Me.uxCancel.Name = "uxCancel"
        Me.uxCancel.Size = New System.Drawing.Size(67, 23)
        Me.uxCancel.TabIndex = 8
        Me.uxCancel.Text = "uxCancel"
        '
        'uxScaleUnits
        '
        Me.uxScaleUnits.Controls.Add(Me.uxArea)
        Me.uxScaleUnits.Controls.Add(Me.uxAreaLabel)
        Me.uxScaleUnits.Controls.Add(Me.uxLength)
        Me.uxScaleUnits.Controls.Add(Me.uxLengthLabel)
        Me.uxScaleUnits.Location = New System.Drawing.Point(13, 13)
        Me.uxScaleUnits.Name = "uxScaleUnits"
        Me.uxScaleUnits.Size = New System.Drawing.Size(406, 119)
        Me.uxScaleUnits.TabIndex = 1
        Me.uxScaleUnits.TabStop = False
        Me.uxScaleUnits.Text = "uxScaleUnits"
        '
        'uxArea
        '
        Me.uxArea.DropDownHeight = 300
        Me.uxArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.uxArea.FormattingEnabled = True
        Me.uxArea.IntegralHeight = False
        Me.uxArea.Location = New System.Drawing.Point(9, 84)
        Me.uxArea.Name = "uxArea"
        Me.uxArea.Size = New System.Drawing.Size(268, 21)
        Me.uxArea.TabIndex = 2
        '
        'uxAreaLabel
        '
        Me.uxAreaLabel.AutoSize = True
        Me.uxAreaLabel.Location = New System.Drawing.Point(9, 68)
        Me.uxAreaLabel.Name = "uxAreaLabel"
        Me.uxAreaLabel.Size = New System.Drawing.Size(66, 13)
        Me.uxAreaLabel.TabIndex = 9
        Me.uxAreaLabel.Text = "uxAreaLabel"
        '
        'uxLength
        '
        Me.uxLength.DropDownHeight = 300
        Me.uxLength.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.uxLength.FormattingEnabled = True
        Me.uxLength.IntegralHeight = False
        Me.uxLength.Location = New System.Drawing.Point(9, 36)
        Me.uxLength.Name = "uxLength"
        Me.uxLength.Size = New System.Drawing.Size(268, 21)
        Me.uxLength.TabIndex = 1
        '
        'uxLengthLabel
        '
        Me.uxLengthLabel.AutoSize = True
        Me.uxLengthLabel.Location = New System.Drawing.Point(9, 20)
        Me.uxLengthLabel.Name = "uxLengthLabel"
        Me.uxLengthLabel.Size = New System.Drawing.Size(77, 13)
        Me.uxLengthLabel.TabIndex = 7
        Me.uxLengthLabel.Text = "uxLengthLabel"
        '
        'uxPaths
        '
        Me.uxPaths.Controls.Add(Me.uxProjectDir)
        Me.uxPaths.Controls.Add(Me.uxBrowse)
        Me.uxPaths.Controls.Add(Me.uxProjectDirLabel)
        Me.uxPaths.Location = New System.Drawing.Point(12, 217)
        Me.uxPaths.Name = "uxPaths"
        Me.uxPaths.Size = New System.Drawing.Size(406, 73)
        Me.uxPaths.TabIndex = 3
        Me.uxPaths.TabStop = False
        Me.uxPaths.Text = "uxPaths"
        '
        'uxProjectDir
        '
        Me.uxProjectDir.Location = New System.Drawing.Point(10, 40)
        Me.uxProjectDir.Name = "uxProjectDir"
        Me.uxProjectDir.Size = New System.Drawing.Size(289, 20)
        Me.uxProjectDir.TabIndex = 1
        '
        'uxBrowse
        '
        Me.uxBrowse.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uxBrowse.Location = New System.Drawing.Point(302, 40)
        Me.uxBrowse.Name = "uxBrowse"
        Me.uxBrowse.Size = New System.Drawing.Size(24, 20)
        Me.uxBrowse.TabIndex = 2
        Me.uxBrowse.Text = "..."
        Me.uxBrowse.UseVisualStyleBackColor = True
        '
        'uxProjectDirLabel
        '
        Me.uxProjectDirLabel.AutoSize = True
        Me.uxProjectDirLabel.Location = New System.Drawing.Point(10, 24)
        Me.uxProjectDirLabel.Name = "uxProjectDirLabel"
        Me.uxProjectDirLabel.Size = New System.Drawing.Size(90, 13)
        Me.uxProjectDirLabel.TabIndex = 14
        Me.uxProjectDirLabel.Text = "uxProjectDirLabel"
        '
        'uxEditMode
        '
        Me.uxEditMode.Controls.Add(Me.uxAfterActivePoint)
        Me.uxEditMode.Controls.Add(Me.uxNearestPoint)
        Me.uxEditMode.Location = New System.Drawing.Point(13, 296)
        Me.uxEditMode.Name = "uxEditMode"
        Me.uxEditMode.Size = New System.Drawing.Size(133, 67)
        Me.uxEditMode.TabIndex = 4
        Me.uxEditMode.TabStop = False
        Me.uxEditMode.Text = "uxEditMode"
        '
        'uxAfterActivePoint
        '
        Me.uxAfterActivePoint.AutoSize = True
        Me.uxAfterActivePoint.Location = New System.Drawing.Point(6, 42)
        Me.uxAfterActivePoint.Name = "uxAfterActivePoint"
        Me.uxAfterActivePoint.Size = New System.Drawing.Size(112, 17)
        Me.uxAfterActivePoint.TabIndex = 2
        Me.uxAfterActivePoint.TabStop = True
        Me.uxAfterActivePoint.Text = "uxAfterActivePoint"
        Me.uxAfterActivePoint.UseVisualStyleBackColor = True
        '
        'uxNearestPoint
        '
        Me.uxNearestPoint.AutoSize = True
        Me.uxNearestPoint.Location = New System.Drawing.Point(6, 19)
        Me.uxNearestPoint.Name = "uxNearestPoint"
        Me.uxNearestPoint.Size = New System.Drawing.Size(97, 17)
        Me.uxNearestPoint.TabIndex = 1
        Me.uxNearestPoint.TabStop = True
        Me.uxNearestPoint.Text = "uxNearestPoint"
        Me.uxNearestPoint.UseVisualStyleBackColor = True
        '
        'uxSnapType
        '
        Me.uxSnapType.Controls.Add(Me.uxSnapToLine)
        Me.uxSnapType.Controls.Add(Me.uxSnapToPoint)
        Me.uxSnapType.Location = New System.Drawing.Point(152, 296)
        Me.uxSnapType.Name = "uxSnapType"
        Me.uxSnapType.Size = New System.Drawing.Size(134, 67)
        Me.uxSnapType.TabIndex = 5
        Me.uxSnapType.TabStop = False
        Me.uxSnapType.Text = "uxSnapType"
        '
        'uxSnapToLine
        '
        Me.uxSnapToLine.AutoSize = True
        Me.uxSnapToLine.Location = New System.Drawing.Point(6, 42)
        Me.uxSnapToLine.Name = "uxSnapToLine"
        Me.uxSnapToLine.Size = New System.Drawing.Size(94, 17)
        Me.uxSnapToLine.TabIndex = 2
        Me.uxSnapToLine.TabStop = True
        Me.uxSnapToLine.Text = "uxSnapToLine"
        Me.uxSnapToLine.UseVisualStyleBackColor = True
        '
        'uxSnapToPoint
        '
        Me.uxSnapToPoint.AutoSize = True
        Me.uxSnapToPoint.Location = New System.Drawing.Point(6, 19)
        Me.uxSnapToPoint.Name = "uxSnapToPoint"
        Me.uxSnapToPoint.Size = New System.Drawing.Size(98, 17)
        Me.uxSnapToPoint.TabIndex = 1
        Me.uxSnapToPoint.TabStop = True
        Me.uxSnapToPoint.Text = "uxSnapToPoint"
        Me.uxSnapToPoint.UseVisualStyleBackColor = True
        '
        'uxRouteMode
        '
        Me.uxRouteMode.Controls.Add(Me.uxPreferColumns)
        Me.uxRouteMode.Controls.Add(Me.uxPreferRows)
        Me.uxRouteMode.Location = New System.Drawing.Point(292, 296)
        Me.uxRouteMode.Name = "uxRouteMode"
        Me.uxRouteMode.Size = New System.Drawing.Size(127, 67)
        Me.uxRouteMode.TabIndex = 6
        Me.uxRouteMode.TabStop = False
        Me.uxRouteMode.Text = "uxRouteMode"
        '
        'uxPreferColumns
        '
        Me.uxPreferColumns.AutoSize = True
        Me.uxPreferColumns.Location = New System.Drawing.Point(6, 42)
        Me.uxPreferColumns.Name = "uxPreferColumns"
        Me.uxPreferColumns.Size = New System.Drawing.Size(104, 17)
        Me.uxPreferColumns.TabIndex = 2
        Me.uxPreferColumns.TabStop = True
        Me.uxPreferColumns.Text = "uxPreferColumns"
        Me.uxPreferColumns.UseVisualStyleBackColor = True
        '
        'uxPreferRows
        '
        Me.uxPreferRows.AutoSize = True
        Me.uxPreferRows.Location = New System.Drawing.Point(6, 19)
        Me.uxPreferRows.Name = "uxPreferRows"
        Me.uxPreferRows.Size = New System.Drawing.Size(91, 17)
        Me.uxPreferRows.TabIndex = 1
        Me.uxPreferRows.TabStop = True
        Me.uxPreferRows.Text = "uxPreferRows"
        Me.uxPreferRows.UseVisualStyleBackColor = True
        '
        'uxProjectionDesc
        '
        Me.uxProjectionDesc.Enabled = False
        Me.uxProjectionDesc.Location = New System.Drawing.Point(9, 39)
        Me.uxProjectionDesc.Name = "uxProjectionDesc"
        Me.uxProjectionDesc.Size = New System.Drawing.Size(192, 20)
        Me.uxProjectionDesc.TabIndex = 1
        '
        'uxProjectionLabel
        '
        Me.uxProjectionLabel.AutoSize = True
        Me.uxProjectionLabel.Location = New System.Drawing.Point(9, 20)
        Me.uxProjectionLabel.Name = "uxProjectionLabel"
        Me.uxProjectionLabel.Size = New System.Drawing.Size(91, 13)
        Me.uxProjectionLabel.TabIndex = 6
        Me.uxProjectionLabel.Text = "uxProjectionLabel"
        '
        'uxChooseProjection
        '
        Me.uxChooseProjection.Location = New System.Drawing.Point(207, 36)
        Me.uxChooseProjection.Name = "uxChooseProjection"
        Me.uxChooseProjection.Size = New System.Drawing.Size(118, 23)
        Me.uxChooseProjection.TabIndex = 2
        Me.uxChooseProjection.Text = "uxChooseProjection"
        Me.uxChooseProjection.UseVisualStyleBackColor = True
        '
        'uxProjection
        '
        Me.uxProjection.Controls.Add(Me.uxProjectionLabel)
        Me.uxProjection.Controls.Add(Me.uxChooseProjection)
        Me.uxProjection.Controls.Add(Me.uxProjectionDesc)
        Me.uxProjection.Location = New System.Drawing.Point(13, 138)
        Me.uxProjection.Name = "uxProjection"
        Me.uxProjection.Size = New System.Drawing.Size(406, 73)
        Me.uxProjection.TabIndex = 2
        Me.uxProjection.TabStop = False
        Me.uxProjection.Text = "uxProjection"
        '
        'LineGeneral
        '
        Me.LineGeneral.BackColor = System.Drawing.Color.DarkGray
        Me.LineGeneral.Location = New System.Drawing.Point(14, 432)
        Me.LineGeneral.Name = "LineGeneral"
        Me.LineGeneral.Size = New System.Drawing.Size(405, 1)
        Me.LineGeneral.TabIndex = 34
        '
        'uxNorthArrow
        '
        Me.uxNorthArrow.AutoSize = True
        Me.uxNorthArrow.Location = New System.Drawing.Point(6, 19)
        Me.uxNorthArrow.Name = "uxNorthArrow"
        Me.uxNorthArrow.Size = New System.Drawing.Size(90, 17)
        Me.uxNorthArrow.TabIndex = 35
        Me.uxNorthArrow.Text = "uxNorthArrow"
        Me.uxNorthArrow.UseVisualStyleBackColor = True
        '
        'uxOther
        '
        Me.uxOther.Controls.Add(Me.uxMapRotationLabel)
        Me.uxOther.Controls.Add(Me.uxMapRotation)
        Me.uxOther.Controls.Add(Me.uxNorthArrow)
        Me.uxOther.Location = New System.Drawing.Point(13, 370)
        Me.uxOther.Name = "uxOther"
        Me.uxOther.Size = New System.Drawing.Size(405, 47)
        Me.uxOther.TabIndex = 36
        Me.uxOther.TabStop = False
        Me.uxOther.Text = "uxOther"
        '
        'uxMapRotation
        '
        Me.uxMapRotation.Location = New System.Drawing.Point(232, 17)
        Me.uxMapRotation.Maximum = New Decimal(New Integer() {360, 0, 0, 0})
        Me.uxMapRotation.Name = "uxMapRotation"
        Me.uxMapRotation.Size = New System.Drawing.Size(56, 20)
        Me.uxMapRotation.TabIndex = 36
        '
        'uxMapRotationLabel
        '
        Me.uxMapRotationLabel.AutoSize = True
        Me.uxMapRotationLabel.Location = New System.Drawing.Point(294, 20)
        Me.uxMapRotationLabel.Name = "uxMapRotationLabel"
        Me.uxMapRotationLabel.Size = New System.Drawing.Size(105, 13)
        Me.uxMapRotationLabel.TabIndex = 37
        Me.uxMapRotationLabel.Text = "uxMapRotationLabel"
        '
        'SettingsDialog
        '
        Me.AcceptButton = Me.uxOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.uxCancel
        Me.ClientSize = New System.Drawing.Size(432, 478)
        Me.Controls.Add(Me.uxOther)
        Me.Controls.Add(Me.LineGeneral)
        Me.Controls.Add(Me.uxProjection)
        Me.Controls.Add(Me.uxRouteMode)
        Me.Controls.Add(Me.uxSnapType)
        Me.Controls.Add(Me.uxEditMode)
        Me.Controls.Add(Me.uxPaths)
        Me.Controls.Add(Me.uxScaleUnits)
        Me.Controls.Add(Me.uxCancel)
        Me.Controls.Add(Me.uxOK)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SettingsDialog"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "SettingsDialog"
        Me.uxScaleUnits.ResumeLayout(False)
        Me.uxScaleUnits.PerformLayout()
        Me.uxPaths.ResumeLayout(False)
        Me.uxPaths.PerformLayout()
        Me.uxEditMode.ResumeLayout(False)
        Me.uxEditMode.PerformLayout()
        Me.uxSnapType.ResumeLayout(False)
        Me.uxSnapType.PerformLayout()
        Me.uxRouteMode.ResumeLayout(False)
        Me.uxRouteMode.PerformLayout()
        Me.uxProjection.ResumeLayout(False)
        Me.uxProjection.PerformLayout()
        Me.uxOther.ResumeLayout(False)
        Me.uxOther.PerformLayout()
        CType(Me.uxMapRotation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents uxOK As System.Windows.Forms.Button
    Friend WithEvents uxCancel As System.Windows.Forms.Button
    Friend WithEvents uxScaleUnits As System.Windows.Forms.GroupBox
    Friend WithEvents uxPaths As System.Windows.Forms.GroupBox
    Friend WithEvents uxEditMode As System.Windows.Forms.GroupBox
    Friend WithEvents uxSnapType As System.Windows.Forms.GroupBox
    Friend WithEvents uxRouteMode As System.Windows.Forms.GroupBox
    Friend WithEvents uxProjectionDesc As System.Windows.Forms.TextBox
    Friend WithEvents uxProjectionLabel As System.Windows.Forms.Label
    Friend WithEvents uxChooseProjection As System.Windows.Forms.Button
    Friend WithEvents uxSnapToLine As System.Windows.Forms.RadioButton
    Friend WithEvents uxSnapToPoint As System.Windows.Forms.RadioButton
    Friend WithEvents uxPreferColumns As System.Windows.Forms.RadioButton
    Friend WithEvents uxPreferRows As System.Windows.Forms.RadioButton
    Friend WithEvents uxAfterActivePoint As System.Windows.Forms.RadioButton
    Friend WithEvents uxNearestPoint As System.Windows.Forms.RadioButton
    Friend WithEvents uxArea As System.Windows.Forms.ComboBox
    Friend WithEvents uxAreaLabel As System.Windows.Forms.Label
    Friend WithEvents uxLength As System.Windows.Forms.ComboBox
    Friend WithEvents uxLengthLabel As System.Windows.Forms.Label
    Friend WithEvents uxProjection As System.Windows.Forms.GroupBox
    Friend WithEvents uxProjectDir As System.Windows.Forms.TextBox
    Friend WithEvents uxBrowse As System.Windows.Forms.Button
    Friend WithEvents uxProjectDirLabel As System.Windows.Forms.Label
    Friend WithEvents LineGeneral As System.Windows.Forms.Label
    Friend WithEvents uxNorthArrow As System.Windows.Forms.CheckBox
    Friend WithEvents uxOther As System.Windows.Forms.GroupBox
    Friend WithEvents uxMapRotationLabel As System.Windows.Forms.Label
    Friend WithEvents uxMapRotation As System.Windows.Forms.NumericUpDown

End Class
