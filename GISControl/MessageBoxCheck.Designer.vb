﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MessageBoxCheck
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.uxIcon = New System.Windows.Forms.PictureBox
        Me.uxMessage = New System.Windows.Forms.Label
        Me.uxCheck = New System.Windows.Forms.CheckBox
        Me.uxYes = New System.Windows.Forms.Button
        Me.uxNo = New System.Windows.Forms.Button
        CType(Me.uxIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'uxIcon
        '
        Me.uxIcon.Location = New System.Drawing.Point(13, 13)
        Me.uxIcon.Name = "uxIcon"
        Me.uxIcon.Size = New System.Drawing.Size(32, 32)
        Me.uxIcon.TabIndex = 2
        Me.uxIcon.TabStop = False
        '
        'uxMessage
        '
        Me.uxMessage.Location = New System.Drawing.Point(51, 13)
        Me.uxMessage.Name = "uxMessage"
        Me.uxMessage.Size = New System.Drawing.Size(316, 43)
        Me.uxMessage.TabIndex = 3
        Me.uxMessage.Text = "uxMessage"
        '
        'uxCheck
        '
        Me.uxCheck.AutoSize = True
        Me.uxCheck.Location = New System.Drawing.Point(13, 59)
        Me.uxCheck.Name = "uxCheck"
        Me.uxCheck.Size = New System.Drawing.Size(68, 17)
        Me.uxCheck.TabIndex = 4
        Me.uxCheck.Text = "uxCheck"
        Me.uxCheck.UseVisualStyleBackColor = True
        '
        'uxYes
        '
        Me.uxYes.Location = New System.Drawing.Point(111, 82)
        Me.uxYes.Name = "uxYes"
        Me.uxYes.Size = New System.Drawing.Size(75, 23)
        Me.uxYes.TabIndex = 5
        Me.uxYes.Text = "uxYes"
        Me.uxYes.UseVisualStyleBackColor = True
        '
        'uxNo
        '
        Me.uxNo.Location = New System.Drawing.Point(192, 82)
        Me.uxNo.Name = "uxNo"
        Me.uxNo.Size = New System.Drawing.Size(75, 23)
        Me.uxNo.TabIndex = 6
        Me.uxNo.Text = "uxNo"
        Me.uxNo.UseVisualStyleBackColor = True
        '
        'MessageBoxCheck
        '
        Me.AcceptButton = Me.uxYes
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(379, 114)
        Me.Controls.Add(Me.uxNo)
        Me.Controls.Add(Me.uxYes)
        Me.Controls.Add(Me.uxCheck)
        Me.Controls.Add(Me.uxMessage)
        Me.Controls.Add(Me.uxIcon)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "MessageBoxCheck"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "MessageBoxCheck"
        CType(Me.uxIcon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents uxIcon As System.Windows.Forms.PictureBox
    Friend WithEvents uxMessage As System.Windows.Forms.Label
    Friend WithEvents uxCheck As System.Windows.Forms.CheckBox
    Friend WithEvents uxYes As System.Windows.Forms.Button
    Friend WithEvents uxNo As System.Windows.Forms.Button

End Class
