﻿Imports System.Data.OleDb
Imports System.Windows.Forms
Imports TatukGIS.NDK
Imports XtremeReportControl

Public Class ImportPropertiesDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 2400
    Private Const _strIdOK As Integer = 2401
    Private Const _strIdCancel As Integer = 2402
    Private Const _strIdProperties As Integer = 2403
    Private Const _strIdHighlight As Integer = 2404
    Private Const _strIdPropertyCount As Integer = 2405
    Private Const _strIdPropertyLayer As Integer = 2406
    Private Const _strIdAttributes As Integer = 2407
    Private Const _strIdCounty As Integer = 2408
    Private Const _strIdMunicipal As Integer = 2409
    Private Const _strIdParish As Integer = 2410
    Private Const _strIdPropNumber As Integer = 2411
    Private Const _strIdPropName As Integer = 2412
    Private Const _strIdBlock As Integer = 2413
    Private Const _strIdUnit As Integer = 2414
    Private Const _strIdArea As Integer = 2415
    Private Const _strIdMeasuredArea As Integer = 2416
    Private Const _strIdObjID As Integer = 2417
    Private Const _strIdMissingPropertyKeyField As Integer = 2418
    Private Const _strIdDestinationLayer As Integer = 2419
#End Region

#Region "Declarations"
    Private _gisCtrl As TatukGIS.NDK.WinForms.TGIS_ViewerWnd
    Private _selectionLayerName As String
    Private _selectionShapeUid As Integer
    Private _previousLayerName As String

    Public Sub New(ByVal layerName As String, ByVal shapeUid As Integer)
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _gisCtrl = GisControlView.Instance.GisCtrl
        _selectionLayerName = layerName
        _selectionShapeUid = shapeUid
    End Sub
#End Region

#Region "Helper functions"
    Private Sub ListProperties()
        Dim ll As TGIS_LayerVector
        Dim shp As TGIS_Shape, selectionShp As TGIS_Shape
        Dim i As Integer, j As Integer
        Dim record As ReportRecord

        If cmbPropertyLayer.SelectedItem IsNot Nothing Then
            'List intersecting shapes
            Me.Cursor = Cursors.WaitCursor
            ll = _gisCtrl.Get(CType(cmbPropertyLayer.SelectedItem, NameCaptionPair).Name)

            uxShapeList.Records.DeleteAll()
            uxShapeList.Columns.DeleteAll()

            'Set up attribute columns
            For i = 0 To ll.Fields.Count - 1
                uxShapeList.Columns.Add(i, ll.FieldInfo(i).Name, 95, True)
            Next

            ll.DeselectAll()

            'Check each shape in layer
            _gisCtrl.Lock() 'Lock viewer so looping won't be interrupted by any paint event
            selectionShp = CType(_gisCtrl.Get(_selectionLayerName), TGIS_LayerVector).GetShape(_selectionShapeUid)
            For i = 1 To ll.GetLastUid()
                shp = ll.GetShape(i)
                If shp IsNot Nothing Then
                    If selectionShp.IsCommonPoint(shp) Then
                        'Add record for this shape
                        record = uxShapeList.Records.Add()
                        record.Tag = shp.Uid
                        For j = 0 To ll.Fields.Count - 1
                            If ll.FieldInfo(j).FieldType = TGIS_FieldType.gisFieldTypeDate Then
                                record.AddItem(Microsoft.VisualBasic.FormatDateTime(Convert.ToDateTime(shp.GetField(ll.FieldInfo(j).Name), System.Globalization.CultureInfo.CurrentCulture), DateFormat.GeneralDate))
                            Else
                                record.AddItem(CStr(shp.GetField(ll.FieldInfo(j).Name)))
                            End If
                        Next

                        If chkHighlight.Checked Then shp.MakeEditable().IsSelected = True
                    End If
                End If
            Next
            _gisCtrl.Unlock()
            ll.InvalidateScope(vbNullString, vbNullString)
            uxShapeList.Populate()
            Me.Cursor = Cursors.Default
        End If

        'Update count label
        lblCount.Text = uxShapeList.Records.Count & " " & GisControlView.LangStr(_strIdPropertyCount)
    End Sub
#End Region

#Region "Event handlers"
    Private Sub ImportPropertiesDialog_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim ll As TGIS_LayerVector

        'Deselect any shapes in current layer
        If cmbPropertyLayer.SelectedItem IsNot Nothing Then
            ll = _gisCtrl.Get(CType(cmbPropertyLayer.SelectedItem, NameCaptionPair).Name)
            ll.DeselectAll()
        End If
    End Sub

    Private Sub ImportPropertiesDialog_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim i As Integer
        Dim ll As TGIS_LayerVector

        'Set up language dependent strings
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        uxOK.Text = GisControlView.LangStr(_strIdOK)
        uxCancel.Text = GisControlView.LangStr(_strIdCancel)
        lblProperties.Text = GisControlView.LangStr(_strIdProperties)
        lblCount.Text = GisControlView.LangStr(_strIdPropertyCount)
        chkHighlight.Text = GisControlView.LangStr(_strIdHighlight)
        lblPropertyLayer.Text = GisControlView.LangStr(_strIdPropertyLayer)
        grpAttributes.Text = GisControlView.LangStr(_strIdAttributes)
        lblCounty.Text = GisControlView.LangStr(_strIdCounty)
        lblMunicipal.Text = GisControlView.LangStr(_strIdMunicipal)
        lblParish.Text = GisControlView.LangStr(_strIdParish)
        lblPropNumber.Text = GisControlView.LangStr(_strIdPropNumber)
        lblPropName.Text = GisControlView.LangStr(_strIdPropName)
        lblBlock.Text = GisControlView.LangStr(_strIdBlock)
        lblUnit.Text = GisControlView.LangStr(_strIdUnit)
        lblArea.Text = GisControlView.LangStr(_strIdArea)
        lblMeasuredArea.Text = GisControlView.LangStr(_strIdMeasuredArea)
        lblObjID.Text = GisControlView.LangStr(_strIdObjID)

        'List all vector layers
        For i = 0 To _gisCtrl.Items.Count - 1
            ll = TryCast(_gisCtrl.Items(i), TGIS_LayerVector)
            If ll IsNot Nothing Then cmbPropertyLayer.Items.Add(New NameCaptionPair(ll.Name, ll.Caption))
        Next

        'Initialize labels, etc.
        ListProperties()
        uxOK.Enabled = False 'Disabled until a property layer is chosen
    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxOK.Click
        Dim ll As TGIS_LayerVector, lDest As TGIS_LayerVector
        Dim shp As TGIS_Shape, selectionShp As TGIS_Shape
        Dim i As Integer
        Dim rt As RelationType

        If cmbPropertyLayer.SelectedItem IsNot Nothing Then
            'Make sure required attributes are specified
            If cmbPropName.SelectedIndex <= 0 AndAlso cmbPropNumber.SelectedIndex <= 0 Then
                MsgBox(GisControlView.LangStr(_strIdMissingPropertyKeyField), MsgBoxStyle.Exclamation)
                Exit Sub
            End If

            'Check layer relation type
            ll = _gisCtrl.Get(CType(cmbPropertyLayer.SelectedItem, NameCaptionPair).Name)
            rt = GisControlView.GetLayerRelationType(ll.Name)
            If Not TypeOf ll Is TGIS_LayerSqlAbstract OrElse (rt <> RelationType.PropertyRelation AndAlso rt <> RelationType.Undefined) Then
                'Let user specify destination layer
                Dim dlg As New LayerChoiceDialog(RelationType.PropertyRelation, GisControlView.LangStr(_strIdDestinationLayer), True, ll.SupportedShapes)
                dlg.ShowDialog()
                If dlg.LayerOne = GisControlView.NewLayerName Then
                    lDest = _gisCtrl.Get(GisControlView.Instance.AddNewLayer(False, vbNullString, RelationType.PropertyRelation))
                Else
                    lDest = _gisCtrl.Get(dlg.LayerOne)
                End If
            ElseIf rt = RelationType.Undefined Then
                'Update relation type
                GisControlView.UpdateLayerRelation(ll.Name, ll.Caption, ll.CS.EPSG, RelationType.PropertyRelation)
                lDest = ll
            Else
                lDest = ll
            End If

            'Check each shape in layer for an intersection
            selectionShp = CType(_gisCtrl.Get(_selectionLayerName), TGIS_LayerVector).GetShape(_selectionShapeUid)
            For i = 1 To ll.GetLastUid()
                shp = ll.GetShape(i)
                If shp IsNot Nothing Then
                    If selectionShp.IsCommonPoint(shp) Then
                        Dim county As String = vbNullString, municipal As String = vbNullString, parish As String = vbNullString
                        Dim propNumber As String = vbNullString, propName As String = vbNullString
                        Dim block As String = vbNullString, unit As String = vbNullString
                        Dim area As Double, measuredArea As Double
                        Dim objId As String = vbNullString

                        'Get mapped values from shape attributes
                        If cmbCounty.SelectedIndex > 0 Then county = shp.GetField(cmbCounty.SelectedItem)
                        If cmbMunicipal.SelectedIndex > 0 Then municipal = shp.GetField(cmbMunicipal.SelectedItem)
                        If cmbParish.SelectedIndex > 0 Then parish = shp.GetField(cmbParish.SelectedItem)
                        If cmbPropNumber.SelectedIndex > 0 Then propNumber = shp.GetField(cmbPropNumber.SelectedItem)
                        If cmbPropName.SelectedIndex > 0 Then propName = shp.GetField(cmbPropName.SelectedItem)
                        If cmbBlock.SelectedIndex > 0 Then block = shp.GetField(cmbBlock.SelectedItem)
                        If cmbUnit.SelectedIndex > 0 Then unit = shp.GetField(cmbUnit.SelectedItem)
                        Try
                            If cmbArea.SelectedIndex > 0 Then area = shp.GetField(cmbArea.SelectedItem)
                        Catch ex As InvalidCastException
                        End Try
                        Try
                            If cmbMeasuredArea.SelectedIndex > 0 Then measuredArea = shp.GetField(cmbMeasuredArea.SelectedItem)
                        Catch ex As InvalidCastException
                        End Try
                        If cmbObjID.SelectedIndex > 0 Then objId = shp.GetField(cmbObjID.SelectedItem)

                        'Copy shape to destination layer
                        If lDest IsNot ll Then shp = lDest.AddShape(shp)

                        'Add property record and bind it to the shape
                        shp.MakeEditable().SetField(GisControlView.FieldNameRelation, GisControlView.AddProperty(county, municipal, parish, propNumber, propName, block, unit, objId, area, measuredArea, shp))
                    End If
                End If
            Next
            lDest.SaveAll()

            Me.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Close()
        End If
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxCancel.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub cmbPropertyLayer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbPropertyLayer.SelectedIndexChanged
        Dim ll As TGIS_LayerVector
        Dim i As Integer

        'Clear any selected shapes in previous layer
        ll = _gisCtrl.Get(_previousLayerName)
        If ll IsNot Nothing Then ll.DeselectAll()

        'Fill up attribute combos with list of current layer fields
        ll = _gisCtrl.Get(CType(cmbPropertyLayer.SelectedItem, NameCaptionPair).Name)
        For Each c As Control In grpAttributes.Controls
            Dim cmb As ComboBox = TryCast(c, ComboBox)
            If cmb IsNot Nothing Then
                cmb.Items.Clear()
                For i = 0 To ll.Fields.Count - 1
                    If i = 0 Then cmb.Items.Add(" ") 'Empty option
                    If Not GisControlView.IsRelationField(ll.FieldInfo(i).Name) Then cmb.Items.Add(ll.FieldInfo(i).Name)
                Next
            End If
        Next

        If cmbPropertyLayer.SelectedItem IsNot Nothing Then uxOK.Enabled = True

        _previousLayerName = CType(cmbPropertyLayer.SelectedItem, NameCaptionPair).Name

        ListProperties()
    End Sub

    Private Sub chkHighlight_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkHighlight.CheckStateChanged
        ListProperties()
    End Sub
#End Region
End Class
