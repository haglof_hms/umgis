﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LayerPropertiesDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.lblAreaColor = New System.Windows.Forms.Label
        Me.lblAreaOutlineColor = New System.Windows.Forms.Label
        Me.chkAreaColorUR = New System.Windows.Forms.CheckBox
        Me.btnOK = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.FontDialog = New System.Windows.Forms.FontDialog
        Me.Caption = New System.Windows.Forms.TextBox
        Me.lblCaption = New System.Windows.Forms.Label
        Me.lblPath = New System.Windows.Forms.Label
        Me.Path = New System.Windows.Forms.TextBox
        Me.Relation = New System.Windows.Forms.ComboBox
        Me.lblRelation = New System.Windows.Forms.Label
        Me.btnRestructure = New System.Windows.Forms.Button
        Me.RenderField = New System.Windows.Forms.ComboBox
        Me.Tabs = New System.Windows.Forms.TabControl
        Me.TabGeneral = New System.Windows.Forms.TabPage
        Me.AlphaChannel = New System.Windows.Forms.ComboBox
        Me.lblAlphaChannel = New System.Windows.Forms.Label
        Me.View = New System.Windows.Forms.ComboBox
        Me.lblView = New System.Windows.Forms.Label
        Me.btnProjection = New System.Windows.Forms.Button
        Me.Projection = New System.Windows.Forms.TextBox
        Me.lblProjection = New System.Windows.Forms.Label
        Me.LineGeneral = New System.Windows.Forms.Label
        Me.Transparency = New System.Windows.Forms.NumericUpDown
        Me.lblTransparency = New System.Windows.Forms.Label
        Me.TabSection = New System.Windows.Forms.TabPage
        Me.btnSectionClearMaxScale = New System.Windows.Forms.Button
        Me.btnSectionClearMinScale = New System.Windows.Forms.Button
        Me.btnSectionCurrentScaleMax = New System.Windows.Forms.Button
        Me.btnSectionCurrentScaleMin = New System.Windows.Forms.Button
        Me.SectionLegend = New System.Windows.Forms.TextBox
        Me.SectionQuery = New System.Windows.Forms.ComboBox
        Me.SectionMaxScale = New System.Windows.Forms.TextBox
        Me.SectionMinScale = New System.Windows.Forms.TextBox
        Me.chkSectionVisible = New System.Windows.Forms.CheckBox
        Me.lblSectionMaxScale = New System.Windows.Forms.Label
        Me.lblSectionMinScale = New System.Windows.Forms.Label
        Me.lblSectionLegend = New System.Windows.Forms.Label
        Me.lblSectionQuery = New System.Windows.Forms.Label
        Me.TabRendering = New System.Windows.Forms.TabPage
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabFirst = New System.Windows.Forms.TabPage
        Me.RenderDefaultColor = New ColorButton.ColorButton
        Me.RenderDefaultSize = New System.Windows.Forms.NumericUpDown
        Me.RenderEndColor = New ColorButton.ColorButton
        Me.RenderZones = New System.Windows.Forms.NumericUpDown
        Me.RenderStartColor = New ColorButton.ColorButton
        Me.RenderEndSize = New System.Windows.Forms.NumericUpDown
        Me.RenderStartSize = New System.Windows.Forms.NumericUpDown
        Me.RenderMaxValue = New System.Windows.Forms.TextBox
        Me.RenderMinValue = New System.Windows.Forms.TextBox
        Me.lblZones = New System.Windows.Forms.Label
        Me.lblDefaultSize = New System.Windows.Forms.Label
        Me.lblStartColor = New System.Windows.Forms.Label
        Me.lblDefaultColor = New System.Windows.Forms.Label
        Me.lblStartSize = New System.Windows.Forms.Label
        Me.lblEndSize = New System.Windows.Forms.Label
        Me.lblMinValue = New System.Windows.Forms.Label
        Me.lblEndColor = New System.Windows.Forms.Label
        Me.lblMaxValue = New System.Windows.Forms.Label
        Me.TabSecond = New System.Windows.Forms.TabPage
        Me.RenderDefaultColorEx = New ColorButton.ColorButton
        Me.RenderDefaultSizeEx = New System.Windows.Forms.NumericUpDown
        Me.RenderZonesEx = New System.Windows.Forms.NumericUpDown
        Me.RenderEndSizeEx = New System.Windows.Forms.NumericUpDown
        Me.RenderStartSizeEx = New System.Windows.Forms.NumericUpDown
        Me.RenderMaxValueEx = New System.Windows.Forms.TextBox
        Me.RenderMinValueEx = New System.Windows.Forms.TextBox
        Me.lblZonesEx = New System.Windows.Forms.Label
        Me.lblDefaultSizeEx = New System.Windows.Forms.Label
        Me.lblStartColorEx = New System.Windows.Forms.Label
        Me.lblDefaultColorEx = New System.Windows.Forms.Label
        Me.lblStartSizeEx = New System.Windows.Forms.Label
        Me.lblEndSizeEx = New System.Windows.Forms.Label
        Me.lblMinValueEx = New System.Windows.Forms.Label
        Me.lblEndColorEx = New System.Windows.Forms.Label
        Me.lblMaxValueEx = New System.Windows.Forms.Label
        Me.RenderEndColorEx = New ColorButton.ColorButton
        Me.RenderStartColorEx = New ColorButton.ColorButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblField = New System.Windows.Forms.Label
        Me.TabArea = New System.Windows.Forms.TabPage
        Me.AreaOutlineStyle = New System.Windows.Forms.ComboBox
        Me.lblAreaOutlineStyle = New System.Windows.Forms.Label
        Me.chkAreaOutlineWidthUR = New System.Windows.Forms.CheckBox
        Me.AreaOutlineWidth = New System.Windows.Forms.NumericUpDown
        Me.lblAreaOutlineWidth = New System.Windows.Forms.Label
        Me.AreaPattern = New System.Windows.Forms.ComboBox
        Me.chkAreaInLegend = New System.Windows.Forms.CheckBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.chkAreaOutlineColorUR = New System.Windows.Forms.CheckBox
        Me.lblAreaPattern = New System.Windows.Forms.Label
        Me.AreaColor = New ColorButton.ColorButton
        Me.AreaOutlineColor = New ColorButton.ColorButton
        Me.TabLine = New System.Windows.Forms.TabPage
        Me.LineOutlineStyle = New System.Windows.Forms.ComboBox
        Me.chkLineOutlineWidthUR = New System.Windows.Forms.CheckBox
        Me.chkLineOutlineColorUR = New System.Windows.Forms.CheckBox
        Me.LineOutlineWidth = New System.Windows.Forms.NumericUpDown
        Me.lblLineOutlineWidth = New System.Windows.Forms.Label
        Me.lblLineOutlineStyle = New System.Windows.Forms.Label
        Me.lblLineOutlineColor = New System.Windows.Forms.Label
        Me.LineStyle = New System.Windows.Forms.ComboBox
        Me.chkLineInLegend = New System.Windows.Forms.CheckBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.chkLineWidthUR = New System.Windows.Forms.CheckBox
        Me.chkLineColorUR = New System.Windows.Forms.CheckBox
        Me.LineWidth = New System.Windows.Forms.NumericUpDown
        Me.lblLineWidth = New System.Windows.Forms.Label
        Me.lblLineStyle = New System.Windows.Forms.Label
        Me.lblLineColor = New System.Windows.Forms.Label
        Me.LineOutlineColor = New ColorButton.ColorButton
        Me.LineColor = New ColorButton.ColorButton
        Me.TabMarker = New System.Windows.Forms.TabPage
        Me.btnMarkerSymbol = New System.Windows.Forms.Button
        Me.MarkerOutlineStyle = New System.Windows.Forms.ComboBox
        Me.chkMarkerOutlineWidthUR = New System.Windows.Forms.CheckBox
        Me.chkMarkerOutlineColorUR = New System.Windows.Forms.CheckBox
        Me.MarkerOutlineWidth = New System.Windows.Forms.NumericUpDown
        Me.lblMarkerOutlineWidth = New System.Windows.Forms.Label
        Me.lblMarkerOutlineStyle = New System.Windows.Forms.Label
        Me.lblMarkerOutlineColor = New System.Windows.Forms.Label
        Me.MarkerStyle = New System.Windows.Forms.ComboBox
        Me.chkMarkerInLegend = New System.Windows.Forms.CheckBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.chkMarkerSizeUR = New System.Windows.Forms.CheckBox
        Me.chkMarkerColorUR = New System.Windows.Forms.CheckBox
        Me.MarkerSize = New System.Windows.Forms.NumericUpDown
        Me.lblMarkerSize = New System.Windows.Forms.Label
        Me.lblMarkerStyle = New System.Windows.Forms.Label
        Me.lblMarkerColor = New System.Windows.Forms.Label
        Me.MarkerOutlineColor = New ColorButton.ColorButton
        Me.MarkerColor = New ColorButton.ColorButton
        Me.TabLabel = New System.Windows.Forms.TabPage
        Me.lblLabelInfo = New System.Windows.Forms.Label
        Me.uxLabelPosition = New System.Windows.Forms.GroupBox
        Me.uxLabelPos9 = New System.Windows.Forms.CheckBox
        Me.uxLabelPos8 = New System.Windows.Forms.CheckBox
        Me.uxLabelPos7 = New System.Windows.Forms.CheckBox
        Me.uxLabelPos6 = New System.Windows.Forms.CheckBox
        Me.uxLabelPos5 = New System.Windows.Forms.CheckBox
        Me.uxLabelPos4 = New System.Windows.Forms.CheckBox
        Me.uxLabelPos3 = New System.Windows.Forms.CheckBox
        Me.uxLabelPos2 = New System.Windows.Forms.CheckBox
        Me.uxLabelPos1 = New System.Windows.Forms.CheckBox
        Me.LabelRotation = New System.Windows.Forms.NumericUpDown
        Me.lblLabelRotation = New System.Windows.Forms.Label
        Me.chkLabelColorUR = New System.Windows.Forms.CheckBox
        Me.LabelField = New System.Windows.Forms.ComboBox
        Me.chkLabelVisible = New System.Windows.Forms.CheckBox
        Me.chkLabelInLegend = New System.Windows.Forms.CheckBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.LabelValue = New System.Windows.Forms.TextBox
        Me.lblLabelValue = New System.Windows.Forms.Label
        Me.lblLabelField = New System.Windows.Forms.Label
        Me.lblLabelColor = New System.Windows.Forms.Label
        Me.lblLabelFont = New System.Windows.Forms.Label
        Me.btnLabelFont = New System.Windows.Forms.Button
        Me.LabelColor = New ColorButton.ColorButton
        Me.TabChart = New System.Windows.Forms.TabPage
        Me.Label6 = New System.Windows.Forms.Label
        Me.chkChartSizeUR = New System.Windows.Forms.CheckBox
        Me.ChartSize = New System.Windows.Forms.NumericUpDown
        Me.lblChartSize = New System.Windows.Forms.Label
        Me.chkChartInLegend = New System.Windows.Forms.CheckBox
        Me.ChartStyle = New System.Windows.Forms.ComboBox
        Me.lblChartStyle = New System.Windows.Forms.Label
        Me.lblChartFields = New System.Windows.Forms.Label
        Me.ChartFields = New System.Windows.Forms.ListBox
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.TabPage4 = New System.Windows.Forms.TabPage
        Me.lstParams = New System.Windows.Forms.ListBox
        Me.btnParamsAdd = New System.Windows.Forms.Button
        Me.btnParamsRemove = New System.Windows.Forms.Button
        Me.btnParamsRemoveAll = New System.Windows.Forms.Button
        Me.btnApply = New System.Windows.Forms.Button
        Me.btnWizard = New System.Windows.Forms.Button
        Me.pbPreview = New System.Windows.Forms.PictureBox
        Me.RemoveParamsMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.RemoveAllToolStripMenuItemToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.RemoveAllButFirstToolStripMenuItemToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.Tabs.SuspendLayout()
        Me.TabGeneral.SuspendLayout()
        CType(Me.Transparency, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabSection.SuspendLayout()
        Me.TabRendering.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabFirst.SuspendLayout()
        CType(Me.RenderDefaultSize, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RenderZones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RenderEndSize, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RenderStartSize, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabSecond.SuspendLayout()
        CType(Me.RenderDefaultSizeEx, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RenderZonesEx, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RenderEndSizeEx, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RenderStartSizeEx, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabArea.SuspendLayout()
        CType(Me.AreaOutlineWidth, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabLine.SuspendLayout()
        CType(Me.LineOutlineWidth, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LineWidth, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabMarker.SuspendLayout()
        CType(Me.MarkerOutlineWidth, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MarkerSize, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabLabel.SuspendLayout()
        Me.uxLabelPosition.SuspendLayout()
        CType(Me.LabelRotation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabChart.SuspendLayout()
        CType(Me.ChartSize, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage1.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        CType(Me.pbPreview, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RemoveParamsMenu.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblAreaColor
        '
        Me.lblAreaColor.AutoSize = True
        Me.lblAreaColor.Location = New System.Drawing.Point(146, 11)
        Me.lblAreaColor.Name = "lblAreaColor"
        Me.lblAreaColor.Size = New System.Drawing.Size(63, 13)
        Me.lblAreaColor.TabIndex = 6
        Me.lblAreaColor.Text = "lblAreaColor"
        '
        'lblAreaOutlineColor
        '
        Me.lblAreaOutlineColor.AutoSize = True
        Me.lblAreaOutlineColor.Location = New System.Drawing.Point(146, 86)
        Me.lblAreaOutlineColor.Name = "lblAreaOutlineColor"
        Me.lblAreaOutlineColor.Size = New System.Drawing.Size(96, 13)
        Me.lblAreaOutlineColor.TabIndex = 8
        Me.lblAreaOutlineColor.Text = "lblAreaOutlineColor"
        '
        'chkAreaColorUR
        '
        Me.chkAreaColorUR.AutoSize = True
        Me.chkAreaColorUR.Location = New System.Drawing.Point(149, 57)
        Me.chkAreaColorUR.Name = "chkAreaColorUR"
        Me.chkAreaColorUR.Size = New System.Drawing.Size(106, 17)
        Me.chkAreaColorUR.TabIndex = 3
        Me.chkAreaColorUR.Text = "chkAreaColorUR"
        Me.chkAreaColorUR.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(461, 34)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 100
        Me.btnOK.Text = "btnOK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(461, 92)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 102
        Me.btnCancel.Text = "btnCancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'Caption
        '
        Me.Caption.Location = New System.Drawing.Point(9, 28)
        Me.Caption.Name = "Caption"
        Me.Caption.Size = New System.Drawing.Size(192, 20)
        Me.Caption.TabIndex = 1
        '
        'lblCaption
        '
        Me.lblCaption.AutoSize = True
        Me.lblCaption.Location = New System.Drawing.Point(6, 12)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(53, 13)
        Me.lblCaption.TabIndex = 20
        Me.lblCaption.Text = "lblCaption"
        '
        'lblPath
        '
        Me.lblPath.AutoSize = True
        Me.lblPath.Location = New System.Drawing.Point(204, 12)
        Me.lblPath.Name = "lblPath"
        Me.lblPath.Size = New System.Drawing.Size(39, 13)
        Me.lblPath.TabIndex = 22
        Me.lblPath.Text = "lblPath"
        '
        'Path
        '
        Me.Path.Location = New System.Drawing.Point(207, 28)
        Me.Path.Name = "Path"
        Me.Path.Size = New System.Drawing.Size(192, 20)
        Me.Path.TabIndex = 2
        '
        'Relation
        '
        Me.Relation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Relation.FormattingEnabled = True
        Me.Relation.Location = New System.Drawing.Point(9, 71)
        Me.Relation.Name = "Relation"
        Me.Relation.Size = New System.Drawing.Size(192, 21)
        Me.Relation.TabIndex = 3
        '
        'lblRelation
        '
        Me.lblRelation.AutoSize = True
        Me.lblRelation.Location = New System.Drawing.Point(6, 55)
        Me.lblRelation.Name = "lblRelation"
        Me.lblRelation.Size = New System.Drawing.Size(56, 13)
        Me.lblRelation.TabIndex = 24
        Me.lblRelation.Text = "lblRelation"
        '
        'btnRestructure
        '
        Me.btnRestructure.Location = New System.Drawing.Point(9, 144)
        Me.btnRestructure.Name = "btnRestructure"
        Me.btnRestructure.Size = New System.Drawing.Size(118, 23)
        Me.btnRestructure.TabIndex = 7
        Me.btnRestructure.Text = "btnRestructure"
        Me.btnRestructure.UseVisualStyleBackColor = True
        '
        'RenderField
        '
        Me.RenderField.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.RenderField.FormattingEnabled = True
        Me.RenderField.Location = New System.Drawing.Point(9, 28)
        Me.RenderField.Name = "RenderField"
        Me.RenderField.Size = New System.Drawing.Size(260, 21)
        Me.RenderField.TabIndex = 1
        '
        'Tabs
        '
        Me.Tabs.Controls.Add(Me.TabGeneral)
        Me.Tabs.Controls.Add(Me.TabSection)
        Me.Tabs.Controls.Add(Me.TabRendering)
        Me.Tabs.Controls.Add(Me.TabArea)
        Me.Tabs.Controls.Add(Me.TabLine)
        Me.Tabs.Controls.Add(Me.TabMarker)
        Me.Tabs.Controls.Add(Me.TabLabel)
        Me.Tabs.Controls.Add(Me.TabChart)
        Me.Tabs.Location = New System.Drawing.Point(12, 12)
        Me.Tabs.Name = "Tabs"
        Me.Tabs.SelectedIndex = 0
        Me.Tabs.Size = New System.Drawing.Size(443, 277)
        Me.Tabs.TabIndex = 33
        '
        'TabGeneral
        '
        Me.TabGeneral.Controls.Add(Me.AlphaChannel)
        Me.TabGeneral.Controls.Add(Me.lblAlphaChannel)
        Me.TabGeneral.Controls.Add(Me.View)
        Me.TabGeneral.Controls.Add(Me.lblView)
        Me.TabGeneral.Controls.Add(Me.btnProjection)
        Me.TabGeneral.Controls.Add(Me.Projection)
        Me.TabGeneral.Controls.Add(Me.lblProjection)
        Me.TabGeneral.Controls.Add(Me.Caption)
        Me.TabGeneral.Controls.Add(Me.LineGeneral)
        Me.TabGeneral.Controls.Add(Me.Transparency)
        Me.TabGeneral.Controls.Add(Me.lblTransparency)
        Me.TabGeneral.Controls.Add(Me.lblCaption)
        Me.TabGeneral.Controls.Add(Me.Path)
        Me.TabGeneral.Controls.Add(Me.btnRestructure)
        Me.TabGeneral.Controls.Add(Me.lblPath)
        Me.TabGeneral.Controls.Add(Me.Relation)
        Me.TabGeneral.Controls.Add(Me.lblRelation)
        Me.TabGeneral.Location = New System.Drawing.Point(4, 22)
        Me.TabGeneral.Name = "TabGeneral"
        Me.TabGeneral.Padding = New System.Windows.Forms.Padding(3)
        Me.TabGeneral.Size = New System.Drawing.Size(435, 251)
        Me.TabGeneral.TabIndex = 0
        Me.TabGeneral.Text = "TabGeneral"
        Me.TabGeneral.UseVisualStyleBackColor = True
        '
        'AlphaChannel
        '
        Me.AlphaChannel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.AlphaChannel.FormattingEnabled = True
        Me.AlphaChannel.Location = New System.Drawing.Point(202, 208)
        Me.AlphaChannel.Name = "AlphaChannel"
        Me.AlphaChannel.Size = New System.Drawing.Size(153, 21)
        Me.AlphaChannel.TabIndex = 39
        '
        'lblAlphaChannel
        '
        Me.lblAlphaChannel.AutoSize = True
        Me.lblAlphaChannel.Location = New System.Drawing.Point(199, 192)
        Me.lblAlphaChannel.Name = "lblAlphaChannel"
        Me.lblAlphaChannel.Size = New System.Drawing.Size(83, 13)
        Me.lblAlphaChannel.TabIndex = 38
        Me.lblAlphaChannel.Text = "lblAlphaChannel"
        '
        'View
        '
        Me.View.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.View.FormattingEnabled = True
        Me.View.Location = New System.Drawing.Point(207, 71)
        Me.View.Name = "View"
        Me.View.Size = New System.Drawing.Size(192, 21)
        Me.View.TabIndex = 36
        '
        'lblView
        '
        Me.lblView.AutoSize = True
        Me.lblView.Location = New System.Drawing.Point(204, 55)
        Me.lblView.Name = "lblView"
        Me.lblView.Size = New System.Drawing.Size(40, 13)
        Me.lblView.TabIndex = 37
        Me.lblView.Text = "lblView"
        '
        'btnProjection
        '
        Me.btnProjection.Location = New System.Drawing.Point(207, 112)
        Me.btnProjection.Name = "btnProjection"
        Me.btnProjection.Size = New System.Drawing.Size(118, 23)
        Me.btnProjection.TabIndex = 6
        Me.btnProjection.Text = " btnProjection"
        Me.btnProjection.UseVisualStyleBackColor = True
        '
        'Projection
        '
        Me.Projection.Enabled = False
        Me.Projection.Location = New System.Drawing.Point(9, 114)
        Me.Projection.Name = "Projection"
        Me.Projection.Size = New System.Drawing.Size(192, 20)
        Me.Projection.TabIndex = 5
        '
        'lblProjection
        '
        Me.lblProjection.AutoSize = True
        Me.lblProjection.Location = New System.Drawing.Point(6, 98)
        Me.lblProjection.Name = "lblProjection"
        Me.lblProjection.Size = New System.Drawing.Size(64, 13)
        Me.lblProjection.TabIndex = 35
        Me.lblProjection.Text = "lblProjection"
        '
        'LineGeneral
        '
        Me.LineGeneral.BackColor = System.Drawing.Color.Black
        Me.LineGeneral.Location = New System.Drawing.Point(9, 182)
        Me.LineGeneral.Name = "LineGeneral"
        Me.LineGeneral.Size = New System.Drawing.Size(416, 1)
        Me.LineGeneral.TabIndex = 33
        '
        'Transparency
        '
        Me.Transparency.Location = New System.Drawing.Point(9, 208)
        Me.Transparency.Name = "Transparency"
        Me.Transparency.Size = New System.Drawing.Size(79, 20)
        Me.Transparency.TabIndex = 8
        '
        'lblTransparency
        '
        Me.lblTransparency.AutoSize = True
        Me.lblTransparency.Location = New System.Drawing.Point(6, 192)
        Me.lblTransparency.Name = "lblTransparency"
        Me.lblTransparency.Size = New System.Drawing.Size(82, 13)
        Me.lblTransparency.TabIndex = 31
        Me.lblTransparency.Text = "lblTransparency"
        '
        'TabSection
        '
        Me.TabSection.Controls.Add(Me.btnSectionClearMaxScale)
        Me.TabSection.Controls.Add(Me.btnSectionClearMinScale)
        Me.TabSection.Controls.Add(Me.btnSectionCurrentScaleMax)
        Me.TabSection.Controls.Add(Me.btnSectionCurrentScaleMin)
        Me.TabSection.Controls.Add(Me.SectionLegend)
        Me.TabSection.Controls.Add(Me.SectionQuery)
        Me.TabSection.Controls.Add(Me.SectionMaxScale)
        Me.TabSection.Controls.Add(Me.SectionMinScale)
        Me.TabSection.Controls.Add(Me.chkSectionVisible)
        Me.TabSection.Controls.Add(Me.lblSectionMaxScale)
        Me.TabSection.Controls.Add(Me.lblSectionMinScale)
        Me.TabSection.Controls.Add(Me.lblSectionLegend)
        Me.TabSection.Controls.Add(Me.lblSectionQuery)
        Me.TabSection.Location = New System.Drawing.Point(4, 22)
        Me.TabSection.Name = "TabSection"
        Me.TabSection.Size = New System.Drawing.Size(435, 251)
        Me.TabSection.TabIndex = 7
        Me.TabSection.Text = "TabSection"
        Me.TabSection.UseVisualStyleBackColor = True
        '
        'btnSectionClearMaxScale
        '
        Me.btnSectionClearMaxScale.Enabled = False
        Me.btnSectionClearMaxScale.Location = New System.Drawing.Point(285, 55)
        Me.btnSectionClearMaxScale.Name = "btnSectionClearMaxScale"
        Me.btnSectionClearMaxScale.Size = New System.Drawing.Size(20, 20)
        Me.btnSectionClearMaxScale.TabIndex = 8
        Me.btnSectionClearMaxScale.TabStop = False
        Me.btnSectionClearMaxScale.UseVisualStyleBackColor = True
        '
        'btnSectionClearMinScale
        '
        Me.btnSectionClearMinScale.Enabled = False
        Me.btnSectionClearMinScale.Location = New System.Drawing.Point(84, 55)
        Me.btnSectionClearMinScale.Name = "btnSectionClearMinScale"
        Me.btnSectionClearMinScale.Size = New System.Drawing.Size(20, 20)
        Me.btnSectionClearMinScale.TabIndex = 2
        Me.btnSectionClearMinScale.TabStop = False
        Me.btnSectionClearMinScale.UseVisualStyleBackColor = True
        '
        'btnSectionCurrentScaleMax
        '
        Me.btnSectionCurrentScaleMax.Location = New System.Drawing.Point(210, 55)
        Me.btnSectionCurrentScaleMax.Name = "btnSectionCurrentScaleMax"
        Me.btnSectionCurrentScaleMax.Size = New System.Drawing.Size(75, 20)
        Me.btnSectionCurrentScaleMax.TabIndex = 3
        Me.btnSectionCurrentScaleMax.Text = "btnCurrentScaleMax"
        Me.btnSectionCurrentScaleMax.UseVisualStyleBackColor = True
        '
        'btnSectionCurrentScaleMin
        '
        Me.btnSectionCurrentScaleMin.Location = New System.Drawing.Point(9, 55)
        Me.btnSectionCurrentScaleMin.Name = "btnSectionCurrentScaleMin"
        Me.btnSectionCurrentScaleMin.Size = New System.Drawing.Size(75, 20)
        Me.btnSectionCurrentScaleMin.TabIndex = 1
        Me.btnSectionCurrentScaleMin.Text = "btnCurrentScaleMin"
        Me.btnSectionCurrentScaleMin.UseVisualStyleBackColor = True
        '
        'SectionLegend
        '
        Me.SectionLegend.Location = New System.Drawing.Point(9, 148)
        Me.SectionLegend.Name = "SectionLegend"
        Me.SectionLegend.Size = New System.Drawing.Size(393, 20)
        Me.SectionLegend.TabIndex = 6
        '
        'SectionQuery
        '
        Me.SectionQuery.FormattingEnabled = True
        Me.SectionQuery.Location = New System.Drawing.Point(9, 101)
        Me.SectionQuery.Name = "SectionQuery"
        Me.SectionQuery.Size = New System.Drawing.Size(393, 21)
        Me.SectionQuery.TabIndex = 5
        '
        'SectionMaxScale
        '
        Me.SectionMaxScale.Location = New System.Drawing.Point(305, 55)
        Me.SectionMaxScale.Name = "SectionMaxScale"
        Me.SectionMaxScale.Size = New System.Drawing.Size(100, 20)
        Me.SectionMaxScale.TabIndex = 4
        '
        'SectionMinScale
        '
        Me.SectionMinScale.Location = New System.Drawing.Point(104, 55)
        Me.SectionMinScale.Name = "SectionMinScale"
        Me.SectionMinScale.Size = New System.Drawing.Size(100, 20)
        Me.SectionMinScale.TabIndex = 2
        '
        'chkSectionVisible
        '
        Me.chkSectionVisible.AutoSize = True
        Me.chkSectionVisible.Location = New System.Drawing.Point(6, 12)
        Me.chkSectionVisible.Name = "chkSectionVisible"
        Me.chkSectionVisible.Size = New System.Drawing.Size(110, 17)
        Me.chkSectionVisible.TabIndex = 0
        Me.chkSectionVisible.Text = "chkSectionVisible"
        Me.chkSectionVisible.UseVisualStyleBackColor = True
        '
        'lblSectionMaxScale
        '
        Me.lblSectionMaxScale.AutoSize = True
        Me.lblSectionMaxScale.Location = New System.Drawing.Point(207, 39)
        Me.lblSectionMaxScale.Name = "lblSectionMaxScale"
        Me.lblSectionMaxScale.Size = New System.Drawing.Size(100, 13)
        Me.lblSectionMaxScale.TabIndex = 10
        Me.lblSectionMaxScale.Text = "lblSectionMaxScale"
        '
        'lblSectionMinScale
        '
        Me.lblSectionMinScale.AutoSize = True
        Me.lblSectionMinScale.Location = New System.Drawing.Point(6, 39)
        Me.lblSectionMinScale.Name = "lblSectionMinScale"
        Me.lblSectionMinScale.Size = New System.Drawing.Size(97, 13)
        Me.lblSectionMinScale.TabIndex = 9
        Me.lblSectionMinScale.Text = "lblSectionMinScale"
        '
        'lblSectionLegend
        '
        Me.lblSectionLegend.AutoSize = True
        Me.lblSectionLegend.Location = New System.Drawing.Point(6, 132)
        Me.lblSectionLegend.Name = "lblSectionLegend"
        Me.lblSectionLegend.Size = New System.Drawing.Size(89, 13)
        Me.lblSectionLegend.TabIndex = 12
        Me.lblSectionLegend.Text = "lblSectionLegend"
        '
        'lblSectionQuery
        '
        Me.lblSectionQuery.AutoSize = True
        Me.lblSectionQuery.Location = New System.Drawing.Point(6, 85)
        Me.lblSectionQuery.Name = "lblSectionQuery"
        Me.lblSectionQuery.Size = New System.Drawing.Size(81, 13)
        Me.lblSectionQuery.TabIndex = 11
        Me.lblSectionQuery.Text = "lblSectionQuery"
        '
        'TabRendering
        '
        Me.TabRendering.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.TabRendering.Controls.Add(Me.TabControl1)
        Me.TabRendering.Controls.Add(Me.RenderField)
        Me.TabRendering.Controls.Add(Me.Label1)
        Me.TabRendering.Controls.Add(Me.lblField)
        Me.TabRendering.Location = New System.Drawing.Point(4, 22)
        Me.TabRendering.Name = "TabRendering"
        Me.TabRendering.Padding = New System.Windows.Forms.Padding(3)
        Me.TabRendering.Size = New System.Drawing.Size(435, 251)
        Me.TabRendering.TabIndex = 1
        Me.TabRendering.Text = "TabRendering"
        Me.TabRendering.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabFirst)
        Me.TabControl1.Controls.Add(Me.TabSecond)
        Me.TabControl1.Location = New System.Drawing.Point(9, 74)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(278, 173)
        Me.TabControl1.TabIndex = 61
        '
        'TabFirst
        '
        Me.TabFirst.Controls.Add(Me.RenderDefaultColor)
        Me.TabFirst.Controls.Add(Me.RenderDefaultSize)
        Me.TabFirst.Controls.Add(Me.RenderEndColor)
        Me.TabFirst.Controls.Add(Me.RenderZones)
        Me.TabFirst.Controls.Add(Me.RenderStartColor)
        Me.TabFirst.Controls.Add(Me.RenderEndSize)
        Me.TabFirst.Controls.Add(Me.RenderStartSize)
        Me.TabFirst.Controls.Add(Me.RenderMaxValue)
        Me.TabFirst.Controls.Add(Me.RenderMinValue)
        Me.TabFirst.Controls.Add(Me.lblZones)
        Me.TabFirst.Controls.Add(Me.lblDefaultSize)
        Me.TabFirst.Controls.Add(Me.lblStartColor)
        Me.TabFirst.Controls.Add(Me.lblDefaultColor)
        Me.TabFirst.Controls.Add(Me.lblStartSize)
        Me.TabFirst.Controls.Add(Me.lblEndSize)
        Me.TabFirst.Controls.Add(Me.lblMinValue)
        Me.TabFirst.Controls.Add(Me.lblEndColor)
        Me.TabFirst.Controls.Add(Me.lblMaxValue)
        Me.TabFirst.Location = New System.Drawing.Point(4, 22)
        Me.TabFirst.Name = "TabFirst"
        Me.TabFirst.Padding = New System.Windows.Forms.Padding(3)
        Me.TabFirst.Size = New System.Drawing.Size(270, 147)
        Me.TabFirst.TabIndex = 0
        Me.TabFirst.Text = "TabFirst"
        Me.TabFirst.UseVisualStyleBackColor = True
        '
        'RenderDefaultColor
        '
        Me.RenderDefaultColor.Automatic = "Automatic"
        Me.RenderDefaultColor.Color = System.Drawing.Color.Transparent
        Me.RenderDefaultColor.Location = New System.Drawing.Point(187, 71)
        Me.RenderDefaultColor.MoreColors = "More Colors..."
        Me.RenderDefaultColor.Name = "RenderDefaultColor"
        Me.RenderDefaultColor.Size = New System.Drawing.Size(72, 23)
        Me.RenderDefaultColor.TabIndex = 8
        Me.RenderDefaultColor.UseVisualStyleBackColor = True
        '
        'RenderDefaultSize
        '
        Me.RenderDefaultSize.Location = New System.Drawing.Point(187, 122)
        Me.RenderDefaultSize.Maximum = New Decimal(New Integer() {50000, 0, 0, 0})
        Me.RenderDefaultSize.Name = "RenderDefaultSize"
        Me.RenderDefaultSize.Size = New System.Drawing.Size(72, 20)
        Me.RenderDefaultSize.TabIndex = 11
        '
        'RenderEndColor
        '
        Me.RenderEndColor.Automatic = "Automatic"
        Me.RenderEndColor.Color = System.Drawing.Color.Transparent
        Me.RenderEndColor.Location = New System.Drawing.Point(97, 71)
        Me.RenderEndColor.MoreColors = "More Colors..."
        Me.RenderEndColor.Name = "RenderEndColor"
        Me.RenderEndColor.Size = New System.Drawing.Size(72, 23)
        Me.RenderEndColor.TabIndex = 7
        Me.RenderEndColor.UseVisualStyleBackColor = True
        '
        'RenderZones
        '
        Me.RenderZones.Location = New System.Drawing.Point(7, 23)
        Me.RenderZones.Minimum = New Decimal(New Integer() {100, 0, 0, -2147483648})
        Me.RenderZones.Name = "RenderZones"
        Me.RenderZones.Size = New System.Drawing.Size(72, 20)
        Me.RenderZones.TabIndex = 2
        '
        'RenderStartColor
        '
        Me.RenderStartColor.Automatic = "Automatic"
        Me.RenderStartColor.Color = System.Drawing.Color.Transparent
        Me.RenderStartColor.Location = New System.Drawing.Point(7, 71)
        Me.RenderStartColor.MoreColors = "More Colors..."
        Me.RenderStartColor.Name = "RenderStartColor"
        Me.RenderStartColor.Size = New System.Drawing.Size(72, 23)
        Me.RenderStartColor.TabIndex = 6
        Me.RenderStartColor.UseVisualStyleBackColor = True
        '
        'RenderEndSize
        '
        Me.RenderEndSize.Location = New System.Drawing.Point(97, 122)
        Me.RenderEndSize.Maximum = New Decimal(New Integer() {50000, 0, 0, 0})
        Me.RenderEndSize.Name = "RenderEndSize"
        Me.RenderEndSize.Size = New System.Drawing.Size(72, 20)
        Me.RenderEndSize.TabIndex = 10
        '
        'RenderStartSize
        '
        Me.RenderStartSize.Location = New System.Drawing.Point(7, 122)
        Me.RenderStartSize.Maximum = New Decimal(New Integer() {50000, 0, 0, 0})
        Me.RenderStartSize.Name = "RenderStartSize"
        Me.RenderStartSize.Size = New System.Drawing.Size(72, 20)
        Me.RenderStartSize.TabIndex = 9
        '
        'RenderMaxValue
        '
        Me.RenderMaxValue.Location = New System.Drawing.Point(187, 23)
        Me.RenderMaxValue.Name = "RenderMaxValue"
        Me.RenderMaxValue.Size = New System.Drawing.Size(75, 20)
        Me.RenderMaxValue.TabIndex = 4
        '
        'RenderMinValue
        '
        Me.RenderMinValue.Location = New System.Drawing.Point(97, 23)
        Me.RenderMinValue.Name = "RenderMinValue"
        Me.RenderMinValue.Size = New System.Drawing.Size(75, 20)
        Me.RenderMinValue.TabIndex = 3
        '
        'lblZones
        '
        Me.lblZones.AutoSize = True
        Me.lblZones.Location = New System.Drawing.Point(7, 7)
        Me.lblZones.Name = "lblZones"
        Me.lblZones.Size = New System.Drawing.Size(47, 13)
        Me.lblZones.TabIndex = 34
        Me.lblZones.Text = "lblZones"
        '
        'lblDefaultSize
        '
        Me.lblDefaultSize.AutoSize = True
        Me.lblDefaultSize.Location = New System.Drawing.Point(187, 106)
        Me.lblDefaultSize.Name = "lblDefaultSize"
        Me.lblDefaultSize.Size = New System.Drawing.Size(71, 13)
        Me.lblDefaultSize.TabIndex = 56
        Me.lblDefaultSize.Text = "lblDefaultSize"
        '
        'lblStartColor
        '
        Me.lblStartColor.AutoSize = True
        Me.lblStartColor.Location = New System.Drawing.Point(7, 55)
        Me.lblStartColor.Name = "lblStartColor"
        Me.lblStartColor.Size = New System.Drawing.Size(63, 13)
        Me.lblStartColor.TabIndex = 36
        Me.lblStartColor.Text = "lblStartColor"
        '
        'lblDefaultColor
        '
        Me.lblDefaultColor.AutoSize = True
        Me.lblDefaultColor.Location = New System.Drawing.Point(187, 55)
        Me.lblDefaultColor.Name = "lblDefaultColor"
        Me.lblDefaultColor.Size = New System.Drawing.Size(75, 13)
        Me.lblDefaultColor.TabIndex = 55
        Me.lblDefaultColor.Text = "lblDefaultColor"
        '
        'lblStartSize
        '
        Me.lblStartSize.AutoSize = True
        Me.lblStartSize.Location = New System.Drawing.Point(7, 106)
        Me.lblStartSize.Name = "lblStartSize"
        Me.lblStartSize.Size = New System.Drawing.Size(59, 13)
        Me.lblStartSize.TabIndex = 35
        Me.lblStartSize.Text = "lblStartSize"
        '
        'lblEndSize
        '
        Me.lblEndSize.AutoSize = True
        Me.lblEndSize.Location = New System.Drawing.Point(97, 106)
        Me.lblEndSize.Name = "lblEndSize"
        Me.lblEndSize.Size = New System.Drawing.Size(56, 13)
        Me.lblEndSize.TabIndex = 47
        Me.lblEndSize.Text = "lblEndSize"
        '
        'lblMinValue
        '
        Me.lblMinValue.AutoSize = True
        Me.lblMinValue.Location = New System.Drawing.Point(97, 7)
        Me.lblMinValue.Name = "lblMinValue"
        Me.lblMinValue.Size = New System.Drawing.Size(61, 13)
        Me.lblMinValue.TabIndex = 50
        Me.lblMinValue.Text = "lblMinValue"
        '
        'lblEndColor
        '
        Me.lblEndColor.AutoSize = True
        Me.lblEndColor.Location = New System.Drawing.Point(97, 55)
        Me.lblEndColor.Name = "lblEndColor"
        Me.lblEndColor.Size = New System.Drawing.Size(60, 13)
        Me.lblEndColor.TabIndex = 46
        Me.lblEndColor.Text = "lblEndColor"
        '
        'lblMaxValue
        '
        Me.lblMaxValue.AutoSize = True
        Me.lblMaxValue.Location = New System.Drawing.Point(187, 7)
        Me.lblMaxValue.Name = "lblMaxValue"
        Me.lblMaxValue.Size = New System.Drawing.Size(64, 13)
        Me.lblMaxValue.TabIndex = 52
        Me.lblMaxValue.Text = "lblMaxValue"
        '
        'TabSecond
        '
        Me.TabSecond.Controls.Add(Me.RenderDefaultColorEx)
        Me.TabSecond.Controls.Add(Me.RenderDefaultSizeEx)
        Me.TabSecond.Controls.Add(Me.RenderZonesEx)
        Me.TabSecond.Controls.Add(Me.RenderEndSizeEx)
        Me.TabSecond.Controls.Add(Me.RenderStartSizeEx)
        Me.TabSecond.Controls.Add(Me.RenderMaxValueEx)
        Me.TabSecond.Controls.Add(Me.RenderMinValueEx)
        Me.TabSecond.Controls.Add(Me.lblZonesEx)
        Me.TabSecond.Controls.Add(Me.lblDefaultSizeEx)
        Me.TabSecond.Controls.Add(Me.lblStartColorEx)
        Me.TabSecond.Controls.Add(Me.lblDefaultColorEx)
        Me.TabSecond.Controls.Add(Me.lblStartSizeEx)
        Me.TabSecond.Controls.Add(Me.lblEndSizeEx)
        Me.TabSecond.Controls.Add(Me.lblMinValueEx)
        Me.TabSecond.Controls.Add(Me.lblEndColorEx)
        Me.TabSecond.Controls.Add(Me.lblMaxValueEx)
        Me.TabSecond.Controls.Add(Me.RenderEndColorEx)
        Me.TabSecond.Controls.Add(Me.RenderStartColorEx)
        Me.TabSecond.Location = New System.Drawing.Point(4, 22)
        Me.TabSecond.Name = "TabSecond"
        Me.TabSecond.Padding = New System.Windows.Forms.Padding(3)
        Me.TabSecond.Size = New System.Drawing.Size(270, 147)
        Me.TabSecond.TabIndex = 1
        Me.TabSecond.Text = "TabSecond"
        Me.TabSecond.UseVisualStyleBackColor = True
        '
        'RenderDefaultColorEx
        '
        Me.RenderDefaultColorEx.Automatic = "Automatic"
        Me.RenderDefaultColorEx.Color = System.Drawing.Color.Transparent
        Me.RenderDefaultColorEx.Location = New System.Drawing.Point(187, 71)
        Me.RenderDefaultColorEx.MoreColors = "More Colors..."
        Me.RenderDefaultColorEx.Name = "RenderDefaultColorEx"
        Me.RenderDefaultColorEx.Size = New System.Drawing.Size(72, 23)
        Me.RenderDefaultColorEx.TabIndex = 8
        Me.RenderDefaultColorEx.UseVisualStyleBackColor = True
        '
        'RenderDefaultSizeEx
        '
        Me.RenderDefaultSizeEx.Location = New System.Drawing.Point(187, 122)
        Me.RenderDefaultSizeEx.Maximum = New Decimal(New Integer() {50000, 0, 0, 0})
        Me.RenderDefaultSizeEx.Name = "RenderDefaultSizeEx"
        Me.RenderDefaultSizeEx.Size = New System.Drawing.Size(72, 20)
        Me.RenderDefaultSizeEx.TabIndex = 11
        '
        'RenderZonesEx
        '
        Me.RenderZonesEx.Location = New System.Drawing.Point(7, 23)
        Me.RenderZonesEx.Minimum = New Decimal(New Integer() {100, 0, 0, -2147483648})
        Me.RenderZonesEx.Name = "RenderZonesEx"
        Me.RenderZonesEx.Size = New System.Drawing.Size(72, 20)
        Me.RenderZonesEx.TabIndex = 2
        '
        'RenderEndSizeEx
        '
        Me.RenderEndSizeEx.Location = New System.Drawing.Point(97, 122)
        Me.RenderEndSizeEx.Maximum = New Decimal(New Integer() {50000, 0, 0, 0})
        Me.RenderEndSizeEx.Name = "RenderEndSizeEx"
        Me.RenderEndSizeEx.Size = New System.Drawing.Size(72, 20)
        Me.RenderEndSizeEx.TabIndex = 10
        '
        'RenderStartSizeEx
        '
        Me.RenderStartSizeEx.Location = New System.Drawing.Point(7, 122)
        Me.RenderStartSizeEx.Maximum = New Decimal(New Integer() {50000, 0, 0, 0})
        Me.RenderStartSizeEx.Name = "RenderStartSizeEx"
        Me.RenderStartSizeEx.Size = New System.Drawing.Size(72, 20)
        Me.RenderStartSizeEx.TabIndex = 9
        '
        'RenderMaxValueEx
        '
        Me.RenderMaxValueEx.Location = New System.Drawing.Point(187, 23)
        Me.RenderMaxValueEx.Name = "RenderMaxValueEx"
        Me.RenderMaxValueEx.Size = New System.Drawing.Size(75, 20)
        Me.RenderMaxValueEx.TabIndex = 4
        '
        'RenderMinValueEx
        '
        Me.RenderMinValueEx.Location = New System.Drawing.Point(97, 23)
        Me.RenderMinValueEx.Name = "RenderMinValueEx"
        Me.RenderMinValueEx.Size = New System.Drawing.Size(75, 20)
        Me.RenderMinValueEx.TabIndex = 3
        '
        'lblZonesEx
        '
        Me.lblZonesEx.AutoSize = True
        Me.lblZonesEx.Location = New System.Drawing.Point(7, 7)
        Me.lblZonesEx.Name = "lblZonesEx"
        Me.lblZonesEx.Size = New System.Drawing.Size(59, 13)
        Me.lblZonesEx.TabIndex = 69
        Me.lblZonesEx.Text = "lblZonesEx"
        '
        'lblDefaultSizeEx
        '
        Me.lblDefaultSizeEx.AutoSize = True
        Me.lblDefaultSizeEx.Location = New System.Drawing.Point(187, 106)
        Me.lblDefaultSizeEx.Name = "lblDefaultSizeEx"
        Me.lblDefaultSizeEx.Size = New System.Drawing.Size(83, 13)
        Me.lblDefaultSizeEx.TabIndex = 77
        Me.lblDefaultSizeEx.Text = "lblDefaultSizeEx"
        '
        'lblStartColorEx
        '
        Me.lblStartColorEx.AutoSize = True
        Me.lblStartColorEx.Location = New System.Drawing.Point(7, 55)
        Me.lblStartColorEx.Name = "lblStartColorEx"
        Me.lblStartColorEx.Size = New System.Drawing.Size(75, 13)
        Me.lblStartColorEx.TabIndex = 71
        Me.lblStartColorEx.Text = "lblStartColorEx"
        '
        'lblDefaultColorEx
        '
        Me.lblDefaultColorEx.AutoSize = True
        Me.lblDefaultColorEx.Location = New System.Drawing.Point(187, 55)
        Me.lblDefaultColorEx.Name = "lblDefaultColorEx"
        Me.lblDefaultColorEx.Size = New System.Drawing.Size(87, 13)
        Me.lblDefaultColorEx.TabIndex = 76
        Me.lblDefaultColorEx.Text = "lblDefaultColorEx"
        '
        'lblStartSizeEx
        '
        Me.lblStartSizeEx.AutoSize = True
        Me.lblStartSizeEx.Location = New System.Drawing.Point(7, 106)
        Me.lblStartSizeEx.Name = "lblStartSizeEx"
        Me.lblStartSizeEx.Size = New System.Drawing.Size(71, 13)
        Me.lblStartSizeEx.TabIndex = 70
        Me.lblStartSizeEx.Text = "lblStartSizeEx"
        '
        'lblEndSizeEx
        '
        Me.lblEndSizeEx.AutoSize = True
        Me.lblEndSizeEx.Location = New System.Drawing.Point(97, 106)
        Me.lblEndSizeEx.Name = "lblEndSizeEx"
        Me.lblEndSizeEx.Size = New System.Drawing.Size(68, 13)
        Me.lblEndSizeEx.TabIndex = 73
        Me.lblEndSizeEx.Text = "lblEndSizeEx"
        '
        'lblMinValueEx
        '
        Me.lblMinValueEx.AutoSize = True
        Me.lblMinValueEx.Location = New System.Drawing.Point(97, 7)
        Me.lblMinValueEx.Name = "lblMinValueEx"
        Me.lblMinValueEx.Size = New System.Drawing.Size(73, 13)
        Me.lblMinValueEx.TabIndex = 74
        Me.lblMinValueEx.Text = "lblMinValueEx"
        '
        'lblEndColorEx
        '
        Me.lblEndColorEx.AutoSize = True
        Me.lblEndColorEx.Location = New System.Drawing.Point(97, 55)
        Me.lblEndColorEx.Name = "lblEndColorEx"
        Me.lblEndColorEx.Size = New System.Drawing.Size(72, 13)
        Me.lblEndColorEx.TabIndex = 72
        Me.lblEndColorEx.Text = "lblEndColorEx"
        '
        'lblMaxValueEx
        '
        Me.lblMaxValueEx.AutoSize = True
        Me.lblMaxValueEx.Location = New System.Drawing.Point(187, 7)
        Me.lblMaxValueEx.Name = "lblMaxValueEx"
        Me.lblMaxValueEx.Size = New System.Drawing.Size(76, 13)
        Me.lblMaxValueEx.TabIndex = 75
        Me.lblMaxValueEx.Text = "lblMaxValueEx"
        '
        'RenderEndColorEx
        '
        Me.RenderEndColorEx.Automatic = "Automatic"
        Me.RenderEndColorEx.Color = System.Drawing.Color.Transparent
        Me.RenderEndColorEx.Location = New System.Drawing.Point(97, 71)
        Me.RenderEndColorEx.MoreColors = "More Colors..."
        Me.RenderEndColorEx.Name = "RenderEndColorEx"
        Me.RenderEndColorEx.Size = New System.Drawing.Size(72, 23)
        Me.RenderEndColorEx.TabIndex = 7
        Me.RenderEndColorEx.UseVisualStyleBackColor = True
        '
        'RenderStartColorEx
        '
        Me.RenderStartColorEx.Automatic = "Automatic"
        Me.RenderStartColorEx.Color = System.Drawing.Color.Transparent
        Me.RenderStartColorEx.Location = New System.Drawing.Point(7, 71)
        Me.RenderStartColorEx.MoreColors = "More Colors..."
        Me.RenderStartColorEx.Name = "RenderStartColorEx"
        Me.RenderStartColorEx.Size = New System.Drawing.Size(72, 23)
        Me.RenderStartColorEx.TabIndex = 6
        Me.RenderStartColorEx.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(9, 62)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(416, 1)
        Me.Label1.TabIndex = 58
        '
        'lblField
        '
        Me.lblField.AutoSize = True
        Me.lblField.Location = New System.Drawing.Point(6, 12)
        Me.lblField.Name = "lblField"
        Me.lblField.Size = New System.Drawing.Size(39, 13)
        Me.lblField.TabIndex = 38
        Me.lblField.Text = "lblField"
        '
        'TabArea
        '
        Me.TabArea.Controls.Add(Me.AreaOutlineStyle)
        Me.TabArea.Controls.Add(Me.lblAreaOutlineStyle)
        Me.TabArea.Controls.Add(Me.chkAreaOutlineWidthUR)
        Me.TabArea.Controls.Add(Me.AreaOutlineWidth)
        Me.TabArea.Controls.Add(Me.lblAreaOutlineWidth)
        Me.TabArea.Controls.Add(Me.AreaPattern)
        Me.TabArea.Controls.Add(Me.chkAreaInLegend)
        Me.TabArea.Controls.Add(Me.Label2)
        Me.TabArea.Controls.Add(Me.chkAreaOutlineColorUR)
        Me.TabArea.Controls.Add(Me.lblAreaPattern)
        Me.TabArea.Controls.Add(Me.lblAreaColor)
        Me.TabArea.Controls.Add(Me.lblAreaOutlineColor)
        Me.TabArea.Controls.Add(Me.chkAreaColorUR)
        Me.TabArea.Controls.Add(Me.AreaColor)
        Me.TabArea.Controls.Add(Me.AreaOutlineColor)
        Me.TabArea.Location = New System.Drawing.Point(4, 22)
        Me.TabArea.Name = "TabArea"
        Me.TabArea.Padding = New System.Windows.Forms.Padding(3)
        Me.TabArea.Size = New System.Drawing.Size(435, 251)
        Me.TabArea.TabIndex = 2
        Me.TabArea.Text = "TabArea"
        Me.TabArea.UseVisualStyleBackColor = True
        '
        'AreaOutlineStyle
        '
        Me.AreaOutlineStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.AreaOutlineStyle.FormattingEnabled = True
        Me.AreaOutlineStyle.Location = New System.Drawing.Point(9, 103)
        Me.AreaOutlineStyle.Name = "AreaOutlineStyle"
        Me.AreaOutlineStyle.Size = New System.Drawing.Size(134, 21)
        Me.AreaOutlineStyle.TabIndex = 3
        '
        'lblAreaOutlineStyle
        '
        Me.lblAreaOutlineStyle.AutoSize = True
        Me.lblAreaOutlineStyle.Location = New System.Drawing.Point(6, 87)
        Me.lblAreaOutlineStyle.Name = "lblAreaOutlineStyle"
        Me.lblAreaOutlineStyle.Size = New System.Drawing.Size(95, 13)
        Me.lblAreaOutlineStyle.TabIndex = 48
        Me.lblAreaOutlineStyle.Text = "lblAreaOutlineStyle"
        '
        'chkAreaOutlineWidthUR
        '
        Me.chkAreaOutlineWidthUR.AutoSize = True
        Me.chkAreaOutlineWidthUR.Location = New System.Drawing.Point(264, 132)
        Me.chkAreaOutlineWidthUR.Name = "chkAreaOutlineWidthUR"
        Me.chkAreaOutlineWidthUR.Size = New System.Drawing.Size(143, 17)
        Me.chkAreaOutlineWidthUR.TabIndex = 7
        Me.chkAreaOutlineWidthUR.Text = "chkAreaOutlineWidthUR"
        Me.chkAreaOutlineWidthUR.UseVisualStyleBackColor = True
        '
        'AreaOutlineWidth
        '
        Me.AreaOutlineWidth.Location = New System.Drawing.Point(264, 103)
        Me.AreaOutlineWidth.Maximum = New Decimal(New Integer() {50000, 0, 0, 0})
        Me.AreaOutlineWidth.Name = "AreaOutlineWidth"
        Me.AreaOutlineWidth.Size = New System.Drawing.Size(79, 20)
        Me.AreaOutlineWidth.TabIndex = 6
        '
        'lblAreaOutlineWidth
        '
        Me.lblAreaOutlineWidth.AutoSize = True
        Me.lblAreaOutlineWidth.Location = New System.Drawing.Point(261, 86)
        Me.lblAreaOutlineWidth.Name = "lblAreaOutlineWidth"
        Me.lblAreaOutlineWidth.Size = New System.Drawing.Size(100, 13)
        Me.lblAreaOutlineWidth.TabIndex = 46
        Me.lblAreaOutlineWidth.Text = "lblAreaOutlineWidth"
        '
        'AreaPattern
        '
        Me.AreaPattern.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.AreaPattern.FormattingEnabled = True
        Me.AreaPattern.Location = New System.Drawing.Point(9, 28)
        Me.AreaPattern.Name = "AreaPattern"
        Me.AreaPattern.Size = New System.Drawing.Size(134, 21)
        Me.AreaPattern.TabIndex = 1
        '
        'chkAreaInLegend
        '
        Me.chkAreaInLegend.AutoSize = True
        Me.chkAreaInLegend.Location = New System.Drawing.Point(9, 169)
        Me.chkAreaInLegend.Name = "chkAreaInLegend"
        Me.chkAreaInLegend.Size = New System.Drawing.Size(111, 17)
        Me.chkAreaInLegend.TabIndex = 8
        Me.chkAreaInLegend.Text = "chkAreaInLegend"
        Me.chkAreaInLegend.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(9, 161)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(416, 1)
        Me.Label2.TabIndex = 42
        '
        'chkAreaOutlineColorUR
        '
        Me.chkAreaOutlineColorUR.AutoSize = True
        Me.chkAreaOutlineColorUR.Location = New System.Drawing.Point(149, 132)
        Me.chkAreaOutlineColorUR.Name = "chkAreaOutlineColorUR"
        Me.chkAreaOutlineColorUR.Size = New System.Drawing.Size(139, 17)
        Me.chkAreaOutlineColorUR.TabIndex = 5
        Me.chkAreaOutlineColorUR.Text = "chkAreaOutlineColorUR"
        Me.chkAreaOutlineColorUR.UseVisualStyleBackColor = True
        '
        'lblAreaPattern
        '
        Me.lblAreaPattern.AutoSize = True
        Me.lblAreaPattern.Location = New System.Drawing.Point(6, 12)
        Me.lblAreaPattern.Name = "lblAreaPattern"
        Me.lblAreaPattern.Size = New System.Drawing.Size(73, 13)
        Me.lblAreaPattern.TabIndex = 40
        Me.lblAreaPattern.Text = "lblAreaPattern"
        '
        'AreaColor
        '
        Me.AreaColor.Automatic = "Automatic"
        Me.AreaColor.Color = System.Drawing.Color.Transparent
        Me.AreaColor.Location = New System.Drawing.Point(149, 28)
        Me.AreaColor.MoreColors = "More Colors..."
        Me.AreaColor.Name = "AreaColor"
        Me.AreaColor.Size = New System.Drawing.Size(75, 23)
        Me.AreaColor.TabIndex = 2
        Me.AreaColor.UseVisualStyleBackColor = True
        '
        'AreaOutlineColor
        '
        Me.AreaOutlineColor.Automatic = "Automatic"
        Me.AreaOutlineColor.Color = System.Drawing.Color.Transparent
        Me.AreaOutlineColor.Location = New System.Drawing.Point(149, 103)
        Me.AreaOutlineColor.MoreColors = "More Colors..."
        Me.AreaOutlineColor.Name = "AreaOutlineColor"
        Me.AreaOutlineColor.Size = New System.Drawing.Size(75, 23)
        Me.AreaOutlineColor.TabIndex = 4
        Me.AreaOutlineColor.UseVisualStyleBackColor = True
        '
        'TabLine
        '
        Me.TabLine.Controls.Add(Me.LineOutlineStyle)
        Me.TabLine.Controls.Add(Me.chkLineOutlineWidthUR)
        Me.TabLine.Controls.Add(Me.chkLineOutlineColorUR)
        Me.TabLine.Controls.Add(Me.LineOutlineWidth)
        Me.TabLine.Controls.Add(Me.lblLineOutlineWidth)
        Me.TabLine.Controls.Add(Me.lblLineOutlineStyle)
        Me.TabLine.Controls.Add(Me.lblLineOutlineColor)
        Me.TabLine.Controls.Add(Me.LineStyle)
        Me.TabLine.Controls.Add(Me.chkLineInLegend)
        Me.TabLine.Controls.Add(Me.Label3)
        Me.TabLine.Controls.Add(Me.chkLineWidthUR)
        Me.TabLine.Controls.Add(Me.chkLineColorUR)
        Me.TabLine.Controls.Add(Me.LineWidth)
        Me.TabLine.Controls.Add(Me.lblLineWidth)
        Me.TabLine.Controls.Add(Me.lblLineStyle)
        Me.TabLine.Controls.Add(Me.lblLineColor)
        Me.TabLine.Controls.Add(Me.LineOutlineColor)
        Me.TabLine.Controls.Add(Me.LineColor)
        Me.TabLine.Location = New System.Drawing.Point(4, 22)
        Me.TabLine.Name = "TabLine"
        Me.TabLine.Size = New System.Drawing.Size(435, 251)
        Me.TabLine.TabIndex = 3
        Me.TabLine.Text = "TabLine"
        Me.TabLine.UseVisualStyleBackColor = True
        '
        'LineOutlineStyle
        '
        Me.LineOutlineStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.LineOutlineStyle.FormattingEnabled = True
        Me.LineOutlineStyle.Location = New System.Drawing.Point(9, 103)
        Me.LineOutlineStyle.Name = "LineOutlineStyle"
        Me.LineOutlineStyle.Size = New System.Drawing.Size(134, 21)
        Me.LineOutlineStyle.TabIndex = 6
        '
        'chkLineOutlineWidthUR
        '
        Me.chkLineOutlineWidthUR.AutoSize = True
        Me.chkLineOutlineWidthUR.Location = New System.Drawing.Point(264, 132)
        Me.chkLineOutlineWidthUR.Name = "chkLineOutlineWidthUR"
        Me.chkLineOutlineWidthUR.Size = New System.Drawing.Size(141, 17)
        Me.chkLineOutlineWidthUR.TabIndex = 10
        Me.chkLineOutlineWidthUR.Text = "chkLineOutlineWidthUR"
        Me.chkLineOutlineWidthUR.UseVisualStyleBackColor = True
        '
        'chkLineOutlineColorUR
        '
        Me.chkLineOutlineColorUR.AutoSize = True
        Me.chkLineOutlineColorUR.Location = New System.Drawing.Point(149, 132)
        Me.chkLineOutlineColorUR.Name = "chkLineOutlineColorUR"
        Me.chkLineOutlineColorUR.Size = New System.Drawing.Size(137, 17)
        Me.chkLineOutlineColorUR.TabIndex = 8
        Me.chkLineOutlineColorUR.Text = "chkLineOutlineColorUR"
        Me.chkLineOutlineColorUR.UseVisualStyleBackColor = True
        '
        'LineOutlineWidth
        '
        Me.LineOutlineWidth.Location = New System.Drawing.Point(264, 103)
        Me.LineOutlineWidth.Maximum = New Decimal(New Integer() {50000, 0, 0, 0})
        Me.LineOutlineWidth.Name = "LineOutlineWidth"
        Me.LineOutlineWidth.Size = New System.Drawing.Size(79, 20)
        Me.LineOutlineWidth.TabIndex = 9
        '
        'lblLineOutlineWidth
        '
        Me.lblLineOutlineWidth.AutoSize = True
        Me.lblLineOutlineWidth.Location = New System.Drawing.Point(261, 86)
        Me.lblLineOutlineWidth.Name = "lblLineOutlineWidth"
        Me.lblLineOutlineWidth.Size = New System.Drawing.Size(98, 13)
        Me.lblLineOutlineWidth.TabIndex = 55
        Me.lblLineOutlineWidth.Text = "lblLineOutlineWidth"
        '
        'lblLineOutlineStyle
        '
        Me.lblLineOutlineStyle.AutoSize = True
        Me.lblLineOutlineStyle.Location = New System.Drawing.Point(6, 87)
        Me.lblLineOutlineStyle.Name = "lblLineOutlineStyle"
        Me.lblLineOutlineStyle.Size = New System.Drawing.Size(93, 13)
        Me.lblLineOutlineStyle.TabIndex = 54
        Me.lblLineOutlineStyle.Text = "lblLineOutlineStyle"
        '
        'lblLineOutlineColor
        '
        Me.lblLineOutlineColor.AutoSize = True
        Me.lblLineOutlineColor.Location = New System.Drawing.Point(146, 86)
        Me.lblLineOutlineColor.Name = "lblLineOutlineColor"
        Me.lblLineOutlineColor.Size = New System.Drawing.Size(94, 13)
        Me.lblLineOutlineColor.TabIndex = 53
        Me.lblLineOutlineColor.Text = "lblLineOutlineColor"
        '
        'LineStyle
        '
        Me.LineStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.LineStyle.FormattingEnabled = True
        Me.LineStyle.Location = New System.Drawing.Point(9, 28)
        Me.LineStyle.Name = "LineStyle"
        Me.LineStyle.Size = New System.Drawing.Size(134, 21)
        Me.LineStyle.TabIndex = 1
        '
        'chkLineInLegend
        '
        Me.chkLineInLegend.AutoSize = True
        Me.chkLineInLegend.Location = New System.Drawing.Point(9, 169)
        Me.chkLineInLegend.Name = "chkLineInLegend"
        Me.chkLineInLegend.Size = New System.Drawing.Size(109, 17)
        Me.chkLineInLegend.TabIndex = 11
        Me.chkLineInLegend.Text = "chkLineInLegend"
        Me.chkLineInLegend.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(9, 161)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(416, 1)
        Me.Label3.TabIndex = 47
        '
        'chkLineWidthUR
        '
        Me.chkLineWidthUR.AutoSize = True
        Me.chkLineWidthUR.Location = New System.Drawing.Point(264, 57)
        Me.chkLineWidthUR.Name = "chkLineWidthUR"
        Me.chkLineWidthUR.Size = New System.Drawing.Size(108, 17)
        Me.chkLineWidthUR.TabIndex = 5
        Me.chkLineWidthUR.Text = "chkLineWidthUR"
        Me.chkLineWidthUR.UseVisualStyleBackColor = True
        '
        'chkLineColorUR
        '
        Me.chkLineColorUR.AutoSize = True
        Me.chkLineColorUR.Location = New System.Drawing.Point(149, 57)
        Me.chkLineColorUR.Name = "chkLineColorUR"
        Me.chkLineColorUR.Size = New System.Drawing.Size(104, 17)
        Me.chkLineColorUR.TabIndex = 3
        Me.chkLineColorUR.Text = "chkLineColorUR"
        Me.chkLineColorUR.UseVisualStyleBackColor = True
        '
        'LineWidth
        '
        Me.LineWidth.Location = New System.Drawing.Point(264, 28)
        Me.LineWidth.Maximum = New Decimal(New Integer() {50000, 0, 0, 0})
        Me.LineWidth.Name = "LineWidth"
        Me.LineWidth.Size = New System.Drawing.Size(79, 20)
        Me.LineWidth.TabIndex = 4
        '
        'lblLineWidth
        '
        Me.lblLineWidth.AutoSize = True
        Me.lblLineWidth.Location = New System.Drawing.Point(261, 11)
        Me.lblLineWidth.Name = "lblLineWidth"
        Me.lblLineWidth.Size = New System.Drawing.Size(65, 13)
        Me.lblLineWidth.TabIndex = 43
        Me.lblLineWidth.Text = "lblLineWidth"
        '
        'lblLineStyle
        '
        Me.lblLineStyle.AutoSize = True
        Me.lblLineStyle.Location = New System.Drawing.Point(6, 12)
        Me.lblLineStyle.Name = "lblLineStyle"
        Me.lblLineStyle.Size = New System.Drawing.Size(60, 13)
        Me.lblLineStyle.TabIndex = 42
        Me.lblLineStyle.Text = "lblLineStyle"
        '
        'lblLineColor
        '
        Me.lblLineColor.AutoSize = True
        Me.lblLineColor.Location = New System.Drawing.Point(146, 11)
        Me.lblLineColor.Name = "lblLineColor"
        Me.lblLineColor.Size = New System.Drawing.Size(61, 13)
        Me.lblLineColor.TabIndex = 9
        Me.lblLineColor.Text = "lblLineColor"
        '
        'LineOutlineColor
        '
        Me.LineOutlineColor.Automatic = "Automatic"
        Me.LineOutlineColor.Color = System.Drawing.Color.Transparent
        Me.LineOutlineColor.Location = New System.Drawing.Point(149, 103)
        Me.LineOutlineColor.MoreColors = "More Colors..."
        Me.LineOutlineColor.Name = "LineOutlineColor"
        Me.LineOutlineColor.Size = New System.Drawing.Size(75, 23)
        Me.LineOutlineColor.TabIndex = 7
        Me.LineOutlineColor.UseVisualStyleBackColor = True
        '
        'LineColor
        '
        Me.LineColor.Automatic = "Automatic"
        Me.LineColor.Color = System.Drawing.Color.Transparent
        Me.LineColor.Location = New System.Drawing.Point(149, 28)
        Me.LineColor.MoreColors = "More Colors..."
        Me.LineColor.Name = "LineColor"
        Me.LineColor.Size = New System.Drawing.Size(75, 23)
        Me.LineColor.TabIndex = 2
        Me.LineColor.UseVisualStyleBackColor = True
        '
        'TabMarker
        '
        Me.TabMarker.Controls.Add(Me.btnMarkerSymbol)
        Me.TabMarker.Controls.Add(Me.MarkerOutlineStyle)
        Me.TabMarker.Controls.Add(Me.chkMarkerOutlineWidthUR)
        Me.TabMarker.Controls.Add(Me.chkMarkerOutlineColorUR)
        Me.TabMarker.Controls.Add(Me.MarkerOutlineWidth)
        Me.TabMarker.Controls.Add(Me.lblMarkerOutlineWidth)
        Me.TabMarker.Controls.Add(Me.lblMarkerOutlineStyle)
        Me.TabMarker.Controls.Add(Me.lblMarkerOutlineColor)
        Me.TabMarker.Controls.Add(Me.MarkerStyle)
        Me.TabMarker.Controls.Add(Me.chkMarkerInLegend)
        Me.TabMarker.Controls.Add(Me.Label4)
        Me.TabMarker.Controls.Add(Me.chkMarkerSizeUR)
        Me.TabMarker.Controls.Add(Me.chkMarkerColorUR)
        Me.TabMarker.Controls.Add(Me.MarkerSize)
        Me.TabMarker.Controls.Add(Me.lblMarkerSize)
        Me.TabMarker.Controls.Add(Me.lblMarkerStyle)
        Me.TabMarker.Controls.Add(Me.lblMarkerColor)
        Me.TabMarker.Controls.Add(Me.MarkerOutlineColor)
        Me.TabMarker.Controls.Add(Me.MarkerColor)
        Me.TabMarker.Location = New System.Drawing.Point(4, 22)
        Me.TabMarker.Name = "TabMarker"
        Me.TabMarker.Size = New System.Drawing.Size(435, 251)
        Me.TabMarker.TabIndex = 4
        Me.TabMarker.Text = "TabMarker"
        Me.TabMarker.UseVisualStyleBackColor = True
        '
        'btnMarkerSymbol
        '
        Me.btnMarkerSymbol.Location = New System.Drawing.Point(9, 51)
        Me.btnMarkerSymbol.Name = "btnMarkerSymbol"
        Me.btnMarkerSymbol.Size = New System.Drawing.Size(66, 23)
        Me.btnMarkerSymbol.TabIndex = 67
        Me.btnMarkerSymbol.Text = "btnMarkerSymbol"
        Me.btnMarkerSymbol.UseVisualStyleBackColor = True
        '
        'MarkerOutlineStyle
        '
        Me.MarkerOutlineStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.MarkerOutlineStyle.FormattingEnabled = True
        Me.MarkerOutlineStyle.Location = New System.Drawing.Point(9, 103)
        Me.MarkerOutlineStyle.Name = "MarkerOutlineStyle"
        Me.MarkerOutlineStyle.Size = New System.Drawing.Size(134, 21)
        Me.MarkerOutlineStyle.TabIndex = 6
        '
        'chkMarkerOutlineWidthUR
        '
        Me.chkMarkerOutlineWidthUR.AutoSize = True
        Me.chkMarkerOutlineWidthUR.Location = New System.Drawing.Point(264, 132)
        Me.chkMarkerOutlineWidthUR.Name = "chkMarkerOutlineWidthUR"
        Me.chkMarkerOutlineWidthUR.Size = New System.Drawing.Size(154, 17)
        Me.chkMarkerOutlineWidthUR.TabIndex = 10
        Me.chkMarkerOutlineWidthUR.Text = "chkMarkerOutlineWidthUR"
        Me.chkMarkerOutlineWidthUR.UseVisualStyleBackColor = True
        '
        'chkMarkerOutlineColorUR
        '
        Me.chkMarkerOutlineColorUR.AutoSize = True
        Me.chkMarkerOutlineColorUR.Location = New System.Drawing.Point(149, 132)
        Me.chkMarkerOutlineColorUR.Name = "chkMarkerOutlineColorUR"
        Me.chkMarkerOutlineColorUR.Size = New System.Drawing.Size(150, 17)
        Me.chkMarkerOutlineColorUR.TabIndex = 8
        Me.chkMarkerOutlineColorUR.Text = "chkMarkerOutlineColorUR"
        Me.chkMarkerOutlineColorUR.UseVisualStyleBackColor = True
        '
        'MarkerOutlineWidth
        '
        Me.MarkerOutlineWidth.Location = New System.Drawing.Point(264, 103)
        Me.MarkerOutlineWidth.Maximum = New Decimal(New Integer() {50000, 0, 0, 0})
        Me.MarkerOutlineWidth.Name = "MarkerOutlineWidth"
        Me.MarkerOutlineWidth.Size = New System.Drawing.Size(79, 20)
        Me.MarkerOutlineWidth.TabIndex = 9
        '
        'lblMarkerOutlineWidth
        '
        Me.lblMarkerOutlineWidth.AutoSize = True
        Me.lblMarkerOutlineWidth.Location = New System.Drawing.Point(261, 86)
        Me.lblMarkerOutlineWidth.Name = "lblMarkerOutlineWidth"
        Me.lblMarkerOutlineWidth.Size = New System.Drawing.Size(111, 13)
        Me.lblMarkerOutlineWidth.TabIndex = 65
        Me.lblMarkerOutlineWidth.Text = "lblMarkerOutlineWidth"
        '
        'lblMarkerOutlineStyle
        '
        Me.lblMarkerOutlineStyle.AutoSize = True
        Me.lblMarkerOutlineStyle.Location = New System.Drawing.Point(6, 87)
        Me.lblMarkerOutlineStyle.Name = "lblMarkerOutlineStyle"
        Me.lblMarkerOutlineStyle.Size = New System.Drawing.Size(106, 13)
        Me.lblMarkerOutlineStyle.TabIndex = 64
        Me.lblMarkerOutlineStyle.Text = "lblMarkerOutlineStyle"
        '
        'lblMarkerOutlineColor
        '
        Me.lblMarkerOutlineColor.AutoSize = True
        Me.lblMarkerOutlineColor.Location = New System.Drawing.Point(146, 86)
        Me.lblMarkerOutlineColor.Name = "lblMarkerOutlineColor"
        Me.lblMarkerOutlineColor.Size = New System.Drawing.Size(107, 13)
        Me.lblMarkerOutlineColor.TabIndex = 63
        Me.lblMarkerOutlineColor.Text = "lblMarkerOutlineColor"
        '
        'MarkerStyle
        '
        Me.MarkerStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.MarkerStyle.FormattingEnabled = True
        Me.MarkerStyle.Location = New System.Drawing.Point(9, 28)
        Me.MarkerStyle.Name = "MarkerStyle"
        Me.MarkerStyle.Size = New System.Drawing.Size(134, 21)
        Me.MarkerStyle.TabIndex = 1
        '
        'chkMarkerInLegend
        '
        Me.chkMarkerInLegend.AutoSize = True
        Me.chkMarkerInLegend.Location = New System.Drawing.Point(9, 169)
        Me.chkMarkerInLegend.Name = "chkMarkerInLegend"
        Me.chkMarkerInLegend.Size = New System.Drawing.Size(122, 17)
        Me.chkMarkerInLegend.TabIndex = 11
        Me.chkMarkerInLegend.Text = "chkMarkerInLegend"
        Me.chkMarkerInLegend.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(9, 161)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(416, 1)
        Me.Label4.TabIndex = 57
        '
        'chkMarkerSizeUR
        '
        Me.chkMarkerSizeUR.AutoSize = True
        Me.chkMarkerSizeUR.Location = New System.Drawing.Point(264, 57)
        Me.chkMarkerSizeUR.Name = "chkMarkerSizeUR"
        Me.chkMarkerSizeUR.Size = New System.Drawing.Size(113, 17)
        Me.chkMarkerSizeUR.TabIndex = 5
        Me.chkMarkerSizeUR.Text = "chkMarkerSizeUR"
        Me.chkMarkerSizeUR.UseVisualStyleBackColor = True
        '
        'chkMarkerColorUR
        '
        Me.chkMarkerColorUR.AutoSize = True
        Me.chkMarkerColorUR.Location = New System.Drawing.Point(149, 57)
        Me.chkMarkerColorUR.Name = "chkMarkerColorUR"
        Me.chkMarkerColorUR.Size = New System.Drawing.Size(117, 17)
        Me.chkMarkerColorUR.TabIndex = 3
        Me.chkMarkerColorUR.Text = "chkMarkerColorUR"
        Me.chkMarkerColorUR.UseVisualStyleBackColor = True
        '
        'MarkerSize
        '
        Me.MarkerSize.Location = New System.Drawing.Point(264, 28)
        Me.MarkerSize.Maximum = New Decimal(New Integer() {50000, 0, 0, 0})
        Me.MarkerSize.Name = "MarkerSize"
        Me.MarkerSize.Size = New System.Drawing.Size(79, 20)
        Me.MarkerSize.TabIndex = 4
        '
        'lblMarkerSize
        '
        Me.lblMarkerSize.AutoSize = True
        Me.lblMarkerSize.Location = New System.Drawing.Point(261, 11)
        Me.lblMarkerSize.Name = "lblMarkerSize"
        Me.lblMarkerSize.Size = New System.Drawing.Size(70, 13)
        Me.lblMarkerSize.TabIndex = 53
        Me.lblMarkerSize.Text = "lblMarkerSize"
        '
        'lblMarkerStyle
        '
        Me.lblMarkerStyle.AutoSize = True
        Me.lblMarkerStyle.Location = New System.Drawing.Point(6, 12)
        Me.lblMarkerStyle.Name = "lblMarkerStyle"
        Me.lblMarkerStyle.Size = New System.Drawing.Size(73, 13)
        Me.lblMarkerStyle.TabIndex = 52
        Me.lblMarkerStyle.Text = "lblMarkerStyle"
        '
        'lblMarkerColor
        '
        Me.lblMarkerColor.AutoSize = True
        Me.lblMarkerColor.Location = New System.Drawing.Point(146, 11)
        Me.lblMarkerColor.Name = "lblMarkerColor"
        Me.lblMarkerColor.Size = New System.Drawing.Size(74, 13)
        Me.lblMarkerColor.TabIndex = 50
        Me.lblMarkerColor.Text = "lblMarkerColor"
        '
        'MarkerOutlineColor
        '
        Me.MarkerOutlineColor.Automatic = "Automatic"
        Me.MarkerOutlineColor.Color = System.Drawing.Color.Transparent
        Me.MarkerOutlineColor.Location = New System.Drawing.Point(149, 103)
        Me.MarkerOutlineColor.MoreColors = "More Colors..."
        Me.MarkerOutlineColor.Name = "MarkerOutlineColor"
        Me.MarkerOutlineColor.Size = New System.Drawing.Size(75, 23)
        Me.MarkerOutlineColor.TabIndex = 7
        Me.MarkerOutlineColor.UseVisualStyleBackColor = True
        '
        'MarkerColor
        '
        Me.MarkerColor.Automatic = "Automatic"
        Me.MarkerColor.Color = System.Drawing.Color.Transparent
        Me.MarkerColor.Location = New System.Drawing.Point(149, 28)
        Me.MarkerColor.MoreColors = "More Colors..."
        Me.MarkerColor.Name = "MarkerColor"
        Me.MarkerColor.Size = New System.Drawing.Size(75, 23)
        Me.MarkerColor.TabIndex = 2
        Me.MarkerColor.UseVisualStyleBackColor = True
        '
        'TabLabel
        '
        Me.TabLabel.Controls.Add(Me.lblLabelInfo)
        Me.TabLabel.Controls.Add(Me.uxLabelPosition)
        Me.TabLabel.Controls.Add(Me.LabelRotation)
        Me.TabLabel.Controls.Add(Me.lblLabelRotation)
        Me.TabLabel.Controls.Add(Me.chkLabelColorUR)
        Me.TabLabel.Controls.Add(Me.LabelField)
        Me.TabLabel.Controls.Add(Me.chkLabelVisible)
        Me.TabLabel.Controls.Add(Me.chkLabelInLegend)
        Me.TabLabel.Controls.Add(Me.Label5)
        Me.TabLabel.Controls.Add(Me.LabelValue)
        Me.TabLabel.Controls.Add(Me.lblLabelValue)
        Me.TabLabel.Controls.Add(Me.lblLabelField)
        Me.TabLabel.Controls.Add(Me.lblLabelColor)
        Me.TabLabel.Controls.Add(Me.lblLabelFont)
        Me.TabLabel.Controls.Add(Me.btnLabelFont)
        Me.TabLabel.Controls.Add(Me.LabelColor)
        Me.TabLabel.Location = New System.Drawing.Point(4, 22)
        Me.TabLabel.Name = "TabLabel"
        Me.TabLabel.Size = New System.Drawing.Size(435, 251)
        Me.TabLabel.TabIndex = 5
        Me.TabLabel.Text = "TabLabel"
        Me.TabLabel.UseVisualStyleBackColor = True
        '
        'lblLabelInfo
        '
        Me.lblLabelInfo.Location = New System.Drawing.Point(171, 103)
        Me.lblLabelInfo.Name = "lblLabelInfo"
        Me.lblLabelInfo.Size = New System.Drawing.Size(16, 16)
        Me.lblLabelInfo.TabIndex = 65
        '
        'uxLabelPosition
        '
        Me.uxLabelPosition.Controls.Add(Me.uxLabelPos9)
        Me.uxLabelPosition.Controls.Add(Me.uxLabelPos8)
        Me.uxLabelPosition.Controls.Add(Me.uxLabelPos7)
        Me.uxLabelPosition.Controls.Add(Me.uxLabelPos6)
        Me.uxLabelPosition.Controls.Add(Me.uxLabelPos5)
        Me.uxLabelPosition.Controls.Add(Me.uxLabelPos4)
        Me.uxLabelPosition.Controls.Add(Me.uxLabelPos3)
        Me.uxLabelPosition.Controls.Add(Me.uxLabelPos2)
        Me.uxLabelPosition.Controls.Add(Me.uxLabelPos1)
        Me.uxLabelPosition.Location = New System.Drawing.Point(197, 12)
        Me.uxLabelPosition.Name = "uxLabelPosition"
        Me.uxLabelPosition.Size = New System.Drawing.Size(120, 93)
        Me.uxLabelPosition.TabIndex = 63
        Me.uxLabelPosition.TabStop = False
        Me.uxLabelPosition.Text = "uxLabelPosition"
        '
        'uxLabelPos9
        '
        Me.uxLabelPos9.AutoSize = True
        Me.uxLabelPos9.Location = New System.Drawing.Point(82, 66)
        Me.uxLabelPos9.Name = "uxLabelPos9"
        Me.uxLabelPos9.Size = New System.Drawing.Size(32, 17)
        Me.uxLabelPos9.TabIndex = 8
        Me.uxLabelPos9.Text = "9"
        Me.uxLabelPos9.UseVisualStyleBackColor = True
        '
        'uxLabelPos8
        '
        Me.uxLabelPos8.AutoSize = True
        Me.uxLabelPos8.Location = New System.Drawing.Point(44, 66)
        Me.uxLabelPos8.Name = "uxLabelPos8"
        Me.uxLabelPos8.Size = New System.Drawing.Size(32, 17)
        Me.uxLabelPos8.TabIndex = 7
        Me.uxLabelPos8.Text = "8"
        Me.uxLabelPos8.UseVisualStyleBackColor = True
        '
        'uxLabelPos7
        '
        Me.uxLabelPos7.AutoSize = True
        Me.uxLabelPos7.Location = New System.Drawing.Point(6, 66)
        Me.uxLabelPos7.Name = "uxLabelPos7"
        Me.uxLabelPos7.Size = New System.Drawing.Size(32, 17)
        Me.uxLabelPos7.TabIndex = 6
        Me.uxLabelPos7.Text = "7"
        Me.uxLabelPos7.UseVisualStyleBackColor = True
        '
        'uxLabelPos6
        '
        Me.uxLabelPos6.AutoSize = True
        Me.uxLabelPos6.Location = New System.Drawing.Point(82, 43)
        Me.uxLabelPos6.Name = "uxLabelPos6"
        Me.uxLabelPos6.Size = New System.Drawing.Size(32, 17)
        Me.uxLabelPos6.TabIndex = 5
        Me.uxLabelPos6.Text = "6"
        Me.uxLabelPos6.UseVisualStyleBackColor = True
        '
        'uxLabelPos5
        '
        Me.uxLabelPos5.AutoSize = True
        Me.uxLabelPos5.Location = New System.Drawing.Point(44, 43)
        Me.uxLabelPos5.Name = "uxLabelPos5"
        Me.uxLabelPos5.Size = New System.Drawing.Size(32, 17)
        Me.uxLabelPos5.TabIndex = 4
        Me.uxLabelPos5.Text = "5"
        Me.uxLabelPos5.UseVisualStyleBackColor = True
        '
        'uxLabelPos4
        '
        Me.uxLabelPos4.AutoSize = True
        Me.uxLabelPos4.Location = New System.Drawing.Point(6, 43)
        Me.uxLabelPos4.Name = "uxLabelPos4"
        Me.uxLabelPos4.Size = New System.Drawing.Size(32, 17)
        Me.uxLabelPos4.TabIndex = 3
        Me.uxLabelPos4.Text = "4"
        Me.uxLabelPos4.UseVisualStyleBackColor = True
        '
        'uxLabelPos3
        '
        Me.uxLabelPos3.AutoSize = True
        Me.uxLabelPos3.Location = New System.Drawing.Point(82, 21)
        Me.uxLabelPos3.Name = "uxLabelPos3"
        Me.uxLabelPos3.Size = New System.Drawing.Size(32, 17)
        Me.uxLabelPos3.TabIndex = 2
        Me.uxLabelPos3.Text = "3"
        Me.uxLabelPos3.UseVisualStyleBackColor = True
        '
        'uxLabelPos2
        '
        Me.uxLabelPos2.AutoSize = True
        Me.uxLabelPos2.Location = New System.Drawing.Point(44, 20)
        Me.uxLabelPos2.Name = "uxLabelPos2"
        Me.uxLabelPos2.Size = New System.Drawing.Size(32, 17)
        Me.uxLabelPos2.TabIndex = 1
        Me.uxLabelPos2.Text = "2"
        Me.uxLabelPos2.UseVisualStyleBackColor = True
        '
        'uxLabelPos1
        '
        Me.uxLabelPos1.AutoSize = True
        Me.uxLabelPos1.Location = New System.Drawing.Point(6, 20)
        Me.uxLabelPos1.Name = "uxLabelPos1"
        Me.uxLabelPos1.Size = New System.Drawing.Size(32, 17)
        Me.uxLabelPos1.TabIndex = 0
        Me.uxLabelPos1.Text = "1"
        Me.uxLabelPos1.UseVisualStyleBackColor = True
        '
        'LabelRotation
        '
        Me.LabelRotation.Increment = New Decimal(New Integer() {5, 0, 0, 0})
        Me.LabelRotation.Location = New System.Drawing.Point(197, 152)
        Me.LabelRotation.Maximum = New Decimal(New Integer() {180, 0, 0, 0})
        Me.LabelRotation.Minimum = New Decimal(New Integer() {180, 0, 0, -2147483648})
        Me.LabelRotation.Name = "LabelRotation"
        Me.LabelRotation.Size = New System.Drawing.Size(120, 20)
        Me.LabelRotation.TabIndex = 62
        '
        'lblLabelRotation
        '
        Me.lblLabelRotation.AutoSize = True
        Me.lblLabelRotation.Location = New System.Drawing.Point(197, 136)
        Me.lblLabelRotation.Name = "lblLabelRotation"
        Me.lblLabelRotation.Size = New System.Drawing.Size(83, 13)
        Me.lblLabelRotation.TabIndex = 60
        Me.lblLabelRotation.Text = "lblLabelRotation"
        '
        'chkLabelColorUR
        '
        Me.chkLabelColorUR.AutoSize = True
        Me.chkLabelColorUR.Location = New System.Drawing.Point(9, 58)
        Me.chkLabelColorUR.Name = "chkLabelColorUR"
        Me.chkLabelColorUR.Size = New System.Drawing.Size(110, 17)
        Me.chkLabelColorUR.TabIndex = 2
        Me.chkLabelColorUR.Text = "chkLabelColorUR"
        Me.chkLabelColorUR.UseVisualStyleBackColor = True
        '
        'LabelField
        '
        Me.LabelField.FormattingEnabled = True
        Me.LabelField.Location = New System.Drawing.Point(9, 152)
        Me.LabelField.Name = "LabelField"
        Me.LabelField.Size = New System.Drawing.Size(160, 21)
        Me.LabelField.TabIndex = 4
        '
        'chkLabelVisible
        '
        Me.chkLabelVisible.AutoSize = True
        Me.chkLabelVisible.Location = New System.Drawing.Point(9, 218)
        Me.chkLabelVisible.Name = "chkLabelVisible"
        Me.chkLabelVisible.Size = New System.Drawing.Size(100, 17)
        Me.chkLabelVisible.TabIndex = 7
        Me.chkLabelVisible.Text = "chkLabelVisible"
        Me.chkLabelVisible.UseVisualStyleBackColor = True
        '
        'chkLabelInLegend
        '
        Me.chkLabelInLegend.AutoSize = True
        Me.chkLabelInLegend.Location = New System.Drawing.Point(9, 195)
        Me.chkLabelInLegend.Name = "chkLabelInLegend"
        Me.chkLabelInLegend.Size = New System.Drawing.Size(115, 17)
        Me.chkLabelInLegend.TabIndex = 6
        Me.chkLabelInLegend.Text = "chkLabelInLegend"
        Me.chkLabelInLegend.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(9, 187)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(416, 1)
        Me.Label5.TabIndex = 59
        '
        'LabelValue
        '
        Me.LabelValue.Location = New System.Drawing.Point(9, 101)
        Me.LabelValue.Name = "LabelValue"
        Me.LabelValue.Size = New System.Drawing.Size(160, 20)
        Me.LabelValue.TabIndex = 5
        '
        'lblLabelValue
        '
        Me.lblLabelValue.AutoSize = True
        Me.lblLabelValue.Location = New System.Drawing.Point(6, 85)
        Me.lblLabelValue.Name = "lblLabelValue"
        Me.lblLabelValue.Size = New System.Drawing.Size(70, 13)
        Me.lblLabelValue.TabIndex = 55
        Me.lblLabelValue.Text = "lblLabelValue"
        '
        'lblLabelField
        '
        Me.lblLabelField.AutoSize = True
        Me.lblLabelField.Location = New System.Drawing.Point(6, 136)
        Me.lblLabelField.Name = "lblLabelField"
        Me.lblLabelField.Size = New System.Drawing.Size(65, 13)
        Me.lblLabelField.TabIndex = 54
        Me.lblLabelField.Text = "lblLabelField"
        '
        'lblLabelColor
        '
        Me.lblLabelColor.AutoSize = True
        Me.lblLabelColor.Location = New System.Drawing.Point(6, 12)
        Me.lblLabelColor.Name = "lblLabelColor"
        Me.lblLabelColor.Size = New System.Drawing.Size(67, 13)
        Me.lblLabelColor.TabIndex = 52
        Me.lblLabelColor.Text = "lblLabelColor"
        '
        'lblLabelFont
        '
        Me.lblLabelFont.AutoSize = True
        Me.lblLabelFont.Location = New System.Drawing.Point(332, 12)
        Me.lblLabelFont.Name = "lblLabelFont"
        Me.lblLabelFont.Size = New System.Drawing.Size(38, 13)
        Me.lblLabelFont.TabIndex = 29
        Me.lblLabelFont.Text = "lblFont"
        '
        'btnLabelFont
        '
        Me.btnLabelFont.Location = New System.Drawing.Point(335, 29)
        Me.btnLabelFont.Name = "btnLabelFont"
        Me.btnLabelFont.Size = New System.Drawing.Size(75, 23)
        Me.btnLabelFont.TabIndex = 3
        Me.btnLabelFont.Text = "btnLabelFont"
        Me.btnLabelFont.UseVisualStyleBackColor = True
        '
        'LabelColor
        '
        Me.LabelColor.Automatic = "Automatic"
        Me.LabelColor.Color = System.Drawing.Color.Transparent
        Me.LabelColor.Location = New System.Drawing.Point(9, 29)
        Me.LabelColor.MoreColors = "More Colors..."
        Me.LabelColor.Name = "LabelColor"
        Me.LabelColor.Size = New System.Drawing.Size(75, 23)
        Me.LabelColor.TabIndex = 1
        Me.LabelColor.UseVisualStyleBackColor = True
        '
        'TabChart
        '
        Me.TabChart.Controls.Add(Me.Label6)
        Me.TabChart.Controls.Add(Me.chkChartSizeUR)
        Me.TabChart.Controls.Add(Me.ChartSize)
        Me.TabChart.Controls.Add(Me.lblChartSize)
        Me.TabChart.Controls.Add(Me.chkChartInLegend)
        Me.TabChart.Controls.Add(Me.ChartStyle)
        Me.TabChart.Controls.Add(Me.lblChartStyle)
        Me.TabChart.Controls.Add(Me.lblChartFields)
        Me.TabChart.Controls.Add(Me.ChartFields)
        Me.TabChart.Location = New System.Drawing.Point(4, 22)
        Me.TabChart.Name = "TabChart"
        Me.TabChart.Size = New System.Drawing.Size(435, 251)
        Me.TabChart.TabIndex = 6
        Me.TabChart.Text = "TabChart"
        Me.TabChart.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(9, 201)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(416, 1)
        Me.Label6.TabIndex = 60
        '
        'chkChartSizeUR
        '
        Me.chkChartSizeUR.AutoSize = True
        Me.chkChartSizeUR.Location = New System.Drawing.Point(168, 54)
        Me.chkChartSizeUR.Name = "chkChartSizeUR"
        Me.chkChartSizeUR.Size = New System.Drawing.Size(105, 17)
        Me.chkChartSizeUR.TabIndex = 3
        Me.chkChartSizeUR.Text = "chkChartSizeUR"
        Me.chkChartSizeUR.UseVisualStyleBackColor = True
        '
        'ChartSize
        '
        Me.ChartSize.Location = New System.Drawing.Point(168, 28)
        Me.ChartSize.Maximum = New Decimal(New Integer() {50000, 0, 0, 0})
        Me.ChartSize.Name = "ChartSize"
        Me.ChartSize.Size = New System.Drawing.Size(79, 20)
        Me.ChartSize.TabIndex = 2
        '
        'lblChartSize
        '
        Me.lblChartSize.AutoSize = True
        Me.lblChartSize.Location = New System.Drawing.Point(165, 12)
        Me.lblChartSize.Name = "lblChartSize"
        Me.lblChartSize.Size = New System.Drawing.Size(62, 13)
        Me.lblChartSize.TabIndex = 55
        Me.lblChartSize.Text = "lblChartSize"
        '
        'chkChartInLegend
        '
        Me.chkChartInLegend.AutoSize = True
        Me.chkChartInLegend.Location = New System.Drawing.Point(6, 209)
        Me.chkChartInLegend.Name = "chkChartInLegend"
        Me.chkChartInLegend.Size = New System.Drawing.Size(114, 17)
        Me.chkChartInLegend.TabIndex = 5
        Me.chkChartInLegend.Text = "chkChartInLegend"
        Me.chkChartInLegend.UseVisualStyleBackColor = True
        '
        'ChartStyle
        '
        Me.ChartStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ChartStyle.FormattingEnabled = True
        Me.ChartStyle.Location = New System.Drawing.Point(9, 28)
        Me.ChartStyle.Name = "ChartStyle"
        Me.ChartStyle.Size = New System.Drawing.Size(134, 21)
        Me.ChartStyle.TabIndex = 1
        '
        'lblChartStyle
        '
        Me.lblChartStyle.AutoSize = True
        Me.lblChartStyle.Location = New System.Drawing.Point(6, 12)
        Me.lblChartStyle.Name = "lblChartStyle"
        Me.lblChartStyle.Size = New System.Drawing.Size(65, 13)
        Me.lblChartStyle.TabIndex = 42
        Me.lblChartStyle.Text = "lblChartStyle"
        '
        'lblChartFields
        '
        Me.lblChartFields.AutoSize = True
        Me.lblChartFields.Location = New System.Drawing.Point(6, 76)
        Me.lblChartFields.Name = "lblChartFields"
        Me.lblChartFields.Size = New System.Drawing.Size(69, 13)
        Me.lblChartFields.TabIndex = 41
        Me.lblChartFields.Text = "lblChartFields"
        '
        'ChartFields
        '
        Me.ChartFields.FormattingEnabled = True
        Me.ChartFields.Location = New System.Drawing.Point(6, 93)
        Me.ChartFields.Name = "ChartFields"
        Me.ChartFields.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.ChartFields.Size = New System.Drawing.Size(236, 95)
        Me.ChartFields.TabIndex = 4
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Label7)
        Me.TabPage1.Controls.Add(Me.Label8)
        Me.TabPage1.Controls.Add(Me.Label9)
        Me.TabPage1.Controls.Add(Me.Label10)
        Me.TabPage1.Controls.Add(Me.Label11)
        Me.TabPage1.Controls.Add(Me.Label12)
        Me.TabPage1.Controls.Add(Me.Label13)
        Me.TabPage1.Controls.Add(Me.Label14)
        Me.TabPage1.Controls.Add(Me.Label15)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(270, 169)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "TabFirst"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 3)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(39, 13)
        Me.Label7.TabIndex = 34
        Me.Label7.Text = "Label7"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(186, 121)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(39, 13)
        Me.Label8.TabIndex = 56
        Me.Label8.Text = "Label8"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(6, 70)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(39, 13)
        Me.Label9.TabIndex = 36
        Me.Label9.Text = "Label9"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(186, 70)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(45, 13)
        Me.Label10.TabIndex = 55
        Me.Label10.Text = "Label10"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(6, 121)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(45, 13)
        Me.Label11.TabIndex = 35
        Me.Label11.Text = "Label11"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(96, 121)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(45, 13)
        Me.Label12.TabIndex = 47
        Me.Label12.Text = "Label12"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(96, 3)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(45, 13)
        Me.Label13.TabIndex = 50
        Me.Label13.Text = "Label13"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(96, 70)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(45, 13)
        Me.Label14.TabIndex = 46
        Me.Label14.Text = "Label14"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(186, 3)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(45, 13)
        Me.Label15.TabIndex = 52
        Me.Label15.Text = "Label15"
        '
        'TabPage2
        '
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(270, 169)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "TabSecond"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.Label16)
        Me.TabPage3.Controls.Add(Me.Label17)
        Me.TabPage3.Controls.Add(Me.Label18)
        Me.TabPage3.Controls.Add(Me.Label19)
        Me.TabPage3.Controls.Add(Me.Label20)
        Me.TabPage3.Controls.Add(Me.Label21)
        Me.TabPage3.Controls.Add(Me.Label22)
        Me.TabPage3.Controls.Add(Me.Label23)
        Me.TabPage3.Controls.Add(Me.Label24)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(270, 169)
        Me.TabPage3.TabIndex = 0
        Me.TabPage3.Text = "TabFirst"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(6, 3)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(45, 13)
        Me.Label16.TabIndex = 34
        Me.Label16.Text = "Label16"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(186, 121)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(45, 13)
        Me.Label17.TabIndex = 56
        Me.Label17.Text = "Label17"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(6, 70)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(45, 13)
        Me.Label18.TabIndex = 36
        Me.Label18.Text = "Label18"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(186, 70)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(45, 13)
        Me.Label19.TabIndex = 55
        Me.Label19.Text = "Label19"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(6, 121)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(45, 13)
        Me.Label20.TabIndex = 35
        Me.Label20.Text = "Label20"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(96, 121)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(45, 13)
        Me.Label21.TabIndex = 47
        Me.Label21.Text = "Label21"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(96, 3)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(45, 13)
        Me.Label22.TabIndex = 50
        Me.Label22.Text = "Label22"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(96, 70)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(45, 13)
        Me.Label23.TabIndex = 46
        Me.Label23.Text = "Label23"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(186, 3)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(45, 13)
        Me.Label24.TabIndex = 52
        Me.Label24.Text = "Label24"
        '
        'TabPage4
        '
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(270, 169)
        Me.TabPage4.TabIndex = 1
        Me.TabPage4.Text = "TabSecond"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'lstParams
        '
        Me.lstParams.FormattingEnabled = True
        Me.lstParams.Location = New System.Drawing.Point(42, 295)
        Me.lstParams.Name = "lstParams"
        Me.lstParams.Size = New System.Drawing.Size(413, 95)
        Me.lstParams.TabIndex = 104
        '
        'btnParamsAdd
        '
        Me.btnParamsAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnParamsAdd.Enabled = False
        Me.btnParamsAdd.Location = New System.Drawing.Point(12, 295)
        Me.btnParamsAdd.Name = "btnParamsAdd"
        Me.btnParamsAdd.Size = New System.Drawing.Size(24, 24)
        Me.btnParamsAdd.TabIndex = 105
        Me.btnParamsAdd.UseVisualStyleBackColor = True
        '
        'btnParamsRemove
        '
        Me.btnParamsRemove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnParamsRemove.Enabled = False
        Me.btnParamsRemove.Location = New System.Drawing.Point(12, 325)
        Me.btnParamsRemove.Name = "btnParamsRemove"
        Me.btnParamsRemove.Size = New System.Drawing.Size(24, 24)
        Me.btnParamsRemove.TabIndex = 106
        Me.btnParamsRemove.UseVisualStyleBackColor = True
        '
        'btnParamsRemoveAll
        '
        Me.btnParamsRemoveAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnParamsRemoveAll.Enabled = False
        Me.btnParamsRemoveAll.Location = New System.Drawing.Point(12, 355)
        Me.btnParamsRemoveAll.Name = "btnParamsRemoveAll"
        Me.btnParamsRemoveAll.Size = New System.Drawing.Size(24, 24)
        Me.btnParamsRemoveAll.TabIndex = 107
        Me.btnParamsRemoveAll.UseVisualStyleBackColor = True
        '
        'btnApply
        '
        Me.btnApply.Location = New System.Drawing.Point(461, 63)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.Size = New System.Drawing.Size(75, 23)
        Me.btnApply.TabIndex = 101
        Me.btnApply.Text = "btnApply"
        Me.btnApply.UseVisualStyleBackColor = True
        '
        'btnWizard
        '
        Me.btnWizard.Location = New System.Drawing.Point(461, 367)
        Me.btnWizard.Name = "btnWizard"
        Me.btnWizard.Size = New System.Drawing.Size(75, 23)
        Me.btnWizard.TabIndex = 103
        Me.btnWizard.Text = "btnWizard"
        Me.btnWizard.UseVisualStyleBackColor = True
        '
        'pbPreview
        '
        Me.pbPreview.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pbPreview.Location = New System.Drawing.Point(461, 121)
        Me.pbPreview.Name = "pbPreview"
        Me.pbPreview.Size = New System.Drawing.Size(75, 75)
        Me.pbPreview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pbPreview.TabIndex = 0
        Me.pbPreview.TabStop = False
        '
        'RemoveParamsMenu
        '
        Me.RemoveParamsMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RemoveAllToolStripMenuItemToolStripMenuItem, Me.RemoveAllButFirstToolStripMenuItemToolStripMenuItem})
        Me.RemoveParamsMenu.Name = "RemoveParamsMenu"
        Me.RemoveParamsMenu.Size = New System.Drawing.Size(274, 48)
        '
        'RemoveAllToolStripMenuItemToolStripMenuItem
        '
        Me.RemoveAllToolStripMenuItemToolStripMenuItem.Name = "RemoveAllToolStripMenuItemToolStripMenuItem"
        Me.RemoveAllToolStripMenuItemToolStripMenuItem.Size = New System.Drawing.Size(273, 22)
        Me.RemoveAllToolStripMenuItemToolStripMenuItem.Text = "RemoveAllToolStripMenuItem"
        '
        'RemoveAllButFirstToolStripMenuItemToolStripMenuItem
        '
        Me.RemoveAllButFirstToolStripMenuItemToolStripMenuItem.Name = "RemoveAllButFirstToolStripMenuItemToolStripMenuItem"
        Me.RemoveAllButFirstToolStripMenuItemToolStripMenuItem.Size = New System.Drawing.Size(273, 22)
        Me.RemoveAllButFirstToolStripMenuItemToolStripMenuItem.Text = "RemoveAllButFirstToolStripMenuItem"
        '
        'LayerPropertiesDialog
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(546, 398)
        Me.Controls.Add(Me.pbPreview)
        Me.Controls.Add(Me.btnWizard)
        Me.Controls.Add(Me.btnApply)
        Me.Controls.Add(Me.btnParamsRemoveAll)
        Me.Controls.Add(Me.btnParamsRemove)
        Me.Controls.Add(Me.btnParamsAdd)
        Me.Controls.Add(Me.lstParams)
        Me.Controls.Add(Me.Tabs)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "LayerPropertiesDialog"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "LayerPropertiesDialog"
        Me.Tabs.ResumeLayout(False)
        Me.TabGeneral.ResumeLayout(False)
        Me.TabGeneral.PerformLayout()
        CType(Me.Transparency, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabSection.ResumeLayout(False)
        Me.TabSection.PerformLayout()
        Me.TabRendering.ResumeLayout(False)
        Me.TabRendering.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabFirst.ResumeLayout(False)
        Me.TabFirst.PerformLayout()
        CType(Me.RenderDefaultSize, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RenderZones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RenderEndSize, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RenderStartSize, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabSecond.ResumeLayout(False)
        Me.TabSecond.PerformLayout()
        CType(Me.RenderDefaultSizeEx, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RenderZonesEx, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RenderEndSizeEx, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RenderStartSizeEx, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabArea.ResumeLayout(False)
        Me.TabArea.PerformLayout()
        CType(Me.AreaOutlineWidth, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabLine.ResumeLayout(False)
        Me.TabLine.PerformLayout()
        CType(Me.LineOutlineWidth, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LineWidth, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabMarker.ResumeLayout(False)
        Me.TabMarker.PerformLayout()
        CType(Me.MarkerOutlineWidth, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MarkerSize, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabLabel.ResumeLayout(False)
        Me.TabLabel.PerformLayout()
        Me.uxLabelPosition.ResumeLayout(False)
        Me.uxLabelPosition.PerformLayout()
        CType(Me.LabelRotation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabChart.ResumeLayout(False)
        Me.TabChart.PerformLayout()
        CType(Me.ChartSize, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.pbPreview, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RemoveParamsMenu.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents AreaColor As ColorButton.ColorButton
    Friend WithEvents AreaOutlineColor As ColorButton.ColorButton
    Friend WithEvents lblAreaColor As System.Windows.Forms.Label
    Friend WithEvents lblAreaOutlineColor As System.Windows.Forms.Label
    Friend WithEvents chkAreaColorUR As System.Windows.Forms.CheckBox
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents FontDialog As System.Windows.Forms.FontDialog
    Friend WithEvents Caption As System.Windows.Forms.TextBox
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents lblPath As System.Windows.Forms.Label
    Friend WithEvents Path As System.Windows.Forms.TextBox
    Friend WithEvents Relation As System.Windows.Forms.ComboBox
    Friend WithEvents lblRelation As System.Windows.Forms.Label
    Friend WithEvents btnRestructure As System.Windows.Forms.Button
    Friend WithEvents RenderField As System.Windows.Forms.ComboBox
    Friend WithEvents Tabs As System.Windows.Forms.TabControl
    Friend WithEvents TabGeneral As System.Windows.Forms.TabPage
    Friend WithEvents TabRendering As System.Windows.Forms.TabPage
    Friend WithEvents TabArea As System.Windows.Forms.TabPage
    Friend WithEvents lblStartSize As System.Windows.Forms.Label
    Friend WithEvents lblZones As System.Windows.Forms.Label
    Friend WithEvents lblField As System.Windows.Forms.Label
    Friend WithEvents lblStartColor As System.Windows.Forms.Label
    Friend WithEvents RenderStartColor As ColorButton.ColorButton
    Friend WithEvents RenderZones As System.Windows.Forms.NumericUpDown
    Friend WithEvents RenderEndColor As ColorButton.ColorButton
    Friend WithEvents lblEndColor As System.Windows.Forms.Label
    Friend WithEvents lblEndSize As System.Windows.Forms.Label
    Friend WithEvents RenderEndSize As System.Windows.Forms.NumericUpDown
    Friend WithEvents RenderStartSize As System.Windows.Forms.NumericUpDown
    Friend WithEvents TabLine As System.Windows.Forms.TabPage
    Friend WithEvents TabMarker As System.Windows.Forms.TabPage
    Friend WithEvents TabLabel As System.Windows.Forms.TabPage
    Friend WithEvents TabChart As System.Windows.Forms.TabPage
    Friend WithEvents lblChartFields As System.Windows.Forms.Label
    Friend WithEvents ChartFields As System.Windows.Forms.ListBox
    Friend WithEvents lblDefaultColor As System.Windows.Forms.Label
    Friend WithEvents RenderDefaultColor As ColorButton.ColorButton
    Friend WithEvents RenderMaxValue As System.Windows.Forms.TextBox
    Friend WithEvents lblMaxValue As System.Windows.Forms.Label
    Friend WithEvents RenderMinValue As System.Windows.Forms.TextBox
    Friend WithEvents lblMinValue As System.Windows.Forms.Label
    Friend WithEvents ChartStyle As System.Windows.Forms.ComboBox
    Friend WithEvents lblChartStyle As System.Windows.Forms.Label
    Friend WithEvents RenderDefaultSize As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblDefaultSize As System.Windows.Forms.Label
    Friend WithEvents chkChartInLegend As System.Windows.Forms.CheckBox
    Friend WithEvents Transparency As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblTransparency As System.Windows.Forms.Label
    Friend WithEvents lblAreaPattern As System.Windows.Forms.Label
    Friend WithEvents AreaPattern As System.Windows.Forms.ComboBox
    Friend WithEvents chkAreaOutlineColorUR As System.Windows.Forms.CheckBox
    Friend WithEvents LineColor As ColorButton.ColorButton
    Friend WithEvents lblLineColor As System.Windows.Forms.Label
    Friend WithEvents LineGeneral As System.Windows.Forms.Label
    Friend WithEvents lblLabelFont As System.Windows.Forms.Label
    Friend WithEvents btnLabelFont As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkAreaInLegend As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblLineStyle As System.Windows.Forms.Label
    Friend WithEvents LineStyle As System.Windows.Forms.ComboBox
    Friend WithEvents LineWidth As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblLineWidth As System.Windows.Forms.Label
    Friend WithEvents chkLineWidthUR As System.Windows.Forms.CheckBox
    Friend WithEvents chkLineColorUR As System.Windows.Forms.CheckBox
    Friend WithEvents chkLineInLegend As System.Windows.Forms.CheckBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents chkMarkerInLegend As System.Windows.Forms.CheckBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents chkMarkerSizeUR As System.Windows.Forms.CheckBox
    Friend WithEvents chkMarkerColorUR As System.Windows.Forms.CheckBox
    Friend WithEvents MarkerSize As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblMarkerSize As System.Windows.Forms.Label
    Friend WithEvents lblMarkerStyle As System.Windows.Forms.Label
    Friend WithEvents MarkerStyle As System.Windows.Forms.ComboBox
    Friend WithEvents MarkerColor As ColorButton.ColorButton
    Friend WithEvents lblMarkerColor As System.Windows.Forms.Label
    Friend WithEvents LabelColor As ColorButton.ColorButton
    Friend WithEvents lblLabelColor As System.Windows.Forms.Label
    Friend WithEvents LabelValue As System.Windows.Forms.TextBox
    Friend WithEvents lblLabelValue As System.Windows.Forms.Label
    Friend WithEvents lblLabelField As System.Windows.Forms.Label
    Friend WithEvents LabelField As System.Windows.Forms.ComboBox
    Friend WithEvents chkLabelVisible As System.Windows.Forms.CheckBox
    Friend WithEvents chkLabelInLegend As System.Windows.Forms.CheckBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents chkChartSizeUR As System.Windows.Forms.CheckBox
    Friend WithEvents ChartSize As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblChartSize As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents chkLabelColorUR As System.Windows.Forms.CheckBox
    Friend WithEvents Projection As System.Windows.Forms.TextBox
    Friend WithEvents lblProjection As System.Windows.Forms.Label
    Friend WithEvents btnProjection As System.Windows.Forms.Button
    Friend WithEvents View As System.Windows.Forms.ComboBox
    Friend WithEvents lblView As System.Windows.Forms.Label
    Friend WithEvents chkAreaOutlineWidthUR As System.Windows.Forms.CheckBox
    Friend WithEvents AreaOutlineWidth As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblAreaOutlineWidth As System.Windows.Forms.Label
    Friend WithEvents AreaOutlineStyle As System.Windows.Forms.ComboBox
    Friend WithEvents lblAreaOutlineStyle As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabFirst As System.Windows.Forms.TabPage
    Friend WithEvents TabSecond As System.Windows.Forms.TabPage
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents RenderDefaultColorEx As ColorButton.ColorButton
    Friend WithEvents RenderDefaultSizeEx As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblZonesEx As System.Windows.Forms.Label
    Friend WithEvents lblDefaultSizeEx As System.Windows.Forms.Label
    Friend WithEvents RenderEndColorEx As ColorButton.ColorButton
    Friend WithEvents RenderZonesEx As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblStartColorEx As System.Windows.Forms.Label
    Friend WithEvents lblDefaultColorEx As System.Windows.Forms.Label
    Friend WithEvents RenderStartColorEx As ColorButton.ColorButton
    Friend WithEvents RenderEndSizeEx As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblStartSizeEx As System.Windows.Forms.Label
    Friend WithEvents lblEndSizeEx As System.Windows.Forms.Label
    Friend WithEvents RenderStartSizeEx As System.Windows.Forms.NumericUpDown
    Friend WithEvents RenderMaxValueEx As System.Windows.Forms.TextBox
    Friend WithEvents lblMinValueEx As System.Windows.Forms.Label
    Friend WithEvents lblEndColorEx As System.Windows.Forms.Label
    Friend WithEvents lblMaxValueEx As System.Windows.Forms.Label
    Friend WithEvents RenderMinValueEx As System.Windows.Forms.TextBox
    Friend WithEvents lstParams As System.Windows.Forms.ListBox
    Friend WithEvents btnParamsAdd As System.Windows.Forms.Button
    Friend WithEvents btnParamsRemove As System.Windows.Forms.Button
    Friend WithEvents btnParamsRemoveAll As System.Windows.Forms.Button
    Friend WithEvents btnApply As System.Windows.Forms.Button
    Friend WithEvents btnWizard As System.Windows.Forms.Button
    Friend WithEvents TabSection As System.Windows.Forms.TabPage
    Friend WithEvents btnSectionCurrentScaleMin As System.Windows.Forms.Button
    Friend WithEvents SectionLegend As System.Windows.Forms.TextBox
    Friend WithEvents SectionQuery As System.Windows.Forms.ComboBox
    Friend WithEvents SectionMaxScale As System.Windows.Forms.TextBox
    Friend WithEvents SectionMinScale As System.Windows.Forms.TextBox
    Friend WithEvents chkSectionVisible As System.Windows.Forms.CheckBox
    Friend WithEvents lblSectionMaxScale As System.Windows.Forms.Label
    Friend WithEvents lblSectionMinScale As System.Windows.Forms.Label
    Friend WithEvents btnSectionClearMaxScale As System.Windows.Forms.Button
    Friend WithEvents btnSectionClearMinScale As System.Windows.Forms.Button
    Friend WithEvents btnSectionCurrentScaleMax As System.Windows.Forms.Button
    Friend WithEvents lblSectionQuery As System.Windows.Forms.Label
    Friend WithEvents lblSectionLegend As System.Windows.Forms.Label
    Friend WithEvents LineOutlineStyle As System.Windows.Forms.ComboBox
    Friend WithEvents chkLineOutlineWidthUR As System.Windows.Forms.CheckBox
    Friend WithEvents chkLineOutlineColorUR As System.Windows.Forms.CheckBox
    Friend WithEvents LineOutlineWidth As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblLineOutlineWidth As System.Windows.Forms.Label
    Friend WithEvents lblLineOutlineStyle As System.Windows.Forms.Label
    Friend WithEvents LineOutlineColor As ColorButton.ColorButton
    Friend WithEvents lblLineOutlineColor As System.Windows.Forms.Label
    Friend WithEvents MarkerOutlineStyle As System.Windows.Forms.ComboBox
    Friend WithEvents chkMarkerOutlineWidthUR As System.Windows.Forms.CheckBox
    Friend WithEvents chkMarkerOutlineColorUR As System.Windows.Forms.CheckBox
    Friend WithEvents MarkerOutlineWidth As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblMarkerOutlineWidth As System.Windows.Forms.Label
    Friend WithEvents lblMarkerOutlineStyle As System.Windows.Forms.Label
    Friend WithEvents MarkerOutlineColor As ColorButton.ColorButton
    Friend WithEvents lblMarkerOutlineColor As System.Windows.Forms.Label
    Friend WithEvents LabelRotation As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblLabelRotation As System.Windows.Forms.Label
    Friend WithEvents btnMarkerSymbol As System.Windows.Forms.Button
    Friend WithEvents pbPreview As System.Windows.Forms.PictureBox
    Friend WithEvents AlphaChannel As System.Windows.Forms.ComboBox
    Friend WithEvents lblAlphaChannel As System.Windows.Forms.Label
    Friend WithEvents uxLabelPosition As System.Windows.Forms.GroupBox
    Friend WithEvents uxLabelPos9 As System.Windows.Forms.CheckBox
    Friend WithEvents uxLabelPos8 As System.Windows.Forms.CheckBox
    Friend WithEvents uxLabelPos7 As System.Windows.Forms.CheckBox
    Friend WithEvents uxLabelPos6 As System.Windows.Forms.CheckBox
    Friend WithEvents uxLabelPos5 As System.Windows.Forms.CheckBox
    Friend WithEvents uxLabelPos4 As System.Windows.Forms.CheckBox
    Friend WithEvents uxLabelPos3 As System.Windows.Forms.CheckBox
    Friend WithEvents uxLabelPos2 As System.Windows.Forms.CheckBox
    Friend WithEvents uxLabelPos1 As System.Windows.Forms.CheckBox
    Friend WithEvents RemoveParamsMenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents RemoveAllToolStripMenuItemToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RemoveAllButFirstToolStripMenuItemToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblLabelInfo As System.Windows.Forms.Label
End Class
