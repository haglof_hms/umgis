<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GisControlView
    Inherits System.Windows.Forms.UserControl

    'UserControl1 overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor. 
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim TgiS_CSUnknownCoordinateSystem1 As TatukGIS.NDK.TGIS_CSUnknownCoordinateSystem = New TatukGIS.NDK.TGIS_CSUnknownCoordinateSystem
        Dim TgiS_CSUnits1 As TatukGIS.NDK.TGIS_CSUnits = New TatukGIS.NDK.TGIS_CSUnits
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(GisControlView))
        Dim TgiS_ControlLegendDialogOptions1 As TatukGIS.NDK.WinForms.TGIS_ControlLegendDialogOptions = New TatukGIS.NDK.WinForms.TGIS_ControlLegendDialogOptions
        Dim TgiS_ControlLegendDialogOptions2 As TatukGIS.NDK.WinForms.TGIS_ControlLegendDialogOptions = New TatukGIS.NDK.WinForms.TGIS_ControlLegendDialogOptions
        Me.ShapeMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.AddRelationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ViewInForestSuiteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.LayerMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ZoomToLayerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.RemoveLayerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PropertiesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator
        Me.HierarchyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AddHierarchyLayerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AddUnusedLayersToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.RemoveHierarchyLayerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator
        Me.AddGroupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.RemoveGroupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PanelMeasure = New System.Windows.Forms.Panel
        Me.lblArea = New System.Windows.Forms.Label
        Me.txtArea = New System.Windows.Forms.TextBox
        Me.lblLength = New System.Windows.Forms.Label
        Me.txtLength = New System.Windows.Forms.TextBox
        Me.PanelMain = New System.Windows.Forms.Panel
        Me.GisCtrl = New TatukGIS.NDK.WinForms.TGIS_ViewerWnd
        Me.ControlScale = New TatukGIS.NDK.WinForms.TGIS_ControlScale
        Me.ControlNorthArrow = New TatukGIS.NDK.WinForms.TGIS_ControlNorthArrow
        Me.PanelData = New System.Windows.Forms.Panel
        Me.DataProgressBar = New System.Windows.Forms.ProgressBar
        Me.ReportData = New AxXtremeReportControl.AxReportControl
        Me.ShapeDataMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ZoomToShapeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AutoZoomToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.FlashCurrentShapeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.RemoveShapeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.ConditionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.CopyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.RelationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.AutoSelectToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SelectionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SelectGroupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.DeselectGroupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.SelectAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.InvertSelectionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.DeselectAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.CachedModeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.DockingPaneManager = New AxXtremeDockingPane.AxDockingPane
        Me.DataTimer = New System.Windows.Forms.Timer(Me.components)
        Me.ControlLegend = New TatukGIS.NDK.WinForms.TGIS_ControlLegend
        Me.ControlHierarchy = New TatukGIS.NDK.WinForms.TGIS_ControlLegend
        Me.CommandBars = New AxXtremeCommandBars.AxCommandBars
        Me.ImageManager = New AxXtremeCommandBars.AxImageManager
        Me.EditMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ApplyEditToolStripMenuItemToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.RevertEditToolStripMenuItemToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator
        Me.AddPartToolStripMenuItemToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.RemovePartToolStripMenuItemToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PanelCopyright = New System.Windows.Forms.Panel
        Me.OSMCopyrightLink = New System.Windows.Forms.LinkLabel
        Me.OSMCopyright = New System.Windows.Forms.Label
        Me.ShapeMenu.SuspendLayout()
        Me.LayerMenu.SuspendLayout()
        Me.PanelMeasure.SuspendLayout()
        Me.PanelMain.SuspendLayout()
        Me.PanelData.SuspendLayout()
        CType(Me.ReportData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ShapeDataMenu.SuspendLayout()
        CType(Me.DockingPaneManager, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CommandBars, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EditMenu.SuspendLayout()
        Me.PanelCopyright.SuspendLayout()
        Me.SuspendLayout()
        '
        'ShapeMenu
        '
        Me.ShapeMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddRelationToolStripMenuItem, Me.ViewInForestSuiteToolStripMenuItem})
        Me.ShapeMenu.Name = "ContextMenuStrip1"
        Me.ShapeMenu.Size = New System.Drawing.Size(270, 48)
        '
        'AddRelationToolStripMenuItem
        '
        Me.AddRelationToolStripMenuItem.Name = "AddRelationToolStripMenuItem"
        Me.AddRelationToolStripMenuItem.Size = New System.Drawing.Size(269, 22)
        Me.AddRelationToolStripMenuItem.Text = "AddRelationToolStripMenuItem"
        '
        'ViewInForestSuiteToolStripMenuItem
        '
        Me.ViewInForestSuiteToolStripMenuItem.Name = "ViewInForestSuiteToolStripMenuItem"
        Me.ViewInForestSuiteToolStripMenuItem.Size = New System.Drawing.Size(269, 22)
        Me.ViewInForestSuiteToolStripMenuItem.Text = "ViewInForestSuiteToolStripMenuItem"
        '
        'LayerMenu
        '
        Me.LayerMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ZoomToLayerToolStripMenuItem, Me.RemoveLayerToolStripMenuItem, Me.PropertiesToolStripMenuItem, Me.ToolStripSeparator5, Me.HierarchyToolStripMenuItem})
        Me.LayerMenu.Name = "ContextMenuStrip1"
        Me.LayerMenu.Size = New System.Drawing.Size(250, 98)
        '
        'ZoomToLayerToolStripMenuItem
        '
        Me.ZoomToLayerToolStripMenuItem.Name = "ZoomToLayerToolStripMenuItem"
        Me.ZoomToLayerToolStripMenuItem.Size = New System.Drawing.Size(249, 22)
        Me.ZoomToLayerToolStripMenuItem.Text = "ZoomToLayerToolStripMenuItem"
        '
        'RemoveLayerToolStripMenuItem
        '
        Me.RemoveLayerToolStripMenuItem.Name = "RemoveLayerToolStripMenuItem"
        Me.RemoveLayerToolStripMenuItem.Size = New System.Drawing.Size(249, 22)
        Me.RemoveLayerToolStripMenuItem.Text = "RemoveLayerToolStripMenuItem"
        '
        'PropertiesToolStripMenuItem
        '
        Me.PropertiesToolStripMenuItem.Name = "PropertiesToolStripMenuItem"
        Me.PropertiesToolStripMenuItem.Size = New System.Drawing.Size(249, 22)
        Me.PropertiesToolStripMenuItem.Text = "PropertiesToolStripMenuItem"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(246, 6)
        '
        'HierarchyToolStripMenuItem
        '
        Me.HierarchyToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AddHierarchyLayerToolStripMenuItem, Me.AddUnusedLayersToolStripMenuItem, Me.RemoveHierarchyLayerToolStripMenuItem, Me.ToolStripSeparator6, Me.AddGroupToolStripMenuItem, Me.RemoveGroupToolStripMenuItem})
        Me.HierarchyToolStripMenuItem.Name = "HierarchyToolStripMenuItem"
        Me.HierarchyToolStripMenuItem.Size = New System.Drawing.Size(249, 22)
        Me.HierarchyToolStripMenuItem.Text = "HierarchyToolStripMenuItem"
        '
        'AddHierarchyLayerToolStripMenuItem
        '
        Me.AddHierarchyLayerToolStripMenuItem.Name = "AddHierarchyLayerToolStripMenuItem"
        Me.AddHierarchyLayerToolStripMenuItem.Size = New System.Drawing.Size(298, 22)
        Me.AddHierarchyLayerToolStripMenuItem.Text = "AddHierarchyLayerToolStripMenuItem"
        '
        'AddUnusedLayersToolStripMenuItem
        '
        Me.AddUnusedLayersToolStripMenuItem.Name = "AddUnusedLayersToolStripMenuItem"
        Me.AddUnusedLayersToolStripMenuItem.Size = New System.Drawing.Size(298, 22)
        Me.AddUnusedLayersToolStripMenuItem.Text = "AddUnusedLayersToolStripMenuItem"
        '
        'RemoveHierarchyLayerToolStripMenuItem
        '
        Me.RemoveHierarchyLayerToolStripMenuItem.Name = "RemoveHierarchyLayerToolStripMenuItem"
        Me.RemoveHierarchyLayerToolStripMenuItem.Size = New System.Drawing.Size(298, 22)
        Me.RemoveHierarchyLayerToolStripMenuItem.Text = "RemoveHierarchyLayerToolStripMenuItem"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(295, 6)
        '
        'AddGroupToolStripMenuItem
        '
        Me.AddGroupToolStripMenuItem.Name = "AddGroupToolStripMenuItem"
        Me.AddGroupToolStripMenuItem.Size = New System.Drawing.Size(298, 22)
        Me.AddGroupToolStripMenuItem.Text = "AddGroupToolStripMenuItem"
        '
        'RemoveGroupToolStripMenuItem
        '
        Me.RemoveGroupToolStripMenuItem.Name = "RemoveGroupToolStripMenuItem"
        Me.RemoveGroupToolStripMenuItem.Size = New System.Drawing.Size(298, 22)
        Me.RemoveGroupToolStripMenuItem.Text = "RemoveGroupToolStripMenuItem"
        '
        'PanelMeasure
        '
        Me.PanelMeasure.BackColor = System.Drawing.SystemColors.Window
        Me.PanelMeasure.Controls.Add(Me.lblArea)
        Me.PanelMeasure.Controls.Add(Me.txtArea)
        Me.PanelMeasure.Controls.Add(Me.lblLength)
        Me.PanelMeasure.Controls.Add(Me.txtLength)
        Me.PanelMeasure.Location = New System.Drawing.Point(208, 270)
        Me.PanelMeasure.Name = "PanelMeasure"
        Me.PanelMeasure.Size = New System.Drawing.Size(193, 65)
        Me.PanelMeasure.TabIndex = 5
        '
        'lblArea
        '
        Me.lblArea.AutoSize = True
        Me.lblArea.Location = New System.Drawing.Point(7, 41)
        Me.lblArea.Name = "lblArea"
        Me.lblArea.Size = New System.Drawing.Size(39, 13)
        Me.lblArea.TabIndex = 3
        Me.lblArea.Text = "lblArea"
        '
        'txtArea
        '
        Me.txtArea.BackColor = System.Drawing.SystemColors.Window
        Me.txtArea.Location = New System.Drawing.Point(60, 38)
        Me.txtArea.Name = "txtArea"
        Me.txtArea.ReadOnly = True
        Me.txtArea.Size = New System.Drawing.Size(120, 20)
        Me.txtArea.TabIndex = 2
        '
        'lblLength
        '
        Me.lblLength.AutoSize = True
        Me.lblLength.Location = New System.Drawing.Point(7, 15)
        Me.lblLength.Name = "lblLength"
        Me.lblLength.Size = New System.Drawing.Size(50, 13)
        Me.lblLength.TabIndex = 1
        Me.lblLength.Text = "lblLength"
        '
        'txtLength
        '
        Me.txtLength.BackColor = System.Drawing.SystemColors.Window
        Me.txtLength.Location = New System.Drawing.Point(60, 12)
        Me.txtLength.Name = "txtLength"
        Me.txtLength.ReadOnly = True
        Me.txtLength.Size = New System.Drawing.Size(120, 20)
        Me.txtLength.TabIndex = 0
        '
        'PanelMain
        '
        Me.PanelMain.Controls.Add(Me.GisCtrl)
        Me.PanelMain.Controls.Add(Me.ControlScale)
        Me.PanelMain.Controls.Add(Me.ControlNorthArrow)
        Me.PanelMain.Location = New System.Drawing.Point(20, 39)
        Me.PanelMain.Name = "PanelMain"
        Me.PanelMain.Size = New System.Drawing.Size(477, 130)
        Me.PanelMain.TabIndex = 6
        '
        'GisCtrl
        '
        Me.GisCtrl.BackColor = System.Drawing.SystemColors.Window
        Me.GisCtrl.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.GisCtrl.Copyright = "TatukGIS Developer Kernel. 10.46.0.15567  (c)2000-2016 TatukGIS. ALL RIGHTS RESER" & _
            "VED."
        TgiS_CSUnknownCoordinateSystem1.DescriptionEx = Nothing
        TgiS_CSUnknownCoordinateSystem1.ReversedCoordinates = False
        Me.GisCtrl.CS = TgiS_CSUnknownCoordinateSystem1
        Me.GisCtrl.Cursor = System.Windows.Forms.Cursors.Default
        Me.GisCtrl.CursorForDrag = System.Windows.Forms.Cursors.Default
        Me.GisCtrl.CursorForEdit = System.Windows.Forms.Cursors.Default
        Me.GisCtrl.CursorForSelect = System.Windows.Forms.Cursors.Default
        Me.GisCtrl.CursorForUserDefined = System.Windows.Forms.Cursors.Default
        Me.GisCtrl.CursorForZoom = System.Windows.Forms.Cursors.Default
        Me.GisCtrl.CursorForZoomEx = System.Windows.Forms.Cursors.Default
        Me.GisCtrl.Device = TatukGIS.NDK.TGIS_Device.gisDeviceWindow
        Me.GisCtrl.FullPaint = True
        Me.GisCtrl.Location = New System.Drawing.Point(19, 18)
        Me.GisCtrl.Mode = TatukGIS.NDK.TGIS_ViewerMode.gisSelect
        Me.GisCtrl.MultiUserMode = TatukGIS.NDK.TGIS_MultiUser.gisDefault
        Me.GisCtrl.Name = "GisCtrl"
        Me.GisCtrl.PrinterModeForceBitmap = False
        Me.GisCtrl.PrinterTileSize = 2700
        Me.GisCtrl.PrintFooter = Nothing
        Me.GisCtrl.PrintFooterFont = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GisCtrl.PrintFooterFontColor = System.Drawing.SystemColors.WindowText
        Me.GisCtrl.PrintSubtitle = Nothing
        Me.GisCtrl.PrintSubtitleFont = New System.Drawing.Font("Arial", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GisCtrl.PrintSubtitleFontColor = System.Drawing.SystemColors.WindowText
        Me.GisCtrl.PrintTitle = Nothing
        Me.GisCtrl.PrintTitleFont = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GisCtrl.PrintTitleFontColor = System.Drawing.SystemColors.WindowText
        Me.GisCtrl.RotationAngle = 0
        Me.GisCtrl.ScaleAsFloat = 1
        Me.GisCtrl.ScaleAsText = "1:1"
        Me.GisCtrl.SelectionColor = System.Drawing.Color.Red
        Me.GisCtrl.SelectionPattern = Nothing
        Me.GisCtrl.SelectionTransparency = 50
        Me.GisCtrl.Size = New System.Drawing.Size(186, 95)
        Me.GisCtrl.TabIndex = 14
        Me.GisCtrl.TemporaryScaleInternal = 0
        Me.GisCtrl.View3D = False
        Me.GisCtrl.ViewportOffset = New System.Drawing.Point(0, 0)
        Me.GisCtrl.Zoom = 0
        Me.GisCtrl.ZoomEx = 0
        '
        'ControlScale
        '
        Me.ControlScale.BackColor = System.Drawing.SystemColors.Window
        Me.ControlScale.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ControlScale.DividerColor1 = System.Drawing.Color.Black
        Me.ControlScale.DividerColor2 = System.Drawing.Color.White
        Me.ControlScale.FreeThread = False
        Me.ControlScale.GIS_Viewer = Me.GisCtrl
        Me.ControlScale.Location = New System.Drawing.Point(275, 72)
        Me.ControlScale.Name = "ControlScale"
        Me.ControlScale.Size = New System.Drawing.Size(185, 41)
        Me.ControlScale.TabIndex = 13
        Me.ControlScale.TextColor = System.Drawing.Color.Black
        Me.ControlScale.Transparent = False
        TgiS_CSUnits1.DescriptionEx = Nothing
        Me.ControlScale.Units = TgiS_CSUnits1
        Me.ControlScale.UnitsEPSG = 9001
        '
        'ControlNorthArrow
        '
        Me.ControlNorthArrow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ControlNorthArrow.Color1 = System.Drawing.Color.Black
        Me.ControlNorthArrow.Color2 = System.Drawing.Color.Black
        Me.ControlNorthArrow.FreeThread = False
        Me.ControlNorthArrow.GIS_Viewer = Me.GisCtrl
        Me.ControlNorthArrow.Location = New System.Drawing.Point(275, 3)
        Me.ControlNorthArrow.Name = "ControlNorthArrow"
        Me.ControlNorthArrow.Path = Nothing
        Me.ControlNorthArrow.Size = New System.Drawing.Size(62, 63)
        Me.ControlNorthArrow.Symbol = 0
        Me.ControlNorthArrow.TabIndex = 2
        Me.ControlNorthArrow.Transparent = False
        '
        'PanelData
        '
        Me.PanelData.BackColor = System.Drawing.SystemColors.Window
        Me.PanelData.Controls.Add(Me.DataProgressBar)
        Me.PanelData.Controls.Add(Me.ReportData)
        Me.PanelData.Location = New System.Drawing.Point(407, 270)
        Me.PanelData.Name = "PanelData"
        Me.PanelData.Size = New System.Drawing.Size(210, 78)
        Me.PanelData.TabIndex = 4
        '
        'DataProgressBar
        '
        Me.DataProgressBar.BackColor = System.Drawing.SystemColors.Control
        Me.DataProgressBar.Location = New System.Drawing.Point(20, 55)
        Me.DataProgressBar.Name = "DataProgressBar"
        Me.DataProgressBar.Size = New System.Drawing.Size(157, 20)
        Me.DataProgressBar.TabIndex = 1
        Me.DataProgressBar.Visible = False
        '
        'ReportData
        '
        Me.ReportData.Location = New System.Drawing.Point(20, 14)
        Me.ReportData.Name = "ReportData"
        Me.ReportData.OcxState = CType(resources.GetObject("ReportData.OcxState"), System.Windows.Forms.AxHost.State)
        Me.ReportData.Size = New System.Drawing.Size(100, 50)
        Me.ReportData.TabIndex = 0
        '
        'ShapeDataMenu
        '
        Me.ShapeDataMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ZoomToShapeToolStripMenuItem, Me.AutoZoomToolStripMenuItem, Me.FlashCurrentShapeToolStripMenuItem, Me.RemoveShapeToolStripMenuItem, Me.ToolStripSeparator4, Me.ConditionsToolStripMenuItem, Me.CopyToolStripMenuItem, Me.RelationToolStripMenuItem, Me.ToolStripSeparator2, Me.AutoSelectToolStripMenuItem, Me.SelectionToolStripMenuItem, Me.ToolStripSeparator1, Me.CachedModeToolStripMenuItem})
        Me.ShapeDataMenu.Name = "ContextMenuStrip1"
        Me.ShapeDataMenu.Size = New System.Drawing.Size(276, 242)
        '
        'ZoomToShapeToolStripMenuItem
        '
        Me.ZoomToShapeToolStripMenuItem.Name = "ZoomToShapeToolStripMenuItem"
        Me.ZoomToShapeToolStripMenuItem.Size = New System.Drawing.Size(275, 22)
        Me.ZoomToShapeToolStripMenuItem.Text = "ZoomToShapeToolStripMenuItem"
        '
        'AutoZoomToolStripMenuItem
        '
        Me.AutoZoomToolStripMenuItem.Name = "AutoZoomToolStripMenuItem"
        Me.AutoZoomToolStripMenuItem.Size = New System.Drawing.Size(275, 22)
        Me.AutoZoomToolStripMenuItem.Text = "AutoZoomToolStripMenuItem"
        '
        'FlashCurrentShapeToolStripMenuItem
        '
        Me.FlashCurrentShapeToolStripMenuItem.Name = "FlashCurrentShapeToolStripMenuItem"
        Me.FlashCurrentShapeToolStripMenuItem.Size = New System.Drawing.Size(275, 22)
        Me.FlashCurrentShapeToolStripMenuItem.Text = "FlashCurrentShapeToolStripMenuItem"
        '
        'RemoveShapeToolStripMenuItem
        '
        Me.RemoveShapeToolStripMenuItem.Name = "RemoveShapeToolStripMenuItem"
        Me.RemoveShapeToolStripMenuItem.Size = New System.Drawing.Size(275, 22)
        Me.RemoveShapeToolStripMenuItem.Text = "RemoveShapeToolStripMenuItem"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(272, 6)
        '
        'ConditionsToolStripMenuItem
        '
        Me.ConditionsToolStripMenuItem.Name = "ConditionsToolStripMenuItem"
        Me.ConditionsToolStripMenuItem.Size = New System.Drawing.Size(275, 22)
        Me.ConditionsToolStripMenuItem.Text = "ConditionsToolStripMenuItem"
        '
        'CopyToolStripMenuItem
        '
        Me.CopyToolStripMenuItem.Name = "CopyToolStripMenuItem"
        Me.CopyToolStripMenuItem.Size = New System.Drawing.Size(275, 22)
        Me.CopyToolStripMenuItem.Text = "CopyToolStripMenuItem"
        '
        'RelationToolStripMenuItem
        '
        Me.RelationToolStripMenuItem.Name = "RelationToolStripMenuItem"
        Me.RelationToolStripMenuItem.Size = New System.Drawing.Size(275, 22)
        Me.RelationToolStripMenuItem.Text = "RelationToolStripMenuItem"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(272, 6)
        '
        'AutoSelectToolStripMenuItem
        '
        Me.AutoSelectToolStripMenuItem.Name = "AutoSelectToolStripMenuItem"
        Me.AutoSelectToolStripMenuItem.Size = New System.Drawing.Size(275, 22)
        Me.AutoSelectToolStripMenuItem.Text = "AutoSelectToolStripMenuItem"
        '
        'SelectionToolStripMenuItem
        '
        Me.SelectionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SelectGroupToolStripMenuItem, Me.DeselectGroupToolStripMenuItem, Me.ToolStripSeparator3, Me.SelectAllToolStripMenuItem, Me.InvertSelectionToolStripMenuItem, Me.DeselectAllToolStripMenuItem})
        Me.SelectionToolStripMenuItem.Name = "SelectionToolStripMenuItem"
        Me.SelectionToolStripMenuItem.Size = New System.Drawing.Size(275, 22)
        Me.SelectionToolStripMenuItem.Text = "SelectionToolStripMenuItem"
        '
        'SelectGroupToolStripMenuItem
        '
        Me.SelectGroupToolStripMenuItem.Name = "SelectGroupToolStripMenuItem"
        Me.SelectGroupToolStripMenuItem.Size = New System.Drawing.Size(254, 22)
        Me.SelectGroupToolStripMenuItem.Text = "SelectGroupToolStripMenuItem"
        '
        'DeselectGroupToolStripMenuItem
        '
        Me.DeselectGroupToolStripMenuItem.Name = "DeselectGroupToolStripMenuItem"
        Me.DeselectGroupToolStripMenuItem.Size = New System.Drawing.Size(254, 22)
        Me.DeselectGroupToolStripMenuItem.Text = "DeselectGroupToolStripMenuItem"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(251, 6)
        '
        'SelectAllToolStripMenuItem
        '
        Me.SelectAllToolStripMenuItem.Name = "SelectAllToolStripMenuItem"
        Me.SelectAllToolStripMenuItem.Size = New System.Drawing.Size(254, 22)
        Me.SelectAllToolStripMenuItem.Text = "SelectAllToolStripMenuItem"
        '
        'InvertSelectionToolStripMenuItem
        '
        Me.InvertSelectionToolStripMenuItem.Name = "InvertSelectionToolStripMenuItem"
        Me.InvertSelectionToolStripMenuItem.Size = New System.Drawing.Size(254, 22)
        Me.InvertSelectionToolStripMenuItem.Text = "InvertSelectionToolStripMenuItem"
        '
        'DeselectAllToolStripMenuItem
        '
        Me.DeselectAllToolStripMenuItem.Name = "DeselectAllToolStripMenuItem"
        Me.DeselectAllToolStripMenuItem.Size = New System.Drawing.Size(254, 22)
        Me.DeselectAllToolStripMenuItem.Text = "DeselectAllToolStripMenuItem"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(272, 6)
        '
        'CachedModeToolStripMenuItem
        '
        Me.CachedModeToolStripMenuItem.Name = "CachedModeToolStripMenuItem"
        Me.CachedModeToolStripMenuItem.Size = New System.Drawing.Size(275, 22)
        Me.CachedModeToolStripMenuItem.Text = "CachedModeToolStripMenuItem"
        '
        'DockingPaneManager
        '
        Me.DockingPaneManager.Enabled = True
        Me.DockingPaneManager.Location = New System.Drawing.Point(525, 39)
        Me.DockingPaneManager.Name = "DockingPaneManager"
        Me.DockingPaneManager.OcxState = CType(resources.GetObject("DockingPaneManager.OcxState"), System.Windows.Forms.AxHost.State)
        Me.DockingPaneManager.Size = New System.Drawing.Size(24, 24)
        Me.DockingPaneManager.TabIndex = 7
        '
        'DataTimer
        '
        '
        'ControlLegend
        '
        Me.ControlLegend.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ControlLegend.ColorSelected = System.Drawing.Color.FromArgb(CType(CType(189, Byte), Integer), CType(CType(207, Byte), Integer), CType(CType(239, Byte), Integer))
        Me.ControlLegend.ColorSelectedFrame = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(198, Byte), Integer))
        TgiS_ControlLegendDialogOptions1.VectorWizardUniqueLimit = 256
        TgiS_ControlLegendDialogOptions1.VectorWizardUniqueSearchLimit = 16384
        Me.ControlLegend.DialogOptions = TgiS_ControlLegendDialogOptions1
        Me.ControlLegend.FontSubtitle = New System.Drawing.Font("Arial", 7.0!)
        Me.ControlLegend.FontSubtitleColor = System.Drawing.SystemColors.WindowText
        Me.ControlLegend.FontTitle = New System.Drawing.Font("Arial", 8.0!)
        Me.ControlLegend.FontTitleColor = System.Drawing.SystemColors.WindowText
        Me.ControlLegend.FreeThread = False
        Me.ControlLegend.GIS_Group = Nothing
        Me.ControlLegend.GIS_Layer = Nothing
        Me.ControlLegend.GIS_Viewer = Me.GisCtrl
        Me.ControlLegend.Location = New System.Drawing.Point(20, 256)
        Me.ControlLegend.Mode = TatukGIS.NDK.WinForms.TGIS_ControlLegendMode.gisControlLegendModeLayers
        Me.ControlLegend.Name = "ControlLegend"
        Me.ControlLegend.Options = CType(((((TatukGIS.NDK.WinForms.TGIS_ControlLegendOptions.gisControlLegendAllowMove Or TatukGIS.NDK.WinForms.TGIS_ControlLegendOptions.gisControlLegendAllowActive) _
                    Or TatukGIS.NDK.WinForms.TGIS_ControlLegendOptions.gisControlLegendAllowExpand) _
                    Or TatukGIS.NDK.WinForms.TGIS_ControlLegendOptions.gisControlLegendAllowSelect) _
                    Or TatukGIS.NDK.WinForms.TGIS_ControlLegendOptions.gisControlLegendShowSubLayers), TatukGIS.NDK.WinForms.TGIS_ControlLegendOptions)
        Me.ControlLegend.ReverseOrder = False
        Me.ControlLegend.Size = New System.Drawing.Size(88, 89)
        Me.ControlLegend.Spacing = 3
        Me.ControlLegend.TabIndex = 9
        Me.ControlLegend.TabStop = True
        '
        'ControlHierarchy
        '
        Me.ControlHierarchy.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ControlHierarchy.ColorSelected = System.Drawing.Color.FromArgb(CType(CType(189, Byte), Integer), CType(CType(207, Byte), Integer), CType(CType(239, Byte), Integer))
        Me.ControlHierarchy.ColorSelectedFrame = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(198, Byte), Integer))
        TgiS_ControlLegendDialogOptions2.VectorWizardUniqueLimit = 256
        TgiS_ControlLegendDialogOptions2.VectorWizardUniqueSearchLimit = 16384
        Me.ControlHierarchy.DialogOptions = TgiS_ControlLegendDialogOptions2
        Me.ControlHierarchy.FontSubtitle = New System.Drawing.Font("Arial", 7.0!)
        Me.ControlHierarchy.FontSubtitleColor = System.Drawing.SystemColors.WindowText
        Me.ControlHierarchy.FontTitle = New System.Drawing.Font("Arial", 8.0!)
        Me.ControlHierarchy.FontTitleColor = System.Drawing.SystemColors.WindowText
        Me.ControlHierarchy.FreeThread = False
        Me.ControlHierarchy.GIS_Group = Nothing
        Me.ControlHierarchy.GIS_Layer = Nothing
        Me.ControlHierarchy.GIS_Viewer = Me.GisCtrl
        Me.ControlHierarchy.Location = New System.Drawing.Point(114, 256)
        Me.ControlHierarchy.Mode = TatukGIS.NDK.WinForms.TGIS_ControlLegendMode.gisControlLegendModeGroups
        Me.ControlHierarchy.Name = "ControlHierarchy"
        Me.ControlHierarchy.Options = CType(((((TatukGIS.NDK.WinForms.TGIS_ControlLegendOptions.gisControlLegendAllowMove Or TatukGIS.NDK.WinForms.TGIS_ControlLegendOptions.gisControlLegendAllowActive) _
                    Or TatukGIS.NDK.WinForms.TGIS_ControlLegendOptions.gisControlLegendAllowExpand) _
                    Or TatukGIS.NDK.WinForms.TGIS_ControlLegendOptions.gisControlLegendAllowSelect) _
                    Or TatukGIS.NDK.WinForms.TGIS_ControlLegendOptions.gisControlLegendShowSubLayers), TatukGIS.NDK.WinForms.TGIS_ControlLegendOptions)
        Me.ControlHierarchy.ReverseOrder = False
        Me.ControlHierarchy.Size = New System.Drawing.Size(88, 89)
        Me.ControlHierarchy.Spacing = 3
        Me.ControlHierarchy.TabIndex = 10
        Me.ControlHierarchy.TabStop = True
        '
        'CommandBars
        '
        Me.CommandBars.Enabled = True
        Me.CommandBars.Location = New System.Drawing.Point(556, 39)
        Me.CommandBars.Name = "CommandBars"
        Me.CommandBars.OcxState = CType(resources.GetObject("CommandBars.OcxState"), System.Windows.Forms.AxHost.State)
        Me.CommandBars.Size = New System.Drawing.Size(24, 24)
        Me.CommandBars.TabIndex = 11
        '
        'ImageManager
        '
        Me.ImageManager.Enabled = True
        Me.ImageManager.Location = New System.Drawing.Point(587, 39)
        Me.ImageManager.Name = "ImageManager"
        Me.ImageManager.OcxState = CType(resources.GetObject("ImageManager.OcxState"), System.Windows.Forms.AxHost.State)
        Me.ImageManager.Size = New System.Drawing.Size(24, 24)
        Me.ImageManager.TabIndex = 12
        '
        'EditMenu
        '
        Me.EditMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ApplyEditToolStripMenuItemToolStripMenuItem, Me.RevertEditToolStripMenuItemToolStripMenuItem, Me.ToolStripSeparator7, Me.AddPartToolStripMenuItemToolStripMenuItem, Me.RemovePartToolStripMenuItemToolStripMenuItem})
        Me.EditMenu.Name = "EditMenu"
        Me.EditMenu.Size = New System.Drawing.Size(241, 98)
        '
        'ApplyEditToolStripMenuItemToolStripMenuItem
        '
        Me.ApplyEditToolStripMenuItemToolStripMenuItem.Name = "ApplyEditToolStripMenuItemToolStripMenuItem"
        Me.ApplyEditToolStripMenuItemToolStripMenuItem.Size = New System.Drawing.Size(240, 22)
        Me.ApplyEditToolStripMenuItemToolStripMenuItem.Text = "ApplyEditToolStripMenuItem"
        '
        'RevertEditToolStripMenuItemToolStripMenuItem
        '
        Me.RevertEditToolStripMenuItemToolStripMenuItem.Name = "RevertEditToolStripMenuItemToolStripMenuItem"
        Me.RevertEditToolStripMenuItemToolStripMenuItem.Size = New System.Drawing.Size(240, 22)
        Me.RevertEditToolStripMenuItemToolStripMenuItem.Text = "RevertEditToolStripMenuItem"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(237, 6)
        '
        'AddPartToolStripMenuItemToolStripMenuItem
        '
        Me.AddPartToolStripMenuItemToolStripMenuItem.Name = "AddPartToolStripMenuItemToolStripMenuItem"
        Me.AddPartToolStripMenuItemToolStripMenuItem.Size = New System.Drawing.Size(240, 22)
        Me.AddPartToolStripMenuItemToolStripMenuItem.Text = "AddPartToolStripMenuItem"
        '
        'RemovePartToolStripMenuItemToolStripMenuItem
        '
        Me.RemovePartToolStripMenuItemToolStripMenuItem.Name = "RemovePartToolStripMenuItemToolStripMenuItem"
        Me.RemovePartToolStripMenuItemToolStripMenuItem.Size = New System.Drawing.Size(240, 22)
        Me.RemovePartToolStripMenuItemToolStripMenuItem.Text = "RemovePartToolStripMenuItem"
        '
        'PanelCopyright
        '
        Me.PanelCopyright.Controls.Add(Me.OSMCopyrightLink)
        Me.PanelCopyright.Controls.Add(Me.OSMCopyright)
        Me.PanelCopyright.Location = New System.Drawing.Point(407, 231)
        Me.PanelCopyright.Name = "PanelCopyright"
        Me.PanelCopyright.Size = New System.Drawing.Size(186, 33)
        Me.PanelCopyright.TabIndex = 13
        Me.PanelCopyright.Visible = False
        '
        'OSMCopyrightLink
        '
        Me.OSMCopyrightLink.AutoSize = True
        Me.OSMCopyrightLink.Location = New System.Drawing.Point(121, 16)
        Me.OSMCopyrightLink.Name = "OSMCopyrightLink"
        Me.OSMCopyrightLink.Size = New System.Drawing.Size(62, 13)
        Me.OSMCopyrightLink.TabIndex = 1
        Me.OSMCopyrightLink.TabStop = True
        Me.OSMCopyrightLink.Tag = "http://www.openstreetmap.org/copyright/en"
        Me.OSMCopyrightLink.Text = "contributors"
        '
        'OSMCopyright
        '
        Me.OSMCopyright.AutoSize = True
        Me.OSMCopyright.Location = New System.Drawing.Point(3, 3)
        Me.OSMCopyright.Name = "OSMCopyright"
        Me.OSMCopyright.Size = New System.Drawing.Size(134, 26)
        Me.OSMCopyright.TabIndex = 0
        Me.OSMCopyright.Text = "� terrestris GmbH && Co KG" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Data � OpenStreetMap"
        '
        'GisControlView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.PanelCopyright)
        Me.Controls.Add(Me.ImageManager)
        Me.Controls.Add(Me.CommandBars)
        Me.Controls.Add(Me.ControlHierarchy)
        Me.Controls.Add(Me.PanelMeasure)
        Me.Controls.Add(Me.PanelMain)
        Me.Controls.Add(Me.ControlLegend)
        Me.Controls.Add(Me.DockingPaneManager)
        Me.Controls.Add(Me.PanelData)
        Me.Name = "GisControlView"
        Me.Size = New System.Drawing.Size(740, 397)
        Me.ShapeMenu.ResumeLayout(False)
        Me.LayerMenu.ResumeLayout(False)
        Me.PanelMeasure.ResumeLayout(False)
        Me.PanelMeasure.PerformLayout()
        Me.PanelMain.ResumeLayout(False)
        Me.PanelData.ResumeLayout(False)
        CType(Me.ReportData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ShapeDataMenu.ResumeLayout(False)
        CType(Me.DockingPaneManager, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CommandBars, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImageManager, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EditMenu.ResumeLayout(False)
        Me.PanelCopyright.ResumeLayout(False)
        Me.PanelCopyright.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ShapeMenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents AddRelationToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewInForestSuiteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LayerMenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents RemoveLayerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PropertiesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ZoomToLayerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PanelMeasure As System.Windows.Forms.Panel
    Friend WithEvents lblLength As System.Windows.Forms.Label
    Friend WithEvents txtLength As System.Windows.Forms.TextBox
    Friend WithEvents lblArea As System.Windows.Forms.Label
    Friend WithEvents txtArea As System.Windows.Forms.TextBox
    Friend WithEvents PanelMain As System.Windows.Forms.Panel
    Friend WithEvents PanelData As System.Windows.Forms.Panel
    Friend WithEvents ShapeDataMenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ZoomToShapeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AutoZoomToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FlashCurrentShapeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReportData As AxXtremeReportControl.AxReportControl
    Friend WithEvents DockingPaneManager As AxXtremeDockingPane.AxDockingPane
    Friend WithEvents ConditionsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataTimer As System.Windows.Forms.Timer
    Friend WithEvents DataProgressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents SelectionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SelectGroupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeselectGroupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SelectAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InvertSelectionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeselectAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents RemoveShapeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ControlLegend As TatukGIS.NDK.WinForms.TGIS_ControlLegend
    Friend WithEvents ControlHierarchy As TatukGIS.NDK.WinForms.TGIS_ControlLegend
    Friend WithEvents HierarchyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddHierarchyLayerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RemoveHierarchyLayerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AddUnusedLayersToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AddGroupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RemoveGroupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CommandBars As AxXtremeCommandBars.AxCommandBars
    Friend WithEvents ImageManager As AxXtremeCommandBars.AxImageManager
    Friend WithEvents ControlScale As TatukGIS.NDK.WinForms.TGIS_ControlScale
    Friend WithEvents AutoSelectToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GisCtrl As TatukGIS.NDK.WinForms.TGIS_ViewerWnd
    Friend WithEvents EditMenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ApplyEditToolStripMenuItemToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RevertEditToolStripMenuItemToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AddPartToolStripMenuItemToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RemovePartToolStripMenuItemToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ControlNorthArrow As TatukGIS.NDK.WinForms.TGIS_ControlNorthArrow
    Friend WithEvents PanelCopyright As System.Windows.Forms.Panel
    Friend WithEvents OSMCopyrightLink As System.Windows.Forms.LinkLabel
    Friend WithEvents OSMCopyright As System.Windows.Forms.Label
    Friend WithEvents CopyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RelationToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CachedModeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
