﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ImportStandDataDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.uxOK = New System.Windows.Forms.Button
        Me.uxCancel = New System.Windows.Forms.Button
        Me.cmbStandPoint = New System.Windows.Forms.ComboBox
        Me.lblStandPoint = New System.Windows.Forms.Label
        Me.lblStandPolygon = New System.Windows.Forms.Label
        Me.cmbStandPolygon = New System.Windows.Forms.ComboBox
        Me.lblSampleTreePoint = New System.Windows.Forms.Label
        Me.cmbSampleTreePoint = New System.Windows.Forms.ComboBox
        Me.lblPlotPoint = New System.Windows.Forms.Label
        Me.cmbPlotPoint = New System.Windows.Forms.ComboBox
        Me.uxChooseLayers = New System.Windows.Forms.GroupBox
        Me.uxChooseLayers.SuspendLayout()
        Me.SuspendLayout()
        '
        'uxOK
        '
        Me.uxOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.uxOK.Enabled = False
        Me.uxOK.Location = New System.Drawing.Point(65, 218)
        Me.uxOK.Name = "uxOK"
        Me.uxOK.Size = New System.Drawing.Size(75, 23)
        Me.uxOK.TabIndex = 1
        Me.uxOK.Text = "uxOK"
        Me.uxOK.UseVisualStyleBackColor = True
        '
        'uxCancel
        '
        Me.uxCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.uxCancel.Location = New System.Drawing.Point(146, 218)
        Me.uxCancel.Name = "uxCancel"
        Me.uxCancel.Size = New System.Drawing.Size(75, 23)
        Me.uxCancel.TabIndex = 2
        Me.uxCancel.Text = "uxCancel"
        Me.uxCancel.UseVisualStyleBackColor = True
        '
        'cmbStandPoint
        '
        Me.cmbStandPoint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbStandPoint.FormattingEnabled = True
        Me.cmbStandPoint.Location = New System.Drawing.Point(9, 33)
        Me.cmbStandPoint.Name = "cmbStandPoint"
        Me.cmbStandPoint.Size = New System.Drawing.Size(268, 21)
        Me.cmbStandPoint.TabIndex = 0
        '
        'lblStandPoint
        '
        Me.lblStandPoint.AutoSize = True
        Me.lblStandPoint.Location = New System.Drawing.Point(6, 16)
        Me.lblStandPoint.Name = "lblStandPoint"
        Me.lblStandPoint.Size = New System.Drawing.Size(69, 13)
        Me.lblStandPoint.TabIndex = 3
        Me.lblStandPoint.Text = "lblStandPoint"
        '
        'lblStandPolygon
        '
        Me.lblStandPolygon.AutoSize = True
        Me.lblStandPolygon.Location = New System.Drawing.Point(6, 67)
        Me.lblStandPolygon.Name = "lblStandPolygon"
        Me.lblStandPolygon.Size = New System.Drawing.Size(83, 13)
        Me.lblStandPolygon.TabIndex = 5
        Me.lblStandPolygon.Text = "lblStandPolygon"
        '
        'cmbStandPolygon
        '
        Me.cmbStandPolygon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbStandPolygon.FormattingEnabled = True
        Me.cmbStandPolygon.Location = New System.Drawing.Point(9, 84)
        Me.cmbStandPolygon.Name = "cmbStandPolygon"
        Me.cmbStandPolygon.Size = New System.Drawing.Size(268, 21)
        Me.cmbStandPolygon.TabIndex = 4
        '
        'lblSampleTreePoint
        '
        Me.lblSampleTreePoint.AutoSize = True
        Me.lblSampleTreePoint.Location = New System.Drawing.Point(6, 119)
        Me.lblSampleTreePoint.Name = "lblSampleTreePoint"
        Me.lblSampleTreePoint.Size = New System.Drawing.Size(98, 13)
        Me.lblSampleTreePoint.TabIndex = 7
        Me.lblSampleTreePoint.Text = "lblSampleTreePoint"
        '
        'cmbSampleTreePoint
        '
        Me.cmbSampleTreePoint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSampleTreePoint.FormattingEnabled = True
        Me.cmbSampleTreePoint.Location = New System.Drawing.Point(9, 136)
        Me.cmbSampleTreePoint.Name = "cmbSampleTreePoint"
        Me.cmbSampleTreePoint.Size = New System.Drawing.Size(268, 21)
        Me.cmbSampleTreePoint.TabIndex = 6
        '
        'lblPlotPoint
        '
        Me.lblPlotPoint.AutoSize = True
        Me.lblPlotPoint.Location = New System.Drawing.Point(6, 170)
        Me.lblPlotPoint.Name = "lblPlotPoint"
        Me.lblPlotPoint.Size = New System.Drawing.Size(59, 13)
        Me.lblPlotPoint.TabIndex = 9
        Me.lblPlotPoint.Text = "lblPlotPoint"
        '
        'cmbPlotPoint
        '
        Me.cmbPlotPoint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPlotPoint.FormattingEnabled = True
        Me.cmbPlotPoint.Location = New System.Drawing.Point(9, 187)
        Me.cmbPlotPoint.Name = "cmbPlotPoint"
        Me.cmbPlotPoint.Size = New System.Drawing.Size(268, 21)
        Me.cmbPlotPoint.TabIndex = 8
        '
        'uxChooseLayers
        '
        Me.uxChooseLayers.Controls.Add(Me.lblPlotPoint)
        Me.uxChooseLayers.Controls.Add(Me.lblStandPoint)
        Me.uxChooseLayers.Controls.Add(Me.cmbSampleTreePoint)
        Me.uxChooseLayers.Controls.Add(Me.uxCancel)
        Me.uxChooseLayers.Controls.Add(Me.cmbPlotPoint)
        Me.uxChooseLayers.Controls.Add(Me.cmbStandPoint)
        Me.uxChooseLayers.Controls.Add(Me.cmbStandPolygon)
        Me.uxChooseLayers.Controls.Add(Me.lblSampleTreePoint)
        Me.uxChooseLayers.Controls.Add(Me.uxOK)
        Me.uxChooseLayers.Controls.Add(Me.lblStandPolygon)
        Me.uxChooseLayers.Location = New System.Drawing.Point(12, 3)
        Me.uxChooseLayers.Name = "uxChooseLayers"
        Me.uxChooseLayers.Size = New System.Drawing.Size(289, 249)
        Me.uxChooseLayers.TabIndex = 10
        Me.uxChooseLayers.TabStop = False
        Me.uxChooseLayers.Text = "uxChooseLayers"
        '
        'ImportStandDataDialog
        '
        Me.AcceptButton = Me.uxOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.uxCancel
        Me.ClientSize = New System.Drawing.Size(314, 260)
        Me.Controls.Add(Me.uxChooseLayers)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ImportStandDataDialog"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "UpdateObjectDialog"
        Me.uxChooseLayers.ResumeLayout(False)
        Me.uxChooseLayers.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents uxOK As System.Windows.Forms.Button
    Friend WithEvents uxCancel As System.Windows.Forms.Button
    Friend WithEvents cmbStandPoint As System.Windows.Forms.ComboBox
    Friend WithEvents lblStandPoint As System.Windows.Forms.Label
    Friend WithEvents lblStandPolygon As System.Windows.Forms.Label
    Friend WithEvents cmbStandPolygon As System.Windows.Forms.ComboBox
    Friend WithEvents lblSampleTreePoint As System.Windows.Forms.Label
    Friend WithEvents cmbSampleTreePoint As System.Windows.Forms.ComboBox
    Friend WithEvents lblPlotPoint As System.Windows.Forms.Label
    Friend WithEvents cmbPlotPoint As System.Windows.Forms.ComboBox
    Friend WithEvents uxChooseLayers As System.Windows.Forms.GroupBox

End Class
