﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SnapshotDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SnapshotDialog))
        Me.uxCancel = New System.Windows.Forms.Button
        Me.uxOK = New System.Windows.Forms.Button
        Me.uxWidgetReport = New AxXtremeReportControl.AxReportControl
        Me.uxName = New System.Windows.Forms.TextBox
        Me.uxNameLabel = New System.Windows.Forms.Label
        Me.uxFilter = New System.Windows.Forms.TextBox
        Me.uxFilterLabel = New System.Windows.Forms.Label
        Me.uxTypesLabel = New System.Windows.Forms.Label
        Me.uxTypes = New System.Windows.Forms.ComboBox
        CType(Me.uxWidgetReport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'uxCancel
        '
        Me.uxCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.uxCancel.Location = New System.Drawing.Point(235, 372)
        Me.uxCancel.Name = "uxCancel"
        Me.uxCancel.Size = New System.Drawing.Size(75, 23)
        Me.uxCancel.TabIndex = 9
        Me.uxCancel.Text = "uxCancel"
        Me.uxCancel.UseVisualStyleBackColor = True
        '
        'uxOK
        '
        Me.uxOK.Enabled = False
        Me.uxOK.Location = New System.Drawing.Point(154, 372)
        Me.uxOK.Name = "uxOK"
        Me.uxOK.Size = New System.Drawing.Size(75, 23)
        Me.uxOK.TabIndex = 8
        Me.uxOK.Text = "uxOK"
        Me.uxOK.UseVisualStyleBackColor = True
        '
        'uxWidgetReport
        '
        Me.uxWidgetReport.Location = New System.Drawing.Point(12, 55)
        Me.uxWidgetReport.Name = "uxWidgetReport"
        Me.uxWidgetReport.OcxState = CType(resources.GetObject("uxWidgetReport.OcxState"), System.Windows.Forms.AxHost.State)
        Me.uxWidgetReport.Size = New System.Drawing.Size(441, 285)
        Me.uxWidgetReport.TabIndex = 5
        '
        'uxName
        '
        Me.uxName.Location = New System.Drawing.Point(13, 29)
        Me.uxName.MaxLength = 50
        Me.uxName.Name = "uxName"
        Me.uxName.Size = New System.Drawing.Size(216, 20)
        Me.uxName.TabIndex = 1
        '
        'uxNameLabel
        '
        Me.uxNameLabel.AutoSize = True
        Me.uxNameLabel.Location = New System.Drawing.Point(13, 13)
        Me.uxNameLabel.Name = "uxNameLabel"
        Me.uxNameLabel.Size = New System.Drawing.Size(72, 13)
        Me.uxNameLabel.TabIndex = 28
        Me.uxNameLabel.Text = "uxNameLabel"
        '
        'uxFilter
        '
        Me.uxFilter.Location = New System.Drawing.Point(280, 346)
        Me.uxFilter.Name = "uxFilter"
        Me.uxFilter.Size = New System.Drawing.Size(173, 20)
        Me.uxFilter.TabIndex = 6
        '
        'uxFilterLabel
        '
        Me.uxFilterLabel.AutoSize = True
        Me.uxFilterLabel.Location = New System.Drawing.Point(235, 349)
        Me.uxFilterLabel.Name = "uxFilterLabel"
        Me.uxFilterLabel.Size = New System.Drawing.Size(66, 13)
        Me.uxFilterLabel.TabIndex = 30
        Me.uxFilterLabel.Text = "uxFilterLabel"
        '
        'uxTypesLabel
        '
        Me.uxTypesLabel.AutoSize = True
        Me.uxTypesLabel.Location = New System.Drawing.Point(236, 12)
        Me.uxTypesLabel.Name = "uxTypesLabel"
        Me.uxTypesLabel.Size = New System.Drawing.Size(73, 13)
        Me.uxTypesLabel.TabIndex = 31
        Me.uxTypesLabel.Text = "uxTypesLabel"
        '
        'uxTypes
        '
        Me.uxTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.uxTypes.FormattingEnabled = True
        Me.uxTypes.Location = New System.Drawing.Point(238, 28)
        Me.uxTypes.Name = "uxTypes"
        Me.uxTypes.Size = New System.Drawing.Size(184, 21)
        Me.uxTypes.TabIndex = 32
        '
        'SnapshotDialog
        '
        Me.AcceptButton = Me.uxOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.uxCancel
        Me.ClientSize = New System.Drawing.Size(465, 404)
        Me.Controls.Add(Me.uxTypes)
        Me.Controls.Add(Me.uxTypesLabel)
        Me.Controls.Add(Me.uxFilter)
        Me.Controls.Add(Me.uxFilterLabel)
        Me.Controls.Add(Me.uxName)
        Me.Controls.Add(Me.uxWidgetReport)
        Me.Controls.Add(Me.uxNameLabel)
        Me.Controls.Add(Me.uxCancel)
        Me.Controls.Add(Me.uxOK)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SnapshotDialog"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "SnapshotDialog"
        CType(Me.uxWidgetReport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents uxCancel As System.Windows.Forms.Button
    Friend WithEvents uxOK As System.Windows.Forms.Button
    Friend WithEvents uxWidgetReport As AxXtremeReportControl.AxReportControl
    Friend WithEvents uxName As System.Windows.Forms.TextBox
    Friend WithEvents uxNameLabel As System.Windows.Forms.Label
    Friend WithEvents uxFilter As System.Windows.Forms.TextBox
    Friend WithEvents uxFilterLabel As System.Windows.Forms.Label
    Friend WithEvents uxTypesLabel As System.Windows.Forms.Label
    Friend WithEvents uxTypes As System.Windows.Forms.ComboBox
End Class
