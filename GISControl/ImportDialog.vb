﻿Imports Microsoft.Win32
Imports System.Data.OleDb
Imports System.Globalization
Imports System.IO
Imports TatukGIS.NDK
Imports XtremeReportControl

Public Class ImportDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 200
    Private Const _strIdOK As Integer = 201
    Private Const _strIdCancel As Integer = 202
    Private Const _strIdFilter As Integer = 203
    Private Const _strIdCheckAll As Integer = 204
    Private Const _strIdUncheckAll As Integer = 205
    Private Const _strIdImportColumn As Integer = 206
    Private Const _strIdMissingLayerName As Integer = 207
    Private Const _strIdEnterGeoReference As Integer = 208
    Private Const _strIdLayer As Integer = 209
    Private Const _strIdAttributes As Integer = 210
    Private Const _strIdShapes As Integer = 211
    Private Const _strIdProperties As Integer = 212
    Private Const _strIdNewHDLayer As Integer = 213
    Private Const _strIdNewDBLayer As Integer = 214
    Private Const _strIdExistingLayer As Integer = 215
    Private Const _strIdImportProperties As Integer = 216
    Private Const _strIdCounty As Integer = 217
    Private Const _strIdMunicipal As Integer = 218
    Private Const _strIdParish As Integer = 219
    Private Const _strIdPropNumber As Integer = 220
    Private Const _strIdPropName As Integer = 221
    Private Const _strIdBlock As Integer = 222
    Private Const _strIdUnit As Integer = 223
    Private Const _strIdArea As Integer = 224
    Private Const _strIdMeasuredArea As Integer = 225
    Private Const _strIdObjID As Integer = 226
    Private Const _strIdMissingPropertyKeyField As Integer = 227
    Private Const _strIdMissingProjection As Integer = 228
    Private Const _strIdConfirmAbort As Integer = 229
    Private Const _strIdNext As Integer = 230
    Private Const _strIdPrevious As Integer = 231
    Private Const _strIdQuickFilter As Integer = 232
    Private Const _strIdDescriptionLayer As Integer = 233
    Private Const _strIdDescriptionShapes As Integer = 234
    Private Const _strIdDescriptionAttributes As Integer = 235
    Private Const _strIdDescriptionProperties As Integer = 236
    Private Const _strIdDescriptionSections As Integer = 237
    Private Const _strIdSections As Integer = 238
    Private Const _strIdCreateSections As Integer = 239
#End Region

#Region "Declarations"
    Private _activeLayerName As String
    Private _fileName As String
    Private _gisCtrl As TatukGIS.NDK.WinForms.TGIS_ViewerWnd
    Private _importLayer As TGIS_LayerAbstract
    Private _lastSelectedLayer As Integer = -1
    Private _filterConditions As New Hashtable
    Private _tabIndex As Integer
    Private _tabsEnabled() As Boolean = {True, True, True, True, True}
    Private _sectionData As New List(Of GisControlView.SectionEventArgs.SectionData)
    Private _sectionFieldsExist As Boolean
    Private _destinationLayer As String

    Private Const _checkIconId As Integer = 1
    Private Const _tabIdLayer As Integer = 0
    Private Const _tabIdShapes As Integer = 1
    Private Const _tabIdSections As Integer = 2
    Private Const _tabIdAttributes As Integer = 3
    Private Const _tabIdProperties As Integer = 4

    Private Enum ImportChoice As Integer
        NewHDLayer
        NewDBLayer
        ExistingLayer
    End Enum

    Public Sub New(ByVal fileName As String, ByVal activeLayerName As String)
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _activeLayerName = activeLayerName
        _fileName = fileName
        _gisCtrl = GisControlView.Instance.GisCtrl 'Shorthand

        Try
            'Propose filename (exclude extension) as default layer name
            Dim fi As New System.IO.FileInfo(_fileName)
            txtTitle.Text = Microsoft.VisualBasic.Left(fi.Name, fi.Name.Length - fi.Extension.Length)
        Catch ex As ArgumentException
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Public ReadOnly Property DestinationLayer() As String
        Get
            Return _destinationLayer
        End Get
    End Property

    Public ReadOnly Property SectionData() As List(Of GisControlView.SectionEventArgs.SectionData)
        Get
            Return _sectionData
        End Get
    End Property
#End Region

#Region "Helper functions"
    Private Function CheckImportRequirements() As Boolean
        'Make sure layer name is not empty
        If txtTitle.Enabled And Trim(txtTitle.Text) = vbNullString Then
            MsgBox(GisControlView.LangStr(_strIdMissingLayerName), MsgBoxStyle.Exclamation)
            Return False
        End If

        'Make sure at least one key field is mapped for propery import
        If _tabsEnabled(_tabIdProperties) AndAlso chkAddProperties.Checked AndAlso cmbPropNumber.SelectedIndex <= 0 AndAlso (cmbPropName.SelectedIndex <= 0 OrElse cmbBlock.SelectedIndex <= 0 OrElse cmbUnit.SelectedIndex <= 0) Then
            MsgBox(GisControlView.LangStr(_strIdMissingPropertyKeyField), MsgBoxStyle.Exclamation)
            Return False
        End If

        Return True
    End Function

    Private Sub CheckGeoreferenceFile()
        Dim fi As New System.IO.FileInfo(_importLayer.Path)
        Dim ext As String = fi.Extension.ToUpperInvariant()
        Dim georef As Boolean = False
        Dim georefFile As String
        Dim lp As TGIS_LayerPixel

        lp = TryCast(_importLayer, TGIS_LayerPixel)
        If lp IsNot Nothing Then
            'If layer.BitWidth is close to Layer.Extent.XMax-Layer.Extent.XMin then layer is probably not georeferenced
            'Select "2" to avoid all issues that some layers are set on a borders, some on mid pixels etc.
            If Math.Abs(lp.BitWidth - (lp.Extent.XMax - lp.Extent.XMin)) < 2 Then
                'Check if this is an image file that may be georeferenced
                If ext = ".JPG" OrElse ext = ".JPEG" Then
                    ext = ".JGW"
                    georef = True
                ElseIf ext = ".PNG" Then
                    ext = ".PGW"
                    georef = True
                ElseIf ext = ".BMP" Then
                    ext = ".BPW"
                    georef = True
                ElseIf ext = ".TIF" OrElse ext = ".TIFF" Then
                    ext = ".TFW"
                    georef = True
                End If

                'Check for georeference file
                georefFile = fi.DirectoryName + "\" + Microsoft.VisualBasic.Left(fi.Name, fi.Name.Length - fi.Extension.Length) + ext
                If georef = True AndAlso Not System.IO.File.Exists(georefFile) AndAlso _
                MsgBox(GisControlView.LangStr(_strIdEnterGeoReference), MsgBoxStyle.YesNo Or MsgBoxStyle.Question) = MsgBoxResult.Yes Then
                    'Georeference file not found - let user specify reference point
                    Dim dlg As New GeoreferenceDialog
                    If dlg.ShowDialog() = Windows.Forms.DialogResult.Cancel Then Exit Sub

                    'Write georeference file
                    Dim fs As New FileStream(georefFile, FileMode.OpenOrCreate, FileAccess.Write)
                    Dim s As New StreamWriter(fs)
                    s.WriteLine(dlg.txtScaleX.Text)
                    s.WriteLine(0)
                    s.WriteLine(0)
                    s.WriteLine(-dlg.txtScaleY.Text)
                    s.WriteLine(dlg.txtReferenceX.Text)
                    s.WriteLine(dlg.txtReferenceY.Text)
                    s.Close()
                    fs.Close()
                End If
            End If
        End If
    End Sub

    Private Function CheckProjectionSettings() As Boolean
        'Check for projection settings, if missing let user specify
        If _importLayer.CS.EPSG = 0 Then
            Select Case MsgBox(GisControlView.LangStr(_strIdMissingProjection) & " '" & _gisCtrl.CS.Description & "'?", MsgBoxStyle.YesNoCancel Or MsgBoxStyle.Question)
                Case MsgBoxResult.Yes
                    _importLayer.CS = _gisCtrl.CS
                Case MsgBoxResult.No
                    Dim dlg As New CoordinateSystemDialog(Nothing)
                    If dlg.ShowDialog() = Windows.Forms.DialogResult.OK Then
                        _importLayer.CS = dlg.CS
                    Else
                        Return False
                    End If
                Case MsgBoxResult.Cancel
                    Return False
            End Select
        End If

        Return True
    End Function

    Private Sub SaveSettings()
        Dim regKey As RegistryKey

        'Save layer choice to registry
        regKey = Registry.CurrentUser.OpenSubKey(GisControlView.RegKeyGis, True)
        If regKey IsNot Nothing Then
            Dim val As ImportChoice
            If optNewHDLayer.Checked Then
                val = ImportChoice.NewHDLayer
            ElseIf optNewDBLayer.Checked Then
                val = ImportChoice.NewDBLayer
            ElseIf optExistingLayer.Checked Then
                val = ImportChoice.ExistingLayer
            End If
            regKey.SetValue(GisControlView.RegValImportChoice, DirectCast(val, Integer))
            regKey.Close()
        End If
    End Sub

    Private Sub ImportVectorLayerToDB()
        Dim cmbField As ComboBox
        Dim lblField As Label
        Dim controls As Control()
        Dim ll As TGIS_LayerVector = Nothing, il As TGIS_LayerVector = CType(_importLayer, TGIS_LayerVector)
        Dim shp As TGIS_Shape, shpIl As TGIS_Shape
        Dim idx As Integer
        Dim progress As New ProgressDialog
        Dim type As RelationType
        Dim i As Integer, j As Integer
        Dim dgv As Double
        Dim si As Integer

        Cursor.Current = Cursors.WaitCursor

        Try
            If optNewDBLayer.Checked Then
                'Create a new layer
                If chkAddProperties.Checked Then
                    type = RelationType.PropertyRelation
                ElseIf uxCreateSections.Checked Then
                    type = RelationType.SectionRelation
                Else
                    type = RelationType.Undefined
                End If

                ll = _gisCtrl.Get(GisControlView.Instance.AddNewLayer(True, txtTitle.Text, type))
                ll.CS = _importLayer.CS
                ll.ZOrder = 0 'Topmost
            Else
                'Use selected layer
                ll = _gisCtrl.Get(CType(cmbLayers.SelectedItem, NameCaptionPair).Name)
                type = GisControlView.GetLayerRelationType(ll.Name)

                'Make sure layer has proper relation type
                If chkAddProperties.Checked AndAlso type = RelationType.Undefined Then
                    GisControlView.UpdateLayerRelation(ll.Name, ll.Caption, ll.CS.EPSG, RelationType.PropertyRelation)
                    type = RelationType.PropertyRelation
                ElseIf uxCreateSections.Checked AndAlso type = RelationType.Undefined Then
                    GisControlView.UpdateLayerRelation(ll.Name, ll.Caption, ll.CS.EPSG, RelationType.SectionRelation)
                    type = RelationType.SectionRelation
                End If
            End If

            'Remember destination layer (only db vector layers)
            _destinationLayer = ll.Name

            'Copy shape data to specified layer
            If ll IsNot Nothing Then
                progress.Show(Me, GisControlView.LangStr(_strIdConfirmAbort))
                Me.Enabled = False

                'If this is a new layer add all fields
                If optNewDBLayer.Checked Then
                    For i = 0 To il.Fields.Count - 1
                        If ll.FindField(il.FieldInfo(i).NewName) = -1 Then
                            ll.AddField(il.FieldInfo(i).NewName, il.FieldInfo(i).FieldType, il.FieldInfo(i).Width, il.FieldInfo(i).Decimal)
                        End If
                    Next
                End If

                'Copy shapes and field data
                For Each rec As ReportRecord In uxShapeList.Records
                    'Only import shapes that are checked in list
                    If rec.Item(0).Checked Then
                        shpIl = il.GetShape(rec.Tag)
                        shp = ll.AddShape(shpIl)

                        'Create section if checked in section list (data will be handled by GisControlView later)
                        If uxCreateSections.Checked Then
                            For Each r2 As ReportRecord In uxSectionList.Records
                                If r2.Tag = rec.Tag AndAlso r2.Item(0).Checked Then
                                    'Get DGV if value exist
                                    If shpIl.Layer.FindField(GisControlView.FieldNewNameSectionDGV) >= 0 Then
                                        dgv = shpIl.GetField(GisControlView.FieldNewNameSectionDGV)
                                    Else
                                        dgv = 0
                                    End If

                                    'Get SI depending on attribute name #5119
                                    If shpIl.Layer.FindField(GisControlView.FieldNewNameSectionStandortsi) >= 0 Then
                                        si = shpIl.GetField(GisControlView.FieldNewNameSectionStandortsi)
                                    Else
                                        si = shpIl.GetField(GisControlView.FieldNameSectionStandortsi)
                                    End If

                                    _sectionData.Add(New GisControlView.SectionEventArgs.SectionData(shp.Uid, _
                                                                                                     shpIl.GetField(GisControlView.FieldNewNameSectionId), _
                                                                                                     shpIl.GetField(GisControlView.FieldNewNameSectionAvdnr), _
                                                                                                     shpIl.GetField(GisControlView.FieldNewNameSectionAgoslag), _
                                                                                                     shpIl.GetField(GisControlView.FieldNewNameSectionMalklass), _
                                                                                                     shpIl.GetField(GisControlView.FieldNewNameSectionHkl), _
                                                                                                     shpIl.GetField(GisControlView.FieldNewNameSectionAlder), _
                                                                                                     shpIl.GetField(GisControlView.FieldNewNameSectionAtgard), _
                                                                                                     shpIl.GetField(GisControlView.FieldNewNameSectionAtgardof), _
                                                                                                     shpIl.GetField(GisControlView.FieldNewNameSectionHasof), _
                                                                                                     shpIl.GetField(GisControlView.FieldNewNameSectionSkifte), _
                                                                                                     shpIl.GetField(GisControlView.FieldNewNameSectionMedelhojd), _
                                                                                                     shpIl.GetField(GisControlView.FieldNewNameSectionVolym), _
                                                                                                     shpIl.GetField(GisControlView.FieldNewNameSectionFuktklass), _
                                                                                                     si, _
                                                                                                     shpIl.GetField(GisControlView.FieldNewNameSectionVegitation), _
                                                                                                     Math.Round(shpIl.Layer.CS.ToWGS(shpIl.Centroid()).Y / Math.PI * 180.0), _
                                                                                                     Math.Round(shpIl.Layer.CS.ToWGS(shpIl.Centroid()).X / Math.PI * 180.0), _
                                                                                                     shpIl.GetField(GisControlView.FieldNewNameSectionGrundyta), _
                                                                                                     shpIl.GetField(GisControlView.FieldNewNameSectionFramskrive), _
                                                                                                     dgv))
                                    Exit For
                                End If
                            Next
                        End If

                        'Field mapping - copy all fields if this is a new layer, otherwise use mapping
                        If optNewDBLayer.Checked Then
                            For i = 0 To il.Fields.Count - 1
                                If shp IsNot Nothing Then shp.SetField(il.FieldInfo(i).NewName, shpIl.GetField(il.FieldInfo(i).NewName))
                            Next
                        ElseIf uxCreateSections.Checked Then
                            'Make sure destination layer contain section fields
                            MakeSureSectionFieldsExist(ll)

                            'Copy values
                            For Each field As TGIS_FieldInfo In CType(_importLayer, TGIS_LayerVector).Fields
                                If Not field.IsUID Then shp.SetField(field.Name, shpIl.GetField(field.NewName))
                            Next
                        Else
                            'Use attribute mapping
                            i = 0
                            Do
                                controls = uxFieldPanel.Controls.Find("lblField" & i, False) : If controls.GetLength(0) = 0 Then Exit Do
                                lblField = CType(controls(0), Windows.Forms.Label)
                                controls = uxFieldPanel.Controls.Find("cmbField" & i, False) : If controls.GetLength(0) = 0 Then Exit Do
                                cmbField = CType(controls(0), Windows.Forms.ComboBox)

                                'Copy field value
                                If cmbField.SelectedIndex > 0 Then
                                    For j = 0 To il.Fields.Count - 1
                                        If il.FieldInfo(j).NewName = cmbField.SelectedItem Then
                                            shp.SetField(lblField.Text, shpIl.GetField(il.FieldInfo(j).NewName))
                                        End If
                                    Next
                                End If

                                i += 1
                            Loop
                        End If

                        'Add to property table if selected
                        If chkAddProperties.Checked AndAlso (type = RelationType.PropertyRelation OrElse type = RelationType.Undefined) Then
                            Dim county As String = vbNullString, municipal As String = vbNullString, parish As String = vbNullString
                            Dim propNumber As String = vbNullString, propName As String = vbNullString
                            Dim block As String = vbNullString, unit As String = vbNullString
                            Dim area As Double, measuredArea As Double
                            Dim objId As String = vbNullString

                            'Get mapped values from shape attributes
                            If cmbCounty.SelectedIndex > 0 Then county = shpIl.GetField(cmbCounty.SelectedItem)
                            If cmbMunicipal.SelectedIndex > 0 Then municipal = shpIl.GetField(cmbMunicipal.SelectedItem)
                            If cmbParish.SelectedIndex > 0 Then parish = shpIl.GetField(cmbParish.SelectedItem)
                            If cmbPropNumber.SelectedIndex > 0 Then propNumber = shpIl.GetField(cmbPropNumber.SelectedItem)
                            If cmbPropName.SelectedIndex > 0 Then propName = shpIl.GetField(cmbPropName.SelectedItem)
                            If cmbBlock.SelectedIndex > 0 Then block = shpIl.GetField(cmbBlock.SelectedItem)
                            If cmbUnit.SelectedIndex > 0 Then unit = shpIl.GetField(cmbUnit.SelectedItem)
                            Try
                                If cmbArea.SelectedIndex > 0 Then area = shpIl.GetField(cmbArea.SelectedItem)
                            Catch ex As InvalidCastException
                            End Try
                            Try
                                If cmbMeasuredArea.SelectedIndex > 0 Then measuredArea = shpIl.GetField(cmbMeasuredArea.SelectedItem)
                            Catch ex As InvalidCastException
                            End Try
                            If cmbObjID.SelectedIndex > 0 Then objId = shpIl.GetField(cmbObjID.SelectedItem)

                            'Add property record and bind it to the shape
                            shp.SetField(GisControlView.FieldNameRelation, GisControlView.AddProperty(county, municipal, parish, propNumber, propName, block, unit, objId, area, measuredArea, shpIl))
                        End If
                    End If

                    'Update progress bar
                    progress.ProgressBar.Value = (idx / uxShapeList.Records.Count) * 100
                    idx += 1

                    'Check if user canceled this operation
                    If progress.UserClosed Then Exit For

                    Application.DoEvents()
                Next

                If progress.UserClosed Then
                    'User cancelled - remove any imported shapes
                    ll.RevertAll()
                    If optNewDBLayer.Checked Then
                        GisControlView.Instance.RemoveLayer(ll.Name) 'Remove newly created layer from db
                        ll = Nothing
                    End If
                Else
                    'Import successful - save layer data
                    ll.SaveData()
                End If

                Me.Enabled = True 'Enable before progress dialog is close so we don't lose focus
                progress.Close()
            End If
        Catch ex As EGIS_Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

        Cursor.Current = Cursors.Default
    End Sub

    Private Sub ImportPixelLayerToDB()
        Dim lSql As TGIS_LayerAbstract = New TGIS_LayerPixelStoreAdo2
        Dim ll As TGIS_LayerAbstract
        Dim lp As TGIS_LayerPixel = CType(_importLayer, TGIS_LayerPixel)
        Dim layerName As String = GisControlView.GetUniqueLayerName()
        Dim h As New Hashtable
        Dim config As String
        Dim i As Integer

        Cursor.Current = Cursors.WaitCursor

        Try
            'Prepare config string
            config = "Storage=PixelStore2\n" & _
                     "Layer=" & GisControlView.GisTablePrefix & layerName & "\n" & _
                     "Dialect=MSSQL\n" & _
                     "ADO=" & GisControlView.ConnStr & "\n" & _
                     ".ttkps"

            'Hide all layers (we need to do this to make sure the background is clean because when exporting to a PixelStore layer the generated image will always be quadratic)
            For i = 0 To _gisCtrl.Items.Count - 1
                ll = CType(_gisCtrl.Items(i), TGIS_LayerAbstract)
                h.Add(ll.Name, ll.Active)
                ll.Active = False
            Next

            'Turn off transparency before import as PixelStore layers does not handle tranparency #3045
            CType(_importLayer, TGIS_LayerPixel).Params.Pixel.AlphaBand = -1

            'Import image to a PixelStore2 layer (we do this by importing a normal pixel layer and then exporting its extent to a PixelStore layer)
            _importLayer.Caption = txtTitle.Text 'Set caption here too as this will be temporary shown in the legend
            _importLayer.Name = layerName
            _gisCtrl.Add(_importLayer)
            _gisCtrl.ExportToImage(config, _importLayer.ProjectedExtent, lp.CellWidth, lp.CellHeight, 80, 100, 96) 'Subformat 100 = 24 bit PNG
            _gisCtrl.Delete(_importLayer.Name)

            'Restore all layers
            For i = 0 To _gisCtrl.Items.Count - 1
                ll = CType(_gisCtrl.Items(i), TGIS_LayerAbstract)
                If h.Contains(ll.Name) Then ll.Active = h(ll.Name)
            Next

            'Load the newly created pixel layer
            lSql.Caption = txtTitle.Text
            lSql.Name = layerName
            lSql.Path = config
            _gisCtrl.Add(lSql)

            'Add to layer index table
            Dim cmd As New OleDbCommand("", GisControlView.Conn)
            cmd.CommandText = "INSERT INTO gis_layers (name, caption, epsg) VALUES('" & layerName & "', '" & Microsoft.VisualBasic.Left(txtTitle.Text, GisControlView.FieldLengthLayerCaption).Replace("'", "''") & "'," & _importLayer.CS.EPSG & ")"
            cmd.ExecuteNonQuery()
        Catch ex As EGIS_Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

        Cursor.Current = Cursors.Default
    End Sub

    Private Sub ImportLayerAsFile()
        'Let TatukGIS set the layername here, it will automatically append the name with an index if necessary
        _importLayer.Caption = txtTitle.Text
        _gisCtrl.Add(_importLayer)
        _importLayer.ZOrder = 0 'Topmost

        'Add layer to index table
        Dim cmd As New OleDbCommand("", GisControlView.Conn)
        cmd.CommandText = "INSERT INTO gis_layers (name, caption, epsg) VALUES('" & _importLayer.Name & "', '" & Microsoft.VisualBasic.Left(txtTitle.Text, GisControlView.FieldLengthLayerCaption).Replace("'", "''") & "', " & _importLayer.CS.EPSG & ")"
        cmd.ExecuteNonQuery()
    End Sub

    Private Function DoOK() As Boolean
        'Check prerequirements
        If Not CheckImportRequirements() Then Return False
        CheckGeoreferenceFile()
        If Not CheckProjectionSettings() Then Return False

        SaveSettings()

        'Import layer data
        If optNewDBLayer.Checked OrElse optExistingLayer.Checked Then
            If TypeOf _importLayer Is TGIS_LayerVector Then
                ImportVectorLayerToDB()
            ElseIf TypeOf _importLayer Is TGIS_LayerPixel Then
                ImportPixelLayerToDB()
            End If
        Else
            ImportLayerAsFile()
        End If

        Return True
    End Function

    Private Sub CheckVisibleItems(ByVal checked As Boolean, ByVal reportCtrl As AxXtremeReportControl.AxReportControl)
        Dim i As Integer
        Dim record As ReportRecord

        'Loop through visible rows
        For i = 0 To reportCtrl.Rows.Count - 1
            'Make sure row has a corresponding record (i.e. not a grouping row), set checked state
            record = reportCtrl.Rows(i).Record
            If record IsNot Nothing AndAlso record.Visible Then
                record(0).Checked = checked
            End If
        Next

        'Redraw control
        reportCtrl.Redraw()
    End Sub

    Private Sub CheckSectionFields()
        Dim ll As TGIS_LayerVector = TryCast(_importLayer, TGIS_LayerVector)

        'Check if layer contain fields required for section creation (DGV is optional, use FindField wrapper as TGIS_LayerVector seems buggy)
        If ll IsNot Nothing Then
            If _
            FindField(ll, GisControlView.FieldNameSectionId) <> -1 AndAlso _
            FindField(ll, GisControlView.FieldNameSectionAvdnr) <> -1 AndAlso _
            FindField(ll, GisControlView.FieldNameSectionAgoslag) <> -1 AndAlso _
            FindField(ll, GisControlView.FieldNameSectionMalklass) <> -1 AndAlso _
            FindField(ll, GisControlView.FieldNameSectionHkl) <> -1 AndAlso _
            FindField(ll, GisControlView.FieldNameSectionAlder) <> -1 AndAlso _
            FindField(ll, GisControlView.FieldNameSectionAtgard) <> -1 AndAlso _
            FindField(ll, GisControlView.FieldNameSectionAtgardof) <> -1 AndAlso _
            FindField(ll, GisControlView.FieldNameSectionHasof) <> -1 AndAlso _
            FindField(ll, GisControlView.FieldNameSectionSkifte) <> -1 AndAlso _
            FindField(ll, GisControlView.FieldNameSectionMedelhojd) <> -1 AndAlso _
            FindField(ll, GisControlView.FieldNameSectionVolym) <> -1 AndAlso _
            FindField(ll, GisControlView.FieldNameSectionFuktklass) <> -1 AndAlso _
            FindField(ll, GisControlView.FieldNameSectionStandortsi) <> -1 AndAlso _
            FindField(ll, GisControlView.FieldNameSectionVegitation) <> -1 AndAlso _
            FindField(ll, GisControlView.FieldNameSectionGrundyta) <> -1 AndAlso _
            FindField(ll, GisControlView.FieldNameSectionFramskrive) <> -1 _
            Then
                _sectionFieldsExist = True
            End If
        End If
    End Sub

    Private Sub MakeSureSectionFieldsExist(ByVal ll As TGIS_LayerVector)
        'Add section fields to layer (DGV is optional)
        If ll.FindField(GisControlView.FieldNameSectionId) < 0 Then ll.AddField(GisControlView.FieldNameSectionId, TGIS_FieldType.gisFieldTypeNumber, 10, 0)
        If ll.FindField(GisControlView.FieldNameSectionAvdnr) < 0 Then ll.AddField(GisControlView.FieldNameSectionAvdnr, TGIS_FieldType.gisFieldTypeString, 10, 0)
        If ll.FindField(GisControlView.FieldNameSectionAgoslag) < 0 Then ll.AddField(GisControlView.FieldNameSectionAgoslag, TGIS_FieldType.gisFieldTypeNumber, 10, 0)
        If ll.FindField(GisControlView.FieldNameSectionMalklass) < 0 Then ll.AddField(GisControlView.FieldNameSectionMalklass, TGIS_FieldType.gisFieldTypeNumber, 10, 0)
        If ll.FindField(GisControlView.FieldNameSectionHkl) < 0 Then ll.AddField(GisControlView.FieldNameSectionHkl, TGIS_FieldType.gisFieldTypeNumber, 10, 0)
        If ll.FindField(GisControlView.FieldNameSectionAlder) < 0 Then ll.AddField(GisControlView.FieldNameSectionAlder, TGIS_FieldType.gisFieldTypeNumber, 10, 0)
        If ll.FindField(GisControlView.FieldNameSectionAtgard) < 0 Then ll.AddField(GisControlView.FieldNameSectionAtgard, TGIS_FieldType.gisFieldTypeNumber, 10, 0)
        If ll.FindField(GisControlView.FieldNameSectionAtgardof) < 0 Then ll.AddField(GisControlView.FieldNameSectionAtgardof, TGIS_FieldType.gisFieldTypeNumber, 10, 0)
        If ll.FindField(GisControlView.FieldNameSectionHasof) < 0 Then ll.AddField(GisControlView.FieldNameSectionHasof, TGIS_FieldType.gisFieldTypeNumber, 10, 0)
        If ll.FindField(GisControlView.FieldNameSectionSkifte) < 0 Then ll.AddField(GisControlView.FieldNameSectionSkifte, TGIS_FieldType.gisFieldTypeNumber, 10, 0)
        If ll.FindField(GisControlView.FieldNameSectionMedelhojd) < 0 Then ll.AddField(GisControlView.FieldNameSectionMedelhojd, TGIS_FieldType.gisFieldTypeNumber, 10, 0)
        If ll.FindField(GisControlView.FieldNameSectionVolym) < 0 Then ll.AddField(GisControlView.FieldNameSectionVolym, TGIS_FieldType.gisFieldTypeNumber, 10, 0)
        If ll.FindField(GisControlView.FieldNameSectionFuktklass) < 0 Then ll.AddField(GisControlView.FieldNameSectionFuktklass, TGIS_FieldType.gisFieldTypeNumber, 10, 0)
        If ll.FindField(GisControlView.FieldNameSectionStandortsi) < 0 Then ll.AddField(GisControlView.FieldNameSectionStandortsi, TGIS_FieldType.gisFieldTypeNumber, 10, 0)
        If ll.FindField(GisControlView.FieldNameSectionVegitation) < 0 Then ll.AddField(GisControlView.FieldNameSectionVegitation, TGIS_FieldType.gisFieldTypeNumber, 10, 0)
        If ll.FindField(GisControlView.FieldNameSectionGrundyta) < 0 Then ll.AddField(GisControlView.FieldNameSectionGrundyta, TGIS_FieldType.gisFieldTypeFloat, 0, 0)
        If ll.FindField(GisControlView.FieldNameSectionFramskrive) < 0 Then ll.AddField(GisControlView.FieldNameSectionFramskrive, TGIS_FieldType.gisFieldTypeString, 10, 0)
    End Sub

    Private Sub SwitchImportMode()
        'Enable create sections
        uxCreateSections.Enabled = _sectionFieldsExist

        'Enable/disable tabs depending on import settings
        If optNewHDLayer.Checked Then
            txtTitle.Enabled = True
            cmbLayers.Enabled = False

            _tabsEnabled(_tabIdShapes) = False
            _tabsEnabled(_tabIdSections) = False
            _tabsEnabled(_tabIdAttributes) = False
            _tabsEnabled(_tabIdProperties) = False
        ElseIf optNewDBLayer.Checked Then
            _tabsEnabled(_tabIdAttributes) = False
            If TypeOf _importLayer Is TGIS_LayerVector Then
                'Shape file
                _tabsEnabled(_tabIdShapes) = True
                _tabsEnabled(_tabIdSections) = uxCreateSections.Checked AndAlso GisControlView.EstimateSodraInstalled
                _tabsEnabled(_tabIdProperties) = Not uxCreateSections.Checked OrElse Not GisControlView.EstimateSodraInstalled 'Enable only when section import is disabled (if EstimateSödra is installed)
            ElseIf _importLayer IsNot Nothing Then
                'Image
                _tabsEnabled(_tabIdShapes) = False
                _tabsEnabled(_tabIdSections) = False
                _tabsEnabled(_tabIdProperties) = False
            End If
            txtTitle.Enabled = True
            cmbLayers.Enabled = False
        ElseIf optExistingLayer.Checked Then
            Dim i As Integer
            Dim fieldCount As Integer
            Dim ll As TGIS_LayerVector = CType(_gisCtrl.Get(CType(cmbLayers.SelectedItem, NameCaptionPair).Name), TGIS_LayerVector)
            Dim type As RelationType

            txtTitle.Enabled = False
            cmbLayers.Enabled = True

            _tabsEnabled(_tabIdShapes) = True
            _tabsEnabled(_tabIdSections) = uxCreateSections.Checked AndAlso GisControlView.EstimateSodraInstalled

            'Enable create section only if relation type match
            type = GisControlView.GetLayerRelationType(ll.Name)
            uxCreateSections.Enabled = _sectionFieldsExist AndAlso (type = RelationType.SectionRelation OrElse type = RelationType.Undefined)

            'Count number of fields in target layer 
            For i = 0 To ll.Fields.Count - 1
                If ll.FieldInfo(i).IsUID OrElse GisControlView.IsRelationField(ll.FieldInfo(i).Name) Then Continue For
                fieldCount += 1
            Next

            'Enable attributes tab only if attributes exists in both source and target layer
            If CType(_importLayer, TGIS_LayerVector).Fields.Count > 0 AndAlso fieldCount > 0 AndAlso Not uxCreateSections.Checked Then
                _tabsEnabled(_tabIdAttributes) = True
            Else
                _tabsEnabled(_tabIdAttributes) = False
            End If

            'Relation type should not be changed so if layer is related to anything but properties this import will be disabled
            _tabsEnabled(_tabIdProperties) = Not uxCreateSections.Checked OrElse Not GisControlView.EstimateSodraInstalled 'Enable only when section import is disabled (if EstimateSödra is installed)
            If cmbLayers.SelectedItem IsNot Nothing Then
                Dim rt As RelationType = GisControlView.GetLayerRelationType(CType(cmbLayers.SelectedItem, NameCaptionPair).Name)
                If rt <> RelationType.PropertyRelation AndAlso rt <> RelationType.Undefined Then
                    _tabsEnabled(_tabIdProperties) = False
                End If
            End If
        End If

        'Uncheck create sections when disabled
        If Not uxCreateSections.Enabled Then uxCreateSections.Checked = False

        'Refresh tab so OK/Next button will be displayed correctly
        SetCurrentTab(_tabIndex)
    End Sub

    Private Sub ListSectionData()
        Dim i As Integer
        Dim col As ReportColumn, c2 As ReportColumn
        Dim rec As ReportRecord, r2 As ReportRecord
        Dim item As ReportRecordItem

        uxSectionList.Records.DeleteAll()
        uxSectionList.Columns.DeleteAll()

        'Copy columns from shape list
        For Each col In uxShapeList.Columns
            c2 = uxSectionList.Columns.Add(col.Index, col.Caption, col.Width, col.Resizable)
            c2.MinimumWidth = col.MinimumWidth
            c2.Width = col.Width
            c2.Icon = col.Icon
        Next

        'Copy shape list records
        For Each rec In uxShapeList.Records
            If rec.Item(0).Checked Then
                r2 = uxSectionList.Records.Add()
                r2.Tag = rec.Tag
                For i = 0 To rec.ItemCount - 1
                    item = r2.AddItem(rec.Item(i).Caption)
                    item.HasCheckbox = rec.Item(i).HasCheckbox
                Next
            End If
        Next

        uxSectionList.Populate()
    End Sub

    Private Sub ListFieldData()
        Dim i As Integer, idx As Integer
        Dim shp As TGIS_Shape
        Dim sql As String
        Dim record As ReportRecord
        Dim item As ReportRecordItem

        Cursor.Current = Cursors.WaitCursor

        'Clear any existing field data
        uxShapeList.Records.DeleteAll()
        uxShapeList.Columns.DeleteAll()
        uxShapeList.Populate()

        'List field data (layer vectors only)
        If _importLayer IsNot Nothing And TypeOf _importLayer Is TGIS_LayerVector Then
            Dim il As TGIS_LayerVector = CType(_importLayer, TGIS_LayerVector)

            'Add columns
            Dim col As ReportColumn = uxShapeList.Columns.Add(0, GisControlView.LangStr(_strIdImportColumn), 5, True)
            col.MinimumWidth = 20
            col.Width = 20
            col.Icon = _checkIconId
            For i = 0 To il.Fields.Count - 1
                uxShapeList.Columns.Add(i + 1, il.FieldInfo(i).NewName, 95, True).Visible = Not GisControlView.IsRelationField(il.FieldInfo(i).NewName)
            Next

            'Get SQL query conditions
            sql = GisControlView.GetFilterConditionString(_importLayer, _filterConditions)

            'Add field data to preview grid, use progress bar to display progress
            idx = 0
            shp = il.FindFirst(il.Extent, sql)
            Do Until shp Is Nothing
                If shp IsNot Nothing And il.Fields.Count > 0 Then
                    record = uxShapeList.Records.Add()
                    record.Tag = shp.Uid
                    item = record.AddItem(vbNullString)
                    item.HasCheckbox = True
                    For i = 0 To il.Fields.Count - 1
                        If il.FieldInfo(i).FieldType = TGIS_FieldType.gisFieldTypeDate Then
                            record.AddItem(Microsoft.VisualBasic.FormatDateTime(Convert.ToDateTime(shp.GetField(il.FieldInfo(i).NewName), CultureInfo.CurrentCulture), DateFormat.GeneralDate))
                        Else
                            record.AddItem(CStr(shp.GetField(il.FieldInfo(i).NewName)))
                        End If
                    Next
                End If

                idx += 1

                shp = il.FindNext()
            Loop

            uxShapeList.Populate()
        End If

        Cursor.Current = Cursors.Default
    End Sub

    Private Sub ListFields()
        Dim cmbField As ComboBox = Nothing
        Dim lblfield As Label
        Dim ll As TGIS_LayerVector
        Dim fieldCount As Integer = 0
        Dim i As Integer, j As Integer

        ll = _gisCtrl.Get(CType(cmbLayers.SelectedItem, NameCaptionPair).Name)

        'List fields of selected layer
        uxFieldPanel.Controls.Clear()
        If ll IsNot Nothing Then
            'Add fields of existing layer
            For i = 0 To ll.Fields.Count - 1
                'Skip reserved fields
                If ll.FieldInfo(i).IsUID OrElse _
                ll.FieldInfo(i).Name.ToUpperInvariant() = GisControlView.FieldNameRelation OrElse _
                ll.FieldInfo(i).Name.ToUpperInvariant() = GisControlView.FieldNameSubrelation Then
                    Continue For
                End If

                'Add label
                lblfield = New Label
                lblfield.AutoSize = False
                lblfield.Text = ll.FieldInfo(i).Name
                lblfield.Name = "lblField" & fieldCount
                lblfield.Height = 13
                lblfield.Width = 86
                lblfield.Location = New Point(3, 24 * fieldCount + 9)
                uxFieldPanel.Controls.Add(lblfield)

                'Add combo
                cmbField = New ComboBox
                cmbField.DropDownStyle = ComboBoxStyle.DropDownList
                cmbField.Name = "cmbField" & fieldCount
                cmbField.Height = 21
                cmbField.Width = 150
                cmbField.Location = New Point(100, 24 * fieldCount + 3)
                uxFieldPanel.Controls.Add(cmbField)

                'List fields of import layer in current combo, select value when a match is found
                If CType(_importLayer, TGIS_LayerVector).Fields.Count > 0 Then cmbField.Items.Add(" ") 'Empty option
                For j = 0 To CType(_importLayer, TGIS_LayerVector).Fields.Count - 1
                    Dim fieldName As String = CType(_importLayer, TGIS_LayerVector).FieldInfo(j).Name
                    Dim idx As Integer = cmbField.Items.Add(fieldName)
                    If fieldName = ll.FieldInfo(i).Name Then cmbField.SelectedIndex = idx
                Next

                fieldCount += 1 'Dedicated field counter (some fields are skipped and thus not counted)
            Next
        End If
    End Sub

    Private Function FindField(ByVal ll As TGIS_LayerVector, ByVal name As String) As Integer
        'FindField wrapper implemented as TGIS_LayerVector.FindField seems buggy with more that 16 fields (NDK 10.9.0)
        For i As Integer = 0 To ll.Fields.Count - 1
            If ll.FieldInfo(i).Name = name Then Return i
        Next

        Return -1
    End Function

    Private Function GetNextEnabledTab() As Integer
        For i As Integer = _tabIndex + 1 To uxTabs.TabCount - 1
            If _tabsEnabled(i) Then Return i
        Next
        Return _tabIndex 'No enabled tab found
    End Function

    Private Function GetPreviousEnabledTab() As Integer
        For i As Integer = _tabIndex - 1 To 0 Step -1
            If _tabsEnabled(i) Then Return i
        Next
        Return _tabIndex 'No enabled tab found
    End Function

    Private Sub SetCurrentTab(ByVal idx As Integer)
        Dim i As Integer

        'Move all controls back to tabcontrol
        For i = uxPanel.Controls.Count - 1 To 0 Step -1
            Dim c As Control = uxPanel.Controls(i)
            uxPanel.Controls.Remove(c)
            uxTabs.TabPages(_tabIndex).Controls.Add(c)
        Next

        _tabIndex = idx

        'Show controls in current tab
        For i = uxTabs.TabPages(_tabIndex).Controls.Count - 1 To 0 Step -1
            Dim c As Control = uxTabs.TabPages(_tabIndex).Controls(i)
            uxTabs.TabPages(_tabIndex).Controls.Remove(c)
            uxPanel.Controls.Add(c)
        Next

        'Show create section option in shapes tab only (if EstimateSödra is installed)
        uxCreateSections.Visible = (_tabIndex = _tabIdShapes) AndAlso GisControlView.EstimateSodraInstalled

        'List section data when section tab is in focus
        If _tabIndex = _tabIdSections Then ListSectionData()

        'First page check
        uxPrevious.Enabled = True
        If _tabIndex = 0 Then uxPrevious.Enabled = False

        'Last page check, update button titles
        If _tabIndex >= GetNextEnabledTab() Then
            uxPrevious.Text = GisControlView.LangStr(_strIdPrevious)
            uxNext.Text = GisControlView.LangStr(_strIdOK)
        Else
            uxPrevious.Text = GisControlView.LangStr(_strIdPrevious)
            uxNext.Text = GisControlView.LangStr(_strIdNext)
        End If

        'Change description
        Select Case idx
            Case _tabIdLayer
                lblDescription.Text = GisControlView.LangStr(_strIdDescriptionLayer)
            Case _tabIdShapes
                lblDescription.Text = GisControlView.LangStr(_strIdDescriptionShapes)
            Case _tabIdSections
                lblDescription.Text = GisControlView.LangStr(_strIdDescriptionSections)
            Case _tabIdAttributes
                lblDescription.Text = GisControlView.LangStr(_strIdDescriptionAttributes)
            Case _tabIdProperties
                lblDescription.Text = GisControlView.LangStr(_strIdDescriptionProperties)
        End Select
    End Sub
#End Region

#Region "Event handlers"
    Private Sub ImportDialog_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim regKey As RegistryKey

        Try
            'Set up language dependent strings
            Me.Text = GisControlView.LangStr(_strIdFormCaption)
            uxNext.Text = GisControlView.LangStr(_strIdOK)
            uxCancel.Text = GisControlView.LangStr(_strIdCancel)
            uxCreateSections.Text = GisControlView.LangStr(_strIdCreateSections)
            lblFilterShapes.Text = GisControlView.LangStr(_strIdQuickFilter)
            lblFilterSections.Text = GisControlView.LangStr(_strIdQuickFilter)
            btnFilterShapes.Text = GisControlView.LangStr(_strIdFilter)
            btnCheckAllShapes.Text = GisControlView.LangStr(_strIdCheckAll)
            btnCheckAllSections.Text = GisControlView.LangStr(_strIdCheckAll)
            btnUncheckAllShapes.Text = GisControlView.LangStr(_strIdUncheckAll)
            btnUncheckAllSections.Text = GisControlView.LangStr(_strIdUncheckAll)
            grpLayer.Text = GisControlView.LangStr(_strIdLayer)
            grpAttributes.Text = GisControlView.LangStr(_strIdAttributes)
            grpShapes.Text = GisControlView.LangStr(_strIdShapes)
            grpSections.Text = GisControlView.LangStr(_strIdSections)
            grpProperties.Text = GisControlView.LangStr(_strIdProperties)
            optNewHDLayer.Text = GisControlView.LangStr(_strIdNewHDLayer)
            optNewDBLayer.Text = GisControlView.LangStr(_strIdNewDBLayer)
            optExistingLayer.Text = GisControlView.LangStr(_strIdExistingLayer)
            chkAddProperties.Text = GisControlView.LangStr(_strIdImportProperties)
            lblCounty.Text = GisControlView.LangStr(_strIdCounty)
            lblMunicipal.Text = GisControlView.LangStr(_strIdMunicipal)
            lblParish.Text = GisControlView.LangStr(_strIdParish)
            lblPropNumber.Text = GisControlView.LangStr(_strIdPropNumber)
            lblPropName.Text = GisControlView.LangStr(_strIdPropName)
            lblBlock.Text = GisControlView.LangStr(_strIdBlock)
            lblUnit.Text = GisControlView.LangStr(_strIdUnit)
            lblArea.Text = GisControlView.LangStr(_strIdArea)
            lblMeasuredArea.Text = GisControlView.LangStr(_strIdMeasuredArea)
            lblObjID.Text = GisControlView.LangStr(_strIdObjID)

            'Remember last choice for layer
            regKey = Registry.CurrentUser.OpenSubKey(GisControlView.RegKeyGis, False)
            If regKey IsNot Nothing Then
                Dim val As ImportChoice = regKey.GetValue(GisControlView.RegValImportChoice, ImportChoice.NewDBLayer)
                If val = ImportChoice.ExistingLayer AndAlso Not TypeOf _importLayer Is TGIS_LayerVector Then val = ImportChoice.NewDBLayer 'Fall back to new layer
                Select Case val
                    Case ImportChoice.NewHDLayer
                        optNewHDLayer.Checked = True
                    Case ImportChoice.NewDBLayer
                        optNewDBLayer.Checked = True
                    Case ImportChoice.ExistingLayer
                        optExistingLayer.Checked = True
                End Select
                regKey.Close()
            End If

            'Create layer instance
            GisControlView.CreateLayerInstance(_importLayer, _fileName)
            If _importLayer Is Nothing Then
                Me.Close()
                Exit Sub
            End If
            _importLayer.Open() 'Open file to list field data and projection settings

            'Load checkbox image
            Dim bmp As Bitmap = My.Resources.ResourceManager.GetObject("Check")
            bmp.MakeTransparent()
            uxShapeList.Icons.AddBitmap(bmp.GetHbitmap(), _checkIconId, XTPImageState.xtpImageNormal, True)
            uxSectionList.Icons.AddBitmap(bmp.GetHbitmap(), _checkIconId, XTPImageState.xtpImageNormal, True)

            'Make sure appropriate groups are enabled
            CheckSectionFields()
            SwitchImportMode()
            If uxCreateSections.Enabled Then uxCreateSections.Checked = GisControlView.EstimateSodraInstalled

            'List fields for mapping (vector layers only)
            If TypeOf _importLayer Is TGIS_LayerVector Then
                Dim i As Integer
                Dim lv As TGIS_LayerVector = CType(_importLayer, TGIS_LayerVector)

                'List existing layers (in reverse order to match legend)
                For i = _gisCtrl.Items.Count - 1 To 0 Step -1
                    Dim ll As TGIS_LayerAbstract = _gisCtrl.Items.Items(i)

                    'List only layer vectors, select active layer by default
                    If TypeOf ll Is TGIS_LayerSqlAdo Then
                        Dim idx As Integer = cmbLayers.Items.Add(New NameCaptionPair(ll.Name, ll.Caption))
                        If _activeLayerName = ll.Name Then cmbLayers.SelectedIndex = idx
                    End If
                Next
                If cmbLayers.Items.Count = 0 Then optExistingLayer.Enabled = False 'Disable this option if there are no existing layers

                'List layer fields in filter menu
                FieldMenu.Items.Add(GisControlView.FieldNameGisUid, Nothing, AddressOf FieldMenuEventHandler)
                For i = 0 To lv.Fields.Count - 1
                    FieldMenu.Items.Add(lv.FieldInfo(i).NewName, Nothing, AddressOf FieldMenuEventHandler)
                Next

                'Pick first layer as default
                If cmbLayers.SelectedIndex < 0 AndAlso cmbLayers.Items.Count > 0 Then cmbLayers.SelectedIndex = 0

                'List fields of import layer in property field combos
                If _importLayer IsNot Nothing AndAlso TypeOf _importLayer Is TGIS_LayerVector Then
                    For i = 0 To lv.Fields.Count - 1
                        For Each c As Control In grpProperties.Controls
                            Dim cmb As ComboBox = TryCast(c, ComboBox)
                            If cmb IsNot Nothing Then
                                If i = 0 Then cmb.Items.Add(" ") 'Empty option
                                cmb.Items.Add(lv.FieldInfo(i).NewName)
                            End If
                        Next
                    Next
                End If

                'List field data, check all items
                ListFieldData()
                CheckVisibleItems(True, uxShapeList)
            Else
                'Disable import to existing layer for images
                optExistingLayer.Enabled = False
            End If

            uxShapeList.SetCustomDraw(XtremeReportControl.XTPReportCustomDraw.xtpCustomBeforeDrawRow)

            'Display first tab
            SetCurrentTab(0)
        Catch ex As EGIS_Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Me.Close()
        End Try
    End Sub

    Private Sub cmbLayers_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLayers.SelectedIndexChanged
        'Make sure selected index is different from previous selection
        If cmbLayers.SelectedIndex <> _lastSelectedLayer Then
            SwitchImportMode() 'Make sure property import is enabled/disabled
            ListFields() 'List fields of selected layer
            _lastSelectedLayer = cmbLayers.SelectedIndex
        End If
    End Sub

    Private Sub uxNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxNext.Click
        If _tabIndex < GetNextEnabledTab() Then
            SetCurrentTab(GetNextEnabledTab())
        Else
            If DoOK() Then
                Me.DialogResult = Windows.Forms.DialogResult.OK
                Me.Close()
            End If
        End If
    End Sub

    Private Sub uxBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxPrevious.Click
        If _tabIndex > 0 Then SetCurrentTab(GetPreviousEnabledTab())
    End Sub

    Private Sub FieldMenuEventHandler(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim i As Integer
        Dim Field As String
        Dim lv As TGIS_LayerVector
        Dim ft As TGIS_FieldType

        'Find out which menu item caused this event
        For i = 0 To FieldMenu.Items.Count - 1
            If sender Is FieldMenu.Items(i) Then
                'Determine field type
                Field = FieldMenu.Items(i).Text
                lv = _importLayer

                'Show filter condition dialog for current field
                GisControlView.GetFieldType(lv, Field, ft)
                Dim dlg As New ConditionDialog(Field, ft, _filterConditions(Field))
                If dlg.ShowDialog() = DialogResult.OK Then
                    If dlg.RemoveCondition Then
                        'Remove filter condition
                        _filterConditions.Remove(Field)
                        CType(FieldMenu.Items(i), ToolStripMenuItem).Checked = False
                    Else
                        'Add/update filter condition
                        _filterConditions(Field) = dlg.ConditionSet
                        CType(FieldMenu.Items(i), ToolStripMenuItem).Checked = True
                    End If

                    'Refresh field data
                    ListFieldData()
                End If

                Exit For
            End If
        Next
    End Sub

    Private Sub reportFields_BeforeDrawRow(ByVal eventSender As System.Object, ByVal eventArgs As AxXtremeReportControl._DReportControlEvents_BeforeDrawRowEvent) Handles uxShapeList.BeforeDrawRow
        'Replace background color of marked rows
        Dim record As ReportRecord = eventArgs.row.Record
        If record IsNot Nothing AndAlso record(0).Checked Then
            eventArgs.metrics.BackColor = System.Convert.ToUInt32(RGB(225, 225, 225))
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub txtFilterShapes_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFilterShapes.TextChanged
        uxShapeList.FilterText = txtFilterShapes.Text
        uxShapeList.Populate()
    End Sub

    Private Sub txtFilterSections_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFilterSections.TextChanged
        uxSectionList.FilterText = txtFilterSections.Text
        uxSectionList.Populate()
    End Sub

    Private Sub btnFilterShapes_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnFilterShapes.MouseUp
        FieldMenu.Show(sender, New Point(e.X, e.Y))
    End Sub

    Private Sub btnCheckAllShapes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckAllShapes.Click
        CheckVisibleItems(True, uxShapeList)
    End Sub

    Private Sub btnCheckAllSections_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckAllSections.Click
        CheckVisibleItems(True, uxSectionList)
    End Sub

    Private Sub btnUncheckAllShapes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUncheckAllShapes.Click
        CheckVisibleItems(False, uxShapeList)
    End Sub

    Private Sub btnUncheckAllSections_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUncheckAllSections.Click
        CheckVisibleItems(False, uxSectionList)
    End Sub

    Private Sub optNewDBLayer_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optNewDBLayer.CheckedChanged
        SwitchImportMode()
    End Sub

    Private Sub optNewHDLayer_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optNewHDLayer.CheckedChanged
        SwitchImportMode()
    End Sub

    Private Sub optExistingLayer_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optExistingLayer.CheckedChanged
        SwitchImportMode()
    End Sub

    Private Sub uxCreateSections_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxCreateSections.CheckedChanged
        SwitchImportMode()
    End Sub

    Private Sub chkAddProperties_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAddProperties.CheckedChanged
        'Enabled/disable all controls in group except this checkbox
        For Each c As Control In grpProperties.Controls
            If c.Handle <> chkAddProperties.Handle Then c.Enabled = chkAddProperties.Checked
        Next
    End Sub
#End Region
End Class
