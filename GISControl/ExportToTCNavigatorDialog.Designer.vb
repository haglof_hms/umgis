﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ExportToTCNavigatorDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.uxExportPointLayer = New System.Windows.Forms.ComboBox
        Me.uxExportPointLayerLabel = New System.Windows.Forms.Label
        Me.uxExportLayers = New System.Windows.Forms.GroupBox
        Me.uxExportPolygonLayer = New System.Windows.Forms.ComboBox
        Me.uxExportPolygonLayerLabel = New System.Windows.Forms.Label
        Me.LineGeneral = New System.Windows.Forms.Label
        Me.uxCancel = New System.Windows.Forms.Button
        Me.uxOK = New System.Windows.Forms.Button
        Me.uxExportPath = New System.Windows.Forms.TextBox
        Me.uxBrowse = New System.Windows.Forms.Button
        Me.uxExportPathLabel = New System.Windows.Forms.Label
        Me.uxExportName = New System.Windows.Forms.TextBox
        Me.uxExportNameLabel = New System.Windows.Forms.Label
        Me.uxExportLayers.SuspendLayout()
        Me.SuspendLayout()
        '
        'uxExportPointLayer
        '
        Me.uxExportPointLayer.DropDownHeight = 300
        Me.uxExportPointLayer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.uxExportPointLayer.FormattingEnabled = True
        Me.uxExportPointLayer.IntegralHeight = False
        Me.uxExportPointLayer.Location = New System.Drawing.Point(9, 92)
        Me.uxExportPointLayer.Name = "uxExportPointLayer"
        Me.uxExportPointLayer.Size = New System.Drawing.Size(268, 21)
        Me.uxExportPointLayer.TabIndex = 10
        '
        'uxExportPointLayerLabel
        '
        Me.uxExportPointLayerLabel.AutoSize = True
        Me.uxExportPointLayerLabel.Location = New System.Drawing.Point(9, 76)
        Me.uxExportPointLayerLabel.Name = "uxExportPointLayerLabel"
        Me.uxExportPointLayerLabel.Size = New System.Drawing.Size(124, 13)
        Me.uxExportPointLayerLabel.TabIndex = 12
        Me.uxExportPointLayerLabel.Text = "uxExportPointLayerLabel"
        '
        'uxExportLayers
        '
        Me.uxExportLayers.Controls.Add(Me.uxExportPointLayer)
        Me.uxExportLayers.Controls.Add(Me.uxExportPointLayerLabel)
        Me.uxExportLayers.Controls.Add(Me.uxExportPolygonLayer)
        Me.uxExportLayers.Controls.Add(Me.uxExportPolygonLayerLabel)
        Me.uxExportLayers.Location = New System.Drawing.Point(12, 57)
        Me.uxExportLayers.Name = "uxExportLayers"
        Me.uxExportLayers.Size = New System.Drawing.Size(322, 134)
        Me.uxExportLayers.TabIndex = 14
        Me.uxExportLayers.TabStop = False
        Me.uxExportLayers.Text = "uxExportLayers"
        '
        'uxExportPolygonLayer
        '
        Me.uxExportPolygonLayer.DropDownHeight = 300
        Me.uxExportPolygonLayer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.uxExportPolygonLayer.FormattingEnabled = True
        Me.uxExportPolygonLayer.IntegralHeight = False
        Me.uxExportPolygonLayer.Location = New System.Drawing.Point(9, 39)
        Me.uxExportPolygonLayer.Name = "uxExportPolygonLayer"
        Me.uxExportPolygonLayer.Size = New System.Drawing.Size(268, 21)
        Me.uxExportPolygonLayer.TabIndex = 1
        '
        'uxExportPolygonLayerLabel
        '
        Me.uxExportPolygonLayerLabel.AutoSize = True
        Me.uxExportPolygonLayerLabel.Location = New System.Drawing.Point(9, 23)
        Me.uxExportPolygonLayerLabel.Name = "uxExportPolygonLayerLabel"
        Me.uxExportPolygonLayerLabel.Size = New System.Drawing.Size(138, 13)
        Me.uxExportPolygonLayerLabel.TabIndex = 7
        Me.uxExportPolygonLayerLabel.Text = "uxExportPolygonLayerLabel"
        '
        'LineGeneral
        '
        Me.LineGeneral.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LineGeneral.BackColor = System.Drawing.Color.DarkGray
        Me.LineGeneral.Location = New System.Drawing.Point(12, 261)
        Me.LineGeneral.Name = "LineGeneral"
        Me.LineGeneral.Size = New System.Drawing.Size(321, 1)
        Me.LineGeneral.TabIndex = 37
        '
        'uxCancel
        '
        Me.uxCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.uxCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.uxCancel.Location = New System.Drawing.Point(177, 275)
        Me.uxCancel.Name = "uxCancel"
        Me.uxCancel.Size = New System.Drawing.Size(67, 23)
        Me.uxCancel.TabIndex = 36
        Me.uxCancel.Text = "uxCancel"
        '
        'uxOK
        '
        Me.uxOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.uxOK.Location = New System.Drawing.Point(104, 275)
        Me.uxOK.Name = "uxOK"
        Me.uxOK.Size = New System.Drawing.Size(67, 23)
        Me.uxOK.TabIndex = 35
        Me.uxOK.Text = "uxOK"
        '
        'uxExportPath
        '
        Me.uxExportPath.Location = New System.Drawing.Point(21, 219)
        Me.uxExportPath.Name = "uxExportPath"
        Me.uxExportPath.ReadOnly = True
        Me.uxExportPath.Size = New System.Drawing.Size(289, 20)
        Me.uxExportPath.TabIndex = 38
        '
        'uxBrowse
        '
        Me.uxBrowse.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.uxBrowse.Location = New System.Drawing.Point(313, 219)
        Me.uxBrowse.Name = "uxBrowse"
        Me.uxBrowse.Size = New System.Drawing.Size(24, 20)
        Me.uxBrowse.TabIndex = 39
        Me.uxBrowse.Text = "..."
        Me.uxBrowse.UseVisualStyleBackColor = True
        '
        'uxExportPathLabel
        '
        Me.uxExportPathLabel.AutoSize = True
        Me.uxExportPathLabel.Location = New System.Drawing.Point(21, 203)
        Me.uxExportPathLabel.Name = "uxExportPathLabel"
        Me.uxExportPathLabel.Size = New System.Drawing.Size(96, 13)
        Me.uxExportPathLabel.TabIndex = 40
        Me.uxExportPathLabel.Text = "uxExportPathLabel"
        '
        'uxExportName
        '
        Me.uxExportName.Location = New System.Drawing.Point(21, 21)
        Me.uxExportName.Name = "uxExportName"
        Me.uxExportName.Size = New System.Drawing.Size(289, 20)
        Me.uxExportName.TabIndex = 43
        '
        'uxExportNameLabel
        '
        Me.uxExportNameLabel.AutoSize = True
        Me.uxExportNameLabel.Location = New System.Drawing.Point(21, 5)
        Me.uxExportNameLabel.Name = "uxExportNameLabel"
        Me.uxExportNameLabel.Size = New System.Drawing.Size(102, 13)
        Me.uxExportNameLabel.TabIndex = 44
        Me.uxExportNameLabel.Text = "uxExportNameLabel"
        '
        'ExportToTCNavigatorDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(348, 308)
        Me.Controls.Add(Me.uxExportName)
        Me.Controls.Add(Me.uxExportNameLabel)
        Me.Controls.Add(Me.uxExportPath)
        Me.Controls.Add(Me.uxBrowse)
        Me.Controls.Add(Me.uxExportPathLabel)
        Me.Controls.Add(Me.LineGeneral)
        Me.Controls.Add(Me.uxCancel)
        Me.Controls.Add(Me.uxOK)
        Me.Controls.Add(Me.uxExportLayers)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ExportToTCNavigatorDialog"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "ExportToTCNavigatorDialog"
        Me.uxExportLayers.ResumeLayout(False)
        Me.uxExportLayers.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents uxExportPointLayer As System.Windows.Forms.ComboBox
    Friend WithEvents uxExportPointLayerLabel As System.Windows.Forms.Label
    Friend WithEvents uxExportLayers As System.Windows.Forms.GroupBox
    Friend WithEvents uxExportPolygonLayer As System.Windows.Forms.ComboBox
    Friend WithEvents uxExportPolygonLayerLabel As System.Windows.Forms.Label
    Friend WithEvents LineGeneral As System.Windows.Forms.Label
    Friend WithEvents uxCancel As System.Windows.Forms.Button
    Friend WithEvents uxOK As System.Windows.Forms.Button
    Friend WithEvents uxExportPath As System.Windows.Forms.TextBox
    Friend WithEvents uxBrowse As System.Windows.Forms.Button
    Friend WithEvents uxExportPathLabel As System.Windows.Forms.Label
    Friend WithEvents uxExportName As System.Windows.Forms.TextBox
    Friend WithEvents uxExportNameLabel As System.Windows.Forms.Label
End Class
