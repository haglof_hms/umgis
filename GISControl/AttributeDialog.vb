﻿Imports TatukGIS.NDK
Imports XtremeReportControl

Public Class AttributeDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 1600
    Private Const _strIdOK As Integer = 1601
    Private Const _strIdCancel As Integer = 1602
    Private Const _strIdName As Integer = 1603
    Private Const _strIdValue As Integer = 1604
#End Region

#Region "Declarations"
    Private _shp As TGIS_Shape

    Public Sub New(ByVal shp As TGIS_Shape)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _shp = shp
    End Sub
#End Region

#Region "Helper functions"
    Private Sub ResizeForm()
        'Size attribute table to fill up the form
        uxAttributeReport.Top = 0
        uxAttributeReport.Left = 0
        uxAttributeReport.Width = Me.ClientSize.Width
        uxAttributeReport.Height = Me.ClientSize.Height - uxOK.Height - 14

        uxOK.Top = Me.ClientSize.Height - uxOK.Height - 4
        uxCancel.Top = Me.ClientSize.Height - uxOK.Height - 4
        uxOK.Left = (Me.ClientSize.Width / 2) - uxOK.Width - 4
        uxCancel.Left = (Me.ClientSize.Width / 2) + 4
    End Sub

    Private Sub ValidateAttributes()
        Dim invalidValue As Boolean = False

        'Validate all attributes
        For Each rec As ReportRecord In uxAttributeReport.Records
            Dim item As ReportRecordItem = rec.Item(1)

            'Test if valid can be converted
            Try
                Select Case CType(rec.Tag, TGIS_FieldType)
                    Case TGIS_FieldType.gisFieldTypeBoolean
                        Dim val As Boolean = CType(item.Value, Boolean)

                    Case TGIS_FieldType.gisFieldTypeDate
                        Dim val As Date = CType(item.Value, Date)

                    Case TGIS_FieldType.gisFieldTypeFloat
                        Dim val As Double = CType(item.Value, Double)

                    Case TGIS_FieldType.gisFieldTypeNumber
                        Dim val As Long = CType(item.Value, Long)
                End Select
            Catch ex As InvalidCastException
                invalidValue = True
                Exit For
            End Try
        Next

        'Disable OK if an invalid value was encountered
        uxOK.Enabled = Not invalidValue
    End Sub
#End Region

#Region "Event handlers"
    Private Sub AttributeDialog_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim i As Integer
        Dim fi As TGIS_FieldInfo
        Dim rec As ReportRecord
        Dim item As ReportRecordItem
        Dim caption As String

        'Set up language
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        uxOK.Text = GisControlView.LangStr(_strIdOK)
        uxCancel.Text = GisControlView.LangStr(_strIdCancel)
        uxAttributeReport.Columns.Add(0, GisControlView.LangStr(_strIdName), 50, True)
        uxAttributeReport.Columns.Add(1, GisControlView.LangStr(_strIdValue), 50, True)

        'List shape attributes
        rec = uxAttributeReport.Records.Add()
        rec.AddItem(GisControlView.FieldNameGisUid).Editable = False
        rec.AddItem(_shp.Uid).Editable = False
        For i = 0 To _shp.Layer.Fields.Count - 1
            fi = _shp.Layer.Fields(i)
            If Not fi.IsUID AndAlso Not GisControlView.IsRelationField(fi.NewName) Then
                rec = uxAttributeReport.Records.Add()
                rec.Tag = fi.FieldType
                rec.AddItem(fi.NewName).Editable = False
                item = rec.AddItem(_shp.GetField(fi.NewName).ToString())
                If Not TypeOf _shp.Layer Is TGIS_LayerSqlAdo And Not TypeOf _shp.Layer Is TGIS_LayerSHP Then item.Editable = False 'Disk or stream layer - disable editing
            End If
        Next

        'List any joined data
        If _shp.Layer.JoinNET IsNot Nothing Then
            Dim rt As RelationType = RelationType.Undefined
            Dim jv As String = vbNullString
            Dim dt As DataTable = _shp.Layer.JoinNET
            Dim j As Integer

            'Find matching record
            GisControlView.GetLayerRelationType(_shp.Layer.Name, rt, jv)
            For i = 0 To dt.Rows.Count - 1
                If dt.Rows(i).Item(GisControlView.ForeignKeyField) = _shp.GetField(GisControlView.GetJoinPrimary(jv)) Then
                    'List all fields
                    For j = 0 To dt.Columns.Count - 1
                        If Not GisControlView.IsRelationField(dt.Columns(j).Caption) Then
                            rec = uxAttributeReport.Records.Add()
                            If IsNumeric(dt.Columns(j).Caption) Then caption = GisControlView.LangStr(CInt(dt.Columns(j).Caption)) Else caption = dt.Columns(j).Caption
                            rec.AddItem(caption).Editable = False
                            rec.AddItem(dt.Rows(i).Item(j)).Editable = False
                        End If
                    Next
                    Exit For
                End If
            Next
        End If

        uxAttributeReport.Populate()

        ResizeForm()

        Me.MinimumSize = Me.Size 'This is not set in form properties because it may cause the dialog to be hidden behind the main window
    End Sub

    Private Sub AttributeDialog_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        ResizeForm()
    End Sub

    Private Sub uxAttributeReport_ValueChanged(ByVal sender As Object, ByVal e As AxXtremeReportControl._DReportControlEvents_ValueChangedEvent) Handles uxAttributeReport.ValueChanged
        ValidateAttributes()
    End Sub

    Private Sub uxOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxOK.Click
        Try
            Dim editable As Boolean

            For Each rec As ReportRecord In uxAttributeReport.Records
                editable = rec.Item(1).Editable()
                'Added check for finding out if the attribute is editable or not, if so no need to save changes 20100820 JÖ
                If rec.Item(0).Value = GisControlView.FieldNameGisUid OrElse editable = False Then Continue For 'Skip GIS_UID field
                _shp.SetField(rec.Item(0).Value, rec.Item(1).Value)
            Next
            _shp.Layer.SaveData()
        Catch ex As EGIS_Exception
            _shp.Layer.RevertAll()
            Throw
        End Try

        Me.DialogResult = Windows.Forms.DialogResult.OK
		Me.Close()
	End Sub

    Private Sub uxCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles uxCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub
#End Region
End Class
