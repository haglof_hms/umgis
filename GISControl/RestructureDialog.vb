﻿Imports System.Data.OleDb
Imports TatukGIS.NDK

Public Class RestructureDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 1300
    Private Const _strIdName As Integer = 1301
    Private Const _strIdType As Integer = 1302
    Private Const _strIdWidth As Integer = 1303
    Private Const _strIdDecimal As Integer = 1304
    Private Const _strIdOK As Integer = 1305
    Private Const _strIdCancel As Integer = 1306
    Private Const _strIdBoolean As Integer = 1307
    Private Const _strIdDate As Integer = 1308
    Private Const _strIdFloat As Integer = 1309
    Private Const _strIdNumber As Integer = 1310
    Private Const _strIdString As Integer = 1311
#End Region

#Region "Declarations"
    Private _ll As TGIS_LayerVector
    Private _isUpdatingName As Boolean

    Private Class FieldInfoItem
        Private _fieldInfo As TGIS_FieldInfo
        Private _locked As Boolean

        Public Sub New(ByVal fieldInfo As TGIS_FieldInfo, ByVal locked As Boolean)
            _fieldInfo = fieldInfo
            _locked = locked
        End Sub

        Public ReadOnly Property FieldInfo() As TGIS_FieldInfo
            Get
                Return _fieldInfo
            End Get
        End Property

        Public ReadOnly Property Locked() As Boolean
            Get
                Return _locked
            End Get
        End Property

        Public Overrides Function ToString() As String
            Return _fieldInfo.NewName
        End Function
    End Class

    Public Sub New(ByVal ll As TGIS_LayerVector)

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me._ll = ll
    End Sub
#End Region

#Region "Helper functions"
    Private Function FindTypeIndex(ByVal type As TGIS_FieldType) As Integer
        Dim i As Integer

        'Find index of specified type in combobox
        For i = 0 To cmbType.Items.Count - 1
            If CType(cmbType.Items.Item(i), ValueDescriptionPair).Value = type Then
                Return i
            End If
        Next

        Return -1
    End Function

    Private Shared Function IsReservedFieldName(ByVal fieldName As String) As Boolean
        fieldName = fieldName.ToUpperInvariant()

        If _
        fieldName = GisControlView.FieldNameRelation OrElse _
        fieldName = GisControlView.FieldNameSubrelation Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "Event handlers"

    Private Sub RestructureDialog_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            'Revert any changes in (if closed by OK changes are already saved)
            _ll.RevertAll()
        Catch ex As EGIS_Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub
    Private Sub RestructureDialog_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim i As Integer
        Dim fi As TGIS_FieldInfo
        Dim item As FieldInfoItem
        Dim val As ValueDescriptionPair
        Dim OnPasteName As New TextBoxOnPaste(Me.txtName)

        'Set up language dependent strings
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        lblName.Text = GisControlView.LangStr(_strIdName)
        lblType.Text = GisControlView.LangStr(_strIdType)
        lblWidth.Text = GisControlView.LangStr(_strIdWidth)
        lblDecimal.Text = GisControlView.LangStr(_strIdDecimal)
        btnOK.Text = GisControlView.LangStr(_strIdOK)
        btnCancel.Text = GisControlView.LangStr(_strIdCancel)

        'Set up default constraints
        txtName.MaxLength = 255

        'List existing fields (ignore any deleted fields)
        For i = 0 To _ll.Fields.Count - 1
            fi = CType(_ll.Fields.Items(i), TGIS_FieldInfo)
            If Not fi.Deleted AndAlso Not fi.IsUID AndAlso Not IsReservedFieldName(fi.Name) Then
                item = New FieldInfoItem(fi, True)
                lstFields.Items.Add(item)
            End If
        Next

        'List field types
        val = New ValueDescriptionPair(TGIS_FieldType.gisFieldTypeBoolean, GisControlView.LangStr(_strIdBoolean))
        cmbType.Items.Add(val)
        val = New ValueDescriptionPair(TGIS_FieldType.gisFieldTypeDate, GisControlView.LangStr(_strIdDate))
        cmbType.Items.Add(val)
        val = New ValueDescriptionPair(TGIS_FieldType.gisFieldTypeFloat, GisControlView.LangStr(_strIdFloat))
        cmbType.Items.Add(val)
        val = New ValueDescriptionPair(TGIS_FieldType.gisFieldTypeNumber, GisControlView.LangStr(_strIdNumber))
        cmbType.Items.Add(val)
        val = New ValueDescriptionPair(TGIS_FieldType.gisFieldTypeString, GisControlView.LangStr(_strIdString))
        cmbType.Items.Add(val)
    End Sub

    Private Sub cmbType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbType.SelectedIndexChanged
        'Update field type property
        If lstFields.SelectedIndex >= 0 Then
            CType(lstFields.SelectedItem, FieldInfoItem).FieldInfo.FieldType = CType(cmbType.SelectedItem, ValueDescriptionPair).Value
        End If

        Select Case CType(cmbType.SelectedItem, ValueDescriptionPair).Value
            Case TGIS_FieldType.gisFieldTypeBoolean, TGIS_FieldType.gisFieldTypeDate, TGIS_FieldType.gisFieldTypeFloat
                lblWidth.Visible = False
                lblDecimal.Visible = False
                txtWidth.Visible = False
                txtDecimal.Visible = False

            Case TGIS_FieldType.gisFieldTypeNumber
                'Number - width max 38; dec max 8
                txtWidth.Maximum = 38
                txtDecimal.Maximum = 8
                lblWidth.Visible = True
                lblDecimal.Visible = True
                txtWidth.Visible = True
                txtDecimal.Visible = True

            Case TGIS_FieldType.gisFieldTypeString
                'String - width max 256
                txtWidth.Maximum = 256
                txtWidth.Visible = True
                lblDecimal.Visible = False
                txtDecimal.Visible = False
        End Select
    End Sub

    Private Sub lstFields_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstFields.SelectedIndexChanged
        Dim fi As TGIS_FieldInfo
        Dim enabled As Boolean

        'Ignore this event if caused by a name change
        If _isUpdatingName Then Exit Sub

        If lstFields.SelectedIndex >= 0 Then
            'Show field info for selected field
            fi = CType(lstFields.SelectedItem, FieldInfoItem).FieldInfo
            txtName.Text = fi.NewName
            cmbType.SelectedIndex = FindTypeIndex(fi.FieldType)
            txtWidth.Value = fi.Width
            txtDecimal.Value = fi.Decimal

            'Check if this item should be locked or not (used to lock existing fields from editing)
            enabled = Not CType(lstFields.SelectedItem, FieldInfoItem).Locked
            txtName.Enabled = True
            cmbType.Enabled = enabled
            txtWidth.Enabled = True
            txtDecimal.Enabled = True
        Else
            'Clear and lock all fields
            txtName.Text = vbNullString
            cmbType.SelectedIndex = FindTypeIndex(TGIS_FieldType.gisFieldTypeString)
            txtName.Enabled = False
            cmbType.Enabled = False
            txtWidth.Enabled = False
            txtDecimal.Enabled = False
        End If
    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim fi As TGIS_FieldInfo
        Dim item As FieldInfoItem
        Dim fieldName As String

        Try
            'Add a new field with default settings
            fieldName = _ll.GetUniqueFieldName("name")
            Try
                'AddField will throw an exception if the last added field was renamed and the layer hasn't been saved in between
                _ll.AddField(fieldName, TGIS_FieldType.gisFieldTypeString, 50, 0)
            Catch ex As ArgumentException 'Field has been added and renamed, nothing to do
            End Try

            'Add to field list
            fi = _ll.Fields.Items(_ll.FindField(fieldName))
            item = New FieldInfoItem(fi, False)
            lstFields.SelectedIndex = lstFields.Items.Add(item)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        'Remove selected field
        If lstFields.SelectedIndex >= 0 Then
            _ll.DeleteField(lstFields.SelectedItem.ToString())
            lstFields.Items.RemoveAt(lstFields.SelectedIndex)
        End If
    End Sub

    Private Sub txtName_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtName.KeyPress
        'Allow only alphanumeric characters
        If (e.KeyChar >= "a"c AndAlso e.KeyChar <= "z"c) OrElse _
        (e.KeyChar >= "A"c AndAlso e.KeyChar <= "Z"c) OrElse _
        (e.KeyChar >= "0"c AndAlso e.KeyChar <= "9"c) OrElse _
        e.KeyChar = ControlChars.Back Then
            'Do nothing
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        Dim item As FieldInfoItem
        Dim newName As String

        _isUpdatingName = True

        Try
            'Update field property name
            If lstFields.SelectedIndex >= 0 Then
                'Prevent user from using reserved field names
                newName = txtName.Text
                If IsReservedFieldName(newName) Then newName += "_"

                'We need to reassign this item for the text to be refreshed
                item = lstFields.SelectedItem
                _ll.RenameField(item.FieldInfo.NewName, newName, item.FieldInfo.Width, item.FieldInfo.Decimal)

                lstFields.Items.Item(lstFields.SelectedIndex) = item
            End If
        Catch ex As EGIS_Exception
            'If the name could not be changed, try a unique name
            item = lstFields.SelectedItem
            _ll.RenameField(item.FieldInfo.NewName, _ll.GetUniqueFieldName(txtName.Text), item.FieldInfo.Width, item.FieldInfo.Decimal)

            lstFields.Items.Item(lstFields.SelectedIndex) = item
        End Try

        _isUpdatingName = False
    End Sub

    Private Sub txtWidth_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWidth.ValueChanged
        Dim item As FieldInfoItem

        Try
            'Update field width property
            If lstFields.SelectedIndex >= 0 Then
                item = CType(lstFields.SelectedItem, FieldInfoItem)
                _ll.RenameField(item.FieldInfo.NewName, item.FieldInfo.NewName, txtWidth.Value, item.FieldInfo.Decimal)
            End If
        Catch ex As EGIS_Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub txtDecimal_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDecimal.ValueChanged
        Dim item As FieldInfoItem

        'Update field decimal property
        If lstFields.SelectedIndex >= 0 Then
            item = CType(lstFields.SelectedItem, FieldInfoItem)
            _ll.RenameField(item.FieldInfo.NewName, item.FieldInfo.NewName, item.FieldInfo.Width, txtDecimal.Value)
        End If
    End Sub

    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Dim i As Integer
        Dim fi As TGIS_FieldInfo
        Dim cmd As New OleDbCommand("", GisControlView.Conn)

        'If this is a sql layer we need to change any field names manually (this is disabled in the DK)
        If TypeOf _ll Is TGIS_LayerSqlAdo Then
            For i = 0 To lstFields.Items.Count - 1
                'We need to do this only for existing (locked) fields (new fields doesn't exist in db yet and NewName will automatically be applied when created)
                If CType(lstFields.Items.Item(i), FieldInfoItem).Locked Then
                    'We need to execute this statement only if name has been changed
                    fi = CType(lstFields.Items.Item(i), FieldInfoItem).FieldInfo
                    If fi.Name <> fi.NewName Then
                        cmd.CommandText = "EXEC sp_rename '" & GisControlView.GisTablePrefix & _ll.Name & "_FEA." & fi.Name & "', '" & fi.NewName & "'"
                        cmd.ExecuteNonQuery()
                    End If
                End If
            Next
        End If

        'Save any changes to layer (we need to do this after changing field names since SaveAll will clear this info)
        _ll.SaveAll()

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub
#End Region
End Class
