﻿Imports TatukGIS.NDK

Public Class ConditionSet
    'Equal, less than, greater than, etc.
    Private _condition1 As Integer
    Private _condition2 As Integer

    'AND, OR
    Private _commonOperator As String

    'Data values
    Private _value1 As String
    Private _value2 As String

    Public Property Condition1() As Integer
        Get
            Return _condition1
        End Get
        Set(ByVal value As Integer)
            _condition1 = value
        End Set
    End Property

    Public Property Condition2() As Integer
        Get
            Return _condition2
        End Get
        Set(ByVal value As Integer)
            _condition2 = value
        End Set
    End Property

    Public Property CommonOperator() As String
        Get
            Return _commonOperator
        End Get
        Set(ByVal value As String)
            _commonOperator = value
        End Set
    End Property

    Public Property Value1() As String
        Get
            Return _value1
        End Get
        Set(ByVal value As String)
            _value1 = value
        End Set
    End Property

    Public Property Value2() As String
        Get
            Return _value2
        End Get
        Set(ByVal value As String)
            _value2 = value
        End Set
    End Property
End Class

Public Class ArrayDescriptionPair
    Private _values As Array
    Private _description As String

    Public Sub New(ByVal values As Array, ByVal description As String)
        _values = values
        _description = description
    End Sub

    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    Public ReadOnly Property Values() As Array
        Get
            Return _values
        End Get
    End Property

    Public Overrides Function ToString() As String
        Return _description
    End Function
End Class

Public Class LayerShapeList
    Private _ids As List(Of Integer)
    Private _ll As TGIS_LayerVector
    Private _dt As DataTable

    Public Sub New(ByVal layer As TGIS_LayerVector)
        _ids = New List(Of Integer)
        _ll = layer
        _dt = Nothing
    End Sub

    Public ReadOnly Property Layer() As TGIS_LayerVector
        Get
            Return _ll
        End Get
    End Property

    Public Sub AddShapeId(ByVal uid As Integer)
        _ids.Add(uid)
    End Sub

    Public ReadOnly Property ShapeId(ByVal index As Integer) As Integer
        Get
            Return _ids(index)
        End Get
    End Property

    Public ReadOnly Property ShapeCount() As Integer
        Get
            Return _ids.Count
        End Get
    End Property

    Public Property DT() As DataTable
        Get
            Return _dt
        End Get
        Set(ByVal value As DataTable)
            _dt = value
        End Set
    End Property
End Class

Public Class NameCaptionPair
    Private _name As String
    Private _caption As String

    Public Sub New(ByVal name As String, ByVal caption As String)
        _name = name
        _caption = caption
    End Sub

    Public ReadOnly Property Name() As String
        Get
            Return _name
        End Get
    End Property

    Public ReadOnly Property Caption() As String
        Get
            Return _caption
        End Get
    End Property

    Public Overrides Function ToString() As String
        Return Caption
    End Function
End Class

Public Class StringBoolPair
    Private _name As String
    Private _value As Boolean

    Public Sub New(ByVal name As String, ByVal value As Boolean)
        _name = name
        _value = value
    End Sub

    Public ReadOnly Property Name() As String
        Get
            Return _name
        End Get
    End Property

    Public ReadOnly Property Value() As Boolean
        Get
            Return _value
        End Get
    End Property
End Class

Public Class ValueDescriptionPair
    Private _value As Object
    Private _description As String

    Public Sub New(ByVal value As Object, ByVal description As String)
        _value = value
        _description = description
    End Sub

    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

    Public Property Value() As Object
        Get
            Return _value
        End Get
        Set(ByVal value As Object)
            _value = value
        End Set
    End Property

    Public Overrides Function ToString() As String
        Return Description
    End Function
End Class

Public Class JoinView
    Private _type As RelationType
    Private _joinprimary As String
    Private _name As String
    Private _title As String
    Private _joindt As DataTable

    Public Sub New(ByVal type As RelationType, ByVal joinprimary As String, ByVal name As String, ByVal title As String)
        _type = type
        _joinprimary = joinprimary
        _name = name
        _title = title
    End Sub

    Public Sub New(ByVal type As RelationType, ByVal joinprimary As String, ByVal name As String, ByVal title As String, ByVal joindt As DataTable)
        _type = type
        _joinprimary = joinprimary
        _name = name
        _title = title
        _joindt = joindt
    End Sub

    Public ReadOnly Property TypeOfRelation() As RelationType
        Get
            Return _type
        End Get
    End Property

    Public ReadOnly Property JoinPrimary() As String
        Get
            Return _joinprimary
        End Get
    End Property

    Public Property JoinDT() As DataTable
        Get
            Return _joindt
        End Get
        Set(ByVal value As DataTable)
            _joindt = value
        End Set
    End Property

    Public ReadOnly Property Name() As String
        Get
            Return _name
        End Get
    End Property

    Public ReadOnly Property Title() As String
        Get
            Return _title
        End Get
    End Property

    Public Overrides Function ToString() As String
        Return _title
    End Function
End Class

Public Class MinMaxPair
    Private _min As Double, _max As Double

    Public Sub New(ByVal min As Double, ByVal max As Double)
        _min = min
        _max = max
    End Sub

    Public Property Min() As Double
        Get
            Return _min
        End Get
        Set(ByVal value As Double)
            _min = value
        End Set
    End Property

    Public Property Max() As Double
        Get
            Return _max
        End Get
        Set(ByVal value As Double)
            _max = value
        End Set
    End Property
End Class

Public Class IntIntPair
    Private _val1 As Integer, _val2 As Integer

    Public Sub New(ByVal val1 As Integer, ByVal val2 As Integer)
        _val1 = val1
        _val2 = val2
    End Sub

    Public ReadOnly Property Val1() As Integer
        Get
            Return _val1
        End Get
    End Property

    Public ReadOnly Property Val2() As Integer
        Get
            Return _val2
        End Get
    End Property
End Class

Friend Class NativeMethods
    Private Sub New()
        'Private constructor is here only to prevent compiler from generating a default constructor (this class is intended only for static methods)
    End Sub

    Friend Declare Auto Function ExtractIcon Lib "shell32" (ByVal hInstance As IntPtr, ByVal lpszExeFileName As String, ByVal nIconIndex As Integer) As IntPtr
    Friend Declare Auto Function ExtractIconEx Lib "shell32" (ByVal lpszFile As String, ByVal nIconIndex As Integer, ByVal phiconLarge() As IntPtr, ByVal phiconSmall() As IntPtr, ByVal nIcons As UInteger) As Integer
    Friend Declare Auto Function DestroyIcon Lib "user32" (ByVal hIcon As IntPtr) As Boolean
    Friend Declare Auto Function GetKeyState Lib "user32" (ByVal virtualKeyCode As Integer) As Short
End Class

Public Enum GisMode
    Drag
    SelectShp
    Zoom
    ViewInfo
    Add
    EditStep1
    EditStep2
    Remove
    Measure
    Split
    CreatePlots
    GenerateRouteStep1WithoutCenterLine
    GenerateRouteStep1WithCenterLine
    GenerateRouteStep2
    ExportRoute
    CreateElvObject
    CreateStand
    CreateSection
    ExportShp
    ApplyRelation
    SumAttributes
    ImportProperties
    ExportToTCNavigator
End Enum

Public Enum EditMode
    NearestPoint
    AfterActivePoint
End Enum

Public Enum RelationType
    Undefined = 0
    StandRelation
    PropertyRelation
    ObjectRelation
    TCTractRelation
    TCPlotRelation
    SampleTreeRelation
    SectionRelation
End Enum

Public Enum RouteDirection
    Unspecified
    Row
    Column
End Enum

Public Enum ShapeType
    Polygon
    Line
    Point
    MultiPoint
End Enum

Public Enum SelectState
    Selected
    Deselected
    Inverted
End Enum

Public Enum ServiceProtocol
    Auto
    WMS
    WFS
    WMTS
    ECWP
End Enum
