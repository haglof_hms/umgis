﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UnusedLayersDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(UnusedLayersDialog))
        Me.uxCancel = New System.Windows.Forms.Button
        Me.uxOK = New System.Windows.Forms.Button
        Me.uxLayers = New AxXtremeReportControl.AxReportControl
        Me.uxCheckUncheckAll = New System.Windows.Forms.Button
        CType(Me.uxLayers, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'uxCancel
        '
        Me.uxCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.uxCancel.Location = New System.Drawing.Point(87, 227)
        Me.uxCancel.Name = "uxCancel"
        Me.uxCancel.Size = New System.Drawing.Size(75, 23)
        Me.uxCancel.TabIndex = 5
        Me.uxCancel.Text = "uxCancel"
        Me.uxCancel.UseVisualStyleBackColor = True
        '
        'uxOK
        '
        Me.uxOK.Enabled = False
        Me.uxOK.Location = New System.Drawing.Point(6, 227)
        Me.uxOK.Name = "uxOK"
        Me.uxOK.Size = New System.Drawing.Size(75, 23)
        Me.uxOK.TabIndex = 4
        Me.uxOK.Text = "uxOK"
        Me.uxOK.UseVisualStyleBackColor = True
        '
        'uxLayers
        '
        Me.uxLayers.Location = New System.Drawing.Point(6, 9)
        Me.uxLayers.Name = "uxLayers"
        Me.uxLayers.OcxState = CType(resources.GetObject("uxLayers.OcxState"), System.Windows.Forms.AxHost.State)
        Me.uxLayers.Size = New System.Drawing.Size(405, 212)
        Me.uxLayers.TabIndex = 6
        '
        'uxCheckUncheckAll
        '
        Me.uxCheckUncheckAll.Location = New System.Drawing.Point(278, 227)
        Me.uxCheckUncheckAll.Name = "uxCheckUncheckAll"
        Me.uxCheckUncheckAll.Size = New System.Drawing.Size(133, 23)
        Me.uxCheckUncheckAll.TabIndex = 6
        Me.uxCheckUncheckAll.Text = "uxCheckUncheckAll"
        Me.uxCheckUncheckAll.UseVisualStyleBackColor = True
        '
        'UnusedLayersDialog
        '
        Me.AcceptButton = Me.uxOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.uxCancel
        Me.ClientSize = New System.Drawing.Size(419, 258)
        Me.Controls.Add(Me.uxCheckUncheckAll)
        Me.Controls.Add(Me.uxLayers)
        Me.Controls.Add(Me.uxCancel)
        Me.Controls.Add(Me.uxOK)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "UnusedLayersDialog"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "UnusedLayersDialog"
        CType(Me.uxLayers, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents uxCancel As System.Windows.Forms.Button
    Friend WithEvents uxOK As System.Windows.Forms.Button
    Friend WithEvents uxLayers As AxXtremeReportControl.AxReportControl
    Friend WithEvents uxCheckUncheckAll As System.Windows.Forms.Button
End Class
