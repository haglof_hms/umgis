﻿Public Class GeoreferenceDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 1100
    Private Const _strIdScale As Integer = 1101
    Private Const _strIdCoordinates As Integer = 1102
    Private Const _strIdOK As Integer = 1103
    Private Const _strIdCancel As Integer = 1104
    Private Const _strIdIllegalValue As Integer = 1105
#End Region

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If Not IsNumeric(txtReferenceX.Text) Or Not IsNumeric(txtReferenceY.Text) Or Not IsNumeric(txtScaleX.Text) Or Not IsNumeric(txtScaleY.Text) Then
            MsgBox(GisControlView.LangStr(_strIdIllegalValue), MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub GeoreferenceDialog_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        lblScale.Text = GisControlView.LangStr(_strIdScale)
        lblCoordinates.Text = GisControlView.LangStr(_strIdCoordinates)
        btnOK.Text = GisControlView.LangStr(_strIdOK)
        btnCancel.Text = GisControlView.LangStr(_strIdCancel)
    End Sub
End Class