﻿Public Class ProgressDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 1000
    Private Const _strIdCancel As Integer = 1001
#End Region

#Region "Declarations"
    Private _userClosed As Boolean
    Private _silentClose As Boolean
    Private _strIdConfAbort As String

    Public Property UserClosed() As Boolean
        Get
            Return _userClosed
        End Get
        Set(ByVal value As Boolean)
            _userClosed = value
        End Set
    End Property
#End Region

    Public Shadows Sub Close()
        'Close without prompting
        _silentClose = True
        MyBase.Close()
    End Sub

	Public Shadows Sub Show(ByVal owner As System.Windows.Forms.Control, ByVal strConfAbort As String)
		_strIdConfAbort = strConfAbort
		'Call shadowed base class function
		CType(Me, System.Windows.Forms.Form).Show(owner)

		'Manually center on parent because setting StartupPosition to CenterParent doesn't work in this case
		Me.Location = New Point(owner.Left + (owner.Width / 2) - (Me.Width / 2), owner.Top + (owner.Height / 2) - (Me.Height / 2))
	End Sub

    Private Sub ConfirmAbort()
		'If MsgBox(GisControlView.LangStr(_strIdConfirmAbort), MsgBoxStyle.YesNo Or MsgBoxStyle.Exclamation) = MsgBoxResult.Yes Then
		If MsgBox(_strIdConfAbort, MsgBoxStyle.YesNo Or MsgBoxStyle.Exclamation) = MsgBoxResult.Yes Then
			Me.DialogResult = Windows.Forms.DialogResult.Cancel
            _userClosed = True
            _silentClose = True
		End If
    End Sub

    Private Sub ProgressDialog_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Set up language dependent strings
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        btnCancel.Text = GisControlView.LangStr(_strIdCancel)
    End Sub

    Private Sub ProgressDialog_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If _silentClose = False Then
            ConfirmAbort()
            e.Cancel = True
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ConfirmAbort()
    End Sub
End Class
