﻿Imports TatukGIS.NDK
Imports System.IO
Imports System.Data.OleDb

Public Class ExportToTCNavigatorDialog
#Region "Language"
    Private Const _strIdFormCaption As Integer = 2900
    Private Const _strIdProjectName As Integer = 2901
    Private Const _strIdPolygonLayer As Integer = 2902
    Private Const _strIdPointLayer As Integer = 2903
    Private Const _strIdFilePath As Integer = 2904
    Private Const _strIdOK As Integer = 2905
    Private Const _strIdCancel As Integer = 2906
    Private Const _strIdLayers As Integer = 2907
#End Region

#Region "Declarations"
    Private _gisCtrl As TatukGIS.NDK.WinForms.TGIS_ViewerWnd
    Private _activeLayerName As String
    Private _selectedOrigin As TGIS_Point
    Private _selectedEndPoint As TGIS_Point

    Public Sub New(ByVal layerName As String, ByVal selectedOrigin As TGIS_Point, ByVal selectedEndPoint As TGIS_Point)
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _gisCtrl = GisControlView.Instance.GisCtrl 'Shorthand
        _activeLayerName = layerName
        _selectedOrigin = selectedOrigin
        _selectedEndPoint = selectedEndPoint

        EnableOkButton()
    End Sub
#End Region

#Region "Helper functions"
    Private Sub ListLayers(ByVal cmb As ComboBox)
        Dim i As Integer

        'List all visible vector layers (in reverse order to match legend)
        Dim ll As TGIS_LayerAbstract
        For i = _gisCtrl.Items.Count - 1 To 0 Step -1
            ll = _gisCtrl.Items(i)
            If ll.Active AndAlso TypeOf ll Is TGIS_LayerVector AndAlso ll.Name <> GisControlView.PreviewLayerName Then
                cmb.Items.Add(New NameCaptionPair(ll.Name, ll.Caption))
            End If
        Next

        'If default layer couldn't be found or was not specified pick new layer
        If cmb.SelectedIndex < 0 Then cmb.SelectedIndex = 0 'New layer
    End Sub

    Private Sub EnableOkButton()
        Dim bError = False

        'No destination path?
        If String.IsNullOrEmpty(uxExportPath.Text) Then
            bError = True
        End If

        'No project name?
        If String.IsNullOrEmpty(uxExportName.Text) Then
            bError = True
        End If

        'Enable or disable the OK button
        If bError = True Then
            uxOK.Enabled = False
        Else
            uxOK.Enabled = True
        End If
    End Sub
#End Region

#Region "Event handlers"
    Private Sub ExportToTCNavigatorDialog_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Set up language
        Me.Text = GisControlView.LangStr(_strIdFormCaption)
        uxOK.Text = GisControlView.LangStr(_strIdOK)
        uxCancel.Text = GisControlView.LangStr(_strIdCancel)

        uxExportNameLabel.Text = GisControlView.LangStr(_strIdProjectName)
        uxExportLayers.Text = GisControlView.LangStr(_strIdLayers)
        uxExportPointLayerLabel.Text = GisControlView.LangStr(_strIdPointLayer)
        uxExportPolygonLayerLabel.Text = GisControlView.LangStr(_strIdPolygonLayer)
        uxExportPathLabel.Text = GisControlView.LangStr(_strIdFilePath)

        'List appropriate layers
        If uxExportPointLayer.Enabled Then ListLayers(uxExportPointLayer)
        If uxExportPolygonLayer.Enabled Then ListLayers(uxExportPolygonLayer)

    End Sub

    Private Sub uxOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxOK.Click
        Dim i As Integer, po As Integer, pa As Integer
        Dim lPoint As TGIS_LayerSHP = Nothing, lPolygon As TGIS_LayerSHP = Nothing
        Dim ll As TGIS_LayerVector = Nothing, lp As TGIS_LayerVector, lt As TGIS_LayerVector = Nothing
        Dim shp As TGIS_Shape, shpRect As TGIS_Shape
        Dim fileName = "test"
        Dim pathFiles As String
        Dim shpPolygon As TGIS_Shape = Nothing
        Dim deg As Double, min As Double, sec As Double, frac As Double
        Dim sign As Integer
        Dim lat As String, lon As String, desc As String
        Dim pt As TGIS_Point
        Dim fs As FileStream    ' FileStream(pathFiles & "\" & fileName & "_point.way", IO.FileMode.Create, IO.FileAccess.Write)
        Dim s As StreamWriter   'New StreamWriter(fs)
        Dim plotnum As Integer = 0
        Dim stratum As String = "", person As String = ""
        Dim cmd As OleDbCommand = Nothing
        Dim reader As OleDbDataReader
        Dim rt As RelationType
        Dim jv As String = vbNullString
        Dim shpPoly As TGIS_Shape


        'No destination path?
        If String.IsNullOrEmpty(uxExportPath.Text) Then
            Exit Sub
        End If
        pathFiles = uxExportPath.Text

        'No project name?
        If String.IsNullOrEmpty(uxExportName.Text) Then
            Exit Sub
        End If
        fileName = uxExportName.Text


        Dim oldCursor As Cursor = _gisCtrl.Cursor
        _gisCtrl.Cursor = Cursors.WaitCursor

        Dim layerPointName As String = "", layerPolygonName As String = ""
        layerPointName = CType(uxExportPointLayer.SelectedItem, NameCaptionPair).Name()
        layerPolygonName = CType(uxExportPolygonLayer.SelectedItem, NameCaptionPair).Name()

        'Create temp layer with selection shape
        lp = GisControlView.GetPreviewLayer()
        lp.Transparency = 0 'Hide this layer

        'Selected rectangle
        shpRect = lp.CreateShape(TGIS_ShapeType.gisShapeTypePolygon)
        shpRect.AddPart()
        shpRect.AddPoint(_gisCtrl.ScreenToMap(TGIS_Utils.Point(_selectedOrigin.X, _selectedOrigin.Y)))     'x1y1
        shpRect.AddPoint(_gisCtrl.ScreenToMap(TGIS_Utils.Point(_selectedEndPoint.X, _selectedOrigin.Y)))   'x2y1
        shpRect.AddPoint(_gisCtrl.ScreenToMap(TGIS_Utils.Point(_selectedEndPoint.X, _selectedEndPoint.Y))) 'x2y2
        shpRect.AddPoint(_gisCtrl.ScreenToMap(TGIS_Utils.Point(_selectedOrigin.X, _selectedEndPoint.Y)))   'x1y2


        'Create a temporary polygon and point layer
        lPolygon = New TGIS_LayerSHP
        lPolygon.CS = _gisCtrl.CS
        lPolygon.Path = pathFiles & "\" & fileName & "_bnd.shp"

        lPoint = New TGIS_LayerSHP
        lPoint.CS = _gisCtrl.CS
        lPoint.Path = pathFiles & "\" & fileName & "_point.shp"

        'Export to Solo Waypoint file
        fs = New FileStream(pathFiles & "\" & fileName & "_point.way", IO.FileMode.Create, IO.FileAccess.Write)
        s = New StreamWriter(fs)


        'Lock the GIS control
        _gisCtrl.Lock()

        'Loop through the point and polygon layer
        For i = 1 To 0 Step -1
            If i = 0 Then
                If Not String.IsNullOrEmpty(layerPointName) Then
                    ll = _gisCtrl.Get(layerPointName)
                Else
                    Continue For
                End If
            ElseIf i = 1 Then
                If Not String.IsNullOrEmpty(layerPolygonName) Then
                    ll = _gisCtrl.Get(layerPolygonName)

                    If TypeOf ll Is TGIS_LayerSqlAdo Then
                        'Is the polygon layer a TCruise tract layer?
                        GisControlView.GetLayerRelationType(ll.Name, rt, jv)
                        If rt = RelationType.TCTractRelation Then
                            lt = ll
                            cmd = New OleDbCommand("", GisControlView.Conn)
                        End If
                    End If
                Else
                    Continue For
                End If
            End If

            'Export all intersecting shape within current layer
            shp = ll.FindFirst(ll.ProjectedExtent)
            Do Until shp Is Nothing
                'Check for intersection (make sure shape is not within temp layer)
                If i = 0 Then    'Point
                    If shp.ShapeType = TGIS_ShapeType.gisShapeTypePoint Then
                        If shpRect.Intersect(shp) Then
                            'Export to point shape file
                            lPoint.AddShape(shp, True)

                            'Export to SOLO way file
                            plotnum += 1
                            pt = _gisCtrl.CS.ToWGS(shp.GetPoint(0, 0))

                            TGIS_Utils.GisDecodeLatitude(pt.Y, deg, min, sec, frac, sign, 4)
                            If sign < 0 Then lat = "-" Else lat = vbNullString
                            lat += CStr(deg).PadLeft(2, "0"c) & CStr(min).PadLeft(2, "0"c) & "." & CStr(sec).PadLeft(2, "0"c) & CStr(frac).PadLeft(4, "0"c)

                            TGIS_Utils.GisDecodeLongitude(pt.X, deg, min, sec, frac, sign, 4)
                            If sign < 0 Then lon = "-" Else lon = vbNullString
                            lon += CStr(deg).PadLeft(2, "0"c) & CStr(min).PadLeft(2, "0"c) & "." & CStr(sec).PadLeft(2, "0"c) & CStr(frac).PadLeft(4, "0"c)

                            'Is the selected polygon layer a TCruise tract?
                            If lt IsNot Nothing Then
                                'Check the underlying polygon for TCruise data
                                shpPoly = lt.Locate(shp.GetPoint(0, 0), 0)
                                If shpPoly IsNot Nothing Then
                                    'We got a shape, now retrieve the data from it
                                    Try
                                        cmd.CommandText = "SELECT TractId,Cruiser FROM " & GisControlView.GisTablePrefix & lt.Name & "_FEA f"
                                        If rt <> RelationType.Undefined AndAlso jv <> vbNullString Then
                                            cmd.CommandText += " LEFT JOIN " & jv & " ON " & jv & "." & GisControlView.ForeignKeyField & " = f." & GisControlView.GetJoinPrimary(jv)
                                        End If
                                        'Only select the uid of the shape
                                        cmd.CommandText += " WHERE UID=" & shpPoly.Uid

                                        reader = cmd.ExecuteReader(CommandBehavior.SingleRow)
                                        If reader.Read() Then
                                            If Not reader.IsDBNull(0) Then
                                                stratum = reader.GetString(0)
                                            End If

                                            If Not reader.IsDBNull(1) Then
                                                person = reader.GetString(1)
                                            End If
                                        End If
                                        reader.Close()
                                    Catch ex As OleDbException
                                    End Try
                                End If
                            End If

                            'desc should contain PlotID, StratumID and PersonID
                            desc = CStr(plotnum) & "^" & stratum & "^" & person

                            'empty the strings
                            stratum = vbNullString
                            person = vbNullString

                            'Format is waypoint name,lat,lon,elevation,description (where lat/lon is in format ddmm.ssssss)
                            s.Write(CStr(plotnum) & "," & lat & "," & lon & ",0," & desc & "," & vbCrLf)
                        End If
                    End If
                ElseIf i = 1 Then    'Polygon
                    If shp.ShapeType = TGIS_ShapeType.gisShapeTypePolygon Then
                        If shpRect.Intersect(shp) Then
                            'First shape to add?
                            If shpPolygon Is Nothing Then
                                shpPolygon = lPolygon.AddShape(shp, False)
                                shpPolygon.Lock(TGIS_Lock.gisLockExtent)
                            Else
                                'Go through all shapes of the polygon to add
                                For pa = 0 To shp.GetNumParts - 1
                                    shpPolygon.AddPart()
                                    For po = 0 To shp.GetPartSize(pa) - 1
                                        'shp.Parts(p)
                                        shpPolygon.AddPoint(shp.GetPoint(pa, po))
                                    Next
                                Next
                            End If

                        End If
                    End If
                End If

                shp = ll.FindNext()
            Loop
        Next


        'Save any created layers
        If s IsNot Nothing Then
            s.Close()
        End If

        If lPoint IsNot Nothing Then
            Try
                lPoint.SaveAll()
                lPoint.Dispose()
                lPoint = Nothing
            Catch ex As IOException
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Catch ex As EGIS_Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            End Try
        End If
        If lPolygon IsNot Nothing Then
            Try
                If Not shpPolygon Is Nothing Then shpPolygon.Unlock()
                lPolygon.SaveAll()
                lPolygon.Dispose()
                lPolygon = Nothing
            Catch ex As IOException
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Catch ex As EGIS_Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            End Try
        End If


        'Make all vector layers temporarily invisible
        Dim list As New ArrayList
        For i = 0 To _gisCtrl.Items.Count - 1
            If TypeOf _gisCtrl.Items.Items(i) Is TGIS_LayerVector Then
                ll = _gisCtrl.Items.Items(i)
                If ll.Active And Not ll.Name = lp.Name Then
                    list.Add(i)
                    ll.Active = False
                End If
            End If
        Next



        'Export raster data inside selection box as GeoTIFF
        _gisCtrl.ExportToImage(pathFiles & "\" & fileName & ".tif", shpRect.Extent, shpRect.Extent.XMax - shpRect.Extent.XMin, shpRect.Extent.YMax - shpRect.Extent.YMin, 80, 0, 96)

        'Rename the PRJ file from filename.extension.prj to filename.prj
        If File.Exists(pathFiles & "\" & fileName & ".tif.prj") Then
            Try
                File.Copy(pathFiles & "\" & fileName & ".tif.prj", pathFiles & "\" & fileName & ".prj", True)
                File.Delete(pathFiles & "\" & fileName & ".tif.prj")
            Catch ex As IOException
            End Try
        End If

        'Make vector layers visible again
        For i = 0 To list.Count - 1
            ll = _gisCtrl.Items.Items(list.Item(i))
            ll.Active = True
        Next

        'Remove temp layer
        If _gisCtrl.Get(GisControlView.PreviewLayerName) IsNot Nothing Then _gisCtrl.Delete(GisControlView.PreviewLayerName)

        'Unlock the GIS control again
        _gisCtrl.Unlock()

        Cursor.Current = oldCursor

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub uxBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxBrowse.Click
        'Let user choose destination path
        Dim dlgFolder As New FolderBrowserDialog
        Dim ret = dlgFolder.ShowDialog()
        If ret = DialogResult.Abort Or ret = DialogResult.Cancel Then
            Exit Sub
        End If

        uxExportPath.Text = dlgFolder.SelectedPath()

        EnableOkButton()
    End Sub

    Private Sub uxCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxCancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub uxExportName_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uxExportName.Validated
        EnableOkButton()
    End Sub
#End Region
End Class
