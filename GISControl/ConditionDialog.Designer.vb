﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ConditionDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnOK = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.cmbCondition1 = New System.Windows.Forms.ComboBox
        Me.cmbCondition2 = New System.Windows.Forms.ComboBox
        Me.btnAnd = New System.Windows.Forms.RadioButton
        Me.btnOr = New System.Windows.Forms.RadioButton
        Me.txtValue1 = New System.Windows.Forms.TextBox
        Me.txtValue2 = New System.Windows.Forms.TextBox
        Me.lblDescription = New System.Windows.Forms.Label
        Me.lblField = New System.Windows.Forms.Label
        Me.btnRemove = New System.Windows.Forms.Button
        Me.lblInstructions = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(250, 167)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 5
        Me.btnOK.Text = "btnOK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(331, 167)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 6
        Me.btnCancel.Text = "btnCancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'cmbCondition1
        '
        Me.cmbCondition1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCondition1.FormattingEnabled = True
        Me.cmbCondition1.Location = New System.Drawing.Point(12, 53)
        Me.cmbCondition1.Name = "cmbCondition1"
        Me.cmbCondition1.Size = New System.Drawing.Size(194, 21)
        Me.cmbCondition1.TabIndex = 8
        '
        'cmbCondition2
        '
        Me.cmbCondition2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCondition2.FormattingEnabled = True
        Me.cmbCondition2.Location = New System.Drawing.Point(12, 103)
        Me.cmbCondition2.Name = "cmbCondition2"
        Me.cmbCondition2.Size = New System.Drawing.Size(194, 21)
        Me.cmbCondition2.TabIndex = 3
        '
        'btnAnd
        '
        Me.btnAnd.AutoSize = True
        Me.btnAnd.Location = New System.Drawing.Point(48, 80)
        Me.btnAnd.Name = "btnAnd"
        Me.btnAnd.Size = New System.Drawing.Size(59, 17)
        Me.btnAnd.TabIndex = 1
        Me.btnAnd.TabStop = True
        Me.btnAnd.Text = "btnAnd"
        Me.btnAnd.UseVisualStyleBackColor = True
        '
        'btnOr
        '
        Me.btnOr.AutoSize = True
        Me.btnOr.Location = New System.Drawing.Point(113, 80)
        Me.btnOr.Name = "btnOr"
        Me.btnOr.Size = New System.Drawing.Size(51, 17)
        Me.btnOr.TabIndex = 2
        Me.btnOr.TabStop = True
        Me.btnOr.Text = "btnOr"
        Me.btnOr.UseVisualStyleBackColor = True
        '
        'txtValue1
        '
        Me.txtValue1.Location = New System.Drawing.Point(212, 53)
        Me.txtValue1.Name = "txtValue1"
        Me.txtValue1.Size = New System.Drawing.Size(194, 20)
        Me.txtValue1.TabIndex = 0
        '
        'txtValue2
        '
        Me.txtValue2.Location = New System.Drawing.Point(212, 103)
        Me.txtValue2.Name = "txtValue2"
        Me.txtValue2.Size = New System.Drawing.Size(194, 20)
        Me.txtValue2.TabIndex = 4
        '
        'lblDescription
        '
        Me.lblDescription.AutoSize = True
        Me.lblDescription.Location = New System.Drawing.Point(9, 9)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(70, 13)
        Me.lblDescription.TabIndex = 8
        Me.lblDescription.Text = "lblDescription"
        '
        'lblField
        '
        Me.lblField.AutoSize = True
        Me.lblField.Location = New System.Drawing.Point(12, 36)
        Me.lblField.Name = "lblField"
        Me.lblField.Size = New System.Drawing.Size(39, 13)
        Me.lblField.TabIndex = 9
        Me.lblField.Text = "lblField"
        '
        'btnRemove
        '
        Me.btnRemove.Location = New System.Drawing.Point(12, 168)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(154, 23)
        Me.btnRemove.TabIndex = 7
        Me.btnRemove.Text = "btnRemove"
        Me.btnRemove.UseVisualStyleBackColor = True
        '
        'lblInstructions
        '
        Me.lblInstructions.AutoSize = True
        Me.lblInstructions.Location = New System.Drawing.Point(12, 137)
        Me.lblInstructions.Name = "lblInstructions"
        Me.lblInstructions.Size = New System.Drawing.Size(71, 13)
        Me.lblInstructions.TabIndex = 11
        Me.lblInstructions.Text = "lblInstructions"
        '
        'ConditionDialog
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(423, 202)
        Me.Controls.Add(Me.lblInstructions)
        Me.Controls.Add(Me.btnRemove)
        Me.Controls.Add(Me.lblField)
        Me.Controls.Add(Me.lblDescription)
        Me.Controls.Add(Me.txtValue2)
        Me.Controls.Add(Me.txtValue1)
        Me.Controls.Add(Me.btnOr)
        Me.Controls.Add(Me.btnAnd)
        Me.Controls.Add(Me.cmbCondition2)
        Me.Controls.Add(Me.cmbCondition1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ConditionDialog"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "ConditionDialog"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents cmbCondition1 As System.Windows.Forms.ComboBox
    Friend WithEvents cmbCondition2 As System.Windows.Forms.ComboBox
    Friend WithEvents btnAnd As System.Windows.Forms.RadioButton
    Friend WithEvents btnOr As System.Windows.Forms.RadioButton
    Friend WithEvents txtValue1 As System.Windows.Forms.TextBox
    Friend WithEvents txtValue2 As System.Windows.Forms.TextBox
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lblField As System.Windows.Forms.Label
    Friend WithEvents btnRemove As System.Windows.Forms.Button
    Friend WithEvents lblInstructions As System.Windows.Forms.Label
End Class
