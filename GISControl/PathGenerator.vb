﻿Imports System.Math
Imports TatukGIS.NDK

Public Class PathGenerator
#Region "Language"
    Private Const _strIdChooseAlgorithm As Integer = 2600
    Private Const _strIdRowColNavigation As Integer = 2601
    Private Const _strIdShortestPossibleRoute As Integer = 2602
    Private Const _strIdConfirmCancelRouteGeneration As Integer = 2603
    Private Const _strIdSequentialNavigation As Integer = 2604
#End Region

#Region "Declarations"
    Private _wp() As TGIS_Point
    Private _bl As TGIS_Shape
    Private _rc As List(Of IntIntPair)
    Private _startIdx As Integer
    Private _leftSide As Boolean
    Private _returnPath As Boolean
    Private _cs As TGIS_CSCoordinateSystem
    Private _userDirection As RouteDirection
    Private _owner As System.Windows.Forms.Control
    Private _shpRcRoute As TGIS_Shape, _shpRandRoute As TGIS_Shape, _shpSeqRoute As TGIS_Shape
    Private _dlgChoice As SingleChoiceDialog

    Private Const _distanceToleranceFactor As Double = 0.01 'Plot generation use calculated coordinates which aren't always exact thus the tolerance factor
    Private Const _idRc As Integer = 0
    Private Const _idRand As Integer = 1
    Private Const _idSeq As Integer = 2

    Public Sub New(ByVal wp() As TGIS_Point, ByVal bl As TGIS_Shape, ByVal rc As List(Of IntIntPair), ByVal startIdx As Integer, ByVal leftSide As Boolean, _
                   ByVal returnPath As Boolean, ByVal cs As TGIS_CSCoordinateSystem, ByVal rd As RouteDirection, ByVal owner As System.Windows.Forms.Control)
        _wp = wp
        _bl = bl
        _rc = rc
        _startIdx = startIdx
        _leftSide = leftSide
        _returnPath = returnPath
        _cs = cs
        _userDirection = rd
        _owner = owner
    End Sub
#End Region

#Region "Helper functions"
    Private Shared Function FindClosestNeighbour(ByVal wp() As TGIS_Point, ByVal idx As Integer, ByVal dist(,) As Double, ByRef visited() As Boolean, Optional ByVal checkVisitedOnly As Boolean = False) As Integer
        Dim mind As Double
        Dim i As Integer
        Dim nb As Integer = -1

        'Find the closest neighbour
        mind = Double.MaxValue
        For i = 0 To wp.Length - 1
            If (visited(i) = Not checkVisitedOnly) OrElse i = idx Then Continue For

            If dist(idx, i) < mind Then
                mind = dist(idx, i)
                nb = i
            End If
        Next

        Return nb
    End Function

    Private Shared Function FindClosestIndexNeighbour(ByVal wp() As TGIS_Point, ByVal idxs() As Integer, ByVal idx As Integer, ByVal dist(,) As Double, ByRef visited() As Boolean) As Integer
        Dim mind As Double
        Dim i As Integer
        Dim nb As Integer = -1

        'Find the closest neighbour (among listed indexes)
        mind = Double.MaxValue
        For i = 0 To idxs.Length - 1
            If visited(idxs(i)) OrElse idxs(i) = idx Then Continue For

            If dist(idx, idxs(i)) < mind Then
                mind = dist(idx, idxs(i))
                nb = idxs(i)
            End If
        Next

        Return nb
    End Function

    Private Shared Function FindClosestNeighbours(ByVal wp() As TGIS_Point, ByVal idx As Integer, ByVal dist(,) As Double, ByRef visited() As Boolean) As List(Of Integer)
        Dim mind As Double
        Dim i As Integer
        Dim nb As New List(Of Integer)

        'Find one or more closest neighbours (if distance are considered equal)
        mind = Double.MaxValue
        For i = 0 To wp.Length - 1
            If visited(i) OrElse i = idx Then Continue For

            If dist(idx, i) < mind * (1.0 - _distanceToleranceFactor) Then 'Point must be significantly closer to be considered closer
                mind = dist(idx, i)
                nb.Clear() 'Remove any previous elements
                nb.Add(i)
            ElseIf dist(idx, i) < mind * (1.0 + _distanceToleranceFactor) Then 'Check for equal distance (within tolerance)
                nb.Add(i)
            End If
        Next

        Return nb
    End Function

    Private Shared Function GetDirectionalPath(ByVal desiredDirection As TGIS_Point, ByVal cs As TGIS_CSCoordinateSystem, ByVal startIdx As Integer, ByVal wp() As TGIS_Point, ByVal visited() As Boolean) As List(Of Integer)
        Dim idx As Integer, i As Integer
        Dim mind As Double, theta As Double, d As Double
        Dim dir As TGIS_Point
        Dim path As New List(Of Integer)
        Const angleTolerance As Double = 30.0 / 180.0 * PI 'Angular tolerance (in radians)

        'Find more points in the same direction
        path.Add(startIdx)
        Do
            'Find closest point
            mind = Double.MaxValue
            For i = 0 To wp.Length - 1
                'Skip any visited points
                If visited(i) OrElse path.Contains(i) Then Continue For

                'Determine direction
                dir.X = wp(i).X - wp(path(path.Count - 1)).X
                dir.Y = wp(i).Y - wp(path(path.Count - 1)).Y

                'Angle check
                theta = GetAngle(dir, desiredDirection)
                If theta > -angleTolerance AndAlso theta < angleTolerance Then
                    'Distance check
                    d = GisControlView.GetPointToPointDistance(cs, wp(path(path.Count - 1)), wp(i))
                    If d < mind Then
                        mind = d
                        idx = i
                    End If
                End If
            Next

            'Add point to path before continuing down the line
            If mind < Double.MaxValue Then path.Add(idx)
        Loop While mind < Double.MaxValue
        path.RemoveAt(0) 'Start point should not be included in returned path

        Return path
    End Function

    Private Function GetWpIndex(ByVal row As Integer, ByVal col As Integer) As Integer
        Dim i As Integer

        'Find _wp index for this row/col
        For i = 0 To _rc.Count - 1
            If _rc(i).Val1 = row AndAlso _rc(i).Val2 = col Then
                Return i
            End If
        Next

        Return -1
    End Function

    Private Shared Function GetAngle(ByVal p0 As TGIS_Point, ByVal p1 As TGIS_Point) As Double
        Return Acos((p0.X * p1.X + p0.Y * p1.Y) / (Sqrt(p0.X * p0.X + p0.Y * p0.Y) * Sqrt(p1.X * p1.X + p1.Y * p1.Y)))
    End Function

    Private Shared Function ModFunc(ByVal i As Integer, ByVal n As Integer) As Integer
        If i Mod n >= 0 Then
            Return i Mod n
        Else
            Return i Mod n + n
        End If
    End Function

    Friend Sub RouteChange(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Select Case _dlgChoice.Choice
            Case _idRc
                If _shpRcRoute IsNot Nothing Then _shpRcRoute.IsHidden = False
                If _shpRandRoute IsNot Nothing Then _shpRandRoute.IsHidden = True
                If _shpSeqRoute IsNot Nothing Then _shpSeqRoute.IsHidden = True

            Case _idRand
                If _shpRcRoute IsNot Nothing Then _shpRcRoute.IsHidden = True
                If _shpRandRoute IsNot Nothing Then _shpRandRoute.IsHidden = False
                If _shpSeqRoute IsNot Nothing Then _shpSeqRoute.IsHidden = True

            Case _idSeq
                If _shpRcRoute IsNot Nothing Then _shpRcRoute.IsHidden = True
                If _shpRandRoute IsNot Nothing Then _shpRandRoute.IsHidden = True
                If _shpSeqRoute IsNot Nothing Then _shpSeqRoute.IsHidden = False
        End Select

        GisControlView.Instance.GisCtrl.Update()
    End Sub
#End Region

#Region "Main functions"
    Public Function GeneratePath() As Integer()
        'Let user choose which algorithm to use
        Dim i As Integer
        Dim lp As TGIS_LayerVector = GisControlView.GetPreviewLayer()
        Dim distRc As Double, distRand As Double, distSeq As Double
        Dim iorderRc() As Integer = GenerateRCPath()
        Dim iorderRand() As Integer = GenerateRandomizedPath()
        Dim iorderSeq() As Integer = GenerateSequentialPath()

        'Row/column sequential path
        If iorderRc IsNot Nothing Then
            For i = 1 To iorderRc.Length - 1
                distRc += GisControlView.GetPointToPointDistance(_cs, _wp(iorderRc(i - 1)), _wp(iorderRc(i)))
            Next
        End If

        'Optimized path
        If iorderRand IsNot Nothing Then
            For i = 1 To iorderRand.Length - 1
                distRand += GisControlView.GetPointToPointDistance(_cs, _wp(iorderRand(i - 1)), _wp(iorderRand(i)))
            Next
        End If

        'Sequential path
        If iorderSeq IsNot Nothing Then
            For i = 1 To iorderSeq.Length - 1
                distSeq += GisControlView.GetPointToPointDistance(_cs, _wp(iorderSeq(i - 1)), _wp(iorderSeq(i)))
            Next
        End If

        'Create preview shapes for routes
        'RC
        If iorderRc IsNot Nothing Then 'Sequential row/column route will only be created when row/column data exist
            _shpRcRoute = lp.CreateShape(TGIS_ShapeType.gisShapeTypeArc)
            _shpRcRoute.AddPart()
            For i = 0 To iorderRc.Length - 1
                _shpRcRoute.AddPoint(_wp(iorderRc(i)))
            Next
        End If

        'Rand
        If iorderRand IsNot Nothing Then
            _shpRandRoute = lp.CreateShape(TGIS_ShapeType.gisShapeTypeArc)
            _shpRandRoute.AddPart()
            For i = 0 To iorderRand.Length - 1
                _shpRandRoute.AddPoint(_wp(iorderRand(i)))
            Next
        End If

        'Sequential
        If iorderSeq IsNot Nothing Then
            _shpSeqRoute = lp.CreateShape(TGIS_ShapeType.gisShapeTypeArc)
            _shpSeqRoute.AddPart()
            For i = 0 To iorderSeq.Length - 1
                _shpSeqRoute.AddPoint(_wp(iorderSeq(i)))
            Next
        End If

        'Display avaliable paths in user choice dialog
        Dim items As New List(Of ValueDescriptionPair)
        Dim unit As String = " " & GisControlView.Instance.GetScaleUnit().Symbol
        If iorderRc IsNot Nothing Then items.Add(New ValueDescriptionPair(_idRc, GisControlView.LangStr(_strIdRowColNavigation) & " (" & Math.Round(distRc) & unit & ")"))
        If iorderSeq IsNot Nothing Then items.Add(New ValueDescriptionPair(_idSeq, GisControlView.LangStr(_strIdSequentialNavigation) & " (" & Math.Round(distSeq) & unit & ")"))
        If iorderRand IsNot Nothing Then items.Add(New ValueDescriptionPair(_idRand, GisControlView.LangStr(_strIdShortestPossibleRoute) & " (" & Math.Round(distRand) & unit & ")"))

        _dlgChoice = New SingleChoiceDialog(GisControlView.LangStr(_strIdChooseAlgorithm), items)
        AddHandler _dlgChoice.uxChoice.SelectedIndexChanged, AddressOf RouteChange 'Our own event handler so we can show/hide selected route
        _dlgChoice.ShowDialog()
        GisControlView.Instance.GisCtrl.Delete(GisControlView.PreviewLayerName) 'Remove preview layer when done

        If _dlgChoice.DialogResult = DialogResult.OK Then
            Select Case _dlgChoice.Choice
                Case _idRc
                    Return iorderRc
                Case _idRand
                    Return iorderRand
                Case _idSeq
                    Return iorderSeq
            End Select
        End If

        Return Nothing
    End Function

    Public Function GenerateRandomizedPath() As Integer()
        Try 'If a user tries to genereate a route between too many plots this function will fail
            Dim maxd As Double
            Dim xmin As Double, xmax As Double
            Dim ymin As Double, ymax As Double
            'Dim pathlen As Double
            Dim dist(_wp.Length - 1, _wp.Length - 1) As Double
            Dim dis(_wp.Length - 1) As Double
            Dim arc(_wp.Length - 1) As Integer
            Dim mst(_wp.Length - 1) As Integer
            Dim iorder(_wp.Length - 1) As Integer, jorder(_wp.Length - 1) As Integer
            Dim d As Double
            Dim i As Integer, j As Integer, k As Integer, l As Integer, a As Integer
            Dim progress As New ProgressDialog

            'Find extreme points
            xmin = _wp(0).X : xmax = _wp(0).X
            ymin = _wp(0).Y : ymax = _wp(0).Y
            For i = 1 To _wp.Length - 1
                If _wp(i).X < xmin Then xmin = _wp(i).X
                If _wp(i).X > xmax Then xmax = _wp(i).X
                If _wp(i).Y < ymin Then ymin = _wp(i).Y
                If _wp(i).Y > ymax Then ymax = _wp(i).Y
            Next

            'Compute distance matrix
            For i = 0 To _wp.Length - 1
                For j = 0 To _wp.Length - 1
                    dist(i, j) = GisControlView.GetPointToPointDistance(_cs, _wp(i), _wp(j))
                Next
            Next

            'Find out max distance
            maxd = GisControlView.GetPointToPointDistance(_cs, xmin, ymin, xmax, ymax)

            'Prepare (fill array with distance from start to each vertex, find nearest neighbour)
            d = maxd
            dis(0) = -1
            For i = 1 To _wp.Length - 1
                dis(i) = dist(i, 0)
                If dis(i) < d Then
                    d = dis(i)
                    j = i
                End If
            Next

            'O(n^2) Minimum Spanning Trees by Prim and Jarnick for graphs with adjacency matrix
            For a = 0 To _wp.Length - 2
                mst(a) = j * _wp.Length + arc(j) 'Join fragment j with MST
                dis(j) = -1
                d = maxd
                For i = 0 To _wp.Length - 1
                    If dis(i) >= 0 Then 'Not connected yet
                        If dis(i) > dist(i, j) Then
                            dis(i) = dist(i, j)
                            arc(i) = j
                        End If
                        If d > dis(i) Then
                            d = dis(i)
                            k = i
                        End If
                    End If
                Next
                j = k
            Next

            'Preorder tour of MST
            k = 0 : l = 1 : d = 0 : arc(0) = 0
            Do While l <> 0 'Not empty
                l = l - 1
                i = arc(l)
                If jorder(i) = 0 Then 'Not visited
                    iorder(k) = i
                    k = k + 1
                    jorder(i) = 1 'Visited
                    For j = 0 To _wp.Length - 2 'Push all kids of i
                        If i = mst(j) Mod _wp.Length Then
                            arc(l) = (mst(j) - mst(j) Mod _wp.Length) / _wp.Length
                            l = l + 1
                        End If
                    Next
                End If
            Loop


            'Annealing
            If _wp.Length > 4 Then 'We need at least four vertices to do this
                Dim p(2) As Integer
                Dim n As Integer = _wp.Length - 1
                Dim pathchg As Integer
                Dim numOnPath As Integer, numNotOnPath As Integer
                Dim energyChange As Double, t As Double

                Const FinalT As Double = 0.1
                Const InitialT As Double = 100
                Const CoolingT As Double = 0.9 'Lower down T (< 1)
                Dim TriesPerT As Integer = 500 * _wp.Length
                Dim ImprovedPathPerT = 60 * _wp.Length

                'Calculate initial path length
                'For i = 0 To iorder.Length - 2
                '    pathlen = pathlen + dist(iorder(i), iorder(i + 1))
                'Next

                progress.Show(_owner, GisControlView.LangStr(_strIdConfirmCancelRouteGeneration))
                _owner.Enabled = False

                t = InitialT
                Do While t > FinalT 'Annealing schedule
                    pathchg = 0
                    For j = 0 To TriesPerT - 1
                        Do
                            p(0) = Rnd() * (n - 1)
                            p(1) = Rnd() * (n - 1)
                            If p(0) = p(1) Then p(1) = ModFunc(p(0) + 1, n) 'Non-empty path
                            numOnPath = ModFunc(p(1) - p(0), n) + 1
                            numNotOnPath = n - numOnPath
                        Loop While numOnPath < 2 OrElse numNotOnPath < 2 'Non-empty path

                        If CInt(Rnd()) Mod 2 Then 'Three way
                            Do
                                p(2) = ModFunc(Rnd() * (numNotOnPath - 1) + p(1) + 1, n)
                            Loop While p(0) = ModFunc(p(2) + 1, n) 'Avoids a non-change

                            'Calculate three way cost
                            Dim aa As Integer, bb As Integer, cc As Integer, dd As Integer, ee As Integer, ff As Integer
                            aa = iorder(ModFunc(p(0) - 1, n)) : bb = iorder(p(0))
                            cc = iorder(p(1)) : dd = iorder(ModFunc(p(1) + 1, n))
                            ee = iorder(p(2)) : ff = iorder(ModFunc(p(2) + 1, n))
                            energyChange = dist(aa, dd) + dist(ee, bb) + dist(cc, ff) - dist(aa, bb) - dist(cc, dd) - dist(ee, ff) 'Add cost between dd and ee if non symetric TSP

                            If energyChange < 0 Then
                                pathchg = pathchg + 1
                                'pathlen = pathlen + energyChange

                                'Do three way
                                Dim count As Integer, m1 As Integer, m2 As Integer, m3 As Integer
                                aa = ModFunc(p(0) - 1, n) : bb = p(0)
                                cc = p(1) : dd = ModFunc(p(1) + 1, n)
                                ee = p(2) : ff = ModFunc(p(2) + 1, n)

                                m1 = ModFunc(n + cc - bb, n) + 1 'Num cities from bb to cc
                                m2 = ModFunc(n + aa - ff, n) + 1 'Num cities from ff to aa
                                m3 = ModFunc(n + ee - dd, n) + 1 'Num cities from dd to ee

                                count = 0

                                '[b..c]
                                For i = 0 To m1 - 1
                                    jorder(count) = iorder(ModFunc(i + bb, n))
                                    count = count + 1
                                Next

                                '[f..a]
                                For i = 0 To m2 - 1
                                    jorder(count) = iorder(ModFunc(i + ff, n))
                                    count = count + 1
                                Next

                                '[d..e]
                                For i = 0 To m3 - 1
                                    jorder(count) = iorder(ModFunc(i + dd, n))
                                    count = count + 1
                                Next

                                'Copy segment back into iorder
                                For i = 0 To n - 1
                                    iorder(i) = jorder(i)
                                Next
                            End If
                        Else 'Path reverse
                            Dim aa As Integer, bb As Integer, cc As Integer, dd As Integer
                            aa = iorder(ModFunc(p(0) - 1, n)) : bb = iorder(p(0))
                            cc = iorder(p(1)) : dd = iorder(ModFunc(p(1) + 1, n))
                            energyChange = dist(dd, bb) + dist(cc, aa) - dist(aa, bb) - dist(cc, dd) 'Add cost between cc and bb if non symetric TSP

                            If energyChange < 0 Then
                                pathchg = pathchg + 1
                                'pathlen = pathlen + energyChange

                                'Reverse path b..c
                                Dim nswaps As Integer, first As Integer, last As Integer, tmp As Integer
                                nswaps = (ModFunc(p(1) - p(0), n) + 1) / 2
                                For i = 0 To nswaps - 1
                                    first = ModFunc(p(0) + i, n)
                                    last = ModFunc(p(1) - i, n)
                                    tmp = iorder(first)
                                    iorder(first) = iorder(last)
                                    iorder(last) = tmp
                                Next
                            End If
                        End If

                        If pathchg > ImprovedPathPerT Then Exit For 'Finish early
                    Next

                    'Update progress bar
                    progress.ProgressBar.Value = (Math.Log(t / InitialT, CoolingT) / Math.Log(FinalT / InitialT, CoolingT)) * 100
                    Application.DoEvents()

                    'Check if user canceled this operation
                    If progress.UserClosed Then Exit Do

                    If pathchg = 0 Then Exit Do 'If no change then quit
                    t = t * CoolingT
                Loop

                _owner.Enabled = True 'Enable before progress dialog is close so we don't lose focus
                progress.Close()
            End If

            If progress.UserClosed Then Return Nothing

            'Rearrange path so start point comes first
            For i = 0 To iorder.Length - 1
                If iorder(i) = _startIdx Then 'Find selected start point
                    For j = i To iorder.Length - 1 'Copy current index until end
                        jorder(j - i) = iorder(j)
                    Next
                    For j = 0 To i - 1 'Copy start until current index
                        jorder(j + (iorder.Length - i)) = iorder(j)
                    Next
                    For j = 0 To iorder.Length - 1 'Replace index order
                        iorder(j) = jorder(j)
                    Next
                    Exit For
                End If
            Next

            Return iorder
        Catch ex As OutOfMemoryException 'Handle any out-of-memory exception gently
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

        Return Nothing
    End Function

    Public Function GenerateGridPath() As Integer()
        ReDim Preserve _wp(_wp.Length - 2) 'Remove last element in _wp (which is empty)
        Dim visited(_wp.Length - 1) As Boolean
        Dim dist(_wp.Length - 1, _wp.Length - 1) As Double
        Dim nb As List(Of Integer) 'Neighbours
        Dim path As New List(Of Integer), subPath As List(Of Integer) = Nothing, tmpPath As List(Of Integer)
        Dim d As Double
        Dim i As Integer, j As Integer, dirlen As Integer
        Dim dir As TGIS_Point

        'Compute distance matrix
        For i = 0 To _wp.Length - 1
            For j = 0 To _wp.Length - 1
                dist(i, j) = GisControlView.GetPointToPointDistance(_cs, _wp(i), _wp(j))
            Next
        Next

        'Find nearest neighbours
        nb = FindClosestNeighbours(_wp, _startIdx, dist, visited)

        'If we have several neighbours with equal distance we have to pick one of them, check how far we can travel in each direction
        For i = 0 To nb.Count - 1
            'Determine direction
            dir.X = _wp(nb(i)).X - _wp(_startIdx).X
            dir.Y = _wp(nb(i)).Y - _wp(_startIdx).Y

            'Obtain a subpath in the desired direction
            tmpPath = GetDirectionalPath(dir, _cs, _startIdx, _wp, visited)

            'Keep track of the neighbour with longest path
            If tmpPath.Count > dirlen Then
                dirlen = tmpPath.Count
                subPath = tmpPath
            End If
        Next

        'Construct path
        path.Add(_startIdx)
        visited(_startIdx) = True
        Do 'While there exist unvisited points
            'Add subpath to path, mark each point as visited
            path.AddRange(subPath)
            For i = 0 To subPath.Count - 1
                visited(subPath(i)) = True
            Next

            'Jump to nearest neighbour, add it to path
            nb = FindClosestNeighbours(_wp, path(path.Count - 1), dist, visited)
            If nb.Count = 0 Then Exit Do
            path.Add(nb(0))
            visited(nb(0)) = True

            'Update preferred direction +180 deg (just negate the values on each axle)
            dir.X = -(_wp(subPath(subPath.Count - 1)).X - _wp(subPath(0)).X)
            dir.Y = -(_wp(subPath(subPath.Count - 1)).Y - _wp(subPath(0)).Y)
            If dir.X = 0 AndAlso dir.Y = 0 Then 'Prevent division by zero
                'Scale might differ on x/y, prefer the shortest axle
                d = Min(_wp(subPath(0)).X, _wp(subPath(0)).Y) * 0.01
                If _
                GisControlView.GetPointToPointDistance(_cs, _wp(subPath(0)).X, _wp(subPath(0)).Y, _wp(subPath(0)).X + d, _wp(subPath(0)).Y) > _
                GisControlView.GetPointToPointDistance(_cs, _wp(subPath(0)).X, _wp(subPath(0)).Y, _wp(subPath(0)).X, _wp(subPath(0)).Y + d) Then
                    dir.Y = 1
                Else
                    dir.X = 1
                End If
            End If

            'Obtain new sub path
            subPath = GetDirectionalPath(dir, _cs, path(path.Count - 1), _wp, visited)
            If subPath.Count = 0 Then 'Path cannot be obtained, pick nearest unvisited point
                nb = FindClosestNeighbours(_wp, path(path.Count - 1), dist, visited)
                If nb.Count = 0 Then Exit Do 'No more unvisited points, we are done
                subPath.Clear()
                subPath.Add(nb(0))
            End If
        Loop

        Return path.ToArray()
    End Function

    Public Function GenerateSequentialPath() As Integer()
        Dim visited(_wp.Length - 1) As Boolean
        Dim dist(_wp.Length - 1, _wp.Length - 1) As Double
        Dim nb As Integer, n0 As Integer, n1 As Integer, n2 As Integer
        Dim path As New List(Of Integer)
        Dim i As Integer, j As Integer
        Dim lineIdx As Integer, lineEndIdx As Integer, lineStep As Integer
        Dim pts As New List(Of Integer)

        'We need at least one point to construct a path
        If _wp.Length = 0 Then Return Nothing

        'Compute distance matrix
        For i = 0 To _wp.Length - 1
            For j = 0 To _wp.Length - 1
                dist(i, j) = GisControlView.GetPointToPointDistance(_cs, _wp(i), _wp(j))
            Next
        Next

        'Check if we have a border line shape (middle of power line)
        If _bl Is Nothing Then
            'Use fake values just to run one step
            lineEndIdx = 0
            lineStep = 1
            lineIdx = 0
        Else
            'Determine in which direction we should travel the line (check whether start point is closest to line start or line end)
            If GisControlView.GetPointToPointDistance(_cs, _wp(_startIdx), _bl.GetPoint(0, 0)) < GisControlView.GetPointToPointDistance(_cs, _wp(_startIdx), _bl.GetPoint(0, _bl.GetNumPoints() - 1)) Then
                'Forward
                lineEndIdx = _bl.GetNumPoints() - 1
                lineStep = 1
            Else
                'Reverse order
                lineEndIdx = 0
                lineStep = -1
            End If

            'Find line segment parallell to start point
            lineIdx = GisControlView.GetClosestLineSegment(_cs, _wp(_startIdx), _bl)
        End If

        'Add start point to path
        n1 = _startIdx
        path.Add(n1)
        visited(n1) = True

        'Go through all line segments in order
        For i = lineIdx To lineEndIdx Step lineStep
            'Collect all points parallell to this line segment
            pts.Clear()
            For j = 0 To _wp.Length - 1
                If _bl IsNot Nothing Then
                    'Add to path if point belongs to this line segment
                    If i = GisControlView.GetClosestLineSegment(_cs, _wp(j), _bl) Then pts.Add(j)
                Else
                    'Add every point, we don't have any line
                    pts.Add(j)
                End If
            Next

            'Extent path with collected points
            nb = n1 'Continue where we end up at last line segment
            n0 = -1
            Do 'While there exist unvisited points belonging to this line segment
                'Move on to closest neighbour
                n1 = nb
                nb = FindClosestIndexNeighbour(_wp, pts.ToArray(), n1, dist, visited)
                If nb < 0 Then Exit Do 'No more points to visit
                If n0 < 0 Then n0 = nb 'Remember first point on line segment

                'Check if we have passed any unvisited point and jump back if required (compare against first point along current line segment)
                n2 = FindClosestIndexNeighbour(_wp, pts.ToArray(), n0, dist, visited)
                If n2 >= 0 AndAlso dist(n2, n0) < dist(nb, n0) Then nb = n2

                'Add neighbour to path
                path.Add(nb)
                visited(nb) = True
            Loop
        Next

        Return path.ToArray()
    End Function

    Public Function GenerateRCPath() As Integer()
        Dim path As New List(Of Integer)
        Dim rowDist As Double, colDist As Double
        Dim row As Integer, col As Integer
        Dim minr As Integer = Integer.MaxValue, minc As Integer = Integer.MaxValue
        Dim maxr As Integer = Integer.MinValue, maxc As Integer = Integer.MinValue
        Dim i As Integer, j As Integer, ct As Integer, maxct As Integer, idx As Integer
        Dim idxStart As Integer, idxEnd As Integer
        Dim ptStart As TGIS_Point, ptEnd As TGIS_Point
        Dim dir As RouteDirection
        Dim increaseDirection As Boolean

        'Row/column indexes are required for this algorithm
        If _rc Is Nothing OrElse _rc.Count = 0 Then Return Nothing

        'Determine row/column dimensions
        For i = 0 To _rc.Count - 1
            'Row
            If _rc(i).Val1 < minr Then
                minr = _rc(i).Val1
            End If
            If _rc(i).Val1 > maxr Then
                maxr = _rc(i).Val1
            End If

            'Column
            If _rc(i).Val2 < minc Then
                minc = _rc(i).Val2
            End If
            If _rc(i).Val2 > maxc Then
                maxc = _rc(i).Val2
            End If
        Next

        'Make sure we have at least two plots in each direction
        If minr = maxr OrElse minc = maxc Then Return Nothing

        'Map row/column list into matrix (with offset)
        Dim rcm(maxr - minr, maxc - minc) As Boolean
        For i = 0 To _rc.Count - 1
            rcm(_rc(i).Val1 - minr, _rc(i).Val2 - minc) = True
        Next


        'Determine spacing along rows/columns
        'Row, find the row with the most number of plots
        maxct = 0
        For i = 0 To rcm.GetUpperBound(0)
            ct = 0
            For j = 0 To rcm.GetUpperBound(1)
                If rcm(i, j) Then ct += 1
            Next
            If ct > maxct Then
                maxct = ct
                idx = i
            End If
        Next

        'Determine index of first and last plot on this row
        idxStart = Integer.MaxValue : idxEnd = Integer.MinValue
        For i = 0 To rcm.GetUpperBound(1)
            If rcm(idx, i) Then
                If i < idxStart Then idxStart = i
                If i > idxEnd Then idxEnd = i
            End If
        Next

        'Obtain coordinates of first and last plot on row
        ptStart = Nothing : ptEnd = Nothing
        For i = 0 To _rc.Count - 1
            If _rc(i).Val1 = idx + minr AndAlso _rc(i).Val2 = idxStart + minc Then
                ptStart = _wp(i)
            ElseIf _rc(i).Val1 = idx + minr AndAlso _rc(i).Val2 = idxEnd + minc Then
                ptEnd = _wp(i)
            End If
        Next

        'Determine average distance between plots in a row
        rowDist = GisControlView.GetPointToPointDistance(_cs, ptStart, ptEnd) / CDbl(idxEnd - idxStart)


        'Column, find the column with the most number of plots
        maxct = 0
        For i = 0 To rcm.GetUpperBound(1)
            ct = 0
            For j = 0 To rcm.GetUpperBound(0)
                If rcm(j, i) Then ct += 1
            Next
            If ct > maxct Then
                maxct = ct
                idx = i
            End If
        Next

        'Determine index of first and last plot in this column
        idxStart = Integer.MaxValue : idxEnd = Integer.MinValue
        For i = 0 To rcm.GetUpperBound(0)
            If rcm(i, idx) Then
                If i < idxStart Then idxStart = i
                If i > idxEnd Then idxEnd = i
            End If
        Next

        'Obtain coordinates of first and last plot in column
        ptStart = Nothing : ptEnd = Nothing
        For i = 0 To _rc.Count - 1
            If _rc(i).Val1 = idxStart + minr AndAlso _rc(i).Val2 = idx + minc Then
                ptStart = _wp(i)
            ElseIf _rc(i).Val1 = idxEnd + minr AndAlso _rc(i).Val2 = idx + minc Then
                ptEnd = _wp(i)
            End If
        Next

        'Determine average distance between plots in a column
        colDist = GisControlView.GetPointToPointDistance(_cs, ptStart, ptEnd) / CDbl(idxEnd - idxStart)


        'Determine if we should travel along rows or columns
        If rowDist > colDist * (1.0 + _distanceToleranceFactor) Then
            'Travel along columns
            dir = RouteDirection.Column
        ElseIf colDist > rowDist * (1.0 + _distanceToleranceFactor) Then
            'Travel along rows
            dir = RouteDirection.Row
        Else
            'Prefer user-defined
            dir = _userDirection
        End If

        'Obtain starting position
        row = _rc(_startIdx).Val1
        col = _rc(_startIdx).Val2
        If row < 1 OrElse col < 1 Then Return Nothing 'Values start from 1, anything else will be ignored

        'Generate actual path
        If dir = RouteDirection.Column Then
            'Travel along column

            'Determine if we should go left or right along the columns
            If col < maxc Then increaseDirection = True

            'Go through columns
            Do
                'Determine if we should go up or down (a neighbour plot might be missing so we need to check the whole line)
                idx = -1
                For i = row + 1 To maxr
                    If rcm(i - minr, col - minc) Then
                        idx = i
                        Exit For
                    End If
                Next
                If idx < 0 Then
                    For i = row - 1 To minr Step -1
                        If rcm(i - minr, col - minc) Then
                            idx = i
                            Exit For
                        End If
                    Next
                End If


                'Travel along column
                If idx > row Then
                    For row = row To maxr 'Increase
                        If rcm(row - minr, col - minc) Then
                            path.Add(GetWpIndex(row, col))
                            idxEnd = row
                        End If
                    Next
                Else
                    For row = row To minr Step -1 'Decrease
                        If rcm(row - minr, col - minc) Then
                            path.Add(GetWpIndex(row, col))
                            idxEnd = row
                        End If
                    Next
                End If
                row = idxEnd


                'Jump to next column, we may have to jump several columns
                ptStart = _wp(GetWpIndex(row, col))
                Do
                    If increaseDirection Then
                        col += 1
                        If col > maxc Then Exit Do
                    Else
                        col -= 1
                        If col < minc Then Exit Do
                    End If

                    'Find nearest end point in next column
                    idxStart = -1 : idxEnd = -1
                    For i = minr To maxr
                        If rcm(i - minr, col - minc) AndAlso idxStart < 0 Then idxStart = i
                        If rcm(i - minr, col - minc) Then idxEnd = i
                    Next
                    If idxStart < 0 OrElse idxEnd < 0 Then Continue Do 'No plots in current column

                    If _
                    GisControlView.GetPointToPointDistance(_cs, _wp(GetWpIndex(idxStart, col)), ptStart) < _
                    GisControlView.GetPointToPointDistance(_cs, _wp(GetWpIndex(idxEnd, col)), ptStart) Then
                        row = idxStart
                    Else
                        row = idxEnd
                    End If

                    If col > 0 Then Exit Do 'Next column found (0 values should be ignored)
                Loop
                If col > maxc OrElse col < minc Then Exit Do
            Loop
        Else
            'Travel along row

            'Determine if we should up or down along the rows
            If row < maxr Then increaseDirection = True

            'Go through rows
            Do
                'Determine if we should go left or right (a neighbour plot might be missing so we need to check the whole line)
                idx = -1
                For i = col + 1 To maxc
                    If rcm(row - minr, i - minc) Then
                        idx = i
                        Exit For
                    End If
                Next
                If idx < 0 Then
                    For i = col - 1 To minc Step -1
                        If rcm(row - minr, i - minc) Then
                            idx = i
                            Exit For
                        End If
                    Next
                End If

                'Travel along row
                If idx > col Then
                    For col = col To maxc 'Increase
                        If rcm(row - minr, col - minc) Then
                            path.Add(GetWpIndex(row, col))
                            idxEnd = col
                        End If
                    Next
                Else
                    For col = col To minc Step -1 'Decrease
                        If rcm(row - minr, col - minc) Then
                            path.Add(GetWpIndex(row, col))
                            idxEnd = col
                        End If
                    Next
                End If
                col = idxEnd


                'Jump to next row, we may have to jump several rows
                ptStart = _wp(GetWpIndex(row, col))
                Do
                    If increaseDirection Then
                        row += 1
                        If row > maxr Then Exit Do
                    Else
                        row -= 1
                        If row < minr Then Exit Do
                    End If

                    'Find nearest end point in next row
                    idxStart = -1 : idxEnd = -1
                    For i = minc To maxc
                        If rcm(row - minr, i - minc) AndAlso idxStart < 0 Then idxStart = i
                        If rcm(row - minr, i - minc) Then idxEnd = i
                    Next
                    If idxStart < 0 OrElse idxEnd < 0 Then Continue Do 'No plots in current row

                    If _
                    GisControlView.GetPointToPointDistance(_cs, _wp(GetWpIndex(row, idxStart)), ptStart) < _
                    GisControlView.GetPointToPointDistance(_cs, _wp(GetWpIndex(row, idxEnd)), ptStart) Then
                        col = idxStart
                    Else
                        col = idxEnd
                    End If

                    If row > 0 Then Exit Do 'Next row found (0 values should be ignored)
                Loop
                If row > maxr OrElse row < minr Then Exit Do
            Loop
        End If

        Return path.ToArray()
    End Function
#End Region
End Class
